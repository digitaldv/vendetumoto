<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user-management' => [
		'class' => 'webvimark\modules\UserManagement\UserManagementModule',
		// 'enableRegistration' => true,
		//'passwordRegexp' => '^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$',	
		// Here you can set your handler to change layout for any controller or action	// Tip: you can use this event in any module
		'on beforeAction'=>function(yii\base\ActionEvent $event) {

				if ( $event->action->uniqueId == 'user-management/auth/login' )
				{
					$event->action->controller->layout = 'loginLayout.php';
				};
			},
	],
        'configuracion'      => ['class' => 'backend\modules\configuracion\configuracion', ],
        'clientes'           => ['class' => 'backend\modules\clientes\clientes', ],
        'reportes'           => ['class' => 'backend\modules\reportes\reportes',  ],
        'perfil'             => ['class' => 'backend\modules\perfil\perfil', ],




    ],
    'components' => [
        's3' => [
            'class' => 'frostealth\yii2\aws\s3\Service',
            'credentials' => [
                'key' => 'AKIAT7ZWMLPORSNEOKZR',
                'secret' => 'Wc2fYNEIsbUuwqqdaHiGRr0CaDEBKXX13VIea9YNs',
            ],
            'region' => 'us-east-2',
            'defaultBucket' => 'vtmprod',
            'defaultAcl' => 'public-read',
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',

            // Comment this if you don't want to record user logins
            'on afterLogin' => function($event) {
                    \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
                }
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                   '@app/views' => '@app/views'//'@vendor/jcabanillas/yii2-inspinia/views'
                ],
            ],
       ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.sendgrid.net',
                'username' => 'apikey',
                'password' => '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', //'AmUWSWqaQmLypudnL58KPqNOXKNhU7YtgiEEmcZx9KEy',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'verify_peer' => false,
                        'allow_self_signed' => true
                    ],
                ],
            ],

        ],
        /*'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'host' => 'smtp.sendgrid.net', //'email-smtp.us-east-1.amazonaws.com',
                'username' => 'apikey', //'AKIAJQAPORG2VKL5JJAQ',
                'password' => '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', //'AmUWSWqaQmLypudnL58KPqNOXKNhU7YtgiEEmcZx9KEy',
                'port' => '587',
                'encryption' => 'tls',
            ],
 
        ],
        /*
         * ,
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.sendgrid.net',
                'username' => 'apikey',
                'password' => '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', //'AmUWSWqaQmLypudnL58KPqNOXKNhU7YtgiEEmcZx9KEy',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'verify_peer' => false,
                        'allow_self_signed' => true
                    ],
                ],
            ],

        ],
         * */
        /*
         *
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
