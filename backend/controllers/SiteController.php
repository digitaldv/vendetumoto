<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Departamento;
use common\models\Ciudad;
use common\models\Barrio;
use common\models\IndCategoria;
use common\models\PpSesionClase;
use common\models\PpSesionEtapa;
use common\models\Insumo;
use common\models\Empleado;
use common\models\OfertaDeportiva;
use common\models\HGrupo;
use common\models\HCapacitacionCompetencia;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\DesDespacho;
use common\components\Utils;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'getproductoscliente' , 'getempleados', 'getvariablesproducto','politicaprivacidad','terminoscondiciones',
                                      'avisoprivacidad', 'soporte'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionPoliticaprivacidad()
    {
        $this->layout = '../../views/layouts/empty';
        $model = array();
        return $this->render('politica_privacidad',[
            'model' => $model,
        ]);
    }
    public function actionSoporte()
    {
        $this->layout = '../../views/layouts/empty';
        $model = array();
        return $this->render('soporte',[
            'model' => $model,
        ]);
    }
    public function actionTerminoscondiciones()
    {
        $this->layout = '../../views/layouts/empty';
        $model = array();
        return $this->render('terminos_condiciones',[
            'model' => $model,
        ]);
    }
    public function actionAvisoprivacidad()
    {
        $this->layout = '../../views/layouts/empty';
        $model = array();
        return $this->render('aviso_privacidad',[
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {

       $this->layout = '../../views/layouts/main-home';
       $rol = Utils::getRol(Yii::$app->user->identity->id);

       if($rol=="SUPERADMIN" || $rol=="ADMIN_MM" ){

           return $this->render('index',[ ]);
       }
       elseif($rol=="GESTOR") {
           return $this->render('index',[ ]);
       }
       elseif($rol=="CLIE_ADMIN") {

           return $this->render('index',[ ]);
       }
       elseif($rol=="USER_CONSULTOR	") {
           return $this->render('index',[ ]);
       }
       
       
       return $this->render('index');
    }
    
    public function getDatosDashboard($rol){
       $datos=null;


       return $datos;
    }
    

    
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return Yii::$app->getResponse()->redirect(\yii\helpers\Url::to(['/site/index']));       //return $this->goHome();
    }
    

    public function actionGetdespachosproceso() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && $parents[0]!='') {
                $proceso = \common\models\ProcProceso::find()->where('id='.$parents[0])->one();                       //(c.id=82 or c.id=170 or c.id=1106) and
                $registros = Yii::$app->db->createCommand('select d.id,concat("(",c.departamento," - ",c.nombre,") ",d.nombre)as nombreFull FROM des_despacho as d,ciudad as c WHERE c.id=d.id_ciudad order by c.departamento,d.nombre')->queryAll();
                foreach ($registros as $reg) {
                      $out[] = ['id' => $reg['id'], 'name' => $reg['nombreFull']] ;
                }
                echo Json::encode(['output' => $out, 'selected' => "$proceso->id_juzgado"]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    
                          
    public function actionGetempleados() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && $parents[0]!='') {
                $despacho = \common\models\Empleado::find()->all();
                echo Json::encode($despacho->id_juzgado);
                
            }
        }
        echo null;
    }


    public function actionGetproductoscliente() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                if($parents[0]!=''){
                    $registros = \common\models\CliProducto::find()->where('id_cliente='.$parents[0].' AND estado=1')->orderBy('nombre ASC')->all();
                    foreach ($registros as $reg) {
                        $out[] = ['id' => $reg->id,  'name' =>  $reg->nombre];
                    }
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }



    public function actionGetvariablesproducto() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                if($parents[0]!=''){
                    $registros = \common\models\CliProdVariable::find()->where('id_producto='.$parents[0].' AND estado=1')->orderBy('nombre ASC')->all();
                    foreach ($registros as $reg) {
                        $out[] = ['id' => $reg->id,  'name' =>  $reg->nombre];
                    }
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
    

}
