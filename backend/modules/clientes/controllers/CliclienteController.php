<?php

namespace backend\modules\clientes\controllers;

use common\components\Utils;
use webvimark\helpers\LittleBigHelper;
use webvimark\modules\UserManagement\models\User;
use Yii;
use common\models\CliCliente;
use common\models\CliUsuario;
use common\models\ProyProyecto;
use backend\modules\clientes\models\search\CliClienteSearch;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use nullref\datatable\DataTableAction;


/**
 * CliclienteController implements the CRUD actions for CliCliente model.
 */
class CliclienteController extends Controller
{

    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAjax_clientes()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT cli.id, cli.nombre, cli.nit, cli.nombres_contacto, cli.telefono_contacto, cli.email_contacto, cli.celular_contacto, cli.logo,
                cli.fecha, d.name as departamento, c.name as ciudad, concat(replace(replace(cli.estado,'1','ACTIVO'),'0','INACTIVO')) AS estado
                FROM cli_cliente as cli, city as c, state as d
                where cli.id_ciudad=c.id and d.id=c.state_id 
                ORDER BY cli.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
       // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'nombre',      'dt' => 3),
            array('db' => 'nit',      'dt' => 4),
            array('db' => 'nombres_contacto',  'dt' => 5),
            array('db' => 'telefono_contacto',        'dt' => 6),
            array('db' => 'email_contacto',      'dt' => 7),
            array('db' => 'celular_contacto',    'dt' => 8),
            array('db' => 'departamento',     'dt' => 9),
            array('db' => 'ciudad',    'dt' => 10),
            array('db' => 'estado',    'dt' => 11),
            array('db' => 'fecha',    'dt' => 12),
        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        \Yii::$app->response->data  =  $data;
    }

    /**
     * Lists all CliCliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new CliClienteSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CliCliente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CliCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CliCliente();

        if ($model->load(Yii::$app->request->post())) {

            // Utils::dumpx($model->attributes);
            // Utils::dumpx($_POST);

            if($inputfile = UploadedFile::getInstance($model, 'logo')) {
                $file = Yii::$app->security->generateRandomString().".".$inputfile->extension;
                $model->logo = $file;
            } 
            
            $model->carpeta = Utils::urls_amigables2($model->nombre.'-'.$model->nit.'-'.time());

            // Creo las carpetas por default del Cliente
            @mkdir(Yii::getAlias('@webroot/uploads/clientes/'.$model->carpeta.'/'), 0777, true);
            system("chown deploy " . Yii::getAlias('@webroot/uploads/clientes/'.$model->carpeta.'/'));
            exec('chmod -R 0777 '. Yii::getAlias('@webroot/uploads/clientes/'.$model->carpeta.'/'));


            @mkdir(Yii::getAlias('@webroot/uploads/logo/'), 0777, true);
            system("chown deploy " . Yii::getAlias('@webroot/uploads/logo/'));
            exec('chmod -R 0777 '. Yii::getAlias('@webroot/uploads/logo/'));
            
            $model->fecha = date('Y-m-d H:i:s');
            if($model->save()){

                if($model->logo !='' || $model->logo != null) {
                    $inputfile->saveAs(Yii::getAlias('@webroot/uploads/logo/') . $file);
                }
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Creado</b> el <b>Cliente</b> con éxito!',]);
                return $this->redirect(['index']);

            }else  
               Utils::dumpx($model->getErrors());
             
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing CliCliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_logo = $model->logo;
        $old_carpeta = $model->carpeta;
        $old_name = $model->nombre;
        $old_nit = $model->nit;

        if ($model->load(Yii::$app->request->post())) {
             
            //Renombar las carpetas con los campos nombre y nit
            if($old_name != $model->nombre  && $old_nit == $model->nit ){
                // Renames the directory
                $new_carpeta =$model->carpeta = Utils::urls_amigables2($model->nombre.'-'.$old_nit.'-'.time()); 
                if(file_exists(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta)){
                    rename(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta ,Yii::getAlias('@webroot/uploads/clientes/').$new_carpeta ) ;
                }

            }else if ($old_name == $model->nombre  && $old_nit != $model->nit){

                $new_carpeta =$model->carpeta = Utils::urls_amigables2($old_name.'-'.$model->nit.'-'.time()); 
                if(file_exists(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta)){
                    rename(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta ,Yii::getAlias('@webroot/uploads/clientes/').$new_carpeta ) ;
                }

            }else if( $old_name != $model->nombre && $old_nit != $model->nit){

                $new_carpeta =$model->carpeta = Utils::urls_amigables2($model->nombre.'-'.$model->nit.'-'.time()); 
                if(file_exists(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta)){
                    rename(Yii::getAlias('@webroot/uploads/clientes/').$old_carpeta ,Yii::getAlias('@webroot/uploads/clientes/').$new_carpeta ) ;
                }

            }

            if($inputfile = UploadedFile::getInstance($model, 'logo')) {
                $file = Yii::$app->security->generateRandomString().".".$inputfile->extension;
                $model->logo = $file;
                $inputfile->saveAs(Yii::getAlias('@webroot/uploads/logo/').$file);
                if($inputfile){
                    if($model->save()){

                        if(file_exists(Yii::getAlias('@webroot/uploads/logo/').$old_logo)){
                            // Delete image old
                            unlink(Yii::getAlias('@webroot/uploads/logo/').$old_logo);
                        }

                        Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Actualizado</b> el <b>Cliente</b> con exito!',]);
                        return $this->redirect(['index']);
                        
                    }else{
                        \common\components\Utils::dumpx($model->getErrors());
                    }
                }else{
                    Yii::$app->getSession()->setFlash('error', ['message' => 'ERROR: ¡No se pudo crear el archivo en el server!',]);
                }

            }else{
                $model->logo = $old_logo;
                if($model->save()){
                    Yii::$app->getSession()->setFlash('success', ['message' => '¡Se han <b>Actualizado</b> el <b>Cliente</b> con exito!',]);
                    return $this->redirect(['index']);
                }
            } 
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Deletes an existing CliCliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
         // Consultamos primero los datos relacionado al $id
        $cliCliente = CliCliente::find()->where('id='.$id)->one();
        $old_logo = $cliCliente->logo;
        $cliUsario = CliUsuario::find()->where('id_cliente='.$cliCliente->id)->count();
        if($cliUsario >0){
            Yii::$app->getSession()->setFlash('error', ['message' => 'ERROR: ¡NO puede eliminar este cliente!, tiene asociados clientes usuarios', ]);
            return $this->redirect(['index']);

        }else{
            
            if($old_logo != '' || $old_logo != NULL){
                
                if(file_exists(Yii::getAlias('@webroot/uploads/logo/').$old_logo)){
                    // Delete image old
                    unlink(Yii::getAlias('@webroot/uploads/logo/').$old_logo);
                }
            }
            // borrar el registro de $this = Empleado
            $this->findModel($id)->delete();
            // -----------
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Cliente Eliminado con éxito!', ]);
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the CliCliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CliCliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CliCliente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
