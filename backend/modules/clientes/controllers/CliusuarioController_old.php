<?php

namespace backend\modules\clientes\controllers;


use Yii;
use common\models\CliUsuario;
use common\models\VFormatoSensibilizacion;
use common\models\VFormatoFallidaSensibilizacion;
use webvimark\helpers\LittleBigHelper;
use backend\modules\clientes\models\search\CliUsuarioSearch;
use webvimark\modules\UserManagement\models\User;
use yii\base\BaseObject;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Utils;



/**
 * CliusuarioController implements the CRUD actions for CliUsuario model.
 */
class CliusuarioControllerOld extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAjax_usercli()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT cu.id, cli.nombre as cliente, concat(replace(replace(cu.rol,'1','CLI_ADMIN'),'2','USER_CONSULTOR')) AS rol, cu.documento,   
                        cu.nombres, cu.apellidos, cu.email, cu.celular, cu.fecha, concat(replace(replace(cu.estado,'1','ACTIVO'),'0','INACTIVO')) AS estado,
                        u.email as creador,
                        concat(replace(replace(replace(replace(cu.tipo_doc,'1','1 - Cédula'),'2','2 - Tarjeta Identidad'),'3','3 - Cédula Extranjería'),'4','4 - Pasaporte')) AS tipo_doc, 
                        concat(replace(replace(cu.genero,'1','Masculino'),'2','Femenino')) AS genero, cu.saldo_preguntas, 
                        (select concat(nombre) as txt from plan where id=cu.id_plan) as name_plan                 
                FROM cli_usuario as cu, cli_cliente as cli, user as u where cu.id_cliente=cli.id and  u.id=cu.id_creador
                ORDER BY cu.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
       // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'fecha', 'dt' => 3,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'nombres',  'dt' => 4),
            array('db' => 'apellidos',        'dt' => 5),
            array('db' => 'name_plan',    'dt' => 6),
            array('db' => 'saldo_preguntas',    'dt' => 7),
            array('db' => 'tipo_doc',  'dt' => 8),
            array('db' => 'documento',      'dt' => 9),
            array('db' => 'genero',      'dt' => 10),
            array('db' => 'email',      'dt' => 11),
            array('db' => 'celular',    'dt' => 12),
            array('db' => 'estado',    'dt' => 13),
            array('db' => 'creador',    'dt' => 14),
           
        );

        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        \Yii::$app->response->data  =  $data;
    }

    /**
     * Lists all CliUsuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new CliUsuarioSearch();
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexconsulta()
    {
        // $searchModel = new CliUsuarioSearch();
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);

        return $this->render('indexconsulta', [
            // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_consultas()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT cc.id, cc.fecha_pregunta,
                        concat(cu.nombres, ' ' , cu.apellidos,' (',cu.email,')') AS usuario,
                         cc.consulta,  cc.riesgo_si, cc.riesgo_no,
                        concat(replace(replace(cu.estado,'1','ACTIVO'),'0','INACTIVO')) AS estado
                FROM cli_usuario as cu, cli_consulta as cc
                where   cu.id_usuario=cc.id_usuario
                ORDER BY cc.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
        // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(

            array('db' => 'id',    'dt' => 0),
            array('db' => 'fecha_pregunta', 'dt' => 1,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'usuario',  'dt' => 2),

            array('db' => 'consulta',        'dt' => 3),
            array('db' => 'riesgo_si',    'dt' => 4),
            array('db' => 'riesgo_no',    'dt' => 5),
            array('db' => 'riesgo_no', 'dt' => 6, 'formatter' => function( $d, $row ) {
                if ($row[5] > 2){
                    return '<span style="color:blue">RIESGO MEDIO</span> ';
                }else if($row[4] == 4){
                    return '<span style="color:green">RIESGO BAJO</span>';
                } else return '<span style="color:red">RIESGO ALTO</span>';
            }
            ),
            array('db' => 'estado',    'dt' => 7),

        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new CliUsuario();

        if ($model->load(Yii::$app->request->post())) {
           
            // Utils::dumpx($model->attributes); 
            // Utils::dumpx($_POST['CliUsuario']['email'];

            //Creamos primero el User y después el Proveedor
            $modelUser = new User();
            $modelUser->username = $model->email;
            $modelUser->auth_key = Yii::$app->security->generateRandomString();
            $modelUser->password_hash = Yii::$app->security->generatePasswordHash(123);
            $modelUser->email_confirmed = 1;
            $modelUser->status = $model->estado;
            $modelUser->superadmin = 0;
            $modelUser->created_at = time();
            $modelUser->updated_at = time();
            $modelUser->registration_ip = LittleBigHelper::getRealIp();
            $modelUser->email = $model->email;

            if ($modelUser->validate()){

                if($modelUser->save()){

                    if($model->rol == 1){
                        $item_name = 'CLI_ADMIN';
                    }elseif($model->rol == 2){
                        $item_name = 'USER_CONSULTOR';
                    }

                    //se asignan roles de acuerdo al ROL
                    Yii::$app->db->createCommand()
                    ->insert(Yii::$app->db->tablePrefix . 'auth_assignment', [
                        'user_id' => $modelUser->id,
                        'item_name' => $item_name,
                        'created_at' => time(),
                    ])->execute();

                    //Se guarda los datos del Cliusuario
                    $model->id_usuario = $modelUser->id;
                    $model->id_creador = Yii::$app->user->identity->id;
                    $model->fecha = date('Y-m-d H:i:s');

                    if($model->save()){

                        // Yii::$app->getSession()->setFlash('success', ['message' => '¡El <b>Usuario  del Cliente</b> fue Creado con éxito!',]);
                        // return $this->redirect(['index']);

                        //EMAIL al Usuario-Cliente
                        Yii::$app->mailer->compose('layouts/nuevo_empleado',
                        ['usuario' => $model->nombres.' '.$model->apellidos,  'username'=>$model->email, 'rol'=>$model->_rol[$model->rol], ])
                        ->setFrom(['no-responder@azteca.com.co' => 'SIRA - Nuevo Cliente'])
                        ->setTo(trim($model->email))
                        // ->setCc(['randall@digitalventures.com.co' => "SIRA - Nuevo Cliente"])
                        ->setBcc([Yii::$app->params['adminEmail'] => "SIRA - Nuevo Cliente"]) // adminEmail esta en backend/config/params;
                        ->setSubject('¡Nuevo Cliente Asignado! ('.date('Y-m-d h:ia').')')->send();

                        Yii::$app->getSession()->setFlash('success', ['message' => 'El Usuario  del Cliente <b>('.$model->_rol[$model->rol].')</b> fue Creado con éxito y se le ha enviado un email con los Datos de acceso por defecto: <b style="color:yellow"> Usuario:</b> ('.$modelUser->email.') - <b style="color:yellow">Contraseña: </b>123',]);
                        return $this->redirect(['index']);

                    }else{
                        Utils::dump($model->getErrors());
                    }
    
                }else{
                    Utils::dump($modelUser->getErrors());
                }

            }else {
                Yii::$app->getSession()->setFlash('error', ['message' => '<b>ERROR:</b> El (Email) ya existe en SIRA, verifique e intente de nuevo!',]);
                return $this->render('create', [ 'model' => $model]);
            }

        } else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing CliUsuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $id_old_plan = $model->id_plan;
        if ($model->load(Yii::$app->request->post()) ) {

            // Utils::dumpx($model->attributes); 

            //Actualizamos primero el User y después el Proveedor
            $modelUser = User::find()->where('id='.$model->id_usuario)->one();
            $modelUser->username = $model->email;
            $modelUser->email = $model->email;
            $modelUser->status = $model->estado;
            $modelUser->updated_at = time();
  
            if ($modelUser->validate()){

                if($modelUser->save()){

                    if($model->rol == 1){
                        $item_name = 'CLI_ADMIN';
                    }elseif($model->rol == 2){
                        $item_name = 'USER_CONSULTOR';
                    }

                    Yii::$app->db->createCommand()
                    ->update(Yii::$app->db->tablePrefix . 'auth_assignment', [
                            'item_name' => $item_name, 
                            'created_at' => time(), 
                        ],  
                        'user_id=:id', [ 
                        ':id' => $model->id_usuario
                    ])->execute(); 
                     
                    $model->id_editor = Yii::$app->user->identity->id;
                    $model->fecha_edicion = date('Y-m-d H:i:s');
                    if($model->id_plan!=$id_old_plan){
                        if($model->id_plan==1)  $model->saldo_preguntas = $model->saldo_preguntas + 5;
                        if($model->id_plan==2)  $model->saldo_preguntas = $model->saldo_preguntas + 10;
                        if($model->id_plan==3)  $model->saldo_preguntas = $model->saldo_preguntas + 15;
                    }

                    if($model->save()){
                        Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Actualizado</b> los datos del <b>Usuario Cliente</b> exito!',]);
                        return $this->redirect(['index']);

                    }else Utils::dumpx($model->getErrors());

               }else Utils::dumpx($modelUser->getErrors());

            }else {
                Yii::$app->getSession()->setFlash('error', ['message' => '<b>ERROR:</b> El (Email) ya existe en SIRA, verifique e intente de nuevo!',]);
                return $this->render('update', [ 'model' => $model]);
            }

        } else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Deletes an existing CliUsuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // BORRADO LOGICO
        $cliUser = CliUsuario::find()->where('id='.$id)->one();

        $cliUser->estado = 0; //0: INACTIVO
        $cliUser->id_editor = Yii::$app->user->identity->id;
        $cliUser->fecha_edicion = date('Y-m-d H:i:s');
        $cliUser->save();
        Yii::$app->getSession()->setFlash('success', ['message' => 'Usuario <b>INACTIVADO</b> con éxito!',]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the CliUsuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CliUsuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CliUsuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
