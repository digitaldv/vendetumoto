<?php

namespace backend\modules\clientes\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CliCliente;

/**
 * CliClienteSearch represents the model behind the search form about `common\models\CliCliente`.
 */
class CliClienteSearch extends CliCliente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipo', 'estado'], 'integer'],
            [['nombre', 'nit', 'nombres_contacto', 'telefono_contacto', 'email_contacto', 'celular_contacto', 'fecha', 'logo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CliCliente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo' => $this->tipo,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'nit', $this->nit])
            ->andFilterWhere(['like', 'nombres_contacto', $this->nombres_contacto])
            ->andFilterWhere(['like', 'telefono_contacto', $this->telefono_contacto])
            ->andFilterWhere(['like', 'email_contacto', $this->email_contacto])
            ->andFilterWhere(['like', 'celular_contacto', $this->celular_contacto])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
    
    public function searchdirjur($params)
    {
        $query = CliCliente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo' => $this->tipo,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'nit', $this->nit])
            ->andFilterWhere(['like', 'nombres_contacto', $this->nombres_contacto])
            ->andFilterWhere(['like', 'telefono_contacto', $this->telefono_contacto])
            ->andFilterWhere(['like', 'email_contacto', $this->email_contacto])
            ->andFilterWhere(['like', 'celular_contacto', $this->celular_contacto])
            ->andFilterWhere(['like', 'logo', $this->logo]);
        
        $dirjur = \common\models\CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
        //\common\components\Utils::dumpx($dirjur->attributes);
        $query->where('id='.$dirjur->id_cliente);
        
        return $dataProvider;
    }
    
}
