<?php

namespace backend\modules\clientes\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CliUsuario;

/**
 * CliUsuarioSearch represents the model behind the search form of `common\models\CliUsuario`.
 */
class CliUsuarioSearch extends CliUsuario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_cliente', 'id_usuario', 'rol', 'estado'], 'integer'],
            [['telefonos', 'nombre', 'email', 'whatssapp', 'fecha', 'imagen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CliUsuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_cliente' => $this->id_cliente,
            'id_usuario' => $this->id_usuario,
            'rol' => $this->rol,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'telefonos', $this->telefonos])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'whatssapp', $this->whatssapp])
            ->andFilterWhere(['like', 'imagen', $this->imagen]);

        return $dataProvider;
    }
}
