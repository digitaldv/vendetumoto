<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\file\FileInput;
use common\models\City;

/* @var $this yii\web\View */
/* @var $model common\models\CliCliente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cli-cliente-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
           <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
                'data' => $model->_tipo,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-md-3">
           <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
          <?= $form->field($model, 'nit')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
          <?= $form->field($model, 'nombres_contacto')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
           <?= $form->field($model, 'telefono_contacto')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
           <?= $form->field($model, 'email_contacto')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
           <?= $form->field($model, 'celular_contacto')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?=  $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(City::find()->orderBy('state_id')->all(), 'id', 'nombreFull'),
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Ciudad');
            ?>
        </div>  
    </div>

    <div class="row">
        <div class="col-md-3">
             <?= $form->field($model, 'estado')->widget(Select2::classname(), [
                'data' => $model->_estado,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'logo')->label('Logo')->widget(FileInput::className(), [
                    'pluginOptions' => [
                        'showPreview' => false, 'showUpload' => false,
                        'browseClass' => 'btn btn-primary',
                        'removeClass' => 'btn btn-danger',
                        'allowedPreviewTypes' => ['image', 'text'],
                        'allowedFileExtensions' => ['jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG'],
                        'msgInvalidFileExtension' => 'El archivo "{name}"tiene una extensión inválida. Las aceptadas son : "{extensions}".',
                        'browseLabel' =>  'Subir Foto'
                    ],
                ]); 
            ?>
        </div>  
        <?php if ($model->logo != '') { ?>
            <div class="col-md-3" style="">
                <div style="width:50%; margin: 0 auto;">
                    <img style="width:100%" style="" src="<?= Yii::$app->request->baseUrl . '/uploads/logo/' . $model->logo ?>" >
                </div>
            </div>
        <?php } ?>
        <div class="col-md-3">
        
        </div>  
        <div class="col-md-3">
        
        </div> 

    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
