<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CliCliente */

$this->title = 'Nuevo Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

