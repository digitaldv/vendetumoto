<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\clientes\models\search\CliClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(' 
jQuery(document).ready(function () {
 
      
      $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
     
    
      $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i>1){
            $(this).html("<input type=\'text\' style=\'width:100%\' placeholder=\'...\' />");
            $("input", this ).on("keyup change", function (e) {
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && (this.value.length >= 2 || e.keyCode == 13) ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }
            } );
        }
  
         
    } );
    
    var span = $("#datatables_w0 thead tr:eq(1)");
    span.find("th").wrapInner("<td />").contents().unwrap();
    span.find("tbody").contents().unwrap();
   

});

' ); //,View::POS_END

?>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <span class="pull-left" >
                <?= Html::a('<i class="icon fa fa-file-excel-o"></i> Exportar', ['exportempleados'], ['title' => 'Exporta a excel los registros completos', 'target' => '_blank', 'style' => ' padding:5px;min-width:0px;height:45px;color:#b51218eb !important', 'class' => 'btn btn-app text-info']) ?>
            </span>
            <p class="pull-right">
                <?= Html::a('Nuevo Cliente', ['create'], ['class' => 'btn btn-success']) ?>

            </p>

            <?= DataTables::widget([
                    'dataProvider' => $dataProvider,
                    'clientOptions' => [
                        "lengthMenu" => [[20, -1], [20, Yii::t('app', "All")]],
                        "processing" => true,
                        "serverSide" => true,
                        "order" => [0, "desc"],
                        "language" => [
                            "search" => "Buscar : _INPUT_",
                            "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                            "zeroRecords"=> "No se encontó registros - Sorry",
                            "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                            "infoEmpty"=> "Sin resultados",
                            "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                        ],
                        // "colReorder" => true,
                        //  "scrollX" => true,
                        "buttons" => [
                            ["name"=> 'excel', "extend"=> 'excel',
                            "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                            "filename" => "title", "sheetName" => 'Results', "title"=> null
                            ],

                        ],
                        "ajax" => \yii\helpers\Url::to(['clicliente/ajax_clientes']),
                        "columnDefs" => [
                            
                            ["targets" => [0], 'searching' => false, 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                                                                        return "<a class=\"btn btn-outline btn-success btn-xs\" href=\"index.php?r=clientes/clicliente/update&id="+data+"\" title=\"Editar\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                                                                    }')],
                            ["targets" => [1], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                                                                        return "<a class=\"btn btn-outline btn-danger btn-xs\" title=\"Eliminar\" href=\"index.php?r=clientes/clicliente/delete&id="+data+"\" data-confirm=\"¿Está seguro de eliminar el registro?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
                                                                    }')],
                            ["targets" => [2], 'width' => '3%', 'className' => 'text-center'],                                        
                            ["targets" => [5,6], 'width' => '9%', 'className' => 'text-left'],
                            

                        ],
                    ],
                    'columns' => [
                        ['attribute' => 'id', 'label' => '',], //0
                        ['attribute' => 'id', 'label' => '',], //1
                        ['attribute' => 'id', 'label' => 'ID',], //2
                        ['attribute' => 'nombre', 'label' => 'Nombre',],//3
                        ['attribute' => 'nit', 'label' => 'Nit',],//4
                        ['attribute' => 'nombres_contacto', 'label' => 'Nombres contacto',],//5
                        ['attribute' => 'telefono_contacto', 'label' => 'Telefono contacto'],//6
                        ['attribute' => 'email_contacto', 'label' => 'Email _contacto'],//7
                        ['attribute' => 'celular_contacto', 'label' => 'Celular contacto',],//8
                        ['attribute' => 'departamento', 'label' => 'Departamento',],//9
                        ['attribute' => 'ciudad', 'label' => 'Ciudad',],//10
                        ['attribute' => 'estado', 'label' => 'Estado',],//11
                        ['attribute' => 'fecha', 'label' => 'Creado',],//11
                    ],
                ]);
            ?>
        </div>
    </div>

