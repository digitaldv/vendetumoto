<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\file\FileInput;
use common\models\CliCliente;

/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cli-usuario-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_cliente')->hiddenInput(['value' => 1])->label(false) ?>
    <?= $form->field($model, 'rol')->hiddenInput(['value' => 2])->label(false) ?>
     <div class="row">



         <div class="col-md-3">
             <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
         </div>
         <div class="col-md-3">
             <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
         </div>
         <div class="col-md-3">
             <?= $form->field($model, 'genero')->widget(Select2::classname(), [
                 'data' => $model->_genero,
                 'options' => ['placeholder' => '- seleccione -'],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ]);?>
         </div>

         <div class="col-md-3">
             <?= $form->field($model, 'estado')->widget(Select2::classname(), [
                 'data' => $model->_estado,
                 'options' => ['placeholder' => '- seleccione -'],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ]);?>
         </div>
     </div>

     <div class="row">
         <div class="col-md-4">
             <?php

             echo $form->field($model, 'id_plan')->widget(Select2::classname(), [
                 'data' =>  ArrayHelper::map(Yii::$app->db->createCommand("SELECT id, concat(UPPER(nombre),' $',precio) as txt FROM `plan` WHERE estado=1 ORDER BY nombre")->queryAll(), 'id', 'txt'),
                 'options' => ['placeholder' => '- seleccione -'],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ])->label('<b style="color: green">Plan Actual :</b> ');
             ?>
         </div>
         <div class="col-md-4">
             <?=  $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
                 'data' =>  ArrayHelper::map(\common\models\City::find()->orderBy('state_id')->all(), 'id', 'nombreFull'),
                 'options' => ['placeholder' => '- seleccione -'],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ])->label('Ciudad');
             ?>
         </div>

         <div class="col-md-2">
             <?= $form->field($model, 'tipo_doc')->widget(Select2::classname(), [
                 'data' => $model->_tipo_doc,
                 'options' => ['placeholder' => '- seleccione -'],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ]);?>
         </div>
         <div class="col-md-2">
             <?= $form->field($model, 'documento')->textInput(['maxlength' => true])->label('Doc_Identidad') ?>
         </div>


     </div>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <div class="form-group field-cliusuario-saldo_preguntas required has-success" style="font-size: 18px;text-align: center">
                <label class="control-label" for="cliusuario-saldo_preguntas">Saldo Preguntas</label>
                <br>
                <b style="color: blue"><?= $model->saldo_preguntas ?></b>

            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?= $form->field($model, 'id')->textInput() ?>

<?= $form->field($model, 'id_cliente')->textInput() ?>

<?= $form->field($model, 'id_usuario')->textInput() ?>

<?= $form->field($model, 'rol')->textInput() ?>

<?= $form->field($model, 'telefonos')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'whatssapp')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'fecha')->textInput() ?>

<?= $form->field($model, 'estado')->textInput() ?>

<?= $form->field($model, 'imagen')->textInput(['maxlength' => true]) ?>
