<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );


$this->title = 'Usuarios Registrados';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(' 
jQuery(document).ready(function () {
  ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
   

});

' ); //,View::POS_END

?>
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <?= DataTables::widget([
                    'dataProvider' => $dataProvider,
                    //"className" => 'xxxx',
                    'clientOptions' => [
                      //  "class" => 'xxxx',
                        "lengthMenu" => [[20], [20]],
                        "processing" => true,
                        "serverSide" => true,
                        "order" => [2, "desc"],
                        "language" => [
                            "search" => "Buscar : _INPUT_",
                            "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                            "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                            "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                            "infoEmpty"=> "Sin resultados",
                            "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                        ],
                        // "colReorder" => true,
                        //  "scrollX" => true,
                        "buttons" => [
                            ["name"=> 'excel', "extend"=> 'excel',
                            "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                            "filename" => "title", "sheetName" => 'Results', "title"=> null
                            ],

                        ],
                        "ajax" => \yii\helpers\Url::to(['cliusuario/ajax_usercli']),
                        "columnDefs" => [

                            ["targets" => [1], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
 
                                                    if(row[2]=="ACTIVO")
                                                         return "<a class=\"btn btn-outline btn-danger text-red btn-xs\" title=\"BLOQUEAR\" href=\"index.php?r=clientes/cliusuario/delete&id="+data+"\" data-confirm=\"¿Está seguro de BLOQUEAR el usuario?\" data-method=\"post\"><span class=\"glyphicon glyphicon-ban-circle\"></span> Bloquear</a>";
                                                      else
                                                         return "<a class=\"btn btn-outline btn-success text-green  btn-xs\" title=\"ACTIVAR\" href=\"index.php?r=clientes/cliusuario/activar&id="+data+"\" data-confirm=\"¿Está seguro de DES-BLOQUEAR (ACTIVAR) al usuario?\" data-method=\"post\"><span class=\"glyphicon glyphicon-check\"></span> Desbloquear</a>";
                                                    }')],
                            ["targets" => [2], 'width' => '100px', 'className' => 'text-center',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
 
                                                    if(row[2]=="ACTIVO")
                                                         return "<b style=\"color: #1c84c6\" >ACTIVO</b>";
                                                         else
                                                         return "<b style=\"color: red\" >BLOQUEADO (INACTIVO)</b>";
                                                      
                                                    }')],
                            ["targets" => [3,7], 'width' => '7%', 'className' => 'text-center'],
                            ["targets" => [0], 'width' => '3%', 'className' => 'text-center'],





                            ["targets" => [8], 'width' => '100px', 'className' => 'text-center'],

                        ],
                    ],
                    'columns' => [
                        ['attribute' => 'id', 'label' => 'ID',], //1
                        ['attribute' => 'id', 'label' => '',], //0
                        ['attribute' => 'estado', 'label' => 'Estado',],//2
                        ['attribute' => 'imagen', 'label' => 'Avatar',],//7
                        ['attribute' => 'nombre', 'label' => 'Usuario',],//3
                        ['attribute' => 'email', 'label' => 'Email '],//4
                        ['attribute' => 'whatssapp', 'label' => 'WhatsApp',],//5
                        ['attribute' => 'telefonos', 'label' => 'Teléfonos',],//6
                        ['attribute' => 'fecha', 'label' => 'Creado',],//8

                    ],
                ]);
            ?>
        </div>
    </div>

