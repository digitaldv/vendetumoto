<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\clientes\models\search\CliUsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de Consultas';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(' 
jQuery(document).ready(function () {
  ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
       
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
   

});

' ); //,View::POS_END

?>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <!--span class="pull-left" >
                <?php //echo Html::a('<i class="icon fa fa-file-excel-o"></i> Exportar', ['exportempleados'], ['title' => 'Exporta a excel los registros completos', 'target' => '_blank', 'style' => ' padding:5px;min-width:0px;height:45px;color:#b51218eb !important', 'class' => 'btn btn-app text-info']) ?>
            </span-->
            <!--p-- class="pull-right">
                <?php //echo Html::a('Nuevo Usuario Cliente', ['create'], ['class' => 'btn btn-success']) ?>

            </p-->

            <?= DataTables::widget([
                    'dataProvider' => $dataProvider,
                    'clientOptions' => [
                        "lengthMenu" => [[20, -1], [20, Yii::t('app', "All")]],
                        "processing" => true,
                        "serverSide" => true,
                        "order" => [0, "desc"],
                        "language" => [
                            "search" => "Buscar : _INPUT_",
                            "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                            "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                            "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                            "infoEmpty"=> "Sin resultados",
                            "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                        ],
                        // "colReorder" => true,
                        //  "scrollX" => true,
                        "buttons" => [
                            ["name"=> 'excel', "extend"=> 'excel',
                            "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                            "filename" => "title", "sheetName" => 'Results', "title"=> null
                            ],

                        ],
                        "ajax" => \yii\helpers\Url::to(['cliusuario/ajax_consultas']),
                        "columnDefs" => [
                            

                            ["targets" => [0], 'width' => '3%', 'className' => 'text-center'],
                            ["targets" => [1], 'width' => '80px', 'className' => 'text-center'],
                            ["targets" => [4,5,6,7 ], 'width' => '80px', 'className' => 'text-center'],

                        ],
                    ],
                    'columns' => [

                        ['attribute' => 'id', 'label' => 'ID',], //2
                        ['attribute' => 'fecha_pregunta', 'label' => 'Creada',],//3
                        ['attribute' => 'usuario', 'label' => 'Usuario',],//4
                        ['attribute' => 'consulta', 'label' => 'Consulta realizada',],//5
                        ['attribute' => 'riesgo_si', 'label' => 'Cuestionario (SI)'],//6
                        ['attribute' => 'riesgo_no', 'label' => 'Cuestionario (NO)'],//7
                        ['attribute' => 'nivel_riesgo', 'label' => 'NIVEL_RIESGO'],//7
                        ['attribute' => 'estado', 'label' => 'Estado',],//8

                    ],
                ]);
            ?>
        </div>
    </div>

