<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Editar Usuario: ' . $model->nombres .' ' .$model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

