<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\AdsPublicidad;
use Yii;
use common\models\AdsClientes;
use backend\modules\configuracion\models\search\AdsClientesSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdsclientesController implements the CRUD actions for AdsClientes model.
 */
class AdsclientesController extends Controller
{

    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {

        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT  a.id, a.nombre_contacto, a.telefonos, a.celular,
                         concat(replace(replace(a.tipo_persona,'1','NATURAL'),'2','JURÍDICA')) AS tipo_persona,
                         a.fecha, a.empresa, concat(c.name,' - ',dep.name,' - ',co.name) as ciudad, a.cargo            
                   FROM ads_clientes as a  , city as c, state as dep, country as co
                   WHERE c.id=a.id_ciudad and c.state_id=dep.id and dep.country_id=co.id                
                   ORDER BY a.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
        // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'fecha', 'dt' => 3,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'tipo_persona',    'dt' => 4),
            array('db' => 'ciudad',    'dt' => 5),
            array('db' => 'nombre_contacto',      'dt' => 6),
            array('db' => 'cargo',    'dt' => 7),
            array('db' => 'empresa', 'dt' => 'DT_RowId',  'dt' => 8, 'formatter' => function ($d, $row) {
                if($d!='') return $d;
                else return  '<b style="color: red">N/A</b>' ;
            }),
            array('db' => 'telefonos',      'dt' => 9),
            array('db' => 'celular',      'dt' => 10),

        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }



    public function actionCreate()
    {
        $model = new AdsClientes();

        if ($model->load(Yii::$app->request->post())) {
            $model->fecha = date('Y-m-d H:i:s');

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>creado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>actualizado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $pubs = AdsPublicidad::find()->where('id_cliente_ads=' . $id)->count();

        if ($pubs == 0){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro eliminado con éxito!', ]);
        }else{
            Yii::$app->getSession()->setFlash('error', ['message' => '¡ERROR! - No se puede eliminar el cliente, tiene publicacidades asociadas.', ]);
        }
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = AdsClientes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
