<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\ArtArticulo;
use Yii;
use common\models\ArtCategoria;
use backend\modules\configuracion\models\search\ArtCategoriaSearch;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * ArtcategoriaController implements the CRUD actions for ArtCategoria model.
 */
class ArtcategoriaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {   date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT  id, nombre, descripcion,
                          concat(replace(replace(estado,'1','ACTIVO'),'0','INACTIVO')) AS estado 
            FROM art_categoria
            ORDER BY id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
        // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'nombre',      'dt' => 3),
            array('db' => 'descripcion',      'dt' => 4),
            array('db' => 'estado',    'dt' => 5),

        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }


    public function actionCreate()
    {
        $model = new ArtCategoria();


        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Creado</b> el <b>Categoría del artículo</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ArtCategoria model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ArtCategoria model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        //Si el Plan, tiene usuarios asociados: BORRAR LÓGICO - INACTIVA

        $total_articulos = ArtArticulo::find()->where('id_categoria='.$id)->count();

        if($total_articulos==0){
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Categoría Eliminada con éxito - Sin artículos relacionados!', ]);
            $this->findModel($id)->delete();
        }else{
            $model = ArtCategoria::findOne($id);
            $model->estado=0; // Borrado lógico
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Categoría INACTIVADA con éxito! - Tiene artículos relacionados, no se verán reflejados en el APP.', ]);
        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the ArtCategoria model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArtCategoria the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArtCategoria::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
