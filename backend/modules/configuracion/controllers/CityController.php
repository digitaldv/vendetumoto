<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\CliCliente;
use common\models\Empleado;
use common\models\PubPublicacion;
use common\models\State;
use Yii;
use common\models\City;
use backend\modules\configuracion\models\search\CitySearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        //echo $rol; exit;
        $where2 = '';
        $sql = "(SELECT c.id, p.name as pais, d.name as departamento,c.name as ciudad, c.code
                   FROM country as p, state as d , city as c
                   Where c.state_id=d.id and d.country_id=p.id
                  ORDER BY d.name ASC, c.name ASC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'ciudad',      'dt' => 3),
            array('db' => 'departamento',      'dt' => 4),
            array('db' => 'pais',      'dt' => 5),
            array('db' => 'code',      'dt' => 6),


        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    public function actionCreate()
    {
        $model = new City();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_date = date('Y-m-d H:i:s');

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>creado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>actualizado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $empleados = Empleado::find()->where('id_ciudad=' . $id)->count();
        $publicaciones = PubPublicacion::find()->where('id_ciudad=' . $id)->count();
        $clientes = CliCliente::find()->where('id_ciudad=' . $id)->count();

        if ($empleados == 0 && $publicaciones == 0 && $clientes==0){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro eliminado con éxito!', ]);
        }else{
            Yii::$app->getSession()->setFlash('error', ['message' => '¡ERROR! - No se puede eliminar la Ciudad, tiene empleados, clientes o publicaciones asociadas.', ]);
        }
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
