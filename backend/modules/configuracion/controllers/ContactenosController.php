<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use Yii;
use common\models\Contactenos;
use backend\modules\configuracion\models\search\ContactenosSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactenosController implements the CRUD actions for Contactenos model.
 */
class ContactenosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        //1: Me interesa publicitar mi negocio en Vendetumoto.co, 2: Pregunta, 3: Comentario, 4: Soporte, 5: Otros
        $sql = "(SELECT d.id, d.fecha, d.email,  d.nombre, d.mensaje, 
                        concat(replace(replace(replace(replace(replace(d.asunto,'1','Me interesa publicitar mi negocio en Vendetumoto.co'),'2','Pregunta'),'3','Comentario'),'4','Soporte'),'5','Otros')) AS asunto
                FROM contactenos as d                 
               ORDER BY d.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'fecha', 'dt' => 1,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'asunto',  'dt' => 2),
            array('db' => 'nombre',  'dt' => 3),
            array('db' => 'mensaje',  'dt' => 4),
            array('db' => 'email',  'dt' => 5),

        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    protected function findModel($id)
    {
        if (($model = Contactenos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
