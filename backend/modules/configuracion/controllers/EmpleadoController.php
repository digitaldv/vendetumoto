<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use webvimark\helpers\LittleBigHelper;
use webvimark\modules\UserManagement\models\User;
use Yii;
use common\models\Empleado;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;




/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {   date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function actionAjax_empleados()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT e.id,    
                   concat(replace(replace(e.rol,'1','ADMIN_VTM'),'2','CLI_USUARIO')) AS rol, e.documento,   
                   e.nombres,e.apellidos,e.email,e.fecha_nacimiento,d.name as departamento, c.name as ciudad, e.direccion,e.telefono,e.celular,
                   concat(replace(replace(e.estado,'1','ACTIVO'),'0','INACTIVO')) AS estado, e.created_date as fecha_creacion, u.email as creador   
            FROM empleado as e, city as c,state as d, user as u
            where e.id_ciudad=c.id and d.id=c.state_id and u.id=e.id_creador
            ORDER BY e.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;
       // $table = 'empleado';
        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'rol',      'dt' => 3),
            array('db' => 'documento',      'dt' => 4),
            array('db' => 'nombres',  'dt' => 5),
            array('db' => 'apellidos',        'dt' => 6),
            array('db' => 'email',      'dt' => 7),
            array('db' => 'fecha_nacimiento',    'dt' => 8),
            array('db' => 'departamento',     'dt' => 9),
            array('db' => 'ciudad',    'dt' => 10),
            array('db' => 'direccion',    'dt' => 11),
            array('db' => 'telefono',    'dt' => 12),
            array('db' => 'celular',    'dt' => 13),
            array('db' => 'estado',    'dt' => 14),
            array('db' => 'fecha_creacion',    'dt' => 15),
            array('db' => 'creador',    'dt' => 16),
        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
         $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
         \Yii::$app->response->data  =  $data;
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionCreate()
    {
        $model = new Empleado();

        $rol = Utils::getRol(Yii::$app->user->identity->id);
        if($rol=="ADMIN_VTM")
            $model->_rol = ['1' => 'ADMIN_VTM'/*, '2' => 'COMERCIAL_HB'*/];


        if ($model->load(Yii::$app->request->post())) {
            //Creamos primero el User y después el Proveedor
            $modeluSER = new User();
            $modeluSER->username = $_POST['Empleado']['email'];
            $modeluSER->auth_key = Yii::$app->security->generateRandomString();
            $modeluSER->password_hash = Yii::$app->security->generatePasswordHash(123);
            $modeluSER->email_confirmed = 1;
            $modeluSER->status = 1;
            $modeluSER->superadmin = 0;
            $modeluSER->created_at = time();
            $modeluSER->updated_at = time();
            $modeluSER->registration_ip = LittleBigHelper::getRealIp();
            $modeluSER->email = $_POST['Empleado']['email'];
            if ($modeluSER->validate())
            {
                if($modeluSER->save()){
                    //se asignan roles de acuerdo al ROL
                    Yii::$app->db->createCommand()
                        ->insert(Yii::$app->db->tablePrefix . 'auth_assignment', [
                            'user_id' => $modeluSER->id,
                            'item_name' => Utils::getRolVTM($model->rol),
                            'created_at' => time(),
                        ])->execute();

                    //Se guarda los datos del Empleado
                    $model->created_date = date('Y-m-d H:i:s');
                    $model->id_usuario = $modeluSER->id;
                    $model->id_creador = Yii::$app->user->identity->id;

                    if($model->save()){
                        $creador = User::find()->where('id='.Yii::$app->user->identity->id)->one();
                        //EMAIL al Usuario-Cliente
                        Yii::$app->mailer->compose('layouts/nuevo_empleado',
                        ['usuario' => $model->nombres.' '.$model->apellidos,  'username'=>$model->email, 'rol'=>$model->_rol[$model->rol], ])
                        ->setFrom(['no-responder@vendetumoto.co' => 'VTM - Nuevo Empleado VTM'])
                        ->setTo(trim($model->email))
                        ->setCc(['wokeer@gmail.com'/*,'randallsmp@gmail.com'*/])
                        //->setBcc([Yii::$app->params['adminEmail'] => "VTM - Nuevo Empleado", Yii::$app->params['adminEmail2'] => "SIRA - Nuevo Empleado",  Yii::$app->params['adminEmail3'] => "SIRA - Nuevo Empleado"]) // adminEmail esta en backend/config/params;
                        ->setSubject('Nuevo Empleado de VTM registrado ('.date('Y-m-d h:ia').')')->send();

                        Yii::$app->getSession()->setFlash('success', ['message' => 'El Empleado <b>('.$model->_rol[$model->rol].')</b> fue Creado con éxito y se le ha enviado un email con los Datos de acceso por defecto: <b style="color:yellow"> Usuario:</b> ('.$modeluSER->email.') - <b style="color:yellow">Contraseña: </b>123',]);
                        return $this->redirect(['index']);
                    }else{
                        Utils::dump($model->getErrors());
                    }


                }
            }else {
                Yii::$app->getSession()->setFlash('error', ['message' => '<b>ERROR:</b> El (Email) ya existe en la base de datos de VTM, verifique e intente de nuevo!',]);
                //Utils::dumpx($modeluSER->getErrors());
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            // Utils::dumpx($_POST);

            $user = \common\models\User::find()->where('id='.$model->id_usuario)->one();
            $user->username = $model->email;
            $user->email = $model->email;
            $user->status = $model->estado;
          
            
            if($user->save()){

                if($model->rol == 1){
                    $item_name = 'ADMIN_VTM';
                }elseif($model->rol == 2){
                    $item_name = 'CONSULTOR_VTM';
                }
                
                Yii::$app->db->createCommand()->update(Yii::$app->db->tablePrefix . 'auth_assignment', ['item_name' => $item_name, 'created_at' => time(), ],  'user_id=:id', [ 
                    ':id' => $model->id_usuario
                ])->execute();  

                if($model->save()){
                    Yii::$app->session->setFlash('success', '¡El Empleado se ha actualizado con éxito!');
                    return $this->redirect(['index']);
                }else Utils::dumpx($model->getErrors());

            }else Utils::dumpx($user->getErrors());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $empleado = Empleado::find()->where('id='.$id)->one();
        if(Yii::$app->user->identity->id == $empleado->id_usuario){
            Yii::$app->getSession()->setFlash('error', ['message' => '¡No puede eliminarse usted Mismo! Es un usuario Administrador.', ]);
            return $this->redirect(['index']);
        }else{
            $empleado->estado=0;
            if($empleado->save()){
                $user = User::find()->where('id='.$empleado->id_usuario)->one();
                $user->status=0;
                $user->save();
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Empleado Inactivado con éxito!', ]);
                return $this->redirect(['index']);
            }

            Yii::$app->getSession()->setFlash('error', ['message' => '¡ERROR: NO se pudo INACTIVAR el empleado!', ]);
            return $this->redirect(['index']);

/*
            // borrar el registro de $this = Empleado
            $this->findModel($id)->delete();
            // borrar el registro de la tabla user
            User::deleteAll('id='.$empleado->id_usuario);
            // borrar el registro de la tabla auth_assignment
            Yii::$app->db->createCommand() ->delete(Yii::$app->db->tablePrefix . 'auth_assignment', [ 'user_id' => $empleado->id_usuario])->execute();
*/

        }


        
    }

    /**
     * Finds the Empleado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empleado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empleado::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
