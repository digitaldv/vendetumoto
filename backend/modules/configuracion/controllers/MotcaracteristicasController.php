<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\MotTipo;
use common\models\PubCaracteristica;
use common\models\PubPublicacion;
use Yii;
use common\models\MotCaracteristicas;
use backend\modules\configuracion\models\search\MotCaracteristicasSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MotcaracteristicasController implements the CRUD actions for MotCaracteristicas model.
 */
class MotcaracteristicasController extends Controller
{

    public function behaviors()
    {   date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);   //echo $rol; exit;
        $where2 = '';
        $sql = "(SELECT  id, nombre, 
                         concat(replace(replace(estado,'1','1 - ACTIVO'),'0','0 - INACTIVO')) AS estado
                   FROM mot_caracteristicas
                  ORDER BY id ASC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'nombre',      'dt' => 3),
            array('db' => 'estado',      'dt' => 4),
        );

        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    public function actionCreate()
    {
        $model = new MotCaracteristicas();

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>creado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>actualizado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $pubs = PubCaracteristica::find()->where('id_caracteristica=' . $id)->count();
        if ($pubs == 0){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro eliminado con éxito!', ]);
        }else{
            Yii::$app->getSession()->setFlash('error', ['message' => '¡ERROR! - No se puede eliminar la Característica. Tiene Publicaciones asociadas.', ]);
        }
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = MotCaracteristicas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
