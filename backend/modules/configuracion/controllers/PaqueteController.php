<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\ImpErrores;
use common\models\MotTipo;
use common\models\Payments;
use common\models\PubPublicacion;
use Yii;
use common\models\Paquete;
use backend\modules\configuracion\models\search\PaqueteSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaqueteController implements the CRUD actions for Paquete model.
 */
class PaqueteController extends Controller
{

    public function behaviors()
    {   date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);   //echo $rol; exit;
        $where2 = '';
        $sql = "(SELECT  id, nombre, descripcion, precio, fecha_creacion as fecha, total_fotos, total_anuncios, total_videos, dias_publicados,
                         concat(replace(replace(estado,'1','1 - ACTIVO'),'0','0 - INACTIVO')) AS estado
                   FROM paquete 
                  ORDER BY id ASC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'nombre',      'dt' => 3),
            array('db' => 'descripcion', 'dt' => 'DT_RowId',  'dt' => 4, 'formatter' => function ($d, $row) {
                $str='';
                $vec = explode(',',$d);
                for($i=0;$i<count($vec);$i++) {
                    $str .= '<b> - </b>'.$vec[$i].'<br>';
                }
                return  $str ;
            }),

            array('db' => 'precio', 'dt' => 'DT_RowId',  'dt' => 5, 'formatter' => function ($d, $row) {
                return  '<b style="color: blue">$'.number_format($d,0,',','.').'</b>' ;
            }),
            array('db' => 'total_anuncios',      'dt' => 6),
            array('db' => 'total_fotos',      'dt' => 7),
            array('db' => 'total_videos',      'dt' => 8),
            array('db' => 'dias_publicados',      'dt' => 9),
            array('db' => 'estado',      'dt' => 10),

            array('db' => 'fecha', 'dt' => 'DT_RowId', 'dt' => 11, 'formatter' => function ($d, $row) {
                if ($d != '') return '<span>'.date('Y-m-d', strtotime($d)).'<br> ('.date('h:i a', strtotime($d)).')'.'</span>'; else return null;
            }),

        );

        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    public function actionCreate()
    {
        $model = new Paquete();

        if ($model->load(Yii::$app->request->post())) {
            $model->fecha_creacion = date('Y-m-d H:i:s');
            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>creado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>actualizado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $paquetes_comprados = Payments::find()->where('id_paquete=' . $id)->count();
        if ($paquetes_comprados == 0){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro ELIMINADO con éxito!', ]);
        }else{
            $model = $this->findModel($id);
            $model->estado = 0;
            $model->save();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Paquete INACTIVADO con éxito!.', ]);
        }
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Paquete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
