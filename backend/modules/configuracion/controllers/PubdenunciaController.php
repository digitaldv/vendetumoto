<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use Yii;
use common\models\PubDenuncia;
use backend\modules\configuracion\models\search\PubDenunciaSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PubdenunciaController implements the CRUD actions for PubDenuncia model.
 */
class PubdenunciaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        //1: Robo, 2: Falsa informacion, 3: Moto robada, 4: Otros
        $sql = "(SELECT d.id, d.fecha, u.username as usuario, d.id_publicacion, d.mensaje,
                        concat(replace(replace(replace(replace(d.motivo,'1','Robo'),'2','Falsa información'),'3','Moto robada'),'4','Otros')) AS motivo
                FROM pub_denuncia as d, user as u
                WHERE u.id=d.id_usuario
               ORDER BY d.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'fecha', 'dt' => 1,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'id_publicacion', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    return '<a target="_blank" href="https://vendetumoto.co/site/pubdetallepublico?id='.$row[6].'">ver publicación</a>';   }
            ),

            array('db' => 'motivo',  'dt' => 3),
            array('db' => 'mensaje',  'dt' => 4),
            array('db' => 'usuario',  'dt' => 5),
            array('db' => 'id_publicacion',  'dt' => 6),


        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    protected function findModel($id)
    {
        if (($model = PubDenuncia::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
