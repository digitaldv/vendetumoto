<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use Yii;
use common\models\PubMensaje;
use backend\modules\configuracion\models\search\PubMensajeSearch;
use yii\data\SqlDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PubmensajeController implements the CRUD actions for PubMensaje model.
 */
class PubmensajeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT m.id,m.fecha, m.mensaje,  concat(cu.nombre,' (',cu.email,')') as creador,
                     concat(replace(replace(m.leido,'1','SI'),'0','NO')) AS leido,
                     m.fecha_leido, m.id_publicacion
                FROM pub_mensaje as m, cli_usuario as cu, pub_publicacion as p
               WHERE m.id_creador=cu.id_usuario and p.id=m.id_publicacion
               ORDER BY cu.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'fecha', 'dt' => 1,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'id', 'dt' => 2,
                'formatter' => function( $d, $row ) {
                    //if ($d != 'ACTIVO') {
                    return  '<a target="_blank" href="https://vendetumoto.co/site/pubdetallepublico?id='.$row[7].'">ver publicación</a>';
                }
            ),
            array('db' => 'mensaje',  'dt' => 3),
            array('db' => 'creador',  'dt' => 4),
            array('db' => 'leido',        'dt' => 5),
            array('db' => 'fecha_leido', 'dt' => 6,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'id_publicacion',  'dt' => 7),

        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }
    protected function findModel($id)
    {
        if (($model = PubMensaje::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
