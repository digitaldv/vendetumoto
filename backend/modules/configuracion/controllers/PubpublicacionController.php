<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\CliUsuario;
use common\models\Paquete;
use common\models\PubMultimedia;
use common\models\PubRechazo;
use common\models\RequestEvidences;
use common\models\RequestHistory;
use common\models\Requests;
use common\models\User;
use Yii;
use common\models\PubPublicacion;
use backend\modules\configuracion\models\search\PubPublicacionSearch;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use ZipArchive;
use common\models\Payments;
/**
 * PubpublicacionController implements the CRUD actions for PubPublicacion model.
 */
class PubpublicacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionDescargarimagenes()
    {
        extract($_GET);
        //Utils::dumpx($_POST);
        //$fotos = PubMultimedia::find()->where('tipo=1 and id_publcacion='.$id_publicacion)->all();

        if (Yii::$app->user->isGuest) {
            return false;
        }

        if (isset($id_publicacion) && $id_publicacion != '') {
            $model = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
            $dir = dirname(__DIR__).'"/../../web/uploads/descargas/';
            if($model){
                $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->all();
                if($fotos){

                    $zip = new ZipArchive();
                    // Ruta absoluta
                    $file = 'fotos_publicacion_'.$id_publicacion."_".time().".zip";
                    $nombreArchivoZip = dirname(__DIR__). "/../../web/uploads/descargas/".$file;
                    // Creamos y abrimos un archivo zip temporal
                    $zip->open($nombreArchivoZip,ZipArchive::CREATE);

                    foreach ($fotos as $foto) {
                        //Leo la foto y la guardo en LOCAL
                        $this->getImage("https://vtmprod.s3.us-east-2.amazonaws.com/pubs/".$foto->publicacion->carpeta.'/'.$foto->archivo, $dir, $foto->archivo, 0);

                        //Agrego cada imagen al archivo comprimido (.ZIP)
                        $zip->addFile($dir.$foto->archivo, $foto->archivo);
                    }


                    // Una vez añadido los archivos deseados cerramos el zip.
                    $zip->close();
                    // Creamos las cabezeras que forzaran la descarga del archivo como archivo zip.
                    header("Content-type: application/octet-stream");
                    header("Content-disposition: attachment; filename=".$file);
                    // leemos el archivo creado
                    readfile($nombreArchivoZip);
                    foreach ($fotos as $foto) {
                        unlink($dir.$foto->archivo); //Destruye el archivo temporal
                    }
                     // Por último eliminamos el archivo temporal creado
                     unlink($nombreArchivoZip);//Destruye el archivo temporal

                }
            }else return false;

        }




    }

    public function actionAjax_index()
    {

        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT p.id, p.fecha_creacion,
                        concat(t.nombre,' / ',ma.nombre,' / ',m.nombre) as vehiculo,
                        ci.name as ciudad,
                        p.descripcion,  p.precio, p.descuento,
                        concat(p.placa,' / ', p.ano) as placa_ano,
                        concat(concat(p.recorrido,' ',replace(replace(replace(p.unid_recorrido,'1','Kms'),'2','Millas'),'3','Horas')),' / ', concat(replace(replace(replace(replace(p.unid_recorrido,'1','Gasolina'),'2','Disel'),'3','Gas'),'4','Eléctrica'))) as recor_comb,
                        concat(replace(replace(replace(replace(replace(replace(replace(replace(replace(p.estado,'1','1 - APROBADO'),'2','2 - PENDIENTE_X_APROBAR'),'3','3 - BLOQUEADO'),'4','4 - VENDIDO'),'5','5 - RECHAZADO'),'0','0 - INACTIVO'),'6','6 - BORRADOR'),'7','7 - PAUSADO'),'8','8 - ELIMINADO')) AS estado,

                        concat(usr.nombre,' (',usr.email,')') as usuario,
                        concat(p.numero_contacto,' ',p.whatsapp) as numero_contacto,
                        concat(replace(replace(p.tipo_vendedor,'1','Directo'),'2','Concesionario')) as tipo_vendedor,
                        p.cilindraje, concat(replace(replace(p.condicion,'1','Nuevo'),'2','Usado')) as condicion, p.slug,
                        if(p.vendido_por IS NULL,'N/A',concat(replace(replace(replace(replace(p.vendido_por,'1','Vendetumoto.co'),'2','WhatsApp'),'3','Email'),'4','Llamada telefónica'))) as vendido_por
                
                  FROM pub_publicacion as p, mot_tipo as t, mot_marca as ma, mot_modelo as m, city as ci, cli_usuario as usr                
                 WHERE t.id=p.id_tipo and m.id=p.id_modelo and ma.id=m.id_marca and ci.id=p.id_ciudad and p.id_usuario=usr.id_usuario                
                 ORDER BY p.id DESC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(

            array('db' => 'id', 'dt' => 0,
                'formatter' => function( $d, $row ) {
                    $pub = PubPublicacion::find()->where('id='.$row[1])->one();

                    $str = "<script>        
                            $('.btn_rechazar".$d."').click(function(e){
                                e.preventDefault(); var modal = $('#modal-delete').modal('show');    var that = $(this);
                                modal.find('.modal-title').html('¿Está Seguro de Rechazar el Anuncio?');                                
                                modal.find('.id_pub').html(that.attr('id_pub'));
                                modal.find('.tmm').html(that.attr('tmm'));
                                modal.find('.descripcion').html(that.attr('descripcion'));                                        
                                $('#id_publicacion').val(that.attr('id_pub'));     
                                //$('#delete-confirm').click(function(e) { e.preventDefault();  window.location = url;  });                                
                            }); 
                           
                            $('.btn_vendido".$d."').click(function(e){
                                e.preventDefault(); var modal = $('#modal-vendido').modal('show');    var that = $(this);
                                modal.find('.modal-title').html('¿Se ha vendido el vehículo del anuncio?');                                
                                modal.find('.id_pub').html(that.attr('id_pub'));
                                modal.find('.tmm').html(that.attr('tmm'));
                                modal.find('.descripcion').html(that.attr('descripcion'));                                        
                                $('#id_publicacion').val(that.attr('id_pub'));     
                               // $('#delete-confirm').click(function(e) { e.preventDefault();  window.location = url;  });                                
                            });  
                                
                            $('.btn_galeria".$d."').click(function(e){
                                e.preventDefault(); 
                                var modal = $('#modal-fotos').modal('show');    
                                var that = $(this);
                                modal.find('.modal-title').html('Galería de fotos del Anuncio: ');                                
                                modal.find('.id_pub').html(that.attr('id_pub'));
                                modal.find('.tmm').html(that.attr('tmm'));
                                                                  
                                $('#id_publicacion').val(that.attr('id_pub'));
                               
                               // alert(that.attr('id_pub'));
                               
                                $.post('index.php?r=configuracion/pubpublicacion/pintargaleriafotosanuncio', {id: that.attr('id_pub') }, 
                                       function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub);
                                            $('.btn_descargar').attr('style','margin-right: 10px;visibility: hidden');
                                            $('#links').html('<center><b style=\"color:red\">Cargando fotos del anuncio, espere un momento....</b><br><img style=\"width:300px\" title=\"Cargando imagenes ...\" src=\"".Yii::$app->request->baseUrl."/img/loading-moto.gif\"></center>');
                                            setTimeout(function() {                                                 
                                                 if(res.res){   
                                                     if(res.total > 0){   
                                                         $('.btn_descargar').attr('style','margin-right: 10px');
                                                         $('#links').html(res.res);
                                                     }else{
                                                        $('#links').html('<center><em style=\"color:red;font-size:20px\">¡El usuario NO ha adjuntado ninguna foto a su publicación!</em></center>');    
                                                     }
                                                }else{
                                                     $('#links').html('<center><em style=\"color:red;font-size:20px\">¡El usuario NO ha adjuntado ninguna foto a su publicación!</em></center>');
                                                }
                                            }, 2000);
                                         
                                            
                                }) ; 
                                
                                //$('#delete-confirm').click(function(e) { e.preventDefault();  window.location = url;  });                                
                            });         
                          </script>";

                    if($pub->estado==1){

                        return $str.'<style>.dropdown-item.active, .dropdown-item:active {color: #fff;text-decoration: none;background-color: #007bff;} .dropdown-item:focus, .dropdown-item:hover {color: #16181b; background-color: #ed55653b;} a:hover { color: #0056b3;    } .dropdown-item {    display: block;    width: 100%;    padding: .25rem 1.5rem;    clear: both;    font-weight: 400;    color: #212529;    text-align: inherit;    white-space: nowrap;    background-color: transparent;    border: 0;} a {    color: #007bff;    text-decoration: none;    background-color: transparent;    -webkit-text-decoration-skip: objects;}</style>
                        <div class="btn-group">
                        
                              <button title="Acción" type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...</button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item btn_galeria'.$d.'" id_pub="'.$d.'"  href="#"><i style="color: #9c27b0;font-weight: bold;font-size: 16px"  class="fa fa-photo" aria-hidden="true"></i> &nbsp; Ver Fotos</a>
                                <a href="index.php?r=configuracion/pubpublicacion/aprobar&id='.$d.'&op=2" data-confirm="¿Está seguro de Volver a PENDIENTE x APROBAR el Anuncio?" data-method="post" class="dropdown-item"><i style="color: #0a6aa1;font-weight: bold;font-size: 16px"  class="fa fa-reply" aria-hidden="true"></i> &nbsp; Volver a Pendiente x Aprobar</a>
                                <a class="dropdown-item btn_rechazar'.$d.'"  id_pub="'.$d.'" tmm="'.$pub->tipo->nombre.' / '.$pub->modelo->marca->nombre.' / '.$pub->modelo->nombre.'" href="#"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-close" aria-hidden="true"></i> &nbsp;&nbsp; Rechazar</a>                                
                                <a class="dropdown-item  btn_vendido'.$d.'" descripcion="'.$pub->descripcion.'" id_pub="'.$d.'" tmm="'.$pub->tipo->nombre.' / '.$pub->modelo->marca->nombre.' / '.$pub->modelo->nombre.'" href="#"><i style="color: blue;font-weight: bold;font-size: 16px"  class="fa fa-thumbs-o-up" aria-hidden="true"></i> &nbsp; Vendido</a>
                                <a class="dropdown-item" href="#"><i style="color: blue;font-weight: bold;font-size: 16px"  class="fa fa-photo" aria-hidden="true"></i> &nbsp; Ver Fotos</a>
                                <a href="index.php?r=configuracion/pubpublicacion/bloquear&id='.$d.'"  data-confirm="¿Está seguro de BLOQUEAR x DENUNCIAS al Anuncio?" data-method="post"  class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-ban" aria-hidden="true"></i> &nbsp;&nbsp; Bloquear x Denuncias</a>
                                <a href="index.php?r=configuracion/pubpublicacion/inactivar&id='.$d.'"  data-confirm="¿Está seguro de INACTIVAR el Anuncio?" data-method="post"  class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-power-off" aria-hidden="true"></i> &nbsp;&nbsp; Inactivar</a>
                              </div>
                            </div>';
                    }else{
                        return $str.'<style>.dropdown-item.active, .dropdown-item:active {color: #fff;text-decoration: none;background-color: #007bff;} .dropdown-item:focus, .dropdown-item:hover {color: #16181b; text-decoration: none; background-color: #ed55653b;} a:hover { color: #0056b3;    text-decoration: underline;} .dropdown-item {    display: block;    width: 100%;    padding: .25rem 1.5rem;    clear: both;    font-weight: 400;    color: #212529;    text-align: inherit;    white-space: nowrap;    background-color: transparent;    border: 0;} a {    color: #007bff;    text-decoration: none;    background-color: transparent;    -webkit-text-decoration-skip: objects;}</style>
                        <div class="btn-group">
                              <button title="Acción" type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...</button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item btn_galeria'.$d.'" id_pub="'.$d.'" href="#"><i style="color: #9c27b0;font-weight: bold;font-size: 16px"  class="fa fa-photo" aria-hidden="true"></i> &nbsp; Ver Fotos</a>
                                <a href="index.php?r=configuracion/pubpublicacion/aprobar&id='.$d.'&op=1" data-confirm="¿Está seguro de APROBAR el Anuncio?" data-method="post" class="dropdown-item"><i style="color: green;font-weight: bold;font-size: 16px"  class="fa fa-check" aria-hidden="true"></i> &nbsp; Aprobar</a>
                                <a class="dropdown-item btn_rechazar'.$d.'" descripcion="'.$pub->descripcion.'" id_pub="'.$d.'" tmm="'.$pub->tipo->nombre.' / '.$pub->modelo->marca->nombre.' / '.$pub->modelo->nombre.'" href="#"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-close" aria-hidden="true"></i> &nbsp;&nbsp; Rechazar</a>                              
                                <a class="dropdown-item  btn_vendido'.$d.'" descripcion="'.$pub->descripcion.'" id_pub="'.$d.'" tmm="'.$pub->tipo->nombre.' / '.$pub->modelo->marca->nombre.' / '.$pub->modelo->nombre.'" href="#"><i style="color: blue;font-weight: bold;font-size: 16px"  class="fa fa-thumbs-o-up" aria-hidden="true"></i> &nbsp; Vendido</a>
                                
                                <a href="index.php?r=configuracion/pubpublicacion/bloquear&id='.$d.'"  data-confirm="¿Está seguro de BLOQUEAR x DENUNCIAS al Anuncio?" data-method="post"  class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-ban" aria-hidden="true"></i> &nbsp;&nbsp; Bloquear x Denuncias</a>
                                <a href="index.php?r=configuracion/pubpublicacion/inactivar&id='.$d.'"  data-confirm="¿Está seguro de INACTIVAR el Anuncio?" data-method="post"  class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px"  class="fa fa-power-off" aria-hidden="true"></i> &nbsp;&nbsp; Inactivar</a>
                              </div>
                            </div>';
                    }

                }
            ),

            array('db' => 'id', 'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return '<a href="#" title="Ver Publicación del Anuncio..." style="font-size: 14px;cursor:pointer;color:blue;text-decoration: underline"  >'.$d.'</span>';
                }
            ),
            array('db' => 'fecha_creacion', 'dt' => 2,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'estado', 'dt' => 3,
                'formatter' => function( $d, $row ) {
                   return $d;
            }
            ),
            array('db' => 'id', 'dt' => 4,
                'formatter' => function( $d, $row ) {
                    $pago_actual = Payments::find()->where('state=1 and id_publicacion='.$d)->one();
                    if($pago_actual){
                        return $pago_actual->paquete->nombre;
                    }else{
                        return '<em style="color: red">-- SIN PLAN --</em>';
                    }
                    return $d;
                }
            ),
            array('db' => 'vehiculo',        'dt' => 5),

            array('db' => 'ciudad',  'dt' => 6),
            array('db' => 'descripcion', 'dt' => 7,
                'formatter' => function( $d, $row ) {
                    return '<span style="cursor:pointer;color:#337ab7" title="'.$d.'">'.substr($d,0,30).'..</span>';
                }
            ),
            array('db' => 'precio', 'dt' => 8,
                'formatter' => function( $d, $row ) {
                    $pub = PubPublicacion::find()->where('id='.$row[1])->one();
                    if($pub->descuento>0){
                        $precio_fin = '<span style="text-decoration:line-through;">$'.number_format($pub->precio,0,',','.').'</span><br><b style="color: blue">$'.number_format(($pub->precio-(($pub->precio*$pub->descuento) / 100)),0,',','.').'</b> <span style="color: red">(-'.$pub->descuento.'%)</span>';
                    }else $precio_fin = $pub->precio;

                return  $precio_fin;
            }
            ),
            array('db' => 'placa_ano',    'dt' => 9),
            array('db' => 'recor_comb',    'dt' => 10),
            array('db' => 'cilindraje', 'dt' => 11,
                'formatter' => function( $d, $row ) {
                    return number_format($d,0,',','.');
                }
            ),
            array('db' => 'condicion',    'dt' => 12),
            array('db' => 'usuario',    'dt' => 13),
            array('db' => 'numero_contacto', 'dt' => 14,
                'formatter' => function( $d, $row ) {
                    $pub = PubPublicacion::find()->where('id='.$row[1])->one();
                    return '<span style="cursor: pointer"  title="Números de contacto"><i style="color: blue;font-weight: bold;font-size: 16px"  class="fa fa-mobile" aria-hidden="true"></i> &nbsp;'.$pub->numero_contacto.'</span><br><span style="cursor: pointer" title="WhatsApp"><i style="color: green;font-weight: bold" class="fa fa-whatsapp" aria-hidden="true"></i> '.$pub->whatsapp.'</span>';
                }
            ),
            array('db' => 'tipo_vendedor',    'dt' => 15),
            array('db' => 'vendido_por',    'dt' => 16),

        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }




    public function actionAjaxvendidoanuncio(){
        extract($_POST);

        //Utils::dumpx($_POST);

        $model = PubPublicacion::find()->where('id='.$id_publicacion)->one();
        $model->fecha_edicion =  date('Y-m-d H:i:s');
        $model->vendido_por = $vend_por;

        if($model->save()){
            echo Json::encode(['res' =>true]);
        }else echo Json::encode(['res' =>false]);

    }

    public function actionAjaxrechazaranuncio(){
        extract($_POST);

        //Utils::dumpx($_POST);

        $model = PubPublicacion::find()->where('id='.$id_publicacion)->one();
        $model->fecha_edicion =  date('Y-m-d H:i:s');
        $model->estado = 5; //5 - Rechazado

        if($model->save()){

/*          //Envio mail al Dependiente
            Yii::$app->mailer->compose('layouts/rechazo_anuncio', ['anuncio' => $model, ])
                ->setFrom(['no-responder@vendetumoto.co'=>'VTM - ANUNCIO [RECHAZADO]'])->setTo(trim($model->usuario->email))
                ->setBcc(['wokeer@gmail.com' => "ANUNCIO [RECHAZADO]"])
                ->setSubject('📢 ANUNCIO [RECHAZADO] | ID # ['.$model->id.']  | '.$model->tipo->nombre.' / '.$model->modelo->marca->nombre.' / '.$model->modelo->nombre.' - ('.date('Y-m-d h:i:sa').')')->send();
*/

            $hist = new PubRechazo();
            $hist->id_publicacion = $model->id;
            $hist->id_usuario =  Yii::$app->user->identity->id;
            $hist->fecha = date('Y-m-d H:i:s');
            $hist->motivo = $razon_rechazo;
            $hist->save();
            //***************

            echo Json::encode(['res' =>true]);
        }else echo Json::encode(['res' =>false]);

    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionInactivar($id)
    {

        $cliUser = PubPublicacion::find()->where('id='.$id)->one();
        $cliUser->estado = 0; //0: INACTIVO (Proceso manual que hace el admin y/o el sistema automáticamente después de los días de publicación que pagó el usuario dependiendo del plan)
        if($cliUser->save()){
            //No hacer nada
            //
        }

        Yii::$app->getSession()->setFlash('success', ['message' => 'El Anuncio ha sido <b>INACTIVADO</b> con éxito.',]);
        return $this->redirect(['index']);
    }

    public function actionAprobar($id,$op)
    {
        //Utils::dumpx($op);
        $cliUser = PubPublicacion::find()->where('id='.$id)->one();
        if($op==1){
            $cliUser->estado = 1; //1: APROBAR ANUNCIO
            if($cliUser->save()){
                //Enviar Email al Cliente avisandole de la aprobación de su anuncio
                //
                Yii::$app->getSession()->setFlash('success', ['message' => 'El Anuncio ha sido <b>APROBADO</b> con éxito.',]);
            }
        }
        else {
            $cliUser->estado = 2; //Volvio a PENDIENTE
            $cliUser->save();
            Yii::$app->getSession()->setFlash('success', ['message' => 'El Anuncio cambió su estado a <b>PENDIENTE x APROBAR</b> con éxito.',]);
        }




        return $this->redirect(['index']);
    }

    //estado =>  0: Inactivo, 1: Aprobado, 2: Pendiente x Aprobar, 3: Bloqueado,  4: Vendido, 5: Rechazado

    public function actionBloquear($id)
    {
        $cliUser = PubPublicacion::find()->where('id='.$id)->one();
        $cliUser->estado = 3; //1: BLOQUEADO (cuando hay muchas denuncias del mismo anuncio)
        if($cliUser->save()){
            //Enviar Email al Cliente notificando el Bloqueo, por denuncias de usuarios a su anuncio.
            //
        }
        Yii::$app->getSession()->setFlash('success', ['message' => 'El Anuncio ha sido <b>BLOQUEADO</b> con éxito.',]);

        return $this->redirect(['index']);
    }








    /**
     * Creates a new PubPublicacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PubPublicacion();

        /*if($s3->exist($cliente->carpeta_autos.'/'.$model->carpeta_autos.'/')){*/

        /*
        $md5_cod = Yii::$app->security->generateRandomString();
        $model->carpeta_autos = $md5_cod;
        //Obtenemos el cliente
        $cliente = \common\models\CliCliente::find()->where('id='.$model->id_cliente)->one();
        //Si el Cliente tiene Carpeta
        if($cliente->carpeta_autos!=''){
            //Instancio S3
            $s3 = Yii::$app->get('s3');

            $result = $s3->put('pubs/'.$model->carpeta_autos.'/','');
            //Si la Carpeta fué Creada con Éxito: Se Guarda el Nuevo Cliente
            if($result){
                //Crea el nuevo cliente en la BD
                if($model->save()){
                   $modArea = new CliArea();
                   $modArea->nombre="GENERAL";
                   $modArea->id_creador = Yii::$app->user->identity->id;
                   $modArea->id_cliente = $model->id;
                   $modArea->fecha = date('Y-m-d H:i:s');
                   $modArea->save();

                   if($model->logo!='' && $model->logo!=null) {
                       $inputfile->saveAs(Yii::getAlias('@webroot/uploads/clientes_logos/') . $file);
                   }
                   Yii::$app->session->setFlash('success', '¡Cliente creado con éxito!');
                   return $this->redirect(['index']);

                }else \common\components\Utils::dumpx($model->getErrors());
            }else{
                Yii::$app->session->setFlash('error', '<b>Error:</b> ¡No se pudo crear el Cliente! La carpeta MD5 no pudo ser creada (ERROR AL CREARLA EN S3).');
                return $this->redirect(['index']);
            }
        }
        */


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = PubPublicacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionPintargaleriafotosanuncio()
    {
        extract($_POST);
        $output='';
        //Utils::dumpx($_POST);
        if(isset($id) && $id!=''){
            $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->all();
            if($fotos){
                $output = "<center>";
                foreach ($fotos as $foto) {
                    //Utils::dump($foto->attributes);
                    $output .= '<a style="vertical-align: top" href="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $foto->publicacion->carpeta . '/' .$foto->archivo . '" title="Click para ampliar" data-gallery="">
                                  <img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $foto->publicacion->carpeta . '/' .$foto->archivo . '" class="img-thumbnail img-responsive"  style="width: 150px;cursor:pointer"   />
                                </a>';
                }
                $output .= '</center>';
            }

            echo Json::encode(['res' => $output,'total'=>count($fotos)]); exit;

        }else
            return false;

    }

    function getImage($url,$save_dir='',$filename='',$type=0)
    {
        if(trim($url)==''){
            return array('file_name'=>'','save_path'=>'','error'=>1);
        }

        if(trim($save_dir)==''){
            $save_dir='./';
        }

        if (trim ($filename) == '') {// Guardar nombre de archivo
            $ext=strrchr($url,'.');
            if($ext!='.gif'&&$ext!='.jpg'){
                return array('file_name'=>'','save_path'=>'','error'=>3);
            }
            $filename=time().$ext;
        }

        if(0!==strrpos($save_dir,'/')){
            $save_dir.='/';
        }

        // Crear guardar directorio
        if(!file_exists($save_dir)&&!mkdir($save_dir,0777,true)){
            return array('file_name'=>'','save_path'=>'','error'=>5);
        }

        // El método utilizado para obtener el archivo remoto
        if($type){
            $ch=curl_init();
            $timeout=5;
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
            $img=curl_exec($ch);
            curl_close($ch);
        }else{
            ob_start();
            readfile($url);
            $img=ob_get_contents();
            ob_end_clean();
        }

        //Tamaño del archivo
        $fp2=@fopen($save_dir.$filename,'a');
        fwrite($fp2,$img);
        fclose($fp2);
        unset($img,$url);

        return array('file_name'=>$filename,'save_path'=>$save_dir.$filename,'error'=>0);
    }



}
