<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use common\models\City;
use Yii;
use common\models\State;
use backend\modules\configuracion\models\search\StateSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StateController implements the CRUD actions for State model.
 */
class StateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {   date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        //echo $rol; exit;
        $where2 = '';
        $sql = "(SELECT d.id, p.name as pais, d.name as departamento, d.code  
                   FROM country as p, state as d 
                  ORDER BY d.name ASC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'id',    'dt' => 1),
            array('db' => 'id',    'dt' => 2),
            array('db' => 'departamento',      'dt' => 3),
            array('db' => 'pais',      'dt' => 4),
            array('db' => 'code',      'dt' => 5),


        );


        // echo json_encode( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    public function actionCreate()
    {
        $model = new State();

        if ($model->load(Yii::$app->request->post())) {
            $model->creation_date = date('Y-m-d H:i:s');

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>creado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro <b>actualizado</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $cities = City::find()->where('state_id=' . $id)->count();
        if ($cities == 0){
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', ['message' => '¡Registro eliminado con éxito!', ]);
        }else{
            Yii::$app->getSession()->setFlash('error', ['message' => '¡ERROR! - No se puede eliminar el departamento, tiene ciudades asociadas!', ]);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the State model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return State the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = State::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
