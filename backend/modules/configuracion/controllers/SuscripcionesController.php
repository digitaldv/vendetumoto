<?php

namespace backend\modules\configuracion\controllers;

use common\components\Utils;
use Yii;
use common\models\Suscripciones;
use backend\modules\configuracion\models\search\SuscripcionesSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;



use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;




/**
 * SuscripcionesController implements the CRUD actions for Suscripciones model.
 */
class SuscripcionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([ 'sql' => 'SELECT 1',]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExportarsuscriptores()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);

        if ($rol == "SUPERADMIN" || $rol == "ADMIN_VTM") {

            $suscriptores = Yii::$app->db->createCommand("SELECT * FROM suscripciones order by id")->queryAll();
            $vector = ArrayHelper::toArray($suscriptores);
            //Utils::dumpx($vector);

            $writer = WriterEntityFactory::createXLSXWriter();
            $writer->setShouldUseInlineStrings(true);
            $file = 'Suscriptores_Full_(' . date('Y-m-d') . ')_(' . date('h_i a') . ').xlsx';
            $writer->openToBrowser($file);

            //$writer->openToFile($filePath);

            /** Create a style with the StyleBuilder */
            $style = (new StyleBuilder())->setFontBold()->setFontSize(11)->setFontColor('ffffff')->setBackgroundColor('2c387b')->setShouldWrapText()->setCellAlignment(CellAlignment::CENTER)->setBackgroundColor('2c387b')->build();
            $style2 = (new StyleBuilder())->setFontBold()->setFontSize(11)->setShouldWrapText()->setCellAlignment(CellAlignment::CENTER)->build();

            $row = WriterEntityFactory::createRowFromArray(['id','email','fecha'], $style);
            $writer->addRow($row);

            for($i=0;$i<count($vector);$i++){
                $row = WriterEntityFactory::createRowFromArray([$vector[$i]['id'],$vector[$i]['email'],$vector[$i]['fecha']], $style2);
                $writer->addRow($row);
            }
            $writer->close();

        }

    }


    public function actionAjax_index()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT *
                FROM suscripciones
               ORDER BY id DESC) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'id',    'dt' => 0),
            array('db' => 'fecha', 'dt' => 1,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'email',  'dt' => 2),



        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    protected function findModel($id)
    {
        if (($model = Suscripciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
