<?php

namespace backend\modules\configuracion\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AdsClientes;

/**
 * AdsClientesSearch represents the model behind the search form of `common\models\AdsClientes`.
 */
class AdsClientesSearch extends AdsClientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipo_persona'], 'integer'],
            [['nombre_contacto', 'telefonos', 'celular', 'fecha', 'empresa'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdsClientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'tipo_persona' => $this->tipo_persona,
        ]);

        $query->andFilterWhere(['like', 'nombre_contacto', $this->nombre_contacto])
            ->andFilterWhere(['like', 'telefonos', $this->telefonos])
            ->andFilterWhere(['like', 'celular', $this->celular])
            ->andFilterWhere(['like', 'empresa', $this->empresa]);

        return $dataProvider;
    }
}
