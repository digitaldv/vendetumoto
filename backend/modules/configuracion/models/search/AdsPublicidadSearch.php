<?php

namespace backend\modules\configuracion\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AdsPublicidad;

/**
 * AdsPublicidadSearch represents the model behind the search form of `common\models\AdsPublicidad`.
 */
class AdsPublicidadSearch extends AdsPublicidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipo_publicidad', 'id_cliente_ads', 'precio', 'estado', 'dias_publicado'], 'integer'],
            [['fecha', 'imagen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdsPublicidad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipo_publicidad' => $this->tipo_publicidad,
            'id_cliente_ads' => $this->id_cliente_ads,
            'fecha' => $this->fecha,
            'precio' => $this->precio,
            'estado' => $this->estado,
            'dias_publicado' => $this->dias_publicado,
        ]);

        $query->andFilterWhere(['like', 'imagen', $this->imagen]);

        return $dataProvider;
    }
}
