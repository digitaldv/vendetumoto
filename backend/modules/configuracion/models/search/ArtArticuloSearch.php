<?php

namespace backend\modules\configuracion\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ArtArticulo;

/**
 * ArtArticuloSearch represents the model behind the search form of `common\models\ArtArticulo`.
 */
class ArtArticuloSearch extends ArtArticulo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_categoria', 'id_creador', 'estado', 'visitas', 'id_editor'], 'integer'],
            [['titulo', 'contenido', 'fecha_creacion', 'imagen_cabecera', 'fecha_edicion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArtArticulo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_categoria' => $this->id_categoria,
            'id_creador' => $this->id_creador,
            'fecha_creacion' => $this->fecha_creacion,
            'estado' => $this->estado,
            'visitas' => $this->visitas,
            'fecha_edicion' => $this->fecha_edicion,
            'id_editor' => $this->id_editor,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'contenido', $this->contenido])
            ->andFilterWhere(['like', 'imagen_cabecera', $this->imagen_cabecera]);

        return $dataProvider;
    }
}
