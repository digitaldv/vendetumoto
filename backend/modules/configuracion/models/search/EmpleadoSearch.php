<?php

namespace backend\modules\configuracion\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Empleado;

/**
 * EmpleadoSearch represents the model behind the search form of `common\models\Empleado`.
 */
class EmpleadoSearch extends Empleado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_usuario', 'id_ciudad', 'id_creador', 'rol', 'tipo_documento', 'documento', 'estado'], 'integer'],
            [['nombres', 'apellidos', 'email', 'fecha_nacimiento', 'direccion', 'telefono', 'celular', 'created_date', 'update_date', 'deleted_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empleado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_usuario' => $this->id_usuario,
            'id_ciudad' => $this->id_ciudad,
            'id_creador' => $this->id_creador,
            'rol' => $this->rol,
            'tipo_documento' => $this->tipo_documento,
            'documento' => $this->documento,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'estado' => $this->estado,
            'created_date' => $this->created_date,
            'update_date' => $this->update_date,
            'deleted_date' => $this->deleted_date,
        ]);

        $query->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'celular', $this->celular]);

        return $dataProvider;
    }
}
