<?php

namespace backend\modules\configuracion\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Paquete;

/**
 * PaqueteSearch represents the model behind the search form of `common\models\Paquete`.
 */
class PaqueteSearch extends Paquete
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'precio', 'estado', 'total_fotos', 'total_videos', 'total_anuncios', 'dias_publicados'], 'integer'],
            [['nombre', 'descripcion', 'fecha_creacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paquete::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'precio' => $this->precio,
            'estado' => $this->estado,
            'fecha_creacion' => $this->fecha_creacion,
            'total_fotos' => $this->total_fotos,
            'total_videos' => $this->total_videos,
            'total_anuncios' => $this->total_anuncios,
            'dias_publicados' => $this->dias_publicados,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
