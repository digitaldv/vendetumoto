<?php

namespace backend\modules\configuracion\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PubMensaje;

/**
 * PubMensajeSearch represents the model behind the search form of `common\models\PubMensaje`.
 */
class PubMensajeSearch extends PubMensaje
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_publicacion', 'id_creador', 'leido'], 'integer'],
            [['mensaje', 'fecha', 'fecha_leido'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PubMensaje::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_publicacion' => $this->id_publicacion,
            'id_creador' => $this->id_creador,
            'leido' => $this->leido,
            'fecha' => $this->fecha,
            'fecha_leido' => $this->fecha_leido,
        ]);

        $query->andFilterWhere(['like', 'mensaje', $this->mensaje]);

        return $dataProvider;
    }
}
