<?php

namespace backend\modules\configuracion\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PubPublicacion;

/**
 * PubPublicacionSearch represents the model behind the search form of `common\models\PubPublicacion`.
 */
class PubPublicacionSearch extends PubPublicacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_usuario', 'id_tipo', 'id_modelo', 'descuento', 'ano', 'estado', 'recorrido', 'unid_recorrido', 'tipo_combustible', 'id_ciudad', 'tipo_vendedor', 'cilindraje', 'condicion', 'vendido_por'], 'integer'],
            [['fecha_creacion', 'descripcion', 'placa', 'numero_contacto', 'whatsapp', 'fecha_edicion', 'ultima_renovacion', 'slug'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PubPublicacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_usuario' => $this->id_usuario,
            'id_tipo' => $this->id_tipo,
            'id_modelo' => $this->id_modelo,
            'fecha_creacion' => $this->fecha_creacion,
            'precio' => $this->precio,
            'descuento' => $this->descuento,
            'ano' => $this->ano,
            'estado' => $this->estado,
            'recorrido' => $this->recorrido,
            'unid_recorrido' => $this->unid_recorrido,
            'tipo_combustible' => $this->tipo_combustible,
            'id_ciudad' => $this->id_ciudad,
            'fecha_edicion' => $this->fecha_edicion,
            'ultima_renovacion' => $this->ultima_renovacion,
            'tipo_vendedor' => $this->tipo_vendedor,
            'cilindraje' => $this->cilindraje,
            'condicion' => $this->condicion,
            'vendido_por' => $this->vendido_por,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'placa', $this->placa])
            ->andFilterWhere(['like', 'numero_contacto', $this->numero_contacto])
            ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
