
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>

<div class="art-categoria-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4" >

            <?=  $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(common\models\City::find()->orderBy('state_id')->all(), 'id', 'nombreFull2'),
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('<span style="cursor: pointer"  title="Ciudad donde se encuentra el vehículo">¿Ubicación del vehículo?</span>');
            ?>
        </div>

        <div class="col-sm-2 col-md-2 col-lg-2">
            <?php

            echo $form->field($model, 'tipo_persona')->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->_tipo_persona,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <?= $form->field($model, 'nombre_contacto')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
        <?= $form->field($model, 'cargo')->textInput(['maxlength' => true])->label('Cargo : ') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 ">
            <?= $form->field($model, 'empresa')->textInput(['maxlength' => true])->label('Razón Social (opcional): ') ?>
        </div>


    </div>



    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
