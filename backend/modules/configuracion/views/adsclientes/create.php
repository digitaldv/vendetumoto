<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Nuevo Cliente Ads';
$this->params['breadcrumbs'][] = ['label' => 'Clientes Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
