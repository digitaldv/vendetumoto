<?php

use yii\helpers\Html;



$this->title = 'Editar Cliente Ads: <span style="color:green">' . $model->nombre_contacto.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Clientes Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

