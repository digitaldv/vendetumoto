<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use nullref\datatable\DataTable;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );



$this->title = 'Publicidades Ads';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(' 
 
 $("[data-toggle=\"popover\"]").popover({
   placement : "left",
   trigger: "hover",
 });

');

$this->registerJs(' 
jQuery(document).ready(function () {
      
 ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 && i!=1 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });


});

' );

//\common\components\Utils::dumpx($dataProvider);

?>
<style>
    .column-comments .post-com-count-wrapper, .column-response .post-com-count-wrapper {
        white-space: nowrap;
        word-wrap: normal;
    }
    .column-comments .post-com-count-approved, .column-comments .post-com-count-no-comments, .column-response .post-com-count-approved, .column-response .post-com-count-no-comments {
        margin-top: 5px;
    }

    .column-comments .post-com-count, .column-response .post-com-count {
        display: inline-block;
        vertical-align: top;
    }
    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }
    .column-comments .post-com-count-wrapper, .column-response .post-com-count-wrapper {
        white-space: nowrap;
        word-wrap: normal;
    }
    .column-comments .comment-count-approved, .column-comments .comment-count-no-comments, .column-response .comment-count-approved, .column-response .comment-count-no-comments {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        display: block;
        padding: 0 8px;
        min-width: 24px;
        height: 2em;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        background-color: #72777c;
        color: #fff;
        font-size: 11px;
        line-height: 21px;
        text-align: center;
    }
    .column-comments .comment-count-approved, .column-comments .comment-count-no-comments, .column-response .comment-count-approved, .column-response .comment-count-no-comments {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        display: block;
        padding: 0 8px;
        min-width: 24px;
        height: 2em;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        background-color: #72777c;
        color: #fff;
        font-size: 11px;
        line-height: 21px;
        text-align: center;
    }


    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }

    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }

    .screen-reader-text, .screen-reader-text span, .ui-helper-hidden-accessible {
        position: absolute;
        margin: -1px;
        padding: 0;
        height: 1px;
        width: 1px;
        overflow: hidden;
        clip: rect(0 0 0 0);
        border: 0;
        word-wrap: normal!important;
    }

    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }

    .column-comments .post-com-count-wrapper, .column-response .post-com-count-wrapper {
        white-space: nowrap;
        word-wrap: normal;
    }

    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }

    .fixed .column-comments {
        width: 5.5em;
        padding: 8px 0;
        text-align: left;
    }

    .widefat td, .widefat th {
        color: #555;
    }

    .widefat td, .widefat td ol, .widefat td p, .widefat td ul {
        font-size: 11px;
        line-height: 1.5em;
    }

    .column-comments .post-com-count-pending, .column-response .post-com-count-pending {
        position: relative;
        left: -3px;
        padding: 0 5px;
        min-width: 7px;
        height: 17px;
        border: 2px solid #fff;
        -webkit-border-radius: 11px;
        border-radius: 11px;
        background: #a50829;
        color: #fff;
        font-size: 11px;
        line-height: 17px;
        text-align: center;
    }

    .column-comments .post-com-count, .column-response .post-com-count {
        display: inline-block;
        vertical-align: top;
    }

    .wp-list-table a {
        -webkit-transition: none;
        transition: none;
    }

    .widefat a {
        text-decoration: none;
    }

    #adminmenu .wp-submenu, #dashboard_quick_press .draft-title, #templateside, #the-comment-list td.comment, .dashboard-comment-wrap, .pre, .widefat * {
        word-wrap: break-word;
    }
    .column-comments .post-com-count-approved:after, .column-comments .post-com-count-no-comments:after, .column-response .post-com-count-approved:after, .column-response .post-com-count-no-comments:after {
        content: "";
        display: block;
        margin-left: 8px;
        width: 0;
        height: 0;
        border-top: 5px solid #72777c;
        border-right: 5px solid transparent;
    }
    .select2-selection__choice {
        color: #555555;
        background: #f5f5f5;
        border: 1px solid #ccc;
        border-radius: 4px;
        cursor: default;
        float: left;
        margin: 5px 0 0 6px;
        padding: 0 6px;
    }
</style>

<div class="ibox float-e-margins">
    <div class="ibox-content">

        <p class="pull-right">
            <?= Html::a('Nueva Publicidad', ['create'], ['class' => 'btn btn-danger']) ?>
        </p


        <?=
        DataTables::widget([
            'dataProvider' => $dataProvider,
            'clientOptions' => [
                "lengthMenu" => [[20, -1], [20, Yii::t('app', "All")]],
                "processing" => true,
                "serverSide" => true,
                "order" => [0, "desc"],
                "language" => [
                    "search" => "Buscar : _INPUT_",
                    "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                    "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                    "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                    "infoEmpty"=> "Sin resultados",
                    "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                ],
                // "colReorder" => true,
                //  "scrollX" => true,
                "buttons" => [
                    ["name"=> 'excel', "extend"=> 'excel',
                        "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                        "filename" => "title", "sheetName" => 'Results', "title"=> null
                    ],

                ],
                "ajax" => \yii\helpers\Url::to(['adspublicidad/ajax_index']),
                "columnDefs" => [

                    ["targets" => [0], 'searching' => false, 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                                                                    return "<a class=\"btn btn-outline btn-success btn-xs\" href=\"index.php?r=configuracion/adspublicidad/update&id="+data+"\" title=\"Editar\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                                                                }')],
                    ["targets" => [1], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                                                                    return "<a class=\"btn btn-outline btn-danger btn-xs\" title=\"Eliminar\" href=\"index.php?r=configuracion/adspublicidad/delete&id="+data+"\" data-confirm=\"¿Está seguro de eliminar el registro?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
                                                                }')],
                    ["targets" => [2], 'width' => '3%', 'className' => 'text-center'],
                    ["targets" => [3], 'width' => '80px', 'className' => 'text-center'],

                    ["targets" => [4,5 ],'width' => '300px',    'className' => 'text-left'],
                    ["targets" => [6,7], 'width' => '1%', 'className' => 'text-center'],
                    ["targets" => [4,5,9 ],   'className' => 'text-center'],
                    ["targets" => [8,9], 'width' => '1%', 'className' => 'text-center'],


                ],
            ],
            'columns' => [
                ['attribute' => 'id', 'label' => '',], //0
                ['attribute' => 'id', 'label' => '',], //1
                ['attribute' => 'id', 'label' => 'ID',], //2
                ['attribute' => 'fecha_creacion', 'label' => 'Creado',],//3
                ['attribute' => 'cliente', 'label' => 'Cliente',], //4
                ['attribute' => 'tipo_publicidad', 'label' => 'Tipo_Publicidad'],//5
                ['attribute' => 'fecha_publicacion', 'label' => 'Fecha_a_Publicar',], //6
                ['attribute' => 'precio', 'label' => 'Precio_Cobrado',],//7
                ['attribute' => 'imagen', 'label' => 'Imagen_Publicitada',],//8
                ['attribute' => 'estado', 'label' => 'Estado',],//9
            ],
        ]);


        ?>
    </div>
</div>



<?php
/*
?>
<div class="ads-publicidad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ads Publicidad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tipo_publicidad',
            'id_cliente_ads',
            'fecha',
            'precio',
            //'estado',
            //'dias_publicado',
            //'imagen',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

*/
?>

