<?php

use yii\helpers\Html;


$this->title = 'Editar Publicidad: <span style="color:green">ID # ' . $model->id.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Publicidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

