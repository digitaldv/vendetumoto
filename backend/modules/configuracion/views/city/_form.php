<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="art-categoria-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-3">

            <?=
            $form->field($model, 'state_id')->widget(\kartik\select2\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\State::find()->orderBy('name')->all(), 'id', 'nombreFull'),
                'options' => ['placeholder' => '-- seleccione --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('País - Departamento : ');
            ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>

    </div>



    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
