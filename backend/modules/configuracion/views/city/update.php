<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Editar Ciudad: <span style="color:green">' . $model->name.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Ciudades', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

