<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;



/* @var $this yii\web\View */
/* @var $model common\models\Empleado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'rol')->widget(Select2::classname(), [
                'data' => $model->_rol,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tipo_documento')->widget(Select2::classname(), [
                'data' => $model->_tipo,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-2"><?= $form->field($model, 'documento')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-2">
            <?php if($model->isNewRecord) $model->estado=1; ?>
            <?= $form->field($model, 'estado')->widget(Select2::classname(), [
                'data' => $model->_estado,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4">
            <?= $form->field($model, 'fecha_nacimiento')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => Yii::t('app', 'Fecha Creación...')],
                'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
     </div>
    <div class="row">
        <div class="col-md-3">
            <?=  $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
                'data' =>  ArrayHelper::map(common\models\City::find()->orderBy('state_id')->all(), 'id', 'nombreFull'),
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Ciudad');
             ?></div>
        <div class="col-md-3"><?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'celular')->textInput() ?></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
