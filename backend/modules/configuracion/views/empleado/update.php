<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Empleado */

$this->title = 'Editar Empleado : ' . $model->nombreFull;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
