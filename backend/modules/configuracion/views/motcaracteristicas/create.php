<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Nueva Característica';
$this->params['breadcrumbs'][] = ['label' => 'Características', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
