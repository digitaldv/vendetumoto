<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Editar Característica: <span style="color:green">' . $model->nombre.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Características', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

