<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="mot-tipo-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">

            <?=
            $form->field($model, 'id_marca')->widget(\kartik\select2\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\MotMarca::find()->where('estado=1')->orderBy('nombre')->all(), 'id', 'nombre'),
                'options' => ['placeholder' => '-- seleccione --'],
                'pluginOptions' => ['allowClear' => true],
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?php
            if($model->isNewRecord) $model->estado=1;
            echo $form->field($model, 'estado')->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->_estado,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>

    </div>



    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
