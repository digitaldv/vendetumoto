<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use nullref\datatable\DataTable;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );


$this->title = 'Tipos de vehículo';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(' 
 
 $("[data-toggle=\"popover\"]").popover({
   placement : "left",
   trigger: "hover",
 });
');

$this->registerJs(' 
jQuery(document).ready(function () {
     
 ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 && i!=1 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
 
});

' );

//\common\components\Utils::dumpx($dataProvider);

?>

<div class="ibox float-e-margins">
    <div class="ibox-content">

        <p class="pull-right">
            <?= Html::a('Nuevo Tipo', ['create'], ['class' => 'btn btn-danger']) ?>
        </p


        <?=
        DataTables::widget([
            'dataProvider' => $dataProvider,
            'clientOptions' => [
                "lengthMenu" => [[20], [20]],
                "processing" => true,
                "serverSide" => true,
                "order" => [2, "desc"],
                "language" => [
                    "search" => "Buscar : _INPUT_",
                    "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                    "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                    "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                    "infoEmpty"=> "Sin resultados",
                    "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                ],
                // "colReorder" => true,
                //  "scrollX" => true,
                "buttons" => [
                    ["name"=> 'excel', "extend"=> 'excel',
                        "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                        "filename" => "title", "sheetName" => 'Results', "title"=> null
                    ],

                ],
                "ajax" => \yii\helpers\Url::to(['mottipo/ajax_index']),
                "columnDefs" => [

                    ["targets" => [0], 'searching' => false, 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                        return "<a class=\"btn btn-outline btn-success btn-xs\" href=\"index.php?r=configuracion/mottipo/update&id="+data+"\" title=\"Editar\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                    }')],
                    ["targets" => [1], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                        return "<a class=\"btn btn-outline btn-danger btn-xs\" title=\"Eliminar\" href=\"index.php?r=configuracion/mottipo/delete&id="+data+"\" data-confirm=\"¿Está seguro de eliminar el registro?\" data-method=\"post\"><span class=\"glyphicon glyphicon-trash\"></span></a>";
                    }')],

                    ["targets" => [2], 'width' => '3%', 'className' => 'text-center'],
                    ["targets" => [3], 'width' => '10%', 'className' => 'text-left'],


                ],
            ],
            'columns' => [
                ['attribute' => 'id', 'label' => '',], //0
                ['attribute' => 'id', 'label' => '',], //1
                ['attribute' => 'id', 'label' => 'ID',], //2
                ['attribute' => 'nombre ', 'label' => 'Nombre',],//3
                ['attribute' => 'estado', 'label' => 'Estado'],//4


            ],
        ]);


        ?>
    </div>
</div>
