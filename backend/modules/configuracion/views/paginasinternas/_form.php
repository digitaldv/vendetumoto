<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JqueryAsset;
use yii\web\View;

use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\widgets\Select2;
use yii\helpers\Url;
use common\models\AdmPaginasInternas;
use unclead\multipleinput\MultipleInput;

$this->registerJs("  
  

$('#admpaginasinternas-foto').change(function (){
   
    if($('#admpaginasinternas-foto').val()==1){
       $('#foto_id').hide('slow');
    }else{
       $('#foto_id').show('slow');
    }
});




");

/*'options' => ['placeholder' => '-- seleccione --','readonly' => 'readonly',
                            'onchange' => 'var url = "'. Url::to(['/configuracion/admpaginasinternas/update']).'";   '
                            . '$("#w0").attr("action",url+"&id="+$("#admpaginasinternas-pagina  option:selected").val()+"&t=1");$("#w0").submit();'],*/
?>

<div class="adm-paginas-internas-form">
    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'], // important
    ]); ?>
    <br>
    <div class="row ">
        <div class="col-lg-12">
            <?php
            if(!$model->isNewRecord){
                echo '<h3>'.$model->paginas[$model->pagina].'</h3>';
                /*  ?>
               <?= $form->field($model,'pagina')->label('Selecciona la página a editar  : ')->widget(Select2::classname(), [
                        'data' => $model->paginas,
                        'options' => ['placeholder' => '-- seleccione --'],
                        'pluginOptions' => ['allowClear' => true ],
                    ]); ?>
                <?php }else{ ?>
                    <?= $form->field($model,'pagina')->label('Selecciona la página a editar  : ')->widget(Select2::classname(), [
                          'data' => $model->paginas,
                          'options' => ['placeholder' => '-- seleccione --'],
                          'pluginOptions' => ['allowClear' => true ],
                      ]); */
            } ?>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!--<h3 class="box-title"><small>Contenido de la página : </small> </h3>-->
                    <!--                    <div class="pull-right box-tools">
                                            <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                                                <i class="fa fa-minus"></i></button>
                                        </div>                   -->
                </div>

                <div class="box-body pad">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'titulo')->label('Subtítulo : ')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?=
                        $form->field($model, 'contenido')->label('Contenido : ')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advanced',
                            'clientOptions' => [
                                'uiColor' => '#CCEAEE',
                                'height' => $model->isNewRecord ? 200 : 250,
                                'toolbarGroups' => [
                                    ['name' => 'paragraph', 'groups' => ['list','image', 'indent', 'blocks', 'align', 'links', 'image']],
                                    ['name' => 'styles', 'groups' => [ 'Styles', 'Format' ] ]

                                ],
                                //'removeButtons' => 'Format,Styles,Subscript,Superscript,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Image,About,Anchor',
                               // 'removePlugins' => 'elementspath',
                                'resize_enabled' => true,
                            ],
                        ])
                        ?>
                    </div>
                    <div class="col-lg-6">


                        <div class="col-lg-12" >
                            <?=  $form->field($model, 'foto')->label('¿El contenido tendrá foto? : ')->widget(Select2::classname(), [
                                'data' => ['1' => 'El contenido NO tendrá foto', '2' => 'Con foto a la IZQUIERDA del contenido', '3' => 'Con foto a la DERECHA del contenido'],
                                'options' => ['placeholder' => '-- seleccione el orden --'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-lg-12" id="foto_id" >
                            <?php
                            echo $form->field($model,'archivo')->label('')->widget(MultipleInput::className(), [
                                'name'=>'foto',
                                'max' => 1,
                                'columns'=>[
                                    [
                                        'name'=>'foto',
                                        'enableError' => true, 'title' => 'Adjuntar Foto: ',
                                        'type'=> FileInput::className(),
                                        'options' => [
                                            'options'=>[ 'multiple' => false, ],
                                            'pluginOptions' => [
                                                'showPreview' => false, 'showUpload' => false,
                                                'browseClass' => 'btn btn-primary',
                                                'removeClass' => 'btn btn-danger',
                                                'allowedPreviewTypes'=>['image','text'],
                                                'allowedFileExtensions'=>['jpg', 'jpeg','JPG','JPEG', 'png', 'PNG'],
                                            ],
                                        ],
                                    ],

                                ],
                            ]);


                            if($model->foto > 1)
                                echo '<div style="width:100%">'
                                    .'<img style="width:54%" style="" src="'.Yii::$app->request->baseUrl.'/uploads/paginas_internas/'.$model->archivo.'" >'
                                    . '</div>';

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group pull-right" style="margin-right:10px;text-align: center">
            <?= Html::submitButton($model->isNewRecord ? ' &nbsp;Guardar &nbsp;Contenido &nbsp;' : ' &nbsp;Guardar &nbsp;Contenido &nbsp;', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?php

    function getPaginaInterna($pag){

        switch ($pag){
            case '1' : return 'nosotros';
            case '2' : return 'preguntas-frecuentes';
            case '3' : return 'terminos-y-condiciones';
            case '4' : return 'politica-privacidad';
            case '5' : return 'politica-de-proteccion-de-datos';
            case '6' : return 'seguridad';
        }

    }

    function getTRVistaPrevia($con){

        if($con->foto==1){

            echo '<tr>
                <td style="text-align: center;background: #e0f2f4;font-weight: bold">'.$con->orden.'</td>
                <td>
                     <div class="row" style="margin-top: 10px">
                         <div class="col-lg-12" >
                            <div  style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                 <h4>'.$con->titulo.'</h4>
                            </div> 
                        </div>
                            <div class="col-lg-12">
                               <div   style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                               '.$con->contenido.'
                               </div>
                           </div> 

                      </div>
                </td>
                <td style="width:5%;text-align: right;"> 
                    <!--a  data-toggle="tooltip" title="Editar" href="'.Url::to(['/configuracion/admpaginasinternas/update','id'=>$con->id]).'"><i class="fa fa-fw fa-pencil-square-o text-primary"></i></a>
                    <a  data-confirm="¿Está seguro de borrar el contenido : \''.$con->titulo.'\'?" data-method="post" href="'.Url::to(['/configuracion/admpaginasinternas/delete','id'=>$con->id]).'"><i class="fa fa-fw fa-remove text-danger"></i></a-->
                </td>
            </tr>
' ;
        } else if($con->foto==2){ //IZQUIERDA
            echo '<tr>
                    <td style="text-align: center;background: #e0f2f4;font-weight: bold">'.$con->orden.'</td>
                    <td>
                         <div class="row">
                            <div class="col-lg-12" >
                                <div  style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                     <h4>'.$con->titulo.'</h4>
                                </div> 
                            </div> 
                            <div class="col-lg-3" >
                                <div  style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                     <img width="100%" src="'.Yii::$app->request->baseUrl.'/uploads/paginas_internas/'.$con->archivo.'" />&nbsp; 
                                </div> 
                            </div>
                            <div class="col-lg-9">
                                <div   style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                   '.$con->contenido.'
                                </div>
                            </div>
                        </div>
                    </td>
                    <td style="width:5%;text-align: right;"> 
                        <!--a  data-toggle="tooltip" title="Editar"  href="'.Url::to(['/configuracion/admpaginasinternas/update','id'=>$con->id]).'"><i class="fa fa-fw fa-pencil-square-o text-primary"></i></a>
                        <a  data-confirm="¿Está seguro de borrar el contenido : \''.$con->titulo.'\'?" data-method="post" href="'.Url::to(['/configuracion/admpaginasinternas/delete','id'=>$con->id]).'"><i class="fa fa-fw fa-remove text-danger"></i></a-->
                    </td>
                </tr>';
        }else if($con->foto==3){ //DERECHA
            echo '<tr > 
                <td style="text-align: center;background: #e0f2f4;font-weight: bold">'.$con->orden.'</td>
                <td>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-lg-12" >
                            <div  style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                 <h4>'.$con->titulo.'</h4>
                            </div> 
                        </div>
                            <div class="col-lg-9">
                                <div   style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                '.$con->contenido.'
                                </div>
                           </div> 
                           <div class="col-lg-3" >
                               <div  style="border:1px solid #ccc;border-style: dotted; border-color: rgba(31, 134, 183, 0.42)">
                                    <img width="100%" src="'.Yii::$app->request->baseUrl.'/uploads/paginas_internas/'.$con->archivo.'" />&nbsp; 
                               </div> 
                           </div>                                 
                       </div>
                </td>
                <td style="width:5%;text-align: right;">
                        <!--a  data-toggle="tooltip" title="Editar"  href="'.Url::to(['/configuracion/admpaginasinternas/update','id'=>$con->id]).'"><i class="fa fa-fw fa-pencil-square-o text-primary"></i></a>
                        <a data-confirm="¿Está seguro de borrar el contenido : \''.$con->titulo.'\'?" data-method="post" href="'.Url::to(['/configuracion/admpaginasinternas/delete','id'=>$con->id]).'"><i class="fa fa-fw fa-remove text-danger"></i></a-->
                </td>
            </tr>';
        }
    }


    ?>




</div>




