<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaginasInternas */

$this->title = 'Create Paginas Internas';
$this->params['breadcrumbs'][] = ['label' => 'Paginas Internas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paginas-internas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
