<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use nullref\datatable\DataTable;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\configuracion\models\search\EmpleadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Páginas Internas';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(' 
 
 $("[data-toggle=\"popover\"]").popover({
   placement : "left",
   trigger: "hover",
 });
 /*
    $("#datatables_w0 tbody").on("click","button", function () {
        
        var id_proceso = $(this).closest("tr").find("td:eq(2)").text();
        
        if($(this).attr("val")=="editar"){
             window.open("index.php?r=procesos/procproceso/update&org=procesos&id="+id_proceso);
                 

        }else if($(this).attr("val")=="borrar"){
            if(confirm("¿Está totalmente seguro de INACTIVAR el proceso?")){
                window.location.href = "index.php?r=procesos/procproceso/delete&id="+id_proceso;
            }
        }
    } );
    
    $(".wrapper1").scroll(function(){
        $(".wrapper2")
            .scrollLeft($(".wrapper1").scrollLeft());
    });
    
    $(".wrapper2").scroll(function(){
        $(".wrapper1")
            .scrollLeft($(".wrapper2").scrollLeft());
    });
 
    $(".dataTables_length").addClass("bs-select");
*/
');

$this->registerJs(' 
jQuery(document).ready(function () {
      
 ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 && i!=1 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });


});

' );

//\common\components\Utils::dumpx($dataProvider);

?>

<div class="ibox float-e-margins">
    <div class="ibox-content">


        <?=
        DataTables::widget([
            'dataProvider' => $dataProvider,
            'clientOptions' => [
                "lengthMenu" => [[20], [20]],
                "processing" => true,
                "serverSide" => true,
                "order" => [1, "asc"],
                "language" => [
                    "search" => "Buscar : _INPUT_",
                    "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                    "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                    "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                    "infoEmpty"=> "Sin resultados",
                    "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                ],
                // "colReorder" => true,
                //  "scrollX" => true,
                "buttons" => [
                    ["name"=> 'excel', "extend"=> 'excel',
                        "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                        "filename" => "title", "sheetName" => 'Results', "title"=> null
                    ],

                ],
                "ajax" => \yii\helpers\Url::to(['paginasinternas/ajax_index']),
                "columnDefs" => [

                    ["targets" => [0], 'searching' => false, 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                            return "<a class=\"btn btn-outline btn-success btn-xs\" href=\"index.php?r=configuracion/paginasinternas/update&id="+data+"\" title=\"Editar\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                        }')],
                    ["targets" => [1], 'width' => '3%', 'className' => 'text-center'],
                    ["targets" => [2], 'width' => '15%', 'className' => 'text-center'],
                    ["targets" => [3], 'width' => '10%', 'className' => 'text-left'],


                ],
            ],
            'columns' => [
                ['attribute' => 'id', 'label' => '',], //0
                ['attribute' => 'id', 'label' => 'ID',], //1
                ['attribute' => 'titulo', 'label' => 'Título'],//4
                ['attribute' => 'archivo', 'label' => 'Foto',],//5
                ['attribute' => 'contenido', 'label' => 'Contenido',],//5



            ],
        ]);


        ?>
    </div>
</div>
