<?php

use yii\helpers\Html;



$this->title = 'Editar Página: <span style="color:green">' . $model->titulo.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Páginas', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

