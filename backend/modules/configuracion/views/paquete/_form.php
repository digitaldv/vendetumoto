<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="art-categoria-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">


        <div class="col-md-3">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'descripcion')->textarea(['rows' => 3])->label('<b>Descripción:</b> <em>(Separar por comas cada item)</em>') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'precio')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?php
            if($model->isNewRecord) $model->estado=1;
            echo $form->field($model, 'estado')->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->_estado,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>

    </div>

    <div class="row">


        <div class="col-md-3">
            <?= $form->field($model, 'total_anuncios')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'total_fotos')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'total_videos')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'dias_publicados')->textInput() ?>
        </div>

    </div>






    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


