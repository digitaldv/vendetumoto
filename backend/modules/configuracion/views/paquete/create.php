<?php

use yii\helpers\Html;


$this->title = 'Nuevo Plan';
$this->params['breadcrumbs'][] = ['label' => 'Planes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
