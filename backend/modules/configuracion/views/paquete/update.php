<?php

use yii\helpers\Html;


$this->title = 'Editar Plan: <span style="color:green">' . $model->nombre.'</span>';
$this->params['breadcrumbs'][] = ['label' => 'Planes', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

