<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\file\FileInput;
use common\models\CliCliente;

/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cli-usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'descripcion')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'precio')->textInput(['style' => 'text-align:right'])->label('Precio: $') ?>
        </div>
        <div class="col-md-2">
            <?php
            if($model->isNewRecord) $model->estado=1;
            echo $form->field($model, 'estado')->widget(Select2::classname(), [
                'data' => $model->_estado,
                'options' => ['placeholder' => '- seleccione -'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




