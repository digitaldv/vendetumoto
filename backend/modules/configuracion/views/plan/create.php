<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CliUsuario */

$this->title = 'Nuevo Plan';
$this->params['breadcrumbs'][] = ['label' => 'Planes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
