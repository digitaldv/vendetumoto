<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PubMensaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pub-mensaje-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_publicacion')->textInput() ?>

    <?= $form->field($model, 'id_creador')->textInput() ?>

    <?= $form->field($model, 'mensaje')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'leido')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'fecha_leido')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
