<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PubMensaje */

$this->title = 'Create Pub Mensaje';
$this->params['breadcrumbs'][] = ['label' => 'Pub Mensajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pub-mensaje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
