<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PubPublicacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pub-publicacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_usuario')->textInput() ?>



    <?= $form->field($model, 'id_tipo')->textInput() ?>

    <?= $form->field($model, 'id_modelo')->textInput() ?>

    <?= $form->field($model, 'fecha_creacion')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <?= $form->field($model, 'descuento')->textInput() ?>

    <?= $form->field($model, 'placa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ano')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'recorrido')->textInput() ?>

    <?= $form->field($model, 'unid_recorrido')->textInput() ?>

    <?= $form->field($model, 'tipo_combustible')->textInput() ?>

    <?= $form->field($model, 'numero_contacto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'whatsapp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_ciudad')->textInput() ?>

    <?= $form->field($model, 'fecha_edicion')->textInput() ?>

    <?= $form->field($model, 'ultima_renovacion')->textInput() ?>

    <?= $form->field($model, 'tipo_vendedor')->textInput() ?>

    <?= $form->field($model, 'cilindraje')->textInput() ?>

    <?= $form->field($model, 'condicion')->textInput() ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendido_por')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
