<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/galeria/css/blueimp-gallery.min.css');
$this->registerJsFile(Yii::$app->request->baseUrl . '/css/galeria/js/blueimp-helper.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/css/galeria/js/blueimp-gallery.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/css/galeria/js/blueimp-gallery-indicator.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/css/galeria/js/vendor/jquery.js');
$this->registerJsFile(Yii::$app->request->baseUrl . '/css/galeria/js/jquery.blueimp-gallery.js');


$this->title = 'Gestión de Anuncios (Publicaciones)';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(' 
jQuery(document).ready(function () {
  ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
   

});



' ); //,View::POS_END


$this->registerJs("

$('.btn_save_vendido').click(function(){
                                
    if($('#pubpublicacion-vendido_por').val()!=''){
        $('#modal-delete').modal('hide');
        $('#modal-loading').modal('show');            
        var vend_por = $('#pubpublicacion-vendido_por').val();
        $.post('index.php?r=configuracion/pubpublicacion/ajaxvendidoanuncio', {id_publicacion: $('#id_publicacion').val(), vend_por: vend_por }, 
           function(res_sub) {
                var res = jQuery.parseJSON(res_sub); 
               // console.log(res); return false;
                if(res.res){
                     alert('¡Anuncio Vendido con éxito!');                        
                     $('#form').submit(); 
                }else{
                    alert('ERROR 101: verificar con soporte del sistema. ');
                }
        });
    }else { 
        $('.error').attr('class','error'); $('#pubpublicacion-id').focus(); 
    }
}); 

$('.btn_descargar').click(function(e){
        e.preventDefault();  
        if($('#id_publicacion').val()!=''){
            window.open('index.php?r=configuracion/pubpublicacion/descargarimagenes&id_publicacion='+$('#id_publicacion').val(), 'width=800, height=600');
        }else{
            alert('ERROR 105: ¡ID_PUBLICACION VACÍA!');
        } 
}); 

$('.btn_save_rechazo').click(function(){
  
    if($('#pubpublicacion-id').val()!=''){
        $('#modal-delete').modal('hide');
        $('#modal-loading').modal('show');            
        var razon_rechazo = $('#pubpublicacion-id').val();
        $.post('index.php?r=configuracion/pubpublicacion/ajaxrechazaranuncio', {id_publicacion: $('#id_publicacion').val(), razon_rechazo: razon_rechazo }, 
           function(res_sub) {
                var res = jQuery.parseJSON(res_sub); 
                if(res.res){
                     alert('¡Anuncio Rechazado satisfactoriamente! Se avisó por correo al anunciante.');                        
                     $(\"#form\").submit(); 
                }else{
                    alert('ERROR 100: verificar con soporte del sistema. ');
                }
        });
    }else { 
        $('.error').attr('class','error'); $('#pubpublicacion-id').focus(); 
    }
        
});   
");

?>
<style>


</style>

<div class="ibox float-e-margins">
    <div class="ibox-content" style="min-height: 600px;">

        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'clientOptions' => [
                "lengthMenu" => [[20], [20]],
                "processing" => true,
                "serverSide" => true,
                "order" => [2, "desc"],
                "language" => [
                    "search" => "Buscar : _INPUT_",
                    "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                    "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                    "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                    "infoEmpty"=> "Sin resultados",
                    "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                ],
                // "colReorder" => true,
                //  "scrollX" => true,
                "buttons" => [
                    ["name"=> 'excel', "extend"=> 'excel',
                        "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                        "filename" => "title", "sheetName" => 'Results', "title"=> null
                    ],

                ],
                "ajax" => \yii\helpers\Url::to(['pubpublicacion/ajax_index']),
                "columnDefs" => [

                    ["targets" => [0], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                              return data;
                    }')],
                    ["targets" => [1], 'width' => '1%', 'className' => 'text-center',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                              return data;
                    }')],

                    ["targets" => [3], 'width' => '7%', 'className' => 'text-center'],
                    ["targets" => [4], 'width' => '150px', 'className' => 'text-center'],
                    ["targets" => [5], 'width' => '150px', 'className' => 'text-left'],
                    ["targets" => [2], 'width' => '70px', 'className' => 'text-center'],
                    ["targets" => [8,9], 'width' => '80px', 'className' => 'text-center'],
                    ["targets" => [10,11,12,15], 'width' => '100px', 'className' => 'text-center'],


                ],
            ],
            'columns' => [

                ['attribute' => 'id', 'label' => '',], //0
                ['attribute' => 'id', 'label' => 'ID',], //1
                ['attribute' => 'fecha_creacion', 'label' => 'Creado',],//2
                ['attribute' => 'estado', 'label' => 'Estado',],//3
                ['attribute' => 'plan', 'label' => 'PLAN',],//4
                ['attribute' => 'tipo', 'label' => 'Tipo / Marca / Modelo',],//5
                ['attribute' => 'ciudad', 'label' => 'Ciudad',],//6
                ['attribute' => 'descripcion', 'label' => 'Descripción',],//7
                ['attribute' => 'precio', 'label' => 'Precio_($)',],//8
                ['attribute' => 'placa_ano', 'label' => 'Placa / Año',],//9
                ['attribute' => 'recor_comb', 'label' => 'Recorrido / Combustible',],//10
                ['attribute' => 'cilindraje', 'label' => 'Cilindraje',],//11
                ['attribute' => 'condicion', 'label' => 'Condición',],//12
                ['attribute' => 'usuario', 'label' => 'Usuario',],//13
                ['attribute' => 'numero_contacto', 'label' => 'Nros_Contacto',],//14
                ['attribute' => 'tipo_vendedor', 'label' => 'Vendedor',],//15
                ['attribute' => 'vendido_por', 'label' => 'Vendido por',],//16

            ],
        ]);
        ?>
    </div>
</div>



<?php

$form = ActiveForm::begin([
    'id' => 'form',

]);

$model1 = new \common\models\PubPublicacion();

\yii\bootstrap\Modal::begin([
    'header' => '<h2 class="modal-title" style="color:#dd4b39">Rechazar Publicación de Anuncio</h2>',
    'id'     => 'modal-delete',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'footer' => '<button type="button" class="btn btn-danger btn_save_rechazo"   aria-hidden="true">Enviar Mensaje</button><button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
]); ?>

<?= '<div id="titulo" style="font-size:13px;margin-top: 0px; padding: 3px;text-align:left;">
     <b style="color:green">ID # </b> <span class="id_pub"> -- </span><br>
     <b style="color:green">Tipo / Marca / Modelo : </b> <span class="tmm"> -- </span><br>
     <b style="color:green">Descripción : </b> <span class="descripcion">  </span> <br> <a class="url pull-right" style="text-decoration: underline" target="_blank" href=""></a>
     <input type="hidden" id="id_publicacion" >
     <br>
     <div class="hidden error" style="color: #f9f9f9;font-weight: bold;background: #f33b42;width: 100%;padding: 10px">ERROR: Debe escribir la razón del rechazo.</div>
     <br>
        '.$form->field($model1, 'id')->textarea(['rows' => 2])->label('Motivo por el cual rechaza el anuncio : ').' 
     </div>
     <br>
     <em class="modal-texto"><b>Nota : </b> Al rechazar un anuncio se notificará vía email a su dueño, informando el motivo de rechazo para su corrección. </em>'; ?>

<?php \yii\bootstrap\Modal::end();


ActiveForm::end();



$form2 = ActiveForm::begin([
    'id' => 'form2',

]);

$model2 = new \common\models\PubPublicacion();

\yii\bootstrap\Modal::begin([
    'header' => '<h2 class="modal-title" style="color:#dd4b39">Rechazar Publicación de Anuncio</h2>',
    'id'     => 'modal-vendido',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'footer' => '<button type="button" class="btn btn-danger btn_save_vendido"   aria-hidden="true">Actualizar a Vendido</button><button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
]); ?>

<?php echo '<div id="titulo" style="font-size:13px;margin-top: 0px; padding: 3px;text-align:left;">
     <b style="color:green">ID # </b> <span class="id_pub"> -- </span><br>
     <b style="color:green">Tipo / Marca / Modelo : </b> <span class="tmm"> -- </span><br>
     <b style="color:green">Descripción : </b> <span class="descripcion">  </span> <br> <a class="url pull-right" style="text-decoration: underline" target="_blank" href=""></a>
     <input type="hidden" id="id_publicacion" >
     <br>
     <div class="hidden error" style="color: #f9f9f9;font-weight: bold;background: #f33b42;width: 100%;padding: 10px">ERROR: Debe seleccionar un canal de venta.</div>
     <br>
     Selecciona el canal de venta donde se vendió el vehículo : 
     <select id="pubpublicacion-vendido_por" class="form-control select2-hidden-accessible" name="PubPublicacion[vendido_por]" data-s2-options="s2options_d6851687" data-krajee-select2="select2_354f8bc4"  data-select2-id="pubpublicacion-vendido_por" tabindex="1" aria-hidden="true">
        <option value="" data-select2-id="2">-- seleccione --</option>
        <option value="1" data-select2-id="6">Por vende tu moto</option>
        <option value="2" data-select2-id="7">Por Whatsapp</option>
        <option value="3" data-select2-id="8">Por email</option>
        <option value="4" data-select2-id="9">Por llamada telefonica</option>
      </select>
      
     </div>'; ?>

<?php \yii\bootstrap\Modal::end();


ActiveForm::end();




\yii\bootstrap\Modal::begin([
    'header' => '<h2 class="modal-title" style="color:#dd4b39">Rechazar Publicación de Anuncio</h2>',
    'id'     => 'modal-fotos',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>

           <p style="width: 100%;float: right;margin-top: 20px">
                <button class="pull-right btn btn-sm btn-success btn_descargar"   style="margin-right: 10px;visibility: hidden">Descargar Imagenes (.zip)</button>
           </p>
           <div id="links" class="links" style="min-height: 500px;padding: 10px">

           </div>
           <!-- The Gallery as lightbox dialog -->
           <div
                    id="blueimp-gallery"
                    class="blueimp-gallery"
                    aria-label="image gallery"
                    aria-modal="true"
                    role="dialog"
            >
                <div class="slides" aria-live="polite"></div>
                <h3 class="title"></h3>
                <a
                        class="prev"
                        aria-controls="blueimp-gallery"
                        aria-label="previous slide"
                        aria-keyshortcuts="ArrowLeft"
                ></a>
                <a
                        class="next"
                        aria-controls="blueimp-gallery"
                        aria-label="next slide"
                        aria-keyshortcuts="ArrowRight"
                ></a>
                <a
                        class="close"
                        aria-controls="blueimp-gallery"
                        aria-label="close"
                        aria-keyshortcuts="Escape"
                ></a>
                <a
                        class="play-pause"
                        aria-controls="blueimp-gallery"
                        aria-label="play slideshow"
                        aria-keyshortcuts="Space"
                        aria-pressed="false"
                        role="button"
                ></a>
                <ol class="indicator"></ol>
           </div>



<?php \yii\bootstrap\Modal::end();
?>
