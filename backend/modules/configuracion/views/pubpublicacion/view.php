<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PubPublicacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pub Publicacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pub-publicacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_usuario',
            'id_tipo',
            'id_modelo',
            'fecha_creacion',
            'descripcion:ntext',
            'precio',
            'descuento',
            'placa',
            'ano',
            'estado',
            'recorrido',
            'unid_recorrido',
            'tipo_combustible',
            'numero_contacto',
            'whatsapp',
            'id_ciudad',
            'fecha_edicion',
            'ultima_renovacion',
            'tipo_vendedor',
            'cilindraje',
            'condicion',
            'slug',
            'vendido_por',
        ],
    ]) ?>

</div>
