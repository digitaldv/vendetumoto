<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="art-categoria-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">

            <?=
            $form->field($model, 'country_id')->widget(\kartik\select2\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Country::find()->orderBy('name')->all(), 'id', 'name'),
                'options' => ['placeholder' => '-- seleccione --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('País : ');
            ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>

    </div>



    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
