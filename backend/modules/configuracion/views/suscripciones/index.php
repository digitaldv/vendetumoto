<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\web\View;

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/site.css'   );


$this->title = 'Suscriptores';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(' 
jQuery(document).ready(function () {
  ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=0 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
   

});

' ); //,View::POS_END

?>
<span class="pull-left" >
    <?= Html::a('<i class="icon fa fa-file-excel-o"></i> Exportar', ['exportarsuscriptores'], ['title' => 'Exporta a excel los registros completos', 'target' => '_blank', 'style' => ' padding:5px;min-width:0px;height:45px;color:#307164 !important', 'class' => 'btn btn-app text-info']) ?>
</span>
<div class="ibox float-e-margins">
    <div class="ibox-content">

        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'clientOptions' => [
                "lengthMenu" => [[20], [20]],
                "processing" => true,
                "serverSide" => true,
                "order" => [2, "desc"],
                "language" => [
                    "search" => "Buscar : _INPUT_",
                    "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                    "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                    "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                    "infoEmpty"=> "Sin resultados",
                    "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                ],
                // "colReorder" => true,
                //  "scrollX" => true,
                "buttons" => [
                    ["name"=> 'excel', "extend"=> 'excel',
                        "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                        "filename" => "title", "sheetName" => 'Results', "title"=> null
                    ],

                ],
                "ajax" => \yii\helpers\Url::to(['suscripciones/ajax_index']),
                "columnDefs" => [

                    ["targets" => [0,1], 'width' => '3%', 'className' => 'text-center'],

                ],
            ],
            'columns' => [
                ['attribute' => 'id', 'label' => 'ID',], //0
                ['attribute' => 'fecha', 'label' => 'Fecha_Envío',],//1
                ['attribute' => 'email', 'label' => 'Email',],//2

            ],
        ]);
        ?>
    </div>
</div>

