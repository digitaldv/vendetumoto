<?php

namespace backend\modules\perfil\controllers;

use common\models\ArtArticulo;
use Yii;
use backend\models\FormActualizarPerfil;
use common\models\CliCliente;
use common\models\CliUsuario;
use common\components\Utils;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\components\GhostAccessControl;
use mPDF;

/**
 * Default controller for the `perfil` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
   
     public function actionIndexadmin()
    {
         /*
          *   $model = new ArtArticulo();

        if ($model->load(Yii::$app->request->post())) {

            if ($inputfile = UploadedFile::getInstance($model, 'imagen_cabecera')) {
                $img = file_get_contents($inputfile->tempName);
                $model->imagen_cabecera = 'data:image/png;base64,'.base64_encode($img) ;
            }

            $model->fecha_creacion = date('Y-m-d H:i:s');
            $model->fecha_edicion = date('Y-m-d H:i:s');
            $model->id_creador = Yii::$app->user->identity->id;
            $model->id_editor = Yii::$app->user->identity->id;
            $model->visitas = 0;

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Creado</b> el <b>Artículo</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
          * */
        $role = Utils::getRol(Yii::$app->user->identity->id);

        if($role=='ADMIN_MUSICALICA' || $role=='SUPERADMIN'){

            $model = User::find()->where('id='.Yii::$app->user->identity->id)->one();


            if(isset($_POST['User']) && isset($_POST['User']['password']) && isset($_POST['User']['username']) && isset($_POST['User']['email'])) {
                //Utils::dump($_POST);
                $model->username = $_POST['User']['username'];
                $model->email = $_POST['User']['email'];

                if($_POST['User']['password']!='') {
                    $model->auth_key = Yii::$app->security->generateRandomString();
                    $model->password_hash = Yii::$app->security->generatePasswordHash($_POST['User']['password']);

                }
                //Utils::dumpx($model->attributes);
                if($model->save()){
                      Yii::$app->getSession()->setFlash('success', ['message' => 'Su perfil ha sido actualizado con éxito', ]);
                }else{
                      Yii::$app->getSession()->setFlash('error', ['message' => 'ERROR: Su perfil NO ha sido actualizado! Verifique con el administrador.', ]);
                }
               // return $this->redirect(['index']);
            }

             return $this->render('indexadmin', [
                    'model' => $model,
                    'role' => $role,
                ]);

        }else{
            echo "ERROR: ¡Usuario No permitido para acceder a ésta pantalla!";
        }
    	 
        
    }
    
    
    public function actionIndex()
    {
    	$model = new CliUsuario(); $apoderado_update=null;
        $apoderado = array();
        $role = Utils::getRol(Yii::$app->user->identity->id);

        $model = new ArtArticulo();

        if ($model->load(Yii::$app->request->post())) {

            if ($inputfile = UploadedFile::getInstance($model, 'imagen_cabecera')) {
                $img = file_get_contents($inputfile->tempName);
                $model->imagen_cabecera = 'data:image/png;base64,'.base64_encode($img) ;
            }

            $model->fecha_creacion = date('Y-m-d H:i:s');
            $model->fecha_edicion = date('Y-m-d H:i:s');
            $model->id_creador = Yii::$app->user->identity->id;
            $model->id_editor = Yii::$app->user->identity->id;
            $model->visitas = 0;

            if($model->save()){

                Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Creado</b> el <b>Artículo</b> con éxito!',]);
                return $this->redirect(['index']);

            }else
                Utils::dumpx($model->getErrors());
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }


       // Utils::dumpx($_POST);
        
    	if($model->load(Yii::$app->request->post())) {
            /*if ($inputfile = UploadedFile::getInstance($model, 'imagen_cabecera')) {
                $img = file_get_contents($inputfile->tempName);
                $model->imagen_cabecera = 'data:image/png;base64,'.base64_encode($img) ;
            }

            $model->fecha_creacion = date('Y-m-d H:i:s');
            $model->fecha_edicion = date('Y-m-d H:i:s');
            $model->id_creador = Yii::$app->user->identity->id;
            $model->id_editor = Yii::$app->user->identity->id;
            $model->visitas = 0;

            */
            //se actualiza el perfil
            $apoderado_update = CliUsuario::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();
            //$apoderado_update->tipo = $model->tipo;
            
            //$apoderado_update->id_cliente = $model->id_cliente;
            //$apoderado_update->id_usuario = $model->id_usuario;          
            $apoderado_update->nombres = $model->nombres;
            $apoderado_update->apellidos = $model->apellidos;
            $apoderado_update->cedula = $model->cedula;
            $apoderado_update->email = $model->email;
            $apoderado_update->celular = $model->celular;                    
            $apoderado_update->estado = $model->estado;                    
//            $apoderado_update->fecha = $model->fecha;          
//            $apoderado_update->id_creador = $model->id_creador;
            $apoderado_update->id_editor = Yii::$app->user->identity->id;
            $apoderado_update->fecha_edicion = date('Y-m-d H:i:s');
            
            //Utils::dump($apoderado_update->attributes);
            if($apoderado_update->save()){
                 Yii::$app->getSession()->setFlash('success', ['message' => 'Su perfil ha sido actualizado con éxito', ]);
            }else{  
                Utils::dumpx($apoderado_update->getErrors());
            }
            //si viene la clave
            //Utils::dumpx($model->attributes);
            if(!empty($model->password)) {
                 
                    $usuario = User::findOne(Yii::$app->user->identity->id);
                    //$usuario->password = $model->password;
                    $usuario->auth_key = Yii::$app->security->generateRandomString();
                    $usuario->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                    $usuario->pass = $model->password;                    
                    $usuario->save();                 
            }
            
            if($role=='DIRECTOR_JURIDICO' || $role=='APODERADO'){
                return $this->render('indexcliente', [
                   'model' => $model,
                   'role' => $role,
                   //'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                   'proveedor' => $apoderado_update
               ]);	
            }else{
                 return $this->render('index', [
                'model' => $model,
                'role' => $role,
                //'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                'proveedor' => $apoderado_update
            ]);	
            }

           // return $this->redirect(['index']);
    	} else {
               // Utils::dumpx($model->getErrors());
    		     $apoderado = CliUsuario::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();
                if($role!='SUPERADMIN' && isset($apoderado)){
                    $model->id_cliente = $apoderado->id_cliente;
                    $model->id_usuario = $apoderado->id_usuario;          
                    $model->nombres = $apoderado->nombres;
                    $model->apellidos = $apoderado->apellidos;
                    $model->cedula = $apoderado->cedula;
                    $model->email = $apoderado->email;
                    $model->celular = $apoderado->celular;                    
                    $model->estado = $apoderado->estado;                    
                    
                    $model->fecha = $apoderado->fecha;          
                    $model->id_creador = $apoderado->id_creador;
                    $model->id_editor = $apoderado->id_editor;
                    $model->fecha_edicion = $apoderado->fecha_edicion;
                    
                }else{
                    Yii::$app->getSession()->setFlash('error', [
                            'message' => 'Usted no tiene un ROL con Permisos, no tiene facultades de ejecutar acciones sobre ésta pantalla!',
                        ]);
                    
                } 

    	  
            if($role=='DIRECTOR_JURIDICO' || $role=='APODERADO'){
                return $this->render('indexcliente', [
                   'model' => $model,
                   'role' => $role,
                   'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                   'proveedor' => $apoderado_update
               ]);	
            }else{
                 return $this->render('index', [
                'model' => $model,
                'role' => $role,
                'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                'proveedor' => $apoderado_update
            ]);	
            }
    	}
        
    }

    public function actionExportar(){
        $mpdf=new mPDF();
        $model = Empleado::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();
        $formaciones_academica = EmpFormacionAcademica::find()->where(['id_empleado' => $model->id])->all();
        $experiencias_profesional = EmpExperienciaProfesional::find()->where(['id_empleado' => $model->id])->all();
        $disciplinas_anexas = EmpDisciplinaAnexa::find()->where(['id_empleado' => $model->id])->all();

        //echo count($rubros);
        $mpdf->WriteHTML($this->renderPartial('exportar_resumen', [
            'model' => $model,
            'role' => $role,
            'formaciones_academica' => $formaciones_academica,
            'experiencias_profesional' => $experiencias_profesional,
            'disciplinas_anexas' => $disciplinas_anexas
        ]));

        $mpdf->Output('Resumen_hv_'.$model->id.'.pdf', 'D');
        exit;
        //return $this->renderPartial('mpdf');
    }
}
