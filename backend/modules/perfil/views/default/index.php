<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use common\components\Utils;
use kartik\widgets\Select2;

$this->title = 'Mi Perfil de Proveedor';
$this->params['breadcrumbs'][] = $this->title;



?>

<div class="perfil-default-index">
    <div class="row">
        <?php $form = ActiveForm::begin([
                                'id' => 'perfil_form',
                                'options'=>['enctype'=>'multipart/form-data'], // important
                            ]); ?>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <?php
                        $ruta = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist')."/img/user2-160x160.jpg";
                        if(!empty($proveedor->logo)) {
                            $ruta = Yii::$app->request->baseUrl.'/uploads/perfiles/'.$proveedor->logo;
                        }
                    ?>
                    <img class="profile-user-img img-responsive img-circle" src="<?= $ruta?>" class="user-image" alt="<?php echo $model->nombre   ?>"/>
                    <br/>
                    <?php echo $form->field($model, 'file_input')->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                            //'showPreview' => false,
                            'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                            'browseLabel' =>  'Examinar']
                        ])->label(false); ?>
                    <h3 class="profile-username text-center"><?php echo $model->nombre ; ?></h3>
                    <p class="text-muted text-center"><?php echo $role; ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title">
                   <i class="fa fa-key"></i> <?= Html::encode($this->title) ?>
               </h3>
            </div>
            <div class="box-body">
                <div class="perfil-form">
                     <div class="row">
                        <div class="col-lg-4"><?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-5"> <?= $form->field($model, 'contacto',[
                                        'addon' => [
                                            'prepend' => ['content'=>'<i class="fa fa-user"></i>'],
                                        ]])->textInput(['maxlength' => true]) ?></div> 
                          <div class="col-lg-3"> <?= $form->field($model, 'email',[                                       
                                        'addon' => [
                                            'prepend' => ['content'=>'<i class="fa fa-envelope"></i>'],
                                        ]])->textInput(['maxlength' => true ]) ?></div>
                     </div>
                    <div class="row">
                        <div class="col-lg-4"><?= $form->field($model, 'direccion_fisica')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-3"><?= $form->field($model, 'telefonos')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-2"><?= $form->field($model, 'mobil')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-3"><?= $form->field($model, 'cif')->textInput(['maxlength' => true]) ?></div>
                        
                    </div>
                     <div class="row">
                       
                        
                        <div class="col-lg-3"><?= $form->field($model, 'web')->label('Página WEB : ')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-3"><?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-3"><?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-lg-3"><?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?></div> 
                    </div>
                     <div class="row">
                        
                        <div class="col-lg-6"><?= $form->field($model, 'url_productos')->label('URL de importación productos : ')->textInput(['maxlength' => true,'readonly'=>true]) ?></div>
                        
                    </div>
                    
                    <div class="horarios-search">

                       
                            <div class="box box-default box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title" style="color:#000; font-size:13px">
                                       Cambiar Contraseña : 
                                    </h3>

<!--                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                    </div>-->

                                </div>

                                <div class="box-body" style="display: block; background: rgba(51, 122, 183, 0.15) ">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                            <?=
                                                    $form->field($model, 'password', [
                                                        'addon' => [
                                                            'prepend' => ['content' => '<i class="fa fa-key"></i>'],
                                                ]])
                                                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off'])
                                            ?>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                            <?=
                                                    $form->field($model, 'repeat_password', [
                                                        'addon' => [
                                                            'prepend' => ['content' => '<i class="fa fa-key"></i>'],
                                                ]])
                                                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off'])
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>

                       
                    </div>
                    
                    
                    <div class="row">
                        <div class="row text-center">
                                <div class="form-group">
                                        <?= Html::submitButton('<span class="icon wb-check"></span> Editar Perfil', ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
