<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use wdmg\widgets\TagsInput;

/* @var $this yii\web\View */
/* @var $model common\models\ArtArticulo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="art-articulo-form">
    <?php  $form = ActiveForm::begin([
        'id' => 'form',
        'options' => ['enctype' => 'multipart/form-data'], // important
    ]);
    ?>

    <div class="row">
        <div class=" col-lg-4 col-md-4 col-sm-4">

            <?php
            if ($model->imagen_cabecera) {
                ?>
                <img style="border: 2px solid #20ccd0; padding: 4px; border-radius: 7px;" class="profile-user-img img-responsive img-responsive" src="<?= $model->imagen_cabecera ?>" class="user-image" alt="<?php echo $model->titulo ?>"/>
                <br>
            <?php } else { ?>
                <img style="border: 2px solid #20ccd0; padding: 4px; border-radius: 7px;" class="profile-user-img img-responsive img-responsive" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMCAggICAgICAgGBggICAgICAgICAgHBwYICAgICAgIBggICAYICAgICAgICAoICAgICQkJCAgLDQoIDQgICQgBAwQEAgICCQICCQgCAgIICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICP/AABEIAYACAAMBEQACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAEDAgn/xABEEAACAQIBBwUOAwcFAQEAAAAAAQIDESEEBQYSMUFxB1FTYaETFhciNHKBkaKxssHR0jNzkiQyQlJigvAjwuHi8ZND/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwFwFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPzVkkm20kldt7Elt7AKZX5TYJtQpynHdLX1L9aWrLDj6gPn4UF0Mv8A6f8AQB4UF0Mv/p/0XvQFvzZnCNWEakHeLXNZp74vg8LgdQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcOfcmlOjVhFXlKElFc7thtsvWBm/eLlXRr9cfqA7xsq6Nfrj9QPHoLlXRr9cfqBeNCs11KNFxqLVlrtpXUvF1Y22NrbfACfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxVrJJybsoq7bwS4vcBWss5Q8ni7R16vXFJR9Dk4pgc/hMpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gPvknKLQk7SVSlja8kmvTquTXqAstDKFJJxaknsaaafpQH0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADyT/wDeYDKtK9JpVpuKbVKLajHZrY/vT577k9npAgGwPNXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eAC3AD0Cb0W0hlQqLH/AE5O049T3pbpRePWroDWKcrpNYppWfOnj8wP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIvSas45PWa6OS/V4t+OIGPAAAAAB42B9cnyaU8IRlN9Sb9yYHdHRvKH/+Nb9L/wA7APjlGZq0FeVOpHjCS96SA42v8tawAAAAAAJLI9G69Ra0KU5R3PCz4NuN/QB9+83Kuhn64feA7zcq6Gfrh94DvNyroZ+uH3gcmX5jrUknUpzgnvdrelpySA4QCA1rQ3Le6ZNTbvdJwx36kpQXYkBNgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIAAAAkczZgq134iVk/Gk8Ir0731IC/wCaNBaNKzku7S55W1Vvwje2HO/VuAsVGnZWSSW5LBepAfsAgIzOOj9Kr+/Ti/6l4sv1LHsYFKz5yfTp3lRbqxX8LXjpcyt+9x29QFSfNsYAAB+6EE5RT2OST4NpAbhCNsErLcubqA/VgFgFgODPtJOjWTV06U/WoyYGMAegapoF5LDzqnxyAsIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAiNLvJq3m/NAZCAAATui2jDyiV3eNOL8aW9/0x24gank+SxhFRilCKVkkrevnfWwPpFAegAAABYCp6WaHqqnUpqMaqWKWHdbfPmYGbSVsNlt29dT6wAH6o1LNPbZp8bO4G05FnGFSKnCUZJ9ezqfM1sa6gPv3Th6wHdOHrAd04esCL0lzrCnRqa0opyhKMY38aTcWlZcQMgA9A1TQLyWHnVPjkBYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEaXeTVvN+aAyEAB9825C6s4047ZOy3253wQGx5uzbGlCMIW1YrDr52+tsDrAAAAAAAA8kgM+5QMw6r7tFYSdp9Unsl6dnECmAAPLANUBqgNUAkB6AA1TQLyWHnVPjkBYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEaXeTVvN+aAyEABd+TXNibqVXu8SL69rt6LAX4AAAAAAAAAA487ZvVWnOm/wCONl1S3AYvKNsHtW31v6AeAAAAAAAAANU0C8lh51T45AWEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARGl3k1bzfmgMhAAapoJkurk0H/ADOUn6ZNLsSQFhAAAAAAAAAADAx3SfJ1HKKySslN24NRl8wIwAAAAAAAABqmgXksPOqfHICwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIADXNEKl8mo+Zb2mBMgAAAAAAAAAADI9Mn+1VuMfXqRuBDAAAAAAAAANU0C8lh51T45AWEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARGl3k1bzfmgMhAAadyfZVrUNXfCcl6G217wLOAAAAAAAAAAAMWzzlndKtSf8ANNtcNi7En6QOMAAAAAAAABqmgXksPOqfHICwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIAC1cn+dtSq6bvaqrLGyU07p/pwA0pMD0AAAAAAAABDaWZ07jRlJO0peJDGzu964K+PADIgPQAAAAAAAAGqaBeSw86p8cgLCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIjS7yat5vzQGQgAPacrNNYPc+Z7n6wNa0Xz8q9NO614q01vvzpc0ufcwJoAAAAAAAD8zmkrtqKWLb2JLnAynS7SHu9Txfw4XjDr55en3AQQAAAAAAOnNmbpVZxpws3LndkudvqSxYFp8GdTpaXqkA8GVTpaXqkBcNHM0uhSVOTUmnJ3V0mpNyW1dYEmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIjS7yat5vzQGQgAAHVmzOc6M1OD1WtvNJfyy6gNS0f0op5QsPFmljBu763DnTAl1MD9AAAAD5ZRlUYJyk1GKxbeCSAzjSzTN1r06d40v4nvqfRc64AVa4AAAAAAAHTkGXypSVSD1XF7edb0+qSwaAsHhKr81D9MsfbA88JVfmofpl96Au2i2d5V6MaktVSbkmo4LCTSwbk8UucCXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAERpd5NW835oDIQAAAB+qdVppptNbGsGuDAtmaeUOpHCrHuqX8S8Wfpex+8Cy5Hp1k8rXk6b5pprtSkgJDvlyfpqP619EB8so0tyaK/Fg+qN5/CmBBZfykxWFKEpv+aS1Y34Yt9gFMzvnqrXd6km7bI7IxvzIDgsB6AAAAAAABIaO16ca8HVScFJXvsV9jfUmBpPfXkvSw9T+gHq0ryXpYdv0Aks35ZCpFSg4yg9jWzr/AMsgOkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcGfci7pRqwV7ypySt/Ntj2pAYw3jbj6GtzA9AAAAAAAAAAAAAAAAAAAAAAAeN/5/lwNi0ZyLudCnB4PV1nxn4zv14gSgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8aAoWmWh0nJ1aMdZPGcFe99rlFY3vvQFHXX9GuIHoAAAAAAAAAAAAAAAAAAAAPJf5/wBdNEdC5NqrWi4xi7wg9s3tTlstFc21vnQGgRYH6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYDjy7M1Gp+/TpzfO4rW/Ukn2gcnejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QOnIsxUaf7lKlF3vfVTl6JO7XrA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHDnTO8aMHOeCWGCu27NpRW9uz5t4FLyjlLqP8Acp00t2s3J267OK9TA68zaf1KlSFOVKD15KN4tq197Tvgrc4Fi0jzw6FJ1IqM2pRVm7J3tjhfn2ARui+l0som4yhCCUda6bb2pbHxA/WlOlksnlCMYRnrRcsZPCztuVgPjo5pv3ao6c4xpyavCzbUrbVjvtiuezAnc95x7jSnUSUtRJ2d0neSWPNtAgtGdM5V6jhKnCCUHO8ZNu6cVbFf1AWsCM0izq6FKVRJTacVZu22SWNr8+zACI0a0xlXqOEoQglFyum28Gtz4gd2lWkMsnjCUYxm5S1bNtWsm27pNblvAg838o0pVIRlThGMpKLak/FTwvs5wLu2BSc5cocoVJwjThKMJOKk5PxtV2e7DFNATeiukTyiM5OMYaskrJt3ur3d7WAnGwKTnXlBlCrOEadOajJq7k7u2D3W23QEnoppY8oc4yioOFmkm3dPDetz6wLGB8Mry6FOLlOShFbW77dyW276gKbl3KYl+HT1lulN6t/7Um16WBww5Tqt8adJrqcl24gXPR7PXd6aqavc73Vm77OZ2WAEDn3TqdGrKmqcJKKi7uTv4yT3KwHD4SqnRU/1SAldGtMp16vc3CEFqt3TbeFtzA/elGl0snlCMYQnrRcm23hjaytdAeaM6ad3qOnOMabavGzb1rfvLHfbECaz5nF0qU6iSk4pOzwTxS3AQmiumMspqSg6caaUXLBtt7MMcALUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoHKdN61FY6tptc2tddtresDn0DWTNSVVU+6OWHdNjjZYQvZayd/+QLnS0fowmqkKcYySdmtmPPG9nxAj9P/ACaXn0/kBXeTP8ap+X/uQHvKb+LT8yXxAViGvTdOawb8eDX9MmvfFrgwNDzznRVshnUW+CuturJTi5R+aArnJuv9eX5UvigBpgFd098mnxh8cQKtycfjy/Ll74gSfKhU8WlH+qb+H6gUapkzSjLdJO3VZq4GrRz3+y93wv3Ny/vSt69YDKlk7cZTd7RcY353NtdtrgXnkwl4tZddN+vXAtec8sVOE6j/AIIuX0XpeAGU5gza61SUdr1KkuMknq34ycUB06HZx7nlEOaV4S4S2X4PV7QNYlIDLtOc9OdZwv4lJ6v9ytrPir6voAmNFdCqbhGrWi5OSvGGyMVucrWbb244AWSponkzVnRp+hOL9DjZgdubsgjShGnFWjHBb9rb47wMv048pn1qC9cUv+QLbmPMmSSo0pThS1nCLld43tjfxl7gJfN2aMnhLWpRpqVmm4u7s922WDAqHKX+LS/LfvYFWhr0nTmsG7ThLdhJrsad+IGi53zoq2QzqLDWSvH+V6yun137AK7ybfiy/KfvgBpQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACPz3mWFeGpPrcZYXg9l1f3AZ1nXQitTTaSrRV/Ghi7f1RePqA/OjelVSjKKk3Ok3aUZNvVW9w22a222WuBctPn+yy8+n6dmPpAr3Jn+NU/L/ANyAcp/4lPzJfEB+4ZndXIKcl+/SdSS/qjry1o+p63/oEJmjOajSr0n+7UhrR5taNnh5yQEnyc+US/Kl8UPmBpYFd098mnxh8cQKtycfjy/Ll74gdfKhPx6K6pvth9AIzLMhvkNGpb92pUT4Tk4/ID4zzv8AsfcU8e7P0wsperWfYB1Vs36ub1J7alWM/wC3FR7PG9IEjyYy8atwh75ASXKLl2rRUN9Sa/TGz99gKxoZn6nk7nKalKUlFR1VfBO7e6zuBC5dWi6k5Quk5uUbqzV3dYbrAbHmvLFUpwqLZOKfC+DXodwMezzF91rJ7e6VE+Ou/eBsGQTTpwktjhFr9KwAy/OmT5VS8abrwi5NJucsXtSwk9wFl5OMqlJVtaU52dO2tJytfW52+ZAV3ThftU+EPhX/AKB7keg1epCM0qdpxUleSvZ7L7dwFp0K0YqUJVHPUSlFJarvjffs5wInlM/Ep/ly97APMzq5BTlFXnSc5pfzR12pR9WKAhs05y1aWUUm8KkLx5lOLuv1JYgSnJz+NLd/pP3wA0kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABXdJ9LHk8qaUVPW1nJXtgmlg+e7YEd4TKVr9zq625Xjb0yuvcBSqOTyr1bJLWqSbtHdrPG/NFLeBoenkbZK1zSpfICvcmf41T8v8A3ID3lO/Fp+ZL4gLHoIv2aHGp8T7NoFJ0wzJ3Gq7JqE7yh1J7V6HguoDt5OPx5flS+KAGlgV3T3yafGHxxAq3Jx+PL8uXviB9eUur/rQXNTb9bl9AJXNWbe6Zv1NrcJtcVKUl7rekDPsmoubjFbZtJf3YAaRprkihkmqsFB0or0eKBB8mU0qtX8tP1S2gcvKBl+vlGrupLV/ueMnx2L0ATWYNB6U6MJ1O6OUlrO0rJJt6tlZ2ulf0gQemWjcaEoOnrasl/E72lHat21AWTk5y+9GVNu7pyw82eztTAhtO9H3Go60Ytwn+/bHVlzvmUsPTcDl0b01lQWpKPdIK9rPxoX3J7GlzMD66WaWQyiEYRjOOrJSvKyv4rVrekCR5MnhX40/dMCD02f7VU4Q+BATOaeUGFOlTpulUk4RUW1KNnZbUBMZn06hWqRpqnOLlfFtNKyb3cAIHlL/Ep/ly97Asmg8f2anzeP6fHkBRdLsy9xqtJWhPxoP4l6GBI8nH40/y38UANJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARmc8wUq9u6Q1mlZO7i0tu1Nb31gQ/g5oX21tuzXVvgAm81ZjpUfw4KL3yb1pP0vED65zzZCtBwmm4tp4Ozutjv8gOPNGjNKhJypqaclqtuV7Lbgue4H6ztozSrtSqKTcVZWk1dPF7OvrQHXmzN0KUFCCair2u7vHF443xA+GdsywrpRqJtJ3VnZp9Tw28wHwzVovRoSc6akpOLjjJvBtPhfBbwJgDkznmyNaDhNNxbWx2eGKxXXz2A4s06LUaMtampptarbldJPbh6EB5nbRWjXlr1FJytbCTStjzJ87A7chyCNOEYQTUY7L4vbf3gcWTaKUISU400pJ3Tu3Z86V37gOzOObY1oOnO+q7b2sU7rFJsDlzRovRoyc6cZRbjqu8tZWunsb6gOXKdBsnnKUpKo3Jtt67xb28wE7RpqKUUrJJJdSWCXYBx53zNCulGopNJ3VnZp8Vu6mB8c0aNUqDcqamm1Z3ldbb7OIEnKF1sXB+584EFlWg+Tzx1HC/8knG3BYrsA+EOTzJk72qPjN/JL3gTmb80U6SapwUE7Xttdr2u7vnYEdnLQ6hVm6k1PWdk7Ta2YLDBbOsDm8H+Tfy1P1v6gdGbtEKNKanBTUle15NrFWeHAD7520ZpV2pVFK8Y2VpNYf2p4gdmbc3RpQUIXUVeybu8Xd48QPjnfMlOulGom0ndNPVafNwA5806L0qEnKmpJtWd5N4bcN25b9wEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//Z" class="user-image" alt="Sin imagen_cabecera"/>

                <?php
            }
            ?>

            <?php
            echo $form->field($model, 'imagen_cabecera')->widget(FileInput::classname(), [
                'options'=>['accept'=>'image/*'],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showUpload' => false,
                    'removeClass' => 'btn btn-danger',
                    'browseClass' => 'btn btn-primary  ',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' =>  'Adjuntar',
                    'removeLabel' => 'Borrar',

                    'allowedFileExtensions'=>['jpg', 'jpeg', 'png']
                ]
            ])->label('Imagen Cabecera <em>(máximo 2MB)</em>');
            ?>

        </div>


        <div class=" col-lg-8 col-md-8 col-sm-8">
            <div class="row">
                <div class="col-md-5">
                    <?php
                    echo $form->field($model, 'tipo_contenido')->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->_tipo_contenido,
                        'options' => ['placeholder' => '- seleccione -'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

                </div>
                <div class="col-md-5">

                    <?=
                    $form->field($model, 'id_categoria')->widget(\kartik\select2\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\ArtCategoria::find()->where('estado=1')->orderBy('nombre')->all(), 'id', 'nombre'),
                        'options' => ['placeholder' => '-- seleccione --'],
                        'pluginOptions' => ['allowClear' => true],
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                    if($model->isNewRecord) $model->estado=1;
                    echo $form->field($model, 'estado')->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->_estado,
                        'options' => ['placeholder' => '- seleccione -'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'titulo')->textInput() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'contenido')->textarea(['rows'=>6]) ?>
                </div>
                <div class="col-lg-12">


                    <?php
                    //\common\components\Utils::dumpx($model->tags);


                    echo $form->field($model, 'tags')->widget(TagsInput::class, [

                        'options' => [
                            'id' => 'post-tags',
                            'class' => 'form-control',
                            'placeholder' => 'Escribir palabra ...',
                            'delimiter' => ','
                        ],
                        'pluginOptions' => [
                            'minInput' => 2,
                            'maxTags' => 100
                        ]
                    ])->label('TAGS <em style="font-size: 12px;color: darkgrey">(Ingrese la palabra y unda ENTER o TAB)</em>');

                    ?>


                </div>
                <div class="text-right" style="margin-right: 15px">
                    <?= !isset($model->isNewRecord) ? '<b style="color: green;font-size: 22px">Visitas : '.$model->visitas.'</b>' : '<b style="color: green;font-size: 22px">Visitas : 0</b>' ?>
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="form-group text-center">
            <?= Html::submitButton('Guardar Artículo', ['class' => 'btn btn-success']) ?>
        </div>
    </div>





    <?php ActiveForm::end(); ?>

</div>
