<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use common\components\Utils;
use kartik\widgets\Select2;

$this->title = 'Mi Perfil';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="perfil-default-index">
    <div class="row">
<?php
$form = ActiveForm::begin([ 'id' => 'perfil_form', ]);
?>

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <i class="fa fa-key"></i> <?= Html::encode($this->title) ?>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="perfil-form">
                        <div class="row">
                            <div class=" col-lg-2"><?= $form->field($model, 'cedula')->textInput(['maxlength' => true]) ?></div>
                            <div class=" col-lg-3"><?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?></div>
                            <div class=" col-lg-2"><?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?></div>  
                            <div class=" col-lg-3"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                            <div class=" col-lg-2"><?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?></div>
                            <?= $form->field($model, 'estado')->hiddenInput()->label(false) ?>
                        </div>
                        </div>

                        <div class="horarios-search">


                            <div class="box box-default box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title" style="color:#000; font-size:13px">
                                        Cambiar Contraseña : 
                                    </h3>

                                    <!--                                    <div class="box-tools pull-right">
                                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                                            </button>
                                                                        </div>-->

                                </div>

                                <div class="box-body" style="display: block; background: rgba(51, 122, 183, 0.15) ">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                            <?=
                                                    $form->field($model, 'password', [
                                                        'addon' => [
                                                            'prepend' => ['content' => '<i class="fa fa-key"></i>'],
                                                ]])
                                                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off'])
                                            ?>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                                            <?=
                                                    $form->field($model, 'repeat_password', [
                                                        'addon' => [
                                                            'prepend' => ['content' => '<i class="fa fa-key"></i>'],
                                                ]])
                                                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off'])
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>


                        <div class="row">
                            <div class="row text-center">
                                <div class="form-group">
                                    <?= Html::submitButton('<span class="icon wb-check"></span> Editar Perfil', ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
 
