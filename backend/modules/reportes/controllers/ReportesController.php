<?php

namespace backend\modules\reportes\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\components\GhostAccessControl;
use common\components\Utils;
use arturoliveira\ExcelView;

use yii\data\SqlDataProvider;


use common\models\ProdProductoGeneral;
use common\models\ProdProducto;
use common\models\ProdProductoAsociado;
use common\models\ProdClicks;
use common\models\ProdImpresiones;
use common\models\VistaProdVisitas;
use common\models\VistaClicks;




/**
 * HorarioController implements the CRUD actions for HHorario model.
 */
class ReportesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionDescargarzips()
    {
        return $this->render('descargarzips', [
        ]);
    }

    public function actionAdeudoproveedor()
    {   
        $prov = \common\models\ProvProveedor::find()->where('id_user='.Yii::$app->user->identity->id)->one();
//        $liquidaciones = \common\models\AdeudoProveedor::find()->where('id_proveedor='.$prov->id)->all();
        $query = 'SELECT  * FROM adeudo_proveedor as ap WHERE ap.id_proveedor='.$prov->id.' ORDER BY fecha_liquidacion DESC ';
         //echo $query2;

            
        $dataProvider = new SqlDataProvider([
                'sql' => $query,               
                'totalCount' => count(Yii::$app->db->createCommand($query)->queryAll()),
                'pagination' => [
                    'pageSize' => 50,
                    //'params' =>  unserialize($campos),
                ],
        ]);
            
        
        return $this->render('adeudoproveedor', [
//               'model' => $liquidaciones,
               'dataProvider' => $dataProvider//, 'searchModel' => $searchModel,
        ]); 
    }
    public function actionVisitasdetalle()
    {   
        $visitasTot=0;
        if (Yii::$app->user->isGuest) {   return $this->goHome();   }
        $id_proveedor2=null;         $model = new VistaProdVisitas(); 

        if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        
        if($rol=='ADMIN_MUSICALICA'){
            
            if(isset($id_proveedor2) && $id_proveedor2!='')  $model->id_proveedor2 = $id_proveedor2;
            if(isset($fecha_ini) && $fecha_ini!='') $model->fecha_ini = $fecha_ini;
            if(isset($fecha_ini) && $fecha_fin!='') $model->fecha_fin = $fecha_fin;
            
            //Si escogió Proveedor           
            $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                    
                    $fecha_ini = date('Y-m-d',strtotime($fecha_ini));
                    $fecha_fin = date('Y-m-d',strtotime($fecha_fin));
                    
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                        $query = ' SELECT *,DATE(v.fecha) as fecha, count(*) as total_visitas FROM prod_impresiones as v, prod_producto as p '
                        . ' WHERE p.id_proveedor='.$id_proveedor2.' and v.id_producto=p.id and v.fecha>="'.$fecha_ini.'" and v.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                        . ' GROUP BY DATE(v.fecha) ORDER BY v.fecha ASC';
            
                    }else{
                        $query = ' SELECT *,DATE(v.fecha) as fecha, count(*) as total_visitas FROM prod_impresiones as v, prod_producto as p '
                        . ' WHERE  v.id_producto=p.id and v.fecha>="'.$fecha_ini.'" and v.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                        . ' GROUP BY DATE(v.fecha) ORDER BY v.fecha ASC';
            
                    }
                    //echo $query;
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                        $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                        $visitas .= $datos_graf[$i]['total_visitas'].',';
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_visitas'];
                        }
                         $visitasTot = $visitasTot + $datos_graf[$i]['total_visitas'];
                    }
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                        $query2 = 'SELECT  *  '
                             . ' FROM vista_prod_visitas WHERE id_proveedor='.$id_proveedor2.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                             . ' ORDER BY fecha_visita DESC ';
                    }else{
                       $query2 = 'SELECT  *  '
                             . ' FROM vista_prod_visitas WHERE  fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                             . ' ORDER BY fecha_visita DESC ';
                    }
                   // echo $query2;exit;

                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            
            }
            else{
              if(isset($_POST['VistaProdVisitas']))  
                Yii::$app->session->setFlash('error', '¡Selecciona la Fecha Inicial y la Fecha Final!');
            } 
            
            $searchModel = new \backend\modules\configuracion\models\search\ProdProductoSearch();    
            return $this->render('visitas_detalle_admin', [
                'model' => $model,
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,   
                'id_proveedor' => $id_proveedor2,
                'datos_grilla' => $datos_grilla 

            ]);
        }else{ 
            //Perfil: PROVEEDOR
            $model = \common\models\ProvProveedor::find()->where('id_user='.Yii::$app->user->identity->id)->one();
            $id_proveedor = $model->id;

            $model = new VistaProdVisitas(); $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 

            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                     $model->fecha_ini = $fecha_ini;
                     $model->fecha_fin = $fecha_fin;
                    
                     $fecha_ini = date('Y-m-d',strtotime($fecha_ini));
                     $fecha_fin = date('Y-m-d',strtotime($fecha_fin));
                    
                    $query = ' SELECT *,DATE(v.fecha) as fecha, count(*) as total_visitas FROM prod_impresiones as v, prod_producto as p '
                    . ' WHERE p.id_proveedor='.$id_proveedor.' and v.id_producto=p.id and v.fecha>="'.$fecha_ini.'" and v.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                    . ' GROUP BY DATE(v.fecha) ORDER BY v.fecha ASC';
            

                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                        $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                        $visitas .= $datos_graf[$i]['total_visitas'].',';
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_visitas'];
                        }
                         $visitasTot = $visitasTot + $datos_graf[$i]['total_visitas'];
                    }

                    $query2 = 'SELECT  *  '
                             . ' FROM vista_prod_visitas WHERE id_proveedor='.$id_proveedor.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                             . ' ORDER BY fecha_visita DESC ';


                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            

            } 

            $searchModel = new \backend\modules\configuracion\models\search\ProdProductoSearch();
            //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('visitas_detalle', [
                'model' => $model,
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,   
                'id_proveedor' => $id_proveedor,
                'datos_grilla' => $datos_grilla

            ]);
        } 
        
        
            
        
    }
    
    
    public function actionVisitasprov()
    {
        //Utils::dump($_POST);
        $visitasTot=0;
        if (Yii::$app->user->isGuest) {   return $this->goHome();   }
        $id_proveedor2=null;         $model = new VistaProdVisitas(); 

        if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        
        if($rol=='ADMIN_MUSICALICA'){
            
            if(isset($id_proveedor2) && $id_proveedor2!='')  $model->id_proveedor2 = $id_proveedor2;
            if(isset($fecha_ini) && $fecha_ini!='') $model->fecha_ini = $fecha_ini;
            if(isset($fecha_ini) && $fecha_fin!='') $model->fecha_fin = $fecha_fin;
            
            //Si escogió Proveedor           
            $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                    
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                       
                        $query = ' SELECT DATE(fecha_visita) as fecha, count(*) as total_visitas '
                               . ' FROM vista_prod_visitas where id_proveedor='.$id_proveedor2.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                               . ' GROUP BY DATE(fecha_visita) ORDER BY fecha ASC ';
                    }else{
                         $query = ' SELECT DATE(fecha_visita) as fecha, count(*) as total_visitas '
                               . ' FROM vista_prod_visitas where fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                               . ' GROUP BY DATE(fecha_visita) ORDER BY fecha ASC ';
                    }
                   // echo $query;
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                        $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                        $visitas .= $datos_graf[$i]['total_visitas'].',';
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_visitas'];
                        }
                        $visitasTot = $visitasTot + $datos_graf[$i]['total_visitas'];
                    }
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                        $query2 = 'SELECT  *, count(*) as visitas '
                                . ' FROM vista_prod_visitas WHERE id_proveedor='.$id_proveedor2.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                                . 'GROUP BY id_producto ORDER BY visitas DESC ';
                    }else{
                        $query2 = 'SELECT  *, count(*) as visitas '
                                . 'FROM vista_prod_visitas WHERE  fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                                . 'GROUP BY id_producto ORDER BY visitas DESC ';
                    }
                    //echo $query2;

                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            
            }
            else{
              if(isset($_POST['VistaProdVisitas']))  
                Yii::$app->session->setFlash('error', '¡Selecciona la Fecha Inicial y la Fecha Final!');
            } 
            
            $searchModel = new \backend\modules\configuracion\models\search\ProdProductoSearch();    
            return $this->render('visitas_prov_admin', [
                'model' => $model,
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,      
                'id_proveedor' => $id_proveedor2,
                'datos_grilla' => $datos_grilla 

            ]);
            
            
        }else{  
           
            
            $model = \common\models\ProvProveedor::find()->where('id_user='.Yii::$app->user->identity->id)->one();
             if (isset($_POST['ProvProveedor']))  extract($_POST['ProvProveedor']); 
            
            
            $id_proveedor = $model->id;
        
            $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            

            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                    $model->fecha_ini = $fecha_ini;
                    $model->fecha_fin = $fecha_fin;
                    $query = ' SELECT DATE(fecha_visita) as fecha, count(*) as total_visitas '
                           . ' FROM vista_prod_visitas where id_proveedor='.$id_proveedor.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                           . ' GROUP BY DATE(fecha_visita) ORDER BY fecha ASC ';
                    //echo $query;
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                        $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                        $visitas .= $datos_graf[$i]['total_visitas'].',';
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_visitas'];
                        }
                        $visitasTot = $visitasTot + $datos_graf[$i]['total_visitas'];
                    }

                    $query2 = 'SELECT  *, count(*) as visitas '
                             . ' FROM vista_prod_visitas WHERE id_proveedor='.$id_proveedor.' and fecha_visita>="'.$fecha_ini.'" and fecha_visita<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                             . 'GROUP BY id_producto ORDER BY visitas DESC ';

                    //echo $query2;

                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            

            } 
            
            $searchModel = new \backend\modules\configuracion\models\search\ProdProductoSearch();
            //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('visitas_prov', [
                'model' => $model,
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,      
                'id_proveedor' => $id_proveedor,
                'datos_grilla' => $datos_grilla

            ]);
        }
        
    }
    
    public function actionClicksdetalle()
    {     //   Utils::dumpx($_POST);
        $visitasTot=0;
        if (Yii::$app->user->isGuest) {   return $this->goHome();   }
        $id_proveedor2=null;         $model = new VistaProdVisitas(); 

        if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        
        if($rol=='ADMIN_MUSICALICA'){
            
            if(isset($id_proveedor2) && $id_proveedor2!='')  $model->id_proveedor2 = $id_proveedor2;
            if(isset($fecha_ini) && $fecha_ini!='') $model->fecha_ini = $fecha_ini;
            if(isset($fecha_ini) && $fecha_fin!='') $model->fecha_fin = $fecha_fin;
            $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                     $fecha_ini = date('Y-m-d',strtotime($fecha_ini));
                     $fecha_fin = date('Y-m-d',strtotime($fecha_fin));
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                        $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                           . '  FROM vista_prod_clicks WHERE id_proveedor='.$id_proveedor2.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                           . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                    }else{
                        $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                           . '  FROM vista_prod_clicks WHERE fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                           . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                    }
                    
                   
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                            $visitas .= $datos_graf[$i]['total_clicks'].',';
                            $visitasTot = $visitasTot + $datos_graf[$i]['total_clicks'];
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_clicks'];
                            $visitasTot = $visitasTot + $datos_graf[$i]['total_clicks'];
                        }
                    }
                       
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                          $query2 = 'SELECT  *,c.fecha as fecha  '
                            . ' FROM prod_clicks as c, prod_producto as p '
                            . ' WHERE p.id=c.id_producto and p.id_proveedor='.$id_proveedor2.' and c.fecha >= "'.$fecha_ini.'" and c.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                            . ' ORDER BY c.fecha DESC ';
                    }else{
                         $query2 = 'SELECT  *,c.fecha as fecha  '
                            . ' FROM prod_clicks as c, prod_producto as p '
                            . ' WHERE p.id=c.id_producto and c.fecha >= "'.$fecha_ini.'" and c.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                            . ' ORDER BY c.fecha DESC ';
                    }
                    //echo $query2;
                     $fecha_ini = date('d-m-Y',strtotime($fecha_ini)); $fecha_fin = date('d-m-Y',strtotime($fecha_fin));
                    $model->fecha_ini = $fecha_ini;     $model->fecha_fin = $fecha_fin;
                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            
            }
            else{
              if(isset($_POST['VistaProdVisitas']))  
                Yii::$app->session->setFlash('error', '¡Selecciona la Fecha Inicial y la Fecha Final!');
            } 
            
            
            
            return $this->render('clicks_detalle_admin', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,                
                'id_proveedor' => $id_proveedor2,
                'datos_grilla' => $datos_grilla

            ]);
            
        }else{
        
         //Capturo el Proveedor
            if (Yii::$app->user->isGuest) {   return $this->goHome();   }

            $model = \common\models\ProvProveedor::find()->where('id_user='.Yii::$app->user->identity->id)->one();
            $id_proveedor = $model->id;

            $model = new VistaProdVisitas(); $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 

            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                    $model->fecha_ini = $fecha_ini;
                    $model->fecha_fin = $fecha_fin;
                     $fecha_ini = date('Y-m-d',strtotime($fecha_ini));
                     $fecha_fin = date('Y-m-d',strtotime($fecha_fin));
                    $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                           . '  FROM vista_prod_clicks WHERE id_proveedor='.$id_proveedor.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                           . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                    //echo $query; exit;
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                        if($i+1 < count($datos_graf)){
                        $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                        $visitas .= $datos_graf[$i]['total_clicks'].',';
                        }else{
                            $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                            $visitas .= $datos_graf[$i]['total_clicks'];
                        }
                         $visitasTot = $visitasTot + $datos_graf[$i]['total_clicks'];
                    }


                    $query2 = 'SELECT  *,c.fecha as fecha  '
                            . ' FROM prod_clicks as c, prod_producto as p '
                            . ' WHERE p.id=c.id_producto and p.id_proveedor='.$id_proveedor.' and c.fecha >= "'.$fecha_ini.'" and c.fecha<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                            . ' ORDER BY c.fecha DESC ';

                   // echo $query2; exit;

                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            

            } 
            
            return $this->render('clicks_detalle', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'fechas'=>$fechas,
                'visitas'=>$visitas,
                'visitasTot'=>$visitasTot,
                'id_proveedor' => $id_proveedor,
                'datos_grilla' => $datos_grilla

            ]);
        }
    }
    
    public function actionClicksprov()
    {
        
        if (Yii::$app->user->isGuest) {   return $this->goHome();   }
        $id_proveedor2=null;         $model = new VistaProdVisitas(); 

        if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        
        if($rol=='ADMIN_MUSICALICA'){
            
            if(isset($id_proveedor2) && $id_proveedor2!='')  $model->id_proveedor2 = $id_proveedor2;
            if(isset($fecha_ini) && $fecha_ini!='') $model->fecha_ini = $fecha_ini;
            if(isset($fecha_ini) && $fecha_fin!='') $model->fecha_fin = $fecha_fin;
             //Si escogió Proveedor           
            $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
            if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                    
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                       $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                              . '  FROM vista_prod_clicks WHERE id_proveedor='.$id_proveedor2.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                              . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                    }else{
                       $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                              . '  FROM vista_prod_clicks WHERE  fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                              . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                    }
            
                    $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                    //Obtengo los Arrays para el gráfico
                    for($i=0;$i<count($datos_graf);$i++){
                           if($i+1 < count($datos_graf)){
                           $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                           $visitas .= $datos_graf[$i]['total_clicks'].',';
                           }else{
                               $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                               $visitas .= $datos_graf[$i]['total_clicks'];
                           }
                    }
                       
                    if(isset($id_proveedor2) && $id_proveedor2!=''){
                          $query2 = 'SELECT  *, count(*) as clicks '
                                . '  FROM vista_prod_clicks where id_proveedor='.$id_proveedor2.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                                . 'GROUP BY id ORDER BY clicks DESC ';
                    }else{
                         $query2 = 'SELECT  *, count(*) as clicks '
                                . '  FROM vista_prod_clicks where fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                                . 'GROUP BY id ORDER BY clicks DESC ';
                    }
                    //echo $query2;

                    $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                    $dataProvider = new SqlDataProvider([
                            'sql' => $query2,               
                            'totalCount' => count($datos_grilla),
                            'pagination' => [
                                'pageSize' => 50,
                                //'params' =>  unserialize($campos),
                            ],
                    ]);
                }else{
                    Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                }            
            }
            else{
              if(isset($_POST['VistaProdVisitas']))  
                Yii::$app->session->setFlash('error', '¡Selecciona la Fecha Inicial y la Fecha Final!');
            } 
            
            $searchModel = new \backend\modules\configuracion\models\search\ProdProductoSearch();    
            return $this->render('clicks_prov_admin', [
                  'model' => $model,
                   'dataProvider' => $dataProvider,
                   'fechas'=>$fechas,
                   'visitas'=>$visitas,
                   'id_proveedor' => $id_proveedor2,
                   'datos_grilla' => $datos_grilla

            ]);
            
        }else{
              //ROL: PROVEEDOR
               if (Yii::$app->user->isGuest) {   return $this->goHome();   }

               $model = \common\models\ProvProveedor::find()->where('id_user='.Yii::$app->user->identity->id)->one();
               $id_proveedor = $model->id;

               $model = new VistaProdVisitas(); $dataProvider=null; $fechas='';$visitas='';$datos_grilla=null;
               if (isset($_POST['VistaProdVisitas'])) extract($_POST['VistaProdVisitas']); 

               if(isset($fecha_ini) && $fecha_ini!='' && isset($fecha_ini) && $fecha_fin!=''){

                   if(strtotime($fecha_ini) <= strtotime($fecha_fin)){
                       $model->fecha_ini = $fecha_ini;
                       $model->fecha_fin = $fecha_fin;
                       $query = 'SELECT DATE(fecha_click) as fecha, count(*) as total_clicks '
                              . '  FROM vista_prod_clicks WHERE id_proveedor='.$id_proveedor.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                              . ' GROUP BY DATE(fecha_click) ORDER BY fecha_click ASC ';
                       //echo $query; exit;
                       $datos_graf = Yii::$app->db->createCommand($query)->queryAll();

                       //Obtengo los Arrays para el gráfico
                       for($i=0;$i<count($datos_graf);$i++){
                           if($i+1 < count($datos_graf)){
                           $fechas .= '"'.$datos_graf[$i]['fecha'].'",';
                           $visitas .= $datos_graf[$i]['total_clicks'].',';
                           }else{
                               $fechas .= '"'.$datos_graf[$i]['fecha'].'"';
                               $visitas .= $datos_graf[$i]['total_clicks'];
                           }
                       }

                       $query2 = 'SELECT  *, count(*) as clicks '
                                . '  FROM vista_prod_clicks where id_proveedor='.$id_proveedor.' and fecha_click >= "'.$fecha_ini.'" and fecha_click<=DATE_ADD("'.$fecha_fin.'", INTERVAL 1 DAY) '
                                . 'GROUP BY id ORDER BY clicks DESC ';

                      // echo $query2; exit;

                       $datos_grilla = Yii::$app->db->createCommand($query2)->queryAll();
                       $dataProvider = new SqlDataProvider([
                               'sql' => $query2,               
                               'totalCount' => count($datos_grilla),
                               'pagination' => [
                                   'pageSize' => 50,
                                   //'params' =>  unserialize($campos),
                               ],
                       ]);
                   }else{
                       Yii::$app->session->setFlash('error', '¡La Fecha Inicial debe ser menor a la Fecha Final!');
                   }            

               } 



               return $this->render('clicks_prov', [
                   'model' => $model,
                   'dataProvider' => $dataProvider,
                   'fechas'=>$fechas,
                   'visitas'=>$visitas,
                   'id_proveedor' => $id_proveedor,
                   'datos_grilla' => $datos_grilla

               ]);
        }
    }
    
    
    public function actionReporte03(){
         
         $model = new Empleado(); $sql='';$sql2='';
         if (isset($_POST['Empleado']) || isset($_POST['Empleado']['id_localidad']) ) {
             if($_POST['Empleado']['id_localidad']!=''){ $sql = 'where v.id_localidad='.$_POST['Empleado']['id_localidad']; $sql2 = ' and v.id_localidad='.$_POST['Empleado']['id_localidad'];}
         }
         
         $localidades = ArrayHelper::map(Yii::$app->db->createCommand("SELECT v.id_localidad as id, v.territorio as nombre FROM v_beneficiarios_grupos as v group by v.id_localidad order by territorio;")->queryAll(), 'id', 'nombre');
         $disciplinas = Yii::$app->db->createCommand("SELECT d.nombre FROM v_beneficiarios_grupos as v, disciplina as d where d.id=v.id_disciplina  $sql2 group by d.id order by d.nombre;")->queryAll();
         $beneficiarios = Yii::$app->db->createCommand("SELECT v.documento_beneficiario, trim(v.beneficiario) as beneficiario FROM v_beneficiarios_grupos as v, beneficiario as b where b.documento=v.documento_beneficiario  $sql2 group by v.documento_beneficiario order by v.beneficiario;")->queryAll();
         $monitores = Yii::$app->db->createCommand("SELECT id_monitor,nombre_monitor FROM v_beneficiarios_grupos as v  $sql group by id_monitor;")->queryAll();
         $equipamentos = Yii::$app->db->createCommand("SELECT concat(equipamento,' / ',escenario,' / ',barrio,' / ',territorio) as equipamento FROM v_beneficiarios_grupos as v  $sql group by equipamento;")->queryAll();
         $barrios = Yii::$app->db->createCommand("SELECT concat(barrio,' / ',territorio) as barrio FROM v_beneficiarios_grupos as v $sql group by barrio;")->queryAll();
         //$localidades =  ArrayHelper::map(Localidad::find()->orderBy('id')->all(), 'id', 'nombre');
         
         return $this->render('reporte03', [
            'model' => $model,
            'localidades' => $localidades,
            'disciplinas' => $disciplinas,
            'beneficiarios' => $beneficiarios,
            'monitores' => $monitores,
            'equipamentos' => $equipamentos,
            'barrios' => $barrios,
           
            
        ]); 
    }
    
    
    public function getDia($dia){
        switch ($dia){
            case "Lunes": return 'lu';
            case "Martes": return 'ma';
            case "Miércoles": return 'mi';
            case "Jueves": return 'ju';
            case "Viernes": return 'vi';
            case "Sábado": return 'sa';
            case "Domingo": return 'do';
        }    
    }
}
