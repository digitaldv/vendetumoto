<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use kartik\widgets\DatePicker;
use yii\helpers\Url;

//common\components\Utils::dump($_POST);

?>
 
<div class="horarios-search"  >

<?php

//\common\components\Utils::dumpx($model->attributes);
$form = ActiveForm::begin([
            'id' => 'horarios_form',
                //'enableAjaxValidation'=>true,
                //'options'=>['enctype'=>'multipart/form-data'], // important
        ]);



 
?>
    
    <div class="box box-primary box-solid" style="border:1px solid #61363f">
            <div class="box-header with-border" style="background: #61363f;">
                <h3 class="box-title" style="color:#fff; font-size:13px">
                    <i  class='fa  fa-search'></i> &nbsp;Filtre por los siguientes campos:
                </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>
            
            </div>
            
            <div class="box-body" style="display: block; background: #ecf0f5 ">
               <div class="row">
         
        <div class="col-lg-2"  >
             
             <?= $form->field($model, 'fecha_ini')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Fecha...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                         'format' => 'dd-mm-yyyy'
                    ]
                ])->label('Fecha Inicial : ');
            ?>
             
        </div>
        <div class="col-lg-2" >
            <?= $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Fecha...'],
                'pluginOptions' => [
                    'autoclose'=>true,
                     'format' => 'dd-mm-yyyy'
                ]
            ])->label('Fecha Final : ');
            ?>
        </div>
                   
        <div class="col-lg-3" >
            <div class="form-group field-vistaprodvisitas-fecha_fin">
                <label class="control-label" for="vistaprodvisitas-fecha_fin">&nbsp;</label>
                <div class="">
                    <div class="form-group" data-toggle="tooltip" title="Consultar">
                        <?= Html::submitButton( 'Consultar', ['class' => 'btn btn-primary']) ?>
                    </div>  

            
                </div>
            </div>
            
        </div>
        
    </div>


            </div>
            
          </div>
   
<?php ActiveForm::end(); ?>

</div>
