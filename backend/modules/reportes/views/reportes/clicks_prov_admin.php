<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use common\models\Empleado;
use arturoliveira\ExcelView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Barrio;
use yii\web\JqueryAsset;
use yii\web\View;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;


$this->title = 'Clicks a Productos por Periodo';
$this->params['breadcrumbs'][] = $this->title;


//common\components\Utils::dump($datos_grilla);
if(isset($datos_grilla) && count($datos_grilla)>0){ 
    $this->registerJs("  
       var table = $('#datatables_w1').dataTable({
                order: [  [ 3, 'desc' ]  ],  
                lengthMenu: [[30, 50, 100, -1], [30, 50, 100, 'All']]
            });
    ", View::POS_END);
}


//common\components\Utils::dump($dataProvider);

?>
<style>

    .table{
        font-size:11px;
    }
    .table tr th{
        font-size:13px;
    }
     .table th{
        background: rgba(51, 122, 183, 0.15);
    }
    .table tbody tr td{
        padding: 0px;
    }
    .form-control{font-size:11px;  /*height: 28px;*/ color:blue;}
    
    
    .grid-view {
        width: 100%;
        overflow-x: auto;
        margin: 0 0 1em;
    }

</style>
<div class="empleado-index">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title" style="color:#337ab7">
                        <i class="fa  fa-hand-pointer-o"></i> <?= Html::encode($this->title) ?>
                    </h3>
                    
                </div>
                
                <div class="box-body">
                <?php  echo $this->render('_filtros_visitas_prov_admin',['model'=>$model]);  ?>

                    <div style="margin-bottom: 10px" class="pull-right">
                        <a href="<?= Url::to(['/reportes/reportes/visitasprov'])   ?>"
                           type="button" class="btn btn-default btn-xs">&nbsp;<i  class='fa  fa-search'></i> Limpiar campos </a>                           
                    </div>
                    
    <div class="wrapper1">
        <div class="div1"></div>
    </div>      
   <br>    <br>      
 <?php 
         if(isset($dataProvider) && isset($datos_grilla) && count($datos_grilla)>0){
             
             
            // common\components\Utils::dump($dataProvider);
//             common\components\Utils::dump($visitas);
            
        ?>   
    
            <div class="box box-primary">
                <div class="box-header with-border" >
                    <i class="fa fa-bar-chart-o"></i>

                    <h3 class="box-title">Periodo consultado : <em style="font-weight: normal">desde <b style="color:61363f"> <?= date('d-m-Y',strtotime($model->fecha_ini)) ?></b> hasta <b style="color:61363f"><?= date('d-m-Y',strtotime($model->fecha_fin)) ?></b></em></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                      
                    </div>
                </div>
                <div class="box-body">
                    <div class="row col-lg-12">
                        <?= Highcharts::widget([
                            'options'=>'{
                               "title": { "text": "Clicks a Productos por Periodo" },
                               "xAxis": {
                                  "categories": ['.$fechas.']
                               },
                               "yAxis": {
                                  "title": { "text": "Total de Clicks" }
                               },
                               "series": [
                                  { "name": "Clicks Productos", "data": ['.$visitas.'] }
                                  
                               ],                               
                               "credits" :  { "enabled" : false }
                            }'
                         ]);
                     ?>
                    </div>
                    <div class="col-lg-12">
                   <?php
                        
                         echo DataTables::widget([
                          'dataProvider' => $dataProvider,
                          'columns' => [
                                 //['contentOptions' => 'color:blue'],
                                  [
                                    'header' => 'Proveedor', 'format' => 'raw',
                                    'attribute' => 'id_proveedor',
                                    'value' => function ($data) {
                                        if (isset($data['id_proveedor']) && $data['id_proveedor'] != '') {
                                            $reg = \common\models\ProvProveedor::find()->where('id=' . $data['id_proveedor'])->one();
                                            return '&nbsp;&nbsp;&nbsp;'.$reg->nombre;
                                        } else
                                            return null;
                                    },
                                  ],     
                                  [
                                      'header' => 'Producto','format'=>'raw',
                                      'attribute' => 'id','contentOptions' =>['style'=>'padding-left:3px'],
                                      'value' => function ($data) {
                                          if(isset($data['title']) && $data['title']!=''){
                                             return Html::a( $data['title'], $data['link'], ['style' => 'color:blue;text-decoration:underline', 'target' => '_blank', 'title' => 'Abrir URL de la tienda Original.', ]);
                                          }else return  null;
                                      },
                                  ],
                                  [
                                      'header' => 'Categoría',
                                      'attribute' => 'id_categoria',
                                      'value' => function ($data) {
                                          if(isset($data['id_categoria']) && $data['id_categoria']!=''){
                                             $reg = \common\models\ProdCategoria::find()->where('id='.$data['id_categoria'])->one();
                                             return $reg->nombre;
                                          }else return  null;
                                      },
                                  ],
                                  [
                                      'header' => 'Marca',
                                      'attribute' => 'id_marca',
                                      'value' => function ($data) {
                                          if(isset($data['id_marca']) && $data['id_marca']!=''){
                                             $reg = \common\models\ProdMarca::find()->where('id='.$data['id_marca'])->one();
                                             return $reg->nombre;
                                          }else return  null;
                                      },
                                  ],
                                  [  
                                      'header' => 'Total de Clicks','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:16px;color:green'],
                                      'attribute' => 'clicks',
                                      'value' => function ($data) {
                                          if(isset($data['clicks']) && $data['clicks']!=''){
                                             return $data['clicks'];
                                          }else return  null;
                                      },
                                  ],             

                                          ],
                  //        'clientOptions' => [
                  //            "lengthMenu" => [[50,100,200, -1], [50,100,200, Yii::t('app', "Todos")]],
                  //            "responsive" => true,
                  //            "dom" => 'lfTrtip',
                  //            "tableTools" => [
                  //                "aButtons" => [
                  //                    ["sExtends" => "xls", "sButtonText"=> " <i style='color:green' class='fa fa-file-excel-o'></i> <b style='color:green'> Excel </b>", "oSelectorOpts" => ["page" => 'current'] ], 
                  //                   // ["sExtends"=> "pdf", "sButtonText"=> " <i class='fa fa-file-pdf-o' style='color:red'></i> <b style='color:red;font-size:13px'>PDF</b>" ],
                  //                ]
                  //            ]
                  //        ],
                    ]);
                   ?>
                    </div>
                </div>
            </div>
   
   
    





   <?php 
          }else{
              echo '<em class="col-lg-12 text-danger text-center text-wg">-- Sin Resultados --</em>';
          }
        ?>   
        
        
    
                </div>
            </div>
        </div>
    </div>
    
    
</div>
