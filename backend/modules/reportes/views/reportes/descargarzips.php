<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use yii\bootstrap\Modal;

$this->title = 'Backups diarios Visitas';
$this->params['breadcrumbs'][] = $this->title;
 
  //\common\components\Utils::dump($model->attributes);

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

    return /*round($bytes, $precision) . ' ' . */$units[$pow];
}

function isa_convert_bytes_to_specified($bytes, $to, $decimal_places = 1) {
    $formulas = array(
        'K' => number_format($bytes / 1024, $decimal_places),
        'M' => number_format($bytes / 1048576, $decimal_places),
        'G' => number_format($bytes / 1073741824, $decimal_places)
    );
    return isset($formulas[$to]) ? $formulas[$to] : 0;
}
?>

<div class="form-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <i class="fa  fa-file-zip-o"></i> <?= Html::encode($this->title) ?>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"><?= date('Y-m-d') ?></span>
                                <h5>Listado de backups diarios de Visitas</h5>
                            </div>
                            <div class="ibox-content">

                                <?php
                                $clientes = \common\models\CliCliente::find()->where('estado=1')->all();
                                foreach ($clientes as $cli) {
                                    $cli_carpeta = $cli->carpeta;
                                    echo"<b>Cliente :</b> <b style='color:blue'>".$cli->nombre."</b><hr><ul>";
                                    $carpeta_cliente =  dirname(__DIR__).'/../../../web/uploads/clientes/'.$cli_carpeta.'/backups_diarios/';
                                    //echo $carpeta_cliente.'<br>';
                                    $directorio = opendir($carpeta_cliente); //ruta actual
                                    while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
                                    {
                                        if (!is_dir($archivo))//verificamos si es o no un directorio
                                        {
                                            $tam = isa_convert_bytes_to_specified(filesize($carpeta_cliente.$archivo),'G');
                                            $unid = formatBytes(filesize($carpeta_cliente.$archivo),2);
                                            echo"<li><a class='text-navy' target='_self' href='".Yii::$app->request->baseUrl .'/uploads/clientes/'.$cli_carpeta.'/backups_diarios/'.$archivo."'>".$archivo."</a> - ".$tam.' ('.$unid.')'."</li>";
                                        }
                                    }
                                    echo "</ul>";
                                }
                                ?>
                            </div>
                        </div>

                    </div>

                    <p>

                    </p>
                </div>
            </div>
        </div>
    </div>
</div>



