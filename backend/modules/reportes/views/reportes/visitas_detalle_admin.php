<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use common\models\Empleado;
use arturoliveira\ExcelView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Barrio;
use yii\web\JqueryAsset;
use yii\web\View;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;


$this->title = 'Impresiones a Productos por Periodo';
$this->params['breadcrumbs'][] = $this->title;


//common\components\Utils::dump($datos_grilla);
if(isset($datos_grilla) && count($datos_grilla)>0){ 
    $this->registerJs("  
       var table = $('#datatables_w1').dataTable({
                order: [  [ 0, 'desc' ]  ],  
                lengthMenu: [[30, 50, 100, -1], [30, 50, 100, 'All']]
            });
    ", View::POS_END);
}


//common\components\Utils::dump($dataProvider);

?>
<style>

    .table{
        font-size:11px;
    }
    .table tr th{
        font-size:13px;
    }
     .table th{
        background: rgba(51, 122, 183, 0.15);
    }
    .table tbody tr td{
        padding: 0px;
    }
    .form-control{font-size:11px;  /*height: 28px;*/ color:blue;}
    
    
    .grid-view {
        width: 100%;
        overflow-x: auto;
        margin: 0 0 1em;
    }

</style>
<div class="empleado-index">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title" style="color:#337ab7">
                        <i class="fa fa-tasks"></i> <?= Html::encode($this->title) ?>
                    </h3>
                    
                </div>
                
                <div class="box-body">
                <?php  echo $this->render('_filtros_visitas_prov_detalle_admin',['model'=>$model]);  ?>

                    <div style="margin-bottom: 10px" class="pull-right">
                        <a href="<?= Url::to(['/reportes/reportes/visitasdetalle'])   ?>"
                           type="button" class="btn btn-default btn-xs">&nbsp;<i  class='fa  fa-search'></i> Limpiar campos </a>                           
                    </div>
                    
    <div class="wrapper1">
        <div class="div1"></div>
    </div>      
   <br>    <br>      
 <?php 
         if(isset($dataProvider) && isset($datos_grilla) && count($datos_grilla)>0){
            
        ?>   
    
            <div class="box box-primary">
                <div class="box-header with-border" >
                    <i class="fa fa-bar-chart-o"></i>

                    <h3 class="box-title">Periodo consultado : <em style="font-weight: normal">desde <b style="color:61363f"> <?= date('d-m-Y',strtotime($model->fecha_ini)) ?></b> hasta <b style="color:61363f"><?= date('d-m-Y',strtotime($model->fecha_fin)) ?></b></em></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
                        <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="row col-lg-12 text-center">
                         <h2 style="color:green">Total de impresiones en el periodo seleccionado : <b style="color:blue"> <?= $visitasTot ?></b></h2>
                        <hr>
                        <?php /* = Highcharts::widget([
                            'options'=>'{
                               "title": { "text": "Visitas a Productos por Periodo" },
                               "xAxis": {
                                  "categories": ['.$fechas.']
                               },
                               "yAxis": {
                                  "title": { "text": "Total de Visitas" }
                               },
                               "series": [
                                  { "name": "Visitas Productos", "data": ['.$visitas.'] }
                                ],
                               "credits" :  { "enabled" : false }
                                 
                            }',
                            
                         ]);*/
                     ?>
                    </div>
                    <div class="col-lg-12">
                   <?php
                        
                         echo DataTables::widget([
                          'dataProvider' => $dataProvider,
                          'columns' => [
                                 //['contentOptions' => 'color:blue'],

                               [  
                                      'header' => 'Fecha Visita','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:13px;color:green'],
                                      'attribute' => 'fecha_visita',
                                      'value' => function ($data) {
                                          if(isset($data['fecha_visita']) && $data['fecha_visita']!=''){
                                             return date('d-m-Y H:i',strtotime($data['fecha_visita']));
                                          }else return  null;
                                      },
                                  ],  
                                 [
                                    'header' => 'Proveedor', 'format' => 'raw',
                                    'attribute' => 'id_proveedor',
                                    'value' => function ($data) {
                                        if (isset($data['id_proveedor']) && $data['id_proveedor'] != '') {
                                            $reg = \common\models\ProvProveedor::find()->where('id=' . $data['id_proveedor'])->one();
                                            return '&nbsp;&nbsp;&nbsp;'.$reg->nombre;
                                        } else
                                            return null;
                                    },
                                ],
                                  [
                                      'header' => 'Producto','format'=>'raw',
                                      'attribute' => 'id_producto','contentOptions' =>['style'=>'padding-left:3px'],
                                      'value' => function ($data) {
                                           // echo $data['id_producto']; exit;
                                          if(isset($data['title']) && $data['title']!=''){
                                            // return ' '.$data['title']; //else return  null;
                                            $prodgen = \common\models\ProdProducto::find()->where('id=' . $data['id_producto'])->one();    
                                            return Html::a( $data['title'],$prodgen->link, ['style' => 'color:blue;text-decoration:underline', 'target' => '_blank', 'title' => 'Abrir URL Original del Producto en tienda original', ]);
                                             
//                                            $prodAsoc =  common\models\ProdProductoAsociado::find()->where('id_prod_producto=' . $data['id_producto'])->one();
//                                            if($prodAsoc){
//                                                $prodgen = \common\models\ProdProductoGeneral::find()->where('id=' . $prodAsoc->id_prod_general)->one();    
//
//                                                 return Html::a( $data['title'], Url::base(true).'/../../producto/'.$prodgen->url_amigable, ['style' => 'color:blue;text-decoration:underline', 'target' => '_blank', 'title' => 'Abrir Perfil Producto en Musicalica.', ]);
//                                            }
//                                            return $data['title'];
                                          }else return  null;
                                      },
                                  ],
                                  [
                                      'header' => 'Categoría',
                                      'attribute' => 'id_categoria',
                                      'value' => function ($data) {
                                          if(isset($data['id_categoria']) && $data['id_categoria']!=''){
                                             $reg = \common\models\ProdCategoria::find()->where('id='.$data['id_categoria'])->one();
                                             return $reg->nombre;
                                          }else return  null;
                                      },
                                  ],
                                  [
                                      'header' => 'Marca',
                                      'attribute' => 'id_marca',
                                      'value' => function ($data) {
                                          if(isset($data['id_marca']) && $data['id_marca']!=''){
                                             $reg = \common\models\ProdMarca::find()->where('id='.$data['id_marca'])->one();
                                             return $reg->nombre;
                                          }else return  null;
                                      },
                                  ],
                                  [  
                                      'header' => 'IP','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:12px;color:green'],
                                      'attribute' => 'ip',
                                      'value' => function ($data) {
                                          if(isset($data['ip']) && $data['ip']!=''){
                                             return $data['ip'];
                                          }else return  null;
                                      },
                                  ],  
                                  [  
                                      'header' => 'Ciudad','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:12px;color:green'],
                                      'attribute' => 'ciudad',
                                      'value' => function ($data) {
                                          if(isset($data['ciudad']) && $data['ciudad']!=''){
                                             return common\components\Utils::limpiarCadena2($data['ciudad']);
                                          }else return  null;
                                      },
                                  ],  
                                 
                                   [  
                                      'header' => 'Usuario','contentOptions'=>['style'=>'text-align:center;font-size:12px; '],
                                      'attribute' => 'id_user','format'=>'raw',
                                      'value' => function ($data) {
                                          if(isset($data['id_user']) && $data['id_user']!=''){
                                             $rol = common\components\Utils::getRol($data['id_user']);
                                             if($rol=="CLIENTE"){
                                                 $reg = \common\models\CliCliente::find()->where('id_user='.$data['id_user'])->one();  
                                                 return $reg->email.' - <em style="color:gray">(cliente)</em>';
                                             }else
                                                 if($rol=="PROVEEDOR"){
                                                     $reg = \common\models\ProvProveedor::find()->where('id_user='.$data['id_user'])->one();  
                                                     return $reg->email.' - <em style="color:gray">(proveedor)</em>';
                                                 }
                                             
                                          }else return  '<span class="not-set">(usuario invitado)</span>';
                                      },
                                  ],  
                                   [  
                                      'header' => 'Impresiones','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:16px;color:green'],
                                      'attribute' => 'impresiones',
                                      'value' => function ($data) {
                                          //if(isset($data['visitas']) && $data['visitas']!=''){
                                             return 1;
                                          //}else return  null;
                                      },
                                  ],      
//                                  [  
//                                      'header' => 'Total de Visitas','contentOptions'=>['style'=>'font-weight:bold;text-align:center;font-size:16px;color:green'],
//                                      'attribute' => 'visitas',
//                                      'value' => function ($data) {
//                                          if(isset($data['visitas']) && $data['visitas']!=''){
//                                             return $data['visitas'];
//                                          }else return  null;
//                                      },
//                                  ],             

                                          ],
                          'clientOptions' => [
                            "lengthMenu"=> [[20,50,100,-1], [20,50,100,"Todos"]],
                            "info"=>true,
                            "responsive"=>true, 
                            "dom"=> 'lfTrtip',
                            "tableTools"=>[
                                "aButtons"=> [  
                                    ["sExtends"=> "xls", "oSelectorOpts"=> ["page"=> 'Exportar Excel (CSV)'], "sButtonText"=> " <i style='color:green' class='fa fa-file-excel-o'></i> Exportar Excel"  ],
                                    //["sExtends"=> "pdf", "sButtonText"=> " <i class='fa fa-file-pdf-o' style='color:red'></i> Exportar PDF" ],
                                    //["sExtends"=> "print", "sButtonText"=> "Imprimir" ],
                                ]
                            ]
                        ],
                    ]);
                   ?>
                    </div>
                </div>
            </div>
   
   
    





   <?php 
          }else{
              echo '<em class="col-lg-12 text-danger text-center text-wg">-- Sin Resultados --</em>';
          }
        ?>   
        
        
    
                </div>
            </div>
        </div>
    </div>
    
    
</div>
