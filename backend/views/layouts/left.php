<?php

use webvimark\modules\UserManagement\components\GhostMenu;

$rol = \common\components\Utils::getRol(Yii::$app->user->identity->id);
echo $rol;
?>
 
<aside class="main-sidebar">
    <section class="sidebar">

   <?php if($rol=='SUPERADMIN') {

       echo GhostMenu::widget(
           [
               'encodeLabels' => false,
               'activateParents' => true,
               'options' => ['class' => 'sidebar-menu'],
               'items' => [
                   ['label' => 'Menú Principal', 'options' => ['class' => 'header']],
                   [
                       'label' => "UsuariosEEEE",
                       'icon' => 'fa fa-users',
                       'options' => ['class' => Yii::$app->controller->module->id == 'user-management' ? 'site-menu-item has-sub active open' : 'site-menu-item has-sub'],
                       'url' => ['/user-management/user/index'],
                       'items' => [
                           ['label' => 'Usuarios', 'icon' => 'fa fa-user',
                               'options' => ['class' => Yii::$app->controller->id == 'user' ? 'site-menu-item active' : 'site-menu-item'],
                               'url' => ['/user-management/user/index']
                           ],
                           ['label' => 'Roles', 'icon' => 'fa fa-gear',
                               'options' => ['class' => Yii::$app->controller->id == 'role' ? 'site-menu-item active' : 'site-menu-item'],
                               'url' => ['/user-management/role/index']
                           ],
                           ['label' => 'Permisos', 'icon' => 'fa fa-angle-double-right',
                               'options' => ['class' => Yii::$app->controller->id == 'permission' ? 'site-menu-item active' : 'site-menu-item'],
                               'url' => ['/user-management/permission/index']
                           ],
                           ['label' => 'Grupos de Permisos', 'icon' => 'fa fa-angle-double-right',
                               'options' => ['class' => Yii::$app->controller->id == 'auth-item-group' ? 'site-menu-item active' : 'site-menu-item'],
                               'url' => ['/user-management/auth-item-group/index']
                           ],
                           ['label' => 'Registro de Ingresos', 'icon' => 'fa fa-angle-double-right',
                               'options' => ['class' => Yii::$app->controller->id == 'user-visit-log' ? 'site-menu-item active' : 'site-menu-item'],
                               'url' => ['/user-management/user-visit-log/index']
                           ],
                       ]
                   ],

                   ['label' => 'Configuración', 'icon' => 'fa fa-gear', 'url' => ['javascript:void(0)'],
                       'items' => [
                           ['label' => 'Empleados', 'icon' => 'fa fa-angle-double-right', 'options' => ['class' => Yii::$app->controller->id == (Yii::$app->controller->module->id == 'configuracion' && Yii::$app->controller->id == 'empleado') ? 'site-menu-item active' : 'site-menu-item'], 'url' => ['/configuracion/empleado/index']],
                       ],
                       ['label' => 'Empleados', 'icon' => 'fa fa-user-plus', 'url' => ['/configuracion/empleado/index'],
                           'items' => [

                           ],
                       ],


                       ['label' => 'Generador Gii',
                           'icon' => 'fa fa-share',
                           'url' => ['/user-management/user/index'],
                           'items' => [
                               ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                           ],
                       ],
                   ],
               ],
           ]
       );
   }

   ?>

    </section>

</aside>
 