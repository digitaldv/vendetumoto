<?php

use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\Url;
use common\components\Utils;

$rol = Utils::getRol(Yii::$app->user->identity->id);

/*
Utils::dump(Yii::$app->controller->id); //clihuella
Utils::dump(Yii::$app->controller->module->id); //huellas
Utils::dump(Yii::$app->controller->module->module->controller->module->module->controller->action->id); //index
*/

?>

<nav class="navbar-default navbar-static-side" role="navigation">
 

<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">

        <li class="nav-header">
            <div class="dropdown profile-element">

                <img title="Ir a home" style="cursor: pointer" onclick="window.location.href = '<?=Url::to(["/site/index"])?>';" alt="Ir a home" class="rounded-circle text-center" src="img/logo.png" width="100%">


                <!-- data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="block m-t-xs font-bold" style="margin-top: 20px">&nbsp;</span>
                    <span class="text-muted text-xs block"><?= $rol ?> <b class="caret"></b></span>
                </>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <?php if($rol=="SUPERADMIN") { ?>
                        <li><a class="dropdown-item" href="#">Perfil</a></li>
                        <li class="dropdown-divider"></li>
                    <?php } ?>
                    <li><a class="dropdown-item" href="<?=Url::to(['/site/logout'])?>" data-method="post">Salir</a></li>
                </ul-->

            </div>
            <div class="logo-element" style="padding: 5px">
                <img  title="Ir a home" style="cursor: pointer" onclick="window.location.href = '<?=Url::to(["/site/index"])?>';" alt="Ir a home" class="rounded-circle" src="img/logo.png" width="100%">
            </div>
        </li>


<?php if($rol=="SUPERADMIN") { ?>
    <li class="<?= (Yii::$app->controller->id == 'user' || Yii::$app->controller->id == 'role' || Yii::$app->controller->id == 'permission' || Yii::$app->controller->id == 'auth-item-group' || Yii::$app->controller->id == 'user-visit-log') ? ' active' : '' ?>">
            <a href="#">
                <i class="fa fa-th-large"></i>
                <span class="nav-label">Superadmin</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level"  >
                <li class="<?= (Yii::$app->controller->id == 'user') ? ' active' : '' ?>"><a href="<?= Url::to(['/user-management/user/index'])?>">Usuarios</a></li>
                <li class="<?= (Yii::$app->controller->id == 'role') ? ' active' : '' ?>"><a href="<?= Url::to(['/user-management/role/index'])?>">Roles</a></li>
                <li class="<?= (Yii::$app->controller->id == 'permission') ? ' active' : '' ?>"><a href="<?= Url::to(['/user-management/permission/index'])?>">Permisos</a></li>
                <li class="<?= (Yii::$app->controller->id == 'auth-item-group') ? ' active' : '' ?>"><a href="<?= Url::to(['/user-management/auth-item-group/index'])?>">Grupos Permisos</a></li>
                <li class="<?= (Yii::$app->controller->id == 'user-visit-log') ? ' active' : '' ?>"><a href="<?= Url::to(['/user-management/user-visit-log/index'])?>">Visitas Log</a></li>
                <li><a target="_blank" href="<?= Url::to(['/gii'])?>">Generador Gii</a></li>
            </ul>
    </li>
<?php }
else
    if($rol=="ADMIN_VTM" || $rol=="SUPERADMIN"  ) {
    ?>

        <li class="<?= (Yii::$app->controller->id == 'cliconsulta') ? ' active' : '' ?>">
            <a href="<?= Url::to(['/site/index'])?>">
                <i class="fa fa-home"></i> <span class="nav-label"> Dashboard</span>
            </a>
        </li>
        <li class="<?= (Yii::$app->controller->id == 'country' || Yii::$app->controller->id == 'state' || Yii::$app->controller->id == 'city') ? ' active' : ''   ?>">
            <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Ubicación</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'country') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/country/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Paises</a>
                </li>
            </ul>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'state') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/state/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Departamentos</a>
                </li>
            </ul>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'city') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/city/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Ciudades</a>
                </li>
            </ul>
        </li>
        <li class="<?= (Yii::$app->controller->id == 'mottipo' || Yii::$app->controller->id == 'motmarca' || Yii::$app->controller->id == 'motmodelo' || Yii::$app->controller->id == 'motcaracteristicas') ? ' active' : ''   ?>">
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Configuración Motos</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'mottipo') ? ' active' : '' ?>">
                    <a href="<?= Url::to(['/configuracion/mottipo/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Tipos</a>
                </li>
            </ul>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'motmarca') ? ' active' : '' ?>">
                    <a href="<?= Url::to(['/configuracion/motmarca/index'])?>">
                      <i class="fa fa-angle-double-right"></i> Marcas</a>
                </li>
            </ul>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'motmodelo') ? ' active' : '' ?>">
                    <a href="<?= Url::to(['/configuracion/motmodelo/index'])?>">
                         <i class="fa fa-angle-double-right"></i> Modelos</a>
                </li>
            </ul>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'motcaracteristicas') ? ' active' : '' ?>">
                    <a href="<?= Url::to(['/configuracion/motcaracteristicas/index'])?>">
                         <i class="fa fa-angle-double-right"></i> Otras Características
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?= (Yii::$app->controller->id == 'empleado') ? ' active' : '' ?>">
            <a href="<?= Url::to(['/configuracion/empleado/index'])?>">
                   <i class="fa fa-user-circle-o"></i> <span class="nav-label"> Empleados VTM</span>
            </a>
        </li>
        <li class="<?= (Yii::$app->controller->id == 'paquete') ? ' active' : '' ?>">
            <a href="<?= Url::to(['/configuracion/paquete/index'])?>">
               <i class="fa fa-dollar"></i> <span class="nav-label"> Planes</span>
            </a>

        </li>
        <li class="<?= (Yii::$app->controller->id == 'paginasinternas') ? ' active' : '' ?>">
               <a href="<?= Url::to(['/configuracion/paginasinternas/index'])?>">
                 <i class="fa fa-files-o"></i> <span class="nav-label">Páginas Internas</span>
               </a>

        </li>
        <li class="<?= (Yii::$app->controller->id == 'artcategoria' || Yii::$app->controller->id == 'artarticulo') ? ' active' : ''   ?>">
            <a href="#"><i class="fa fa-rss-square"></i> <span class="nav-label">Blog</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'artcategoria') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/artcategoria/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Categorías</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'artarticulo') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/artarticulo/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Artículos</a>
                </li>
            </ul>
       </li>
        <li class="<?= (Yii::$app->controller->id == 'adsclientes' || Yii::$app->controller->id == 'adspublicidad') ? ' active' : ''   ?>">
            <a href="#"><i class="fa fa-buysellads"></i> <span class="nav-label">Ads (Publicidad)</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'adsclientes') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/adsclientes/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Clientes</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'adspublicidad') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/adspublicidad/index'])?>">
                        <i class="fa fa-angle-double-right"></i> Publicidades</a>
                </li>
            </ul>
        </li>
        <li class="<?= (Yii::$app->controller->id == 'cliusuario' || Yii::$app->controller->id == 'pubpublicacion' || Yii::$app->controller->id == 'pubmensaje' ||
                        Yii::$app->controller->id == 'suscripciones' || Yii::$app->controller->id == 'contactenos' || Yii::$app->controller->id == 'pubdenuncia') ? ' active' : ''   ?>">
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reportes</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse" aria-eanded="false">
                <li class="<?= (Yii::$app->controller->id == 'cliusuario') ? ' active' : '' ?>"><a href="<?= Url::to(['/clientes/cliusuario/index'])?>">
                        <i class="fa fa-user"></i> Usuarios Registrados</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'pubpublicacion') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/pubpublicacion/index']) ?>">
                        <i class="fa fa-bullhorn"></i> Gestión de publicaciones </a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'pubmensaje') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/pubmensaje/index'])?>">
                        <i class="fa fa-envelope"></i> Mensajes a publicaciones</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'pubdenuncia') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/pubdenuncia/index'])?>">
                        <i class="fa fa-ban"></i> Denuncias a publicaciones</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'suscripciones') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/suscripciones/index'])?>">
                        <i class="fa fa-star"></i> Suscriptores</a>
                </li>
                <li class="<?= (Yii::$app->controller->id == 'contactenos') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/contactenos/index'])?>">
                        <i class="fa fa-comment"></i> Contactos</a>
                </li>

            </ul>
        </li>
<?php
} elseif($rol=="CLI_ADMIN") {
        echo "----";

    }
/*        <li class="<?= (Yii::$app->controller->id == 'empleado' ) ? ' active' : ''   ?>">
    <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Configuración</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse" aria-eanded="false">
        <li class="<?= (Yii::$app->controller->id == 'empleado') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/empleado/index'])?>">
                <i class="fa fa-angle-double-right"></i> Empleados HB </a></li>

    </ul>
    </li>
 <!--li class="<?= (Yii::$app->controller->id == 'clicliente') ? ' active' : '' ?>"><a href="<?= Url::to(['/clientes/clicliente/index'])?>">
                    <i class="fa fa-angle-double-right"></i> Clientes</a></li-->
*/
?>


</nav>    
