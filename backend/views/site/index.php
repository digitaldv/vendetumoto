<?php

/* @var $this yii\web\View */

use yii\helpers\ArrayHelper;
use yii\web\View;

$rol = \common\components\Utils::getRol(Yii::$app->user->identity->id);
$this->title = 'Bienvenido a Vende tu Moto <em style="font-size: 16px;color:green">('.$rol.')</em>';



?>
<div class="site-index">

    <?php
      if($rol=="ADMIN_MM" || $rol=="SUPERADMIN"){

          $usuarios   = \common\models\CliUsuario::find()->count();
          $usuarios_act   = \common\models\CliUsuario::find()->where('estado=1')->count();

          $planes     = 1; //  \common\models\Plan::find()->count();
          $planes_act = 1; // \common\models\Plan::find()->where('estado=1')->count();

          $consultas  =  1; // \common\models\CliConsulta::find()->count();

          $articulos  = \common\models\ArtArticulo::find()->count();
          $articulos_act  = \common\models\ArtArticulo::find()->where('estado=1')->count();

          $visitas        = Yii::$app->db->createCommand("SELECT sum(visitas) FROM `art_articulo`")->queryScalar();
          $visitas_act    = Yii::$app->db->createCommand("SELECT sum(visitas) FROM `art_articulo` WHERE estado=1")->queryScalar();


    ?>

    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?= \yii\helpers\Url::to(['/clientes/cliusuario/index']) ?>">
                        <span class="label label-success pull-right">ver usuarios</span>
                    </a>
                    <h5>Usuarios Registrados</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $usuarios_act ?></h1>
                    <div class="stat-percent font-bold text-success"><?= $usuarios ?> totales</div>
                    <small>Total activos</small>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <a href="<?= \yii\helpers\Url::to(['/clientes/cliusuario/indexconsulta']) ?>">
                        <span class="label label-info pull-right">ver consultas</span>
                    </a>
                    <h5>Consultas realizadas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $consultas ?></h1>
                    <div class="stat-percent font-bold text-info"><?= $consultas ?> totales</div>
                    <small>Total activos</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <a href="<?= \yii\helpers\Url::to(['/configuracion/artarticulo/index']) ?>">
                        <span class="label label-success pull-right">ver artículos</span>
                    </a>
                    <h5>Artículos</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $articulos_act ?> <span style="font-size: 13px">Activos</span></h1>
                    <div class="stat-percent font-bold text-success"><?= $visitas ?> visitas</div>

                    <small><?= $articulos ?> Totales</small>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?= \yii\helpers\Url::to(['/configuracion/plan/index']) ?>">
                        <span class="label label-info pull-right">ver planes</span>
                    </a>
                    <h5>Planes</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $planes_act ?></h1>
                    <div class="stat-percent font-bold text-navy"><?= $planes ?> totales</div>
                    <small>Total activos</small>
                </div>
            </div>
        </div>



    </div>
    <?php
    }elseif($rol=="CLI_ADMIN"){


          $usuarios=$visitas=$visitas_hoy=1;

    ?>
          <div class="row">
              <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <span class="label label-success pull-right">ver clientes</span>
                          <h5>Clientes Predio</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins"><?= $usuarios ?></h1>
                          <div class="stat-percent font-bold text-success"><?= $usuarios ?> totales</div>
                          <small>Total activos</small>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <span class="label label-info pull-right">ver visitas</span>
                          <h5>Visitas Reactivación</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins"><?= $visitas ?></h1>
                          <div class="stat-percent font-bold text-info"><?= $visitas ?> hoy</div>
                          <small>Total </small>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <span class="label label-primary pull-right">ver visitas</span>
                          <h5>Visitas Sensibilización</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins"><?= $visitas ?></h1>
                          <div class="stat-percent font-bold text-navy"><?= $visitas ?> hoy</div>
                          <small>Total </small>
                      </div>
                  </div>
              </div>

          </div>

<?php
    }

?>

</div>
    <!--div class="row">

        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>SIRA (tiempo real)</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>



                    </div>
                </div>
                <div class="ibox-content ibox-heading">
                    <h3>¡Últimas visitas registradas!</h3>
                    <small><i class="fa fa-map-marker"></i> Listado de las últimas visitas realizadas por los gestores.</small>
                </div>
                <div class="ibox-content inspinia-timeline">

                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-briefcase"></i>
                                6:00 am
                                <br>
                                <small class="text-navy">hace 2 horas</small>
                            </div>
                            <div class="col-xs-7 content no-top-border">
                                <p class="m-b-xs"><strong>Gestor: Carolina Mendieta (Cali - Valle)</strong></p>

                                <p>
                                    ID : 122 <br>
                                    Cliente : Andrés Benavides Calambraio <br>
                                    Dirección : Andrés Benavides Calambraio <br>
                                    Tipo Residencia : Unidad <br>
                                </p>


                            </div>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-briefcase"></i>
                                6:00 am
                                <br>
                                <small class="text-navy">hace 2 horas</small>
                            </div>
                            <div class="col-xs-7 content no-top-border">
                                <p class="m-b-xs"><strong>Gestor: Carolina Mendieta (Cali - Valle)</strong></p>

                                <p>
                                    ID : 122 <br>
                                    Cliente : Andrés Benavides Calambraio <br>
                                    Dirección : Andrés Benavides Calambraio <br>
                                    Tipo Residencia : Unidad <br>
                                </p>


                            </div>
                        </div>
                    </div>
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-briefcase"></i>
                                6:00 am
                                <br>
                                <small class="text-navy">hace 2 horas</small>
                            </div>
                            <div class="col-xs-7 content no-top-border">
                                <p class="m-b-xs"><strong>Gestor: Carolina Mendieta (Cali - Valle)</strong></p>

                                <p>
                                    ID : 122 <br>
                                    Cliente : Andrés Benavides Calambraio <br>
                                    Dirección : Andrés Benavides Calambraio <br>
                                    Tipo Residencia : Unidad <br>
                                </p>


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div-->



</div>
