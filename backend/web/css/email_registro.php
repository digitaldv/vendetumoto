<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php require('header.php'); ?>

<table style="width:100%;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td height="16px" style="Margin:0;border-collapse:collapse!important;color:#969696;font-family:Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <br />
                ¡Bienvenido a <a href="<?= Url::base(true) ?>">gestionemos.com</a>!

                <div   style="font-size:12px">
                    <br />Sus datos de registro son:</div>
                <br>
                <div  style="font-size:12px">
                    <b style="color:#814652">Usuario : </b> <?= $username ?> 
                </div>      
                <br><br>
                <hr>
            </td>
        </tr>
    </tbody>
</table>

<?php require('footer.php'); ?>