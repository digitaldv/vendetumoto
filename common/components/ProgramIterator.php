<?php

namespace common\components;

use Yii;

class ProgramIterator implements \Iterator
{
    /**
     * XML file path
     *
     * @var string
     */
    protected $strFile = null;
 
    /**
     * XML reader object
     *
     * @var \XMLReader
     */
    protected $reader = null;
 
    /**
     * Current program
     *
     * @var array
     */
    protected $program = null;
 
    /**
     * Dummy-key for iteration.
     * Has no real value except maybe act as a counter
     *
     * @var integer
     */
    protected $nKey = null;
 
    public $strObjectTagname = '';
 
 
 
    function __construct($strFile)
    {
        $this->strFile = $strFile;
    }
 
    public function current() {
        return $this->program;
    }
 
    public function key() {
        return $this->nKey;
    }
 
    public function next() {
        $this->program = null;
    }
 
    public function rewind() {
        $this->reader = new \XMLReader();
        $this->reader->open($this->strFile);
        $this->program = null;
        $this->nKey    = null;
    }
 
    public function valid() {
        if ($this->program === null) {
            $this->loadNext();
        }
 
        return $this->program !== null;
    }
 
    /**
     * Loads the next program
     *
     * @return void
     */
    protected function loadNext()
    {
        $strElementName = null;  $bCaptureValues = false;  $arValues = array(); $arNesting  = array();
 
        while ($this->reader->read()) {
            switch ($this->reader->nodeType) {
                case \XMLReader::ELEMENT:
                    $strElementName = $this->reader->name;
                    if ($bCaptureValues) {
                        if ($this->reader->isEmptyElement) {
                            $arValues[$strElementName] = null;
                        } else {
                            $arNesting[] = $strElementName;  $arValues[implode('-', $arNesting)] = null;
                        }
                    }

                    if ($strElementName == 'entry') {  $bCaptureValues = true;  }
                    if ($strElementName == 'item') {   $bCaptureValues = true;  }
                    break;
 
                case \XMLReader::TEXT:
                    if ($bCaptureValues) {  $arValues[implode('-', $arNesting)] = trim($this->reader->value);  }   break;
 
                case \XMLReader::CDATA:
                    if ($bCaptureValues) {  $arValues[implode('-', $arNesting)] = trim($this->reader->value);  }
                    break;
 
                case \XMLReader::END_ELEMENT:
                    //if ($this->reader->name == $this->strObjectTagname) {
                    if ($this->reader->name == 'entry') {   
                        $this->program = $arValues;
                        ++$this->nKey;
                        break 2;
                    }
                    
                    if ($this->reader->name == 'item') {   
                        $this->program = $arValues;
                        ++$this->nKey;
                        break 2;
                    }
                    
                    if ($bCaptureValues) {
                        array_pop($arNesting);
                    }
                    break;
            }
        }
    }
}