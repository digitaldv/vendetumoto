<?php

namespace common\components;

use Yii;
use common\models\CliUsuario;

use common\models\ProcProceso;
use common\models\User;
use common\models\ProdComentarios;
require(__DIR__ .'/../../vendor/bower-asset/datatables/examples/server_side/scripts/ssp.class.php' );

class Utils
{
    
    static function getProcesosDeptosMapa()
    {   
        $datos=[]; $cad="";$desc='';$total_depto=0;
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        if($rol=='DIRECTOR_JURIDICO'){
            $cliente = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
            
            //**************************************************************************************
            //Sólo los de BOGOTÁ DC
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='CO-DC') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
            $resBog = Yii::$app->db->createCommand($sql)->queryOne();
            $total_bog = isset($resBog['total']) ? $resBog['total']:0;
            $cad .= ' { title:"BOGOTÁ DC : '.$total_bog.'", id: "CO-DC", description: "'.'<b>BOGOTÁ DC : '.$total_bog.'</b>'.'", value: '.$total_bog.'}, ';
            
            //**************************************************************************************
            //Sólo los de CUNDINAMARCA
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
            $resCun = Yii::$app->db->createCommand($sql)->queryOne();
            $total_cund = 0;
            
            //Obtenemos las Ciudades del Depto
            $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
            $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
            //Utils::dumpx($resCiud);
            $desc .='<br>';
            foreach($resCiud as $depto){
                 $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
                 $total_cund = $total_cund + $depto['total'];
                 
            }
            $cad .= ' { title:"CUNDINAMARCA : '.($total_cund).'", id: "CO-CUN", description: "'.$desc.'</b>'.'", value: '.($total_cund).'}, ';
            //**************************************************************************************
            //TODOS los demás DEPARTAMENTOS
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
            $res = Yii::$app->db->createCommand($sql)->queryAll();
            //Utils::dumpx($res);
            foreach($res as $depto){
                $desc='';
                    //Utils::dump($depto);
                    if($depto['cod']!='CO-DC' && $depto['cod']!='CO-CUN'){
                        
                        //Obtenemos las Ciudades del Depto
                        $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='".$depto['cod']."') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
                        $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
                        $desc .='<br>';
                        $total_depto=0;
                        foreach($resCiud as $depto2){
                             $desc .= '<b> - <span style=\'color:green\'>'.$depto2['nombre'].'</span> : '.$depto2['total'].'</b><br>';
                             $total_depto = $total_depto + intval($depto2['total']);
                        }
                        $title = '<b>'.$depto['departamento'].': '.$total_depto.'</b>';
                        $cad .= ' { title:"'.$title.'", id: "'.$depto['cod'].'", description: "'.$desc.'", value: '.$total_depto.'}, ';
                    }else{
                        $total_depto=1;
                    }
            }
            //
            // Utils::dump('{ title:"BOGOTÁ DC : 220", id: "CO-DC", description: "<b>BOGOTÁ DC : 220</b>", value: 220},  { title:"CUNDINAMARCA : 220", id: "CO-CUN", description: "<br><b> - <span >BOGOTÁ DC</span> : 220</b><br></b>", value: 220},  { title:"<b>ANTIOQUIA: 2</b>", id: "CO-ANT", description: "<br><b> - <span >MEDELLÍN</span> : 2</b><br>", value: 2}, ');
            
            if($total_depto==0){ return null; }else   return $cad;
        }else
          if($rol=='APODERADO'){
            $cliente = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
            
//            $config_cliente = \common\models\CliClienteConfig::find()->where('id_cliente='.$cliente->id_cliente)->one();
//            if($config_cliente){
//                if($config_cliente->notif_apoderado==0){
//                    //**************************************************************************************
//                    //Sólo los de BOGOTÁ DC
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE  p.id_apoderado='.$cliente->id.' and (c.cod='CO-DC') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                    $resBog = Yii::$app->db->createCommand($sql)->queryOne();
//                    $total_bog = isset($resBog['total']) ? $resBog['total']:0;
//                    $cad .= ' { title:"BOGOTÁ DC : '.$total_bog.'", id: "CO-DC", description: "'.'<b>BOGOTÁ DC : '.$total_bog.'</b>'.'", value: '.$total_bog.'}, ';
//
//                    //**************************************************************************************
//                    //Sólo los de CUNDINAMARCA
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE  p.id_apoderado='.$cliente->id.' and (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
//                    $resCun = Yii::$app->db->createCommand($sql)->queryOne();
//                    $total_cund = 0;
//
//                    //Obtenemos las Ciudades del Depto
//                    $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE  p.id_apoderado='.$cliente->id.' and  (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
//                    $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
//                    //Utils::dumpx($resCiud);
//                    $desc .='<br>';
//                    foreach($resCiud as $depto){
//                         $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
//                         $total_cund = $total_cund + $depto['total'];
//
//                    }
//                    $cad .= ' { title:"CUNDINAMARCA : '.($total_cund).'", id: "CO-CUN", description: "'.$desc.'</b>'.'", value: '.($total_cund).'}, ';
//                    //**************************************************************************************
//                    //TODOS los demás DEPARTAMENTOS
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE  p.id_apoderado='.$cliente->id.' and  p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                    $res = Yii::$app->db->createCommand($sql)->queryAll();
//                    //Utils::dumpx($res);
//                    foreach($res as $depto){
//                        $desc='';
//                        //Utils::dump($depto);
//                            if($depto['cod']!='CO-DC' && $depto['cod']!='CO-CUN'){
//
//                                //Obtenemos las Ciudades del Depto
//                                $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE  p.id_apoderado='.$cliente->id.' and  (c.cod='".$depto['cod']."') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                                $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
//                                $desc .='<br>';$total_depto=0;
//                                foreach($resCiud as $depto){
//                                     $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
//                                     $total_depto = $total_depto + $depto['total'];
//                                }
//                                $title = '<b>'.$depto['departamento'].': '.$total_depto.'</b>';
//                                $cad .= ' { title:"'.$title.'", id: "'.$depto['cod'].'", description: "'.$desc.'", value: '.$total_depto.'}, ';
//                            }
//                    }
//                   if($total_depto==0) return null; else   return $cad;
//                }else{
//                    $cliente = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
//            
//                    //**************************************************************************************
//                    //Sólo los de BOGOTÁ DC
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='CO-DC') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                    $resBog = Yii::$app->db->createCommand($sql)->queryOne();
//                    $total_bog = isset($resBog['total']) ? $resBog['total']:0;
//                    $cad .= ' { title:"BOGOTÁ DC : '.$total_bog.'", id: "CO-DC", description: "'.'<b>BOGOTÁ DC : '.$total_bog.'</b>'.'", value: '.$total_bog.'}, ';
//
//                    //**************************************************************************************
//                    //Sólo los de CUNDINAMARCA
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
//                    $resCun = Yii::$app->db->createCommand($sql)->queryOne();
//                    $total_cund = 0;
//
//                    //Obtenemos las Ciudades del Depto
//                    $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
//                    $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
//                    //Utils::dumpx($resCiud);
//                    $desc .='<br>';
//                    foreach($resCiud as $depto){
//                         $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
//                         $total_cund = $total_cund + $depto['total'];
//
//                    }
//                    $cad .= ' { title:"CUNDINAMARCA : '.($total_cund).'", id: "CO-CUN", description: "'.$desc.'</b>'.'", value: '.($total_cund).'}, ';
//                    //**************************************************************************************
//                    //TODOS los demás DEPARTAMENTOS
//                    $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                    $res = Yii::$app->db->createCommand($sql)->queryAll();
//                    //Utils::dumpx($res);
//                    foreach($res as $depto){
//                        $desc='';
//                        //Utils::dump($depto);
//                            if($depto['cod']!='CO-DC' && $depto['cod']!='CO-CUN'){
//
//                                //Obtenemos las Ciudades del Depto
//                                $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='".$depto['cod']."') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
//                                $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
//                                $desc .='<br>';$total_depto=0;
//                                foreach($resCiud as $depto){
//                                     $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
//                                     $total_depto = $total_depto + $depto['total'];
//                                }
//                                $title = '<b>'.$depto['departamento'].': '.$total_depto.'</b>';
//                                $cad .= ' { title:"'.$title.'", id: "'.$depto['cod'].'", description: "'.$desc.'", value: '.$total_depto.'}, ';
//                            }
//                    }
//                   if($total_depto==0) return null; else   return $cad;
//                }
//            }else{
//                $cliente = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
            
                //**************************************************************************************
                //Sólo los de BOGOTÁ DC
                $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='CO-DC') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
                $resBog = Yii::$app->db->createCommand($sql)->queryOne();
                $total_bog = isset($resBog['total']) ? $resBog['total']:0;
                $cad .= ' { title:"BOGOTÁ DC : '.$total_bog.'", id: "CO-DC", description: "'.'<b>BOGOTÁ DC : '.$total_bog.'</b>'.'", value: '.$total_bog.'}, ';

                //**************************************************************************************
                //Sólo los de CUNDINAMARCA
                $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
                $resCun = Yii::$app->db->createCommand($sql)->queryOne();
                $total_cund = 0;

                //Obtenemos las Ciudades del Depto
                $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
                $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
                //Utils::dumpx($resCiud);
                $desc .='<br>';
                foreach($resCiud as $depto){
                     $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
                     $total_cund = $total_cund + $depto['total'];

                }
                $cad .= ' { title:"CUNDINAMARCA : '.($total_cund).'", id: "CO-CUN", description: "'.$desc.'</b>'.'", value: '.($total_cund).'}, ';
                //**************************************************************************************
                //TODOS los demás DEPARTAMENTOS
                $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
                $res = Yii::$app->db->createCommand($sql)->queryAll();
                //Utils::dumpx($res);
                foreach($res as $depto){
                    $desc='';
                    //Utils::dump($depto);
                        if($depto['cod']!='CO-DC' && $depto['cod']!='CO-CUN'){

                            //Obtenemos las Ciudades del Depto
                            $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='".$depto['cod']."') and p.id_cliente=".$cliente->id_cliente." and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
                            $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
                            $desc .='<br>';$total_depto=0;
                            foreach($resCiud as $depto2){
                                 $desc .= '<b> - <span style=\'color:green\'>'.$depto2['nombre'].'</span> : '.$depto2['total'].'</b><br>';
                                 $total_depto = $total_depto + $depto2['total'];
                            }
                            $title = '<b>'.$depto['departamento'].': '.$total_depto.'</b>';
                            $cad .= ' { title:"'.$title.'", id: "'.$depto['cod'].'", description: "'.$desc.'", value: '.$total_depto.'}, ';
                        }else{
                            $total_depto=1;
                        }
                }
              if($total_depto==0) return null; else   return $cad;
//            }
            
            
            
        //ADMIN_VIGPRO Y SUPERADMIN   
        }else{
            // $cliente = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
            
            //**************************************************************************************
            //Sólo los de BOGOTÁ DC
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='CO-DC') and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
            $resBog = Yii::$app->db->createCommand($sql)->queryOne();
            $total_bog = isset($resBog['total']) ? $resBog['total']:0;
            $cad .= ' { title:"BOGOTÁ DC : '.$total_bog.'", id: "CO-DC", description: "'.'<b>BOGOTÁ DC : '.$total_bog.'</b>'.'", value: '.$total_bog.'}, ';
            
            //**************************************************************************************
            //Sólo los de CUNDINAMARCA
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA')  and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
            $resCun = Yii::$app->db->createCommand($sql)->queryOne();
            $total_cund = 0;
            
            //Obtenemos las Ciudades del Depto
            $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.departamento='CUNDINAMARCA')   and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.departamento ORDER BY departamento;";
            $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
            //Utils::dumpx($resCiud);
            $desc .='<br>';
            foreach($resCiud as $depto){
                 $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
                 $total_cund = $total_cund + $depto['total'];
                 
            }
            $cad .= ' { title:"CUNDINAMARCA : '.($total_cund).'", id: "CO-CUN", description: "'.$desc.'</b>'.'", value: '.($total_cund).'}, ';
            //**************************************************************************************
            //TODOS los demás DEPARTAMENTOS
            $sql = "SELECT c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE   p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
            $res = Yii::$app->db->createCommand($sql)->queryAll();
            //Utils::dumpx($res);
            foreach($res as $depto){
                $desc='';
                //Utils::dump($depto);
                    if($depto['cod']!='CO-DC' && $depto['cod']!='CO-CUN'){
                        
                        //Obtenemos las Ciudades del Depto
                        $sql = "SELECT c.nombre ,c.departamento,c.cod,count(*) as total FROM ciudad as c, proc_proceso as p, des_despacho as d WHERE (c.cod='".$depto['cod']."')   and p.id_juzgado=d.id and d.id_ciudad=c.id and p.estado=1 GROUP BY c.cod ORDER BY departamento;";
                        $resCiud = Yii::$app->db->createCommand($sql)->queryAll();
                        $desc .='<br>';$total_depto=0;
                        foreach($resCiud as $depto){
                             $desc .= '<b> - <span style=\'color:green\'>'.$depto['nombre'].'</span> : '.$depto['total'].'</b><br>';
                             $total_depto = $total_depto + $depto['total'];
                        }
                        $title = '<b>'.$depto['departamento'].': '.$total_depto.'</b>';
                        $cad .= ' { title:"'.$title.'", id: "'.$depto['cod'].'", description: "'.$desc.'", value: '.$total_depto.'}, ';
                    }
            }
//            Utils::dump($cad);
           if($total_depto==0) return null; else   return $cad;
            
        }
    }

    

    static function getDispositivo(){
        $tablet_browser = 0;
        $mobile_browser = 0;
        $body_class = 'desktop';

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
            $body_class = "tablet";
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
            $body_class = "mobile";
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
            $body_class = "mobile";
        }

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }
        if ($tablet_browser > 0) {
        // Si es tablet has lo que necesites
           return 0;
        }
        else if ($mobile_browser > 0) {
        // Si es dispositivo mobil has lo que necesites
          return 0;
        }
        else {
        // Si es ordenador de escritorio has lo que necesites
          return 1;
        }  
    }

        static function getStartsPromedioProducto($id_producto){
       // return 0;            
        
        $coms[1] = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2 and calificacion=1')->orderBy('fecha DESC')->count();
        $coms[2] = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2 and calificacion=2')->orderBy('fecha DESC')->count();
        $coms[3] = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2 and calificacion=3')->orderBy('fecha DESC')->count();
        $coms[4] = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2 and calificacion=4')->orderBy('fecha DESC')->count();
        $coms[5] = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2 and calificacion=5')->orderBy('fecha DESC')->count();
                
        
        $comentarios = ProdComentarios::find()->where('id_producto='.$id_producto.' and estado=2')->orderBy('fecha DESC')->all();
        if(count($comentarios)>0){
            $totalCom = count($comentarios);
            $start[] = (100 * $coms[1])/$totalCom; $start[] = (100 * $coms[2])/$totalCom;$start[] = (100 * $coms[3])/$totalCom;
            $start[] = (100 * $coms[4])/$totalCom;$start[] = (100 * $coms[5])/$totalCom; rsort($start); $start= $start[0];
            if($start>0 && $start<=10) $res = 1; else if($start>10 && $start<=25) $res = 2; else if($start>25 && $start<=50) $res = 3;
            else if($start>50 && $start<=80) $res = 4;else if($start>80) $res = 5;
        }else $res=0; 
        
       return  $res;
    }
    
    static function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }


    static function getStarts($starts){
        switch ($starts) {
            case 0: return '<i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';    
            case 1: return '<i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';    
            case 2: return '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';    
            case 3: return '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';    
            case 4: return '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>';    
            case 5: return  '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';    
        }
    }
    
    static function dump($args)
    {
        $print = print_r($args, 1);
        $print = str_replace(array('<', '>'), array('&lt;', '&gt;'), $print);
        echo "<pre>$print</pre>";
    }

    static function dumpx($args)
    {
        self::dump($args);
        exit;
    }
    


    static function getRolVTM($tipo)
    {   
            if($tipo==1)  $role = 'ADMIN_VTM';
        elseif($tipo==2)  $role = 'CONSULTOR_VTM';
        else  return null;
        return $role;
    }

    static function getRolCliente($tipo)
    {
        if($tipo==3)  $role = 'CLI_USUARIO';
        //elseif($tipo==4)  $role = 'CONSULTOR_VTM';
        else  return null;
        return $role;
    }

    static function getRol($id_usuario)
    {
        $role= "";
        $user = User::find()->where('id='.$id_usuario)->one();
        //Utils::dump($user->attributes);
        if(isset($user->superadmin) && $user->superadmin==1) {
            $role = "SUPERADMIN";
        } else {
            //consultar rol
            $role_one = (new \yii\db\Query())
                ->select(['item_name'])
                ->from(Yii::$app->db->tablePrefix.'auth_assignment')
                ->where(['user_id' => $id_usuario])
                ->one();
            $role = $role_one['item_name'];
        }
        return $role;
    }
    
    static function mesSpanish($mes)
    {
        $mes = intval($mes);
        //echo "<b style='color:red'>";
       // Utils::dump($mes);
       $mesesN=array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",
                 8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
       //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
      // exit;
       return $mesesN[$mes];
    }
    
    
    static function fechaSpanish($FechaStamp)
    {
       $ano = date('Y',$FechaStamp);
       $mes = date('n',$FechaStamp);
       $dia = date('d',$FechaStamp);
       $diasemana = date('w',$FechaStamp);
       $diassemanaN= array("Domingo","Lunes","Martes","Miércoles",
                      "Jueves","Viernes","Sábado");
       $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
                 "Agosto","Septiembre","Octubre","Noviembre","Diciembre");
       //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
       return $mesesN[$mes].' '.$dia.' de '.$ano;
    }
    
    static function truncarString($string, $limit, $break=".", $pad="...") {
        // return with no change if string is shorter than $limit
        if(strlen($string) <= $limit){
            return $string;
        }
        // is $break present between $limit and the end of the string?
        if(false !== ($breakpoint = strpos($string, $break, $limit))) {
            if($breakpoint < (strlen($string)-1)) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }
        return $string;
    }
    static function l($cadena)
    {
        $descripcion = substr(strip_tags(trim(addslashes($cadena))),0,250);
        $buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $descripcion = str_ireplace($buscar,$reemplazar,$descripcion);
        $descripcion = str_replace('&nbsp;', '', $descripcion);
        $descripcion = html_entity_decode($descripcion);
        return $descripcion;
    }

    static function limpiarCadena($cadena)
    {
        $cadena = trim($cadena);
        $cadena = mb_strtolower($cadena,'UTF-8');      
        $cadena = str_replace(' ','',$cadena);
        $cadena = str_replace(array('á','à','â','ã','ª','ä'),"a",$cadena);
        $cadena = str_replace(array('&aacute;','&aecute;','&aicute;','&aocute;','&aucute;'),array('a','e','i','o','u'),$cadena);
        
        
        $cadena = str_replace(array('Á','À','Â','Ã','Ä'),"A",$cadena);
        $cadena = str_replace(array('Í','Ì','Î','Ï'),"I",$cadena);
        $cadena = str_replace(array('í','ì','î','ï'),"i",$cadena);
        $cadena = str_replace(array('é','è','ê','ë'),"e",$cadena);
        $cadena = str_replace(array('É','È','Ê','Ë'),"E",$cadena);
        $cadena = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$cadena);
        $cadena = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$cadena);
        $cadena = str_replace(array('ú','ù','û','ü'),"u",$cadena);
        $cadena = str_replace(array('Ú','Ù','Û','Ü'),"U",$cadena);
        $cadena = str_replace(array('[','^','´','`','¨','~',']'),"",$cadena);
        $cadena = str_replace("ç","c",$cadena);
        $cadena = str_replace("Ç","C",$cadena);
        $cadena = str_replace("ñ","n",$cadena);
        $cadena = str_replace("Ñ","N",$cadena);
        $cadena = str_replace("Ý","Y",$cadena);
        $cadena = str_replace("ý","y",$cadena);
        $cadena = str_replace("&aacute;","a",$cadena);
        $cadena = str_replace("&Aacute;","A",$cadena);
        $cadena = str_replace("&eacute;","e",$cadena);
        $cadena = str_replace("&Eacute;","E",$cadena);
        $cadena = str_replace("&iacute;","i",$cadena);
        $cadena = str_replace("&Iacute;","I",$cadena);
        $cadena = str_replace("&oacute;","o",$cadena);
        $cadena = str_replace("&Oacute;","O",$cadena);
        $cadena = str_replace("&uacute;","u",$cadena);
        $cadena = str_replace("&Uacute;","U",$cadena);
        $cadena = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',$cadena);
        return $cadena;
    }

    static function limpiarCadena3($cadena)
    {
        $cadena = trim($cadena);
        $cadena = mb_strtoupper($cadena,'UTF-8');
        //$cadena = str_replace(' ','',$cadena);
        $cadena = str_replace(array('á','à','â','ã','ª','ä'),"a",$cadena);
        $cadena = str_replace(array('&aacute;','&aecute;','&aicute;','&aocute;','&aucute;'),array('a','e','i','o','u'),$cadena);


        $cadena = str_replace(array('Á','À','Â','Ã','Ä'),"A",$cadena);
        $cadena = str_replace(array('Í','Ì','Î','Ï'),"I",$cadena);
        $cadena = str_replace(array('í','ì','î','ï'),"i",$cadena);
        $cadena = str_replace(array('é','è','ê','ë'),"e",$cadena);
        $cadena = str_replace(array('É','È','Ê','Ë'),"E",$cadena);
        $cadena = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$cadena);
        $cadena = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$cadena);
        $cadena = str_replace(array('ú','ù','û','ü'),"u",$cadena);
        $cadena = str_replace(array('Ú','Ù','Û','Ü'),"U",$cadena);
        $cadena = str_replace(array('[','^','´','`','¨','~',']'),"",$cadena);
        $cadena = str_replace("ç","c",$cadena);
        $cadena = str_replace("Ç","C",$cadena);
        $cadena = str_replace("ñ","n",$cadena);
        $cadena = str_replace("Ñ","N",$cadena);
        $cadena = str_replace("Ý","Y",$cadena);
        $cadena = str_replace("ý","y",$cadena);
        $cadena = str_replace("&aacute;","a",$cadena);
        $cadena = str_replace("&Aacute;","A",$cadena);
        $cadena = str_replace("&eacute;","e",$cadena);
        $cadena = str_replace("&Eacute;","E",$cadena);
        $cadena = str_replace("&iacute;","i",$cadena);
        $cadena = str_replace("&Iacute;","I",$cadena);
        $cadena = str_replace("&oacute;","o",$cadena);
        $cadena = str_replace("&Oacute;","O",$cadena);
        $cadena = str_replace("&uacute;","u",$cadena);
        $cadena = str_replace("&Uacute;","U",$cadena);
        $cadena = str_replace(
            array("\\", "¨", "º", "-", "~",
                "#", "@", "|", "!", "\"",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "`", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":",
                ".", " "),
            ' ',$cadena);
        return $cadena;
    }
    
    static function limpiarCadena2($cadena)
    {
        $cadena = trim($cadena);
       // $cadena = mb_strtolower($cadena,'UTF-8');      
            
//        $cadena = str_replace(array('&aacute;','&aecute;','&aicute;','&aocute;','&aucute;'),array('a','e','i','o','u'),$cadena);
            
        $cadena = str_replace("&ntilde;","ñ",$cadena);    
        $cadena = str_replace("&Ntilde;","Ñ",$cadena);    
        $cadena = str_replace("&aacute;","a",$cadena);
        $cadena = str_replace("&Aacute;","A",$cadena);
        $cadena = str_replace("&eacute;","e",$cadena);
        $cadena = str_replace("&Eacute;","E",$cadena);
        $cadena = str_replace("&iacute;","i",$cadena);
        $cadena = str_replace("&Iacute;","I",$cadena);
        $cadena = str_replace("&oacute;","o",$cadena);
        $cadena = str_replace("&Oacute;","O",$cadena);
        $cadena = str_replace("&uacute;","u",$cadena);
        $cadena = str_replace("&Uacute;","U",$cadena);
            
        return $cadena;
    }
    
    static function getNick($name)
    {
      $name_nick_explode = explode(" ", $name);
      return $name_nick_explode[0][0]. " " . $name_nick_explode[1][0];
    }
    
    function urls_amigables($url) {
        $url = strtolower($url);
        $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $repl = array('a', 'e', 'i', 'o', 'u', 'n');
        $url = str_replace($find, $repl, $url);
        $find = array(' ', '&', '\r\n', '\n', '+');
        $url = str_replace($find, '-', $url);
        $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
        $repl = array('', '-', '');
        $url = preg_replace($find, $repl, $url);
        return $url;
    }

    static function urls_amigables2($url) {
        $url = strtolower($url);
        $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $repl = array('a', 'e', 'i', 'o', 'u', 'n');
        $url = str_replace($find, $repl, $url);
 
        $url = str_replace('+', '__', $url);
        $url = str_replace('*', '--', $url);
 
        $find = array(' ', '&', '\r\n', '\n');
        $url = str_replace($find, '-', $url);
 
        $find = array('/[^a-z0-9\-__<>]/');
        $repl = array('', '-', '');
        $url = preg_replace($find, $repl, $url);
        return $url;
    }

}
