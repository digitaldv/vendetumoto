<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
              'facebook' => [
                'class' => 'yii\authclient\clients\Facebook',
                'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                'clientId' => '4452482451514336',
                'clientSecret' => 'bef5a9f2f89584a34eeee4ac5b207974',
                'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
              ],
              'google' => [
                    'class' => 'yii\authclient\clients\Google',// 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '196068589161-pqtghcacg3721a6oskej9qnftr10tqhn.apps.googleusercontent.com',
                    'clientSecret' => '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', // ID: vende-tu-moto
                ],
            ],
          ],
    ],
];
