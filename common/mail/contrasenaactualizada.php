<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
require('layouts/header.php');
?>


    <table style="width:600px;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td height="16px" style="Margin:0;border-collapse:collapse!important;color:#969696;font-family:Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <br />
                <div style="margin-left: 20px;margin-top: 10px">
                    <span style="color:#000000">RECUPERACIÓN DE CONTRASEÑA</span>
                    <hr style="border: 5px solid red; width: 100px;clear: both;margin-left: 0px">
                    <br>
                    <b style="color:#000000">¡Hola <?= Html::encode($user->username) ?>!</b><br>
                    <p style="color:#666666; line-height: normal;">
                        Has actualizado con éxito tu contraseña para la cuenta (<?= Html::encode($user->username) ?>).<br><br>

                    </p>
                    <br>
                    <div style="font-size:12px;text-align: center">
                        <a style=" border-radius: 10px; background: red; font-size: 18px;color: #fff;padding: 10px;text-decoration: none"
                           href="https://vtmclientes.digitalventures.com.co/">Iniciar Sesión</a>
                        <br>
                    </div>
                    <br><br>
                    <p style="margin-bottom: 3px;color:#666666">Atentamente, </p>
                    <b style="color:#000000">Vende Tu Moto</b><br><br>

                    <hr>
            </td>
        </tr>
        </tbody>
    </table>


<?php require('layouts/footer.php');
