<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php require('header.php'); ?>
<tr>
<td>

<table style="width:600px;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td height="16px" style="Margin:0;border-collapse:collapse!important;color:#969696;font-family:Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <br />
                        <span style="color:#000000">
                            <table>
                                <tr>
                                    <td>CAMBIO DE CONTRASEÑA
                                        <hr style="border: 5px solid red; width: 100px;clear: both;margin-left: 0px">
                                    </td>
                                </tr>
                            </table>
                        </span>
                        <br>
                        <b style="color:#000000">¡Hola <?= $username ?>!</b><br><br>
                        <p style=";color:#666666;    line-height: normal;">
                            Cambiamos tu contraseña, tal como lo solicitaste. Para ver o cambiar información adicional, visita <a href="https://vtmclientes.digitalventures.com.co/site/miperfil">Tu cuenta.</a>
                            <br><br> Si no solicitaste el cambio de contraseña, estamos aquí para ayudarte a proteger tu cuenta. Tan solo  <a href="https://api.whatsapp.com/send?text=Deseo%20comunicarme%20con%20un%20agente%20de%20Soporte%20de%20Vende%20Tu%20Moto%20-%20Cambio%20Contraseña">contáctanos.</a>
                            <br><br> Tus amigos de Vende Tu Moto
                            <!--https://api.whatsapp.com/send?phone=573057508454&text=Deseo%20comunicarme%20con%20un%20agente%20de%20Soporte%20de%20Vende%20Tu%20Moto%20-%20Cambio%20Contraseña-->
                        </p>
                    <br><br>
                    <div style="font-size:12px;text-align: center">
                        <a style="color:red;font-size: 18px" href="https://vtmclientes.digitalventures.com.co/site/resultados">SIGUE BUSCANDO O COMPRANDO MOTOS ></a>
                            <br>
                    </div>
                <br><br>
                <p style="margin-bottom: 3px;color:#666666">Atentamente, </p>
                <b style="color:#000000">Vende Tu Moto</b><br><br>

                <hr>
            </td>
        </tr>
    </tbody>
</table>
</td>
</tr>
<?php



require('footer.php');

?>
