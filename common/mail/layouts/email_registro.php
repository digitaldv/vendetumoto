<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php require('header.php'); ?>
<tr>
<td>

<table style="width:600px;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td height="16px" style="Margin:0;border-collapse:collapse!important;color:#969696;font-family:Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <br />
                <div style="margin-left: 20px;margin-top: 10px">
                    <span style="color:#000000">BIENVENIDO A VENDE TU MOTO</span>
                    <hr style="border: 5px solid red; width: 100px;clear: both;margin-left: 0px">
                    <br>
                    <b style="color:#000000">¡Hola <?= $username ?>!</b><br>

                    <p style=";color:#666666;    line-height: normal;">Nos alegra tenerte en nuestro portal Vende Tu Moto. Ahora podrás realizar búsquedas, administrar y tener control de tus Motos - Cuatrimotos - UTV’s - Lanchas, revisar las estadísticas y
                        tener conversaciones con tus contactos.
                    </p>
                    <br>
                    <div   style="font-size:12px;text-align: center">
                        <a style=" border-radius: 10px; background: red; font-size: 18px;color: #fff;padding: 10px;text-decoration: none"   href="https://vtmclientes.digitalventures.com.co">INGRESAR A MI CUENTA</a>
                    <br>

                </div>
                <br><br>
                <p style="margin-bottom: 3px;color:#666666">Atentamente, </p>
                <b style="color:#000000">Vende Tu Moto</b><br><br>

                <hr>
            </td>
        </tr>
    </tbody>
</table>
</td>
</tr>
<?php require('footer.php');

?>
