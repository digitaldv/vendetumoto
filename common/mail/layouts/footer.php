<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
    <tr style="background-color: #fff! important">
        <td style="vertical-align:middle; padding:7px 9px 6px 10px!important;text-align:left;" align="center">
            <p>Síguenos en:</p>
            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                <tbody><tr>
                    <td align="center" valign="top">
                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody><tr>
                                            <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                    <tbody><tr>

                                                        <td align="center" valign="middle" width="24">
                                                            <a href="https://vtmclientes.digitalventures.com.co" target="_blank">
                                                                <img src="https://vtmclientes.digitalventures.com.co/img/globe.png" alt="Twitter" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:10px;padding-bottom:9px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody><tr>
                                            <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                    <tbody><tr>

                                                        <td align="center" valign="middle" width="24">
                                                            <a href="https://www.facebook.com/vendetumoto.co" target="_blank">
                                                                <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd">
                                                            </a>
                                                        </td>


                                                    </tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>


                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                            <tbody><tr>
                                <td valign="top" style="padding-right:0;padding-bottom:9px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                        <tbody><tr>
                                            <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                    <tbody><tr>

                                                        <td align="center" valign="middle" width="24">
                                                            <a href="https://www.instagram.com/vendetumoto.co/" target="_blank" >
                                                                <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" alt="Instagram" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd">
                                                            </a>
                                                        </td>


                                                    </tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>
    <tr>
        <td style="background: #f3f3f3;font-size:10px;padding: 30px;color:#b5b5b5;    font-family: sans-serif;text-align: center">
            "Usted ha recibido este mensaje por ser un cliente de <b>Vende Tu Moto</b>. Si usted ha recibido este e-mail por error o no desea recibir más información de este tipo,
            por favor de clic en el link que aparece abajo o envíe un correo a info@vendetumoto.co de lo contrario, entenderemos que está interesado en continuar recibiendo este tipo de
            comunicados."<br>
            La información contenida en este correo electrónico y en todos sus archivos anexos, es confidencial y/o privilegiada y sólo puede ser utilizada por la(s) persona(s) a la(s)
            cual(es) está dirigida. Si usted no es el destinatario autorizado, cualquier modificación, retención, difusión, distribución o copia total o parcia de este mensaje y/o de la
            información contenida en el mismo y/o en sus archivos anexos está prohibida y son sancionadas por la ley. Si por error recibe este mensaje, le ofecemos disculpas, sirvase
            borrarlo de inmediato, notificarle su error a la persona que lo envió y abstenerse de divulgar su contenido y anexos. Visite www.vendetumoto.co
        </td>
    </tr>
</tbody>
</table>
<?php $this->endPage() ?>

