<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php require('header.php'); ?>

 
<tr style="background: #fff;">
    <td style="color:#969696;font-size:16px;padding: 0px">
        <br />
        ¡<b><?= $usuario ?></b>, bienvenido a <b style=" color:#F33B42">Vendetumoto.co</b>!
        <div style="font-size:12px">
            <br />Se han registrado sus datos como nuevo <b style="color:#2e354f">(<?= $rol ?>)</b> en nuestra plataforma.
           <br> <br />Sus datos de acceso son:</div>
        <br>
        <div  style="font-size:12px">
            <b style="color:#F33B42;margin-left: 10px">Enlace : </b> <a href="https://vtmclientes.digitalventures.com.co">https://vendetumoto.co</a> <br>
            <b style="color:#F33B42;margin-left: 10px">Usuario : </b> <?= $username ?> <br>
            <b style="color:#F33B42;margin-left: 10px">Contraseña : </b> 123 <em>(Cuando ingrese la primera vez, podrá modificarla)</em>
        </div>      
        <br>
        <br>
        <div style="font-size:12px">
            Cordialmente,
            <br><br>__________________________<br>
            <b style="color: #F33B42">Equipo de Vendetumoto.co</b><br>
            <em>soporte@vendetumoto.co</em>
        </div>
        <br>
    </td>
</tr>
       
    

<?php require('footer.php'); ?>
