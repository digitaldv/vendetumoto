<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php require('header.php'); ?>
<tr>
<td>

<table style="width:600px;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td height="16px" style="Margin:0;border-collapse:collapse!important;color:#969696;font-family:Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                <br />
                <div style="margin-left: 20px;margin-top: 10px">
                    <?php

                    if($tipo=='al_vendedor'){

                        ?>
                        <span style="color:#000000">
                            <table>
                                <tr>
                                    <td>Respóndele a <b>(<?= $comprador->nombre ?>)</b> pronto. Te envió un mensaje por (<?= $pub->modelo->marca->nombre.' - '.$pub->modelo->nombre.' - '.$pub->ano ?>)
                                    <hr style="border: 5px solid red; width: 100px;clear: both;margin-left: 0px"></td>
                                    <td>
                                        <div style="display: inline-block; position: relative; width: 100px; height: 100px; overflow: hidden; border-radius: 50%;margin-left: 10px">
                                            <?php
                                            if(is_array($foto1) && isset($foto1[0]) && $foto1[0]!='') {?>
                                                <img style="width: auto; height: 100%; margin-left: -50px;"  src="<?= $foto1[0] ?>">
                                            <?php }else{ ?>
                                                <img style="width: auto; height: 100%; margin-left: -50px;"  src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/<?= $foto1->publicacion->carpeta . '/' . $foto1->archivo ?>">
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </span>
                        <br>
                        <b style="color:#000000">¡Hola <?= $vendedor->nombreFull ?>!</b><br><br>
                        <p style=";color:#666666;    line-height: normal;">
                        (<?= $comprador->nombre ?>) te envio un mensaje preguntando por (<?= $pub->modelo->marca->nombre.' - '.$pub->modelo->nombre.' - '.$pub->ano ?>).
                        </p>

                    <?php }else{ ?>
                        <span style="color:#000000">
                            <table>
                                <tr>
                                    <td>Respóndele a <b>(<?= $vendedor->nombre ?>)</b> pronto. Te envió un mensaje por <b>(<?= $pub->modelo->marca->nombre.' - '.$pub->modelo->nombre.' - '.$pub->ano ?>)</b>
                                    <hr style="border: 5px solid red; width: 100px;clear: both;margin-left: 0px"></td>
                                    <td>
                                        <div style="display: inline-block; position: relative; width: 100px; height: 100px; overflow: hidden; border-radius: 50%;margin-left: 10px">
                                              <?php
                                              if(is_array($foto1) && isset($foto1[0]) && $foto1[0]!='') {?>
                                                  <img style="width: auto; height: 100%; margin-left: -50px;"  src="<?= $foto1[0] ?>">
                                              <?php }else{ ?>
                                                  <img style="width: auto; height: 100%; margin-left: -50px;"  src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/<?= $foto1->publicacion->carpeta . '/' . $foto1->archivo ?>">
                                              <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </span>
                        <br>
                        <b style="color:#000000">¡Hola <?= $comprador->nombreFull ?>!</b><br><br>
                        <p style=";color:#666666;    line-height: normal;">
                            (<?= $vendedor->nombre ?>) te envio un mensaje por (<?= $pub->modelo->marca->nombre.' - '.$pub->modelo->nombre.' - '.$pub->ano ?>)
                        </p>
                    <?php } ?>

                    <br><br>
                    <div style="font-size:12px;text-align: center">
                        <a style=" border-radius: 10px; background: red; font-size: 18px;color: #fff;padding: 10px;text-decoration: none;padding: 10px"   href="https://vtmclientes.digitalventures.com.co/site/mismensajes">VER PREGUNTA</a>
                    <br>

                </div>
                <br><br>
                <p style="margin-bottom: 3px;color:#666666">Atentamente, </p>
                <b style="color:#000000">Vende Tu Moto</b><br><br>

                <hr>
            </td>
        </tr>
    </tbody>
</table>
</td>
</tr>
<?php



require('footer.php');

?>
