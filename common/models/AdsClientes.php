<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ads_clientes".
 *
 * @property int $id
 * @property string $nombre_contacto
 * @property string $telefonos
 * @property string $celular
 * @property string $fecha
 * @property string $empresa
 * @property int $tipo_persona 1: Individual, 2: Juridica
 * @property int $id_ciudad
 * @property string $cargo
 *
 * @property City $ciudad
 * @property AdsPublicidad[] $adsPublicidads
 */

class AdsClientes extends \yii\db\ActiveRecord
{
    public $_tipo_persona = [
        '1' => 'Natural',
        '2' => 'Jurídica',
    ];
    public static function tableName()
    {
        return 'ads_clientes';
    }


    public function rules()
    {
        return [
            [['nombre_contacto', 'telefonos', 'celular', 'fecha', 'id_ciudad', 'cargo'], 'required'],
            [['fecha'], 'safe'],
            [['tipo_persona', 'id_ciudad'], 'integer'],
            [['nombre_contacto', 'telefonos'], 'string', 'max' => 100],
            [['celular'], 'string', 'max' => 10],
            [['empresa', 'cargo'], 'string', 'max' => 45],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_ciudad' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_contacto' => 'Nombre Contacto',
            'telefonos' => 'Teléfonos',
            'celular' => 'Celular',
            'fecha' => 'Fecha',
            'empresa' => 'Empresa',
            'tipo_persona' => 'Tipo Persona',
            'id_ciudad' => 'Ciudad',
            'cargo' => 'Cargo',
        ];
    }

    public function getCiudad()
    {
        return $this->hasOne(City::className(), ['id' => 'id_ciudad']);
    }


    public function getAdsPublicidads()
    {
        return $this->hasMany(AdsPublicidad::className(), ['id_cliente_ads' => 'id']);
    }
}
