<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ads_publicidad".
 *
 * @property int $id
 * @property int $tipo_publicidad 1: Imagen 1 Home - Arriba Full (1170px x 180px), 2: Imagen 2 Home - Mitad página (570px x 180px),
 * 3: Imagen 3 Home - Abajo Full (1170px x 180px), 4: Imagen Detalle Anuncio  (720px x 220px)
 * @property int $id_cliente_ads quien compro
 * @property string $fecha
 * @property string $fecha_publicacion
 * @property int $precio precio acordado
 * @property int $estado 0: Inactivo, 1: Activo
 * @property int $dias_publicado dias que debe estar rodando la publicidad
 * @property string $imagen Imagen a publicar del cliente
 *
 * @property AdsClientes $clienteAds
 */
class AdsPublicidad extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public $_tipo_publicidad = [
        '1' => 'Imagen 1 Home - Arriba Full (1170px x 180px)',
        '2' => 'Imagen 2 Home - Mitad (IZQ) (570px x 180px)',
        '5' => 'Imagen 2 Home - Mitad (DER) (570px x 180px)',
        '3' => 'Imagen 3 Home - Abajo Full (1170px x 180px)',
        '4' => 'Imagen Detalle Anuncio (720px x 220px)'
    ];
    public static function tableName()
    {
        return 'ads_publicidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_publicidad', 'id_cliente_ads', 'fecha', 'precio', 'estado', 'dias_publicado','fecha_publicacion'], 'required'],
            [['tipo_publicidad', 'id_cliente_ads', 'precio', 'estado', 'dias_publicado'], 'integer'],
            [['fecha','fecha_publicacion'], 'safe'],
            [['imagen'], 'string'],
            [['id_cliente_ads'], 'exist', 'skipOnError' => true, 'targetClass' => AdsClientes::className(), 'targetAttribute' => ['id_cliente_ads' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_publicidad' => 'Tipo Publicidad',
            'id_cliente_ads' => 'Cliente',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
            'estado' => 'Estado',
            'dias_publicado' => 'Días Publicado',
            'imagen' => 'Imagen',
            'fecha_publicacion' => 'Fecha Publicación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteAds()
    {
        return $this->hasOne(AdsClientes::className(), ['id' => 'id_cliente_ads']);
    }
}
