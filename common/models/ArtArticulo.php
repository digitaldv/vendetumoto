<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "art_articulo".
 *
 * @property int $id
 * @property int $id_categoria
 * @property int $id_creador
 * @property string $titulo
 * @property string $contenido
 * @property string $fecha_creacion
 * @property resource $imagen_cabecera BASE 64
 * @property int $estado 0: Inactivo (sin publicar), 1: Publicado, 2: Eliminado (logico)
 * @property int $visitas Total de visitas
 * @property string $fecha_edicion
 * @property int $id_editor
 * @property string $tags legal,abogados,etc
 * @property string $url_permanente
 *
 * @property User $creador
 * @property User $editor
 * @property ArtCategoria $categoria
 */
class ArtArticulo extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'art_articulo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_categoria', 'id_creador', 'titulo', 'contenido', 'fecha_creacion', 'estado', 'visitas', 'fecha_edicion', 'id_editor', 'tags', 'url_permanente'], 'required'],
            [['id_categoria', 'id_creador', 'estado', 'visitas', 'id_editor'], 'integer'],
            [['contenido', 'imagen_cabecera', 'tags'], 'string'],
            [['fecha_creacion', 'fecha_edicion'], 'safe'],
            [['titulo'], 'string', 'max' => 300],
            [['url_permanente'], 'string', 'max' => 255],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
            [['id_editor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_editor' => 'id']],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => ArtCategoria::className(), 'targetAttribute' => ['id_categoria' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_categoria' => 'Id Categoria',
            'id_creador' => 'Id Creador',
            'titulo' => 'Titulo',
            'contenido' => 'Contenido',
            'fecha_creacion' => 'Fecha Creacion',
            'imagen_cabecera' => 'Imagen Cabecera',
            'estado' => 'Estado',
            'visitas' => 'Visitas',
            'fecha_edicion' => 'Fecha Edicion',
            'id_editor' => 'Id Editor',
            'tags' => 'Tags',
            'url_permanente' => 'Url Permanente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditor()
    {
        return $this->hasOne(User::className(), ['id' => 'id_editor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(ArtCategoria::className(), ['id' => 'id_categoria']);
    }
}
