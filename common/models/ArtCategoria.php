<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "art_categoria".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property int $estado 0: Inactiva, 1: Activa

 *
 * @property ArtArticulo[] $artArticulos
 */
class ArtCategoria extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'art_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion', 'estado' ], 'required'],
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 200],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtArticulos()
    {
        return $this->hasMany(ArtArticulo::className(), ['id_categoria' => 'id']);
    }
}
