<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property int $state_id
 * @property string $name
 * @property int $code
 * @property string $created_date
 *
 * @property State $state
 * @property CliCliente[] $cliClientes
 * @property Empleado[] $empleados
 * @property ProyCliente[] $proyClientes
 */
class City extends \yii\db\ActiveRecord
{

    public $nombreFull;
    public $nombreFull2;
    public $nombreFull3;

    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_id', 'name', 'created_date'], 'required'],
            [['state_id', 'code'], 'integer'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_id' => 'State ID',
            'name' => 'Nombre',
            'code' => 'Code',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliClientes()
    {
        return $this->hasMany(CliCliente::className(), ['id_ciudad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleado::className(), ['id_ciudad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyClientes()
    {
        return $this->hasMany(ProyCliente::className(), ['id_ciudad' => 'id']);
    }

    public function afterFind()
    {
        $state = State::find()->where('id='.$this->state_id)->orderBy('name')->one();
        $this->nombreFull = $this->name.' - '.$state->name;
        $this->nombreFull2 = $this->name.' - '.$state->name.' - '.$state->country->name;
        $this->nombreFull3 = $state->name.' - '.$this->name;
    }
}
