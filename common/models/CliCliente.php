<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cli_cliente".
 *
 * @property int $id
 * @property int $tipo 1: Firma de Abogados, 2: Gobierno, 3: Salud, 4: Financiero, 5: Fundacion, 6: Seguros, 7: Partido Politico, 8: Empresa Privada
 * @property string $nombre
 * @property string $nit
 * @property string $nombres_contacto
 * @property string $telefono_contacto
 * @property string $email_contacto
 * @property string $celular_contacto
 * @property string $fecha
 * @property int $estado 1: Activo, 2: Inactivo
 * @property string $logo
 * @property int $id_ciudad
 * @property string $carpeta carpeta MD5 
 *
 * @property City $ciudad
 * @property CliUsuario[] $cliUsuarios
 * @property ProyProyecto[] $proyProyectos
 */
class CliCliente extends \yii\db\ActiveRecord
{
    public $nombreFull;
    public $nombreFull2;
    public $_tipo = [
        '1'=>'Firma de Abogados', '2'=>'Gobierno', '3'=>'Salud', '4'=>'Financiero', 
        '5'=>'Fundacion','6'=>'Seguros', '7'=>'Partido Politico', '8'=>'Empresa Privada'
    ];
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cli_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'nombre', 'nit', 'nombres_contacto', 'telefono_contacto', 'email_contacto', 'celular_contacto', 'fecha', 'estado', 'id_ciudad', 'carpeta'], 'required'],
            [['tipo', 'estado', 'id_ciudad'], 'integer'],
            [['fecha'], 'safe'],
            [['email_contacto'], 'email'],

            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, gif, JPG, JPEG, PNG, GIF', 'maxSize' => 1024 * 1024 * 2],
       
            [['nombre', 'email_contacto', 'logo'], 'string', 'max' => 100],
            [['nit', 'nombres_contacto', 'telefono_contacto', 'celular_contacto', 'carpeta'], 'string', 'max' => 45],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_ciudad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'nombre' => 'Nombre',
            'nit' => 'Nit',
            'nombres_contacto' => 'Nombres Contacto',
            'telefono_contacto' => 'Telefono Contacto',
            'email_contacto' => 'Email Contacto',
            'celular_contacto' => 'Celular Contacto',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'logo' => 'Logo',
            'id_ciudad' => 'Id Ciudad',
            'carpeta' => 'Carpeta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(City::className(), ['id' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliUsuarios()
    {
        return $this->hasMany(CliUsuario::className(), ['id_cliente' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyProyectos()
    {
        return $this->hasMany(ProyProyecto::className(), ['id_cliente' => 'id']);
    }

    public function afterFind()
    {
         $this->nombreFull2 = strtoupper($this->nombre.' - '.$this->nit.' ('.$this->ciudad->name.' - '.$this->ciudad->state->country->name.')');
        // $this->nombreFull2 = $this->nombre.' ('.$this->idCiudad->nombre.' - '.$this->idCiudad->departamento.')';
        // $this->nombreShort = $this->nombre.' ('.$this->idCiudad->nombre.')';
        $this->nombreFull = $this->nombre.' - '.$this->nit;
    }
    
}
