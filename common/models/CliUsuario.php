<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "cli_usuario".

 * @property int $id_cliente Siempre id_cliente=1
 * @property int $id_usuario
 * @property int $rol 2: USUARIO
 * @property string $telefonos
 * @property string $nombre Telefonos contacto
 * @property string $email
 * @property string $whatssapp +573187704246
 * @property string $fecha
 * @property int $estado 1: Activo, 0: Inactivo
 * @property string $imagen Imagen o avatar del usuario
 * @property int $check_acepto_term
 * @property int $check_recibe_prom 0: No quiero recibir promociones, 1: Si quiero recibir promociones a mi email
 *
 * @property CliCliente $cliente
 * @property User $usuario
 */
class CliUsuario extends \yii\db\ActiveRecord
{
    public $password;
    public $repeat_password;
    public $nombreFull;

    public $_rol = [
       // '1' => 'CLI_ADMIN',
        '2' => 'USUARIO',
    ];

    public $_tipo_doc = [
        '1' => 'Cédula',
        '0' => 'Tarjeta Identidad',
        '3' => 'Cédula Extranjería',
        '4' => 'Pasaporte',
        //5' => 'NIT',
    ];
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public $_genero = [
        '1' => 'Masculino',
        '2' => 'Femenino',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cli_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'rol', 'nombre', 'email', 'fecha', 'estado'], 'required'],
            [['id_cliente', 'id_usuario', 'rol', 'estado', 'check_acepto_term', 'check_recibe_prom'], 'integer'],

            
            [['fecha'], 'safe'],
            [['email'], 'email'],
            [['imagen'], 'string'],

            [['telefonos','whatssapp'] , 'string', 'min'=> 10, 'max'=>10],
            [['telefonos','whatssapp'], 'match', 'pattern' => '/^\d*(?:\.\d{1,2})?$/', 'message'=>'Debe ingresar un número de 10 dígitos'],
            //[['telefonos'], 'integer', 'min' => 10, 'max' => 999999999, 'message'=>'Debe ingresar un número de 10 dígitos'],
            //[['whatssapp'], 'integer', 'min' => 10, 'max' => 999999999, 'message'=>'Debe ingresar un número de 10 dígitos'],
            [['nombre', 'email'], 'string', 'max' => 100],


            [['password', 'repeat_password'], 'trim'],
            ['repeat_password', 'compare', 'compareAttribute'=>'password'],

            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => CliCliente::className(), 'targetAttribute' => ['id_cliente' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];

    }



    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cliente' => 'Cliente',
            'id_usuario' => 'Usuario',
            'rol' => 'Rol',
            'telefonos' => 'Número de contacto',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'whatssapp' => 'Número de WhatsApp',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'imagen' => 'Imagen',
            'repeat_password' => 'Confirmar contraseña'
        ];
    }

    public function getCliente()
    {
        return $this->hasOne(CliCliente::className(), ['id' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    public function afterFind()
    {
        $this->nombreFull = $this->nombre;
    }


}
