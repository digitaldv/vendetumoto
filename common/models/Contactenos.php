<?php

namespace common\models;

use Yii;
/**
 * This is the model class for table "contactenos".
 *
 * @property int $id
 * @property int $asunto 1: ¿Como compro en Vende Tu Moto?
 * @property string $nombre
 * @property string $email
 * @property string $mensaje
 * @property string $fecha
 */
class Contactenos extends \yii\db\ActiveRecord
{
    //1: Me interesa publicitar mi negocio en Vendetumoto.co, 2: Pregunta, 3: Comentario, 4: Soporte, 5: Otros
    public $_asunto = [
        '1' => 'Me interesa publicitar mi negocio en Vendetumoto.co',
        '2' => 'Pregunta',
        '3' => 'Comentario',
        '4' => 'Soporte',
        '5' => 'Otros',
    ];
    public static function tableName()
    {
        return 'contactenos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asunto', 'nombre', 'email', 'mensaje', 'fecha'], 'required'],
            [['asunto'], 'integer'],
            [['mensaje'], 'string'],
            [['fecha'], 'safe'],
            [['nombre', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'asunto' => 'Asunto',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'mensaje' => 'Mensaje',
            'fecha' => 'Fecha',
        ];
    }
}
