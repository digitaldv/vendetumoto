<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "empleado".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_ciudad
 * @property int $id_creador Creador del registro
 * @property int $rol 1: ADMIN_VTM, 2: CONSULTOR_VTM
 * @property int $tipo_documento 1: Cedula, 2: Targeta Identidad, 3: Cedula Extranjeria
 * @property string $documento
 * @property string $nombres
 * @property string $apellidos
 * @property string $email
 * @property string $fecha_nacimiento
 * @property string $direccion
 * @property string $telefono
 * @property string $celular
 * @property int $estado 1: Activo, 0: Inactivo
 * @property string $created_date Fecha de creacion del empleado
 * @property string $update_date
 * @property string $deleted_date
 *
 * @property User $usuario
 * @property User $creador
 * @property City $ciudad
 */
class Empleado extends \yii\db\ActiveRecord
{
    public $nombreFull;
    public $_rol = [
        '1' => 'ADMIN_VTM',
        //'2' => 'CONSULTOR_VTM',
    ];
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public $_tipo = [
        '1' => 'Cédula',
        '2' => 'Cédula de Extranjeria',
        '3' => 'Pasaporte',
    ];

    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_ciudad', 'id_creador', 'rol', 'tipo_documento', 'documento', 'nombres', 'apellidos', 'email', 'fecha_nacimiento', 'celular', 'created_date', 'estado'], 'required'],
            [['id_usuario', 'id_ciudad', 'id_creador', 'rol', 'tipo_documento', 'documento', 'estado'], 'integer'],
            
            [['fecha_nacimiento', 'created_date', 'update_date', 'deleted_date'], 'safe'],
            [['nombres', 'apellidos'], 'string', 'max' => 60],
            [['email'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['email'],'unique','message'=>'¡El Email ingresado ya existe! Verifique de nuevo.'],
            [['telefono', 'celular'], 'string', 'max' => 45],
            [['direccion'], 'string', 'max' => 150],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_ciudad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Usuario',
            'id_ciudad' => 'Ciudad',
            'id_creador' => 'Creador',
            'rol' => 'Rol',
            'tipo_documento' => 'Tipo Documento',
            'documento' => 'Documento',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'direccion' => 'Dirección',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'estado' => 'Estado',
            'created_date' => 'Created Date',
            'update_date' => 'Update Date',
            'deleted_date' => 'Deleted Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(City::className(), ['id' => 'id_ciudad']);
    }

    public function afterFind()
    {
        $this->nombreFull = $this->nombres.' '.$this->apellidos.' - CC. '.$this->documento;


    }

}
