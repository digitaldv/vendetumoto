<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mot_caracteristicas".
 *
 * @property int $id
 * @property string $nombre Con Alarma, etc...
 * @property string $estado 0: Inactivo, 1: Activo
 */
class MotCaracteristicas extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'mot_caracteristicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado'], 'required'],
            [['nombre', 'estado'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }
}
