<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mot_modelo".
 *
 * @property int $id
 * @property string $nombre
 * @property int $estado
 * @property int $id_marca
 *
 * @property MotMarca $marca
 * @property PubPublicacion[] $pubPublicacions
 */
class MotModelo extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'mot_modelo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado', 'id_marca'], 'required'],
            [['estado', 'id_marca'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['id_marca'], 'exist', 'skipOnError' => true, 'targetClass' => MotMarca::className(), 'targetAttribute' => ['id_marca' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_marca' => 'Marca',
            'nombre' => 'Nombre',
            'estado' => 'Estado',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(MotMarca::className(), ['id' => 'id_marca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubPublicacions()
    {
        return $this->hasMany(PubPublicacion::className(), ['id_modelo' => 'id']);
    }
}
