<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mot_tipo".
 *
 * @property int $id
 * @property string $nombre
 * @property int $estado
 *
 * @property PubPublicacion[] $pubPublicacions
 */
class MotTipo extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'mot_tipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estado'], 'required'],
            [['estado'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubPublicacions()
    {
        return $this->hasMany(PubPublicacion::className(), ['id_tipo' => 'id']);
    }
}
