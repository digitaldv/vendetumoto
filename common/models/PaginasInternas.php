<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paginas_internas".
 *
 * @property int $id
 * @property int $pagina  '1' => 'Nosotros', '2' => 'Preguntas Frecuentes', '3' => 'Términos y condiciones', '4' => 'Política de privacidad','5' => 'Política de protección de datos', '6' => 'Seguridad'
 * @property string $titulo
 * @property string $contenido
 * @property string $archivo Nombre del archivo
 * @property string $fecha
 * @property int $foto 1: Sin Foto, 2: Foto IZQ, 3: Foto DER, 4: foto Centro arriba
 */
class PaginasInternas extends \yii\db\ActiveRecord
{
    public $paginas = [
        '1' => 'Nosotros',
        '2' => 'Preguntas Frecuentes',
        '3' => 'Términos y condiciones',
        '4' => 'Política de privacidad',
        '5' => 'Política de protección de datos',
        '6' => 'Seguridad',
        '7' => 'Vende más rápido',
        '8' => '¿Cómo publicar un anuncio?',
        '9' => 'Publicidad - Promociona tu empresa con nosotros',


    ];

    public static function tableName()
    {
        return 'paginas_internas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pagina', 'titulo', 'contenido', 'fecha', 'foto'], 'required'],
            [['pagina', 'foto'], 'integer'],
            [['contenido', 'archivo'], 'string'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 250],
            [['archivo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, gif, JPG, JPEG, PNG, GIF', 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pagina' => 'Pagina',
            'titulo' => 'Titulo',
            'contenido' => 'Contenido',
            'archivo' => 'Archivo',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
        ];
    }
}
