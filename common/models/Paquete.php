<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paquete".
 *
 * @property int $id
 * @property string $nombre Plan A, B, C
 * @property string $descripcion
 * @property int $precio
 * @property int $estado 0: INACTIVO , 1: ACTIVO
 * @property string $fecha_creacion
 * @property int $total_fotos Total de fotos x publicacion
 * @property int $total_videos Total de videos por Publicacion
 * @property int $total_anuncios Total de anuncios que podra publicar
 * @property int $dias_publicados Total de dias que debe estar publicado en la web
 *
 * @property Payments[] $payments
 */
class Paquete extends \yii\db\ActiveRecord
{
    public $_estado = [
        '1' => 'ACTIVO',
        '0' => 'INACTIVO',
    ];
    public static function tableName()
    {
        return 'paquete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion', 'precio', 'estado', 'fecha_creacion', 'total_fotos', 'total_videos', 'dias_publicados'], 'required'],
            [['descripcion'], 'string'],
            [['precio', 'estado', 'total_fotos', 'total_videos', 'total_anuncios', 'dias_publicados'], 'integer'],
            [['fecha_creacion'], 'safe'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'precio' => 'Precio',
            'estado' => 'Estado',
            'fecha_creacion' => 'Fecha Creación',
            'total_fotos' => 'Total Fotos',
            'total_videos' => 'Total Videos',
            'total_anuncios' => 'Total Anuncios',
            'dias_publicados' => 'Días Publicados',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['id_paquete' => 'id']);
    }
}
