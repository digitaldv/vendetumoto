<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $id_usuario usuario quien compro
 * @property int $id_paquete id_plan o paquete
 * @property int $dias_publicados dias del plan. se metio aca por que se podra consultar mas facil para crone
 * @property int $total_price
 * @property string $observations Observaciones
 * @property int $state 0: Intento de pago, 1: Aprovado
 * @property string $collection_id factura pago
 * @property string $preference_id referencia pago
 * @property string $external_reference Referencia externa quien pago
 * @property string $merchant_order_id orden generada
 * @property string $code Codigo de la transaccion
 * @property string $payment_method_id Método de pago
 * @property string $payment_type_id Tipo de pago
 * @property string $currency_id Moneda de pago
 * @property string $date_generate_order fecha de generar orden
 * @property string $date_generate_payment fecha de generar pago
 * @property string $date_approve_payment fecha de aprobar pago
 * @property string $created_at
 * @property string $updated_at
 * @property int $id_alegra id alegra
 * @property int $price_col Valor cobrado en pesos COP
 * @property string $transaction_code
 * @property string $transaction_state
 * @property int $id_publicacion
 *
 * @property User $usuario
 * @property Paquete $paquete
 * @property PubPublicacion $publicacion
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_publicacion'], 'required'],
            [['id_usuario', 'id_paquete', 'dias_publicados', 'total_price', 'state', 'id_alegra', 'price_col', 'id_publicacion'], 'integer'],
            [['date_generate_order', 'date_generate_payment', 'date_approve_payment', 'created_at', 'updated_at'], 'safe'],
            [['observations', 'collection_id', 'preference_id', 'external_reference', 'merchant_order_id'], 'string', 'max' => 255],
            [['code', 'transaction_code', 'transaction_state'], 'string', 'max' => 45],
            [['payment_method_id', 'payment_type_id'], 'string', 'max' => 50],
            [['currency_id'], 'string', 'max' => 10],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_paquete'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['id_paquete' => 'id']],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_paquete' => 'Id Paquete',
            'dias_publicados' => 'Dias Publicados',
            'total_price' => 'Total Price',
            'observations' => 'Observations',
            'state' => 'State',
            'collection_id' => 'Collection ID',
            'preference_id' => 'Preference ID',
            'external_reference' => 'External Reference',
            'merchant_order_id' => 'Merchant Order ID',
            'code' => 'Code',
            'payment_method_id' => 'Payment Method ID',
            'payment_type_id' => 'Payment Type ID',
            'currency_id' => 'Currency ID',
            'date_generate_order' => 'Date Generate Order',
            'date_generate_payment' => 'Date Generate Payment',
            'date_approve_payment' => 'Date Approve Payment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'id_alegra' => 'Id Alegra',
            'price_col' => 'Price Col',
            'transaction_code' => 'Transaction Code',
            'transaction_state' => 'Transaction State',
            'id_publicacion' => 'Id Publicacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaquete()
    {
        return $this->hasOne(Paquete::className(), ['id' => 'id_paquete']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }
}
