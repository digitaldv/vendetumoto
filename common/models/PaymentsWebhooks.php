<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payments_webhooks".
 *
 * @property int $id
 * @property string $json
 * @property string $create_at
 * @property int $platform_type
 */
class PaymentsWebhooks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_webhooks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['json', 'create_at', 'platform_type'], 'required'],
            [['json'], 'string'],
            [['create_at'], 'safe'],
            [['platform_type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'json' => 'Json',
            'create_at' => 'Create At',
            'platform_type' => 'Platform Type',
        ];
    }
}
