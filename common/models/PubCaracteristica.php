<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_caracteristica".
 *
 * @property int $id
 * @property int $id_caracteristica
 * @property int $id_publicacion
 *
 * @property MotCaracteristicas $caracteristica
 * @property PubPublicacion $publicacion
 */
class PubCaracteristica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_caracteristica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_caracteristica', 'id_publicacion'], 'required'],
            [['id_caracteristica', 'id_publicacion'], 'integer'],
            [['id_caracteristica'], 'exist', 'skipOnError' => true, 'targetClass' => MotCaracteristicas::className(), 'targetAttribute' => ['id_caracteristica' => 'id']],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_caracteristica' => 'Id Caracteristica',
            'id_publicacion' => 'Id Publicacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristica()
    {
        return $this->hasOne(MotCaracteristicas::className(), ['id' => 'id_caracteristica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }
}
