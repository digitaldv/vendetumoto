<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_denuncia".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $motivo 1: Robo, 2: Falsa informacion, 3: Moto robada, 4: Otros
 * @property string $mensaje
 * @property string $fecha
 * @property int $id_publicacion
 *
 * @property User $usuario
 * @property PubPublicacion $publicacion
 */
class PubDenuncia extends \yii\db\ActiveRecord
{
    //1: Robo, 2: Falsa informacion, 3: Moto robada, 4: Otros
    public $_motivo = [
        '1' => 'Los datos del vehiculo no coinciden con lo publicado.',
        '2' => 'El vehiculo ya no esta disponible.',
        '3' => 'Al parecer es un engañó o intento de fraude.',
        '4' => 'El vendedor me estafó.',
        '5' => 'El vehiculo publicado es mío',
        '6' => 'Otros',
    ];

    public static function tableName()
    {
        return 'pub_denuncia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'motivo', 'mensaje', 'fecha', 'id_publicacion'], 'required'],
            [['id_usuario', 'motivo', 'id_publicacion'], 'integer'],
            [['mensaje'], 'string'],
            [['fecha'], 'safe'],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'motivo' => 'Motivo',
            'mensaje' => 'Mensaje',
            'fecha' => 'Fecha',
            'id_publicacion' => 'Id Publicacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }
}
