<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_estadisticas".
 *
 * @property int $id
 * @property int $tipo 1: Visita, 2: Iteraccion, 3: Favorito, 4: Click Whatsapp, 5: Click Llamada
 * @property int $id_publicacion
 * @property int $id_usuario
 * @property string $ip
 * @property string $fecha
 * @property string $hora 23:09
 *
 * @property PubPublicacion $publicacion
 * @property User $usuario
 */
class PubEstadisticas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_estadisticas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'id_publicacion', 'ip', 'fecha', 'hora'], 'required'],
            [['tipo', 'id_publicacion', 'id_usuario'], 'integer'],
            [['fecha'], 'safe'],
            [['ip'], 'string', 'max' => 15],
            [['hora'], 'string', 'max' => 5],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'id_publicacion' => 'Id Publicacion',
            'id_usuario' => 'Id Usuario',
            'ip' => 'Ip',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }
}
