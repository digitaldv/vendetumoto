<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_favoritos".
 *
 * @property int $id
 * @property int $id_publicacion
 * @property int $id_usuario
 * @property string $fecha
 *
 * @property PubPublicacion $publicacion
 * @property User $usuario
 */
class PubFavoritos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_favoritos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_publicacion', 'id_usuario', 'fecha'], 'required'],
            [['id_publicacion', 'id_usuario'], 'integer'],
            [['fecha'], 'safe'],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_publicacion' => 'Id Publicacion',
            'id_usuario' => 'Id Usuario',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }
}
