<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_mensaje".
 *
 * @property int $id
 * @property int $id_papa
 * @property int $id_publicacion
 * @property int $id_creador id_usuario que escribio mensaje
 * @property string $fecha
 * @property string $mensaje mensaje
 * @property int $leido 0: No, 1: Si
 * @property string $fecha_leido
 * @property int $id_dueno_publicacion id_usuario quien creo la publicacion
*
 * @property PubPublicacion $publicacion
 * @property User $creador
 * @property PubMensaje[] $pubMensajes
 */
class PubMensaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_mensaje';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'id_papa','id_publicacion', 'id_creador', 'leido', 'id_dueno_publicacion'], 'integer'],
            [[ 'id_publicacion', 'id_creador', 'fecha', 'mensaje', 'id_dueno_publicacion'], 'required'],
            [['fecha', 'fecha_leido'], 'safe'],
            [['mensaje'], 'string'],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_papa' => 'Papa',
            'id_publicacion' => ' Publicación',
            'id_creador' => 'Creador',
            'fecha' => 'Fecha',
            'mensaje' => 'Mensaje',
            'leido' => 'Leído',
            'fecha_leido' => 'Fecha Leído',
        ];
    }


    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }


    public function getCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubMensajes()
    {
        return $this->hasMany(PubMensaje::className(), ['id_papa' => 'id']);
    }
}
