<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_multimedia".
 *
 * @property int $id
 * @property int $tipo 1: Foto, 2: Video youtube, 3: Video Vimeo
 * @property string $archivo
 * @property int $id_publicacion
 * @property string $fecha
 * @property string $url_video
 * @property int $img_order
 *
 * @property PubPublicacion $publicacion
 */
class PubMultimedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_multimedia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'id_publicacion', 'fecha'], 'required'],
            [['tipo', 'id_publicacion','img_order'], 'integer'],
            [['fecha'], 'safe'],
            [['archivo'], 'string', 'max' => 45],
            [['url_video'], 'string', 'max' => 200],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'archivo' => 'Archivo',
            'id_publicacion' => 'Id Publicacion',
            'fecha' => 'Fecha',
            'url_video' => 'Url Video',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }
}
