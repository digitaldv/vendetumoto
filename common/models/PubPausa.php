<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_pausa".
 *
 * @property int $id
 * @property int $id_publicacion
 * @property string $fecha
 *
 * @property PubPublicacion $publicacion
 */
class PubPausa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_pausa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_publicacion', 'fecha'], 'required'],
            [['id_publicacion'], 'integer'],
            [['fecha'], 'safe'],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_publicacion' => 'Id Publicacion',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }
}
