<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_publicacion".
 *
 * @property int $id
 * @property int $id_usuario id_usuario creador
 * @property int $id_tipo Moto, Cuatrimoto, Nautico, UVT, etc
 * @property int $id_modelo id_modelo
 * @property string $fecha_creacion
 * @property string $descripcion
 * @property double $precio
 * @property int $descuento Descuento en % porcentaje
 * @property string $placa
 * @property int $ano
 * @property int $estado 0: Inactivo, 1: Activo, 2: Pendiente x Aprobar, 3: Bloqueado, 4: Pausado, 5: Vendido, 6: Rechazado
 * @property int $recorrido 200
 * @property int $unid_recorrido 1: Kilometros, 2: Millas, 3: Horas
 * @property int $tipo_combustible 1: Gasolina, 2: Disel, 3: Gas, 4: Electrica
 * @property string $numero_contacto Oculto por ahora
 * @property string $whatsapp Oculto por ahora
 * @property int $id_ciudad ciudad donde se encuentra el vehiculo
 * @property string $fecha_edicion
 * @property string $ultima_renovacion ultima renovacion
 * @property int $tipo_vendedor 1: Directo, 2: Concesionario
 * @property int $cilindraje 600 cc, 350 cc - solo numero
 * @property int $condicion 1: Nueva, 2: Usada
 * @property string $slug url amigable en minuscula
 * @property int $vendido_por 1: Por vende tu moto, 2: Por Whatsapp, 3: Por email, 4: Por llamada telefonica
 * @property string $carpeta carpeta S3
 * @property string $vehiculo
 * @property int $dias_faltantes Saldo de días acumulados comprados en los planes que faltan por estar publicados.  Saldo de días acumulados comprados en los planes que faltan por estar publicados.
 * @property string $fecha_ult_compra se actualiza cuando hay una compra
 * @property int $id_plan_actual id_plan_actual
 *
 * @property Payments[] $payments
 * @property PubCaracteristica[] $pubCaracteristicas
 * @property PubDenuncia[] $pubDenuncias
 * @property PubEstadisticas[] $pubEstadisticas
 * @property PubFavoritos[] $pubFavoritos
 * @property PubMensaje[] $pubMensajes
 * @property PubMultimedia[] $pubMultimedia
 * @property PubPausa[] $pubPausas
 * @property User $usuario
 * @property MotTipo $tipo
 * @property MotModelo $modelo
 * @property City $ciudad
 * @property Paquete $planActual
 * @property PubRechazo[] $pubRechazos
 */
class PubPublicacion extends \yii\db\ActiveRecord
{
    /*
     * estado = 0 - INACTIVO', '1 - APROBADO','2 - PENDIENTE_X_APROBAR','3 - BLOQUEADO','4 - VENDIDO','5 - RECHAZADO', '6','6 - BORRADOR' , '7',PAUSADO, 8 - ELIMINADO
     */
    public $nameFull;
    public $ano_desde;
    public $ano_hasta;
    public $precio_desde;
    public $precio_hasta;

    public $_estado = [
        '0' => 'INACTIVO',
        '1' => 'ACTIVO',
        '2' => 'PENDIENTE x APROBAR',
        '3' => 'BLOQUEADO',
        '4' => 'VENDIDO',
        '5' => 'RECHAZADO',
        '6' => 'BORRADOR',
        '7' => 'PAUSADO',
        '8' => 'ELIMINADO',
    ];

    public $_vendido_por = [
        '1' => 'Por vende tu moto',
        '2' => 'Por Whatsapp',
        '3' => 'Por email',
        '4' => 'Por llamada telefónica',
    ];


    public $_ano = [2024=>2024, 	2023=>2023, 	2022=>2022, 	2021=>2021, 	2020=>2020, 	2019=>2019, 	2018=>2018, 	2017=>2017, 	2016=>2016, 	2015=>2015, 	2014=>2014, 	2013=>2013, 	2012=>2012, 	2011=>2011, 	2010=>2010, 	2009=>2009, 	2008=>2008, 	2007=>2007, 	2006=>2006, 	2005=>2005, 	2004=>2004, 	2003=>2003, 	2002=>2002, 	2001=>2001, 	2000=>2000, 	1999=>1999, 	1998=>1998, 	1997=>1997, 	1996=>1996, 	1995=>1995, 	1994=>1994, 	1993=>1993, 	1992=>1992, 	1991=>1991, 	1990=>1990, 	1989=>1989, 	1988=>1988, 	1987=>1987, 	1986=>1986, 	1985=>1985, 	1984=>1984, 	1983=>1983, 	1982=>1982, 	1981=>1981, 	1980=>1980, 	1979=>1979, 	1978=>1978, 	1977=>1977, 	1976=>1976, 	1975=>1975, 	1974=>1974, 	1973=>1973, 	1972=>1972, 	1971=>1971, 	1970=>1970, 	1969=>1969, 	1968=>1968, 	1967=>1967, 	1966=>1966, 	1965=>1965, 	1964=>1964, 	1963=>1963, 	1962=>1962, 	1961=>1961, 	1960=>1960, 	1959=>1959, 	1958=>1958, 	1957=>1957, 	1956=>1956, 	1955=>1955, 	1954=>1954, 	1953=>1953, 	1952=>1952, 	1951=>1951, 	1950=>1950];
    public $_unid_recorrido = [1 => 'Kms', 	2 => 'Millas', 	3 => 'Horas'];
    public $_tipo_combustible = [2 => 'Disel', 4 => 'Eléctrica', 3 => 'Gas', 1 => 'Gasolina', 5 => 'Híbrido'];
    public $_condicion = [1 => 'Nueva', 	2 => 'Usada'];
    public $_tipo_vendedor = [1 => 'Directo', 	2 => 'Concesionario'];

    public $id_marca;
    public $archivos;
    public $orden;
    public $_orden = [
        '1' => 'Mayor precio',
        '2' => 'Menor precio',
        '3' => 'Últimas publicadas',

    ];

    public $items_x_pag;
    public $_items_x_pag = [
        '2' => '2',
        '5' => '5',
        '10' => '10',
        '20' => '20',
        '50' => '50',
    ];


    public static function tableName()
    {
        return 'pub_publicacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_tipo', 'id_modelo', 'fecha_creacion', 'descripcion', 'precio', 'ano', 'estado', 'recorrido', 'unid_recorrido', 'tipo_combustible', 'id_ciudad', 'fecha_edicion'/*, 'tipo_vendedor'*/, 'cilindraje', 'condicion', 'slug', 'numero_contacto', 'whatsapp'], 'required'],
            [['id_usuario', 'id_tipo', 'id_modelo', 'descuento', 'ano', 'estado', 'unid_recorrido', 'tipo_combustible', 'id_ciudad', 'tipo_vendedor', 'condicion', 'vendido_por', 'orden', 'dias_faltantes'], 'integer'],
            //[['placa'],'unique','message'=>'¡Esa PLACA YA existe, no puedes usarla!'],
            [['fecha_creacion', 'fecha_edicion', 'ultima_renovacion', 'fecha_ult_compra'], 'safe'],
            [['descripcion', 'recorrido', 'cilindraje'], 'string'],
            [['vehiculo'], 'string'],
            [['precio','precio_desde','precio_hasta'], 'number'],
            [['placa'], 'string', 'max' => 6],
            [['numero_contacto', 'carpeta'], 'string', 'max' => 45],
            [['whatsapp'], 'string', 'max' => 10],
            [['slug'], 'string', 'max' => 200],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_tipo'], 'exist', 'skipOnError' => true, 'targetClass' => MotTipo::className(), 'targetAttribute' => ['id_tipo' => 'id']],
            [['id_modelo'], 'exist', 'skipOnError' => true, 'targetClass' => MotModelo::className(), 'targetAttribute' => ['id_modelo' => 'id']],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_ciudad' => 'id']],
            [['id_plan_actual'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['id_plan_actual' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Usuario',
            'id_tipo' => 'Tipo',
            'id_modelo' => 'Modelo',
            'fecha_creacion' => 'Fecha Creación',
            'descripcion' => 'Descripción',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'placa' => 'Placa',
            'ano' => 'Año',
            'estado' => 'Estado',
            'recorrido' => 'Recorrido',
            'unid_recorrido' => 'Und.',
            'tipo_combustible' => 'Tipo Combustible',
            'numero_contacto' => 'Número Contacto',
            'whatsapp' => 'Whatsapp',
            'id_ciudad' => 'Ciudad',
            'fecha_edicion' => 'Fecha Edición',
            'ultima_renovacion' => 'Última Renovación',
            'tipo_vendedor' => 'Tipo Vendedor',
            'cilindraje' => 'Cilindraje',
            'condicion' => 'Condición',
            'slug' => 'Slug',
            'vendido_por' => 'Vendido Por',
            'carpeta' => 'Carpeta',
            'vehiculo' => 'Vehículo',
            'dias_faltantes' => 'Días Faltantes',
            'fecha_ult_compra' => 'Fecha Ult Compra',
            'id_plan_actual' => 'Plan Actual',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubCaracteristicas()
    {
        return $this->hasMany(PubCaracteristica::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubDenuncias()
    {
        return $this->hasMany(PubDenuncia::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubEstadisticas()
    {
        return $this->hasMany(PubEstadisticas::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubFavoritos()
    {
        return $this->hasMany(PubFavoritos::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubMensajes()
    {
        return $this->hasMany(PubMensaje::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubMultimedia()
    {
        return $this->hasMany(PubMultimedia::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubPausas()
    {
        return $this->hasMany(PubPausa::className(), ['id_publicacion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(MotTipo::className(), ['id' => 'id_tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelo()
    {
        return $this->hasOne(MotModelo::className(), ['id' => 'id_modelo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(City::className(), ['id' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanActual()
    {
        return $this->hasOne(Paquete::className(), ['id' => 'id_plan_actual']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubRechazos()
    {
        return $this->hasMany(PubRechazo::className(), ['id_publicacion' => 'id']);
    }

    public function afterFind()
    {
        $this->nameFull = $this->tipo->nombre.' '.$this->modelo->marca->nombre.' '.$this->modelo->nombre;
    }

}
