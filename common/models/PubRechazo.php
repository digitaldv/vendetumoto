<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pub_rechazo".
 *
 * @property int $id
 * @property int $id_publicacion
 * @property string $motivo
 * @property string $fecha
 * @property int $id_usuario usr que rechazo
 *
 * @property PubPublicacion $publicacion
 * @property User $usuario
 */
class PubRechazo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pub_rechazo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_publicacion', 'motivo', 'fecha', 'id_usuario'], 'required'],
            [['id_publicacion', 'id_usuario'], 'integer'],
            [['motivo'], 'string'],
            [['fecha'], 'safe'],
            [['id_publicacion'], 'exist', 'skipOnError' => true, 'targetClass' => PubPublicacion::className(), 'targetAttribute' => ['id_publicacion' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_publicacion' => 'Id Publicacion',
            'motivo' => 'Motivo',
            'fecha' => 'Fecha',
            'id_usuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacion()
    {
        return $this->hasOne(PubPublicacion::className(), ['id' => 'id_publicacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }
}
