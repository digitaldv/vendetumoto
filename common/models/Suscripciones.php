<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "suscripciones".
 *
 * @property int $id
 * @property string $email
 * @property string $fecha
 */
class Suscripciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suscripciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'fecha'], 'required'],
            [['fecha'], 'safe'],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'fecha' => 'Fecha',
        ];
    }
}
