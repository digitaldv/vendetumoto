<?php
namespace common\models;

use common\components\Utils;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $confirmation_token
 * @property int $status 1:Activo, 2: Inactivo, 3:Eliminado
 * @property int $superadmin
 * @property int $created_at
 * @property int $updated_at
 * @property string $registration_ip
 * @property string $bind_to_ip
 * @property string $email
 * @property int $email_confirmed
 * @property string $pass
 * @property string $code
 * @property string $code_change
 * @property string $password_reset_token

 *
 * @property ArtArticulo[] $artArticulos
 * @property ArtArticulo[] $artArticulos0
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property CliUsuario[] $cliUsuarios
 * @property Empleado[] $empleados
 * @property Empleado[] $empleados0
 * @property Payments[] $payments
 * @property PubDenuncia[] $pubDenuncias
 * @property PubEstadisticas[] $pubEstadisticas
 * @property PubFavoritos[] $pubFavoritos
 * @property PubMensaje[] $pubMensajes
 * @property PubPublicacion[] $pubPublicacions
 * @property PubRechazo[] $pubRechazos
 */

class User extends ActiveRecord // implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public $password_hash;
    public $password_reset_token;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            [['username', 'auth_key', 'password_hash', 'created_at', 'updated_at'], 'required'],
            [['status', 'superadmin', 'created_at', 'updated_at', 'email_confirmed'], 'integer'],
            [['username', 'password_hash', 'confirmation_token', 'bind_to_ip', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip', 'code', 'code_change'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 128],
            [['pass'], 'string', 'max' => 45],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        $modUser =  static::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE, ]);
        //Utils::dumpx($modUser);
        return $modUser;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);

        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getArtArticulos()
    {
        return $this->hasMany(ArtArticulo::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtArticulos0()
    {
        return $this->hasMany(ArtArticulo::className(), ['id_editor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliUsuarios()
    {
        return $this->hasMany(CliUsuario::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleado::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados0()
    {
        return $this->hasMany(Empleado::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubDenuncias()
    {
        return $this->hasMany(PubDenuncia::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubEstadisticas()
    {
        return $this->hasMany(PubEstadisticas::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubFavoritos()
    {
        return $this->hasMany(PubFavoritos::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubMensajes()
    {
        return $this->hasMany(PubMensaje::className(), ['id_creador' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubPublicacions()
    {
        return $this->hasMany(PubPublicacion::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPubRechazos()
    {
        return $this->hasMany(PubRechazo::className(), ['id_usuario' => 'id']);
    }


}
