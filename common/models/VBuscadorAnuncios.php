<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "v_buscador_anuncios".
 *
 * @property string $tipo
 * @property int $id_marca
 * @property string $nombre
 * @property string $vehiculo
 * @property string $ciudad
 * @property string $placa_ano
 * @property double $precio_desc
 * @property string $recor_comb
 * @property string $recorrido_full
 * @property string $caracteristicas
 * @property string $estado_pub
 * @property string $usuario
 * @property string $num_contacto
 * @property string $tipo_vendedor_full
 * @property int $cilindraje_full 600 cc, 350 cc - solo numero
 * @property string $condicion_full
 * @property string $vendido_por_full
 * @property int $id
 * @property int $id_usuario id_usuario creador
 * @property int $id_tipo Moto, Cuatrimoto, Nautico, UVT, etc
 * @property int $id_modelo id_modelo
 * @property string $fecha_creacion
 * @property string $descripcion
 * @property double $precio
 * @property int $descuento Descuento en % porcentaje
 * @property string $placa
 * @property int $ano
 * @property int $estado 0: Inactivo, 1: Activo, 2: Pendiente x Aprobar, 3: Bloqueado, 4: Pausado, 5: Vendido, 6: Rechazado
 * @property int $recorrido 200
 * @property int $unid_recorrido 1: Kilometros, 2: Millas, 3: Horas
 * @property int $tipo_combustible 1: Gasolina, 2: Disel, 3: Gas, 4: Electrica
 * @property string $numero_contacto Oculto por ahora
 * @property string $whatsapp Oculto por ahora
 * @property int $id_ciudad ciudad donde se encuentra el vehiculo
 * @property string $fecha_edicion
 * @property string $ultima_renovacion ultima renovacion
 * @property int $tipo_vendedor 1: Directo, 2: Concesionario
 * @property int $cilindraje 600 cc, 350 cc - solo numero
 * @property int $condicion 1: Nueva, 2: Usada
 * @property string $slug url amigable en minuscula
 * @property int $vendido_por 1: Por vende tu moto, 2: Por Whatsapp, 3: Por email, 4: Por llamada telefonica
 * @property string $carpeta carpeta S3
 */
class VBuscadorAnuncios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'v_buscador_anuncios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'nombre', 'cilindraje_full', 'id_usuario', 'id_tipo', 'id_modelo', 'fecha_creacion', 'descripcion', 'precio', 'placa', 'ano', 'estado', 'recorrido', 'unid_recorrido', 'tipo_combustible', 'id_ciudad', 'fecha_edicion', 'tipo_vendedor', 'cilindraje', 'condicion', 'slug'], 'required'],
            [['id_marca', 'cilindraje_full', 'id', 'id_usuario', 'id_tipo', 'id_modelo', 'descuento', 'ano', 'estado', 'recorrido', 'unid_recorrido', 'tipo_combustible', 'id_ciudad', 'tipo_vendedor', 'cilindraje', 'condicion', 'vendido_por'], 'integer'],
            [['precio_desc', 'precio'], 'number'],
            [['recor_comb', 'recorrido_full', 'caracteristicas', 'estado_pub', 'tipo_vendedor_full', 'vendido_por_full', 'descripcion'], 'string'],
            [['fecha_creacion', 'fecha_edicion', 'ultima_renovacion'], 'safe'],
            [['tipo', 'nombre', 'numero_contacto', 'carpeta'], 'string', 'max' => 45],
            [['vehiculo'], 'string', 'max' => 141],
            [['ciudad'], 'string', 'max' => 511],
            [['placa_ano'], 'string', 'max' => 20],
            [['usuario'], 'string', 'max' => 203],
            [['num_contacto'], 'string', 'max' => 56],
            [['condicion_full'], 'string', 'max' => 275],
            [['placa'], 'string', 'max' => 6],
            [['whatsapp'], 'string', 'max' => 10],
            [['slug'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tipo' => 'Tipo',
            'id_marca' => 'Id Marca',
            'nombre' => 'Nombre',
            'vehiculo' => 'Vehiculo',
            'ciudad' => 'Ciudad',
            'placa_ano' => 'Placa Ano',
            'precio_desc' => 'Precio Desc',
            'recor_comb' => 'Recor Comb',
            'recorrido_full' => 'Recorrido Full',
            'caracteristicas' => 'Caracteristicas',
            'estado_pub' => 'Estado Pub',
            'usuario' => 'Usuario',
            'num_contacto' => 'Num Contacto',
            'tipo_vendedor_full' => 'Tipo Vendedor Full',
            'cilindraje_full' => 'Cilindraje Full',
            'condicion_full' => 'Condicion Full',
            'vendido_por_full' => 'Vendido Por Full',
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_tipo' => 'Id Tipo',
            'id_modelo' => 'Id Modelo',
            'fecha_creacion' => 'Fecha Creacion',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'placa' => 'Placa',
            'ano' => 'Ano',
            'estado' => 'Estado',
            'recorrido' => 'Recorrido',
            'unid_recorrido' => 'Unid Recorrido',
            'tipo_combustible' => 'Tipo Combustible',
            'numero_contacto' => 'Numero Contacto',
            'whatsapp' => 'Whatsapp',
            'id_ciudad' => 'Id Ciudad',
            'fecha_edicion' => 'Fecha Edicion',
            'ultima_renovacion' => 'Ultima Renovacion',
            'tipo_vendedor' => 'Tipo Vendedor',
            'cilindraje' => 'Cilindraje',
            'condicion' => 'Condicion',
            'slug' => 'Slug',
            'vendido_por' => 'Vendido Por',
            'carpeta' => 'Carpeta',
        ];
    }
}
