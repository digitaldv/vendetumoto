<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "v_buscador_publicaciones".
 *
 * @property int $id
 * @property string $fecha_creacion
 * @property string $vehiculo
 * @property string $ciudad
 * @property string $descripcion
 * @property double $precio
 * @property int $descuento Descuento en % porcentaje
 * @property string $placa_ano
 * @property string $recor_comb
 * @property string $estado
 * @property string $usuario
 * @property string $numero_contacto
 * @property string $tipo_vendedor
 * @property int $cilindraje 600 cc, 350 cc - solo numero
 * @property string $condicion
 * @property string $slug url amigable en minuscula
 * @property string $vendido_por
 * @property int $estado_pub 0: Inactivo, 1: Activo, 2: Pendiente x Aprobar, 3: Bloqueado, 4: Pausado, 5: Vendido, 6: Rechazado
 * @property int $id_usuario id_usuario creador
 */
class VBuscadorPublicaciones extends \yii\db\ActiveRecord
{
    public $_estado = [
        '0' => 'INACTIVO',
        '1' => 'ACTIVO',
        '2' => 'PENDIENTE x APROBAR',
        '3' => 'BLOQUEADO',
        '4' => 'VENDIDO',
        '5' => 'RECHAZADO',
        '6' => 'BORRADOR',
        '7' => 'PAUSADO',
        '8' => 'ELIMINADO',
    ];

    public $palabras_search;

    public static function tableName()
    {
        return 'v_buscador_publicaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'descuento', 'cilindraje', 'estado_pub', 'id_usuario'], 'integer'],
          //  [['fecha_creacion', 'ciudad', 'descripcion', 'precio', 'cilindraje', 'slug', 'estado_pub', 'id_usuario'], 'required'],
            [['fecha_creacion'], 'safe'],
            [['descripcion', 'recor_comb', 'estado', 'tipo_vendedor', 'vendido_por'], 'string'],
            [['precio'], 'number'],
            [['vehiculo'], 'string', 'max' => 141],
            [['ciudad'], 'string', 'max' => 255],
            [['placa_ano'], 'string', 'max' => 20],
            [['usuario'], 'string', 'max' => 203],
            [['numero_contacto'], 'string', 'max' => 56],
            [['condicion'], 'string', 'max' => 275],
            [['slug'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_creacion' => 'Fecha Creacion',
            'vehiculo' => 'Vehiculo',
            'ciudad' => 'Ciudad',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'placa_ano' => 'Placa Ano',
            'recor_comb' => 'Recor Comb',
            'estado' => 'Estado',
            'usuario' => 'Usuario',
            'numero_contacto' => 'Numero Contacto',
            'tipo_vendedor' => 'Tipo Vendedor',
            'cilindraje' => 'Cilindraje',
            'condicion' => 'Condicion',
            'slug' => 'Slug',
            'vendido_por' => 'Vendido Por',
            'estado_pub' => 'Estado Pub',
        ];
    }
}
