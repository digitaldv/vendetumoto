<?php

namespace ddddd\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property integer $id
 * @property integer $id_pago
 * @property integer $id_usuario
 * @property integer $id_curso
 * @property integer $id_contenido
 * @property integer $total_price
 * @property integer $state
 * @property string $code
 *
 * @property User $idUsuario
 * @property CurCurso $idCurso
 * @property ContenidoFree $idContenido
 * @property PaymentsTotal $idPago
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pago', 'id_usuario', 'total_price', 'state', 'code'], 'required'],
            [['id_pago', 'id_usuario', 'id_curso', 'id_contenido', 'total_price', 'state'], 'integer'],
            [['code'], 'string', 'max' => 45],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_curso'], 'exist', 'skipOnError' => true, 'targetClass' => CurCurso::className(), 'targetAttribute' => ['id_curso' => 'id']],
            [['id_contenido'], 'exist', 'skipOnError' => true, 'targetClass' => ContenidoFree::className(), 'targetAttribute' => ['id_contenido' => 'id']],
            [['id_pago'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsTotal::className(), 'targetAttribute' => ['id_pago' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pago' => 'Id Pago',
            'id_usuario' => 'Id Usuario',
            'id_curso' => 'Id Curso',
            'id_contenido' => 'Id Contenido',
            'total_price' => 'Total Price',
            'state' => 'State',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurso()
    {
        return $this->hasOne(CurCurso::className(), ['id' => 'id_curso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdContenido()
    {
        return $this->hasOne(ContenidoFree::className(), ['id' => 'id_contenido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPago()
    {
        return $this->hasOne(PaymentsTotal::className(), ['id' => 'id_pago']);
    }
}
