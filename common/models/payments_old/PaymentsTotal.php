<?php

namespace common\models;

/**
 * This is the model class for table "payments_total".
 *
 * @property integer $id
 * @property integer $id_usuario
 * @property integer $total_price
 * @property string $observations
 * @property integer $state
 * @property string $collection_id
 * @property string $preference_id
 * @property string $external_reference
 * @property string $merchant_order_id
 * @property string $code
 * @property string $payment_method_id
 * @property string $payment_type_id
 * @property string $currency_id
 * @property string $date_generate_order
 * @property string $date_generate_payment
 * @property string $date_approve_payment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $id_alegra
 * @property string $price_col
 * @property string $transaction_code
 * @property string $transaction_state
 * 
 *
 * @property Payments[] $payments
 * @property User $idUsuario
 */
class PaymentsTotal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_total';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_usuario', 'total_price', 'state', 'id_alegra'], 'integer'],
            [['date_generate_order', 'date_generate_payment', 'date_approve_payment', 'created_at', 'updated_at'], 'safe'],
            [['observations', 'collection_id', 'preference_id', 'external_reference', 'merchant_order_id'], 'string', 'max' => 255],
            [['code', 'price_col', 'transaction_code', 'transaction_state'], 'string', 'max' => 45],
            [['payment_method_id', 'payment_type_id'], 'string', 'max' => 50],
            [['currency_id'], 'string', 'max' => 10],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'total_price' => 'Total Price',
            'observations' => 'Observations',
            'state' => 'State',
            'collection_id' => 'Collection ID',
            'preference_id' => 'Preference ID',
            'external_reference' => 'External Reference',
            'merchant_order_id' => 'Merchant Order ID',
            'code' => 'Code',
            'payment_method_id' => 'Payment Method ID',
            'payment_type_id' => 'Payment Type ID',
            'currency_id' => 'Currency ID',
            'date_generate_order' => 'Date Generate Order',
            'date_generate_payment' => 'Date Generate Payment',
            'date_approve_payment' => 'Date Approve Payment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'id_alegra' => 'Id Alegra',
            'price_col' => 'Price Col',
            'transaction_code' => 'transaction code',
            'transaction_state' => 'transaction state',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['id_pago' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usuario']);
    }
}