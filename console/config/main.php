<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',

            // 'enableRegistration' => true,

            // Here you can set your handler to change layout for any controller or action
            // Tip: you can use this event in any module
            'on beforeAction'=>function(yii\base\ActionEvent $event) {
                    if ( $event->action->uniqueId == 'user-management/auth/login' )
                    {
                        $event->action->controller->layout = 'loginLayout.php';
                    };
                },
        ],
    ],
    'components' => [
        'request' => [
            //'csrfParam' => '_csrf-frontend',
            'class' => 'common\components\Request',
            'web'=> '/frontend/web',
            'cookieValidationKey' => 'tuClave',
            'enableCsrfValidation' => false,
        ],
        'urlManager' => [
           'class' => 'yii\web\UrlManager',
           'baseUrl' => $baseUrl,
           'enablePrettyUrl' => true,
           'showScriptName' => false,
           'rules' => [
               '' => 'site/index',
//               '<action>'=>'site/<action>',
//               '<controller>s' => '<controller>/index',
//               '<controller>/<id:\d+>s' => '<controller>/view',
               /********************************************************************************************/
               'blog' => 'site/blog',
               'blog/<url>' => 'site/getarticulo',
               'blog/categoria/<url>' => 'site/getcategoriablog',
               'blog/tag/<url>' => 'site/gettagblog',
               'producto/<url>' => 'site/getproducto',
               'acceder' => 'site/login',
               'como-funciona' => 'site/comofunciona',
               'quienes-somos' => '/site/quienessomos',
               'especiales' => 'site/especiales',
               'proximos-lanzamientos' => 'site/lanzamientos',
               'politica-cookies' => 'site/cookies',
               'aviso-legal' => 'site/avisolegal',
               'contacto' => 'site/contacto',
               'politica-privacidad' => 'site/privacidad',
               'registrarse' => 'site/registrousuario',
               'bienvenido' => 'site/bienvenido',
               // 'resultados' => 'site/resultadosbusqueda',
               'cursos' => 'site/allcourses',
               // 'cursos' => 'site/cursos',
               'detalle-curso' => 'site/detallecurso',
               'empresas' => 'site/empresas',
               'descargas' => 'site/descargas',
               'resultados' => 'site/resultados',
               'carrito' => 'site/carrito',
               'mis-compras' => 'site/miscursos',
               'contenido-curso' => 'site/cursocontenidos',
               //'curso-detalle' => 'site/cursocomprado',
               'pago-realizado' => 'site/pagorealizado',
               'empleados-empresa' => 'site/empresaempleados',


               /****** Enlaces usuario Logueado ***********/
               'mi-perfil' => 'site/perfilcliente',
               'perfil-proveedor/<url>' => 'site/perfilproveedor',
               'mis-deseos' => 'site/misdeseos',
               'mis-alertas' => 'site/misalertas',
               'mis-colecciones' => 'site/miscolecciones',
               'perfil-experto' => 'site/perfilexperto',
               'notificaciones' => 'site/notificaciones',
               'expertos' => 'site/expertos',
               'experto' => 'site/experto',
               'cursos-empresas' => 'site/cursosempresas',


               /********************************************************************************************/
           ],
        ],
        'user' => [
            'identityClass' => 'webvimark\modules\UserManagement\models\User',
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',

            // Comment this if you don't want to record user logins
            'on afterLogin' => function($event) {
                    \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
                }
        ],

        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'view' => [
             'theme' => [
                 'pathMap' => [
                    //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                    '@app/views' => '@app/views'
                 ],
             ],
        ],

        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-green',
                ],
            ],
        ],

//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'viewPath' => '@common/mail',
//            'useFileTransport' => true,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'wokeer@gmail.com',
//                'password' => 'jesusbley1978',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
//
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'no-responder@vigpro.com',
                'password' => 'Asdqwe123#',
                'port' => '587',
                'encryption' => 'tls',
            ],

        ],
    ],
    'params' => $params,
];
