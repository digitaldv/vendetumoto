<?php
namespace console\controllers;

use common\models\CliCliente;
use common\models\CliUsuario;
use common\models\Empleado;
use common\models\ProcUsuarioAdicional;
use common\models\ProyCliente;
use common\models\VFormatoFallidaSensibilizacion;
use common\models\VFormatoSensibilizacion;
use Yii;
use yii\helpers\Url;
use yii\console\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\Utils;
use yii\data\SqlDataProvider;
use Swift_MailTransport;
use common\models\ProcActuacion;
use common\models\CliClienteConfig;
use common\models\RequestEvidences;
use common\models\ProcProceso;
use common\models\Requests;
use kartik\mpdf\Pdf;


class CronesController extends Controller {


    public function actionIndex() {
        echo "Hola eeeeeeeeee Chris este es el INDEX!!!! \n";
    }


    /* primero creamos la función que hace la magia
 * esta funcion recorre carpetas y subcarpetas
 * añadiendo todo archivo que encuentre a su paso
 * recibe el directorio y el zip a utilizar
 */
    function agregar_zip($dir, $zip) {
        //verificamos si $dir es un directorio
        if (is_dir($dir)) {
            //abrimos el directorio y lo asignamos a $da
            if ($da = opendir($dir)) {
                //leemos del directorio hasta que termine
                while (($archivo = readdir($da)) !== false) {
                    /*Si es un directorio imprimimos la ruta
                     * y llamamos recursivamente esta función
                     * para que verifique dentro del nuevo directorio
                     * por mas directorios o archivos
                     */
                    if (is_dir($dir . $archivo) && $archivo != "." && $archivo != "..") {
                        //echo "<strong>Creando directorio: $dir$archivo</strong><br/>";
                        system('echo -e "- Creando directorio: '.$dir.$archivo.' \n   "');
                        $this->agregar_zip($dir . $archivo . "/", $zip);

                        /*si encuentra un archivo imprimimos la ruta donde se encuentra
                         * y agregamos el archivo al zip junto con su ruta
                         */
                    } elseif (is_file($dir . $archivo) && $archivo != "." && $archivo != "..") {
                        //echo "Agregando archivo: $dir$archivo <br/>";
                        system('echo -e "- Agregando archivo: '.$dir.$archivo.' \n   "');
                        $zip->addFile($dir . $archivo, $dir . $archivo);
                    }
                }
                //cerramos el directorio abierto en el momento
                closedir($da);
            }
        }
    }


    public function actionComprimir_backup_dia(){

        $fecha_hoy = date('Y-m-d');
        $clientes = CliCliente::find()->where('estado=1')->all();
        foreach ($clientes as $cli) {
            $carpeta = $cli->carpeta;

            //creamos una instancia de ZipArchive
            $zip = new \ZipArchive();

            /*directorio a comprimir
             * la barra inclinada al final es importante
             * la ruta debe ser relativa no absoluta
             */
            $dir = dirname(__DIR__) . '/../backend/web/uploads/clientes/' . $carpeta . '/backups_diarios/'.$fecha_hoy.'/';

            //ruta donde guardar los archivos zip, ya debe existir
            $rutaFinal = dirname(__DIR__) . '/../backend/web/uploads/clientes/' . $carpeta . '/backups_diarios/';

            if (!file_exists($rutaFinal)) {
                mkdir($rutaFinal);
            }

            $archivoZip = $fecha_hoy."_backup.zip";

            if ($zip->open($archivoZip, \ZIPARCHIVE::CREATE) === true) {
                $this->agregar_zip($dir, $zip);
                $zip->close();

                //Muevo el archivo a una ruta
                //donde no se mezcle los zip con los demas archivos
                 rename($archivoZip, "$rutaFinal/$archivoZip");

                //Hasta aqui el archivo zip ya esta creado
                //Verifico si el archivo ha sido creado
                if (file_exists($rutaFinal . "/" . $archivoZip)) {
                    $link_descarga  = "<a href='$rutaFinal/$archivoZip'>$archivoZip</a>";
                    system('echo -e "- OK ZIP CREADO : '.$link_descarga.' \n   "');
                } else {
                    system('echo -e "- ERROR : NO SE CREÓ EL ZIP \n   "');
                }
            }
        }//finFOR

    }

    function rmDir_rf()
    {
        $fecha_hoy = date('Y-m-d');
        $clientes = CliCliente::find()->where('estado=1')->all();
        foreach ($clientes as $cli) {
            $carpeta = $cli->carpeta;
            $url_temp = dirname(__DIR__) . '/../backend/web/uploads/clientes/'.$carpeta.'/backups_diarios/'.$fecha_hoy.'/';
            //$dir='tu/directorio';
            exec('rm -rf '.escapeshellarg($url_temp));
        }
    }

    public function url_exists( $url = NULL ) {

        if( empty( $url ) ){
            return false;
        }

        $ch = curl_init( $url );

        // Establecer un tiempo de espera
        curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );

        // Establecer NOBODY en true para hacer una solicitud tipo HEAD
        curl_setopt( $ch, CURLOPT_NOBODY, true );
        // Permitir seguir redireccionamientos
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        // Recibir la respuesta como string, no output
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Descomentar si tu servidor requiere un user-agent, referrer u otra configuración específica
        // $agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36';
        // curl_setopt($ch, CURLOPT_USERAGENT, $agent)

        $data = curl_exec( $ch );

        // Obtener el código de respuesta
        $httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        //cerrar conexión
        curl_close( $ch );

        // Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
        $accepted_response = array( 200, 301, 302 );
        if( in_array( $httpcode, $accepted_response ) ) {
            return true;
        } else {
            return false;
        }

    }

    public function actionBackupdiariovisitas()
    {
        $c=1;
        $clientes = CliCliente::find()->where('estado=1')->all();

        $fecha_hoy = date('Y-m-d');

        foreach ($clientes as $cli){
            $carpeta = $cli->carpeta;
            $url_temp = dirname(__DIR__) . '/../backend/web/uploads/clientes/'.$carpeta.'/backups_diarios/'.$fecha_hoy.'/';
            //Se crea la carpeta del DÍA ACTUAL
            if(!file_exists(Yii::getAlias($url_temp))){
                mkdir(Yii::getAlias($url_temp), 0777, true);
                system("chown deploy " . Yii::getAlias($url_temp.'/'));
                exec('chmod -R 0777 '. Yii::getAlias($url_temp.'/'));
            }else
                system('echo -e "-NO EXISTE LA RUTA : $url_temp \n   "');
            //Se crea FORMATO SENSIBILIZACIÓN
            if(!file_exists(Yii::getAlias($url_temp.'FORMATOS_SENSIBILIZACION/'))) {
                mkdir(Yii::getAlias($url_temp.'FORMATOS_SENSIBILIZACION/'), 0777, true);
                system("chown deploy " . Yii::getAlias($url_temp . 'FORMATOS_SENSIBILIZACION/'));
                exec('chmod -R 0777 ' . Yii::getAlias($url_temp . 'FORMATOS_SENSIBILIZACION/'));
            }

            //Se crea FORMATO FALLIDOS
            if(!file_exists(Yii::getAlias($url_temp.'FORMATOS_FALLIDOS/'))) {
                mkdir(Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/'), 0777, true);
                system("chown deploy " . Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/'));
                exec('chmod -R 0777 ' . Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/'));
            }

            //***********************************************************************************************************************
            // SENSIBILIZACIÓN  *****************************************************************************************************
            //***********************************************************************************************************************
            //1. FORMATOS_SENSIBILIZACION [APROBADOS]  and fecha_visita>="'.$fecha_hoy.'"
            $formatosSensib = VFormatoSensibilizacion::find()->where('estado=2'/* and fecha_visita>="'.$fecha_hoy.'"'*/)->all();
            foreach($formatosSensib as $fs){
                $id = $fs->id;

                //Creo carpeta CUS
                if(!file_exists(Yii::getAlias($url_temp.'FORMATOS_SENSIBILIZACION/'.$fs->cus.'/'))) {
                    mkdir(Yii::getAlias($url_temp . 'FORMATOS_SENSIBILIZACION/'.$fs->cus.'/'), 0777, true);
                    system("chown deploy " . Yii::getAlias($url_temp . 'FORMATOS_SENSIBILIZACION/' . $fs->cus . '/'));
                    exec('chmod -R 0777 ' . Yii::getAlias($url_temp . 'FORMATOS_SENSIBILIZACION/' . $fs->cus . '/'));
                }

                $formato =Yii::$app->db->createCommand(

                    "SELECT fs.id, cc.nombre as cliente,pp.nombre as proyecto,  
                concat(e.nombres,' ',e.apellidos) as gestor, concat(pc.nombres,' ',pc.apellidos, ' - [DIR] : ',pc.direccion,' - [Cel] : ',pc.celular) as usuario_predio_original, 
                fs.id_proy_cliente, fs.cus,  DATE_FORMAT(fs.fecha_visita,'%d - %m - %Y') AS fecha_visita, fs.nombre_cliente, 
                concat( replace(replace(replace(fs.tipo_documento,'1','CC'),'2','CE'), '3','P') ) AS tipo_documento, 
                fs.documento, fs.fecha_nacimiento, 
                CASE WHEN fs.edad <=17 THEN '0' ELSE fs.edad END as edad,
    
                concat( replace( replace( replace( replace( replace( replace(fs.grupo_etnico,'1','1'),'2','2'),'3','3'), '4','4'), '5','5'), '6','6')  ) AS grupo_etnico, 
    
                concat( replace(replace(replace(fs.genero,'1','1'),'2','2'),'3','3') ) AS genero, 
    
                concat( 
                    replace( replace( replace( replace( replace( replace( replace( replace( replace(replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace(fs.ocupacion,'1','1'), 
                    '2','2') ,  '3','3'), '4','4'),  '5','5'), '6','6'), '7', '7'), '8','8'),
                    '9','9'), '10','10'), '11','11'), '12','12'), '13','13'), '14','14'), 
                    '15','15'), '16','16'), '17','17'), '18','18'), '19','19'), '20','20'), '21','21'), 
                    '22','22'), '23','23'), '24','24'), '25','25'), '26','26'), '27','27'), 
                    '28','28'), '29','29')
    
                    ) AS ocupacion,
    
                fs.otra_ocupacion, fs.celular, fs.email, fs.direccion, fs.barrio,
                concat( replace( replace( replace(fs.estrato,'1','1'),'2','2'), '3','3') ) AS estrato, d.name as departamento, c.name as ciudad,
    
                concat( 
                    replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace(fs.nivel_formacion,'1','1'), 
                    '2','2') ,  '3','3'), '4','4'),  '5','5'), '6','6'), '7', '7'), '8','8'), '9','9'), '10','10'), '11','11') ) AS nivel_formacion,
    
                fs.otra_formacion,
    
                concat( 
                    replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace( replace(fs.situacion_particular,'1','1'), 
                    '2','2') ,  '3','3'), '4','4'),'5','5'), '6','6'), '7', '7'), '8','8'), '9','9'), '10','10'), '11','11'), '12','12'),'13','13')  ) AS situacion_particular,
    
                concat( replace( replace( replace(fs.respuesta_1,'1','1'),'2','2'), '3','3') ) AS respuesta_1,
                concat( replace( replace(fs.respuesta_2,'1','1'),'2','0') ) AS respuesta_2,
                concat( replace( replace(fs.respuesta_3,'1','1'),'2','0') ) AS respuesta_3,
    
                fs.firma, fs.foto_fachada
    
                FROM v_formato_sensibilizacion as fs, proy_cliente as pc, proy_proyecto as pp, empleado as e, cli_cliente as cc, city as c, state as d
                where fs.id_proy_cliente=pc.id and pc.id_proyecto=pp.id and pp.id_cliente=cc.id and pc.id_gestor=e.id_usuario  and fs.id_ciudad=c.id and d.id=c.state_id and 
                fs.estado  in (1,2,3,4,5) and fs.id =$id
                ORDER BY fs.id DESC"

                )->queryOne();
                $modelRS =Yii::$app->db->createCommand(
                    "SELECT id, nombre, apellido,  concat( replace(replace(replace(tipo_documento,'1','CC'),'2','CE'), '3','P') ) AS tipo_documento, documento, fecha_nacimiento, edad,
                    concat( replace(replace(replace(sexo,'1','1'),'2','2'),'3','3') ) AS sexo
                    FROM v_registro_sensibilizacion
                    where id_formato_sensiblizacion =$id
                    ORDER BY id DESC"
                )->queryAll();
                $modelRE =Yii::$app->db->createCommand(
                    "SELECT id, elemento, concat( replace(tipo,'1','Información') ) AS tipo, descripcion, cantidad
                    FROM v_registro_entrega
                    where id_formato_sensiblizacion =$id
                    ORDER BY id DESC"
                )->queryOne();

                //$cliente = ProyCliente::find()->where('id='.$formato['id_proy_cliente'])->one();
                //$carpeta = $cliente->proyecto->cliente->carpeta;

                $img_fachada = dirname(__DIR__).'/../backend/web/uploads/clientes/'.$carpeta.'/proyectos/'.$formato['foto_fachada'];
                $array = explode('.',$img_fachada); $extension = end($array);

                //Guardo la Imagen en la carpeta
                if (file_exists($img_fachada)) {
                    copy($img_fachada , $url_temp.'FORMATOS_SENSIBILIZACION/'.$fs->cus.'/enccxd_foto_fachada_('.$fs->cus.').'.$extension);
                    system('echo -e "- SI existe y SI guardó la imagen\n   "');
                } else
                    system('echo -e "NO existe la imagen\n   "'.$img_fachada);


                $img_firma = dirname(__DIR__).'/../backend/web/uploads/clientes/'.$carpeta.'/proyectos/'.$formato['firma'];

                $css = '
                    .contenedor{
                        background-image: url("'.dirname(__DIR__)."/../backend/web/img/formatosensibilizacion.png" .'") ;
                        background-size:  100%;
                        background-repeat: no-repeat;
                        height: 100%;
                        // background-position: 0%;
                    }
                ';

                //  Utils::dumpx(gettype($userData));
                if( !empty($formato) && ($formato != "") ){

                    // Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                        'destination' => Pdf::DEST_FILE,
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        'format' => Pdf::FORMAT_LEGAL,
                        'content' => $this->renderPartial('reportPdf', ['data' => $formato, 'modelRS'=> $modelRS, 'modelRE'=> $modelRE,  'img_fachada' => $img_fachada, 'img_firma' => $img_firma ]),
                        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                        'cssInline' => $css,
                        'marginRight' => '0', 'marginLeft' => '0', 'marginTop' => '0',  'marginBottom' => '0',
                        'options' => [
                            // any mpdf options you wish to set
                        ],
                        'filename' => $url_temp.'FORMATOS_SENSIBILIZACION/'.$fs->cus.'/enccxd_acta_entrega_('.$formato['cus'].').pdf',
                        'methods' => [
                            'SetTitle' => 'Formato Sensibilizacion',
                            'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                        ],
                    ]);

                    $pdf->render();

                } else {
                    //para generar certificado se necesita nombres, apellidos y cedula de ciudadania
                    echo 'ERROR.... FALTAN DATOS... PARA GENERAR EL PDF';
                }
                $c++;

              //  if($c>6) break;
            }//end for

            //***********************************************************************************************************************
            //***********************************************************************************************************************
            // FALLIDOS *****************************************************************************************************
            //***********************************************************************************************************************
            //1. FORMATOS_SENSIBILIZACION [APROBADOS] and fecha_visita>="'.$fecha_hoy.'"
            $formatosFallidos = VFormatoFallidaSensibilizacion::find()->where('estado=2'/* and fecha_visita>="'.$fecha_hoy.'"'*/)->all();
            $c=1;
            foreach($formatosFallidos as $fs){
                $id = $fs->id;

                //Creo carpeta CUS
                if(!file_exists(Yii::getAlias($url_temp.'FORMATOS_FALLIDOS/'.$fs->cus.'/'))) {
                    mkdir(Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/'.$fs->cus.'/'), 0777, true);
                    system("chown deploy " . Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/' . $fs->cus . '/'));
                    exec('chmod -R 0777 ' . Yii::getAlias($url_temp . 'FORMATOS_FALLIDOS/' . $fs->cus . '/'));
                }

                $formato =Yii::$app->db->createCommand("SELECT ffs.id, cc.nombre as cliente,pp.nombre as proyecto,  concat(e.nombres,' ',e.apellidos) as gestor, 
                            concat(pc.nombres,' ',pc.apellidos, ' - [DIR] : ',pc.direccion,' - [Cel] : ',pc.celular) as usuario_predio_original, ffs.id_proy_cliente,
                
                            pc.nombres, pc.apellidos, ffs.cus,  DATE_FORMAT(ffs.fecha_visita,'%d - %m - %Y') AS fecha_visita, ffs.nombre_cliente, 
                            concat( replace(replace(replace(ffs.tipo_documento,'1','CC'),'2','CE'),'3','P') ) AS tipo_documento, 
                            ffs.documento, ffs.direccion, ffs.barrio,
                            concat( replace( replace( replace(ffs.estrato,'1','1'),'2','2'), '3','3') ) AS estrato, d.name as departamento, c.name as ciudad, ffs.motivo_visita,
                            ffs.piv_nombre_completo, ffs.piv_documento, ffs.piv_direccion, ffs.piv_responsable, ffs.cp_cedula, ffs.cp_recibo_publico, ffs.cp_certificado_vip, ffs.cp_solicitud_servicio,
                            ffs.razones, ffs.pr_nombres, ffs.pr_cedula, ffs.pr_telefono, ffs.pr_firma, ffs.tc_nombres,  ffs.tc_cedula,  ffs.tc_telefono, ffs.tc_firma,  ffs.created_at as  creado, ffs.foto_fachada
                
                            FROM v_formato_fallida_sensibilizacion as ffs, proy_cliente as pc, proy_proyecto as pp, empleado as e, cli_cliente as cc, city as c, state as d
                            where ffs.id_ciudad=c.id and d.id=c.state_id and ffs.id_proy_cliente = pc.id and pc.id_proyecto=pp.id and pp.id_cliente=cc.id and pc.id_gestor=e.id_usuario 
                            and ffs.estado  in (1, 2, 3, 4, 5) and ffs.id=$id
                            ORDER BY ffs.id DESC ")->queryOne();

                $img_fachada = dirname(__DIR__).'/../backend/web/uploads/clientes/'.$carpeta.'/proyectos/'.$formato['foto_fachada'];
                $array = explode('.',$img_fachada); $extension = end($array);

                //Guardo la Imagen en la carpeta
                if (file_exists($img_fachada)) {
                    copy($img_fachada , $url_temp.'FORMATOS_FALLIDOS/'.$fs->cus.'/enccxd_foto_visita_fallida_('.$fs->cus.').'.$extension);
                    system('echo -e "- SI existe y SI guardó la imagen\n   "');
                } else
                    system('echo -e "NO existe la imagen\n   "'.$img_fachada);

                $img_pr_firma = dirname(__DIR__).'/../backend/web/uploads/clientes/'.$carpeta.'/proyectos/'.$formato['pr_firma'];
                $img_tc_firma = dirname(__DIR__).'/../backend/web/uploads/clientes/'.$carpeta.'/proyectos/'.$formato['tc_firma'];

                $css = '
                .contenedor{
                    background-image: url("'.dirname(__DIR__)."/../backend/web/img/formatofallido.png" .'") ;
                    background-size:  100%;
                    background-repeat: no-repeat;
                    height: 100%;
                    // background-position: 0%;
                }
            ';


                if( !empty($formato) && ($formato != "") ){
                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                        'destination' => Pdf::DEST_FILE,
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        'format' => Pdf::FORMAT_A4,
                        'content' => $this->renderPartial('reportPdf2', ['data' => $formato, 'img_fachada' => $img_fachada, 'img_pr_firma' => $img_pr_firma, 'img_tc_firma' => $img_tc_firma ]),
                        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                        'cssInline' => $css,
                        'marginRight' => '0', 'marginLeft' => '0', 'marginTop' => '0',  'marginBottom' => '0',
                        'options' => [
                            // any mpdf options you wish to set
                        ],
                        'filename' => $url_temp.'FORMATOS_FALLIDOS/'.$fs->cus.'/enccxd_visita_fallida_('.$formato['cus'].').pdf',
                        'methods' => [
                            'SetTitle' => 'Formato Fallido Sensibilizacion',
                            'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                        ],
                    ]);


                    $pdf->render();
                } else {
                    //para generar certificado se necesita nombres, apellidos y cedula de ciudadania
                    echo 'ERROR.... FALTAN DATOS... PARA GENERAR EL PDF';
                }
                $c++;

               // if($c>6) break;
            }//end for

            //***********************************************************************************************************************
            //***********************************************************************************************************************



        }

        //Comprime el ZIP de cada cliente del día
        $this->actionComprimir_backup_dia();

        //Eliminamos la Carpeta de Cada Cliente del día
        $this->rmDir_rf();

    }

    public function actionImportarevidencias()
    {
        $vec='';

//          $vec[0]="8276,2,2019-04-24 22:25:45";


        $url_temp =  dirname(__DIR__).'/../backend/web/uploads/autos/'; //Yii::getAlias('@webroot/uploads/autos/'  );

        for($i=0;$i < count($vec);$i++){
            $proc = explode(',',$vec[$i]);
            $id_proceso = $proc[0];
            //echo $id_proceso.'<br>';
            system('echo -e " ID_PROCESO : '.$id_proceso.'**********************************************************************\n   "');
            $id_tipo_solicitud = $proc[1];
            $fecha_creacion_proc = $proc[2];
            $proc = ProcProceso::find()->where('id='.$id_proceso)->one();

            if(isset($proc)) {

                //Obtengo la Solicitud del proceso
                $solicitud = Requests::find()->where('judicial_process_id=' . $id_proceso)->one();
                //Si NO existe la Solicitud: 1) Debo crear la Solicitud, con Estado en Proceso
                if (!isset($solicitud)) {

                    $solicitud = new Requests();
                    $solicitud->judicial_process_id = $id_proceso;
                    $solicitud->request_type_id = $id_tipo_solicitud;
                    $solicitud->created_at = $fecha_creacion_proc;
                    $solicitud->updated_at = $fecha_creacion_proc;
                    $solicitud->notes = '---';
                    $solicitud->billable = 0; // 0: No Facturable
                    $solicitud->state = 1; //(1: creado), después si tiene al menos una actuacion, se edita a (2: En Proceso)
                    $solicitud->created_by_id = $proc->id_creador_inicial;
                }


                //
                /*  $solicitud = new Requests();
                  $solicitud->judicial_process_id = $id_proceso;
                  $solicitud->request_type_id = $id_tipo_solicitud;
                  $solicitud->created_at = $fecha_creacion_proc;
                  $solicitud->updated_at = $fecha_creacion_proc;
                  $solicitud->notes = '---';
                  $solicitud->billable = 0; // 0: No Facturable
                  $solicitud->state = 1; //(1: creado), después si tiene al menos una actuacion, se edita a (2: En Proceso)
                  $solicitud->created_by_id = $proc->id_creador_inicial;

                  */

                if ($solicitud->save()) {
                    //Obtengo las Actuaciones del proceso ycreo las Evidencias
                    $actuaciones = ProcActuacion::find()->where('id_proceso=' . $id_proceso)->all();
                    //Utils::dumpx($actuaciones);
                    foreach ($actuaciones as $actuacion) {
                        $evidencia = new RequestEvidences();
                        $evidencia->request_id = $solicitud->id;
                        $evidencia->detail = $actuacion->detalle . ' - ' . $actuacion->observacion;
                        if (isset($actuacion->archivo_pdf) && $actuacion->archivo_pdf != '') {
                            $evidencia->file = isset($actuacion->archivo_pdf) ? $actuacion->archivo_pdf : null;
                            $evidencia->file_folder = $actuacion->idProceso->idCliente->carpeta_autos . '/' . $actuacion->idProceso->carpeta_autos;
                        }

                        $evidencia->found = 0;
                        $evidencia->approved = 0; // 0: PENDIENTE x APROBAR
                        $evidencia->created_at = $actuacion->fecha;
                        $evidencia->updated_at = $actuacion->fecha;
                        //Utils::dumpx($evidencia->attributes);

                        if ($evidencia->save()) {
                            //********************************************************************
                            $solicitud->state = 2;  //2: EN PROCESO - Tiene al menos una evidencia...
                            $solicitud->save();
                            //*****************************
                            // Descargo el archivo de la actuación y vuelvo y guardo pero en carpeta evidencias
                            //*****************************
                            if (isset($actuacion->archivo_pdf) && $actuacion->archivo_pdf != '') {
                                $s3 = Yii::$app->get('s3');
                                $url = 'https://s3.us-east-2.amazonaws.com/vigpro-production/' . $actuacion->idProceso->idCliente->carpeta_autos . '/' . $actuacion->idProceso->carpeta_autos . '/' . $actuacion->archivo_pdf;
                                if ($this->url_exists($url)) {
                                    $ch = curl_init($url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                                    $output = curl_exec($ch);
                                    $fh = fopen($url_temp . $actuacion->archivo_pdf, 'w');
                                    fwrite($fh, $output);
                                    fclose($fh);

                                    //Guardo el archivo en la carpeta EVIDENCIAS en S3
                                    $result = $s3->upload($evidencia->file_folder . '/request_evidences/' . $actuacion->archivo_pdf, $url_temp . $actuacion->archivo_pdf);
                                    system('echo -e "- SI existe y SI guardó en Evidencias\n   "');
                                } else
                                    system('echo -e "NO existe la actuación en S3\n   "');

                            } else system('echo -e "-- Se guardó la evidencia, pero sin archivo Pdf --\n   "');

                        } else {
                            //Utils::dumpx($evidencia->getErrors());
                            system('echo -e " ERROR: Datos evidencia NO grabaron, faltan datos Evidencia: ' . $evidencia->id . '**********************************************************************\n   "');
                        }
                    }
                }else{
                    // Utils::dumpx($solicitud->getErrors());
                    system('echo -e " ERROR: Datos Solicitud NO ID_SOL : ('.$solicitud->id.') NO grabaron, faltan datos **********************************************************************\n   "');
                }
            }else
                system('echo -e " PROCESO ('.$id_proceso.') NO EXISTE **********************************************************************\n   "');

        }

        system('echo -e " Listo terminado **********************************************************************\n   "');

    }


    function quitarComillas($cadena)
    {
        $buscar = ["\'", "\""];
        $reemplazar = ["", ""];
        foreach ($buscar as $index => $value)
        {
            $cadena = preg_replace("/".$buscar[$index]."/", $reemplazar[$index], $cadena);
        }
        return $cadena;
    }


}

