<?php
use common\components\Utils;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;


// Utils::dumpx($user);
// Utils::dumpx($curso);
// Utils::dumpx($experto);
/*ob_start(); */
?>
<div class="contenedor" style=" ">

    <div style=" padding-top:10%;">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="padding-left:37%; text-align:center">  <?= $data['cus']  ?></td>
                <td style="padding-right:10%; text-align:right"> <?= $data['fecha_visita']  ?></td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-left:2%; padding-top:2%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left:20%;  width:60%"><?= $data['nombre_cliente']  ?></td>
                <td style="text-align:left; padding-left: 10% "><?= $data['tipo_documento']  ?></td>
                <td style="text-align:left; padding-left: 12%; "><?= $data['documento']  ?></td>
            </tr>
        </table>
    </div>
    <div style="padding-left:18.5%;  padding-top:0.5%; ">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:left;"><?= $data['fecha_nacimiento']  ?></td>
                <td style="padding-top:0; padding-right:15%; "><?= $data['edad']  ?></td>

                <td style="text-align:center; padding-right: 7.5%; ">
                    <?php if($data['genero'] == 1){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center; padding-right: 2.5%; ">
                    <?php if($data['genero'] == 2){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center; padding-right: 12.5%;">
                    <?php if($data['genero'] == 3){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-left:7%;  padding-top:-0.3%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left: 26.5%; ">
                    <?php if($data['grupo_etnico'] ==1){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center; padding-left: 8.5%; ">
                    <?php if($data['grupo_etnico'] == 2){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:left; padding-left: 7%;">
                    <?php if($data['grupo_etnico'] == 3){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:left; padding-left: 8.8%; ">
                    <?php if($data['grupo_etnico'] == 4){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:left; padding-left: 12%; ">
                    <?php if($data['grupo_etnico'] == 5){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:left; padding-left: 8%;">
                    <?php if($data['grupo_etnico'] == 6){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-top:1.7%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left: 21%; ">
                    <?php if($data['ocupacion'] == 1 ){ ?>  <!-- 1 -->
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%">  <!-- 2 -->
                    <?php if($data['ocupacion'] == 2 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%">  <!-- 3 -->
                    <?php if($data['ocupacion'] == 3 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; ">  <!-- 4 -->
                    <?php if($data['ocupacion'] == 4 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; ">
                    <?php if($data['ocupacion'] == 5 ){ ?> <!-- 5 -->
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%">  <!-- 6 -->
                    <?php if($data['ocupacion'] == 6 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%">  <!-- 7 -->
                    <?php if($data['ocupacion'] == 7 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; ">  <!-- 8 -->
                    <?php if($data['ocupacion'] == 8 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; ">  <!-- 9 -->
                    <?php if($data['ocupacion'] == 9 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%">  <!-- 10 -->
                    <?php if($data['ocupacion'] == 10 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%">  <!-- 11 -->
                    <?php if($data['ocupacion'] == 11 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; ">  <!-- 12 -->
                    <?php if($data['ocupacion'] == 12 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; padding-top:-0.5% ">  <!-- 13 -->
                    <?php if($data['ocupacion'] == 13 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%;  padding-top:-0.5%">  <!-- 14 -->
                    <?php if($data['ocupacion'] == 14 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%; padding-top:-0.5%">  <!-- 15 -->
                    <?php if($data['ocupacion'] == 15 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; padding-top:-0.5% ">  <!-- 16 -->
                    <?php if($data['ocupacion'] == 16 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; padding-top:-0.5%">  <!-- 17 -->
                    <?php if($data['ocupacion'] == 17 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%; padding-top:-0.5%">  <!-- 18 -->
                    <?php if($data['ocupacion'] == 18 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%; padding-top:-0.5%">  <!-- 19 -->
                    <?php if($data['ocupacion'] == 19 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; padding-top: 0.5% ">  <!-- 20 -->
                    <?php if($data['ocupacion'] == 20 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; padding-top:-1%">  <!-- 21 -->
                    <?php if($data['ocupacion'] == 21 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: -24%; padding-top:-1%">  <!-- 22 -->
                    <?php if($data['ocupacion'] == 22 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%; padding-top:-1%">  <!-- 23 -->
                    <?php if($data['ocupacion'] == 23 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; padding-top:0%">  <!-- 24 -->
                    <?php if($data['ocupacion'] == 24 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; padding-top:-1%">  <!-- 25 -->
                    <?php if($data['ocupacion'] == 25 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center;padding-left: -24%; padding-top:-1%">  <!-- 26 -->
                    <?php if($data['ocupacion'] == 26 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 11%; padding-top:-1%">  <!-- 27 -->
                    <?php if($data['ocupacion'] == 27 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 14.5%; ">  <!-- 28 -->
                    <?php if($data['ocupacion'] == 28 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left:60%; padding-top:-1%;">  <!-- 28 -->
                    <?php if($data['ocupacion'] == 28){ ?>
                        <?= $data['otra_ocupacion'] ?>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>

        </table>
    </div>

    <!--  -->
    <div style="padding-left:25.5%;  padding-top: 0%; ">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="padding-left:-7%; width:20%"><?= $data['celular']  ?></td>
                <td style="text-align:left; padding-left:12%; "><?= $data['email']  ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left:-15%; padding-top: -0.4%; "><?= $data['direccion']  ?></td>
                <td style="text-align:left; padding-left:20%; padding-top:-0.6%; "><?= $data['barrio']  ?></td>

                <?php if($data['estrato'] == 1){ ?>
                    <td style="text-align:center; padding-top: -0.5% ; padding-right:25%"><strong>X</strong></td>
                <?php } ?>
                <?php if($data['estrato'] == 2){ ?>
                    <td style="text-align:center;   padding-right: 18%; "><strong>X</strong></td>
                <?php } ?>
                <?php if($data['estrato'] == 3){ ?>
                    <td style="text-align:right;  padding-right: 15%; "><strong>X</strong></td>
                <?php } ?>

            </tr>
            <tr>
                <td style="padding-left:-3%; padding-top:-0.8%; "><?= $data['departamento']  ?></td>
                <td style="text-align:center; padding-left: 26%; padding-top:-0.8%; "><?= $data['ciudad']  ?></td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-top: -0.2%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:center; padding-left: 32.5%; "> <!-- 1 -->
                    <?php if($data['nivel_formacion'] == 1 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 18%"> <!-- 5 -->
                    <?php if($data['nivel_formacion'] == 5 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 19%"> <!-- 7 -->
                    <?php if($data['nivel_formacion'] == 7 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 16%; "> <!-- 3 -->
                    <?php if($data['nivel_formacion'] == 3 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 32.5%; padding-top:-0.5%; "> <!-- 2 -->
                    <?php if($data['nivel_formacion'] == 2 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 18%; padding-top:-0.5%;"> <!-- 6 -->
                    <?php if($data['nivel_formacion'] == 6 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 19%; padding-top:-0.5%;"> <!-- 8 -->
                    <?php if($data['nivel_formacion'] == 8 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:left; padding-left: 16%; padding-top:-0.5%; "> <!-- 4 -->
                    <?php if($data['nivel_formacion'] == 4 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 32.5%; padding-top:-0.5%; "> <!-- 10 -->
                    <?php if($data['nivel_formacion'] == 10 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 18%; padding-top:-0.5%;"> <!-- 9 -->
                    <?php if($data['nivel_formacion'] == 9 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:right; padding-left:16.5%; padding-top:-1%;"> <!-- 11 -->
                    <?php if($data['nivel_formacion'] == 11){ ?>
                        <?= $data['otra_formacion'] ?>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-top: 2.5%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:center; padding-left: 28%; "> <!-- 1 -->
                    <?php if($data['situacion_particular'] == 1 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%"> <!-- 8 -->
                    <?php if($data['situacion_particular'] == 8 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 28%;  "> <!-- 2 -->
                    <?php if($data['situacion_particular'] == 2 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%; "> <!-- 9 -->
                    <?php if($data['situacion_particular'] == 9 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 28%; padding-top:-0.5%; "> <!-- 3 -->
                    <?php if($data['situacion_particular'] == 3 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%;"> <!-- 10 -->
                    <?php if($data['situacion_particular'] == 10 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 28%; padding-top: 0.5%; "> <!-- 4 -->
                    <?php if($data['situacion_particular'] == 4 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%; padding-top: -0.5%;"> <!-- 11 -->
                    <?php if($data['situacion_particular'] == 11 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 28%; padding-top: -0.5%; "> <!-- 5 -->
                    <?php if($data['situacion_particular'] == 5 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%; padding-top:-0.5%;"> <!-- 12 -->
                    <?php if($data['situacion_particular'] == 12 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 37%; padding-top:-0.5%; "> <!-- 6 -->
                    <?php if($data['situacion_particular'] == 6 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
                <td style="text-align:center; padding-left: 34%; padding-top:-0.5%;"> <!-- 13 -->
                    <?php if($data['situacion_particular'] == 13 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 53%; padding-top: -0.5%; "> <!-- 7 -->
                    <?php if($data['situacion_particular'] == 7 ){ ?>
                        <strong>X</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-top: 2.7%; height:14.2%; ">
        <table class="" style="">
            <?php if(isset($modelRS))
                foreach ($modelRS as $value ){ ?>

                    <tr>
                        <td style="text-align:left; padding-left: 22%;  ">
                            <?= $value['nombre'] .' '. $value['apellido']; ?>
                        </td>
                        <td style="text-align:left; padding-left: 17% ">
                            <?= $value['tipo_documento']; ?>
                        </td>
                        <td style="text-align:left; padding-left: 12%; ">
                            <?= $value['documento']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left; padding-left: 22%; padding-top: 0%; ">
                            <?= $value['fecha_nacimiento']; ?>
                        </td>
                        <td style="text-align:left; padding-left: -7%; padding-top: 0%; ">
                            <?= $value['edad']; ?>
                        </td>
                        <td style="text-align:center; padding-left: -16%; padding-top: -0.5%;">
                            <?php if($value['sexo'] == 1 ){ ?>
                                <strong>X</strong>
                            <?php }else echo "&nbsp;"; ?>
                        </td>
                        <td style="text-align:center; padding-left: -17%">
                            <?php if($value['sexo'] == 2 ){ ?>
                                <strong>X</strong>
                            <?php }else echo "&nbsp;"; ?>
                        </td>
                        <td style="text-align:left; padding-left: -1.5%">
                            <?php if($value['sexo'] == 3 ){ ?>
                                <strong>X</strong>
                            <?php }else echo "&nbsp;"; ?>
                        </td>

                    </tr>
                <?php } ?>
        </table>
    </div>
    <!--  -->
    <div style="height: 8.2%; padding-top: -1%">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:left; padding-left:8%">
                    <img style="width:10%; height:5%" src="<?= $img_firma ?>">
                </td>
                <td style="text-align:center; padding-left: 25%;"><?= $data['nombre_cliente']  ?></td>
                <td style="text-align:right;  padding-right: 12%;"><?= $data['documento']  ?></td>
            </tr>
        </table>
    </div>
    <!--  -->
    <!--  -->
    <div style="padding-top: 0%; height:5%;">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left: 10%; font-size:10 ">
                    <?= $modelRE['elemento']; ?>
                </td>
                <td style="text-align:left; padding-left: 2%; ">
                    <?= $modelRE['tipo']; ?>
                </td>
                <td style="text-align:left; padding-left: 14%;">
                    <a href="<?= $modelRE['descripcion']; ?>" target="_blank">Multimedia Sensibilización</a>
                </td>
                <td style="text-align:left; padding-left: 17%; ">
                    <?= $modelRE['cantidad']; ?>
                </td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-top: 0%; height:7%; ">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:left; padding-left:8%">
                    <img style="width:10%; height:5%" src="<?= $img_firma ?>">
                </td>
                <td style="text-align:center; padding-left: 25%;"><?= $data['nombre_cliente']  ?></td>
                <td style="text-align:right;  padding-right: 12%;"><?= $data['documento']  ?></td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-top: 0%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left:70%;  ">
                    <?php if($data['respuesta_1'] == 1 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:right; padding-top: -1%   ">
                    <?php if($data['respuesta_1'] == 2 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:right; padding-top: -1.5% ">
                    <?php if($data['respuesta_1'] == 3 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
        </table>
        <table class="" style="padding-top: -1.5%">
            <tr>
                <td style="text-align:left; padding-left:74%;  ">
                    <?php if($data['respuesta_2'] == 1 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:right; padding-top: -1.2%   ">
                    <?php if($data['respuesta_2'] == 2 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
        </table>
        <table class="" style="padding-top: -1.5%">
            <tr>
                <td style="text-align:left; padding-left:78%;  ">
                    <?php if($data['respuesta_3'] == 1 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:right; padding-top: -1.2%   ">
                    <?php if($data['respuesta_3'] == 2 ){ ?>
                        <strong>x</strong>
                    <?php }else echo "&nbsp;"; ?>
                </td>
            </tr>
        </table>
    </div>

    <!--  -->

</div>

