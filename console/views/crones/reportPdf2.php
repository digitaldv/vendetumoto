<?php
use common\components\Utils;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;


// Utils::dumpx($user);
// Utils::dumpx($curso);
// Utils::dumpx($experto);
/*ob_start(); */
?>
<div class="contenedor" style=" ">

    <div style=" padding-top:22.5%;">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="padding-left:37%; text-align:center">  <?= $data['cus']  ?></td>
                <td style="padding-right:13%; text-align:right"> <?= $data['fecha_visita']  ?></td>
            </tr>
        </table>
    </div>
    <!--  -->
    <?php
    $str_blancos='';
    $total = 36;  // $esp_blan = 26 - 16;
    $tot_name = strlen($data['nombre_cliente']);
    $esp_blanco = $total - $tot_name;
    for($i=0;$i <= $esp_blanco;$i++){
        $str_blancos .= "&nbsp;";
    }
    ?>
    <div style="padding-left:2%; padding-top:1.5%; ">
        <table class="" style="">
            <tr>
                <td style="text-align:left; padding-left: 24%; "><?=  $data['nombre_cliente'].$str_blancos ?></td>
                <td style="text-align:left; padding-left: 10% "><?= $data['tipo_documento']  ?></td>
                <td style="text-align:left; padding-left: 12%; "><?= $data['documento']  ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 20%;padding-top:-0.5% "><?= $data['direccion']  ?></td>
                <td style="text-align:left; padding-left: 4%; padding-top:-0.5%"><?= $data['barrio']  ?></td>

                <?php if($data['estrato'] == 1){ ?>
                    <td style="text-align:right;  padding-right: 11%;"><strong>X</strong></td>
                <?php } ?>
                <?php if($data['estrato'] == 2){ ?>
                    <td style="text-align:right;  padding-right: 7%; "><strong>X</strong></td>
                <?php } ?>
                <?php if($data['estrato'] == 3){ ?>
                    <td style="text-align:right;  padding-right: 2%; "><strong>X</strong></td>
                <?php } ?>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 21%; padding-top:-0.5%; font-size:10px"><?= $data['departamento']  ?></td>
                <td style="text-align:left; padding-left: -10%; padding-top:-0.5%;  font-size:10px"><?= $data['ciudad']  ?></td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-left:24%;  padding-top:-1%; ">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:center; padding-left: 17%; padding-top: 5%;">
                    <?php if($data['motivo_visita'] == 1){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center; padding-right: 22%; padding-top: 5.3%">
                    <?php if($data['motivo_visita'] == 2){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;  padding-left: 17%; padding-top: 2.5%">
                    <?php if($data['motivo_visita'] == 3){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center;  padding-right: 22%; padding-top: 2.5%">
                    <?php if($data['motivo_visita'] == 4){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; padding-left: 17%; padding-top: 2.5%">
                    <?php if($data['motivo_visita'] == 5){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
                <td style="text-align:center;  padding-right: 22%;  padding-top: 2.5%">
                    <?php if($data['motivo_visita'] == 6){ ?>
                        <strong>X</strong>
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-left:23%;  padding-top:6.6%; ">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="padding-left:-7%; width:50%;"><?= $data['piv_nombre_completo']  ?></td>
                <td style="text-align:right; padding-right:23%; "><?= $data['piv_documento']  ?></td>
            </tr>
            <tr>
                <td style="padding-left:4%; padding-top: -0.4%; "><?= $data['piv_direccion']  ?></td>
            </tr>
            <tr>
                <td style="padding-left:-9%; padding-top:-0.8%; "><?= $data['piv_responsable']  ?></td>
            </tr>

        </table>
    </div>
    <!--  -->
    <div style="padding-left:24%;  padding-top:15%;">
        <table class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:left; padding-left: 18%;  padding-top: 3%; ">
                    <?php if($data['cp_cedula'] == 0){ ?>
                        <strong>X</strong>
                    <?php } else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 18%;  padding-top: -0.5%; ">
                    <?php if($data['cp_recibo_publico'] == 1){ ?>
                        <strong>X</strong>
                    <?php } else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 18%;  padding-top: -1%; ">
                    <?php if($data['cp_certificado_vip'] == 1){ ?>
                        <strong>X</strong>
                    <?php } else echo "&nbsp;"; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: 18%;  padding-top: -1.5%; ">
                    <?php if($data['cp_solicitud_servicio'] == 1){ ?>
                        <strong>X</strong>
                    <?php } else echo "&nbsp;"; ?>
                </td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-top:3%; height:9%;">
        <table  class="" style="width: 100%;max-width: 100%;">
            <tr>
                <td style="text-align:center; padding-left:15%; padding-right:15%">
                    <?= $data['razones'] ?>
                </td>
            </tr>
        </table>
    </div>
    <!--  -->
    <div style="padding-left:20%;  padding-top: 0%; ">
        <table class="" style="width: 100%;max-width: 100%;">

            <tr>
                <td style="text-align:left; padding-left: 1%;  padding-right: 15%; padding-top: 2%">
                    <img style="width:10%; height:5%" src="<?= $img_pr_firma ?>">
                </td>
                <td style="text-align:left; padding-left: 4%; padding-top: 1.5% ">
                    <img style="width:10%; height:5%" src="<?= $img_tc_firma ?>" >
                </td>
            </tr>
            <tr>
                <td style="text-align:left;  padding-left: -0.4%; padding-top: 1.2% ">
                    <?= $data['pr_nombres'] ?>
                </td>
                <td style="text-align:left;  padding-left: 9%; padding-top: 1.2%">
                    <?= $data['tc_nombres'] ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: -0.4%; padding-top: 1.2%">
                    <?= $data['pr_cedula'] ?>
                </td>
                <td style="text-align:left;  padding-left: 9%; padding-top: 1.2%">
                    <?= $data['tc_cedula'] ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left: -0.4%; padding-top: 1.3%">
                    <?= $data['pr_telefono'] ?>
                </td>
                <td style="text-align:left; padding-left: 9%; padding-top: 1.3% ">
                    <?= $data['tc_telefono'] ?>
                </td>
            </tr>
        </table>
    </div>

</div>

