<?php

use \yii\web\Request;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language'=>'es',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
        ],
        's3' => [
            'class' => 'frostealth\yii2\aws\s3\Service',
            'credentials' => [
                'key' => 'AKIAT7ZWMLPORUQHILMN',
                'secret' => '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',
            ],
            'region' => 'us-east-2',
            'defaultBucket' => 'vtmprod',
            'defaultAcl' => 'public-read',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class
            ]
        ],/*
        'user' => [
            'identityClass' => 'webvimark\modules\UserManagement\models\User',
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',

            // Comment this if you don't want to record user logins
            'on afterLogin' => function($event) {
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
            }
        ],*/
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '/yiiadv/frontend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],


        'urlManager2222' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
//               '<action>'=>'site/<action>',
//               '<controller>s' => '<controller>/index',
//               '<controller>/<id:\d+>s' => '<controller>/view',
                /********************************************************************************************/

//                /*'publicar-anuncio' => 'site/publicaranuncio',



                'blog' => 'site/blog',
                'blog/<url>' => 'site/getarticulo',
                'quienes-somos' => '/site/quienessomos',
                'preguntas-frecuentes' => '/site/preguntasfrecuentes',
                'terminos-y-condiciones' => '/site/terminosycondiciones',
                'politica-de-proteccion-de-datos' => '/site/politicadeprotecciondedatos',
                'politica-de-privacidad' => '/site/politicadeprivacidad',


/*              'producto/<url>' => 'site/getproducto',
                'acceder' => 'site/login',
                'como-funciona' => 'site/comofunciona',
                'quienes-somos' => '/site/quienessomos',
                'especiales' => 'site/especiales',
                'proximos-lanzamientos' => 'site/lanzamientos',
                'politica-cookies' => 'site/cookies',
                'aviso-legal' => 'site/avisolegal',
                'contacto' => 'site/contacto',
                'politica-privacidad' => 'site/privacidad',
                'registrarse' => 'site/registrousuario',
                'bienvenido' => 'site/bienvenido',
                // 'resultados' => 'site/resultadosbusqueda',
                'cursos' => 'site/allcourses',
                // 'cursos' => 'site/cursos',
                'detalle-curso' => 'site/detallecurso',
                'empresas' => 'site/empresas',
                'descargas' => 'site/descargas',
                'resultados' => 'site/resultados',
                'carrito' => 'site/carrito',
                'mis-compras' => 'site/miscursos',
                'contenido-curso' => 'site/cursocontenidos',
                //'curso-detalle' => 'site/cursocomprado',
                'pago-realizado' => 'site/pagorealizado',
                'empleados-empresa' => 'site/empresaempleados',


                /****** Enlaces usuario Logueado ***********
                'mi-perfil' => 'site/perfilcliente',
                'perfil-proveedor/<url>' => 'site/perfilproveedor',
                'mis-deseos' => 'site/misdeseos',
                'mis-alertas' => 'site/misalertas',
                'mis-colecciones' => 'site/miscolecciones',
                'perfil-experto' => 'site/perfilexperto',
                'notificaciones' => 'site/notificaciones',
                'expertos' => 'site/expertos',
                'experto' => 'site/experto',
                'cursos-empresas' => 'site/cursosempresas',
*/

                /********************************************************************************************/
            ],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => \yii\rest\UrlRule::class, 'controller' => ['post', 'comment']],
                '' => 'site/index',
                'como-funciona' => 'site/comofunciona',
                //'publicar-anuncio' => 'site/publicaranuncio',
                //'publicar-anuncio-paso-2/<id:\d+>/<tipo:\d+>' => 'site/publicaranuncio2',
                //'publicar-anuncio-paso-3/<id>' => 'site/publicaranuncio3',
                //'blog/<url>' => 'site/getarticulo',
                //'anuncio/<url>' => 'site/getpublicacion',
            ],
        ],
        // 'mailer' => [
        //     'class' => 'yii\swiftmailer\Mailer',
        //     'viewPath' => '@common/mail',
        //     'useFileTransport' => false,
        //     'transport' => [
        //         'class' => 'Swift_SmtpTransport',
        //         'host' => 'smtp.gmail.com',
        //         'username' => 'no-responder@vigpro.com',
        //         'password' => 'Asdqwe123#',
        //         'port' => '587',
        //         'encryption' => 'tls',
        //     ],

        // ],
        'mailer' => [
           'class' => 'yii\swiftmailer\Mailer',
           'viewPath' => '@common/mail',
           'useFileTransport' => false,
           'transport' => [
               'class' => 'Swift_SmtpTransport',
               'host' => 'smtp.sendgrid.net',
               'username' => 'apikey',
               'password' => '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',
               'port' => '587',
               'encryption' => 'tls',
               'streamOptions' => [ 'ssl' =>
                   [ 'allow_self_signed' => true,
                       'verify_peer' => false,
                       'verify_peer_name' => false,
                   ],
               ]
           ]
       ]

    ],
    'params' => $params,
];
