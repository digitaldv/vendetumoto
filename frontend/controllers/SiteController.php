<?php
namespace frontend\controllers;

use app\models\VMisFavoritos;
use common\models\AdmPaginasInternas;
use common\models\AdsPublicidad;
use common\models\ArtArticulo;
use common\models\City;
use common\models\MotCaracteristicas;
use common\models\MotModelo;
use common\models\MotTipo;
use common\models\PaginasInternas;
use common\models\Paquete;
use common\models\Payments;
use common\models\PubCaracteristica;
use common\models\PubDenuncia;
use common\models\PubEstadisticas;
use common\models\PubFavoritos;
use common\models\PubMensaje;
use common\models\PubMultimedia;
use common\models\Suscripciones;
use common\models\VBuscadorAnuncios;
use common\models\VBuscadorPublicaciones;
use SebastianBergmann\CodeCoverage\Util;
use webvimark\modules\UserManagement\models\User;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Json;
use common\components\Utils;
use common\models\CliUsuario;
use common\models\PubPublicacion;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\ArrayHelper;
use common\models\MotMarca;
use common\models\PaymentsWebhooks;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use ZipArchive;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public $successUrl = '';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        date_default_timezone_set('America/Bogota');
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'registrousuario'],
                'rules' => [[
                    'actions' => ['login', 'error', 'registrousuario', 'publicaranuncio', 'publicaranuncio2', 'publicaranuncio3', 'publicaranuncio4', 'pagoaprobado',
                        'pubdetalle', 'descargarimagenes', 'eliminarpubcli', 'epaycotransaction', 'epaycoconfirmation', 'auth', 'ajaxadddenuncia','diaspasados'], 'allow' => true,
                ],
                    ['actions' => ['logout', 'index', 'index1', 'getmodelosmarca', 'getfotosanuncio', 'getmensajespub', 'ajaxsavemensajepub', 'descargarimagenes'],
                        'allow' => true, 'roles' => ['@'],
                    ],
                    ['actions' => ['registrousuario'],
                        'allow' => true, 'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
                'successUrl' => $this->successUrl
            ],
        ];
    }



    public function actionIndex1()
    {
        $this->layout = 'main-blank2';
        return $this->render('index1');

    }

    /* Sólo crea nuevas fotos a local y sube a S3 */

    public function actionUpdateimagespub()
    {

        //Utils::dump($_GET);
        //Utils::dump($_POST);
       // Utils::dump($_FILES);
        $model = PubPublicacion::find()->where('id=' . $_GET['id_pub'] . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
      //  Utils::dump($model->attributes);
    /*
     *
     *
        //1: ACTIVO , 5: RECHAZADO o 7: PAUSADO

        if($model->estado==1 || $model->estado==5 || $model->estado==7){
            $model->estado = 2; //  2 - PENDIENTE_X_APROBAR
            //Enviar email a superadmin:


            //****************************
        }
    */


        //Actualizo el orden de las imagenes al nuevo definidio por el usuario
        if(isset($_GET['id_pub']) && $_GET['id_pub']!='' && isset($_GET['op']) && $_GET['op']==2){
            if(isset($_POST['ids']) && $_POST['ids']!=''){
                $vec_imgs = explode(',',$_POST['ids']);
                for($k=0;$k<count($vec_imgs);$k++){
                    $foto = PubMultimedia::find()->where('id='.$vec_imgs[$k])->one();
                    $foto->img_order = $k+1;
                    $foto->save();
                }
                return 1;
            }
        }else{
            if(isset($_GET['id_pub']) && $_GET['id_pub']!='' && isset($_GET['op']) && $_GET['op']==1)
            { //echo "-11111";
                if (!empty($_FILES))
                {
                //    echo "-2222";
                    // 1) Borro las de la BD actuales
                    //PubMultimedia::deleteAll('id_publicacion='.$_GET['id_pub']);

                    //2 subo a S3 las imagenes
                    foreach ($_FILES as $file){
                        // Utils::dumpx(count($file['name']));
                        //recorro cada imagen
                        for ($i = 0; $i < count($file['name']); $i++)
                        {
                            $total_fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion='.$_GET['id_pub'])->count();
                            if($total_fotos < 10) {
                                move_uploaded_file($file['tmp_name'][$i], Yii::getAlias('@webroot/uploads/tmpfotos/') . $file['name'][$i]);
                                $extension = pathinfo($file['name'][$i], PATHINFO_EXTENSION);
                                if (isset($extension) && $extension != '') {
                                    $file_file = Yii::$app->security->generateRandomString() . "." . $extension;
                                } else {
                                    echo 'ERROR: Archivo NO tiene una extensión correcta';
                                    return false;
                                }

                                //Subo el archivo PDF a S3
                                $s3 = Yii::$app->get('s3');
                                $result = $s3->upload('pubs/' . $model->carpeta . '/' . $file_file, Yii::getAlias('@webroot/uploads/tmpfotos/') . $file['name'][$i]);

                                if (!$result) {
                                    echo 'ERROR: Foto no creada en S3: ' . Yii::getAlias('@webroot/uploads/tmpfotos/') . $file['name'][$i];
                                    return false;
                                } else {
                                    unlink(Yii::getAlias('@webroot/uploads/tmpfotos/') . $file['name'][$i]);
                                    //Creo la foto relacionada a la publicacion
                                    $modMult = new PubMultimedia();
                                    $modMult->fecha = date('Y-m-d H:i:s');
                                    $modMult->id_publicacion = $_GET['id_pub'];
                                    $modMult->tipo = 1;
                                    $modMult->archivo = $file_file;
                                    if($total_fotos > 0){
                                        $ult_orden = PubMultimedia::find()->where('tipo=1 and id_publicacion='.$_GET['id_pub'])->orderBy('img_order desc')->limit(1)->one();
                                        $modMult->img_order = $ult_orden->img_order + 1;
                                    }else
                                        $modMult->img_order = 1;

                                    if (!$modMult->save()) {
                                        Utils::dumpx($modMult->getErrors());
                                    }
                                }

                            }else
                                echo 'Error: ¡Haz sobrepasado el máximo de 10 imágenes!';


                        }//endfor
                    }
                    //echo "-44444";

                    //pregunto por el estado actual es ACTIVO y llega hasta acá, es por que almenos agregó alguna foto nueva y EDITÖ
                    //Por eso el estado debe cambiar a 2 - PEND x APROBAR
                    if ($model->estado == 1)
                    {
                       // echo "-55555";
                        $model->fecha_edicion = date('Y-m-d H:i:s');
                        $model->estado = 2; //  2 - PENDIENTE_X_APROBAR
                        $model->recorrido = $model->recorrido.'';
                        $model->cilindraje = $model->cilindraje.'';
                        if(!$model->save()){

                            Utils::dumpx($model->getErrors());
                        }else{
                            Utils::dumpx($model->attributes);
                        }
                    }
                }
            }/*else{
                echo "-6666"; exit;
            }*/
        }//elsefin



    }

    public function actionSubirimagenes()
    {
       // Utils::dumpx($_GET);

        $this->layout = 'main-blank';

        $pub = PubPublicacion::find()->where('id='.$_GET['id'])->one();

        return $this->render('_subirimagenes', [
            'id'  => $pub->id,
            'tipo'  => $pub->id_tipo
        ]
        );
    }

    public function actionSubirarchivos333()
    {
        extract($_GET);

        // Utils::dump($_GET);Utils::dump($_POST);
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (isset($id) && $id != '') {
            $model = PubPublicacion::find()->where('id=' . $id . ' and id_usuario=' . Yii::$app->user->identity->id)->one();



            if ($model && $model->id_usuario == Yii::$app->user->identity->id)
            {
                $total_fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion='.$id)->count();

                //Guarda FOTO en local ****************************************
                if (!empty($_FILES)) {
                    if($total_fotos<10) {
                        move_uploaded_file($_FILES['file']['tmp_name'], Yii::getAlias('@webroot/uploads/tmpfotos/') . $_FILES['file']['name']);
                        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        if (isset($extension) && $extension != '') {
                            $file = Yii::$app->security->generateRandomString() . "." . $extension;
                        } else {
                            echo 'ERROR: Archivo NO tiene una extensión correcta';
                            return false;
                        }

                        //Subo el archivo PDF a S3
                        $s3 = Yii::$app->get('s3');
                        $result = $s3->upload('pubs/' . $model->carpeta . '/' . $file, Yii::getAlias('@webroot/uploads/tmpfotos/') . $_FILES['file']['name']);

                        if (!$result) {
                            echo 'ERROR: Foto no creada en S3: ' . Yii::getAlias('@webroot/uploads/tmpfotos/') . $_FILES['file']['name'];
                            return false;
                        } else {
                            unlink(Yii::getAlias('@webroot/uploads/tmpfotos/') . $_FILES['file']['name']);
                            //Creo la foto relacionada a la publicacion
                            $modMult = new PubMultimedia();
                            $modMult->fecha = date('Y-m-d H:i:s');
                            $modMult->id_publicacion = $id;
                            $modMult->tipo = 1;
                            $modMult->archivo = $file;
                            if($modMult->save()){
                                //pregunto por el estado actual es ACTIVO y llega hasta acá, es por que almenos agregó alguna foto nueva y EDITÖ
                                //Por eso el estado debe cambiar a 2 - PEND x APROBAR
                                if($model->estado==1){
                                    $model->fecha_edicion = date('Y-m-d H:i:s');
                                    $model->estado = 2; //  2 - PENDIENTE_X_APROBAR
                                    $model->save();
                                }
                            }
                            //Utils::dumpx($modMult->attributes);

                        }
                    }
                }else{
                    $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->all();
                    $output = "<div class='row'>
                                    <script>
                                        $('.btn_galeria').click(function(e){  
                                                e.preventDefault();                                             
                                                var modal = $('#modal-fotos').modal('show');    
                                                var that = $(this);
                                                modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
                                                $('#div_image').attr('src',that.attr('url_image'));                                                                         
                                            });
                                    </script>";
                    foreach ($fotos as $foto) {
                        $output .= '<div class=" col-sm-1 col-md-1 col-lg-1 " id="rm'.$foto->id.'" id_foto="'.$foto->id.'" >
                                            <img   class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="width:100%;cursor:pointer" title="Click para ampliar" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" archivo="'.$foto->id.'"  />
                                            <button type="button" class="btn btn-sm btn-link remove_image text-danger rm'.$foto->id.'" style="color:red" id_pub="'.$model->id.'" id_foto="' .  $foto->id. '">Eliminar</button>
                                        </div>';
                    }
                    $output .= '</div>';
                    echo $output; exit;
                }


                //Elimina foto
                /* <div class="btn-group">

                              <button title="Acción" type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...</button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item btn_galeria12" id_pub="12" href="#"><i style="color: #9c27b0;font-weight: bold;font-size: 16px" class="fa fa-photo" aria-hidden="true"></i> &nbsp; Ver Fotos</a>
                                <a href="index.php?r=configuracion/pubpublicacion/aprobar&amp;id=12&amp;op=2" data-confirm="¿Está seguro de Volver a PENDIENTE x APROBAR el Anuncio?" data-method="post" class="dropdown-item"><i style="color: #0a6aa1;font-weight: bold;font-size: 16px" class="fa fa-reply" aria-hidden="true"></i> &nbsp; Volver a Pendiente x Aprobar</a>
                                <a class="dropdown-item btn_rechazar12" id_pub="12" tmm="CUATRIMOTO / HONDA / XRE 300" href="#"><i style="color: red;font-weight: bold;font-size: 16px" class="fa fa-close" aria-hidden="true"></i> &nbsp;&nbsp; Rechazar</a>
                                <a class="dropdown-item  btn_vendido12" descripcion="QWEQWE qew qweqweqweqweqwe" id_pub="12" tmm="CUATRIMOTO / HONDA / XRE 300" href="#"><i style="color: blue;font-weight: bold;font-size: 16px" class="fa fa-thumbs-o-up" aria-hidden="true"></i> &nbsp; Vendido</a>
                                <a class="dropdown-item" href="#"><i style="color: blue;font-weight: bold;font-size: 16px" class="fa fa-photo" aria-hidden="true"></i> &nbsp; Ver Fotos</a>
                                <a href="index.php?r=configuracion/pubpublicacion/bloquear&amp;id=12" data-confirm="¿Está seguro de BLOQUEAR x DENUNCIAS al Anuncio?" data-method="post" class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px" class="fa fa-ban" aria-hidden="true"></i> &nbsp;&nbsp; Bloquear x Denuncias</a>
                                <a href="index.php?r=configuracion/pubpublicacion/inactivar&amp;id=12" data-confirm="¿Está seguro de INACTIVAR el Anuncio?" data-method="post" class="dropdown-item"><i style="color: red;font-weight: bold;font-size: 16px" class="fa fa-power-off" aria-hidden="true"></i> &nbsp;&nbsp; Inactivar</a>
                              </div>
                            </div>
                }*/


            } else {
                echo 'ERROR: ¡NO tiene permisos para acceder a subir archivos a esta publicación!'; return false;
            }

        } else {
            echo 'ERROR: ¡Faltan parámetros!';  return false;
        }



    }

    //enviarpreguntavendedor'])."', {id_publicacion: ".$model->id.", mensaje: $('#pubmensaje-mensaje').val(), id_consultor:
    public function actionEnviarpreguntavendedor()
    {
        extract($_POST);
       // Utils::dumpx($_POST);

        if (isset($id_publicacion) && $id_publicacion != '') {
            $pub = PubPublicacion::find()->where('id='.$id_publicacion)->one();
            $model = new PubMensaje();
            $model->id_publicacion = $id_publicacion;
            $model->fecha = date('Y-m-d H:i:s');
            //$model->id_papa = 0;
            $model->id_creador = Yii::$app->user->identity->id;
            $model->mensaje = $mensaje;
            $model->id_dueno_publicacion = $pub->id_usuario;

            if ($model->save()) {
                /*Utils::dumpx($model->attributes);
                //Pregunto si ya hay un mensaje de ese Comprador a esa Publicacion
                $exist_msg_anterior = PubMensaje::find()->where('id_publicacion=' . $id_publicacion   . ' and id_creador=' . Yii::$app->user->identity->id )->limit(1)->one();
                if ($exist_msg_anterior) {
                    $model->id_papa = $exist_msg_anterior->id_papa;
                } else {*/
                    $model->id_papa = $model->id;
               // }


                if($model->save()){
                    $foto1 = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id_publicacion)->orderBy('id ASC')->one();

                    if (!$foto1) {
                        //No tiene imagenes
                        $foto1[0] = Yii::$app->request->baseUrl . '/img/no-image-moto.png';
                    }

                    $vendedor = CliUsuario::find()->where('id_usuario=' . $pub->id_usuario)->one();
                    $comprador = CliUsuario::find()->where('id_usuario=' . Yii::$app->user->identity->id)->one();

                    //Enviar email a Vendedor
                    //$nuevafecha = strtotime('+2 hour', time());
                    //$nuevafecha = date('Y-m-d H:ia', $nuevafecha);

                    Yii::$app->mailer->compose('layouts/pregunta_a_vendedor',
                        ['tipo' => 'al_vendedor', 'mensaje' => $model->mensaje, 'username' => $vendedor->nombreFull, 'id_pub' => $id_publicacion, 'fecha' => date('Y-m-d (H:i a)'),
                            'comprador' => $comprador, 'vendedor' => $vendedor, 'pub' => $pub, 'foto1' => $foto1])
                        ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                        ->setTo($vendedor->email)//->setCc('wokeer+admin@gmail.com')
                        ->setSubject('Te preguntaron por (' . $pub->modelo->marca->nombre . ' ' . $pub->modelo->nombre . ' ' . $pub->ano . ') en Vendetumoto.co (ID Publicación #' . $pub->id . ' ) | ' . date('Y-m-d (H:i a)'))
                        ->send();





                    echo Json::encode(['res' => true]);
                }else{
                    Utils::dumpx($model->getErrors());
                }




            } else
                Utils::dumpx($model->getErrors());
        } else
            echo Json::encode(['res' => false]);
    }


    public function actionAjaxadddenuncia()
    {
        extract($_POST);

        //Utils::dumpx($_POST);
        if (!Yii::$app->user->isGuest) {
            $model = new PubDenuncia();
            $model->fecha = date('Y-m-d H:i:s');
            $model->motivo = $motivo;
            $model->mensaje = $mensaje;
            $model->id_usuario = Yii::$app->user->identity->id;
            $model->id_publicacion = $id_publicacion;

            if ($model->save()) {
                $pub = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
                //Enviar email al Admin
                Yii::$app->mailer->compose('layouts/enviar_denuncia',
                    ['tipo' => 2, 'mensaje' => $model->mensaje, 'motivo' => $model->_motivo[$motivo], 'mensaje' => $mensaje, 'id_pub' => $id_publicacion, 'fecha' => date('Y-m-d H:ia')
                    ])
                    ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                    ->setTo(['wokeer+admin@gmail.com' => 'Vendetumoto.co'])
                    ->setSubject('Admin, Han denunciado ésta publicación: (' . $pub->tipo->nombre . ' ' . $pub->modelo->marca->nombre . ' ' . $pub->modelo->nombre . ') en Vendetumoto.co | [' . date('Y-m-d H:ia') . ']')
                    ->send();

                echo Json::encode(['res' => true]);
            } else echo Json::encode(['res' => false]);
        } else echo Json::encode(['res' => false, 'msg' => 'ERROR: Para poder denunciar una publicación, debes primero registrarte en Vendetumoto.co']);

    }

    public function actionPlacaexiste()
    {
        extract($_POST);
        //Utils::dump($_POST);
        //var_dump($placa);
        //var_dump($id_pub);
        /*
            [placa] => 323WWW
            [id_pub] => 14
        */

        if(isset($placa) && $placa!='' && isset($id_pub) && $id_pub!=''){ //Editar Publicación
           // Utils::dumpx('placa="'.$placa.'" and (estado=1 || estado=2)');
            if ($placa != null) {
                if ($placa != '') {
                    $existe = PubPublicacion::find()->where('placa="'.$placa.'" and  (estado=1 || estado=2 || estado=7) and id!='.$id_pub)->count();
                    if ($existe == 1) {
                        echo Json::encode(['res' => true]); //TRUE: SI EXISTE
                        exit;
                    }
                }
            }

        }else
            if(isset($placa) && $placa!='' && $id_pub=='')
            { //Nueva Publicación
                if ($placa != null) {
                   // Utils::dumpx('placa="'.$placa.'" and (estado=1 || estado=2)');
                    if ($placa != '') {
                        $existe = PubPublicacion::find()->where('placa="'.$placa.'" and (estado=1 || estado=2 || estado=7)')->count();
                     //   Utils::dumpx('placa="'.$placa.'" and (estado=1 || estado=2)');
                        if ($existe == 1) {
                            echo Json::encode(['res' => true]); //TRUE: SI EXISTE
                            exit;
                        }
                    }
                }
            //$existe = PubPublicacion::find()->where('placa="'.$placa.'" and estado= and id_usuario!='.Yii::$app->user->identity->id)->count();
            }

        echo Json::encode(['res' => false]); //FALSE: NO EXISTE
        exit;
        /*
           //LOGICA DE PLACA
            CREAR NUEVA:
                    Validar que PLACA NO EXISTA en Publicaciones Estado:Activo

            Editar :
                    Validar que PLACA NO EXISTA en Publicaciones Estado:Activo - sin contar con la propia


           BLOQUEAR CAMPOS AL EDITAR:
           ------------------------------------------------
            Si ACTIVO:
                       BLOQUEAR:  Marca + modelo + placa

            Si otro Estado:
                       ACTIVO + INACTIVO + RECHAZADO + BORRADOR + PAUSADO : SI EDITAR  (editar todo menos: Marca + modelo + placa)
                       PENDIENTE x APROBAR + BLOQUEADO + VENDIDO + ELIMINADO: NO EDITAR

        */



    }

    public function actionEmailexiste()
    {
        extract($_POST);
        // Utils::dump($_POST);
        if (isset($email)) {

            if ($email != null) {
                if ($email != '') {
                    $existe = \common\models\User::find()->where('username="' . $email . '"')->count();
                    //Utils::dumpx($existe);
                    if ($existe == 0) {
                        echo Json::encode(['res' => true]); //TRUE: NO EXISTE
                        exit;
                    }
                }
            }
        }

        echo Json::encode(['res' => false]); //FALSE: SI EXISTE
    }

    public function actionExistepassword()
    {
        extract($_POST);
        // Utils::dump($_POST);
        if (isset($email)) {

            if ($email != null) {
                if ($email != '') {
                    $existe = \common\models\User::find()->where('username="' . $email . '" and pass="' . $pass . '"')->count();
                    //Utils::dumpx($existe);
                    if ($existe > 0) {
                        echo Json::encode(['res' => true]); //TRUE: SI EXISTE
                        exit;
                    }
                }
            }
        }

        echo Json::encode(['res' => false]); //FALSE: SI EXISTE
    }



    public function actionIndex()
    {

        // PUBLICACIONES POPULARES...

        //PREMIUM
        $anuncios_full = 1; /*PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where(' p.id=pa.id_publicacion and pa.id_paquete=3 and p.estado=1')->offset(0)->limit(18)->orderBy('RAND()')->all();
*/
        //ESTANDARD
        $anuncios_full_st = 1;
        /*PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where(' p.id=pa.id_publicacion and pa.id_paquete=1 and p.estado=1 ')->offset(0)->limit(18)->orderBy('RAND()')->all();
*/

        //\common\components\Utils::dumpx($anuncios_full);
        $anuncios_full = [];

        $anuncios_full['premiun'] =  Yii::$app->db->createCommand(
            'select p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas
               from pub_publicacion as p, payments as pa
              where  p.id=pa.id_publicacion and pa.id_paquete=3 and p.estado=1
              group by p.id  order by RAND()
              limit 0,18
            ')->queryAll();

        $anuncios_full['standard'] = Yii::$app->db->createCommand(
            ' select p.*,  (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas
                from pub_publicacion as p, payments as pa
               where  p.id=pa.id_publicacion and pa.id_paquete=1 and p.estado=1
               group by p.id  order by RAND()
               limit 0,18
            ')->queryAll();

       // \common\components\Utils::dumpx($anuncios_full);

        // LAS MÁS BUSCADAS...
        $anuncios_full2 = PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where('p.id=pa.id_publicacion and pa.id_paquete=3 and p.estado=1')->offset(0)->limit(11)->orderBy('RAND()')->all();


        $anuncios_full2_st = PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where('p.id=pa.id_publicacion and pa.id_paquete=1 and p.estado=1')->offset(0)->limit(11)->orderBy('RAND()')->all();



        //NUESTRAS RECOMENDADAS...
        $anuncios_full3 = PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where('p.id=pa.id_publicacion and pa.id_paquete=3 and p.estado=1')->offset(0)->limit(17)->orderBy('RAND()')->all();

        $anuncios_full3_st = PubPublicacion::find()
            ->select('p.*, (select count(*) from pub_estadisticas where p.id=id_publicacion and tipo=1) as visitas')
            ->from('pub_publicacion as p, payments as pa')
            ->where('p.id=pa.id_publicacion and pa.id_paquete=1 and p.estado=1')->offset(0)->limit(17)->orderBy('RAND()')->all();



        //PUBLICIDADES ADS
        $fotos = AdsPublicidad::find()->where('estado=1 and tipo_publicidad=1')->orderBy('id DESC')->all();
        if ($fotos) {
            foreach ($fotos as $foto) {
                $images[] = '<img src="' . $foto->imagen . '" class="img-fluid img-responsive center"  style=" max-height: 180px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; "    />';
            }
        } else {
            //No hay imagenes vendidas a ninguna empresa
            $images[] = '<img src="' . Yii::$app->request->baseUrl . '/img/ads-publicidad-1.jpg' . '" class="img-fluid img-responsive center" alt="Publica en este espacio tu empresa.">';
        }

        $fotos2 = AdsPublicidad::find()->where('estado=1 and tipo_publicidad=2')->orderBy('id DESC')->all();
        if ($fotos2) {
            foreach ($fotos2 as $foto) {
                $images2[] = '<img src="' . $foto->imagen . '" class="img-fluid img-responsive center"  style=" max-height: 180px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; "    />';
            }
        } else {
            //No hay imagenes vendidas a ninguna empresa
            $images2[] = '<img src="' . Yii::$app->request->baseUrl . '/img/ads-publicidad-2.jpg' . '" class="img-fluid img-responsive center" alt="Publica en este espacio tu empresa.">';
        }

        $fotos3 = AdsPublicidad::find()->where('estado=1 and tipo_publicidad=5')->orderBy('id DESC')->all();
        if ($fotos3) {
            foreach ($fotos3 as $foto) {
                $images3[] = '<img src="' . $foto->imagen . '" class="img-fluid img-responsive center"  style=" max-height: 180px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; "    />';
            }
        } else {
            //No hay imagenes vendidas a ninguna empresa
            $images3[] = '<img src="' . Yii::$app->request->baseUrl . '/img/ads-publicidad-2.jpg' . '" class="img-fluid img-responsive center" alt="Publica en este espacio tu empresa.">';
        }

        $fotos4 = AdsPublicidad::find()->where('estado=1 and tipo_publicidad=3')->orderBy('id DESC')->all();
        if ($fotos4) {
            foreach ($fotos4 as $foto) {
                $images4[] = '<img src="' . $foto->imagen . '" class="img-fluid img-responsive center"  style=" max-height: 180px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; "   />';
            }
        } else {
            //No hay imagenes vendidas a ninguna empresa
            $images4[] = '<img src="' . Yii::$app->request->baseUrl . '/img/cinco-anos-experiencia-vtm.jpg' . '" class="img-fluid img-responsive center" alt="Publica en este espacio tu empresa.">';
        }

        $visitas = PubEstadisticas::find()->where('tipo=1')->groupBy('tipo,id_usuario,fecha')->count();

        return $this->render('index', [
            'anuncios_full'  => $anuncios_full,/*   'anuncios_full_st' => $anuncios_full_st,
            'anuncios_full2' => $anuncios_full2, 'anuncios_full2_st' => $anuncios_full2_st,
            'anuncios_full3' => $anuncios_full3, 'anuncios_full3_st' => $anuncios_full3_st,*/
            'images' => $images,
            'imagesIZQ' => $images2,
            'imagesDER' => $images3,
            'imagesABA' => $images4,
            'visitas' => $visitas,
        ]);
//→
    }

    public function actionPubdetalle()
    {
        extract($_GET);

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado2';

        $existe = PubPublicacion::find()->where('id=' . $id)->count();
        if ($existe == 1) {
            $model = PubPublicacion::find()->where('id=' . $id . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
            if ($model && $model->id_usuario == Yii::$app->user->identity->id) {
                $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->orderBy('img_order asc')->all();
                if ($fotos) {
                    $c = 0;
                    foreach ($fotos as $foto) {
                        $imagesPeke[] = '<img  data-target="#w1" data-slide-to="' . $c . '" class="btn_galeria img-thumbnail img-responsive center" data-method="post" style=" cursor:pointer;display: block; max-width: 100%; max-height: 100%; width: auto; height: auto; "   src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />';
                        $images[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="border: 0px solid #ffffff; cursor:pointer; max-height: 393px; max-width:544px;    display: block;    width: auto;    height: auto;cursor:pointer;  "   src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />';
                        $c++;
                    }
                } else {
                    //No tiene imagenes
                    $images[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="width:100%;cursor:pointer;" title="No tiene fotos adjuntas" src="' . Yii::$app->request->baseUrl . '/img/no-image-moto.png"  />';
                    $imagesPeke[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="width:100%;cursor:pointer;" title="No tiene fotos adjuntas" src="' . Yii::$app->request->baseUrl . '/img/no-image-moto.png"  />';
                }

                /*if ($fotos) {

                    foreach ($fotos as $foto) {
                        $images[] = '<img  class="btn_galeria img-thumbnail img-responsive" data-method="post" style="width:100%;cursor:pointer; " src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />';

                    }


                } else {
                    //No tiene imagenes
                    $images[] = '<img  class="btn_galeria img-thumbnail img-responsive" data-method="post" style="width:100%;cursor:pointer;" title="No tiene fotos adjuntas" src="' . Yii::$app->request->baseUrl . '/img/no-image-moto.png"  />';

                }*/

                return $this->render('pubdetalle', ['model' => $model, 'images' => $images, 'imagesPeke' => $imagesPeke]);
            } else {
                echo 'ERROR: ¡Usuario no tiene permisos para ver esta publicacion!. <a href="javascript:history.back()">volver atrás</a>';
                exit;
            }
        } else
            echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>';
        exit;


    }

    public function actionPubdetallepublico()
    {
        extract($_GET);

        if (isset($_GET['id']) && $_GET['id'] != '') {
            //$categorias = \common\models\ArtCategoria::find()->orderBy('nombre')->all();
            $articulo = \common\models\PubPublicacion::find()->where(['id' => $id])->one();
            if ($articulo) {

                $foto_principal = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->orderBy('img_order asc')->one();
                if (!$foto_principal) {
                    $foto = Yii::$app->request->baseUrl . '/img/no-image-moto.png';
                } else {
                    $foto = 'https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $articulo->carpeta . '/' . $foto_principal->archivo;
                }

                //Pongo TAGS
                \Yii::$app->view->registerLinkTag(['rel' => 'canonical', 'href' => Url::base(true) . '/site/pubdetallepublico?id=' . $id]);
                \Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => 'Se vende ' . Utils::l($articulo->modelo->marca->nombre . ' ' . $articulo->modelo->nombre. ' ' . $articulo->ano. ' en ' . $articulo->ciudad->name)]);
                \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Utils::l($articulo->descripcion), 'data-head-react' => 'true']);
                \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => 'motos, publicaciones, anuncios, vender motos, cali, colombia, medellin, vende tu moto, miles de motos, marcas, modelos' /*Utils::l($tagstr)*/]);
                \Yii::$app->view->registerMetaTag(['name' => 'robots', 'content' => 'index, follow']);
                \Yii::$app->view->registerMetaTag(['name' => 'author', 'content' => '@Vendetumoto']);



                //Twitter
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:card', 'content' => 'summary' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:site', 'content' => '@Vendetumoto' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:creator', 'content' => '@Vendetumoto' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:title', 'content' => 'Se vende ' . Utils::l($articulo->modelo->marca->nombre . ' ' . $articulo->modelo->nombre. ' ' . $articulo->ano. ' en ' . $articulo->ciudad->name)]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:description', 'content' => ucfirst(Utils::l($articulo->descripcion))]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:image', 'content' => $foto ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:image:src', 'content' => $foto ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:domain', 'content' => 'vtmclientes.digitalventures.com.co']);


                //El Titulo debe ser: "Se vende Marca + Modelo + Ano en Ciudad"
                //Subtitulo o Descripcion: "Poner la descripcion que haya puesto el cliente en su publicacion"
                 //Facebook
                \Yii::$app->view->registerMetaTag(['property' => 'og:locale', 'content' => 'es_ES' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => 'Vente Tu Moto' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true) . '/site/pubdetallepublico?id=' . $id, 'data-head-react' => 'true']);
                \Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => 'Se vende ' . Utils::l($articulo->modelo->marca->nombre . ' ' . $articulo->modelo->nombre. ' ' . $articulo->ano. ' en ' . $articulo->ciudad->name), 'data-head-react' => 'true']);
                \Yii::$app->view->registerMetaTag(['property' => 'og:description','content' => ucfirst(Utils::l($articulo->descripcion)), 'data-head-react' => 'true']);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $foto ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:secure_url', 'content' => $foto ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:width', 'content' => '795' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:height', 'content' => '495' ]);


            } else {
                echo('Url no existe... <a style="text-decoration:none;color:blue" href="https://vendetumoto.co/blog"> << volver al BLOG</a>');
                exit;
            }
        }

        //Utils::dump($_POST);
        /* if (Yii::$app->user->isGuest) {
        Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
             return $this->redirect(['/site/index']);
         }*/
        $imagesADS = [];
        $modPreg = new PubMensaje();
        $existe = PubPublicacion::find()->where('id=' . $id)->count();
        if ($existe == 1) {
            $model = PubPublicacion::find()->where('id=' . $id)->one();
            if ($model) {
                $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->orderBy('img_order asc')->all();
                if ($fotos) {
                    $c = 0;
                    foreach ($fotos as $foto) {
                        $imagesPeke[] = '<img  data-target="#w0" data-slide-to="' . $c . '" class="btn_galeria img-thumbnail img-responsive center" data-method="post" style=" cursor:pointer;display: block; max-width: 100%; max-height: 100%; width: auto; height: auto; "   src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />';
                        $images[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="border: 0px solid #ffffff; cursor:pointer; max-height: 393px; max-width:544px;    display: block;    width: auto;    height: auto;cursor:pointer;  "   src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />';
                        $c++;
                    }
                } else {
                    //No tiene imagenes
                    $images[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="width:100%;cursor:pointer;" title="No tiene fotos adjuntas" src="' . Yii::$app->request->baseUrl . '/img/no-image-moto.png"  />';
                    $imagesPeke[] = '<img  class="btn_galeria img-thumbnail img-responsive center" data-method="post" style="width:100%;cursor:pointer;" title="No tiene fotos adjuntas" src="' . Yii::$app->request->baseUrl . '/img/no-image-moto.png"  />';
                }

                //**************************************

                //**************************************


                //VEHICULOS RELACIONADOS: Son los que tengan el mismo: (Marca - Modelo)
                $pub_actual = PubPublicacion::find()->where('id=' . $id)->one();

//                $anuncios_rel = PubPublicacion::find()
//                    ->select('p.*')
//                    ->from('pub_estadisticas v, pub_publicacion as p, payments as pa')
//                    ->where('p.id=v.id_publicacion and p.id=pa.id_publicacion and pa.id_paquete=3 and p.id!=' . $id)
//                    ->groupBy('v.id_publicacion')->limit(111)->orderBy('RAND()')->all();

                $sql = 'SELECT pl.id_paquete, p.*
                            FROM payments as pl, pub_publicacion as p
                            WHERE p.id=pl.id_publicacion and p.id_modelo='.$pub_actual->id_modelo.' and p.id!='.$id.' and p.estado=1
                            
                            ORDER BY id_paquete DESC limit 6
                            ';
                $anuncios_rel = Yii::$app->db->createCommand($sql) ->queryAll();
               // $anuncios_rel = PubPublicacion::find()->where('estado=1 and id_modelo='.$pub_actual->id_modelo.' and id!='.$id)->limit(6)->all();
                //PUBLICIDADES ADS
                $fotos = AdsPublicidad::find()->where('estado=1 and tipo_publicidad=4')->orderBy('id DESC')->all();
                if ($fotos) {
                    foreach ($fotos as $foto) {
                        $imagesADS[] = '<img src="' . $foto->imagen . '" class="img-fluid img-responsive center"  style=" max-height: 180px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; "    />';
                    }
                } else {
                    //No hay imagenes vendidas a ninguna empresa
                    $imagesADS[] = '<img src="' . Yii::$app->request->baseUrl . '/img/ads-pub-publicacion.png' . '"   class="img-fluid img-responsive center" alt="Publica en este espacio tu empresa.">';
                }

                //Si está logueado y mandó un mensaje...
                if (isset($_POST['PubMensaje']['mensaje'])) {
                    if ($_POST['PubMensaje']['mensaje'] != '') {
                        $modelMen = new PubMensaje();
                        $modelMen->fecha = date('Y-m-d H:i:s');
                        //mirar el último mensaje de esa publicación
                        $modelMen->id_creador = Yii::$app->user->identity->id;
                        $modelMen->mensaje = $_POST['PubMensaje']['mensaje'];
                        $modelMen->id_publicacion = $id;
                        /*
                        //Utils::dump('id_publicacion='.$id.' and id_creador='.Yii::$app->user->identity->id);
                        //Debe haber almenos un mensaje + id_pub en PubMensaje, si existe, traiga el id_papa
                        if($exist_msg_anterior){
                            $modelMen->id_papa = $exist_msg_anterior->id_papa; //Ya existe un HILO de mensajes, traiga el papa del primer registro
                        }else
                            $modelMen->id_papa = 0; //0: Es el primer mensaje de ese USR a esa Publicacion*/

                        if ($modelMen->save()) {
                            //Pregunto si ya hay un mensaje de ese Comprador a esa Publicacion
                            $exist_msg_anterior = PubMensaje::find()->where('id_publicacion=' . $id . ' and id_creador=' . Yii::$app->user->identity->id)->limit(1)->one();
                            if ($exist_msg_anterior) {
                                $modelMen->id_papa = $exist_msg_anterior->id_papa;
                            } else {
                                $modelMen->id_papa = $modelMen->id;
                            }
                            $modelMen->save();
                            //Enviar EMAIL **************************************************************


                            //****************************************************************************************************************************
                            Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Enviado</b> el <b>Mensaje</b> al vendedor con éxito!',]);
                            return $this->redirect(['/site/pubdetallepublico', 'id' => $id, 'imagesADS' => $imagesADS, 'imagesPeke' => $imagesPeke]);
                            exit;
                        } else {

                            Utils::dumpx($modelMen->getErrors());
                        }


                    } else {
                        Yii::$app->getSession()->setFlash('error', ['message' => 'ERROR: ¡Debes ingresar un mensaje al vendedor!',]);
                        return $this->render('pubdetallepublico', ['model' => $model, 'images' => $images, 'modPreg' => $modPreg,
                            'anuncios_rel' => $anuncios_rel, 'imagesADS' => $imagesADS, 'imagesPeke' => $imagesPeke
                        ]);
                    }

                } else {
                    return $this->render('pubdetallepublico', ['model' => $model, 'images' => $images, 'modPreg' => $modPreg,
                        'anuncios_rel' => $anuncios_rel, 'imagesADS' => $imagesADS, 'imagesPeke' => $imagesPeke
                    ]);
                }


            } else
                echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>';
            exit;
        } else {
            echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>';
            exit;
        }


    }


    public function oAuthSuccess($client)
    {

        // URI de redireccionamiento de OAuth válidos = https://dominio.com.co/site/auth?authclient=facebook
        if (!Yii::$app->user->isGuest) return $this->goHome();

        $mode = Yii::$app->getRequest()->getQueryParam('mode');
        $attributes = $client->getUserAttributes();
        $serviceId = $attributes['id'];
        $serviceProvider = $client->getId();
        $serviceTitle = $client->getTitle();
        $firstname = '';
        $lastname = '';
        $fullname = '';
        $username = '';
        $email = '';

        switch ($serviceProvider) {
            case 'facebook':
                $email = $attributes['email'];
                $fullname = $attributes['name'];
                break;
            case 'google':
                $email = $attributes['email'];
                if (isset($attributes['displayName'])) {
                    $fullname = $attributes['displayName'];
                }
                if (isset($attributes['name']['family_name']) and isset($attributes['name']['given_name'])) {
                    $lastname = $attributes['name']['family_name'];
                    $firstname = $attributes['name']['given_name'];
                }
                break;
            case 'twitter':
                $username = $attributes['screen_name'];
                $fullname = $attributes['name'];
                // to do - fix social helpers
                $email = $attributes['email'];
                break;
        }

        if (isset($attributes)) {
            //Si el usuario existe lo Loguea de inmediato
            $user = \common\models\User::find()->where(['username' => $email])->one();
            if (!empty($user)) {
                Yii::$app->user->login($user);
                $model = new LoginForm();
                $modelSignup = new SignupForm();
                $modelCliU = new CliUsuario();

                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    return $this->goBack();
                } else {
                    return $this->render('login', [
                        'model' => $model,
                        'modelSignup' => $modelSignup,
                        'modelCliU' => $modelCliU,
                    ]);
                }
            } else {
                //NO EXISTE: GRABELO COMO NUEVO DESDE FACEBOOK

                $model = new SignupForm();
                //    $session = Yii::$app->session;
                //    if (!empty($session['attributes'])){
                //        $model->username = $session['attributes']['email'];
                //        $model->email = $session['attributes']['email'];
                //    }

                $model->username = $email;
                $model->email = $email;

                $model->password = Yii::$app->security->generatePasswordHash(123456);
                $model->repeat_password = $model->password; //Yii::$app->security->generatePasswordHash($email);

                //Utils::dumpx($attributes['name'])    ;

                if ($user = $model->signup()) {

                    if (Yii::$app->getUser()->login($user)) {

                        //Guardar en tabla Usuarios Cliente 
                        $modelCli = new CliUsuario();
                        $modelCli->nombre = $attributes['name'];
                        $modelCli->id_cliente = 1;
                        $modelCli->id_usuario = $user->id;
                        $modelCli->email = $user->email;
                        $modelCli->rol = 2;
                        $modelCli->estado = 1;
                        $modelCli->fecha = date('Y-m-d H:i:s');
                        $modelCli->check_acepto_term = 1;
                        if (isset($model->promocionales) && $model->promocionales != '') $modelCli->check_recibe_prom = 1; else $modelCli->check_recibe_prom = 0;
                        //Utils::dumpx($modelCli->attributes);

                        if ($modelCli->save()) {
                            Yii::$app->db->createCommand() //ingresamos el usuario al grupo Directores Comerciales
                            ->insert(Yii::$app->db->tablePrefix . 'auth_assignment', [
                                'user_id' => $modelCli->id_usuario,
                                'item_name' => 'CLI_USUARIO',
                                'created_at' => time(),
                            ])->execute();
                        } else {
                            Utils::dumpx($modelCli->getErrors());
                        }


                        //Envío al usuario + admin
                        $nuevafecha = strtotime('+2 hour', time());
                        $nuevafecha = date('Y-m-d H:ia', $nuevafecha);
                        Yii::$app->mailer->compose('layouts/email_registro', ['username' => $user->email, 'password' => '***'])
                            ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                            ->setTo($modelCli->email)
                            ->setBcc([Yii::$app->params['adminEmail'] => "Administrador Vendetumoto.co"]) //;
                            ->setSubject('Bienvenido a Vendetumoto.co | ' . $modelCli->email . ' [' . $nuevafecha . ']')
                            ->send();

                        //ENVIA a admin

                        // return $this->redirect(\yii\helpers\Url::to(['/site/bienvenido']));
                        Yii::$app->getUser()->setReturnUrl(\yii\helpers\Url::to(['/']));
                    }
                }
            }

        }

    }


    public function actionMisfavoritos()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado2';


        $todos = false;
        //Utils::dump($_GET);

        if (isset($_GET['VMisFavoritos'])) {
            extract($_GET['VMisFavoritos']);
            if ($palabras_search != '') {
                $condition = [
                    'AND',                                //Operand (C1 OR C2)
                    [                                    //Condition1 (C1) which is complex
                        'OR',                            //Operand (C1.1 OR C1.2)
                        ['like', 'vehiculo', $palabras_search],   //C1.1
                        ['like', 'ciudad', $palabras_search], //C1.2
                    ]
                ];
            } else {
                $todos = true;
            }
        }


        if ($todos || !isset($condition)) {

            $sql = \common\models\VMisFavoritos::find()
                ->andWhere(['id_user_fav' => Yii::$app->user->identity->id,])
                ->andWhere('estado=1')
                ->orderBy('id DESC');

            //Utils::dumpx($sql);

        } else if (isset($condition)) {

            $sql = \common\models\VMisFavoritos::find()
                ->andWhere(['id_user_fav' => Yii::$app->user->identity->id])
                ->andWhere('estado=1')
                ->andWhere($condition)
                ->orderBy('id DESC');
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $sql,
            'pagination' => [
                'pageSize' => 5,
            ],
            'TotalCount' => $sql->count(),
        ]);


        //$sql = \common\models\VMisFavoritos::find()->where('id_user_fav='.Yii::$app->user->identity->id)->orderBy('id DESC');

        // Utils::dump(Yii::$app->user->identity->id);
        /*
                $dataProvider = new ActiveDataProvider([
                    'query' => $sql,
                    'pagination' => [
                        'pageSize' => 5,
                    ],
                    'TotalCount' => $sql->count(),
                ]);*/

        return $this->render('misfavoritos', ['dataProvider' => $dataProvider]);
    }


    public function actionResultados()
    {
        date_default_timezone_set('America/Bogota');
        set_time_limit(0); ini_set('display_errors','On');  ini_set('memory_limit', '100000M');ini_set('max_execution_time', '30000');
      //  Utils::dump($_GET); //.' '.$sql_city
        $model = new PubPublicacion();
        $desde = 0;
        $hasta = 50000000;
        extract($_GET);
        $str_marca = $str_modelo = $str_tipo = $str_cilindraje = $str_ano = $str_vendedor = $str_condicion = $str_recorrido = $str_otras = '';

        $sql_city = '';
        if(isset($id_ciudad) && $id_ciudad!=''){
            $sql_city = ' and ( id_ciudad='.$id_ciudad.' )';
        }


        //BUSCADOR ARRIBA: No tiene en cuenta NINGUN FILTRO
        if(isset($buscador_arriba) && $buscador_arriba!=''){

            $tipos = MotTipo::find()->where('estado=1')->orderBy('nombre')->all();
            $marcas = MotMarca::find()->where('estado=1')->orderBy('nombre')->all();
            $modelos = VBuscadorAnuncios::find()->where('estado=1')->groupBy('id_modelo')->orderBy('modelo')->all();

            $sql = "SELECT cilindraje  FROM pub_publicacion p  WHERE estado=1  GROUP BY cilindraje";
            $cilindrajes = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT ano FROM pub_publicacion p WHERE estado=1 GROUP BY ano";
            $anos = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT tipo_vendedor, replace(replace(tipo_vendedor,'1','Directo'),'2','Concesionario') as vendedor FROM pub_publicacion p WHERE estado=1 GROUP BY tipo_vendedor";
            $vendedores = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT condicion as tipo_condicion, replace(replace(condicion,'1','Nueva'),'2','Usada') as condicion FROM pub_publicacion p WHERE estado=1 GROUP BY tipo_condicion";
            $condicion = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT recorrido, unid_recorrido, concat(recorrido,' ',concat(replace(replace(replace(unid_recorrido,'1','Klms'),'2','Millas'),'3','Horas'))) as str_recorrido
                      FROM pub_publicacion p  WHERE estado=1  
                     GROUP BY concat(recorrido,' ',concat(replace(replace(replace(unid_recorrido,'1','Klms'),'2','Millas'),'3','Horas')))";
            $recorridos = Yii::$app->db->createCommand($sql)->queryAll();
            //OTRAS_CARACTERISTICAS
            $sql = "SELECT car.id, car.nombre as caracteristica  FROM pub_caracteristica as pcar, mot_caracteristicas AS car,  pub_publicacion as p
                    where p.id=pcar.id_publicacion and car.id=pcar.id_caracteristica and p.estado=1 group by car.id;";
            $otras_car = Yii::$app->db->createCommand($sql)->queryAll();
            $anos_home = [];
            /*if (isset($ano_desde) && $ano_desde != '' && isset($ano_hasta) && $ano_hasta != '') {
                $anos_home = [];
                for ($i = $ano_desde; $i <= $ano_hasta; $i++) {
                    $anos_home[$i] = $i;
                    $str_ano .= ' ano=' . $i . ' OR';
                }
                $str_ano = substr($str_ano, 0, strlen($str_ano) - 2);
            }*/


            //Parto la cadena en palabras y las comparo con los el titulo ()
            $palabras = explode(' ', trim($buscador_arriba));
            $str='';
            $str = ' vehiculo like "%'.$buscador_arriba.'%" ';
            //recorro cada una
            /*
            for($i=0;$i<count($palabras);$i++){
                //$str .= ' (vehiculo like"%'.$palabras[$i].'%" OR ciudad like"%'.$palabras[$i].'%") OR ';
                $str .= ' (vehiculo like"%'.$palabras[$i].'%") or ';
            }*/

           // $str = substr($str,0,strlen($str)-3);
            //echo $str; //exit;

            $sql = VBuscadorAnuncios::find()->andWhere('estado=1')->andWhere($str)->orderBy('id DESC');
            $dataProvider = new ActiveDataProvider([
                'query' => $sql,
                'pagination' => [
                    'pageSize' => isset($_GET['PubPublicacion']['items_x_pag']) && $_GET['PubPublicacion']['items_x_pag'] != '' ? $_GET['PubPublicacion']['items_x_pag'] : 6,
                ],
                'TotalCount' => $sql->count(),
            ]);


            return $this->render('resultados', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'tipos' => $tipos,
                'marcas' => $marcas,
                'modelos' => $modelos,
                'cilindrajes' => $cilindrajes,
                'anos' => $anos,
                'vendedores' => $vendedores,
                'condicion' => $condicion,
                'recorridos' => $recorridos,
                'otras_car' => $otras_car,
                'desde' => $desde,
                'hasta' => $hasta,
                'anos_home' => $anos_home

            ]);


        }
        else
        {
            $anos_home = [];
            if (isset($_GET['PubPublicacion'])) {
                extract($_GET['PubPublicacion']);
                if (isset($orden) && $orden != '') $model->orden = $orden;
                if (isset($items_x_pag) && $items_x_pag != '') $model->items_x_pag = $items_x_pag;
                if (isset($precio) && $precio != '') {
                    $vec = explode(',', $_GET['precio']);
                    if (count($vec) == 2) {
                        $desde = $vec[0];
                        $hasta = $vec[1];
                    }
                }
            }


            $str_marca = $str_modelo = $str_tipo = $str_cilindraje = $str_ano = $str_vendedor = $str_condicion = $str_recorrido = $str_otras = '';

            // FILTROS IZQUIERDOS
            $tipos = VBuscadorAnuncios::find()->select('id_tipo as id, tipo as nombre')->where('estado=1 '.$sql_city)->groupBy('id_tipo')->orderBy('nombre')->all();
            $marcas = VBuscadorAnuncios::find()->select('id_marca as id, nombre')->where('estado=1 '.$sql_city)->groupBy('id_marca')->orderBy('nombre')->all();
            $modelos = VBuscadorAnuncios::find()->select('id_modelo, modelo')->where('estado=1 '.$sql_city)->groupBy('id_modelo')->orderBy('modelo')->all();
            //Utils::dumpx($modelos);
            //$tipos = MotTipo::find()->where('estado=1')->orderBy('nombre')->all();
            //$marcas = MotMarca::find()->where('estado=1')->orderBy('nombre')->all();


            //MARCAS
            if (isset($marca) && $marca != '') {
                //sinó es array es por que viene del HOME: volverlo array
                if (!is_array($marca)) {
                    $aux[$marca] = $marca;
                    $marca = $aux;
                }
                //****** BORRAR DEL $_GET MODELOS SELECCIONADOS QUE NO ESTEN EN LAS MARCAS SELECCIONADAS *******************
                $borrar_mod_get = []; // Array donde se meterán los que hay que eliminar...
                if (isset($modelo) && $modelo != '') {
                    //sinó es array es por que viene del HOME: volverlo array
                    if (!is_array($modelo)) {
                        $aux2[$modelo] = $modelo;
                        $modelo = $aux2;
                    }
                    //recorro los modelos seleccionados y pregunto si están en alguna de los modelos de cada marca seleccionada
                    foreach ($modelo as $mod) {
                        foreach ($marca as $masel) {
                            $existe_modelo_sel_en_hijo_mod_marca_sel = VBuscadorAnuncios::find()->where('id_marca=' . $masel . ' and estado=1 and id_modelo=' . $mod)->count();
                            //echo 'id_marca=' . $masel . ' and estado=1 and id_modelo=' . $mod.'<hr>';
                            if ($existe_modelo_sel_en_hijo_mod_marca_sel == 0) {
                                $borrar_mod_get[$mod] = $mod;
                                break;
                            }
                        }
                    }
                }


                //Borrar de los Filtros GET
                foreach ($borrar_mod_get as $mod_borrar) if (isset($_GET['modelo'][$mod_borrar])) unset($_GET['modelo'][$mod_borrar]);
                //**********************************************************************************************************

                foreach ($marca as $reg) $str_marca .= ' id_marca=' . $reg . ' OR';
                $str_marca = substr($str_marca, 0, strlen($str_marca) - 2) ;
            }

            //MODELOS
            if (isset($modelo) && $modelo != '') {
                //sinó es array es por que viene del HOME: volverlo array
                if (!is_array($modelo)) {
                    $aux[$modelo] = $modelo;
                    $modelo = $aux;
                }

                foreach ($modelo as $reg) $str_modelo .= ' id_modelo=' . $reg . ' OR';
                $str_modelo = substr($str_modelo, 0, strlen($str_modelo) - 2) ;
            }


            //TIPOS
            if (isset($tipo) && $tipo != '') {
                //sinó es array es por que viene del HOME: volverlo array
                if (!is_array($tipo)) {
                    $aux[$tipo] = $tipo;
                    $tipo = $aux;
                }


                foreach ($tipo as $reg) $str_tipo .= ' id_tipo=' . $reg . ' OR';
                $str_tipo = substr($str_tipo, 0, strlen($str_tipo) - 2) ;
            }
            // echo 1111;exit;

            //cilindrajes
            if (isset($cilindrajes) && $cilindrajes != '') {
                foreach ($cilindrajes as $reg) $str_cilindraje .= ' cilindraje=' . $reg . ' OR';
                $str_cilindraje = substr($str_cilindraje, 0, strlen($str_cilindraje) - 2) ;
            }
            //ano


            if (isset($anos) && $anos != '') {
                foreach ($anos as $reg) {
                    $str_ano .= ' ano=' . $reg . ' OR';
                }
                $str_ano = substr($str_ano, 0, strlen($str_ano) - 2) ;
            } else {
                //Desde HOME: ano_desde - ano_hasta
                if (isset($ano_desde) && $ano_desde != '' && isset($ano_hasta) && $ano_hasta != '') {
                    $anos_home = [];
                    for ($i = $ano_desde; $i <= $ano_hasta; $i++) {
                        $anos_home[$i] = $i;
                        $str_ano .= ' ano=' . $i . ' OR';
                    }
                    $str_ano = substr($str_ano, 0, strlen($str_ano) - 2);
                }
            }


            //vendedores
            if (isset($vendedores) && $vendedores != '') {
                foreach ($vendedores as $reg) $str_vendedor .= ' tipo_vendedor=' . $reg . ' OR';
                $str_vendedor = substr($str_vendedor, 0, strlen($str_vendedor) - 2) ;
            }
            //condicion
            if (isset($condiciones) && $condiciones != '') {
                foreach ($condiciones as $reg) $str_condicion .= ' condicion=' . $reg . ' OR';
                $str_condicion = substr($str_condicion, 0, strlen($str_condicion) - 2) ;
            }
            //Recorrido_FULL
            if (isset($recorridos) && $recorridos != '') {
                foreach ($recorridos as $reg) {
                    $vec = explode('_', $reg);
                    switch ($vec[1]) {
                        case 1 :
                            $str = 'Kms';
                            break;
                        case 2 :
                            $str = 'Millas';
                            break;
                        case 3 :
                            $str = 'Horas';
                            break;
                    }
                    $str_recorrido .= ' recorrido_full="' . $vec[0] . ' ' . $str . '" OR';
                }
                $str_recorrido = substr($str_recorrido, 0, strlen($str_recorrido) - 2) ;
            }
            //otras
            if (isset($otras) && $otras != '') {
                foreach ($otras as $reg) $str_otras .= ' FIND_IN_SET(' . $reg . ',caracteristicas) OR';
                $str_otras = substr($str_otras, 0, strlen($str_otras) - 2) ;
            }


            //Posibles condiciones dependiendo de los Filtros IZQUIERDA seleccionados....
            $str_filtros = $this->actionGetFiltrosIzquierda($str_otras, $str_cilindraje, $str_marca, $str_modelo, $str_tipo, $str_ano, $str_vendedor, $str_condicion, $str_recorrido);

            //$modelos = VBuscadorAnuncios::find()->where('estado=1 '.$sql_city)->groupBy('id_modelo')->orderBy('nombre')->groupBy('id_modelo')->all();

            //  Utils::dumpx($modelos);

            if (isset($marca) && $marca != '') {
                $modelos = VBuscadorAnuncios::find()->where('estado=1 and (' . $str_marca . ') '.$sql_city)->groupBy('id_modelo')->orderBy('modelo')->all();
                // unset($_GET['modelo']);
            }
            //***************************************************************************************
            // Obtener todos los Checks, y con esos Checks traer los que cumplan esos CHECKS
            //***************************************************************************************

            $sql = "SELECT cilindraje  FROM pub_publicacion p  WHERE estado=1 $sql_city GROUP BY cilindraje";
            $cilindrajes = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT ano FROM pub_publicacion p WHERE estado=1 $sql_city GROUP BY ano";
            $anos = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT tipo_vendedor, replace(replace(tipo_vendedor,'1','Directo'),'2','Concesionario') as vendedor FROM pub_publicacion p WHERE estado=1 $sql_city GROUP BY tipo_vendedor";
            $vendedores = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT condicion as tipo_condicion, replace(replace(condicion,'1','Nueva'),'2','Usada') as condicion FROM pub_publicacion p WHERE estado=1 $sql_city GROUP BY tipo_condicion";
            $condicion = Yii::$app->db->createCommand($sql)->queryAll();
            $sql = "SELECT recorrido, unid_recorrido, concat(recorrido,' ',concat(replace(replace(replace(unid_recorrido,'1','Klms'),'2','Millas'),'3','Horas'))) as str_recorrido
                      FROM pub_publicacion p  WHERE estado=1 $sql_city  
                     GROUP BY concat(recorrido,' ',concat(replace(replace(replace(unid_recorrido,'1','Klms'),'2','Millas'),'3','Horas')))";
            $recorridos = Yii::$app->db->createCommand($sql)->queryAll();
            //OTRAS_CARACTERISTICAS
            $sql = "SELECT car.id, car.nombre as caracteristica  FROM pub_caracteristica as pcar, mot_caracteristicas AS car,  pub_publicacion as p
                    where p.id=pcar.id_publicacion and car.id=pcar.id_caracteristica and p.estado=1 $sql_city group by car.id;";
            $otras_car = Yii::$app->db->createCommand($sql)->queryAll();


            if(isset($str_filtros) && $str_filtros!='' && isset($id_ciudad) && $id_ciudad!=''){
                $str_filtros .= ' '.$sql_city;
            }else
                if((isset($str_filtros) || $str_filtros=='') && isset($id_ciudad) && $id_ciudad!=''){
                    $str_filtros .= '( id_ciudad='.$id_ciudad.' )';
                }

            //Utils::dump($anos);


            $where = '1=1';
            /* BUSCADOR ARRIBA */
            /*if (isset($_GET['txt_buscador']) && $_GET['txt_buscador'] != '') {
                $where .= ' and (vehiculo LIKE "%' . $_GET['txt_buscador'] . '%")';
            }*/

            // Utils::dump($_GET);
            /* RANGO DE PRECIOS */
            if (isset($_GET['precio']) && $_GET['precio'] != '') {
                $v_precio = explode(',', $_GET['precio']);
                $precio_desde = $v_precio[0];
                $precio_hasta = $v_precio[1];
                $where .= ' and (precio_desc>=' . $precio_desde . ' and precio_desc<=' . $precio_hasta . ')';
                $desde = $v_precio[0];
                $hasta = $v_precio[1];
            } else {
                $precio_desde = $precio_hasta = $desde = 0;
                $hasta = 50000000;
                if (isset($_GET['PubPublicacion']['precio_desde']) && $_GET['PubPublicacion']['precio_desde'] == '' && $_GET['PubPublicacion']['precio_hasta'] == '') {
                    $precio_desde = 0;

                    $where .= ' and (precio_desc>=' . $precio_desde . ' and precio_desc<=' . $precio_hasta . ')';
                    $desde = $precio_desde;

                } else
                    if (isset($_GET['PubPublicacion']['precio_desde']) && isset($_GET['PubPublicacion']['precio_desde']) && $_GET['PubPublicacion']['precio_desde'] == '' && isset($_GET['PubPublicacion']['precio_hasta']) && isset($_GET['PubPublicacion']['precio_hasta']) != '') {
                        $precio_desde = 0;
                        $precio_hasta = $_GET['PubPublicacion']['precio_hasta'];
                        $where .= ' and (precio_desc>=' . $precio_desde . ' and precio_desc<=' . $precio_hasta . ')';
                        $desde = $precio_desde;
                        $hasta = $_GET['PubPublicacion']['precio_hasta'];
                    } else
                        if (isset($_GET['PubPublicacion']['precio_desde']) && $_GET['PubPublicacion']['precio_desde'] != '' && isset($_GET['PubPublicacion']['precio_hasta']) && $_GET['PubPublicacion']['precio_hasta'] != '') {

                            $precio_desde = $_GET['PubPublicacion']['precio_desde'];
                            $precio_hasta = $_GET['PubPublicacion']['precio_hasta'];
                            $where .= ' and (precio_desc>=' . $precio_desde . ' and precio_desc<=' . $precio_hasta . ')';
                            $desde = $_GET['PubPublicacion']['precio_desde'];
                            $hasta = $_GET['PubPublicacion']['precio_hasta'];
                        }
            }

            /* ORDENAR POR */
            if (isset($_GET['PubPublicacion']['orden']) && $_GET['PubPublicacion']['orden'] != '') {
                if ($_GET['PubPublicacion']['orden'] == 1)
                    $sql = VBuscadorAnuncios::find()->where($where)->andWhere('estado=1')->andWhere($str_filtros)->orderBy('id_plan DESC, precio DESC');
                elseif ($_GET['PubPublicacion']['orden'] == 2)
                    $sql = VBuscadorAnuncios::find()->where($where)->andWhere('estado=1')->andWhere($str_filtros)->orderBy('id_plan DESC, precio ASC');
                else {
                    $sql = VBuscadorAnuncios::find()->where($where)->andWhere('estado=1')->andWhere($str_filtros)->orderBy('id_plan DESC');
                }
            }
            else //no hay ordenados
            {
              //  echo 1;
                $sql = VBuscadorAnuncios::find()->where($where)->andWhere('estado=1')->andWhere($str_filtros)->orderBy('id_plan DESC, id DESC');
                //echo 2;
            }

            //echo '----- '.$str_filtros.' -----------';
            //Utils::dumpx($sql);

            $dataProvider = new ActiveDataProvider([
                'query' => $sql,
                'pagination' => [
                    'pageSize' => isset($_GET['PubPublicacion']['items_x_pag']) && $_GET['PubPublicacion']['items_x_pag'] != '' ? $_GET['PubPublicacion']['items_x_pag'] : 6,
                ],
                'TotalCount' => $sql->count(),
            ]);


            return $this->render('resultados', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'tipos' => $tipos,
                'marcas' => $marcas,
                'modelos' => $modelos,
                'cilindrajes' => $cilindrajes,
                'anos' => $anos,
                'vendedores' => $vendedores,
                'condicion' => $condicion,
                'recorridos' => $recorridos,
                'otras_car' => $otras_car,
                'desde' => $desde,
                'hasta' => $hasta,
                'anos_home' => $anos_home

            ]);

        }





    }


    public function actionAddsuscriptor()
    {
        extract($_POST);

        if (isset($email) && $email != '') {
            $exist = Suscripciones::find()->where('email="' . $email . '"')->count();
            if ($exist == 0) {
                $model = new Suscripciones();
                $model->fecha = date('Y-m-d H:i:s');
                $model->email = $email;
                if ($model->save()) {
                    $nuevafecha = strtotime('+2 hour', time());
                    $nuevafecha = date('Y-m-d H:ia', $nuevafecha);
                    Yii::$app->mailer->compose('layouts/email_registro', ['username' => $model->email])
                        ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                        ->setTo($model->email)
                        // ->setBcc([Yii::$app->params['adminEmail'] => "Administrador Vendetumoto.co"]) //;
                        ->setSubject('Bienvenido al Newsletter de Vendetumoto.co | ' . $model->email . ' [' . $nuevafecha . ']')
                        ->send();
                }
                echo Json::encode(['res' => true, 'tipo' => 1, 'email' => $email]);
            } else {
                echo Json::encode(['res' => true, 'tipo' => 2, 'email' => $email]);
            }
        } else
            echo Json::encode(['res' => false]);
    }



    public function actionPausarpub()
    {
        extract($_POST);
        if (!empty($id_publicacion)) {
            $cliUser = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
        }

        //Pregunto si está Pausando o está Activando
        if ($op == 1) { //PAUSAR
            //Verificar que NO haya pausado antes el mismo anuncio:
            if ($cliUser->ultima_renovacion != "") {
                echo Json::encode(['res' => false, 'error' => 'ERROR: ¡Ya has PAUSADO este anuncio!  Sólo se puede PAUSAR una vez por publicación.']);
            } else {
                $cliUser->estado = 7; //7: PAUSADO
                $cliUser->fecha_edicion = date('Y-m-d H:i:s');
                $cliUser->ultima_renovacion = date('Y-m-d H:i:s');//Fecha en la cual se PAUSÓ el anuncio
                if ($cliUser->save()) {

                    //Calcular Fecha nueva mirando : campo : ultima_renovacion
                    //Enviar Email al Cliente notificando la pausa: y que tiene 20 días hábiles para Activarlo Nuevamente
                    //
                    //
                    echo Json::encode(['res' => 'El Anuncio ha sido <b>PAUSADO</b> con éxito. NO podrá pausar nuevamente este anuncio. En 20 días, si NO lo ha ACTIVADO nuevamente, se activará automáticamente.']);
                    exit;
                } else {
                    echo Json::encode(['res' => false, 'error' => 'No pudo pausarse. Verificar con soporte.']);
                }
            }
        } else { //ACTIVAR
            $cliUser->estado = 1; //1: ACTIVAR
            $cliUser->fecha_edicion = date('Y-m-d H:i:s');
            if ($cliUser->save()) {
                //Calcular Fecha nueva mirando : campo : ultima_renovacion
                //Enviar Email al Cliente notificando la pausa: y que tiene 20 días hábiles para Activarlo Nuevamente
                //
                //
                echo Json::encode(['res' => 'El Anuncio ha sido <b>ACTIVADO</b> con éxito. NO podrá pausar nuevamente este anuncio.']);
                exit;
            }
        }


    }

    public function actionEliminarpubcli()
    {
        extract($_POST);
        $cliUser = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
        $cliUser->estado = 8; //8: ELIMINADO X EL USUARIO (Lo elimina lógicamente, pero EL no lo volverá a ver...)
        $cliUser->fecha_edicion = date('Y-m-d H:i:s');
        if ($cliUser->save()) {
            //Enviar Email al Cliente notificando el Bloqueo, por denuncias de usuarios a su anuncio.
            //
            echo Json::encode(['res' => 'El Anuncio ha sido <b>ELIMINADO</b> con éxito.']);
        }


    }


    public function actionGetbotonescompartir()
    {
        extract($_POST);
        $model = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
        $url_self = Url::home('https') . 'site/pubdetallepublico?id=' . $id_publicacion;//(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        //$url_self =  Url::to(['site/pubdetallepublico','id'=>$model->id]);
        //https://www.linkedin.com/cws/share?url=https%3A%2F%2Fwpnovatos.com%2Fponer-botones-compartir-redes-sociales%2F
        //Se vende ' . Utils::l($articulo->modelo->marca->nombre . ' ' . $articulo->modelo->nombre. ' ' . $articulo->ano. ' en ' . $articulo->ciudad->name)
/*
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $url_self ?>&t=<?= 'Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí: ' ?> "><i class="fa fa-facebook-square" style="font-size: 40px;color: #425EAC"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?= urlencode($url_self) ?>" style="margin-left: 10px">
                                <i class="fa fa-linkedin-square" style="font-size: 40px"></i>
                            </a>
                            <a target="_blank" href="https://api.whatsapp.com/send?text=<?= 'Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. *Vende Tu Moto*  %20'.$url_self ?>" style="margin-left: 10px"><img class="iconos" src="<?= Yii::$app->request->baseUrl . '/img/wp.png' ?>"></img></a>
                            <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= 'Se vende '.\common\components\Utils::limpiarCadena2($model->modelo->marca->nombre.' '.$model->modelo->nombre).' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí: ' ?>&url=<?= $url_self ?>" style="margin-left: 10px"><i class="fa fa-twitter-square" style="font-size: 40px;color: #1DADEB"></i></a>
*/
        $botones = '
           <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' . $url_self . '&t=Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí: "><i class="fa fa-facebook-square" style="font-size: 40px;color: #425EAC"></i></a>
            <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url='. urlencode($url_self).'" style="margin-left: 10px">
                                <i class="fa fa-linkedin-square" style="font-size: 40px"></i>
                            </a>
           <a target="_blank" href="https://api.whatsapp.com/send?text=Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. *Vende Tu Moto* %20' . $url_self . '" style="margin-left: 10px;"><img style="width:40px;margin-top:-21px" src="' . Yii::$app->request->baseUrl . '/img/wp.png' . '"></img></a>
           <a target="_blank" href="https://twitter.com/share?text=Se Vende ' . \common\components\Utils::limpiarCadena2($model->modelo->marca->nombre . ' ' . $model->modelo->nombre).' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí:&url='.$url_self.'" style="margin-left: 10px"><i class="fa fa-twitter-square" style="font-size: 40px;color: #1DADEB"></i></a>
            ';
        echo Json::encode(['res' => $botones]);
    }

    public function actionDescargarimagenes()
    {
        extract($_GET);
        //Utils::dumpx($_POST);
        //$fotos = PubMultimedia::find()->where('tipo=1 and id_publcacion='.$id_publicacion)->all();

        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (isset($id_publicacion) && $id_publicacion != '') {
            $model = PubPublicacion::find()->where('id=' . $id_publicacion)->one();
            $dir = dirname(__DIR__) . '"/../../web/uploads/descargas/';
            if ($model) {
                $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->all();
                if ($fotos) {

                    $zip = new ZipArchive();
                    // Ruta absoluta
                    $file = 'fotos_publicacion_' . $id_publicacion . "_" . time() . ".zip";
                    $nombreArchivoZip = dirname(__DIR__) . "/../../web/uploads/descargas/" . $file;
                    // Creamos y abrimos un archivo zip temporal
                    $zip->open($nombreArchivoZip, ZipArchive::CREATE);

                    foreach ($fotos as $foto) {
                        //Leo la foto y la guardo en LOCAL
                        $this->getImage("https://vtmprod.s3.us-east-2.amazonaws.com/pubs/" . $foto->publicacion->carpeta . '/' . $foto->archivo, $dir, $foto->archivo, 0);

                        //Agrego cada imagen al archivo comprimido (.ZIP)
                        $zip->addFile($dir . $foto->archivo, $foto->archivo);
                    }


                    // Una vez añadido los archivos deseados cerramos el zip.
                    $zip->close();
                    // Creamos las cabezeras que forzaran la descarga del archivo como archivo zip.
                    header("Content-type: application/octet-stream");
                    header("Content-disposition: attachment; filename=" . $file);
                    // leemos el archivo creado
                    readfile($nombreArchivoZip);
                    foreach ($fotos as $foto) {
                        unlink($dir . $foto->archivo); //Destruye el archivo temporal
                    }
                    // Por último eliminamos el archivo temporal creado
                    unlink($nombreArchivoZip);//Destruye el archivo temporal

                }
            } else return false;

        }

    }


    public function actionMismensajes()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado2';

        //Obtengo algun mensaje donde yo todos los mensajes a partir del id_papa el ID_creador  del mensaje

        $sql = '   select m.id_publicacion, usr.nombre as usuario, p.vehiculo, m.id_papa, m.fecha as fecha_ult_mensaje , m.mensaje 
                      from (SELECT * 
                              FROM pub_mensaje 
                             WHERE (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') 
                             group by id, id_publicacion
                             ORDER BY id desc) as m, v_buscador_publicaciones as p, cli_usuario as usr
                    where m.id_publicacion=p.id and m.id_creador=usr.id_usuario
                    group by id_publicacion, id_papa
                    order by m.fecha desc;                    
                    ';
       // echo $sql;

        $pubs_chats = Yii::$app->db->createCommand($sql)->queryAll();

        // Utils::dumpx($pubs_chats);

        $dataProvider = new SqlDataProvider(['sql' => 'SELECT 1',]);
        return $this->render('mismensajes', ['pubs_chats' => $pubs_chats]);
    }

    public function actionGetchatssearch()
    {
        extract($_POST);
        $lista='';
        if($tipo==1){
            $sql = ' SELECT m.id_publicacion, p.usuario1 as comprador, p.vehiculo, vend.nombre as vendedor, m.id_papa, 
                         (select fecha  from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1) as fecha_ult_mensaje, 
                         (select id_creador from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1) as user_ult_mensaje,
                         comp.imagen
                    FROM v_buscador_publicaciones as p, pub_mensaje as m, cli_usuario as comp , cli_usuario as vend 
                    where (m.id_publicacion=p.id) and  vend.id_usuario=m.id_dueno_publicacion    
                          and (comp.id_usuario=(select id_creador from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1))
                          and (p.vehiculo like "%'.$mensaje.'%" || p.id like "%'.$mensaje.'%" || p.usuario1 like "%'.$mensaje.'%" || m.mensaje like "%'.$mensaje.'%")
                    group by m.id_publicacion 
                    order by fecha_ult_mensaje desc;';

        }else{
            $sql = '  SELECT m.id_publicacion, p.usuario1 as comprador, p.vehiculo, vend.nombre as vendedor, m.id_papa, 
                         (select fecha  from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1) as fecha_ult_mensaje, 
                         (select id_creador from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1) as user_ult_mensaje,
                         comp.imagen
                    FROM v_buscador_publicaciones as p, pub_mensaje as m, cli_usuario as comp , cli_usuario as vend 
                    where (m.id_publicacion=p.id) and  vend.id_usuario=m.id_dueno_publicacion    
                          and (comp.id_usuario=(select id_creador from pub_mensaje where (id_creador='.Yii::$app->user->identity->id.' or id_dueno_publicacion='.Yii::$app->user->identity->id.') order by id desc limit 1))
                    group by m.id_publicacion 
                    order by fecha_ult_mensaje desc;';
        }

        $pubs_chats = Yii::$app->db->createCommand($sql)->queryAll();

        for($i=0; $i < count($pubs_chats); $i++) {

            $articulo = \common\models\PubPublicacion::find()->where(['id' => $pubs_chats[$i]['id_publicacion']])->one();
            $foto_principal = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $pubs_chats[$i]['id_publicacion'])->orderBy('id asc')->one();
            if (!$foto_principal) {
                $foto = Yii::$app->request->baseUrl . '/img/no-image-moto.png';
            } else {
                $foto = 'https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $articulo->carpeta . '/' . $foto_principal->archivo;
            }
            
            $lista .= ' <div style=" border-bottom: 1px solid #DFDFDF;height: 122px;" class="ove cargar_chat" id_papa="'. $pubs_chats[$i]['id_papa'] .'"  id_pub="'. $pubs_chats[$i]['id_publicacion'] .'">
                                <table>
                                    <tr>
                                        <td class="image-cropper" style="padding:15px">
                                            <span class="circle-image">
                                            <img class="rounded profile-user-img img-responsive img-responsive" src="'.$foto.'" style="">
                                            </span>

                                        </td>
                                        <td style="width: 100%;margin-right: 5px">
                                            <p class="nombre_pub">'.$pubs_chats[$i]['vehiculo'] .'</p>
                                            <p class="comprador">'.$pubs_chats[$i]['comprador'] .'</p>
                                            <p class="id_mensaje">#'.$pubs_chats[$i]['id_publicacion'] .'</p>
                                            <p class="fecha pull-right">'. date('Y-m-d', strtotime($pubs_chats[$i]['fecha_ult_mensaje'])).' ('.date('h:i a', strtotime($pubs_chats[$i]['fecha_ult_mensaje'])).')</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>';


        }

        $script = " <script>$('.cargar_chat').click(function(e){
                            e.preventDefault();          
                                 $('#id_papa_actual').val($(this).attr('id_papa'));
                                 $('#id_pub_actual').val($(this).attr('id_pub'));
                                 $('#txt_mensaje').attr('style',' background: transparent; box-sizing: border-box;border-radius: 25.1351px;height: 50px;width: 95%;');
                                 $('.enviar').attr('style','cursor: pointer;margin-top: 12px;');
                                 
                                 $.post('".Url::to(['site/getchats'])."', {id_papa: $(this).attr('id_papa'), id_publicacion: $(this).attr('id_pub')}, 
                                   function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub); 
                                           // console.log(res); return false;
                                            if(res.res){                      
                                                 $('.chat_hilo').html(res.res);
                                            }else{
                                               alert('Error 1');
                                            }
                                        
                                   }
                                 );
                     });</script>";
        echo Json::encode(['res' => $lista.' '.$script ]);
        exit;

    }

    public function actionGetchats()
    {
        extract($_POST);
        $chat='';
        if(isset($id_papa))
         $chats = PubMensaje::find()->where('id_papa='.$id_papa)->all();
        else
         $chats = PubMensaje::find()->where('id_papa='.$id_papa)->all();

        $foto_principal = CliUsuario::find()->where('id_usuario=' . Yii::$app->user->identity->id)->orderBy('id asc')->one();
        if (isset($foto_principal->imagen) && $foto_principal->imagen!='') {
            $foto =  $foto_principal->imagen;
        } else {
            $foto = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOIAAADiCAYAAABTEBvXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAByhSURBVHgB7Z0LcFxXece/c3dXD1uWZCfxO8k6IRDbIZZJKbYDeB2muARau9MmLQwQGwxDeYxjQVpoh1hKhzBAkG2mEKbE2AbKw+Zhd9IkTlu8hjwoM8UyxE6ICdrUTpzEDZafkqXde3q+c/dKu9qH7u7e17nn+82sd7XaKLb2/vf/ne9xDgMiFPCBQ52QSCTBNJIQhyRw3gmGcTUw1ikfg3hu7MXiOZDPlYdBJv+6QWBc3MQ9F/em+bx8nBXfZzlxn+1nC5YOAhE4DAhfEYJLCsGlhCCSUmicdYEUWRVheQpDIWbAELdc7jDkWD8YZobNv6EfCN8gIXrImOiM+BLhTF2W6IISXK1IN+0Xf+d+yJoHUaBswcIMEJ5AQnSRfHi5FmKxleLLFPCCcDIaZMS/KQ08dxBGY2kSpnuQEBuEH/9NCmKJNcI91kZQeJPAMHxNQ250H7vy9Wkg6oaEWCPS9eLxLnG7Q4Rta9UJNb1GJoT2Qo7vY1ct2gtETZAQHSKdj8TnkLwozewuckpnkBCrIN2vpWWjEN+dJL66sdaVI9BLa8rKkBDLkHe/zeICSgHhHgzXk7ldojSyE4giSIh5yP18JQNmrpcyr+NoL0QSYJDk15IUtuorRH7y6aS4w/BzHRDBw2GnzoLUTogkwJCjqSC1ESKFoIqhmSAjL0QSoNJkxHu2jc1dtBUiTqSFaLWfxXcA6NZ6FjlkljXKZY9ICjG/DtxBdcCIEeFw1YCIwV96BsPQQyTCCMJEgq2ZHRIftJshYkTGEfmJp7ogFttCAtSGDFyCVVFxx0g4onRBI0YuqBdJaIaBqLij0o5Ia0Eij/LuqKwj0lqQKCAp144vHr0TFEU5R5R1weZWEY5wZX/phJewrXBpqFe13emUEqIMRTkcAKoLEtVRLlRVJjQVIrxDhqIkQmJyrFD1xFPrQBGUEKLMjGExl1rUCMfgBs2xHapkVUMdmub7RLfQpATRELIjZ3hTmNeNoRWiXA+a7CdyY16CaJxQrxtDKURKyhAeEVoxhm6NKFvVSISEN2A3zgF5jYWMUDmi/AUZ8QOUlCG8hQ2CmV0VpoN2QuOIsjyB/aIkQsJzZEb1ED/52zsgJITCEa0aIZYnCMJnmLGOzXndLgiYwIVIIiQCJwRiDFSIJEIiNAQsxsCEaCVmcE1IECHBzC0NKoETiBBVzo4eefY5ePJXh+GFF1+GM+fOQ8e0Npg3dxYsvu5aWH7TEiBUJrhsqu9CVLVY/+T/HIYt3/i2FGEl2qdNheVv6ILb3vl2WJ1aAYSSBFL091WIqoqwt+9+eOD7P67pv5k/ZxZ0f+j9cNu73g6qcuLky/ID6PjJl+CEiABsNIgCfBejb0K0BnpblBtj6r7nS7DnwUehXlCQe75+n7ifDSqAwnv04BPwyMHHpRCdsPwNS2DZTTfK+8gIk7N+GBla5VejuH9CPPn0DtWmKLZ841vQJ8LRRmkXDoLu+MG/+QsIK/hhg7dqobcT7PB89coV8ob/dmURGX02b+F68AFfhJifJ+wBhcCkzJ++9yPgJijGTR96H4QJFF+f+MBx6n61snrlzVKQyoboDHrYnIW94DGeC1FOSRuxHaAYy9e815OLMyxidJJ8chMM0TF07f7w+5QJ08fwocboqRCt5AxTrn8UXQLXhl4RpBhPiMRLj0g+7RfrwKBAQaJDquOSoqxxiS/1MnnjmRBVTc4gt3/kk8Ipfg1e0tP9Ud/XjNtF5hfD0LPnLkAYUCyzLDKpw0u9St54J8QXn9mi4paH6BjL1/jjVn133+XLReh3GFor6giSbWVzr98EHuCJEOXmvyZX8kw7r8PSQjCjuPv++2Dxa68FLzh77rwUYK010KDAxE5P90dCvobkm7w4r9F1Iaq6LrTZcNdmX9dPXokRP1B6tnwtNGFoLYQxuzyON+tF94X44tMDoPA2F15lS6uBYuz77F2utMWFPQx1SsgbIdJs7sJV4CKuClHFemEhGMotfltwRXe76F9PETwqAiwk3I0Q7oaorgkx30c6AAqDToiOGCR24mL5TTdO6gb4wbH73x+FR9NPREqAEwltqOri2JR7QlQ8JLW58o//BMIC1ttw7YgN1h1tlkueOX8ejv72OTj67HNw5NhzoAs40dK3+S4IGa6FqK4IUfWQtJAwCZEoJpxidCdEbXgXt3yWNDJHpGFoSISTPSIM7+71p7TkHGMzH5AH5jb2U6BxNtMWiIRfoBi3uDAR4x7i2m+GhnupGxKibOiO2AExHW1TgQg32Ka3Px1cr2wZUvz40ylogMYc0YgpceRVLSg9P6cR3f/0JZk1Dg0x2CH7q+ukbiHKNrYInk9BQlQDFGH3PfdBiEhCU1PduZK6hGgdmRbNM+xJiOqw/+DjspEhNLDYxnpdsV5HxJA0CQQRMKFL3NTpijULMd9Bsw4iCpUv1AI7isLnirWXM+pxxMglaAi1CXK3gVLQFWvXSE1CjLobIleSIyoH1hZDlUFlsK5WV6zVEckNidCBIsRd90JFja7oWIg6uCHS3kZZUxUJV3gKeVd0nkGtxRG1cEMqX6jJ0bA5IlJDBjXu5EX5xu61uPVx1FFViPNnzoTF1yShfeoU+fWJV07Bk785ArqA53OEDquuuNXJzm+OhAi5XAqMmBaN3R2KCBEF98E//zNY/vrFeQGW9sieeOUVKca+7/5ACjPK+L29iTPG6oo9k73S0TxiVIZ+nRCGKf1qoPA2vfuv5X0t9H13N2z53g8gyjy57zshrAOzQTb3+umTvWrSNaKcsNCoiyasBX10wM0bPgC7772nZhEi3e+5HZ7cfr8IYa8Awk94p5PJjMmTNbHYHUAECopn/7Yvw4Y174RGwHXk/q98WYS07wLCR2KTJzqrCjFfskiBZoTJFXH9t0e44PxZM8ENcC3Z86H1MrwlfCM1WSljMkekAn6AoBPudlGEhWCo2nfnxyFKtId5qHuSUkZ1IWrohkgYpvRRhOiE5bKhbnHb21bBI9vuGyt5qE6oS0+ilFHt2xWFyI//JgWajjqF4Q3d45ETTmTxNQuk66ouRq/OD3GP6kmbyo4Yj2ubpAlaiJvefbsvIrRBMWISR+WMqhKHnxpwR+VvVUJ20qgLTm/j1nv2RkO1dOcHKUQUQ/d7/E+kYEZ1z+fvUVaMy266EUIPY2srJW3KCpG/cHSt6lsk4hFf3R9+P1wpPinxVCQ80wKFGaoh0jJgSBoUKotx8XVhD00Roal4S1e575R3RIOtgQiAZQg8/BI7LvDsBJxbu/1vPwUb7uqRB5JWIqjsGyZP/AxJy6GiGPF9Xn7TElCCCuFpeSEqHpaWAw8ysVugMGzFU4G3f/8nZV8bVGjaLdaGYUA1MSojQoSV11aJEK1saTR37pYi/M7XRdhqnUPY0/c16O27H8JAGNywEJXEiGdiqEP57GmpI7JY5NywEHS7B77UO3bMFx5rPfE8hSC2ywiLGxaighiVCkttGC/RWKkQDWMlaEDhmXtBH26CTdxhcsNC7P5UbLULI/g+KkeZHEyREGVvKfAu0AR8EzGZg6AYgwpTMSwNM9jd88i2L4fu74nvnf3+KUZyYhmj2BFxAFgz+u6+ayz1jWHqngcf9bU4jGFf2IVog72pYWkWt09WVpZEoig8LRZiLKZ8WIqDvWfPXRh7jLt7Ye2w2gT3A/f1jk1c9Gy539dp7+WvvwFUwm4WD3rdWPieKQkr1lrRhH5UJ/FRjNuF29nFfCzyY2/iMjwa+3XXSAfE72GNEWmfNnVMzF6z+95e5cSI4DYct33mbt+34MD3pmfTR1UNSQvJsLkLF9hfjAkxP3s4ABEHQ09seyt0Pfxk3fDuvxRi7Pd1Wz5cex35/rdAZR7Y9yBs/7cHfREkvk/ohGp00TihOcnmXvM8PhoXIra1MfYT0IQHvvdj6ZKFgvTTCZHVy/4YHvjHvwfVQXfc/Z9p2PNfP/VMkKtTK6Dvs38n36PIYObWs/k37MSH40I8cXSrSKtuBI1AEaI7oks6BUsNmFxZfkNxyQF3SzsyMADb9zl3B1xrqZKoccLZCxdg/y9+KQR5wLWtHLFGuGnD+9SrFTrB5NvY/EVyYLgwND2g6yBwuXB1IrXsnoYXYe83vimEman6OhzKxRGkKGJv5YjCPPL7gZqcEkW37A03ysb98M8ZNkRarBPlJ/G4EF985nRUW9ucgCLc8KnNcORY6Y7ROB9Yz2gSOkOlPUWjsD6sBXTLI7/PSIEef/mVou9hk32H+H0sWpCEK+fNgfarwtnc4D7jWy1KIfITT3WBETsEhOw/LWwGr1eENpWyi1FZH7pOTFTULtfp2AMrYWPVEZmRBELS0/3Rsda3RkWIYIvYk9u/XrKFIX76E2XImaAV5kUZmua33Me2NkebfmsBdmwsnjMfVr/xj8AtcAtDDEft3bajujZ0Bc5xXAi0gLEk3uUdkUUwJdUAI1lXRWiDXSl2ixjtuF0FrUzRuBr/zDsihqbRP+nJMcOj4BUoRlw3kiNWgaMSY6AFjMkhCytZo3nGtIRT57DGA0RATJ8C0OTsoDL1sTKnTI5jNLecBsJChKVw+iIQAdLeCtCaAG24NDzdgHhcm/lDR2Q1y9oRwZNIJA1dDiB1zGgOCMJXRPnQEKvEJBDj0NoweHSrJQoNGqJmQ45IEEEiNCjqiCwJxDgGNTYQfmNcjaFpBxDjkBAJv2FcZE0Zo9C0kGZd6ldEeGAdYo1IQiwiESNXJPwGs6aUrCkCm41bm4Ag/MQAopSpTeSKhK+I0JTqiCWgK05tBiIgtPsQZJ3kiJWY0mTdCP/RZRZxDE5CrMq0Fr2aj8OChssCEuJk4CQAhan+ouHynITohLZmS5Ax+nX5AjkiUREMUXFgtYVCVc/R8AOPhFgLeIF0tFrb/ZEgvUO7ZI0UIhsEojYmCpJCVveI6/m7jAPjg6KWSN019WALErk0CjCctQaLdZuncxM9P9Qy1OHsFs0J64bQvjf1E9dk97YJUEzlBRSq1k9CRyGyQbxiMkAQYUFLR+SDuFUGJWuIcICRREzHZnt+Bpu+zwBBhAEtw1IBZ6fFRxDPAEGEAW1rs+bzuFUGhaZEOGjS1BEZJms4JWuIEIBuqGFHjURoUAjRzABBBI3O42YmOuLoaAYIIkiwrU2b05/KkB3uN9iCpYPUb0oESpvO855sEDVot4BkgCCCoKWgNVBPMviHJURuHgaC8Bss4Le1gN6Yz+OfeUekWiIRAJ1TNO2kKYDzfrzLOyIJkfAZ3HpE09nDYpgUopWqisXSQMcCEn6A4Wh7i95Z0kLy5cOxuIC/+Mxp3F8RiMbBweD/Ow9EAbghFCZmMEOqa+G+BJExnXv9dHxUEBtYsSoRfUYuXQIz59MR5Ym4tR0lbiuC+8SSCAsY19x4fGDyw+JTKwVE5GlqboahoYswOjIKRiwGiUQCmpoSQiMurNnQ+cTPhJa41S1DwqsMai5PQaBuCnVq2nSrIa2tU4T4cnDxwgU4e8aahIvH49ZNCDMej8nHTsRpchOMFuF6rXFr7UficwaDtP1wXIiUsNGOmHCuae3tMGXqVCnI4eFhyGazIB4Uvc4QCRZ87URyIrxtnt4OU2fOoO1B6mGEjYWmRR9d/OTTA3Q6lAsomqxBYaEgR0ZGwDQn34mupWMaTLtqFhB1kWFzFy6wvyjOIZv8oAgrkkBoie2QyPDwEAxdHLIcsgItnW1A1AkfD0uR4niCm2kgCEFLSytMnzFD3lpaWsAwSkNPg2qB9cPhYOGXxb/d0dG9QBAFYMIGXfKyyy+Hjo4OKUpmJ2NilNyrm9FiRyxJb9E60QU0KOiPinVkYu5l1CtaF6xfFPKXFj5TGm/k+D4giElINNFpynVjmgcnPlUqRM4oPCUILymjsbJxBfWdNoguvaaXT6PQtGbG+0sLKV+F5SaFpwThBZyXjTjLC9FkO4EgCA8on4MpL8TscD9tKEUQbiPC0nmLnDui3NmNwlOCcJcKYSlSuVOXwlOCcBcTdlX6VtWUF2VP64SypkQpRU3eE6k+u8Jz24AgKuHXlH8UmNDkPZHqQhwZ2QoEUYkcDbA6ZgR6q327qhCt7firK5nQmNzkM4uEJM0WLMxUe8HkY9W56komNObiiPikz2I2sPR7Bk3sj1ElSWPjaKVNSZsKoCNkc1aIho/xXn6tmVPMuBxg9jxrLCpmzShmH34YzEOH5GPz1QolaZHoMebNHPuSTZ0ibq0A4samtFpf4/3My0BhqiZpbJxNdmLShhmbQUdQVPiJP1ogOrzH50xaI0k6ZuDWcEVPjT70H5A75N6RKoYQI7tihhQqu+IyMMSNzZwh743kfAgtprOI0pkQMWnT3LpRK1ds7QBITBVXQNz6pMcLbeBZEY5Rw1EJra0lT7kpQsR85VUAvFUAxWigMK8W9wvmQ0zcB++kbBBGedrJKx0JEZM2/IUjkXNFfuEi8FN/ADNzQjweglzmuBDakPj6BYjf+g5o2rC++D9o0v3kojLgXOIEN8wd8n+vanwP8Qa//PXYcxjaxha/BmKLXiuEOk88fi34Cud7J0vS2DiuxvKBQ53Q3HIaFKNQbDkpuItSaPgcPq4Ea2uDqY9M6PI7M2i5IjEOrg+vuqboqdHdP4JLX/kahJHY4usg/sYl8t7zkPYSLHAqRMe7/1iueHQXMHYHhIxCsWEIY5561ZHYqv7M8+dleBVbumT8ybZpQEwA14cTyP78cQgruSPH5A2R60shyETqTe67JYedTkWI1LYNl8F6xP8gMCFabnZCigzFxvOCM6usHRoh+/PHioWIa8XWKQBD9Yk7krSVbqloHnsOVEB+YKdfhWz6F1YY+8Yb3RPlSG1lv5obBYUr7vTaFbkQVu55EUa+8ge5bmvU3eoFw9MpP/xXeT/GC/8LcOolIARt7QCvub7oKVwfDn3ik6AytlM233ZrfQkfdMN5C9fX8p/UvjGldEW2xo0MalgEVwkZnv7qMMTfevP4kx3TSYg2uD6cQPahR0F1Cp0yJkPXZRAXN8eM1N4EU7MQ2ZyFmVozqPYaDmNza/1mhZdhEVw1Rvf8qFiIuE7EEJUansuumd0uWwSNvaY0dj8ETbffOrkga1wb2tQ1w2JlUFsHyrmidLmjx6wMpcdrOL+YsuNfwLju2vEnKDy1IoMF1xU9hUma4c/cDVEGw9b4qjdBYuWycmFrRmRKV9UjxLr2TMcMau7wz7cJkW0uDCtlHSeCjD78CDRf97HxJyg8FWHpFSVPZR/aD1EHI7oR4Y7ZA/8NiXelIHHrLQXf5dvYgkUZqIO6pzpPp1KdsRF2iGlwaE3ZpM1vn9I3e4pF/EVdRU/xl16GC3/1HtANdMh8yOqop7Tiz4E6mZ5ODzKD15QZUhVM2mCRuogyjqANs0sL4SPbJx0wiCTokMNf/bbjntJKNDSrMu2xdFr4RRo0YHT3j6Ugx8CMoY6HsKAbTkjSoBuOPhz9sLQSIqzcyeYv3AkN0PDQWDaXQ1eMfCd0iSuiCK+YDdqBkUDJpMUjoCtcXPujObPhmd2GhTj9F+mMCXoMD5e4IgpRJ1dEN8S5wwLQDUe++S3QFcZgG2oAGsSVMeqOx3+6VYcQtawrTrgwIw2tDYvgwDPTHvtpD7iAa/sZMJ7bBBogXVG4wBjoim3tEHlwTTyhk0b3tWEux1eBS7gmxLYn0v0iRI28GNEVL237avGT866CSFMmJEWGPt4N2sKg142Q1MbVHX50CVGxg6SolQsnMqIsRpw3nJCgGfnmLjBf0rOpwc2Q1Mb1rbZ0yaJe+twXSxM3HdMhcqATTgi9sz97XNsEDWZJ3QxJbVwXoi5ZVHSDkotROkeEjrRGEZbLkoZ0+t4PhGBcDUkLfq77yBBVpHUh4mAGtShExSzqaxZGQ4zlRHj+glwX6hqS4jXdJpdf7uPZLrDZuNnDOc9AxMFpg6IsKq6lVBYjfpigs1dIzui8LsRrGjzCMyFiL2rOlLF0pNeLuE4c+sxni5+0xYhJHJXAteDrbig78Hvp3i+A+bvfgY7Y60K8psEjPN0XHWNpzs3IlzRwj5bhe79Y/CSKES9qFdrg0AUx64vbXkzIjspw9BPdMBqByfu6EdewF+vCQjzvz/r88Uz/P1y9AMetUhBh7A2TYkuLx4OgvcO6uIcuhG+qHwU4ay5A8tqyTQkYcg91fxrMo0+Dtoh6YfvjBzw/Fc23UybP3rxqJ4PwbcXoNk0feL+4lflnoghxmPgPp3DndAgUFB1OUFwxa+ysiomYvxMu/+m79U3MgFwX7hIiXAc+UNeEfj3kEvzO+CgsEdrvgghjlzRKxGj3peL6CzcqPnXSX0Ha4pO36i15mA3Gf0dRnVQzMDmD1yz4hK/nLp9elkrGDHZAh6n+is5YyPlzlkPipL+b0/4YCmOiCAWH51Lg49jkn7kYig5/7guR2wCqVqQIMTnj8bqwEN8PQNdJjPG33AzNGz8GbPasyV+cywoxDlmCHBm2QtlyjllYFkHBxQoOySlzDoUTMCEzuvuHpWNeGhKECBHfhYicX5Hq4sw4IB5G/nQpY/ZsaP3nPmdiDADc8AnDUJ3XgjayTMHNVdOfSPt+ik4gQkTyYjwEmuAoVPUJcsDyZLm5NAgRIoEJETm7IrWOMWMHaAK6IwoyfutqCALcDj/7sycg+/B+EuAEGDfXtz2R3gkBEagQEd3EiNiCjL31zcDapoJXoPOZx46R+CYhaBHKvwOEAB3FaBN/x2qIv3UFxJYubViU/ORLkOvvh9yzv5ftaNhkQOKrThhEKP8eEBJ0FqMNbuuPbinv58ySR57ZmxpLQeVFxc9dkF+bJ1+Wz0nRnXwZiNoIiwiR0AgR0SmbSgRHkNnRSnja9F0ruO+N+JRapcP4FBEMsk4YMhEioXJEG52K/oR/BFWsd0KoHNEGf1E4y0jOSLgH7w+rCJFQChGRYmziS7EDHgiiAfAayibCK0IklKHpRM69+ZYe8dt0fEIxQYzBoNftrQ+9QAkhImdX3CLKG7AFKKNKOAAzowY3N4WlPDEZyggRoSQO4YQwJ2UqEdo1YjnsdaMOWzUSdSKujVyCL1VJhIhSjljImZtvuVN8iuC6kUJVwgpFAXq92nfUa5QVIkKhKmHB09kcX6+aCxaitBBtKKuqL+IC3qSqCxYSCSEi5I66IVyQ801ha1Wrl8gI0SbvjhuB1o6RRPW1YCUiJ0REumOM9eiwj6peqL8WrEQkhWiDTQDizdtM4araYF2QMb5+2mPpNESUSAvRBksdjPONJEi1wDCUibpgNm5u9fIAmDCghRARClfVQScB2mgjRBsSZLgRF+TO0ZzZG8V1YDW0E6INCTJc6CpAG22FaGMLUvwq1jAqefiKjiFoJbQXoo3VEGCkKMvqPSTAUkiIZbBmHzFk5SkgXISngfHeKJch6oWEWIWxsJXDSnLJ+iD3cwYJ0SHn3pxKcc7W0VpycvJtaHs5M3eR+zmDhFgHZ9+SWgsmW0uiHKdQfNk49JP71QYJsUHQKcE01oq1z8qoH0s+Ec4hIy6gfWCYe8n5GoOE6CJjmVcmkjwRXFei8AwGaZPDwVyTuZdczz1IiB4ihZmALpYzhDjZEg68S5VQViZZgPcDZ4fF4/6EaaZbNS22+wEJ0WfwoB0zBkmeM7rEmmqJcM8kB5YMSqB5wYkQk/XnODwv/h4ZEp3/kBBDwulUqjOehS7OoJNnIQnM6IwxuFqEg53CTYVIzU6RtZViFeWAZKWfg8ISf4yFjCI8zoiYEksIgyg04OYgi4sQMweZ0SbIUHgZDv4fuQgYAEqCNtEAAAAASUVORK5CYII=";
        }

        foreach ($chats as $reg){
            //Utils::dumpx($reg->id_publicacion);
            $foto_principal2 = CliUsuario::find()->where('id_usuario=' . $reg->id_creador)->orderBy('id asc')->one();
            if (isset($foto_principal2->imagen) && $foto_principal2->imagen!='') {
                $foto2 =  $foto_principal2->imagen;
            } else {
                $foto2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOIAAADiCAYAAABTEBvXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAByhSURBVHgB7Z0LcFxXece/c3dXD1uWZCfxO8k6IRDbIZZJKbYDeB2muARau9MmLQwQGwxDeYxjQVpoh1hKhzBAkG2mEKbE2AbKw+Zhd9IkTlu8hjwoM8UyxE6ICdrUTpzEDZafkqXde3q+c/dKu9qH7u7e17nn+82sd7XaKLb2/vf/ne9xDgMiFPCBQ52QSCTBNJIQhyRw3gmGcTUw1ikfg3hu7MXiOZDPlYdBJv+6QWBc3MQ9F/em+bx8nBXfZzlxn+1nC5YOAhE4DAhfEYJLCsGlhCCSUmicdYEUWRVheQpDIWbAELdc7jDkWD8YZobNv6EfCN8gIXrImOiM+BLhTF2W6IISXK1IN+0Xf+d+yJoHUaBswcIMEJ5AQnSRfHi5FmKxleLLFPCCcDIaZMS/KQ08dxBGY2kSpnuQEBuEH/9NCmKJNcI91kZQeJPAMHxNQ250H7vy9Wkg6oaEWCPS9eLxLnG7Q4Rta9UJNb1GJoT2Qo7vY1ct2gtETZAQHSKdj8TnkLwozewuckpnkBCrIN2vpWWjEN+dJL66sdaVI9BLa8rKkBDLkHe/zeICSgHhHgzXk7ldojSyE4giSIh5yP18JQNmrpcyr+NoL0QSYJDk15IUtuorRH7y6aS4w/BzHRDBw2GnzoLUTogkwJCjqSC1ESKFoIqhmSAjL0QSoNJkxHu2jc1dtBUiTqSFaLWfxXcA6NZ6FjlkljXKZY9ICjG/DtxBdcCIEeFw1YCIwV96BsPQQyTCCMJEgq2ZHRIftJshYkTGEfmJp7ogFttCAtSGDFyCVVFxx0g4onRBI0YuqBdJaIaBqLij0o5Ia0Eij/LuqKwj0lqQKCAp144vHr0TFEU5R5R1weZWEY5wZX/phJewrXBpqFe13emUEqIMRTkcAKoLEtVRLlRVJjQVIrxDhqIkQmJyrFD1xFPrQBGUEKLMjGExl1rUCMfgBs2xHapkVUMdmub7RLfQpATRELIjZ3hTmNeNoRWiXA+a7CdyY16CaJxQrxtDKURKyhAeEVoxhm6NKFvVSISEN2A3zgF5jYWMUDmi/AUZ8QOUlCG8hQ2CmV0VpoN2QuOIsjyB/aIkQsJzZEb1ED/52zsgJITCEa0aIZYnCMJnmLGOzXndLgiYwIVIIiQCJwRiDFSIJEIiNAQsxsCEaCVmcE1IECHBzC0NKoETiBBVzo4eefY5ePJXh+GFF1+GM+fOQ8e0Npg3dxYsvu5aWH7TEiBUJrhsqu9CVLVY/+T/HIYt3/i2FGEl2qdNheVv6ILb3vl2WJ1aAYSSBFL091WIqoqwt+9+eOD7P67pv5k/ZxZ0f+j9cNu73g6qcuLky/ID6PjJl+CEiABsNIgCfBejb0K0BnpblBtj6r7nS7DnwUehXlCQe75+n7ifDSqAwnv04BPwyMHHpRCdsPwNS2DZTTfK+8gIk7N+GBla5VejuH9CPPn0DtWmKLZ841vQJ8LRRmkXDoLu+MG/+QsIK/hhg7dqobcT7PB89coV8ob/dmURGX02b+F68AFfhJifJ+wBhcCkzJ++9yPgJijGTR96H4QJFF+f+MBx6n61snrlzVKQyoboDHrYnIW94DGeC1FOSRuxHaAYy9e815OLMyxidJJ8chMM0TF07f7w+5QJ08fwocboqRCt5AxTrn8UXQLXhl4RpBhPiMRLj0g+7RfrwKBAQaJDquOSoqxxiS/1MnnjmRBVTc4gt3/kk8Ipfg1e0tP9Ud/XjNtF5hfD0LPnLkAYUCyzLDKpw0u9St54J8QXn9mi4paH6BjL1/jjVn133+XLReh3GFor6giSbWVzr98EHuCJEOXmvyZX8kw7r8PSQjCjuPv++2Dxa68FLzh77rwUYK010KDAxE5P90dCvobkm7w4r9F1Iaq6LrTZcNdmX9dPXokRP1B6tnwtNGFoLYQxuzyON+tF94X44tMDoPA2F15lS6uBYuz77F2utMWFPQx1SsgbIdJs7sJV4CKuClHFemEhGMotfltwRXe76F9PETwqAiwk3I0Q7oaorgkx30c6AAqDToiOGCR24mL5TTdO6gb4wbH73x+FR9NPREqAEwltqOri2JR7QlQ8JLW58o//BMIC1ttw7YgN1h1tlkueOX8ejv72OTj67HNw5NhzoAs40dK3+S4IGa6FqK4IUfWQtJAwCZEoJpxidCdEbXgXt3yWNDJHpGFoSISTPSIM7+71p7TkHGMzH5AH5jb2U6BxNtMWiIRfoBi3uDAR4x7i2m+GhnupGxKibOiO2AExHW1TgQg32Ka3Px1cr2wZUvz40ylogMYc0YgpceRVLSg9P6cR3f/0JZk1Dg0x2CH7q+ukbiHKNrYInk9BQlQDFGH3PfdBiEhCU1PduZK6hGgdmRbNM+xJiOqw/+DjspEhNLDYxnpdsV5HxJA0CQQRMKFL3NTpijULMd9Bsw4iCpUv1AI7isLnirWXM+pxxMglaAi1CXK3gVLQFWvXSE1CjLobIleSIyoH1hZDlUFlsK5WV6zVEckNidCBIsRd90JFja7oWIg6uCHS3kZZUxUJV3gKeVd0nkGtxRG1cEMqX6jJ0bA5IlJDBjXu5EX5xu61uPVx1FFViPNnzoTF1yShfeoU+fWJV07Bk785ArqA53OEDquuuNXJzm+OhAi5XAqMmBaN3R2KCBEF98E//zNY/vrFeQGW9sieeOUVKca+7/5ACjPK+L29iTPG6oo9k73S0TxiVIZ+nRCGKf1qoPA2vfuv5X0t9H13N2z53g8gyjy57zshrAOzQTb3+umTvWrSNaKcsNCoiyasBX10wM0bPgC7772nZhEi3e+5HZ7cfr8IYa8Awk94p5PJjMmTNbHYHUAECopn/7Yvw4Y174RGwHXk/q98WYS07wLCR2KTJzqrCjFfskiBZoTJFXH9t0e44PxZM8ENcC3Z86H1MrwlfCM1WSljMkekAn6AoBPudlGEhWCo2nfnxyFKtId5qHuSUkZ1IWrohkgYpvRRhOiE5bKhbnHb21bBI9vuGyt5qE6oS0+ilFHt2xWFyI//JgWajjqF4Q3d45ETTmTxNQuk66ouRq/OD3GP6kmbyo4Yj2ubpAlaiJvefbsvIrRBMWISR+WMqhKHnxpwR+VvVUJ20qgLTm/j1nv2RkO1dOcHKUQUQ/d7/E+kYEZ1z+fvUVaMy266EUIPY2srJW3KCpG/cHSt6lsk4hFf3R9+P1wpPinxVCQ80wKFGaoh0jJgSBoUKotx8XVhD00Roal4S1e575R3RIOtgQiAZQg8/BI7LvDsBJxbu/1vPwUb7uqRB5JWIqjsGyZP/AxJy6GiGPF9Xn7TElCCCuFpeSEqHpaWAw8ysVugMGzFU4G3f/8nZV8bVGjaLdaGYUA1MSojQoSV11aJEK1saTR37pYi/M7XRdhqnUPY0/c16O27H8JAGNywEJXEiGdiqEP57GmpI7JY5NywEHS7B77UO3bMFx5rPfE8hSC2ywiLGxaighiVCkttGC/RWKkQDWMlaEDhmXtBH26CTdxhcsNC7P5UbLULI/g+KkeZHEyREGVvKfAu0AR8EzGZg6AYgwpTMSwNM9jd88i2L4fu74nvnf3+KUZyYhmj2BFxAFgz+u6+ayz1jWHqngcf9bU4jGFf2IVog72pYWkWt09WVpZEoig8LRZiLKZ8WIqDvWfPXRh7jLt7Ye2w2gT3A/f1jk1c9Gy539dp7+WvvwFUwm4WD3rdWPieKQkr1lrRhH5UJ/FRjNuF29nFfCzyY2/iMjwa+3XXSAfE72GNEWmfNnVMzF6z+95e5cSI4DYct33mbt+34MD3pmfTR1UNSQvJsLkLF9hfjAkxP3s4ABEHQ09seyt0Pfxk3fDuvxRi7Pd1Wz5cex35/rdAZR7Y9yBs/7cHfREkvk/ohGp00TihOcnmXvM8PhoXIra1MfYT0IQHvvdj6ZKFgvTTCZHVy/4YHvjHvwfVQXfc/Z9p2PNfP/VMkKtTK6Dvs38n36PIYObWs/k37MSH40I8cXSrSKtuBI1AEaI7oks6BUsNmFxZfkNxyQF3SzsyMADb9zl3B1xrqZKoccLZCxdg/y9+KQR5wLWtHLFGuGnD+9SrFTrB5NvY/EVyYLgwND2g6yBwuXB1IrXsnoYXYe83vimEman6OhzKxRGkKGJv5YjCPPL7gZqcEkW37A03ysb98M8ZNkRarBPlJ/G4EF985nRUW9ucgCLc8KnNcORY6Y7ROB9Yz2gSOkOlPUWjsD6sBXTLI7/PSIEef/mVou9hk32H+H0sWpCEK+fNgfarwtnc4D7jWy1KIfITT3WBETsEhOw/LWwGr1eENpWyi1FZH7pOTFTULtfp2AMrYWPVEZmRBELS0/3Rsda3RkWIYIvYk9u/XrKFIX76E2XImaAV5kUZmua33Me2NkebfmsBdmwsnjMfVr/xj8AtcAtDDEft3bajujZ0Bc5xXAi0gLEk3uUdkUUwJdUAI1lXRWiDXSl2ixjtuF0FrUzRuBr/zDsihqbRP+nJMcOj4BUoRlw3kiNWgaMSY6AFjMkhCytZo3nGtIRT57DGA0RATJ8C0OTsoDL1sTKnTI5jNLecBsJChKVw+iIQAdLeCtCaAG24NDzdgHhcm/lDR2Q1y9oRwZNIJA1dDiB1zGgOCMJXRPnQEKvEJBDj0NoweHSrJQoNGqJmQ45IEEEiNCjqiCwJxDgGNTYQfmNcjaFpBxDjkBAJv2FcZE0Zo9C0kGZd6ldEeGAdYo1IQiwiESNXJPwGs6aUrCkCm41bm4Ag/MQAopSpTeSKhK+I0JTqiCWgK05tBiIgtPsQZJ3kiJWY0mTdCP/RZRZxDE5CrMq0Fr2aj8OChssCEuJk4CQAhan+ouHynITohLZmS5Ax+nX5AjkiUREMUXFgtYVCVc/R8AOPhFgLeIF0tFrb/ZEgvUO7ZI0UIhsEojYmCpJCVveI6/m7jAPjg6KWSN019WALErk0CjCctQaLdZuncxM9P9Qy1OHsFs0J64bQvjf1E9dk97YJUEzlBRSq1k9CRyGyQbxiMkAQYUFLR+SDuFUGJWuIcICRREzHZnt+Bpu+zwBBhAEtw1IBZ6fFRxDPAEGEAW1rs+bzuFUGhaZEOGjS1BEZJms4JWuIEIBuqGFHjURoUAjRzABBBI3O42YmOuLoaAYIIkiwrU2b05/KkB3uN9iCpYPUb0oESpvO855sEDVot4BkgCCCoKWgNVBPMviHJURuHgaC8Bss4Le1gN6Yz+OfeUekWiIRAJ1TNO2kKYDzfrzLOyIJkfAZ3HpE09nDYpgUopWqisXSQMcCEn6A4Wh7i95Z0kLy5cOxuIC/+Mxp3F8RiMbBweD/Ow9EAbghFCZmMEOqa+G+BJExnXv9dHxUEBtYsSoRfUYuXQIz59MR5Ym4tR0lbiuC+8SSCAsY19x4fGDyw+JTKwVE5GlqboahoYswOjIKRiwGiUQCmpoSQiMurNnQ+cTPhJa41S1DwqsMai5PQaBuCnVq2nSrIa2tU4T4cnDxwgU4e8aahIvH49ZNCDMej8nHTsRpchOMFuF6rXFr7UficwaDtP1wXIiUsNGOmHCuae3tMGXqVCnI4eFhyGazIB4Uvc4QCRZ87URyIrxtnt4OU2fOoO1B6mGEjYWmRR9d/OTTA3Q6lAsomqxBYaEgR0ZGwDQn34mupWMaTLtqFhB1kWFzFy6wvyjOIZv8oAgrkkBoie2QyPDwEAxdHLIcsgItnW1A1AkfD0uR4niCm2kgCEFLSytMnzFD3lpaWsAwSkNPg2qB9cPhYOGXxb/d0dG9QBAFYMIGXfKyyy+Hjo4OKUpmJ2NilNyrm9FiRyxJb9E60QU0KOiPinVkYu5l1CtaF6xfFPKXFj5TGm/k+D4giElINNFpynVjmgcnPlUqRM4oPCUILymjsbJxBfWdNoguvaaXT6PQtGbG+0sLKV+F5SaFpwThBZyXjTjLC9FkO4EgCA8on4MpL8TscD9tKEUQbiPC0nmLnDui3NmNwlOCcJcKYSlSuVOXwlOCcBcTdlX6VtWUF2VP64SypkQpRU3eE6k+u8Jz24AgKuHXlH8UmNDkPZHqQhwZ2QoEUYkcDbA6ZgR6q327qhCt7firK5nQmNzkM4uEJM0WLMxUe8HkY9W56komNObiiPikz2I2sPR7Bk3sj1ElSWPjaKVNSZsKoCNkc1aIho/xXn6tmVPMuBxg9jxrLCpmzShmH34YzEOH5GPz1QolaZHoMebNHPuSTZ0ibq0A4samtFpf4/3My0BhqiZpbJxNdmLShhmbQUdQVPiJP1ogOrzH50xaI0k6ZuDWcEVPjT70H5A75N6RKoYQI7tihhQqu+IyMMSNzZwh743kfAgtprOI0pkQMWnT3LpRK1ds7QBITBVXQNz6pMcLbeBZEY5Rw1EJra0lT7kpQsR85VUAvFUAxWigMK8W9wvmQ0zcB++kbBBGedrJKx0JEZM2/IUjkXNFfuEi8FN/ADNzQjweglzmuBDakPj6BYjf+g5o2rC++D9o0v3kojLgXOIEN8wd8n+vanwP8Qa//PXYcxjaxha/BmKLXiuEOk88fi34Cud7J0vS2DiuxvKBQ53Q3HIaFKNQbDkpuItSaPgcPq4Ea2uDqY9M6PI7M2i5IjEOrg+vuqboqdHdP4JLX/kahJHY4usg/sYl8t7zkPYSLHAqRMe7/1iueHQXMHYHhIxCsWEIY5561ZHYqv7M8+dleBVbumT8ybZpQEwA14cTyP78cQgruSPH5A2R60shyETqTe67JYedTkWI1LYNl8F6xP8gMCFabnZCigzFxvOCM6usHRoh+/PHioWIa8XWKQBD9Yk7krSVbqloHnsOVEB+YKdfhWz6F1YY+8Yb3RPlSG1lv5obBYUr7vTaFbkQVu55EUa+8ge5bmvU3eoFw9MpP/xXeT/GC/8LcOolIARt7QCvub7oKVwfDn3ik6AytlM233ZrfQkfdMN5C9fX8p/UvjGldEW2xo0MalgEVwkZnv7qMMTfevP4kx3TSYg2uD6cQPahR0F1Cp0yJkPXZRAXN8eM1N4EU7MQ2ZyFmVozqPYaDmNza/1mhZdhEVw1Rvf8qFiIuE7EEJUansuumd0uWwSNvaY0dj8ETbffOrkga1wb2tQ1w2JlUFsHyrmidLmjx6wMpcdrOL+YsuNfwLju2vEnKDy1IoMF1xU9hUma4c/cDVEGw9b4qjdBYuWycmFrRmRKV9UjxLr2TMcMau7wz7cJkW0uDCtlHSeCjD78CDRf97HxJyg8FWHpFSVPZR/aD1EHI7oR4Y7ZA/8NiXelIHHrLQXf5dvYgkUZqIO6pzpPp1KdsRF2iGlwaE3ZpM1vn9I3e4pF/EVdRU/xl16GC3/1HtANdMh8yOqop7Tiz4E6mZ5ODzKD15QZUhVM2mCRuogyjqANs0sL4SPbJx0wiCTokMNf/bbjntJKNDSrMu2xdFr4RRo0YHT3j6Ugx8CMoY6HsKAbTkjSoBuOPhz9sLQSIqzcyeYv3AkN0PDQWDaXQ1eMfCd0iSuiCK+YDdqBkUDJpMUjoCtcXPujObPhmd2GhTj9F+mMCXoMD5e4IgpRJ1dEN8S5wwLQDUe++S3QFcZgG2oAGsSVMeqOx3+6VYcQtawrTrgwIw2tDYvgwDPTHvtpD7iAa/sZMJ7bBBogXVG4wBjoim3tEHlwTTyhk0b3tWEux1eBS7gmxLYn0v0iRI28GNEVL237avGT866CSFMmJEWGPt4N2sKg142Q1MbVHX50CVGxg6SolQsnMqIsRpw3nJCgGfnmLjBf0rOpwc2Q1Mb1rbZ0yaJe+twXSxM3HdMhcqATTgi9sz97XNsEDWZJ3QxJbVwXoi5ZVHSDkotROkeEjrRGEZbLkoZ0+t4PhGBcDUkLfq77yBBVpHUh4mAGtShExSzqaxZGQ4zlRHj+glwX6hqS4jXdJpdf7uPZLrDZuNnDOc9AxMFpg6IsKq6lVBYjfpigs1dIzui8LsRrGjzCMyFiL2rOlLF0pNeLuE4c+sxni5+0xYhJHJXAteDrbig78Hvp3i+A+bvfgY7Y60K8psEjPN0XHWNpzs3IlzRwj5bhe79Y/CSKES9qFdrg0AUx64vbXkzIjspw9BPdMBqByfu6EdewF+vCQjzvz/r88Uz/P1y9AMetUhBh7A2TYkuLx4OgvcO6uIcuhG+qHwU4ay5A8tqyTQkYcg91fxrMo0+Dtoh6YfvjBzw/Fc23UybP3rxqJ4PwbcXoNk0feL+4lflnoghxmPgPp3DndAgUFB1OUFwxa+ysiomYvxMu/+m79U3MgFwX7hIiXAc+UNeEfj3kEvzO+CgsEdrvgghjlzRKxGj3peL6CzcqPnXSX0Ha4pO36i15mA3Gf0dRnVQzMDmD1yz4hK/nLp9elkrGDHZAh6n+is5YyPlzlkPipL+b0/4YCmOiCAWH51Lg49jkn7kYig5/7guR2wCqVqQIMTnj8bqwEN8PQNdJjPG33AzNGz8GbPasyV+cywoxDlmCHBm2QtlyjllYFkHBxQoOySlzDoUTMCEzuvuHpWNeGhKECBHfhYicX5Hq4sw4IB5G/nQpY/ZsaP3nPmdiDADc8AnDUJ3XgjayTMHNVdOfSPt+ik4gQkTyYjwEmuAoVPUJcsDyZLm5NAgRIoEJETm7IrWOMWMHaAK6IwoyfutqCALcDj/7sycg+/B+EuAEGDfXtz2R3gkBEagQEd3EiNiCjL31zcDapoJXoPOZx46R+CYhaBHKvwOEAB3FaBN/x2qIv3UFxJYubViU/ORLkOvvh9yzv5ftaNhkQOKrThhEKP8eEBJ0FqMNbuuPbinv58ySR57ZmxpLQeVFxc9dkF+bJ1+Wz0nRnXwZiNoIiwiR0AgR0SmbSgRHkNnRSnja9F0ruO+N+JRapcP4FBEMsk4YMhEioXJEG52K/oR/BFWsd0KoHNEGf1E4y0jOSLgH7w+rCJFQChGRYmziS7EDHgiiAfAayibCK0IklKHpRM69+ZYe8dt0fEIxQYzBoNftrQ+9QAkhImdX3CLKG7AFKKNKOAAzowY3N4WlPDEZyggRoSQO4YQwJ2UqEdo1YjnsdaMOWzUSdSKujVyCL1VJhIhSjljImZtvuVN8iuC6kUJVwgpFAXq92nfUa5QVIkKhKmHB09kcX6+aCxaitBBtKKuqL+IC3qSqCxYSCSEi5I66IVyQ801ha1Wrl8gI0SbvjhuB1o6RRPW1YCUiJ0REumOM9eiwj6peqL8WrEQkhWiDTQDizdtM4araYF2QMb5+2mPpNESUSAvRBksdjPONJEi1wDCUibpgNm5u9fIAmDCghRARClfVQScB2mgjRBsSZLgRF+TO0ZzZG8V1YDW0E6INCTJc6CpAG22FaGMLUvwq1jAqefiKjiFoJbQXoo3VEGCkKMvqPSTAUkiIZbBmHzFk5SkgXISngfHeKJch6oWEWIWxsJXDSnLJ+iD3cwYJ0SHn3pxKcc7W0VpycvJtaHs5M3eR+zmDhFgHZ9+SWgsmW0uiHKdQfNk49JP71QYJsUHQKcE01oq1z8qoH0s+Ec4hIy6gfWCYe8n5GoOE6CJjmVcmkjwRXFei8AwGaZPDwVyTuZdczz1IiB4ihZmALpYzhDjZEg68S5VQViZZgPcDZ4fF4/6EaaZbNS22+wEJ0WfwoB0zBkmeM7rEmmqJcM8kB5YMSqB5wYkQk/XnODwv/h4ZEp3/kBBDwulUqjOehS7OoJNnIQnM6IwxuFqEg53CTYVIzU6RtZViFeWAZKWfg8ISf4yFjCI8zoiYEksIgyg04OYgi4sQMweZ0SbIUHgZDv4fuQgYAEqCNtEAAAAASUVORK5CYII=";
            }

            if($reg->id_creador==Yii::$app->user->identity->id){
                $chat .= '<!-- DERECHA YO - USER LOGUEADO -->
                          <div class="cols">
                              <div class="col1 vineta">
                                  <p class=" globo ii derecha-arriba">
                                      '.$reg->mensaje.'
                                  </p>
                              </div>
                              <div class="col2 ">
                                  <p>
                                      <span class="circle-image">
                                           <img class="rounded profile-user-img img-responsive img-responsive" src="'.$foto.'" style="">
                                           <p class="txtnegro" style="display: flex;justify-content: center">'.date('Y-m-d', strtotime($reg->fecha)).'<br>('.date('h:i a', strtotime($reg->fecha)).')'.'</p>
                                      </span>
                                  </p>
                              </div>
                          </div>';

            }else{
                $chat .= ' <!-- IZQUIERDA MENSAJE COMPRADOR (NO MIO) -->
                          <div class="cols">
                              <div class="col22">
                                  <p  >
                                    <span class="circle-image">
                                           <img class="rounded profile-user-img img-responsive img-responsive" src="'.$foto2.'" style="">
                                  <p class="txtnegro2"  style="display: flex; ">'.date('Y-m-d', strtotime($reg->fecha)).'<br>('.date('h:i a', strtotime($reg->fecha)).')'.'</p>
                                  </span>
                                  </p>
                              </div>
                              <div class="col11 vineta">
                                  <p  class=" globo iii izquierda-arriba">
                                      '.$reg->mensaje.'
                                  </p>
                              </div>
    
                          </div>';
            }


        }

        echo Json::encode(['res' => $chat ]);
        exit;

    }

    public function actionAddmensaje()
    {
        extract($_POST);
        //Utils::dump($_POST);

        $model = new PubMensaje();
        $model->fecha = date('Y-m-d H:i:s');
        //mirar el último mensaje de esa publicación
        $model->id_creador = Yii::$app->user->identity->id;

        $pub = PubPublicacion::find()->where('id='.$id_publicacion)->one();
        $model->id_dueno_publicacion = $pub->id_usuario;
        $model->mensaje = $mensaje;
        $model->id_publicacion = $id_publicacion;
        //Utils::dump($model->attributes);
        if($model->save()){

            if(isset($id_papa) && $id_papa!='')     $model->id_papa = $id_papa;
            else $model->id_papa = $model->id;

            $model->save();

            //Obtengo con el id_papa el primer registro ASC, así veo el id_creador (primer mensaje es del comprador siempre)
            $primer_msg = PubMensaje::find()->where('id_papa='.$id_papa)->orderBy('id asc')->one();

            $comprador = CliUsuario::find()->where('id_usuario=' .$primer_msg->id_creador)->one();
            //Utils::dump($comprador->attributes);

            //Usuario dueño de la publicacion
            $vendedor = CliUsuario::find()->where('id_usuario=' .$pub->id_usuario)->one();

            //usuario que envia mensaje (dueno o comprador)
            //$usuario_envia_msg = CliUsuario::find()->where('id_usuario=' . Yii::$app->user->identity->id)->one();

            //Enviar email a Vendedor
            $nuevafecha = strtotime('+2 hour', time()); $nuevafecha = date('Y-m-d H:ia', $nuevafecha);

            $foto1 = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id_publicacion)->orderBy('id ASC')->one();
            if (!$foto1) {
                //No tiene imagenes
                $foto1[0] = Yii::$app->request->baseUrl . '/img/no-image-moto.png';
            }

            //Si el usuario que envia el MSG es el dueño de la publicación:
            if($pub->id_usuario==Yii::$app->user->identity->id){
                //Enviar email al COMPRADOR: Te han respondido por ()
                Yii::$app->mailer->compose('layouts/pregunta_a_vendedor',
                    ['tipo' => 'al_comprador', 'mensaje' => $model->mensaje, 'username' => $vendedor->nombreFull, 'id_pub' => $id_publicacion, 'fecha' => $nuevafecha,
                        'comprador' => $comprador, 'vendedor' => $vendedor, 'pub' => $pub, 'foto1' => $foto1])
                    ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                    ->setTo($comprador->email)//->setCc('wokeer+admin@gmail.com')
                    ->setSubject('Comprador - Te respondieron por (' . $pub->modelo->marca->nombre . ' ' . $pub->modelo->nombre . ' ' . $pub->ano . ') en Vendetumoto.co (ID Publicación #' . $pub->id . ' ) | ' . $nuevafecha)
                    ->send();
            }else{
                //Enviar email al vendedor(dueño de la pub)
                Yii::$app->mailer->compose('layouts/pregunta_a_vendedor',
                    ['tipo' => 'al_vendedor', 'mensaje' => $model->mensaje, 'username' => $vendedor->nombreFull, 'id_pub' => $id_publicacion, 'fecha' => $nuevafecha,
                        'comprador' => $comprador, 'vendedor' => $vendedor, 'pub' => $pub, 'foto1' => $foto1])
                    ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                    ->setTo($vendedor->email)//->setCc('wokeer+admin@gmail.com')
                    ->setSubject('Vendedor - Te respondieron por (' . $pub->modelo->marca->nombre . ' ' . $pub->modelo->nombre . ' ' . $pub->ano . ') en Vendetumoto.co (ID Publicación #' . $pub->id . ' ) | ' . $nuevafecha)
                    ->send();
            }

        }

        echo Json::encode(['res' => true, 'id_papa' => $id_papa , 'id_publicacion' => $id_publicacion  ]);
        exit;
    }

    public function actionAjax_mismensajes()
    {
        $rol = Utils::getRol(Yii::$app->user->identity->id);
        $where2 = '';
        $sql = "(SELECT  m.id_creador, p.id as id_pub, m.id,m.fecha, concat('ID: ',p.id,' - ',t.nombre,' / ',ma.nombre,' / ',md.nombre) as vehiculo,
                        concat(cu.nombre,' (',cu.email,')') as creador, m.id_papa, m.mensaje,                    
                        (select  GROUP_CONCAT(concat('|',fecha,'###',mensaje,'###',id_creador) ORDER BY id DESC SEPARATOR '') from pub_mensaje where id_publicacion=p.id and id_creador=m.id_creador) as hilo,
                        concat(replace(replace(m.leido,'1','SI'),'0','NO')) AS leido, m.fecha_leido
                    FROM pub_mensaje as m, cli_usuario as cu, pub_publicacion as p, mot_tipo as t, mot_marca as ma, mot_modelo as md
                   WHERE m.id_creador=cu.id_usuario and p.id=m.id_publicacion and t.id=p.id_tipo and md.id=p.id_modelo and ma.id=md.id_marca
                   and p.id_usuario=".Yii::$app->user->identity->id." 
                   group by p.id, m.id_papa
                   ORDER BY m.id desc 
                   ) temp";
        $table = <<<EOT
        $sql
EOT;

        $primaryKey = 'id';

        $columns = array(

            array('db' => 'fecha', 'dt' => 0,
                'formatter' => function( $d, $row ) { return date('Y-m-d', strtotime($d)).'<br>('.date('h:i a', strtotime($d)).')';   }
            ),
            array('db' => 'vehiculo', 'dt' => 1,
                'formatter' => function( $d, $row ) {
                    //Poner Url de Detalle de la publicación
                    return  '<a target="_blank" title="Ir al Detalle del Anuncio" href="'.Url::to(['/site/pubdetalle','id'=>$row[5]]).'">'.$d.'</a>';
                }
            ),


            array('db' => 'hilo', 'dt' => 2,
                'formatter' => function( $d, $row ) {

                    //traiga los mensajes DESC
                /*    [id] => 12
                    [id_papa] => 7
                    [id_publicacion] => 8
                    [id_creador] => 2
                    [fecha] => 2021-11-21 21:24:17
                    [mensaje] => En Cali
                    [leido] => 0
                    [fecha_leido] => */
                    if($row[7]!='' || $row[7]!=null){
                        $pm = PubMensaje::find()->where('id_publicacion='.$row[5].' and id_papa='.$row[7])->orderBy('id desc')->all();
                        $str ='';
                        $str_msgs = '';
                        foreach ($pm as $reg){
                            //Utils::dumpx($reg->attributes);
                            $fecha = date('Y-m-d',strtotime($reg->fecha)).' ('.date('h:ia',strtotime($reg->fecha)).')';
                            $creador = CliUsuario::find()->where('id_usuario='.$reg->id_creador)->one();
                            $str_msgs .= '<span style=\'color:red;padding: 4px;font-size:10px !important;\'>'.$fecha.' | '.$reg->mensaje.' | '.$creador->nombre.'</span><br>';
                        }
                        $str .= Html::label('('.count($pm).') mensajes', '#', ['data-html'=>"1",'style' => 'color:green;text-decoration:underline', 'title' => 'Historial de mensajes : ', 'data' => ['style'=>'color:red','html'=>'true','trigger'=>"hover",'placement'=>"right",'toggle' =>'popover', 'title' =>'<span style="color:#3c8dbc">Actividades Asociadas :</span> ', 'content' => $str_msgs ],]);
                        $str .='<script> $("[data-toggle=\'popover\']").popover();</script>';
                    }else{
                        $str = '---';
                    }


                    /*
                    $mensajes = explode('|',$d);
                   // Utils::dumpx($mensajes);
                    $str ='';
                    $str_msgs = '';
                    for($i=1;$i<count($mensajes);$i++) {
                        $msg = explode('###',$mensajes[$i]);
                        $fecha = date('Y-m-d',strtotime($msg[0])).' ('.date('h:ia',strtotime($msg[0])).')';
                        $creador = CliUsuario::find()->where('id_usuario='.$msg[2])->one();
                        //Utils::dump($fecha);
                        //    Utils::dumpx($creador);
                        $str_msgs .= '<span style=\'color:red;padding: 4px;font-size:10px !important;\'>'.$fecha.' | '.$msg[1].' | '.$creador->nombre.'</span><br>';
                    }*/


                    return $str;
                }
            ),
            array('db' => 'mensaje',  'dt' => 3),
            array('db' => 'creador',  'dt' => 4),
            array('db' => 'id_pub', 'dt' => 5,
                'formatter' => function( $d, $row ) {
                    $pub = PubMensaje::find()->where('id_publicacion='.$d)->one();

                    $str = "<script> 
                                $('.btn_enviar_mensaje".$d."_".$row[6]."').click(function(e){
                                    e.preventDefault(); 
                                    var modal = $('#modal-delete').modal('show');    
                                    var that = $(this);
                                    modal.find('.modal-title').html('Enviar mensaje : ');                                
                                    modal.find('.id_pub').html(that.attr('id_pub'));
                                    modal.find('.tmm').html(that.attr('tmm'));
                                                                      
                                    $('#id_publicacion').val(that.attr('id_pub'));
                                    $('#id_creador').val(".$row[6].");
                                    $('#id_papa').val(".$row[7].");
                                    
                                    $.post('".Url::to(['site/getmensajespub'])."', {id_publicacion: that.attr('id_pub'), id_papa: ".$row[7]." }, 
                                       function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub);                                             
                                            $('#links').html('<center><b style=\"color:red\">Cargando mensajes ...</b><br><img style=\"width:50px\" title=\"Cargando mensajes ...\" src=\"".Yii::$app->request->baseUrl."/img/loading.gif\"></center>');
                                            setTimeout(function() {                                                 
                                                 if(res.res){   
                                                     if(res.total > 0){   
                                                         $('#links').html(res.res);
                                                     }else{
                                                        $('#links').html('<center><em style=\"color:red;font-size:20px\">'+res.error+'</em></center>');    
                                                     }
                                                }else{
                                                     $('#links').html('<center><em style=\"color:red;font-size:20px\">'+res.error+'</em></center>');
                                                }
                                            }, 2000);
                                       }) ; 
                                    });         
                          </script>";

                    return $str.'<div class="btn-group"> 
                                   <button title="Acción" type="button" class="btn btn-danger btn-sm btn_enviar_mensaje'.$d."_".$row[6].'" id_pub="'.$d.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Enviar Mensaje</button>
                                 </div>';


                }
            ),
            array('db' => 'id_creador', 'dt' => 6,
                'formatter' => function( $d, $row ) { return '';   }
            ),
            array('db' => 'id_papa', 'dt' => 7,
                'formatter' => function( $d, $row ) { return '';   }
            ),



        );

        $data =( \SSP::simple( $_GET, [], $table, $primaryKey, $columns, $where2 ) );
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $data;
    }

    public function actionFavoritoupdate(){
        extract($_POST);
        // Utils::dumpx($_POST);

        if(isset($id_publicacion) && $id_publicacion!='' && isset($id_usuario) && $id_usuario!='')
        {
            $exist_fav = PubFavoritos::find()->where('id_publicacion='.$id_publicacion.' and id_usuario='.$id_usuario)->count();
            if($exist_fav==0){
                $model = new PubFavoritos();
                $model->fecha = date('Y-m-d H:i:s');
                $model->id_usuario = Yii::$app->user->identity->id;
                $model->id_publicacion = $id_publicacion;
                $model->save();
                echo Json::encode(['res' => true, 'tipo' => 1, 'id_pub' => $id_publicacion]);
            }else{
                //Borramos y devolvemos : YA No es tu Favorito
                PubFavoritos::deleteAll('id_publicacion='.$id_publicacion.' and id_usuario='.$id_usuario);
                echo Json::encode(['res' => true, 'tipo' => 2, 'id_pub' => $id_publicacion]);
            }

        }else
            echo Json::encode(['res' => false]);
    }


    public function actionAjaxsavemensajepub()
    {
        extract($_POST);
        //Utils::dumpx($_POST);

        if(isset($id_publicacion) && $id_publicacion!=''){
            $model = new PubMensaje();
            $model->fecha = date('Y-m-d H:i:s');
            //mirar el último mensaje de esa publicación
            $model->id_creador = Yii::$app->user->identity->id;
            $model->id_papa = $id_papa;
            $model->mensaje = $mensaje_enviado;
            $model->id_publicacion = $id_publicacion;
            if($model->save())
                echo Json::encode(['res' => true]);
            else Utils::dumpx($model->getErrors());
        }else
            echo Json::encode(['res' => false]);
    }

    public function actionGetmensajespub()
    {
        extract($_POST);

        if(isset($id_publicacion) && $id_publicacion!=''){
            $model = PubMensaje::find()->where('id_publicacion=' . $id_publicacion.' and id_papa='.$id_papa)->orderBy('id DESC')->all();

            //Utils::dump($model->attributes);
            $str_msgs    = '<table class=\'table\'><thead style="background: #de5342;color: #fff"><tr><th style="text-align: center;">Fecha</th> <th>Mensaje</th> <th>Enviado por</th></tr></thead>';


            foreach ($model as $reg){
                $fecha = date('Y-m-d',strtotime($reg->fecha)).'<br>('.date('h:ia',strtotime($reg->fecha)).')';
                $creador = CliUsuario::find()->where('id_usuario='.$reg->id_creador)->one();
                $str_msgs .= '<tr style="font-size: 11px">
                                    <td style=\'padding: 4px;text-align: center;width: 100px\'>'.$fecha.'</td>
                                    <td>'.$reg->mensaje.'</td>
                                    <td>'.$creador->nombre.'</td>
                              </tr>';
            }
            $str_msgs .= '</table>';

                if(count($model)>0)
                    echo Json::encode(['res' => $str_msgs, 'id_pub' => $id_publicacion, 'total' => count($model) ]);
                else{
                    echo Json::encode(['res' => false, 'error' => 'Información: La publicación No tiene mensajes por el momento.', 'total' => 0]);
                }
        }else
            echo Json::encode(['res' => false, 'error' => 'Error: ¡Parámetros inválidos falta id publicacion!', 'total' => 0]);

    }

    public function actionMiperfil()
    {
        extract($_GET); $this->layout = 'main-logueado2';
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }

        $role = Utils::getRol(Yii::$app->user->identity->id);
        $aux = '';
        $model = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
        //Utils::dumpx(Yii::$app->user->identity->id);
        if(isset($model->imagen) && $model->imagen!='')
         $aux = $model->imagen;


        if ($model->load(Yii::$app->request->post()))
        {
            if ($inputfile = UploadedFile::getInstance($model, 'imagen')) {
                $img = file_get_contents($inputfile->tempName);
                $model->imagen = 'data:image/png;base64,'.base64_encode($img) ;
            }else
                $model->imagen = $aux;


            $model->fecha = date('Y-m-d H:i:s');
            $model->id_usuario = Yii::$app->user->identity->id;

            if($model->save()){
                $usuario = \common\models\User::findOne(Yii::$app->user->identity->id);
                $usuario->username = $model->email;
                $usuario->email = $model->email;
                $sw='';
                $pass_old = $usuario->pass;

                //Utils::dump($usuario->attributes);
                if(!empty($model->password) && !empty($model->repeat_password) && $model->password==$model->repeat_password) {
                  //  echo 111111; exit;
                    $usuario->auth_key = Yii::$app->security->generateRandomString();
                    $usuario->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                    $usuario->pass = $model->password;
                    $sw = 'Como actualizó la contraseña, debe volver a iniciar sesión.';
                }
               // Utils::dump($usuario->attributes);
                 //Utils::dumpx($pass_old.' != '.$usuario->pass);
                if($usuario->save())
                {

                    if($pass_old != $usuario->pass){
                        Yii::$app->mailer->compose('layouts/cambio_contrasena',
                            ['username' => $model->nombreFull])
                            ->setFrom(['no-responder@vendetumoto.co' => 'Vendetumoto.co'])
                            ->setTo($model->email) //->setCc('wokeer+admin@gmail.com')
                            ->setSubject('Tu contraseña se ha cambiado')
                            ->send();
                    }


                    Yii::$app->getSession()->setFlash('success', ['message' => '¡Se ha <b>Editado</b> el <b>Perfil</b> con éxito! '.$sw]);
                    return $this->redirect(['miperfil']);
                }else
                    Utils::dumpx($usuario->getErrors());
            }else
                Utils::dumpx($model->getErrors());
        }else{


            return $this->render('miperfil', [
                'model' => $model,
            ]);
        }


        // Utils::dumpx($_POST);

        if($model->load(Yii::$app->request->post())) {
            /*if ($inputfile = UploadedFile::getInstance($model, 'imagen_cabecera')) {
                $img = file_get_contents($inputfile->tempName);
                $model->imagen_cabecera = 'data:image/png;base64,'.base64_encode($img) ;
            }

            $model->fecha_creacion = date('Y-m-d H:i:s');
            $model->fecha_edicion = date('Y-m-d H:i:s');
            $model->id_creador = Yii::$app->user->identity->id;
            $model->id_editor = Yii::$app->user->identity->id;
            $model->visitas = 0;

            */
            //se actualiza el perfil
            $apoderado_update = CliUsuario::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();
            //$apoderado_update->tipo = $model->tipo;

            //$apoderado_update->id_cliente = $model->id_cliente;
            //$apoderado_update->id_usuario = $model->id_usuario;
            $apoderado_update->nombres = $model->nombres;
            $apoderado_update->apellidos = $model->apellidos;
            $apoderado_update->cedula = $model->cedula;
            $apoderado_update->email = $model->email;
            $apoderado_update->celular = $model->celular;
            $apoderado_update->estado = $model->estado;
//            $apoderado_update->fecha = $model->fecha;
//            $apoderado_update->id_creador = $model->id_creador;
            $apoderado_update->id_editor = Yii::$app->user->identity->id;
            $apoderado_update->fecha_edicion = date('Y-m-d H:i:s');

            //Utils::dump($apoderado_update->attributes);
            if($apoderado_update->save()){
                Yii::$app->getSession()->setFlash('success', ['message' => 'Su perfil ha sido actualizado con éxito', ]);
            }else{
                Utils::dumpx($apoderado_update->getErrors());
            }
            //si viene la clave
            //Utils::dumpx($model->attributes);
            if(!empty($model->password)) {

                $usuario = User::findOne(Yii::$app->user->identity->id);
                //$usuario->password = $model->password;
                $usuario->auth_key = Yii::$app->security->generateRandomString();
                $usuario->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                $usuario->pass = $model->password;
                $usuario->save();
            }

            if($role=='DIRECTOR_JURIDICO' || $role=='APODERADO'){
                return $this->render('indexcliente', [
                    'model' => $model,
                    'role' => $role,
                    //'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                    'proveedor' => $apoderado_update
                ]);
            }else{
                return $this->render('index', [
                    'model' => $model,
                    'role' => $role,
                    //'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                    'proveedor' => $apoderado_update
                ]);
            }

            // return $this->redirect(['index']);
        } else {
            // Utils::dumpx($model->getErrors());
            $apoderado = CliUsuario::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();
            if($role!='SUPERADMIN' && isset($apoderado)){
                $model->id_cliente = $apoderado->id_cliente;
                $model->id_usuario = $apoderado->id_usuario;
                $model->nombres = $apoderado->nombres;
                $model->apellidos = $apoderado->apellidos;
                $model->cedula = $apoderado->cedula;
                $model->email = $apoderado->email;
                $model->celular = $apoderado->celular;
                $model->estado = $apoderado->estado;

                $model->fecha = $apoderado->fecha;
                $model->id_creador = $apoderado->id_creador;
                $model->id_editor = $apoderado->id_editor;
                $model->fecha_edicion = $apoderado->fecha_edicion;

            }else{
                Yii::$app->getSession()->setFlash('error', [
                    'message' => 'Usted no tiene un ROL con Permisos, no tiene facultades de ejecutar acciones sobre ésta pantalla!',
                ]);

            }


                return $this->render('miperfil', [
                    'model' => $model,
                    'role' => $role,
                    'tipos' => isset($apoderado->tipos) ? $apoderado->tipos : null,
                    'proveedor' => $apoderado_update
                ]);

        }

    }


     public function actionEliminarfoto(){
         extract($_POST);
        // Utils::dumpx($_POST);
         /*
           [id_pub] => 9
           [id_foto] => 222
         */
         if(isset($id_foto) && $id_foto!=''){

             if(PubMultimedia::deleteAll('id='.$id_foto)){

                 echo Json::encode(['res' => true, 'id_foto' => $id_foto]);

                 //S3 PENDIENTE ELIMINAR FÍSICA EN AWS ******************

                 //**********************************************
             }else{
                 echo Json::encode(['res' => false ]);
             }

         }
     }

    public function actionPublicaranuncio2()
    {
        extract($_GET);

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado';

        $modCaracts = MotCaracteristicas::find()->where('estado=1')->all();

        if(isset($id) && $id!=''){
            $existe = PubPublicacion::find()->where('id='.$id)->count();
            if($existe==1){
                $model = PubPublicacion::find()->where('id='.$id.' and id_usuario='.Yii::$app->user->identity->id)->one();

                if($model && $model->id_usuario==Yii::$app->user->identity->id) {
                    $model->id_marca = $model->modelo->id_marca;
                    $model->id_modelo = $model->modelo->id;
                    $modelos = ArrayHelper::map(MotModelo::find()->where('id_marca='.$model->id_marca)->all(), 'id', 'nombre');
                }else{
                    echo 'ERROR: ¡Usuario no tiene permisos para ver esta publicacion!. <a href="javascript:history.back()">volver atrás</a>'; exit;
                }
            }else{
                echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>'; exit;
            }

        }else{
            $model = new PubPublicacion();

            $modelos = [];
        }


        if(isset($tipo) && $tipo!='') {
           // Utils::dump($model->isNewRecord);

                $usuario = CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
                $marcas = ArrayHelper::map(MotMarca::find()->orderBy('nombre')->all(), 'id', 'nombre');
                $model->id_tipo = $tipo;
                //NUEVO + CAMPOS COMPLETADOS
                if ($model->isNewRecord) {

                    if ($model->load(Yii::$app->request->post())) {



                        $model->id_usuario = Yii::$app->user->identity->id;
                        $model->fecha_creacion = date('Y-m-d H:i:s');
                        $model->estado = 6; //6: Borrador
                        $model->fecha_edicion = date('Y-m-d H:i:s');
                        $model->slug = '-';
                        $model->carpeta = Yii::$app->security->generateRandomString();

                        if(empty($model->descuento)) $model->descuento=0;
                        //Utils::dumpx($_POST);

                        if ($model->save()) {
                            $model->vehiculo =  $model->modelo->marca->nombre.' '.$model->modelo->nombre;
                            $model->slug = Utils::urls_amigables($model->id.'-'.$model->tipo->nombre . '-' . $model->modelo->marca->nombre . '-' . $model->modelo->nombre . '-' . $model->ano . '-' . $model->_tipo_combustible[$model->tipo_combustible] .
                                '-' . $model->recorrido . $model->_unid_recorrido[$model->unid_recorrido] . '-' . $model->ciudad->nombreFull  );

                            if($model->save()){
                                //Actualizo siempre Cel + whattsap en Cliusuario
                                $usuario->telefonos = $model->numero_contacto;
                                $usuario->whatssapp = $model->whatsapp;
                                if(!$usuario->save()){
                                    Utils::dumpx($usuario->getErrors());
                                }
                                //Todas las publicaciones de ese usuario
                                Yii::$app->db->createCommand()->update('pub_publicacion', ['numero_contacto' => $usuario->telefonos, 'whatsapp' => $usuario->whatssapp, ],  'id_usuario=:id', [
                                    ':id' => $usuario->id_usuario
                                ])->execute();

                                // Guardo las Caracteristicas: Elimino las que tiene y vuelvo a guardar todas las seleccionadas
                                if(isset($_POST['caracteristicas'])){
                                    $vec_ids = array_keys($_POST['caracteristicas']);
                                    for($i=0;$i < count($vec_ids);$i++){
                                        $modCar = new PubCaracteristica();
                                        $modCar->id_publicacion = $model->id;
                                        $modCar->id_caracteristica = $vec_ids[$i];
                                        $modCar->save();
                                    }
                                }
                            }
                            Yii::$app->getSession()->setFlash('success', ['message' => '¡Excelente! Falta poco, adjunta las fotos de tu vehículo. Por el momento la publicación está en estado "'.$model->_estado[$model->estado].'".',]);
                            return $this->redirect(['publicaranuncio3', 'id' => $model->id]);
                        } else {
                            Utils::dumpx($model->getErrors());
                        }

                    }
            }else  //YA EXISTE : PARA EDITAR
                if (!$model->isNewRecord && $model->load(Yii::$app->request->post())) {
                    $model->fecha_edicion = date('Y-m-d H:i:s');


                    if ($model->save()) {
                        $model->vehiculo =  $model->modelo->marca->nombre.' '.$model->modelo->nombre;
                        $model->slug = Utils::urls_amigables($model->id.'-'.$model->tipo->nombre . '-' . $model->modelo->marca->nombre . '-' . $model->modelo->nombre . '-' . $model->ano . '-' . $model->_tipo_combustible[$model->tipo_combustible] .
                            '-' . $model->recorrido . $model->_unid_recorrido[$model->unid_recorrido] . '-' . $model->ciudad->nombreFull  );

                        PubCaracteristica::deleteAll('id_publicacion='.$model->id);
                        if(isset($_POST['caracteristicas'])){
                            $vec_ids = array_keys($_POST['caracteristicas']);
                            for($i=0;$i < count($vec_ids);$i++){
                                $modCar = new PubCaracteristica();
                                $modCar->id_publicacion = $model->id;
                                $modCar->id_caracteristica = $vec_ids[$i];
                                $modCar->save();
                            }
                        }

                        //*******************************************************************
                        //Actualizo siempre Cel + whattsap en Cliusuario
                        $usuario->telefonos = $model->numero_contacto;
                        $usuario->whatssapp = $model->whatsapp;
                        if(!$usuario->save()){
                            Utils::dumpx($usuario->getErrors());
                        }
                        //Todas las publicaciones de ese usuario
                        Yii::$app->db->createCommand()->update('pub_publicacion', ['numero_contacto' => $usuario->telefonos, 'whatsapp' => $usuario->whatssapp, ],  'id_usuario=:id', [
                            ':id' => $usuario->id_usuario
                        ])->execute();

                        //*******************************************************************

                        Yii::$app->getSession()->setFlash('success', ['message' => ' ¡Publicación ACTUALIZADA con éxito! Por el momento la publicación está en estado "'.$model->_estado[$model->estado].'".',]);
                        return $this->redirect(['publicaranuncio3', 'id' => $model->id]);
                    } else {
                        Utils::dumpx($model->getErrors());
                    }

                }


        }else{
            echo 'ERROR: ¡Faltan parámetros!. <a href="javascript:history.back()">volver atrás</a>'; exit;
        }

        return $this->render('publicaranuncio2', [
            'model' => $model, 'marcas' => $marcas, 'modelos' => $modelos,
            'numero_contacto' => $usuario->telefonos, 'whatsapp' => $usuario->whatssapp, 'modCaracts'=>$modCaracts
        ]);

    }

    public function actionMispublicaciones()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado2';

        $todos=false;
        //Utils::dump($_GET);

        if(isset($_GET['VBuscadorPublicaciones'])){
            extract($_GET['VBuscadorPublicaciones']);
            if($palabras_search!='' && $estado_pub!=''){
                $condition = [
                    'AND',                                //Operand (C1 OR C2)
                    [                                    //Condition1 (C1) which is complex
                        'OR',                            //Operand (C1.1 OR C1.2)
                        ['like','vehiculo',$palabras_search],   //C1.1
                        ['like','ciudad',$palabras_search], //C1.2
                    ],
                    ['like','estado_pub',$estado_pub]      //Condition2(C2)
                ];
            }else if($palabras_search=='' && $estado_pub!=''){
                $condition = [
                    'OR',                               //Operand (C1 OR C2)
                    ['like','estado_pub',$estado_pub]      //Condition2(C2)
                ];
            }else if($palabras_search!='' && $estado_pub==''){

                $condition = [
                    'AND',                                //Operand (C1 OR C2)
                    [                                    //Condition1 (C1) which is complex
                        'OR',                            //Operand (C1.1 OR C1.2)
                        ['like','vehiculo',$palabras_search],   //C1.1
                        ['like','ciudad',$palabras_search], //C1.2
                    ]
                ];
            }else{
                $todos=true;
            }
        }


        if($todos || !isset($condition)){

            $sql = VBuscadorPublicaciones::find()
                ->andWhere(['id_usuario' => Yii::$app->user->identity->id,])
                ->andWhere('estado!=8')
                ->orderBy([new \yii\db\Expression('FIELD (estado, 1 ) DESC, id desc')]);

            //Utils::dumpx($sql);

        }else if(isset($condition)){

            $sql = VBuscadorPublicaciones::find()
                ->andWhere(['id_usuario' => Yii::$app->user->identity->id])
                ->andWhere('estado!=8')
                ->andWhere($condition)
                ->orderBy([new \yii\db\Expression('FIELD (estado, 1 ) DESC, id desc')]);
               // ->orderByF('estado desc,id DESC'); // BY FIELD (marca,'Smart','Audi','Seat') DESC;
        }



        $dataProvider = new ActiveDataProvider([
            'query' => $sql,
            'pagination' => [
                'pageSize' => 100,
            ],
            'TotalCount' => $sql->count(),
        ]);




        return $this->render('mispublicaciones',['dataProvider' => $dataProvider]);
    }

    public function actionPublicaranuncio()
    {
        extract($_GET);
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }

        $this->layout = 'main-logueado';
        $model = new PubPublicacion();
        //Si EXISTE: viene con ID, es por que ya existe
        if(isset($id) && $id!=''){
            $model = PubPublicacion::find()->where('id='.$id.' and id_usuario='.Yii::$app->user->identity->id)->one();
            if($model && $model->id_usuario==Yii::$app->user->identity->id){
               return $this->render('publicaranuncio', ['tipo' => $model->id_tipo, 'model' => $model]);
            }else{
                echo 'ERROR: ¡NO tiene permisos para acceder a ésta publicación! <a href="javascript:history.back()">volver atrás</a>'; exit;
            }

        }else //si no viene ID, pero SI viene TIPO: es nuevo, pero vino del paso 2 (sin guardar pub)
            if(isset($tipo) && $tipo!='' && ($tipo==1 || $tipo==2 || $tipo==3 || $tipo==4)){
                //echo 22222;
                return $this->render('publicaranuncio', ['tipo' => $tipo,'model' => $model ]);
            }else
                if(isset($tipo) && $tipo!='' &&  $tipo>4) {
                    //echo 333333;
                    echo 'ERROR: ¡Parámetros Inválidos!. <a href="javascript:history.back()">volver atrás</a>'; exit;
                }else{
                    //echo 111111;
                    return $this->render('publicaranuncio',['model' => $model]);
                }

            //echo 'ERROR: ¡Faltan parámetros!. <a href="javascript:history.back()">volver atrás</a>'; exit;



    }


    public function actionPublicaranuncio3()
    {
        extract($_GET);
        //Utils::dump($_GET);        Utils::dump($_POST);        Utils::dump($_FILES);
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado';

        if(isset($id) && $id!=''){
            $existe = PubPublicacion::find()->where('id='.$id)->count();
            if($existe==1) {
                $model = PubPublicacion::find()->where('id=' . $id . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
                //    Utils::dumpx($model->attributes);
                if ($model && $model->id_usuario == Yii::$app->user->identity->id) {
                    $modMult = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $id)->all();

                    return $this->render('publicaranuncio3', [
                        'model' => $model, 'modelmult' => $modMult
                    ]);
                } else {
                    echo 'ERROR: ¡NO tiene permisos para acceder a ésta publicación! <a href="javascript:history.back()">volver atrás</a>';
                    exit;
                }
            }else{
                echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>'; exit;
            }

        }else{
            echo 'ERROR: ¡Faltan parámetros!. <a href="javascript:history.back()">volver atrás</a>'; exit;
        }

    }

    public function actionPublicaranuncio4()
    {
        extract($_GET);
        //Utils::dump($_GET);        Utils::dump($_POST);        Utils::dump($_FILES);
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">La sesión ha finalizado. Debes iniciar sesión nuevamente.</b></div>');
            return $this->redirect(['/site/index']);
        }
        $this->layout = 'main-logueado';

        if(isset($id) && $id!=''){
            $existe = PubPublicacion::find()->where('id='.$id)->count();
            if($existe==1) {
                $model = PubPublicacion::find()->where('id=' . $id . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
                if ($model && $model->id_usuario == Yii::$app->user->identity->id) {
                    $planes = Paquete::find()->where('estado=1')->all();

                    return $this->render('publicaranuncio4', [
                        'model' => $model, 'planes' => $planes,
                    ]);
                } else {
                    echo 'ERROR: ¡NO tiene permisos para acceder a ésta publicación! <a href="javascript:history.back()">volver atrás</a>';  exit;
                }
            }else{
                echo 'ERROR: ¡ID de Publicación NO Existe!. <a href="javascript:history.back()">volver atrás</a>'; exit;
            }

        }else{
            echo 'ERROR: ¡Faltan parámetros!. <a href="javascript:history.back()">volver atrás</a>'; exit;
        }

    }


    public function actionLoginajax()
    {
        Utils::dump(Yii::$app->request->post());
        $model = new LoginForm();
        Utils::dumpx($model->attributes);
        if($model->load(Yii::$app->request->post()) && $model->login()) {
            Utils::dumpx($model->login());
        }else{
            Utils::dumpx($model->getErrors());
        }
    }
    public function actionLogin()
    {

        //Utils::dumpx($_GET);
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $modelSignup = new SignupForm();
        $modelCliU = new CliUsuario();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
          //  Utils::dumpx(88888888888888888);
            if(isset($_GET['origen']) && $_GET['origen']==2){
                Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">Bienvenido a <b>Vendetumoto.co</b></div>');
                return $this->redirect(Url::to(['site/publicaranuncio']));
            }else
            if(isset($_GET['origen']) && $_GET['origen']==3){
                    $refer = explode('pubdetallepublico',$_SERVER['HTTP_REFERER']);
                    if(count($refer)>0){
                        $vec = explode('?id=',$refer[1]);
                        if(count($vec)==2) $id_pub=$vec[1];
                        Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">Bienvenido a <b>Vendetumoto.co</b></div>');
                        return $this->redirect(Url::to(['site/pubdetallepublico','id'=>$id_pub]));
                    }else {
                         return $this->goBack();
                    }

            }else
            if(isset($_GET['origen']) && $_GET['origen']==4) {
                $refer = explode('pubdetallepublico',$_SERVER['HTTP_REFERER']);
                if(count($refer)>0){
                    $vec = explode('?id=',$refer[1]);
                    if(count($vec)==2) $id_pub=$vec[1];
                    //Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">Bienvenido a <b>Vendetumoto.co</b></div>');
                    header("Location: ".$_GET['url']);
                    die();
                }else {
                    return $this->goBack();
                }
            }else{
                // Utils::dumpx($_GET);
                 return $this->goBack();
            }

        } else
            if($modelSignup->load(Yii::$app->request->post()) && $modelCliU->load(Yii::$app->request->post()) )
            {
                // registar
                $existUser = \common\models\User::find()->where('username="'.$modelSignup['email'].'"')->count();
                if($existUser > 0){
                   // Utils::dumpx(000000000000000000);
                    Yii::$app->session->setFlash('error', 'ERROR: ¡Usuario ya existe!');
                    return $this->render('login', [
                        'model' => $model,
                        'modelSignup' => $modelSignup,
                        'modelCliU' => $modelCliU,
                    ]);
                }else{
                    //Sinó: Crea el NUEVO USUARIO



                    $model = new SignupForm();
                    $model->username = $modelSignup['email'];
                    $model->email = $modelSignup['email'];
                    $model->pass = $modelSignup['password'];
                    $model->password = Yii::$app->security->generatePasswordHash($modelSignup['password']);
                    $model->repeat_password = $modelSignup['password']; //Yii::$app->security->generatePasswordHash($email);

                    if ($user = $model->signup()) {
                        if (Yii::$app->getUser()->login($user)) {

                            //Guardar en tabla Usuarios Cliente
                            $modelCli = new CliUsuario();
                            $modelCli->nombre = $modelCliU['nombre'];
                            $modelCli->id_cliente = 1;
                            $modelCli->telefonos = $modelCliU['telefonos'];
                            $modelCli->whatssapp = $modelCliU['whatssapp'];
                            $modelCli->id_usuario = $user->id;
                            $modelCli->email = $user->email;
                            $modelCli->rol = 2;
                            $modelCli->estado=1;
                            $modelCli->fecha = date('Y-m-d H:i:s');
                            $modelCli->check_acepto_term = 1;
                            if(isset($model->promocionales) && $model->promocionales!='') $modelCli->check_recibe_prom = 1; else $modelCli->check_recibe_prom = 0;


                            //Utils::dumpx($modelCli->attributes);

                            if($modelCli->save()){
                                Yii::$app->db->createCommand() //ingresamos el usuario al grupo Directores Comerciales
                                ->insert(Yii::$app->db->tablePrefix . 'auth_assignment', [
                                    'user_id' => $modelCli->id_usuario,
                                    'item_name' => 'CLI_USUARIO',
                                    'created_at' => time(),
                                ])->execute();

                                $nuevafecha = strtotime ( '+2 hour' , time() ) ; $nuevafecha = date('Y-m-d H:ia', $nuevafecha);
                                Yii::$app->mailer->compose('layouts/email_registro', [ 'username'=>$modelCli->email, 'password' => '***'])
                                    ->setFrom(['no-responder@vendetumoto.co'=>'Vendetumoto.co'])
                                    ->setTo($modelCli->email)
                                    ->setBcc([Yii::$app->params['adminEmail'] => "Administrador Vendetumoto.co"]) //;
                                    ->setSubject('Bienvenido a Vendetumoto.co | '.$modelCli->email. ' ['.$nuevafecha.']')
                                    ->send();


                            }else {
                                Utils::dumpx($modelCli->getErrors());
                            }

                            Yii::$app->session->setFlash('success', '<div class="row" style="margin-left: 20px">Bienvenido a <b>Vendetumoto.co</b></div>');
                            if(isset($_GET['origen']) && $_GET['origen']==2){
                                return $this->redirect(Url::to(['site/publicaranuncio']));
                            }
                            else   return $this->goHome();
                            // Yii::$app->getUser()->setReturnUrl(\yii\helpers\Url::to(['/']));

                        }else
                            Utils::dumpx(2222222);

                    }else{
                        Utils::dumpx(11111111111);
                    }
                }


            }else {
                //Utils::dumpx('Error: linea: 1874');
                $model->password = '';
                $modelSignup->password = '';

                return $this->render('login', [
                    'model' => $model,
                    'modelSignup' => $modelSignup,
                    'modelCliU' => $modelCliU,
                ]);
            }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
       // Utils::dumpx($model->attributes);
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        //    Utils::dumpx(Yii::$app->request->post());
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Revise su correo electrónico, le hemos enviado un enlace de confirmación');



                return $this->render('requestPasswordResetToken', [
                    'model' => $model,
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'Lo sentimos, no podemos restablecer la contraseña de la dirección de correo electrónico proporcionada.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {

        try {
            $model = new ResetPasswordForm($token);
            $model2 = \common\models\User::find()->where('password_reset_token="'.$token.'"')->one();

        } catch (InvalidArgumentException $e) {

            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {

            if($model->resetPassword()){




                //envio de correo electrónico de contraseña cambiada
                Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'contrasenaactualizada'],
                        ['user' => $model2]
                    )
                    ->setFrom(['no-responder@vendetumoto.co' => 'Vende Tu Moto'])
                    ->setTo($model2->email)
                    ->setSubject('Contraseña actualizada con éxito para Vende Tu Moto') // . Yii::$app->name
                    ->send();

                Yii::$app->session->setFlash('success', '¡Nueva contraseña actualizada!');

                return $this->goHome();

            }


            //$model->validate() && $model->resetPassword()




        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGetmodelosmarca() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                if($parents[0]!=''){
                    $registros = \common\models\MotModelo::find()->where('id_marca='.$parents[0].' AND estado=1')->orderBy('nombre ASC')->all();
                    foreach ($registros as $reg) {
                        $out[] = ['id' => $reg->id,  'name' =>  $reg->nombre];
                    }
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetfotosanuncio()
    {
        extract($_POST);
        if(isset($id_publicacion) && $id_publicacion!=''){
            $model = PubPublicacion::find()->where('id=' . $id_publicacion . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
            //Utils::dump($model->attributes);
            if ($model && $model->id_usuario == Yii::$app->user->identity->id)
            {
                $total = PubMultimedia::find()->where('id_publicacion='.$id_publicacion.' and tipo=1')->count();
                if($total>0)
                    echo Json::encode(['res' => true, 'id_pub' => $id_publicacion]);
                else{
                    echo Json::encode(['res' => false, 'error' => 'Error: ¡No tiene adjunta ninguna foto!']);
                }
            }else
                echo Json::encode(['res' => false, 'error' => 'Error: El ID de la publicación NO existe o el usuario no tiene permisos para continuar al siguiente paso.']);
        }else
            echo Json::encode(['res' => false, 'error' => 'Error: ¡Parámetros inválidos!']);

    }

    public function actionAddvendido()
    {
        extract($_POST);
      //  Utils::dumpx($_POST);
        /*
            [id_publicacion] => 9
            [tipo] => 2
        */
        if(isset($id_publicacion) && $id_publicacion!=''){
            $model = PubPublicacion::find()->where('id=' . $id_publicacion . ' and id_usuario=' . Yii::$app->user->identity->id)->one();

            if ($model && $model->id_usuario == Yii::$app->user->identity->id)
            {
                $model->vendido_por = $tipo;
                $model->estado=4; // 4: VENDIDO
                if($model->save()){
                    //Enviar EMAIL a : Admin y a Usuario *******************************

                    //******************************************************************
                    echo Json::encode(['res' => true]);
                }else
                    Utils::dumpx($model->getErrors());
            }else
                echo Json::encode(['res' => false, 'error' => 'Error: El ID de la publicación NO existe o el usuario no tiene permisos para continuar al siguiente paso.']);
        }else
            echo Json::encode(['res' => false, 'error' => 'Error: ¡Parámetros inválidos!']);

    }

    function getImage($url,$save_dir='',$filename='',$type=0)
    {
        if(trim($url)==''){
            return array('file_name'=>'','save_path'=>'','error'=>1);
        }

        if(trim($save_dir)==''){
            $save_dir='./';
        }

        if (trim ($filename) == '') {// Guardar nombre de archivo
            $ext=strrchr($url,'.');
            if($ext!='.gif'&&$ext!='.jpg'){
                return array('file_name'=>'','save_path'=>'','error'=>3);
            }
            $filename=time().$ext;
        }

        if(0!==strrpos($save_dir,'/')){
            $save_dir.='/';
        }

        // Crear guardar directorio
        if(!file_exists($save_dir)&&!mkdir($save_dir,0777,true)){
            return array('file_name'=>'','save_path'=>'','error'=>5);
        }

        // El método utilizado para obtener el archivo remoto
        if($type){
            $ch=curl_init();
            $timeout=5;
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
            $img=curl_exec($ch);
            curl_close($ch);
        }else{
            ob_start();
            readfile($url);
            $img=ob_get_contents();
            ob_end_clean();
        }

        //Tamaño del archivo
        $fp2=@fopen($save_dir.$filename,'a');
        fwrite($fp2,$img);
        fclose($fp2);
        unset($img,$url);

        return array('file_name'=>$filename,'save_path'=>$save_dir.$filename,'error'=>0);
    }


     public function actionGetFiltrosIzquierda($str_otras, $str_cilindraje, $str_marca, $str_modelo, $str_tipo, $str_ano, $str_vendedor, $str_condicion, $str_recorrido)
     {
         $str_filtros='';

       //  echo '_____'.$str_marca.' - '.$str_modelo.' - '.$str_tipo.' - '.$str_ano.' - '.$str_vendedor.' - '.$str_cilindraje.' - '.$str_condicion.' - '.$str_recorrido.'-'.$str_otras.'_____';



         //new
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.')';
         //if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = ''; //Vacío

         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.')';

         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';

         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_condicion.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and  ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';

         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_vendedor.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';

         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_ano.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.')';

         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.')';

         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.')';


         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_recorrido.')';


         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_condicion.') and ('.$str_recorrido.')';

         if($str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';

         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';

         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.')';


         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.')';

//ok
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_recorrido.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_vendedor.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_condicion.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
//fin new *****************

         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = $str_ano;
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_ano.') and ('.$str_vendedor.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = $str_vendedor;
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = $str_condicion;

//NO encontrados y Agregados:
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         //año+cond
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='')  $str_filtros = '('.$str_ano.') and ('.$str_condicion.')';
         //año+mod
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='')  $str_filtros = '('.$str_ano.') and ('.$str_modelo.')';
         //cond+mod
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='')  $str_filtros = '('.$str_condicion.') and ('.$str_modelo.')';
         //vend+mod
         if($str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='')  $str_filtros = '('.$str_vendedor.') and ('.$str_modelo.')';
         //vend+recorr
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='')  $str_filtros = '('.$str_vendedor.') and ('.$str_recorrido.')';
         //ano+recorr
         if($str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='')  $str_filtros = '('.$str_ano.') and ('.$str_recorrido.')';
         //marca+año+cond
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='')  $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_condicion.')';
         //marca+año+cond+recorr
         if($str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='')  $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         //marca+mod+año+vend+cond+recorr
         if($str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         //marca+mod+vendedor+condicion+recorr
         if($str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';


         //CILINDRAJE
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 0and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 1and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 2and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 3and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 4and ('.$str_modelo.') and ('.$str_tipo.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') 5and ('.$str_modelo.')';
         if($str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_ano.') and ('.$str_condicion.')';
         //if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.')';
         if($str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_recorrido.')';

         //OTRAS
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') ';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_marca.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.')';

         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_recorrido.')';

         //if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_vendedor.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_tipo.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_modelo.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_ano.') and ('.$str_condicion.') and ('.$str_recorrido.')';

//add
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_ano.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_modelo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_tipo.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_vendedor.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_modelo.') and ('.$str_modelo.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_modelo.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_tipo.') and ('.$str_marca.')';


         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_modelo.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_tipo.')';

         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.')';

         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_modelo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_cilindraje.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_ano.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_otras.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_cilindraje.')';
         //tip+cil+cond+recorr
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_cilindraje.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_recorrido.') and ('.$str_vendedor.') and ('.$str_condicion.') and ('.$str_cilindraje.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_vendedor.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_cilindraje.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_tipo.') and ('.$str_cilindraje.') and ('.$str_condicion.') and ('.$str_vendedor.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_recorrido.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_condicion.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')  and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_modelo.') and ('.$str_condicion.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')  and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_condicion.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')  and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_cilindraje.') and ('.$str_marca.') and ('.$str_condicion.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')  and ('.$str_tipo.')';
         //tipo-cil-ano-vend-cond-recorr-otras
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_cilindraje.') and ('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_ano.') and ('.$str_vendedor.') and ('.$str_otras.')  and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_tipo.') and ('.$str_marca.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_tipo.') and ('.$str_marca.') and ('.$str_modelo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_tipo.') and ('.$str_marca.') and ('.$str_modelo.') and ('.$str_cilindraje.')';

         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_vendedor.') and ('.$str_otras.') and ('.$str_cilindraje.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_marca.') and ('.$str_otras.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_condicion.') and ('.$str_marca.') and ('.$str_otras.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_marca.') and ('.$str_otras.') and ('.$str_tipo.') and ('.$str_cilindraje.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_recorrido.') and ('.$str_condicion.') and ('.$str_marca.') and ('.$str_otras.') and ('.$str_tipo.') and ('.$str_cilindraje.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_ano.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_cilindraje.') and ('.$str_recorrido.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_modelo.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_tipo.') and ('.$str_modelo.') and ('.$str_vendedor.')';

         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_modelo.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_ano.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_vendedor.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_condicion.') and ('.$str_recorrido.') and ('.$str_otras.')';

         if($str_otras!='' && $str_cilindraje=='' && $str_marca=='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_modelo.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo=='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras!='' && $str_cilindraje=='' && $str_marca!='' && $str_modelo!='' && $str_tipo=='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_modelo.') and ('.$str_recorrido.') and ('.$str_otras.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_modelo.') and ('.$str_cilindraje.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_modelo.') and ('.$str_cilindraje.') and ('.$str_condicion.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca=='' && $str_modelo!='' && $str_tipo!='' && $str_ano=='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_modelo.') and ('.$str_cilindraje.') and ('.$str_otras.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_vendedor.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_condicion.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_recorrido.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_tipo.') and ('.$str_marca.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_otras.')';
         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_recorrido.') and ('.$str_vendedor.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_tipo.')';
         if($str_otras!='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor!='' && $str_condicion=='' && $str_recorrido=='') $str_filtros = '('.$str_marca.') and ('.$str_otras.') and ('.$str_vendedor.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_tipo.')';

         if($str_otras=='' && $str_cilindraje!='' && $str_marca!='' && $str_modelo=='' && $str_tipo!='' && $str_ano!='' && $str_vendedor=='' && $str_condicion!='' && $str_recorrido!='') $str_filtros = '('.$str_marca.') and ('.$str_condicion.') and ('.$str_recorrido.') and ('.$str_cilindraje.') and ('.$str_ano.') and ('.$str_tipo.')';



         return $str_filtros;

     }

     

    //========================================================================================================================
    
    /* RANDI: esta función la hice mientras.. para continuar con el proceso cuando ya está paga... */
    public function actionPagoaprobado()
    {
        //$this->layout = 'main-logueado';
        extract($_POST);
        // Utils::dumpx(($_POST));

        //Si EXISTE: viene con ID, es por que ya existe
        if (isset($id_publicacion) && $id_publicacion != '') {
            $model = PubPublicacion::find()->where('id=' . $id_publicacion . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
            if ($model && $model->id_usuario == Yii::$app->user->identity->id) {

                //Actualizo

                $modPlan = Paquete::find()->where('id='.$id_plan)->one();

                $modelPago = new Payments();
                $modelPago->id_usuario = Yii::$app->user->identity->id;
                $modelPago->id_paquete = $id_plan;
                $modelPago->id_publicacion = $id_publicacion;
                $modelPago->dias_publicados = $modPlan->dias_publicados;
                $modelPago->created_at = date('Y-m-d H:i:s');
                $modelPago->updated_at = date('Y-m-d H:i:s');
                $modelPago->date_generate_order = date('Y-m-d H:i:s');
                $modelPago->date_generate_payment = date('Y-m-d H:i:s');
                $modelPago->date_approve_payment = date('Y-m-d H:i:s');
                $modelPago->total_price = $modPlan->precio;
                $modelPago->price_col = $modPlan->precio;
                $modelPago->state = 1;
                if($modelPago->save()){
                    $model->estado = 2; //2 - PENDIENTE_X_APROBAR
                    $model->save();

                    echo Json::encode(['res' => true, 'url' => Url::to(['site/mispublicaciones','id'=>$id_publicacion])]); // mientras
                }


            } else {

                echo Json::encode(['res' => false]);
            }

        } else
        {
            echo $id_publicacion;
        }
    }
    //========================================================================================================================
    public function actionEpaycotransaction (){


        if (!Yii::$app->user->isGuest) {

            if(!isset($_GET['ref_payco'])){
                Utils::dumpx('<b>ERROR:</b> ¡Parámetros inválidos! Vuelva atrás en su navegador.');
            }
            // extract($_GET);
            // extract($_POST);
            // --------------------------------------------------------
            $url = 'https://secure.epayco.co/validation/v1/reference/'.$_GET['ref_payco'];
            $content = file_get_contents($url);
            $json = json_decode($content, true);
            $payment = $json['data'];
            // --------------------------------------------------------
//             Utils::dump(($json));
//             Utils::dump(!empty($payment));
//             Utils::dump(isset($payment));
//             Utils::dump($payment);
            // ---------------------------------------------------
            if( isset($payment) && !empty($payment) )
            {

                $model = PubPublicacion::find()->where('id=' . $payment['x_extra3'] . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
                $model->estado = 2; //2 - PENDIENTE_X_APROBAR

                if(!$model->save(false)){
                    Utils::dumpx($model->getErrors());
                }

                if($payment['x_cod_transaction_state'] == 1)
                {

                    Yii::$app->session->setFlash('success', '¡Listo! Tu pago está aprobado');
                    if(isset($payment['x_type_payment']) && $payment['x_type_payment']=='TDC')
                        return $this->render('epaycotransaction', [
                            'referencia' => $payment['x_id_factura'],        'fecha' => $payment['x_transaction_date'],
                            'respuesta' => $payment['x_response'],           'motivo' => $payment['x_response_reason_text'],
                            'banco' => $payment['x_bank_name'],              'refEpayco' => $payment['x_transaction_id'],
                            'compra' => $payment['x_description'],           'total' => $payment['x_amount']." ". $payment['x_currency_code'],
                            'extra1' => $payment['x_extra1'],                'tipo_pago' => $payment['x_type_payment'],
                            'nro_tarjeta' => $payment['x_cardnumber'],       'cuotas' => $payment['x_quotas'],
                            'estado_transaccion' => $payment['x_cod_transaction_state'], 
                                'estado_transaccion' => $payment['x_cod_transaction_state'], 
                            'estado_transaccion' => $payment['x_cod_transaction_state'], 
                                'estado_transaccion' => $payment['x_cod_transaction_state'], 
                            'estado_transaccion' => $payment['x_cod_transaction_state'], 

                        ]);
                    else{
                        return $this->render('epaycotransaction', [
                            'referencia' => $payment['x_id_factura'],        'fecha' => $payment['x_transaction_date'],
                            'respuesta' => $payment['x_response'],           'motivo' => $payment['x_response_reason_text'],
                            'banco' => $payment['x_bank_name'],              'refEpayco' => $payment['x_transaction_id'],
                            'compra' => $payment['x_description'],           'total' => $payment['x_amount']." ". $payment['x_currency_code'],
                            'extra1' => $payment['x_extra1'],                'tipo_pago' => $payment['x_type_payment'],
                            'estado_transaccion' => $payment['x_cod_transaction_state'],
                        ]);
                    }

                }else
                {
                    Yii::$app->session->setFlash('info', '¡Estamos verificando que pasó con tu pago!');
                    if(isset($payment['x_type_payment']) && $payment['x_type_payment']=='TDC')
                        return $this->render('epaycotransaction', [
                            'referencia' => $payment['x_id_factura'],        'fecha' => $payment['x_transaction_date'],
                            'respuesta' => $payment['x_response'],           'motivo' => $payment['x_response_reason_text'],
                            'banco' => $payment['x_bank_name'],              'refEpayco' => $payment['x_transaction_id'],
                            'compra' => $payment['x_description'],           'total' => $payment['x_amount']." ". $payment['x_currency_code'],
                            'extra1' => $payment['x_extra1'],                'tipo_pago' => $payment['x_type_payment'],
                            'nro_tarjeta' => $payment['x_cardnumber'],       'cuotas' => $payment['x_quotas'],
                            'estado_transaccion' => $payment['x_cod_transaction_state'],

                        ]);
                    else {
                        return $this->render('epaycotransaction', [
                            'referencia' => $payment['x_id_factura'], 'fecha' => $payment['x_transaction_date'],
                            'respuesta' => $payment['x_response'], 'motivo' => $payment['x_response_reason_text'],
                            'banco' => $payment['x_bank_name'], 'refEpayco' => $payment['x_transaction_id'],
                            'compra' => $payment['x_description'], 'total' => $payment['x_amount'] . " " . $payment['x_currency_code'],
                            'extra1' => $payment['x_extra1'], 'tipo_pago' => $payment['x_type_payment'],
                            'estado_transaccion' => $payment['x_cod_transaction_state'],
                        ]);
                    }
                }

            }else{
                Yii::$app->session->setFlash('success', '<b>ERROR:</b>¡No es válida la referencia de pago!');
                return $this->redirect(\yii\helpers\Url::to(['/']));
            }

          

        }else{
            //Utils::dumpx($payFind);
            return $this->redirect(\yii\helpers\Url::to(['/']));
        }

    }

    public function actionEpaycoconfirmation()
    {

        //     Utils::dumpx($_POST);
        //   Utils::dump($_REQUEST['x_extra4']);
        //        Utils::dumpx(isset(Yii::$app->user->identity->id));
        //   return $this->render('epaycoconfirmation');

        /*En esta página se reciben las variables enviadas desde ePayco hacia el servidor.
        Antes de realizar cualquier movimiento en base de datos se deben comprobar algunos valores
        Es muy importante comprobar la firma enviada desde ePayco
        Ingresar  el valor de p_cust_id_cliente lo encuentras en la configuración de tu cuenta ePayco
        Ingresar  el valor de p_key lo encuentras en la configuración de tu cuenta ePayco
        */

        if(isset($_REQUEST['x_extra3']) && isset($_REQUEST['x_extra4']) && ($_REQUEST['x_extra4']!='' && $_REQUEST['x_extra3']!=''))
        {
            $id_publicacion = $_REQUEST['x_extra3'];
            $id_usuario_comprador = $_REQUEST['x_extra4'];
            $id_plan = $_REQUEST['x_extra5'];

            $payments_wh = new PaymentsWebhooks();
            $payments_wh->json = json_encode($_REQUEST);
            $payments_wh->create_at = date('Y-m-d H:i:s');
            $payments_wh->platform_type = 1;
            $payments_wh->save();

            // --------------------------------------------------------
            $p_cust_id_cliente = '570413';
            $p_key             = '11b979788c7b42c44e836c82781d74284c44fe47';
            // ------------------------------------------------
            $x_ref_payco      = $_REQUEST['x_ref_payco'];
            $x_transaction_id = $_REQUEST['x_transaction_id'];
            $x_amount         = $_REQUEST['x_amount'];
            $x_currency_code  = $_REQUEST['x_currency_code'];
            $x_signature      = $_REQUEST['x_signature'];
            // ------------------------------------------------
            $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
            // ------------------------------------------------
            $x_response     = $_REQUEST['x_response'];
            $x_motivo       = $_REQUEST['x_response_reason_text'];
            $x_id_invoice   = $_REQUEST['x_id_invoice'];
            $x_autorizacion = $_REQUEST['x_approval_code'];
            // ------------------------------------------------

            //Validamos la firma
            if ($x_signature == $signature)
            {

                /*Si la firma esta bien podemos verificar los estado de la transacción*/
                $x_cod_response = $_REQUEST['x_cod_response'];

                $model = PubPublicacion::find()->where('id=' . $id_publicacion . ' and id_usuario=' . $id_usuario_comprador)->one();

                // ----------------------------------------------------------------------
                //SOLO SI EPAYCO DEVUELVE QUE : SI PAGÓ EL USUARIO - TRANCACCIÓN EXITOSA
                // ---------------------------------------------------------------------
                if($model && $x_cod_response == "1")
                {

                    $externalDecode = json_decode(base64_decode($_REQUEST['x_extra2']));
                    //Utils::dumpx($externalDecode);
                    $modPlan = Paquete::find()->where('id='.$externalDecode->id_plan)->one();
                    // ---------------------------------------------------
                    $payTotalFind = new Payments();
                    $payTotalFind->id_usuario = $externalDecode->id_usuario;
                    $payTotalFind->id_paquete = $externalDecode->id_plan;
                    $payTotalFind->id_publicacion = $externalDecode->id_publicacion;
                    $payTotalFind->dias_publicados = $modPlan->dias_publicados;
                    $payTotalFind->total_price = $modPlan->precio;
                    $payTotalFind->date_generate_order = $_REQUEST['x_fecha_transaccion'];
                    $payTotalFind->created_at = $_REQUEST['x_fecha_transaccion'];
                    $payTotalFind->updated_at = date('Y-m-d H:i:s');
                    $payTotalFind->collection_id = ''.$_REQUEST['x_approval_code'].'';
                    $payTotalFind->observations = $_REQUEST['x_response_reason_text'].' - '.$_REQUEST['x_extra1'];
                    $payTotalFind->code = "approved";
                    $payTotalFind->transaction_code = $_REQUEST['x_cod_transaction_state'];
                    $payTotalFind->transaction_state = $_REQUEST['x_transaction_state'];
                    $payTotalFind->state = 1;
                    $payTotalFind->external_reference = $_REQUEST['x_extra2'];
                    $payTotalFind->payment_method_id = $_REQUEST['x_franchise'];
                    $payTotalFind->payment_type_id = $_REQUEST['x_bank_name'];
                    $payTotalFind->date_generate_payment = $_REQUEST['x_fecha_transaccion'];
                    $payTotalFind->date_approve_payment = $_REQUEST['x_transaction_date'];
                    $payTotalFind->merchant_order_id = $_REQUEST['x_transaction_id'];
                    $payTotalFind->price_col = $_REQUEST['x_amount_country'];
                    $payTotalFind->currency_id = $_REQUEST['x_currency_code'];
                    $payTotalFind->preference_id = $_REQUEST['x_ref_payco'];

                    // GUARDO EL PAGO EN PAYMENTS
                    if($payTotalFind->save())
                    {
                        //Actualizo de la Publicación los días_faltantes
                        $model->dias_faltantes = $model->dias_faltantes + $payTotalFind->dias_publicados;
                        $model->fecha_ult_compra = date('Y-m-d H:i:s');
                        $model->id_plan_actual = $id_plan;
                        $model->save(false);
                        // ---------------------------------------------------
                        return Yii::$app->response->statusCode = 200;

                    }else
                        \common\components\Utils::dumpx($payTotalFind->getErrors());

                }else{
                    echo "Error: id_pub NO EXISTE O no ES UNA TRANSACCIÓN PAGA ACEPTADA ( x_cod_response!=1 ): !!";
                    return Yii::$app->response->statusCode = 400;
                }

                // -------------------------------------------------------------------
                switch ((int) $x_cod_response) {

                    case 1:
                        # code transacción aceptada
                        //echo 'transacción aceptada';
                        break;

                    case 2:
                        # code transacción rechazada
                        //echo 'transacción rechazada';
                        break;

                    case 3:
                        # code transacción pendiente
                        //echo 'transacción pendiente';
                        break;

                    case 4:
                        # code transacción fallida
                        //echo 'transacción fallida';
                        break;

                }

            } else {
                die('Firma no valida');
                return Yii::$app->response->statusCode = 400;
            }
        }else{
            echo "Error: PARÁMETOS INVÁLIDOS! NO concuerda el id_pub o id_usuario que hizo la transacción";
            return Yii::$app->response->statusCode = 400;
        }



    }



    //========================================================================================================================

    public function actionQuienessomos()
    {
        $contenido = PaginasInternas::find()->where('pagina=1')->one();
        return $this->render('quienessomos', [
            'contenido' => $contenido,
        ]);
    }

    public function actionPreguntasfrecuentes()
    {
        $contenido = PaginasInternas::find()->where('pagina=2')->one();
        return $this->render('preguntasfrecuentes', [
            'contenido' => $contenido,
        ]);
    }

    public function actionTerminosycondiciones()
    {
        $contenido = PaginasInternas::find()->where('pagina=3')->one();
        return $this->render('terminosycondiciones', [
            'contenido' => $contenido,
        ]);
    }

    public function actionPoliticadeprivacidad()
    {
        $contenido = PaginasInternas::find()->where('pagina=4')->one();
        return $this->render('politicadeprivacidad', [
            'contenido' => $contenido,
        ]);
    }

    public function actionPoliticadeprotecciondedatos()
    {
        $contenido = PaginasInternas::find()->where('pagina=5')->one();
        return $this->render('politicadeprotecciondedatos', [
            'contenido' => $contenido,
        ]);
    }

    public function actionSeguridad()
    {
        $contenido = PaginasInternas::find()->where('pagina=6')->one();
        return $this->render('seguridad', [
            'contenido' => $contenido,
        ]);
    }

    public function actionVendemasrapido()
    {
        $contenido = PaginasInternas::find()->where('pagina=7')->one();
        return $this->render('vendemasrapido', [
            'contenido' => $contenido,
        ]);
    }

    public function actionConfiguracioncuenta()
    {
        $contenido = PaginasInternas::find()->where('pagina=8')->one();
        return $this->render('configuracioncuenta', [
            'contenido' => $contenido,
        ]);
    }

    public function actionPromocionatuempresa()
    {
        $contenido = PaginasInternas::find()->where('pagina=9')->one();
        return $this->render('promocionatuempresa', [
            'contenido' => $contenido,
        ]);
    }


    public function actionBlog()
    {
        //\common\components\Utils::dumpx($_POST);
        $categorias = \common\models\ArtCategoria::find()->orderBy('nombre')->groupBy('nombre')->all();
        $nuevafecha = strtotime ( '+2 hour' , time() ) ;
        $nuevaHora = date('H:i', $nuevafecha);


        //$query = 'SELECT * FROM blog_articulos where (estado=2 or estado=1)  and fecha_publicacion <= "'.date('Y-m-d').'" and hora_publicacion <= "'.$nuevaHora.':00" ORDER BY fecha_creacion DESC';
        $query = 'SELECT * FROM art_articulo where (estado=1)   ORDER BY fecha_creacion DESC';
        //echo $query;
        $dataProvider = new SqlDataProvider([
            'sql' => $query,
            'totalCount' => count(Yii::$app->db->createCommand($query)->queryAll()),
            'pagination' => [
                'pageSize' => 6,
                //'params' =>  unserialize($campos),
            ],
        ]);


        return $this->render('blog',[
            'categorias' => $categorias,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetarticulo()
    {
        // $this->layout = '../../views/layouts/main_blog';


        //\common\components\Utils::dump($_POST);
        if(isset($_GET['url'])){
            $categorias = \common\models\ArtCategoria::find()->orderBy('nombre')->all();
            $articulo = \common\models\ArtArticulo::find()->where(['url_permanente'=>$_GET['url'],'estado'=>1])->one();
            if($articulo){
                //$tags = \common\models\BlogTagsArticulo::find()->where('id_articulo='.$articulo->id)->all();
                //$tagstr=''; foreach($tags as $t) $tagstr = $tagstr . $t->idTag->nombre.', ';



                //Pongo TAGS
                \Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => Utils::l($articulo->titulo) ]);
                \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Utils::l($articulo->contenido) ]);
                \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => 'motos, publicaciones, anuncios, vender motos, cali, colombia, medellin, vende tu moto, miles de motos, marcas, modelos' /*Utils::l($tagstr)*/ ]);
                \Yii::$app->view->registerMetaTag(['name' => 'robots', 'content' => 'index, follow' ]);
                \Yii::$app->view->registerMetaTag(['name' => 'author', 'content' => 'Vendetumoto' ]);
                \Yii::$app->view->registerLinkTag(['rel' => 'canonical', 'href' => Url::base(true).'/blog/'.$articulo->url_permanente ]);

                //Facebook
                \Yii::$app->view->registerMetaTag(['property' => 'og:locale', 'content' => 'es_ES' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => 'Vente Tu Moto' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Url::base(true).'/blog/'.$articulo->url_permanente ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => Utils::l($articulo->titulo) ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => Utils::l($articulo->contenido) ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $articulo->imagen_cabecera ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:secure_url', 'content' => $articulo->imagen_cabecera ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:width', 'content' => '795' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'og:image:height', 'content' => '495' ]);


                //Twitter
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:card', 'content' => 'summary_large_image' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:title', 'content' => Utils::l($articulo->titulo) ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:description', 'content' => ucfirst(Utils::l($articulo->contenido)) ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:url', 'content' =>  Url::base(true).'/producto/'.$articulo->url_permanente ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:site', 'content' => 'Vendetumoto' ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:image', 'content' => $articulo->imagen_cabecera ]);
                \Yii::$app->view->registerMetaTag(['property' => 'twitter:creator', 'content' => 'Vendetumoto' ]);



            }else{
                echo ('Url no existe... <a style="text-decoration:none;color:blue" href="https://vendetumoto.co/blog"> << volver al BLOG</a>'); exit;
            }


            return $this->render('blog_articulo',[
                'categorias' => $categorias,
                'model' => $articulo,


            ]);
        }else
        {echo ('Url no existe... <a style="text-decoration:none;color:blue" href="https://vendetumoto.co/blog/blog"> << volver al BLOG</a>'); exit;}

    }



}

