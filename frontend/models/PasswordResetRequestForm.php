<?php
namespace frontend\models;

use common\components\Utils;
use Yii;
use yii\base\Model;
use common\models\User;
//use webvimark\modules\UserManagement\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Error: No hay ningún usuario con esta dirección de correo electrónico!'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        $ModUser = User::find()->where('status=1 and email="'.$this->email.'"')->one();
        //Utils::dump($ModUser->attributes);
        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save(false)) {
                return false;
            }
        }


       // Utils::dumpx($user->attributes);
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom(['no-responder@vendetumoto.co' => 'Vende Tu Moto'])
            ->setTo($this->email)
            ->setSubject('Restablecimiento de contraseña para Vende Tu Moto') // . Yii::$app->name
            ->send();

        return 1;
    }
}
