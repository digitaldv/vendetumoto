<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
 


use webvimark\components\BaseController;
use webvimark\modules\UserManagement\components\UserAuthEvent;
use webvimark\modules\UserManagement\models\forms\ChangeOwnPasswordForm;
use webvimark\modules\UserManagement\models\forms\ConfirmEmailForm;
use webvimark\modules\UserManagement\models\forms\LoginForm;
use webvimark\modules\UserManagement\models\forms\PasswordRecoveryForm;
//use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use common\components\Utils;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $repeat_password;
    public $captcha;
    public $acepto_terminos;
    public $promocionales;
    public $codigo;
    public $pass;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],


            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email', 'message'=>'Debe ingresar un email válido.'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este email ya existe en VTM.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['pass', 'string', 'min' => 6],


            ['repeat_password', 'required'],
            ['repeat_password', 'string', 'min' => 6, 'message' => 'La contraseña debe tener mínimo 6 caracteres'],
            ['repeat_password', 'compare', 'compareAttribute'=>'password'],


            ['acepto_terminos', 'required', 'requiredValue' => 1, 'message' => 'Debe Aceptar términos y condiciones para continuar..']

        ];
    }

    public function attributeLabels()
    {
        return [

            'password' => 'Contraseña',
            'repeat_password' => 'Confirmar contraseña'
        ];
    }

    public function signup()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->pass = $this->pass;
        $user->setPassword($this->pass);
        $user->generateAuthKey();
        $user->email_confirmed = 1;
        $user->status = 1;


        //Utils::dumpx($user->attributes);

        if ($user->save()) {
            Yii::$app->session->setFlash('success', '¡Bienvenido a <b>Vendetumoto.co</b>! Se ha registrado con éxito.');
            return $user;
        }
        else
        {
            $this->addError('username', 'El Email ya existe en Vendetumoto.co');
        }

        return   null;

    }

    public function signup11()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateAccessToken(); //wokeer
        $user->generateEmailVerificationToken();
        return $user->save() && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
