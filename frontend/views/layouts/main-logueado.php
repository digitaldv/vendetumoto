<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;


AppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/jcabanillas/yii2-inspinia/assets');
$this->registerCssFile(Yii::$app->request->baseUrl . '/css/font-awesome.css'   );



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
    <link href="<?= Yii::$app->request->baseUrl . '/css/sidebar/style.css'?>" rel="stylesheet">
    <link href='<?= Yii::$app->request->baseUrl . '/css/font-awesome.min.css'?>' rel='stylesheet'/>
    <link href="<?= Yii::$app->request->baseUrl . '/css/animate.css'?>" rel="stylesheet">

    <link href='<?= Yii::$app->request->baseUrl . '/css/nunito.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/opensans.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/proximanova.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/monserrat.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/sourcesanspro.css' ?>' rel='stylesheet' type='text/css'>


    <?php $this->head() ?>
</head>
<body>
<style>

    .main {
        width: 50%;
        margin: 50px auto;
    }

    /* Bootstrap 3 text input with search icon */

    .has-search .form-control-feedback {
        right: initial;
        right: 0;
        color: #ccc;

    }

    .has-search .form-control {
        padding-right: 24px;
        padding-left: 10px;

    }

    .buscador::placeholder {
        color: #D9D9D9 !important;
        opacity: 1; /* Firefox */
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }

    .buscador:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #D9D9D9 !important;
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }

    .buscador::-ms-input-placeholder { /* Microsoft Edge */
        color: #D9D9D9 !important;
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }
    .nav > li > a {
        position: relative;
        display: block;
        padding: 0px 0px;
    }

    .menu_der{
        font-weight: normal;
    }


    a.list-group-item:hover, button.list-group-item:hover, a.list-group-item:focus, button.list-group-item:focus {
        color: #F33B42 !important;
        text-decoration: none;
        font-weight: bold !important;
        background-color: #f5f5f5;
    }

    .panel > .list-group .list-group-item, .panel > .panel-collapse > .list-group .list-group-item {
        border-width: 1px 0px 0px;
        border-radius: 0;
        margin-bottom: 11px;
        margin-top: 10px;

    }

    .active_menu{
        font-weight: bold !important;
        color: #F33B42 !important;
    }
    .card {
        background-color: transparent !important;
    }
    .panel {

       border: 0px solid #fff !important;
    }
</style>

<div class="wrap">
   <?php

$logo = Yii::$app->request->baseUrl .'../img/logo.png';
?>



    <div id="modal-menu" class="fade modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-md pull-right" style="margin-right: 200px;margin-top: 0px;margin-right: 0px">
            <div class="modal-content" style="border-radius: 0px;">

                <div class="modal-body">
                    <input type="hidden" id="param" name="param">
                    <div class="site-login">
                        <!-- <h1>Vendetumoto.co | ¡La tienda de motos más grande de Colombia!</h1> -->


                        <div class="panel panel-login card bg-secondary shadow border-0 " style="width: 100%;height: auto; margin: 0 auto;">
                            <div class="list-group" style="font-size: 25px">

                                <a href="#" class="  "> </a>
                                <a href="<?= Url::to(['/site/index']) ?>" class="list-group-item menu_der active_menu">
                                    <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_home2.png"  style="width:24px"> &nbsp;&nbsp;Home
                                </a>
                                <?php
                                if(!Yii::$app->user->isGuest){
                                    ?>
                                    <a href="<?= Url::to(['/site/publicaranuncio']) ?>" class="btn_login2 list-group-item menu_der">
                                        <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_pub1.png"  style="width:24px"> &nbsp;&nbsp;Publicar o Anunciar
                                    </a>

                                    <?php
                                }else{ ?>
                                    <a href="#" class="btn_login2 list-group-item menu_der">
                                        <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_pub1.png"  style="width:24px"> &nbsp;&nbsp;Publicar o Anunciar
                                    </a>
                                    <?php
                                }

                                ?>
                                <a href="<?= Url::to(['/site/blog']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_blog1.png"  style="width:24px"> &nbsp;&nbsp;Blog</a>
                                <a href="<?= Url::to(['/site/preguntasfrecuentes']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_faq1.png"  style="width:24px"> &nbsp;&nbsp;Preguntas Frecuentes</a>
                                <a href="<?= Url::to(['/site/quienessomos']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_nos1.png"  style="width:24px"> &nbsp;&nbsp;Nosotros</a>
                                <a href="<?= Url::to(['/site/contact']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_con.png"  style="width:24px"> &nbsp;&nbsp;Contáctenos</a>
                                <a href="<?= Url::to(['/site/terminosycondiciones']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_term1.png"  style="width:24px"> &nbsp;&nbsp;Términos y Condiciones</a>
                                <?php
                                if(!Yii::$app->user->isGuest){
                                    ?>
                                    <a data-method="post" href="<?= Url::to(['/site/logout']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/salir.png"  style="width:24px"> &nbsp;&nbsp;Cerrar Sesión</a>
                                    <?php
                                }

                                ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <nav id="w6" class="navbar navbar-default navbar-static-top" style="min-height: 59px;background: #fff;border-bottom: 2px solid #f33b42;border-top: 1px solid #8D8D8D !important;">
        <div class="container">
            <div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w6-collapse"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                <a class="navbar-brand" href="<?= Url::to(['/site/index']) ?>"></a>
                <a class="navbar-brand text-left" href="<?= Url::to(['/site/index']) ?>" style="float: left; margin-left: -47px; margin-top: -7px;">
                    <img src="<?= $logo ?>" height="39" class="d-inline-block align-top" alt="">
                </a>
            </div>
            <div id="w6-collapse" class="collapse navbar-collapse">
                <ul id="w7" class="navbar-nav navbar-right nav" style="margin-top: 0px">

                    <?php
                    if(!Yii::$app->user->isGuest){
                        ?>
                        <li class=" " style="margin-bottom: 0px">
                            <a href="<?=  Url::to(['/site/mispublicaciones']) ?>"  style="margin-top: 7px; margin-bottom: 0px;margin-right: 6px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/mi-cuenta.png">
                            </a>
                        </li>
                        <li class="<?= Yii::$app->user->isGuest ? 'btn_login2' : ''?> dropdown" style="margin-bottom: 0px">
                            <a href="<?= Url::to(['/site/publicaranuncio']) ?>"   aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;;margin-right: 0px">

                                <img src="<?= Yii::$app->request->baseUrl ?>/img/anuncia.png">
                            </a>
                        </li>

                        <?php
                    }else{ ?>
                        <li class="<?= Yii::$app->user->isGuest ? 'btn_login2' : Url::to(['/site/index'])?> dropdown" style="margin-bottom: 0px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;;margin-right: 0px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/anuncia.png">
                            </a>

                        </li>
                        <li class="btn_login" style="margin-bottom: 0px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 7px; margin-bottom: 0px;margin-right: 6px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/inicio_sesion.png">
                            </a>
                        </li>


                    <?php }
                    ?>
                    <li class="btn_menu" style="margin-bottom: 0px">
                        <a href="#" class="dropdown-toggle btn_menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;">
                            <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_1.png">
                        </a>
                    </li>
                </ul>

                <form class="navbar-form navbar-right" role="search" action="<?= Yii::$app->request->baseUrl ?>/site/resultados" method="get">

                    <div class="form-group has-feedback">

                        <div class="form-group has-feedback has-search" style="    margin-top: 3px;">
                            <!--a href="" style="cursor: pointer;z-index: initial" title="Buscar" > <span class="glyphicon glyphicon-search form-control-feedback"></span></a-->
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            <input type="text" id="buscador_arriba" name="buscador_arriba" value="<?= isset($_GET['buscador_arriba']) && trim($_GET['buscador_arriba'])!='' ? $_GET['buscador_arriba'] : '' ?>" class="form-control buscador" style="width: 530px;border: 1px solid #A0A0A0; font-family: Source Sans Pro;" placeholder="Buscar por tipo, marca o modelo ...">


                        </div>

                    </div>
                </form>
            </div>
        </div>
    </nav>



    <?php
/*
 *
NavBar::begin([
    'brandLabel' =>'<a class="navbar-brand text-left" href="'.Url::to(['/site/index']).'">
                        <img src="'.$logo.'"   height="31" class="d-inline-block align-top" alt="">
                    </a>',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-inverse  navbar-static-top ',
        //'style' => 'background-color: #e8c0c02b;'
    ],
]);

if (Yii::$app->user->isGuest) {
    $menuItems[] = '<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i style=" color:#F33B42; " class="glyphicon glyphicon-option-horizontal"></i>
                      </a>
                      <ul class="dropdown-menu">                                            
                         <li><a href="'.Url::to(['site/blog']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-list"></i>&nbsp; Blog</a></li>
                        <li><a href="'.Url::to(['site/preguntasfrecuentes']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-comment"></i>&nbsp; Preguntas Frecuentes</a></li>
                        <li><a href="'.Url::to(['site/quienessomos']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-heart"></i>&nbsp; ¿Quienes somos?</a></li>
                        <li><a href="#"><i style=" color:#F33B42; " class="glyphicon glyphicon-heart"></i>&nbsp; Contáctenos</a></li>

                      </ul>
                    </li>';
    $menuItems[] = ['label' => '<i style=" color:#F33B42; " class="glyphicon glyphicon-bullhorn"></i>&nbsp; Publicar Anuncio' , 'options'=>['class'=>'btn_login2']  ];
    $menuItems[] = ['label' => '<i style=" color:#F33B42; " class="glyphicon glyphicon-log-in"></i>&nbsp; Ingresar', 'options'=>['class'=>'btn_login']  ];
    // $menuItems[] = ['label' => '<i style=" color:#F33B42; " class="glyphicon glyphicon-log-in"></i>&nbsp; Registro' , 'url' => ['/site/signup']];

} else {

    $menuItems[] = '<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i style=" color:#F33B42; " class="glyphicon glyphicon-option-horizontal"></i>
                      </a>
                      <ul class="dropdown-menu">                                                    
                        <li><a href="'.Url::to(['site/blog']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-list"></i>&nbsp; Blog</a></li>
                        <li><a href="'.Url::to(['site/preguntasfrecuentes']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-comment"></i>&nbsp; Preguntas Frecuentes</a></li>
                        <li><a href="'.Url::to(['site/quienessomos']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-heart"></i>&nbsp; ¿Quienes somos?</a></li>
                        <li><a href="'.Url::to(['site/blog']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-heart"></i>&nbsp; Contáctenos</a></li>

                      </ul>
                    </li>';

    $menuItems[] = '<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i style=" color:#F33B42; " class="glyphicon glyphicon-user"></i>&nbsp; ' . Yii::$app->user->identity->username . ' <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="'.Url::to(['site/miperfil']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-user"></i>&nbsp; Mi Perfil</a></li>
                        <li><a href="'.Url::to(['/site/publicaranuncio']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-bullhorn"></i>&nbsp; Publicar Anuncio</a></li>
                                                    
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Reportes</li>                            
                        <li><a href="'.Url::to(['site/mispublicaciones']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-list"></i>&nbsp; Mis Publicaciones (3)</a></li>
                        <li><a href="'.Url::to(['site/mismensajes']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-comment"></i>&nbsp; Mis Mensajes (12)</a></li>
                        <li><a href="'.Url::to(['site/misfavoritos']).'"><i style=" color:#F33B42; " class="glyphicon glyphicon-heart"></i>&nbsp; Mis Favoritos (4)</a></li>
                        
                        
                        <li role="separator" class="divider"></li>
                        <li><a  data-method="post" href="'.Url::to(['/site/logout']).'">                                                             
                                <i style=" color:#F33B42; " class="glyphicon glyphicon-log-out"></i>&nbsp; Salir 
                             </a>
                        </li>
                      </ul>
                    </li>';
}
echo Nav::widget([
    'encodeLabels' => false,
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);

echo "<form class='navbar-form navbar-right' role='search' action='".Url::to(['/site/resultados'])."' method='get'>
           <div class='form-group has-feedback'>
                <input id='txt_buscador' name='txt_buscador' type='text' placeholder='Buscar por palabra (tipo, marca o modelo) ...' class='form-control' style='width: 430px'>
                 <button class='btn  btn-danger my-2 my-sm-0' type='submit'>Buscar</button>                    
                
            </div>
      </form>";


NavBar::end();

*/

?>
<style>

    .txt_blanco{
        color: #ffffff !important;
        font-family: Nunito;
        font-style: normal;
        font-size: 14px;
        font-weight: 500;
    }

    .txt_active{
        color: #F33B42 !important;
    }

</style>


   <div id="wrapper" class="" style="margin-top: -20px">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Menú
                    </a>
                </li>

                    <?php

                    $mpu=$pa=$mpe=$mm=$umf='';
                    if(strpos(Yii::$app->request->url, 'mispublicaciones') !== false ) $mpu = ' txt_active';
                    if(strpos(Yii::$app->request->url, 'publicaranuncio') !== false ) $pa = ' txt_active';
                    if(strpos(Yii::$app->request->url, 'miperfil') !== false ) $mpe = ' txt_active';
                    if(strpos(Yii::$app->request->url, 'mismensajes') !== false ) $mm = ' txt_active';
                    if(strpos(Yii::$app->request->url, 'misfavoritos') !== false ) $umf = ' txt_active';

                    ?>
                <li><a href="<?= Url::to(['site/mispublicaciones']) ?>" class="txt_blanco <?= $mpu ?>"><img src="<?= Yii::$app->request->baseUrl .'../img/mis-anuncios-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Mis anuncios</a></li>
                <li><a href="<?= Url::to(['site/publicaranuncio']) ?>" class="txt_blanco <?= $pa ?>"><img src="<?= Yii::$app->request->baseUrl .'../img/publicar-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Publicar anuncio</a></li>
                <li><a href="<?= Url::to(['site/miperfil']) ?>" class="txt_blanco <?= $mpe ?>"><img src="<?= Yii::$app->request->baseUrl .'../img/usuarios-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Mi Perfil</a></li>
                <li><a href="<?= Url::to(['site/mismensajes']) ?>" class="txt_blanco <?= $mm ?>"><img src="<?= Yii::$app->request->baseUrl .'../img/mensajes-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Mensajes</a></li>
                <li><a href="<?= Url::to(['site/misfavoritos']) ?>" class="txt_blanco <?= $umf ?>"><img src="<?= Yii::$app->request->baseUrl .'../img/favoritos-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Favoritos</a></li>
                <li><hr style="border: 1px solid #403d3d"></li>
                <li>
                    <a data-method="post" href="<?= Url::to(['/site/logout']) ?>" class="txt_blanco ">
                        <img src="<?= Yii::$app->request->baseUrl .'../img/cerrar-blanco.png' ?>">&nbsp;&nbsp;&nbsp;&nbsp;Cerrar sesión
                    </a>
                </li>

            </ul>
        </div>
       <div class="wrapper wrapper-content">
           <?php //echo Alert::widget() ?>
           <div class="row">
           <?= Alert::widget() ?>
           <div class="col-sm-12 col-md-12 col-lg-12 ">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       <h3 class="panel-title">
                           <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                               <i class="fa fa-bars"></i>
                           </a>
                           <?php echo Breadcrumbs::widget([
                               'options' =>['class' => 'breadcrumb'],
                               'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [''],
                           ]) ?>

                       </h3>
                   </div>
                   <div class="panel-body">
                       <?= $content ?>
                   </div>
               </div>
           </div>
           </div>
       </div>
   </div>

</div>





<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="<?= Yii::$app->request->baseUrl . '/css/sidebar/script.js'?>"></script>

<?php
$this->registerJs(" 
jQuery(document).ready(function () {
              
   $('.btn_menu').click(function(e){
    
        var modal = $('#modal-menu').modal('show');
        var that = $(this);
        
        $('.modal-header').remove();
    
        $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');
        $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');

    });

});");

?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage()  ?>

