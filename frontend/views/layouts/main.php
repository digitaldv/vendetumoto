<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
//for($i=0;$i<=20;$i++)    echo md5(time().$i).'<br>';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='<?= Yii::$app->request->baseUrl . '/css/font-awesome.min.css'?>' rel='stylesheet'/>
    <link href="<?= Yii::$app->request->baseUrl . '/css/style.css'?>" rel="stylesheet">
    <link href="<?= Yii::$app->request->baseUrl . '/css/animate.css'?>" rel="stylesheet">

    <link href='<?= Yii::$app->request->baseUrl . '/css/nunito.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/opensans.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/proximanova.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/monserrat.css' ?>' rel='stylesheet' type='text/css'>
    <link href='<?= Yii::$app->request->baseUrl . '/css/sourcesanspro.css' ?>' rel='stylesheet' type='text/css'>


    <?php $this->head() ?>
</head>
<body style="">

<?php $this->beginBody();

$logo = Yii::$app->request->baseUrl .'/img/logo.png';

?>

<style>

    .main {
        width: 50%;
        margin: 50px auto;
    }

    /* Bootstrap 3 text input with search icon */

    .has-search .form-control-feedback {
        right: initial;
        right: 0;
        color: #ccc;

    }

    .has-search .form-control {
        padding-right: 24px;
        padding-left: 10px;

    }

    #txt_email::placeholder {
        color: #D9D9D9 !important;


    }
    .buscador::placeholder {
        color: #D9D9D9 !important;
        opacity: 1; /* Firefox */
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }

    .buscador:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #D9D9D9 !important;
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }

    .buscador::-ms-input-placeholder { /* Microsoft Edge */
        color: #D9D9D9 !important;
        width: 397px;

        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        letter-spacing: 1.18397px;

    }

    .nav > li > a {
        position: relative;
        display: block;
        padding: 0px 0px;
    }

    .menu_der{
        font-weight: normal;
    }


    a.list-group-item:hover, button.list-group-item:hover, a.list-group-item:focus, button.list-group-item:focus {
        color: #F33B42 !important;
        text-decoration: none;
        font-weight: bold !important;
        background-color: #f5f5f5;
    }

    .panel > .list-group .list-group-item, .panel > .panel-collapse > .list-group .list-group-item {
        border-width: 1px 0px 0px;
        border-radius: 0;
        margin-bottom: 11px;
        margin-top: 10px;

    }

    .active_menu{
        font-weight: bold !important;
        color: #F33B42 !important;
    }

    .list-unstyled{
        font-family: Source Sans Pro;
        font-size: 11px;
        line-height: 13px;

        align-items: center;
        letter-spacing: 0.663525px;
        color: #FFFFFF;
    }
</style>
<div class="wrap">
    <?php

    $this->registerJs(" 
       
        jQuery(document).ready(function () {
          
          
           
       
           
           $('.btn_menu').click(function(e){
            
                var modal = $('#modal-menu').modal('show');
                var that = $(this);
                 
                $('.modal-header').remove();
            
                $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');
                $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');
        
            });
        
        });
 
       
   
        " );


    ?>

    <div id="modal-menu" class="fade modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-md pull-right" style="margin-right: 200px;margin-top: 0px;margin-right: 0px">
            <div class="modal-content" style="border-radius: 0px;">

                <div class="modal-body">
                    <input type="hidden" id="param" name="param">
                    <div class="site-login">
                        <!-- <h1>Vendetumoto.co | ¡La tienda de motos más grande de Colombia!</h1> -->


                        <div class="panel panel-login card bg-secondary shadow border-0 " style="width: 100%;height: auto; margin: 0 auto;">
                            <div class="list-group" style="font-size: 25px">

                                <a href="#" class="  "> </a>
                                <a href="<?= Url::to(['/site/index']) ?>" class="list-group-item menu_der active_menu">
                                    <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_home2.png" style="width:24px"> &nbsp;&nbsp;Home
                                </a>
                                <?php
                                if(!Yii::$app->user->isGuest){
                                ?>
                                <a href="<?= Url::to(['/site/publicaranuncio']) ?>" class="btn_login2 list-group-item menu_der">
                                    <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_pub1.png" style="width:24px"> &nbsp;&nbsp;Publicar o Anunciar
                                </a>

                                    <?php
                                }else{ ?>
                                    <a href="#" class="btn_login2 list-group-item menu_der">
                                        <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_pub1.png"  style="width:24px"> &nbsp;&nbsp;Publicar o Anunciar
                                    </a>
                                <?php
                                }

                                ?>
                                <a href="<?= Url::to(['/site/blog']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_blog1.png"  style="width:24px"> &nbsp;&nbsp;Blog</a>
                                <a href="<?= Url::to(['/site/preguntasfrecuentes']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_faq1.png"  style="width:24px"> &nbsp;&nbsp;Preguntas Frecuentes</a>
                                <a href="<?= Url::to(['/site/quienessomos']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_nos1.png"  style="width:24px"> &nbsp;&nbsp;Nosotros</a>
                                <a href="<?= Url::to(['/site/contact']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_con.png"  style="width:24px"> &nbsp;&nbsp;Contáctenos</a>
                                <a href="<?= Url::to(['/site/terminosycondiciones']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_term1.png"  style="width:24px"> &nbsp;&nbsp;Términos y Condiciones</a>
                                <?php
                                if(!Yii::$app->user->isGuest){
                                ?>
                                <a data-method="post" href="<?= Url::to(['/site/logout']) ?>" class="list-group-item menu_der"> <img src="<?= Yii::$app->request->baseUrl ?>/img/salir.png"  style="width:24px"> &nbsp;&nbsp;Cerrar Sesión</a>
                                <?php
                                }

                                ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <nav id="w6" class="navbar navbar-default navbar-static-top" style="min-height: 59px;background: #fff;border-bottom: 2px solid #f33b42;border-top: 1px solid #8D8D8D !important;">
        <div class="container">
            <div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w6-collapse"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                    <a class="navbar-brand" href="<?= Url::to(['/site/index']) ?>"></a>
                    <a class="navbar-brand text-left" href="<?= Url::to(['/site/index']) ?>" style="float: left; margin-left: -47px; margin-top: -7px;">
                        <img src="<?= $logo ?>" height="39" class="d-inline-block align-top" alt="">
                    </a>
            </div>
            <div id="w6-collapse" class="collapse navbar-collapse">
                <ul id="w7" class="navbar-nav navbar-right nav" style="margin-top: 0px">

                    <?php
                    if(!Yii::$app->user->isGuest){
                    ?>
                        <li class=" " style="margin-bottom: 0px">
                            <a href="<?=  Url::to(['/site/mispublicaciones']) ?>"  style="margin-top: 7px; margin-bottom: 0px;margin-right: 10px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/mi-cuenta.png">
                            </a>
                        </li>
                        <li class="<?= Yii::$app->user->isGuest ? 'btn_login2' : ''?> dropdown" style="margin-bottom: 0px">
                            <a href="<?= Url::to(['/site/publicaranuncio']) ?>"   aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;;margin-right: 0px">

                                <img src="<?= Yii::$app->request->baseUrl ?>/img/anuncia.png">
                            </a>
                        </li>

                    <?php
                    }else{ ?>
                        <li class="btn_login" style="margin-bottom: 0px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 7px; margin-bottom: 0px;margin-right: 10px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/inicio_sesion.png">
                            </a>
                        </li>
                        <li class="<?= Yii::$app->user->isGuest ? 'btn_login2' : Url::to(['/site/index'])?> dropdown" style="margin-bottom: 0px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;;margin-right: 0px">
                                <img src="<?= Yii::$app->request->baseUrl ?>/img/anuncia.png">
                            </a>

                        </li>

                    <?php }
                    ?>
                    <li class="btn_menu" style="margin-bottom: 0px">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 0px; margin-bottom: 0px;">
                            <img src="<?= Yii::$app->request->baseUrl ?>/img/menu_1.png">
                        </a>
                    </li>
                </ul>

                <form class="navbar-form navbar-right" role="search" action="<?= Yii::$app->request->baseUrl ?>/site/resultados" method="get">

                    <div class="form-group has-feedback">

                        <div class="form-group has-feedback has-search" style="    margin-top: 3px;">
                            <!--a href="" style="cursor: pointer;z-index: initial" title="Buscar" > <span class="glyphicon glyphicon-search form-control-feedback"></span></a-->
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            <input type="text" id="buscador_arriba" name="buscador_arriba" value="<?= isset($_GET['buscador_arriba']) && trim($_GET['buscador_arriba'])!='' ? $_GET['buscador_arriba'] : '' ?>" class="form-control buscador" style="width: 530px;border: 1px solid #A0A0A0; font-family: Source Sans Pro; " placeholder="Buscar por tipo, marca o modelo ...">

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </nav>



    <div class="container">
        <?php /*echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [''],
        ]) */?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

</div>


<!-- SUSCRIPCIONES -->
<center style="background: #ffffff">
    <div class="container">
        <hr class="hr-separador">
        <div class="row   " style="margin-top: 40px;background: #ffffff">
            <div class="col-lg-3 col-md-3 col-sm-3  text-left">
                        <span class="txt-susc-1">
                            Suscríbete a nuestro Newsletter
                        </span>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12  text-right">
                <div class="">
                    <div class="form-group  row">
                        <table style="width: 100%">

                            <tr>
                                <td style="padding-right: 10px">
                                    <input id="txt_email" type="email" class="form-control" placeholder="Correo electrónico"></td>
                                <td style="width: 1%">
                                    <button class="btn  btn-danger btn_suscribirme" style="background:  #F33B42;border-radius: 0px" type="button">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suscríbete&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left;padding-top: 8px">
                                    <div class="form-check abc-checkbox form-check-inline m-l-md">
                                        <input id="check_suscrib" name="check_suscrib" class="form-check-input" type="checkbox" value="1">
                                        <label class="form-check-label" style="font-weight: normal;color:#7D7D7D"> Declaro que he leído y acepto la
                                            <a style="color: #e46a76" target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>">Política de Privacidad</a> y los
                                            <a style="color: #e46a76" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de manejo de datos de VENDETUMOTO.CO
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <hr class="hr-separador">
    </div>
</center>



<footer class="nb-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="footer-info-single">
                            <h2 class="title">CENTRO DE AYUDA</h2>
                            <hr style="border: 1px solid #ffffff;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class="pull-left">
                            <ul class="list-unstyled" style="clear: both">
                                <li><a href="<?= Url::to(['site/comopublicarunanuncio']) ?>" title=""> ¿Cómo publicar un anuncio?</a></li>
                                <li><a href="<?= Url::to(['site/preguntasfrecuentes']) ?>" title=""> Preguntas Frecuentes</a></li>
                                <li><a href="#" title=""> &nbsp;</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="footer-info-single">
                            <h2 class="title">INFORMACIÓN AL CLIENTE</h2>
                            <hr style="border: 1px solid #ffffff;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class="pull-left">
                            <ul class="list-unstyled" style="clear: both">
                                <li><a href="<?= Url::to(['site/quienessomos']) ?>" title=""> ¿Quienes somos?</a></li>
                                <li><a href="<?= Url::to(['site/preguntasfrecuentes']) ?>" title=""> Preguntas Frecuentes</a></li>
                                <li><a href="<?= Url::to(['site/vendemasrapido']) ?>" title=""> Vende más rápido</a></li>
                                <li><a href="<?= Url::to(['site/contactenos']) ?>" title=""> Contáctenos</a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="footer-info-single">
                            <h2 class="title">SEGURIDAD & PRIVACIDAD</h2>
                            <hr style="border: 1px solid #ffffff;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class="pull-left">
                            <ul class="list-unstyled" style="clear: both">
                                <li><a href="<?= Url::to(['site/terminosycondiciones']) ?>" title=""> Términos y condiciones</a></li>
                                <li><a href="<?= Url::to(['site/politicadeprotecciondedatos']) ?>" title=""> Política de protección de datos</a></li>
                                <li><a href="<?= Url::to(['site/seguridad']) ?>" title=""> Seguridad</a></li>
                                <li><a href="<?= Url::to(['site/politicadeprivacidad']) ?>" title=""> Política de privacidad</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <div class="footer-info-single">
                            <h2 class="title" style="color: #ffffff">SERVICIOS

                            </h2>
                            <hr style="border: 1px solid #ffffff;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class="pull-left">
                            <ul class="list-unstyled" style="clear: both">
                                <li><a href="<?= Url::to(['site/terminosycondiciones']) ?>" title=""> Términos y condiciones</a></li>
                                <li><a href="<?= Url::to(['site/politicadeprotecciondedatos']) ?>" title=""> Política de protección de datos</a></li>
                                <li><a href="<?= Url::to(['site/seguridad']) ?>" title=""> Seguridad</a></li>
                                <li><a href="<?= Url::to(['site/politicadeprivacidad']) ?>" title=""> Política de privacidad</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1" style="text-align: center"><img style="height: 220px" src="<?= Yii::$app->request->baseUrl ?>/img/linea_blanca.png"></div>
            <div class="col-lg-3 col-md-3 col-sm-3" style=" ">
                <div class="about">
                    <img src="<?= $logo ?>"  class="img-responsive center-block" alt="">
                    <p style="font-family: Source Sans Pro;
                       font-size: 13px;
                       line-height: 15px;


                    align-items: center;
                    text-align: center;
                    letter-spacing: 0.784165px;
                    text-transform: uppercase;

                    color: #FFFFFF;">¡Síguenos!</p>

                    <div class="social-media">
                        <ul class="list-inline">
                            <li><a target="_blank" href="https://www.facebook.com/vendetumoto.co" title=""><i class="fa fa-facebook text-white"></i></a></li>
                            <li><a target="_blank"  href="https://www.instagram.com/vendetumoto.co/" title=""><i class="fa fa-instagram text-white"></i></a></li>
                            <li><a target="_blank"  href="https://www.youtube.com/channel/UC790Oj7fBwTjK00Z7Y8-yYA" title=""><i class="fa fa-youtube text-white"></i></a></li>
                            <!--li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>Copyright © 2021. Vendetumoto.co</p>
                </div>
                <div class="col-sm-6 pull-right text-right">Escríbenos a <b>soporte@vendetumoto.co</b></div>
            </div>
        </div>
    </section>
</footer>

<div class="si-sticky visible-md visible-lg ocultar">
    <a target="_blank" href="https://www.instagram.com/vendetumoto.co/"  class="social-icon si-colored si-google " title="Síguenos en Instagram"  >
        <!--svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-quote texto-blanco" viewBox="0 0 16 16"><path d="M12 12a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1h-1.388c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 9 7.558V11a1 1 0 0 0 1 1h2Zm-6 0a1 1 0 0 0 1-1V8.558a1 1 0 0 0-1-1H4.612c0-.351.021-.703.062-1.054.062-.372.166-.703.31-.992.145-.29.331-.517.559-.683.227-.186.516-.279.868-.279V3c-.579 0-1.085.124-1.52.372a3.322 3.322 0 0 0-1.085.992 4.92 4.92 0 0 0-.62 1.458A7.712 7.712 0 0 0 3 7.558V11a1 1 0 0 0 1 1h2Z"></path></svg-->
        <svg width="16" height="16" fill="currentColor" class="instageeram-logo  texto-blanco" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 551.034 551.034" style=" color: #ffffff" xml:space="preserve">
		    <path  class="logo" id="XMLID_17_" d="M386.878,0H164.156C73.64,0,0,73.64,0,164.156v222.722 c0,90.516,73.64,164.156,164.156,164.156h222.722c90.516,0,164.156-73.64,164.156-164.156V164.156 C551.033,73.64,477.393,0,386.878,0z M495.6,386.878c0,60.045-48.677,108.722-108.722,108.722H164.156 c-60.045,0-108.722-48.677-108.722-108.722V164.156c0-60.046,48.677-108.722,108.722-108.722h222.722 c60.045,0,108.722,48.676,108.722,108.722L495.6,386.878L495.6,386.878z"/>
            <path id="XMLID_81_"   d="M275.517,133C196.933,133,133,196.933,133,275.516 s63.933,142.517,142.517,142.517S418.034,354.1,418.034,275.516S354.101,133,275.517,133z M275.517,362.6 c-48.095,0-87.083-38.988-87.083-87.083s38.989-87.083,87.083-87.083c48.095,0,87.083,38.988,87.083,87.083 C362.6,323.611,323.611,362.6,275.517,362.6z"/>
        </svg>
    </a>
    <a target="_blank" href="https://www.facebook.com/vendetumoto.co" class="social-icon si-colored si-facebook" title="Síguenos en Facebook" >
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook texto-blanco" viewBox="0 0 16 16">
            <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"></path>
        </svg>
    </a>
    <a href="https://api.whatsapp.com/send?phone=573057508454&text=Deseo comunicarme con un agente de Soporte de Vende Tu Moto" title="Escríbenos por WhatsApp" target="_blank" data-action="share/whatsapp/share" class="social-icon si-colored si-whatsapp" data-text="Colocar div CSS Posición absoluta, relativa, fija y flotante" data-href="https://disenowebakus.net/posicion-de-los-cuadros-box-en-css.php">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-whatsapp texto-blanco" viewBox="0 0 16 16"><path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"></path></svg>
    </a>

</div>


<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage(); ?>

<?php

?>
