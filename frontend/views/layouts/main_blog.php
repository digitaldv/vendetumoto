<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;

AppAsset::register($this);
 


    if (class_exists('frontend\assets\AppAsset')) {   frontend\assets\AppAsset::register($this);
    } else {   app\assets\AppAsset::register($this);    }



    // Propios Frontend *****************************************
    $this->registerJsFile(Yii::$app->request->baseUrl . '/js/scripts.js', ['depends' => [JqueryAsset::className()]], View::POS_END);
    $this->registerCssFile(Yii::$app->request->baseUrl . '/css/estilos.css');
    $this->registerCssFile(Yii::$app->request->baseUrl . '/css/blog.css');
    $this->registerCssFile(Yii::$app->request->baseUrl . '/css/blog2.css');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="google-site-verification" content="WHlTweCO4moDxEGQCxNN-gDRAaADV-R5jlv1rVlQKaA" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body>
            <?php $this->beginBody() ?>
            <?= $this->render('header'); ?>
            <section class="content3 ancho_blog" style="margin-top:60px" >
                <div class="container">
                    <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
                    <?= Alert::widget() ?>
                </div>
                <?= $content ?>
            </section>

            <?= $this->render('footer'); ?>

            <?php $this->endBody() ?>
        </body>
    </html>
    <?php
    $this->endPage();

?>
