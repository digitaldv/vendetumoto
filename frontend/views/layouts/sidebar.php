<?php

use yii\helpers\Url;
use common\components\Utils;

$rol = Utils::getRol(Yii::$app->user->identity->id);


?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <?php

                if($rol=="ADMIN_VTM" || $rol=="SUPERADMIN"  ) {    ?>

                    <li class="<?= (Yii::$app->controller->id == 'cliconsulta') ? ' active' : '' ?>">
                        <a href="<?= Url::to(['/site/index'])?>">
                            <i class="fa fa-home"></i> <span class="nav-label"> Dashboard</span>
                        </a>
                    </li>
                    <li class="<?= (Yii::$app->controller->id == 'country' || Yii::$app->controller->id == 'state' || Yii::$app->controller->id == 'city') ? ' active' : ''   ?>">
                        <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Ubicación</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse" aria-eanded="false">
                            <li class="<?= (Yii::$app->controller->id == 'country') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/country/index'])?>">
                                    <i class="fa fa-angle-double-right"></i> Paises</a>
                            </li>
                        </ul>
                        <ul class="nav nav-second-level collapse" aria-eanded="false">
                            <li class="<?= (Yii::$app->controller->id == 'state') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/state/index'])?>">
                                    <i class="fa fa-angle-double-right"></i> Departamentos</a>
                            </li>
                        </ul>
                        <ul class="nav nav-second-level collapse" aria-eanded="false">
                            <li class="<?= (Yii::$app->controller->id == 'city') ? ' active' : '' ?>"><a href="<?= Url::to(['/configuracion/city/index'])?>">
                                    <i class="fa fa-angle-double-right"></i> Ciudades</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?= (Yii::$app->controller->id == 'empleado') ? ' active' : '' ?>">
                        <a href="<?= Url::to(['/configuracion/empleado/index'])?>">
                            <i class="fa fa-user-circle-o"></i> <span class="nav-label"> Empleados VTM</span>
                        </a>
                    </li>
                    <li class="<?= (Yii::$app->controller->id == 'paquete') ? ' active' : '' ?>">
                        <a href="<?= Url::to(['/configuracion/paquete/index'])?>">
                            <i class="fa fa-dollar"></i> <span class="nav-label"> Paquetes de Anuncios</span>
                        </a>

                    </li>

                    <?php
                } elseif($rol=="CLI_ADMIN") {
                    echo "----";

                }

            ?>
        </ul>
    </div>
</nav>    

