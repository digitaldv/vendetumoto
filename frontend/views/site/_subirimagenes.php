<?php
use common\models\PubMultimedia;
use common\models\PubPublicacion;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\web\View;


$this->registerJs(" 
 
   ",View::POS_END);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Upload Images</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl . '/dropzone/dropzone.css'?>" type="text/css">
</head>
<body>
<div class="row" style=" margin-right: 0px;  margin-left: 0px;">
<div class="col-sm-12 col-md-12 col-lg-12  " style="padding: 10px;" >

    <span style="display: flex;justify-content: center;color:red"> Sólo se permiten 10 imágenes por publicación.</span>
    <div class="dropzone dz-clickable" id="myDrop">

        <div class="dz-default dz-message" data-dz-message="">
            <span>Drop files here to upload</span>
        </div>
    </div>

</div>

<!--div class="col-sm-12 col-md-12 col-lg-12 pull-right  " style="width: 100%">
    <input type="button" id="add_file" value="Guardar Imágenes" class="btn btn-primary mt-3 pull-right">
</div-->
</div>

<input type="hidden" id="id_pub" value="<?= isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '' ?>">

<div class="row" style=" margin-right: 0px;  margin-left: 0px;">
    <div class="col-sm-12 col-md-12 col-lg-12  "   >
    <div id="msg" class="mb-3"></div>
    <a href="javascript:void(0);" class="btn btn-outline-primary reorder" id="updateReorder">Reordenar Imágenes</a>
    <div id="reorder-msg" class="alert alert-warning mt-3" style="display:none;">
        <i class="fa fa-3x fa-exclamation-triangle float-right"></i> 1. Arrastra las fotos para reordenarlas.<br>2. Haz clic en 'Guardar reordenación' cuando termines.
    </div>
    <div class="gallery">
        <ul class="nav nav-pills">

            <?php
            $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . (isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : ''))->orderBy('img_order asc')->all();
            if($fotos) {
                $model = PubPublicacion::find()->where('id='.$_GET['id'].' and id_usuario=' . Yii::$app->user->identity->id)->one();
                $output = '<div class="gallery">
                           
                            <ul class="nav nav-pills">';
                                foreach ($fotos as $foto) {
                                    $output .= '<li id="image_li_'.$foto->id.'" class="ui-sortable-handle mr-2 mt-2"  id_foto="'.$foto->id.'" >
                                                      <div>
                                                      <a href="javascript:void(0);" class="img-link">
                                                         <img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" alt="" class="img-thumbnail" width="100">
                                                      </a>
                                                      <button type="button" class="btn btn-sm btn-link remove_image text-danger rm'.$foto->id.'" style="color:red;display:flex; justify-content: flex-start;" id_pub="'.$model->id.'" id_foto="' .  $foto->id. '">Eliminar</button>
                                                      </div>
                                                </li>
                                ';
                }
                $output .= ' </ul>
        </div>';
                echo $output;
            }

            ?>
        </ul>
    </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="<?= Yii::$app->request->baseUrl . '/dropzone/dropzone.js'?>"></script>
<script>
    $(document).ready(function(){
        $(document).on('click', '.remove_image', function(e)
        {
            e.preventDefault();
            if(confirm('¿Confirma eliminar la foto?'))
            {
                    $.post('<?= Url::to(['site/eliminarfoto']) ?>', {id_pub:$(this).attr('id_pub'), id_foto:$(this).attr('id_foto') },
                        function(res_sub) {
                                var res = jQuery.parseJSON(res_sub);
                                //console.log(res); return false;
                                if(res.res)
                                {
                                    //******************************************
                                    $('#image_li_'+res.id_foto).remove();
                                    $('.rm'+res.id_foto).remove();
                                    //validar que NO pueda eliminar la foto, si esta es la única.


                                    //alert('¡Foto eliminada con éxito!');
                                    //******************************************
                                }else{
                                    alert('ERROR ------');
                                }
                            }
                    );
            }else return false;

        });



        $('.reorder').on('click',function(){
            $("ul.nav").sortable({ tolerance: 'pointer' });
            $('.reorder').html('Guardar Reordenación');
            $('.reorder').attr("id","updateReorder");
            $('#reorder-msg').slideDown('');
            $('.img-link').attr("href","javascript:;");
            $('.img-link').css("cursor","move");
            $("#updateReorder").click(function( e ){
                if(!$("#updateReorder i").length){
                    $(this).html('').prepend('<i class="fa fa-spin fa-spinner"></i>');
                    $("ul.nav").sortable('destroy');
                    $("#reorder-msg").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                    var h = [];
                    $("ul.nav li").each(function() {  h.push($(this).attr('id').substr(9));  });

                    $.ajax({
                        type: "POST",
                        url: "<?= Url::to(['site/updateimagespub','id_pub'=>$id, 'op'=>2]) ?>",
                        data: {ids: " " + h + ""},
                        success: function(data){
                           // console.log(data);
                            if(data==1 || parseInt(data)==1){
                                alert('¡Se ha actualizado el orden de las fotos satisfactoriamente!')
                                window.location.reload();
                            }
                        }
                    });
                    return false;
                }
                e.preventDefault();
            });
        });

        $(function() {
            $("#myDrop").sortable({
                items: '.dz-preview',
                cursor: 'move',
                opacity: 0.5,
                containment: '#myDrop',
                distance: 10,
                tolerance: 'pointer',
            });

            $("#myDrop").disableSelection();
        });

        //Dropzone script
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#myDrop",
            {
                maxFiles: 10,
                paramName: "files", // The name that will be used to transfer the file
                addRemoveLinks: false,
                uploadMultiple: true,
                autoProcessQueue: true,
                parallelUploads: 10,
                maxFilesize: 5,
                acceptedFiles: ".png, .jpeg, .jpg, .gif, .jfif, .PNG, .JPEG, .JPG, .GIF, .JFIF",
                url: '<?= Url::to(['site/updateimagespub','id_pub'=>$id, 'op'=>1]) ?>',
                dictDefaultMessage: 'Haz clic aquí y adjunta un máximo 10 fotos.',
            });

        myDropzone.on("sending", function(file, xhr, formData) {
            var filenames = [];

            $('.dz-preview .dz-filename').each(function() {
                filenames.push($(this).find('span').text());
            });

            formData.append('filenames', filenames);
        });

        /* Add Files Script*/
        myDropzone.on("success", function(file, message){

           // $("#msg").html('<div class="alert alert-danger">'+message+'</div>');
            parent.location.reload();

        });



        myDropzone.on("error", function (data) {
            $("#msg").html('<div class="alert alert-danger">There is some thing wrong, Please try again!</div>'+data);
        });

        myDropzone.on("complete", function(file) {
            myDropzone.removeFile(file);

        });

        $("#add_file").on("click",function (){
            myDropzone.processQueue();

        });

    });
</script>

</body>
</html>
