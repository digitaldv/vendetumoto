<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;


$this->title = 'BLOG - VENDETUMOTO.CO';

$this->registerJs(" 
    jQuery(document).ready(function () {
     
     $('.btn_login').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            
            $('.modal-header').remove();                 
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');      
            
         
    });
    $('.btn_login2').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            
            $('.modal-header').remove();
            $('#modal-menu').modal('hide');  
                        
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
     $('.btn_menu').click(function(e){ 
       
            var modal = $('#modal-menu').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
         
           
    });");
?>


<div class="site-about" >

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <img src="<?= Yii::$app->request->baseUrl . '/img/motos-fondo.png'?>" class="img-fluid img-responsive" alt="Responsive image">
        </div>
    </div>
   <hr>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-lg-12">

                                          <?=
                                            ListView::widget([
                                                'dataProvider' => $dataProvider,
                                                'options' => [
                                                    'tag' => 'div',
                                                    'class' => 'list-wrapper',
                                                    'id' => 'list-wrapper',
                                                ],
                                                //'layout' => "{pager}{summary}\n{items}\n\n<div class='row'></div> ",
                                                'layout' => '<div class="row">
                                                                 {items}   
                                                             </div>
                                                             <div class="row">
                                                                 <div class="col-xs-12 col-lg-10 pull-right t_r" >
                                                                    {pager}
                                                                 </div>
                                                             </div>
                                                            ',

                                                'itemView' => function ($model, $key, $index, $widget) {
                                                    return $this->render('_mini_articulo',['arti' => $model]);
                                                },

                                                'pager' => [
                                                    'firstPageLabel' => 'Primera',
                                                    'lastPageLabel' => 'Última',
                                                    'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left h_12"  ></span>',
                                                    'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right h_12" ></span>',
                                                    'maxButtonCount' => 10,
                                                ],
                                            ]);
                                            ?>


    </div>
        <div class="col-lg-3 col-md-3 col-lg-12">
            <div id="sidebar" role="complementary" class="sidebar fusion-widget-area fusion-content-widget-area fusion-blogsidebar f_r">
                <div id="categories-3" class="widget widget_categories">
                    <div class="heading">
                        <h4 class="widget-title col_1" data-fontsize="15" data-lineheight="21" >Categoría</h4></div>
                    <ul>
                        <?php
                        foreach($categorias as $cat){
                            ?>
                            <span style="color: #e46a76"> <i class="fa fa-angle-double-right"></i> <?= $cat->nombre ?> </span>
                        <?php } ?>
                    </ul>
                </div>

                <div id="tag_cloud-2" class="widget widget_tag_cloud"><div class="heading">
                        <h4 class="widget-title col_1" data-fontsize="15" data-lineheight="21" >Tags</h4></div>
                    <div class="tagcloud">
                        <?php

                        $tags = \common\models\ArtArticulo::find()->all();
                        $tagFin=[];
                        foreach($tags as $t){
                            $tag_vec = explode(',',$t->tags);
                            for($i=0;$i<count($tag_vec);$i++){
                                $tagFin[$tag_vec[$i]] = $tag_vec[$i];
                            }
                        }
                        foreach ($tagFin as $tg){
                            echo '<span class="flex-column    " style="    color: #e46a76;    background: #f5f5f5;    border: 1px solid #ccc;    border-radius: 4px;    cursor: default;    float: left;    margin: 5px 0 0 6px;    padding: 0 6px;">
                                    '.$tg.'
                                  </span>';
                        }


                        /*


                        //\common\components\Utils::dump($tags);
                        foreach($tags as $t){
                            $tags = \common\models\BlogTagsArticulo::find()->where('id_tag='.$t->id)->all();
                            if($tags){ ?>
                                 <a href="<?= Url::base(true); ?>/blog/tag/<?= common\components\Utils::urls_amigables($t->nombre) ?>" class="tag-link-23 tag-link-position-1" style="color: #0747A6" title="<?= $t->nombre ?>"><?= $t->nombre ?></a>
                        <?php }}
                        */ ?>
                    </div>
                </div>

                <!--div id="facebook-like-widget-2" class="widget facebook_like">
                    <div class="heading">
                        <h4 class="widget-title col_1" data-fontsize="15" data-lineheight="21" >Facebook</h4>
                    </div>
                    <div class="over_">
                        <div class="footer-widget twitter-widget"  >
                            <div class="fb-page" data-href="https://www.facebook.com/gestionemoscom-849616428580555/" data-tabs="timeline" data-width="262"  data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/gestionemoscom-122768544995733/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gestionemoscom-122768544995733/">gestionemos.com</a></blockquote></div>
                        </div>

                    </div>
                </div>
                <div id="tweets-widget-15" class="widget tweets">
                    <div class="heading">
                        <h4 class="widget-title col_1" data-fontsize="15" data-lineheight="21" >Twitter</h4>
                    </div>
                    <div class="over_">
                        <div class="footer-widget twitter-widget min_h" >
                            <a class="twitter-timeline" data-lang="es" data-width="320" data-height="262" data-theme="light" href="https://twitter.com/gestionemoscom">Tweets by gestionemoscom</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div-->

            </div>
        </div>

</div>


<!--

<div class="container">
    <h1><b><?= Html::encode($this->title) ?></b></h1>
    <hr>
<?= Html::a('Articulo # 1', ['site/getarticulo', 'url' => 'hola-mundo-feliz']) ?> <br><br><br>
<?= Html::a('Articulo # 2', ['site/getarticulo', 'url' => 'hola-mundo-feliz-dos']) ?> <br>
<?= Html::a('Articulo # 3', ['site/getarticulo', 'url' => 'hola-mundo-feliz-tres']) ?> <br>
<?= Html::a('Articulo # 4', ['site/getarticulo', 'url' => 'hola-mundo-feliz-cuatro']) ?> <br>

    <br>

</div>-->


    <style>

        .btn-facebook {
            border: 1px solid #D9D9D9;
            border-radius: 15px;
            padding: 8px;
            color: #969696;font-size: 20px;
        }
        .btn-google {
            /* color: #fff;
            background-color: #dd4b39;
            border-color: rgba(0, 0, 0, 0.2); */
            border: 1px solid #D9D9D9;
            border-radius: 15px;
            color: #969696;
            font-size: 20px;
        }

        .btn-login {
            color: #fff;
            background-color: #F33B42;
            border-color: #F33B42;
        }
        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px #fff;
            box-shadow: 0 1px 1px #fff;
        }
        .panel-heading .box {
            padding: 18px 0 6px;
            text-align: center;
        }

        .panel-heading .line {
            margin-top: 4px;
            margin-bottom: 20px;
            border: 0;
            width: 100%;
            border-top: 1px solid #c5c5c54d;

        }

        .panel-heading .active .line {
            border-top: 3px solid #F33B42;

        }


        .panel-heading .text {
            color: #000;
            font-weight: bold;
        }

        .panel-heading .text2{
            color: #000;
            font-weight: normal;
        }

        .panel-heading .text, .text2:hover,
        .panel-heading .text, .text2:focus {
            text-decoration: none;
        }

        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            border: 1px solid #fff;
            border-radius: .375rem;
            background-color: #fff;
            background-clip: border-box;
        }

        .card-body {
            padding: 1.5rem;
            flex: 1 1 auto;
        }

        .card-header {
            margin-bottom: 0;
            padding: 1.25rem 1.5rem;
            border-bottom: 1px solid rgba(0, 0, 0, .05);
            background-color: #fff;
        }

        .card-header:first-child {
            border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
        }

        .shadow {
            /* box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;*/
        }
        #signupform-acepto_terminos {
            display: block;
        }

        .tambien{
            font-family: Source Sans Pro;
            font-style: normal;
            font-weight: 600;
            font-size: 14px;
            line-height: 18px;

            letter-spacing: 0.0278198px;

            color: #969696;
        }

        .titulos_form{
            font-family: Source Sans Pro;
            font-size: 12px;
            line-height: 14px;
            display: flex;
            align-items: center;
            letter-spacing: 0.0238455px;
            font-weight: normal;
            color: #F33B42 !important;
        }
        .help-block{
            font-family: Source Sans Pro;
            font-style: normal;
            font-weight: normal;
            font-size: 11px;
            line-height: 14px;
            /* identical to box height */


            text-align: right;
            letter-spacing: 0.0218584px;

            color: #353435 !important;
        }

        .rosado_aref{
            font-family: Source Sans Pro;
            font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
            color: #FF596A;
        }

        .negro_aref{
            font-family: Source Sans Pro;
            font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
            color: #353435;
        }

        .normal_text{
            font-family: Source Sans Pro;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 18px;
            letter-spacing: 0.0278198px;

            color: #353435;
        }

    </style>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <?php
    use yii\widgets\ActiveForm;
    use common\models\LoginForm;
    use frontend\models\SignupForm;
    use common\models\CliUsuario;
    use yii\authclient\widgets\AuthChoice;

    \yii\bootstrap\Modal::begin([
        'header' => '<h4 class="text-danger  modal-title">Debes iniciar sesión para realizar la acción</h4>',
        'id'     => 'modal-login',
        'size' => 'modal-lg',
        'clientOptions' => [

            //'backdrop' => 'static',
            'keyboard' => false],
        'footer' => null,
    ]); ?>
    <input type="hidden" id="param" name="param">
    <?php

    $model = new LoginForm();
    $modelSignup = new SignupForm();
    $modelCliU = new CliUsuario();
    /*
    echo $this->render('loginpopup', [
        'model' => $model,'modelSignup' => $modelSignup,'modelCliU' => $modelCliU,
    ])
    */
    ?>
    <div class="site-login">
        <!-- <h1><?= Html::encode($this->title) ?></h1> -->


        <div class="panel panel-login card bg-secondary shadow border-0" style="width: 80%;height: auto;
    margin: 0 auto;">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6 box active" id="login-form-link">
                        <a href="#" class="text txt_is">INICIAR SESIÓN</a>
                        <hr class="line">
                    </div>
                    <div class="col-xs-6 box" id="register-form-link">
                        <a href="#" class="text2 txt_reg" style="">REGISTRATE</a>
                        <hr class="line">
                    </div>
                </div>
                <!-- <hr> -->
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- ############################################### -->
                        <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=>false,'method'=>'post','action'=>\yii\helpers\Url::to(['site/login'])]); ?>

                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                            </div>
                            <!-- <?= $form->field($model, 'rememberMe')->checkbox() ?> -->
                        </div>
                        <div class="col-lg-12">
                            <div class="pull-right" style="padding-right: 10px; margin-bottom: 15px"><?= Html::a('<span class="titulos_form">Olvidé mi contraseña</span>', ['site/request-password-reset']) ?></div>
                        </div>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#F33B42;font-size:11px', 'name' => 'login-button']) ?>
                                </div>
                            </div>
                        </div>



                        <!-- start auth facebook y google -->
                        <?php
                        $authAuthChoice = AuthChoice::begin([
                            'baseAuthUrl' => ['auth'],
                            'options' => ['class' => 'social-auth-links text-center'],
                        ])
                        ?>
                        <p class="tambien" style="font-weight: normal">También puedes ingresar con</p>
                        <hr style="width: 70%;text-align: center">
                        <div class="social-button-login">
                            <div class="row">
                                <div class="" style="display: flex; justify-content: center">
                                    <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                        <div class="col-sm-3 col-sm-offset-3" style="margin: 0.5em;color:#969696">
                                            <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                            <?php $class = $name === 'live' ? 'windows' : $name ?>
                                            <?php $text = $class === 'facebook' ?
                                                sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                                sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                            ?>
                                            <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <?php AuthChoice::end() ?>
                        <!-- end auth facebook y google -->
                        <?php ActiveForm::end(); ?>

                        <!-- ############################################### -->
                        <?php $form = ActiveForm::begin(['id' => 'register-form',  'options' => ['style' => 'display: none;']]); ?>

                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <?= $form->field($modelCliU, 'nombre')->label( '<span class="titulos_form">Nombre completo</span>')?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($modelSignup, 'email')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($modelCliU, 'telefonos')->label('<span class="titulos_form">Número de contacto</span>')?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($modelCliU, 'whatssapp')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de WhatsApp</span>')?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($modelSignup, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('<span class="titulos_form">Confirmación de contraseña</span>') ?>
                            </div>
                        </div>
                        <p class="tambien" style="margin-top: 10px">&nbsp;</p>
                        <!-- start auth facebook y google -->
                        <?php
                        $authAuthChoice = AuthChoice::begin([
                            'baseAuthUrl' => ['auth'],
                            'options' => ['class' => 'social-auth-links text-center'],
                        ])
                        ?>


                        <div class="social-button-login">
                            <p class="tambien" style="margin-top: 10px">También puedes ingresar con</p>
                            <hr style="    margin-top: 0px;    margin-bottom: 9px;    border: 0;    border-top: 1px solid #eeeeee;">
                            <div class="row">

                                <div class="" style="display: flex; justify-content: center">
                                    <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                        <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                            <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                            <?php $class = $name === 'live' ? 'windows' : $name ?>
                                            <?php $text = $class === 'facebook' ?
                                                sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                                sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                            ?>
                                            <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <?php AuthChoice::end() ?>
                        <!-- end auth facebook y google -->
                        <div class="row">
                            <div class="form-group" style="margin-bottom: 30px;">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">


                                        <table class="table" style="margin-top: 20px">
                                            <tr>
                                                <td style="border: 0px">
                                                    <input type="checkbox" id="signupform-promocionales" name="SignupForm[promocionales]" value="1" aria-required="true">
                                                </td>
                                                <td class="normal_text" style="border: 0px">
                                                    Me gustaría recibir comunicaciones promocionales (Recibirás un e-mail de confirmación)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 0px">
                                                    <input type="checkbox" id="signupform-acepto_terminos" name="SignupForm[acepto_terminos]" value="1" aria-required="true">
                                                </td>
                                                <td style="border: 0px" class="normal_text">Declaro que he leído y acepto la nueva <a target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>" class="rosado_aref" style="">Política de privacidad</a> y los
                                                    <a class="rosado_aref" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de VENDETUMOTO.CO
                                                </td>
                                            </tr>
                                        </table>


                                        <?= $form->field($modelSignup, 'promocionales')->hiddenInput()->label(false) ?>
                                        <?= $form->field($modelSignup, 'acepto_terminos')->hiddenInput()->label(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0px;">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <?= Html::submitButton('Registrarme', ['class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#FF596A;font-size:11px', 'name' => 'signup-button']) ?>
                                </div>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                        <!-- ############################################### -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php \yii\bootstrap\Modal::end();


    ?>



