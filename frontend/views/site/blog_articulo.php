<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $model->titulo.' | Vendetumoto.co';

$this->registerJs(" 
    jQuery(document).ready(function () {
     
     $('.btn_login').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            
            $('.modal-header').remove();                 
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');      
            
         
    });
    $('.btn_login2').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            
            $('.modal-header').remove();
            $('#modal-menu').modal('hide');  
                        
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
     $('.btn_menu').click(function(e){ 
       
            var modal = $('#modal-menu').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
         
           
    });");
?>

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
    <div class="fusion-page-title-row">
        <div class="fusion-page-title-wrapper opac" >
            <div class="fusion-page-title-captions">
                <h1 class="entry-title" data-fontsize="30 "data-lineheight="26">
                    <a style="color: #e46a76" class="a_1" href="<?= Url::to(['site/'])?>/blog">BLOG</a>
                </h1>
            </div>
            <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs" >
                            <span style="color: #e46a76"  class="col_1  pull-right "  href="<?= Url::to(['site/']) ?>/blog" style="color: #0747A6"><?=  $model->titulo ?></span>
                            <span style="color: #e46a76" class="fusion-breadcrumb-sep  pull-right ">&nbsp; / &nbsp;</span>
                            <span style="color: #e46a76" class="pull-right">
                                <a style="color: #e46a76" href="<?= Url::to(['site/']) ?>/blog"><span>Inicio</span></a>

                            </span>


                        </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-left: 2px ">
    <center class="col-lg-12 col-md-12 col-lg-12">
        <a href="<?= Url::to(['site/getarticulo', 'url' => $model->url_permanente]) ?>">
            <?php
            if ($model->imagen_cabecera) {
                ?>
                <img style="border: 2px solid #e46a76; padding: 4px; border-radius: 7px;" class="profile-user-img img-responsive img-responsive" src="<?= $model->imagen_cabecera ?>" class="user-image" alt="<?php echo $model->titulo ?>"/>
                <br>
            <?php } else { ?>
                <img style="border: 2px solid #20ccd0; padding: 4px; border-radius: 7px;" class="profile-user-img img-responsive img-responsive" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMCAggICAgICAgGBggICAgICAgICAgHBwYICAgICAgIBggICAYICAgICAgICAoICAgICQkJCAgLDQoIDQgICQgBAwQEAgICCQICCQgCAgIICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICP/AABEIAYACAAMBEQACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAEDAgn/xABEEAACAQIBBwUOAwcFAQEAAAAAAQIDESEEBQYSMUFxB1FTYaETFhciNHKBkaKxssHR0jNzkiQyQlJigvAjwuHi8ZND/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwFwFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPzVkkm20kldt7Elt7AKZX5TYJtQpynHdLX1L9aWrLDj6gPn4UF0Mv8A6f8AQB4UF0Mv/p/0XvQFvzZnCNWEakHeLXNZp74vg8LgdQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcOfcmlOjVhFXlKElFc7thtsvWBm/eLlXRr9cfqA7xsq6Nfrj9QPHoLlXRr9cfqBeNCs11KNFxqLVlrtpXUvF1Y22NrbfACfAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPxVrJJybsoq7bwS4vcBWss5Q8ni7R16vXFJR9Dk4pgc/hMpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gHhNpdFX9j7gPvknKLQk7SVSlja8kmvTquTXqAstDKFJJxaknsaaafpQH0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADyT/wDeYDKtK9JpVpuKbVKLajHZrY/vT577k9npAgGwPNXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eADV4ANXgA1eAC3AD0Cb0W0hlQqLH/AE5O049T3pbpRePWroDWKcrpNYppWfOnj8wP0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIvSas45PWa6OS/V4t+OIGPAAAAAB42B9cnyaU8IRlN9Sb9yYHdHRvKH/+Nb9L/wA7APjlGZq0FeVOpHjCS96SA42v8tawAAAAAAJLI9G69Ra0KU5R3PCz4NuN/QB9+83Kuhn64feA7zcq6Gfrh94DvNyroZ+uH3gcmX5jrUknUpzgnvdrelpySA4QCA1rQ3Le6ZNTbvdJwx36kpQXYkBNgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIAAAAkczZgq134iVk/Gk8Ir0731IC/wCaNBaNKzku7S55W1Vvwje2HO/VuAsVGnZWSSW5LBepAfsAgIzOOj9Kr+/Ti/6l4sv1LHsYFKz5yfTp3lRbqxX8LXjpcyt+9x29QFSfNsYAAB+6EE5RT2OST4NpAbhCNsErLcubqA/VgFgFgODPtJOjWTV06U/WoyYGMAegapoF5LDzqnxyAsIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAiNLvJq3m/NAZCAAATui2jDyiV3eNOL8aW9/0x24gank+SxhFRilCKVkkrevnfWwPpFAegAAABYCp6WaHqqnUpqMaqWKWHdbfPmYGbSVsNlt29dT6wAH6o1LNPbZp8bO4G05FnGFSKnCUZJ9ezqfM1sa6gPv3Th6wHdOHrAd04esCL0lzrCnRqa0opyhKMY38aTcWlZcQMgA9A1TQLyWHnVPjkBYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEaXeTVvN+aAyEAB9825C6s4047ZOy3253wQGx5uzbGlCMIW1YrDr52+tsDrAAAAAAAA8kgM+5QMw6r7tFYSdp9Unsl6dnECmAAPLANUBqgNUAkB6AA1TQLyWHnVPjkBYQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABEaXeTVvN+aAyEABd+TXNibqVXu8SL69rt6LAX4AAAAAAAAAA487ZvVWnOm/wCONl1S3AYvKNsHtW31v6AeAAAAAAAAANU0C8lh51T45AWEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARGl3k1bzfmgMhAAapoJkurk0H/ADOUn6ZNLsSQFhAAAAAAAAAADAx3SfJ1HKKySslN24NRl8wIwAAAAAAAABqmgXksPOqfHICwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIADXNEKl8mo+Zb2mBMgAAAAAAAAAADI9Mn+1VuMfXqRuBDAAAAAAAAANU0C8lh51T45AWEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARGl3k1bzfmgMhAAadyfZVrUNXfCcl6G217wLOAAAAAAAAAAAMWzzlndKtSf8ANNtcNi7En6QOMAAAAAAAABqmgXksPOqfHICwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACI0u8mreb80BkIAC1cn+dtSq6bvaqrLGyU07p/pwA0pMD0AAAAAAAABDaWZ07jRlJO0peJDGzu964K+PADIgPQAAAAAAAAGqaBeSw86p8cgLCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIjS7yat5vzQGQgAPacrNNYPc+Z7n6wNa0Xz8q9NO614q01vvzpc0ufcwJoAAAAAAAD8zmkrtqKWLb2JLnAynS7SHu9Txfw4XjDr55en3AQQAAAAAAOnNmbpVZxpws3LndkudvqSxYFp8GdTpaXqkA8GVTpaXqkBcNHM0uhSVOTUmnJ3V0mpNyW1dYEmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIjS7yat5vzQGQgAAHVmzOc6M1OD1WtvNJfyy6gNS0f0op5QsPFmljBu763DnTAl1MD9AAAAD5ZRlUYJyk1GKxbeCSAzjSzTN1r06d40v4nvqfRc64AVa4AAAAAAAHTkGXypSVSD1XF7edb0+qSwaAsHhKr81D9MsfbA88JVfmofpl96Au2i2d5V6MaktVSbkmo4LCTSwbk8UucCXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAERpd5NW835oDIQAAAB+qdVppptNbGsGuDAtmaeUOpHCrHuqX8S8Wfpex+8Cy5Hp1k8rXk6b5pprtSkgJDvlyfpqP619EB8so0tyaK/Fg+qN5/CmBBZfykxWFKEpv+aS1Y34Yt9gFMzvnqrXd6km7bI7IxvzIDgsB6AAAAAAABIaO16ca8HVScFJXvsV9jfUmBpPfXkvSw9T+gHq0ryXpYdv0Aks35ZCpFSg4yg9jWzr/AMsgOkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcGfci7pRqwV7ypySt/Ntj2pAYw3jbj6GtzA9AAAAAAAAAAAAAAAAAAAAAAAeN/5/lwNi0ZyLudCnB4PV1nxn4zv14gSgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8aAoWmWh0nJ1aMdZPGcFe99rlFY3vvQFHXX9GuIHoAAAAAAAAAAAAAAAAAAAAPJf5/wBdNEdC5NqrWi4xi7wg9s3tTlstFc21vnQGgRYH6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYDjy7M1Gp+/TpzfO4rW/Ukn2gcnejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QHejk3Qw7fqA70cm6GHb9QOnIsxUaf7lKlF3vfVTl6JO7XrA7gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHDnTO8aMHOeCWGCu27NpRW9uz5t4FLyjlLqP8Acp00t2s3J267OK9TA68zaf1KlSFOVKD15KN4tq197Tvgrc4Fi0jzw6FJ1IqM2pRVm7J3tjhfn2ARui+l0som4yhCCUda6bb2pbHxA/WlOlksnlCMYRnrRcsZPCztuVgPjo5pv3ao6c4xpyavCzbUrbVjvtiuezAnc95x7jSnUSUtRJ2d0neSWPNtAgtGdM5V6jhKnCCUHO8ZNu6cVbFf1AWsCM0izq6FKVRJTacVZu22SWNr8+zACI0a0xlXqOEoQglFyum28Gtz4gd2lWkMsnjCUYxm5S1bNtWsm27pNblvAg838o0pVIRlThGMpKLak/FTwvs5wLu2BSc5cocoVJwjThKMJOKk5PxtV2e7DFNATeiukTyiM5OMYaskrJt3ur3d7WAnGwKTnXlBlCrOEadOajJq7k7u2D3W23QEnoppY8oc4yioOFmkm3dPDetz6wLGB8Mry6FOLlOShFbW77dyW276gKbl3KYl+HT1lulN6t/7Um16WBww5Tqt8adJrqcl24gXPR7PXd6aqavc73Vm77OZ2WAEDn3TqdGrKmqcJKKi7uTv4yT3KwHD4SqnRU/1SAldGtMp16vc3CEFqt3TbeFtzA/elGl0snlCMYQnrRcm23hjaytdAeaM6ad3qOnOMabavGzb1rfvLHfbECaz5nF0qU6iSk4pOzwTxS3AQmiumMspqSg6caaUXLBtt7MMcALUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoHKdN61FY6tptc2tddtresDn0DWTNSVVU+6OWHdNjjZYQvZayd/+QLnS0fowmqkKcYySdmtmPPG9nxAj9P/ACaXn0/kBXeTP8ap+X/uQHvKb+LT8yXxAViGvTdOawb8eDX9MmvfFrgwNDzznRVshnUW+CuturJTi5R+aArnJuv9eX5UvigBpgFd098mnxh8cQKtycfjy/Ll74gSfKhU8WlH+qb+H6gUapkzSjLdJO3VZq4GrRz3+y93wv3Ny/vSt69YDKlk7cZTd7RcY353NtdtrgXnkwl4tZddN+vXAtec8sVOE6j/AIIuX0XpeAGU5gza61SUdr1KkuMknq34ycUB06HZx7nlEOaV4S4S2X4PV7QNYlIDLtOc9OdZwv4lJ6v9ytrPir6voAmNFdCqbhGrWi5OSvGGyMVucrWbb244AWSponkzVnRp+hOL9DjZgdubsgjShGnFWjHBb9rb47wMv048pn1qC9cUv+QLbmPMmSSo0pThS1nCLld43tjfxl7gJfN2aMnhLWpRpqVmm4u7s922WDAqHKX+LS/LfvYFWhr0nTmsG7ThLdhJrsad+IGi53zoq2QzqLDWSvH+V6yun137AK7ybfiy/KfvgBpQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACPz3mWFeGpPrcZYXg9l1f3AZ1nXQitTTaSrRV/Ghi7f1RePqA/OjelVSjKKk3Ok3aUZNvVW9w22a222WuBctPn+yy8+n6dmPpAr3Jn+NU/L/ANyAcp/4lPzJfEB+4ZndXIKcl+/SdSS/qjry1o+p63/oEJmjOajSr0n+7UhrR5taNnh5yQEnyc+US/Kl8UPmBpYFd098mnxh8cQKtycfjy/Ll74gdfKhPx6K6pvth9AIzLMhvkNGpb92pUT4Tk4/ID4zzv8AsfcU8e7P0wsperWfYB1Vs36ub1J7alWM/wC3FR7PG9IEjyYy8atwh75ASXKLl2rRUN9Sa/TGz99gKxoZn6nk7nKalKUlFR1VfBO7e6zuBC5dWi6k5Quk5uUbqzV3dYbrAbHmvLFUpwqLZOKfC+DXodwMezzF91rJ7e6VE+Ou/eBsGQTTpwktjhFr9KwAy/OmT5VS8abrwi5NJucsXtSwk9wFl5OMqlJVtaU52dO2tJytfW52+ZAV3ThftU+EPhX/AKB7keg1epCM0qdpxUleSvZ7L7dwFp0K0YqUJVHPUSlFJarvjffs5wInlM/Ep/ly97APMzq5BTlFXnSc5pfzR12pR9WKAhs05y1aWUUm8KkLx5lOLuv1JYgSnJz+NLd/pP3wA0kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABXdJ9LHk8qaUVPW1nJXtgmlg+e7YEd4TKVr9zq625Xjb0yuvcBSqOTyr1bJLWqSbtHdrPG/NFLeBoenkbZK1zSpfICvcmf41T8v8A3ID3lO/Fp+ZL4gLHoIv2aHGp8T7NoFJ0wzJ3Gq7JqE7yh1J7V6HguoDt5OPx5flS+KAGlgV3T3yafGHxxAq3Jx+PL8uXviB9eUur/rQXNTb9bl9AJXNWbe6Zv1NrcJtcVKUl7rekDPsmoubjFbZtJf3YAaRprkihkmqsFB0or0eKBB8mU0qtX8tP1S2gcvKBl+vlGrupLV/ueMnx2L0ATWYNB6U6MJ1O6OUlrO0rJJt6tlZ2ulf0gQemWjcaEoOnrasl/E72lHat21AWTk5y+9GVNu7pyw82eztTAhtO9H3Go60Ytwn+/bHVlzvmUsPTcDl0b01lQWpKPdIK9rPxoX3J7GlzMD66WaWQyiEYRjOOrJSvKyv4rVrekCR5MnhX40/dMCD02f7VU4Q+BATOaeUGFOlTpulUk4RUW1KNnZbUBMZn06hWqRpqnOLlfFtNKyb3cAIHlL/Ep/ly97Asmg8f2anzeP6fHkBRdLsy9xqtJWhPxoP4l6GBI8nH40/y38UANJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARmc8wUq9u6Q1mlZO7i0tu1Nb31gQ/g5oX21tuzXVvgAm81ZjpUfw4KL3yb1pP0vED65zzZCtBwmm4tp4Ozutjv8gOPNGjNKhJypqaclqtuV7Lbgue4H6ztozSrtSqKTcVZWk1dPF7OvrQHXmzN0KUFCCair2u7vHF443xA+GdsywrpRqJtJ3VnZp9Tw28wHwzVovRoSc6akpOLjjJvBtPhfBbwJgDkznmyNaDhNNxbWx2eGKxXXz2A4s06LUaMtampptarbldJPbh6EB5nbRWjXlr1FJytbCTStjzJ87A7chyCNOEYQTUY7L4vbf3gcWTaKUISU400pJ3Tu3Z86V37gOzOObY1oOnO+q7b2sU7rFJsDlzRovRoyc6cZRbjqu8tZWunsb6gOXKdBsnnKUpKo3Jtt67xb28wE7RpqKUUrJJJdSWCXYBx53zNCulGopNJ3VnZp8Vu6mB8c0aNUqDcqamm1Z3ldbb7OIEnKF1sXB+584EFlWg+Tzx1HC/8knG3BYrsA+EOTzJk72qPjN/JL3gTmb80U6SapwUE7Xttdr2u7vnYEdnLQ6hVm6k1PWdk7Ta2YLDBbOsDm8H+Tfy1P1v6gdGbtEKNKanBTUle15NrFWeHAD7520ZpV2pVFK8Y2VpNYf2p4gdmbc3RpQUIXUVeybu8Xd48QPjnfMlOulGom0ndNPVafNwA5806L0qEnKmpJtWd5N4bcN25b9wEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//Z" class="user-image" alt="Sin imagen_cabecera"/>

                <?php
            }
            ?>
        </a>
    </center>
</div>
<hr>
<div class="row" style="margin-left: 2px ">
    <div class="col-lg-12 col-md-12 col-lg-12" style="margin-bottom: 50px">
        <span class="text-danger" style="margin-bottom: 30px">
            <a class='col_1' href="<?= Url::to(['site/getarticulo', 'url' => $model->url_permanente]) ?>" style="font-size: 25px;color: #e46a76 !important;"><?= $model->titulo ?>..</a>
        </span>
        <hr style="margin-top: 5px;margin-bottom: 11px;border: 1px solid #e46a765c">
        <div class="fusion-post-content-container">
            <p style="color: #766d6d"><?= $model->contenido ?>..</p>

        </div>
    </div>
</div>

<style>

    .btn-facebook {
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        padding: 8px;
        color: #969696;font-size: 20px;
    }
    .btn-google {
        /* color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        color: #969696;
        font-size: 20px;
    }

    .btn-login {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px #fff;
        box-shadow: 0 1px 1px #fff;
    }
    .panel-heading .box {
        padding: 18px 0 6px;
        text-align: center;
    }

    .panel-heading .line {
        margin-top: 4px;
        margin-bottom: 20px;
        border: 0;
        width: 100%;
        border-top: 1px solid #c5c5c54d;

    }

    .panel-heading .active .line {
        border-top: 3px solid #F33B42;

    }


    .panel-heading .text {
        color: #000;
        font-weight: bold;
    }

    .panel-heading .text2{
        color: #000;
        font-weight: normal;
    }

    .panel-heading .text, .text2:hover,
    .panel-heading .text, .text2:focus {
        text-decoration: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid #fff;
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .card-header {
        margin-bottom: 0;
        padding: 1.25rem 1.5rem;
        border-bottom: 1px solid rgba(0, 0, 0, .05);
        background-color: #fff;
    }

    .card-header:first-child {
        border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    }

    .shadow {
        /* box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;*/
    }
    #signupform-acepto_terminos {
        display: block;
    }

    .tambien{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 18px;

        letter-spacing: 0.0278198px;

        color: #969696;
    }

    .titulos_form{
        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;
        font-weight: normal;
        color: #F33B42 !important;
    }
    .help-block{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 14px;
        /* identical to box height */


        text-align: right;
        letter-spacing: 0.0218584px;

        color: #353435 !important;
    }

    .rosado_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #FF596A;
    }

    .negro_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #353435;
    }

    .normal_text{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: 0.0278198px;

        color: #353435;
    }

</style>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<?php

use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\CliUsuario;
use yii\authclient\widgets\AuthChoice;

\yii\bootstrap\Modal::begin([
    'header' => '<h4 class="text-danger  modal-title">Debes iniciar sesión para realizar la acción</h4>',
    'id'     => 'modal-login',
    'size' => 'modal-lg',
    'clientOptions' => [

        //'backdrop' => 'static',
        'keyboard' => false],
    'footer' => null,
]); ?>
<input type="hidden" id="param" name="param">
<?php

$model = new LoginForm();
$modelSignup = new SignupForm();
$modelCliU = new CliUsuario();
/*
echo $this->render('loginpopup', [
    'model' => $model,'modelSignup' => $modelSignup,'modelCliU' => $modelCliU,
])
*/
?>
<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->


    <div class="panel panel-login card bg-secondary shadow border-0" style="width: 80%;height: auto;
    margin: 0 auto;">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6 box active" id="login-form-link">
                    <a href="#" class="text txt_is">INICIAR SESIÓN</a>
                    <hr class="line">
                </div>
                <div class="col-xs-6 box" id="register-form-link">
                    <a href="#" class="text2 txt_reg" style="">REGISTRATE</a>
                    <hr class="line">
                </div>
            </div>
            <!-- <hr> -->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=>false,'method'=>'post','action'=>\yii\helpers\Url::to(['site/login'])]); ?>

                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <!-- <?= $form->field($model, 'rememberMe')->checkbox() ?> -->
                    </div>
                    <div class="col-lg-12">
                        <div class="pull-right" style="padding-right: 10px; margin-bottom: 15px"><?= Html::a('<span class="titulos_form">Olvidé mi contraseña</span>', ['site/request-password-reset']) ?></div>
                    </div>
                    <div class="form-group" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#F33B42;font-size:11px', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    </div>



                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>
                    <p class="tambien" style="font-weight: normal">También puedes ingresar con</p>
                    <hr style="width: 70%;text-align: center">
                    <div class="social-button-login">
                        <div class="row">
                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-3 col-sm-offset-3" style="margin: 0.5em;color:#969696">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <?php ActiveForm::end(); ?>

                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'register-form',  'options' => ['style' => 'display: none;']]); ?>

                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'nombre')->label( '<span class="titulos_form">Nombre completo</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'email')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'telefonos')->label('<span class="titulos_form">Número de contacto</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'whatssapp')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de WhatsApp</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('<span class="titulos_form">Confirmación de contraseña</span>') ?>
                        </div>
                    </div>
                    <p class="tambien" style="margin-top: 10px">&nbsp;</p>
                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>


                    <div class="social-button-login">
                        <p class="tambien" style="margin-top: 10px">También puedes ingresar con</p>
                        <hr style="    margin-top: 0px;    margin-bottom: 9px;    border: 0;    border-top: 1px solid #eeeeee;">
                        <div class="row">

                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <div class="row">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">


                                    <table class="table" style="margin-top: 20px">
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-promocionales" name="SignupForm[promocionales]" value="1" aria-required="true">
                                            </td>
                                            <td class="normal_text" style="border: 0px">
                                                Me gustaría recibir comunicaciones promocionales (Recibirás un e-mail de confirmación)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-acepto_terminos" name="SignupForm[acepto_terminos]" value="1" aria-required="true">
                                            </td>
                                            <td style="border: 0px" class="normal_text">Declaro que he leído y acepto la nueva <a target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>" class="rosado_aref" style="">Política de privacidad</a> y los
                                                <a class="rosado_aref" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de VENDETUMOTO.CO
                                            </td>
                                        </tr>
                                    </table>


                                    <?= $form->field($modelSignup, 'promocionales')->hiddenInput()->label(false) ?>
                                    <?= $form->field($modelSignup, 'acepto_terminos')->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::submitButton('Registrarme', ['class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#FF596A;font-size:11px', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <!-- ############################################### -->
                </div>

            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::end();


?>



