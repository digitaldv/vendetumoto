<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Utils;

$this->title = '¡Resultado de la transacción - Vende Tu Moto!';
//  Script de respuesta de Epayco
 $this->registerJs("   

$(document).ready(function() {

setTimeout(function(){location.href='".Url::to(['/site/mispublicaciones'])."'}  , 5000);
        
    // function getQueryParam(param) {
    //     location.search.substr(1)
    //         .split('&')
    //         .some(function(item) { // returns first occurence and stops
    //             return item.split('=')[0] == param && (param = item.split('=')[1])
    //         })
    //     return param
    // }
    // $(document).ready(function() {
    //     //llave publica del comercio
    //     //Referencia de payco que viene por url
    //     var ref_payco = getQueryParam('ref_payco');
    //     // alert(ref_payco);
    //     //Url Rest Metodo get, se pasa la llave y la ref_payco como paremetro
    //     var urlapp = 'https://secure.epayco.co/validation/v1/reference/' + ref_payco;
    //     $.get(urlapp, function(response) {
    //         if (response.success) {
                
    //             // Al finalizar la transacción con un botón de pago, retornan los siguientes datos a la URL de respuesta por POST 
    //             $.post('".Url::to(['/site/epaycoconfirmation'])."', {'transaccion':response.data.x_transaction_date, 'estado': response.data.x_cod_response});

    //             if (response.data.x_cod_response == 1) {
    //                 //Codigo personalizado
    //                 alert('Transaccion Aprobada');
    //                 console.log('transacción aceptada');
    //             }
    //             //Transaccion Rechazada
    //             if (response.data.x_cod_response == 2) {
    //                 console.log('transacción rechazada');
    //             }
    //             //Transaccion Pendiente
    //             if (response.data.x_cod_response == 3) {
    //                 console.log('transacción pendiente');
    //             }
    //             //Transaccion Fallida
    //             if (response.data.x_cod_response == 4) {
    //                 console.log('transacción fallida');
    //             }

    //             $('#referencia').text(response.data.x_id_invoice);
    //             $('#fecha').html(response.data.x_transaction_date);
    //             $('#respuesta').html(response.data.x_response);
    //             $('#motivo').text(response.data.x_response_reason_text);
    //             $('#banco').text(response.data.x_bank_name);
    //             $('#refEpayco').text(response.data.x_transaction_id);
    //             $('#compra').text(response.data.x_description);
    //             $('#total').text(response.data.x_amount + ' ' + response.data.x_currency_code);
               
    //         } else {
    //             alert('Error consultando la información');
    //         }
    //     });
    // });
        
});");

?>
<style>
    .alert {
        padding: 15px;
         margin-bottom: 0px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>
    <div  id="seleccion" class="container" style="margin-top: -20px;">
        <div class="row" style="margin-top:20px">
            <div class="col-lg-8 col-lg-offset-2 ">
                <h3 style="text-align:left;font-weight: bold; color:#663cc5"> Respuesta de la Transacción </h3>
                <hr>
            </div>
            <div class="col-lg-8 col-lg-offset-2 ">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <!--
                          'referencia' => $payment['x_id_factura'],        'fecha' => $payment['x_transaction_date'],
                                'respuesta' => $payment['x_response'],           'motivo' => $payment['x_response_reason_text'],
                                'banco' => $payment['x_bank_name'],              'refEpayco' => $payment['x_transaction_id'],
                                'compra' => $payment['x_description'],           'total' => $payment['x_amount']." ". $payment['x_currency_code'],
                                'extra1' => $payment['x_extra1'],                'tipo_pago' => $payment['x_type_payment'],
                                'nro_tarjeta' => $payment['x_cardnumber'],
                        -->
                            <tr>
                                <td class="bold">Transacción #</td>
                                <td id="refEpayco"><?=$refEpayco ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Ref. Factura</td>
                                <td id="referencia"><?=$referencia ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Fecha</td>
                                <td id="fecha" class=""><?=$fecha ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Tipo Pago</td>
                                <td id="refEpayco"><?=$tipo_pago ?></td>
                            </tr>
                            <?php
                                if(isset($tipo_pago) && $tipo_pago=='TDC'){
                                    echo' <tr>
                                            <td class="bold">Tipo Pago</td>
                                            <td id="refEpayco">'.$nro_tarjeta.'</td>
                                        </tr>';
                                }

                                if(isset($cuotas) && $cuotas>0){
                                    echo' <tr>
                                                <td class="bold">Cuotas</td>
                                                <td id="refEpayco">'.$cuotas.'</td>
                                          </tr>';
                                }

                                if($estado_transaccion==2 || $estado_transaccion==4) $color = 'red';
                                else if($estado_transaccion==1) $color = 'green';
                                else if($estado_transaccion==3) $color = 'blue';
                                     else $color = '#fff';
                            ?>


                            <tr>
                                <td class="bold">Respuesta</td>
                                <td>
                                    <b style="color:<?= $color ?>">
                                        <?= strtoupper($estado_transaccion.'-'.$respuesta) ?>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold">Motivo</td>
                                <td id="motivo"><?=$motivo ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Banco</td>
                                <td id="banco"><?=$banco ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Descripción Compra</td>
                                <td id="compra"><?=$compra ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Observaciones extras</td>
                                <td id="compra"><?=$extra1 ?></td>
                            </tr>
                            <tr>
                                <td class="bold">Total</td>
                                <td id="total">$<?=$total ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top: 30px">
        <div class="container">
            <p style="text-align: center">
                <a class="btn" style="background-color: #e41f06; color: #fff; padding: 5px 10px; border-radius: 3px; font-size: 20px;" href="<?= Url::to(['/site/mispublicaciones']) ?>">
                <i class="fa fa-bookmark fa-fw"></i>Ver mis Publicaciones
                </a>
            </p>
        </div>
        <a class="btn pull-right" href="javascript:imprSelec('seleccion')"  style="background-color: #8F8F8F; color: #fff; padding: 2px 5px; border-radius: 3px; font-size: 14px;">Imprimir</a>
    </div>
<div>
    <div class="row">
        <div class="container" style="padding: 10px">
            <div class="col-lg-8 col-lg-offset-2 pull-left">
                <img src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/pagos_procesados_por_epayco_260px.png" style="margin-top:10px; float:left">

            </div>
        </div>
    </div>
</div>
<script>
    function imprSelec(nombre) {
        var ficha = document.getElementById('seleccion');
        var ventimp = window.open(' ', 'popimpr');
        ventimp.document.write( ficha.innerHTML );
        ventimp.document.close();
        ventimp.print( );
        ventimp.close();
    }
</script>
