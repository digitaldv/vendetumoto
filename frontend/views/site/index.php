<?php

use common\models\PubFavoritos;
use yii\widgets\ActiveForm;
use yii\web\View;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;
use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\CliUsuario;


$this->title = 'Vendetumoto.co | ¡La tienda de motos más grande de Colombia!';

$this->registerJs(" 
jQuery(document).ready(function () {

//******************************************************
     
      
     $('#btn_iniciar_sesion').click(function(e){
        e.preventDefault();   
        
        if($('#loginform-username').val()!='' && $('#loginform-password').val()!='')
        {      
        
            if($('#loginform-username').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#loginform-username').val($('#loginform-username').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#loginform-username').val()).toLowerCase())){                      
                  $('.field-loginform-username > .help-block').html('¡El Correo es incorrecto!');
                  return false;
               }
            }   
        
        
             $.post('".Url::to(['site/emailexiste'])."', {email: $('#loginform-username').val() }, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        console.log(res); //return false;
                        if(!res.res){  //FALSE: SI EXISTE: Ahora pregunte si el password es correcto                    
                             //Preguntar por contraseña
                              $.post('".Url::to(['site/existepassword'])."', {email: $('#loginform-username').val(), pass: $('#loginform-password').val() }, 
                                   function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub); 
                                            console.log(res); //return false;
                                            if(res.res){  //TRUE: SI ES EL PASSWORD: LOGUEARSE....                    
                                                 $('#login-form').submit();                                                  
                                            }else{
                                                $('.field-loginform-password > .help-block').html('<b style=\'color:red\'>¡Contraseña Incorrecta!</b>');
                                                return false;
                                            }
                              });
                        }else{
                            $('.field-loginform-username > .help-block').html('<b style=\'color:red\'>¡Correo NO Existe en VTM!</b>');
                            return false;
                        }
                    
             });
        }else{
          if($('#loginform-username').val()=='') $('.field-loginform-username > .help-block').html('Debe ingresar el correo');
          if($('#loginform-password').val()=='') $('.field-loginform-password > .help-block').html('Debe Ingresar su contraseña');
        }
    }); 
    
    $('#cliusuario-nombre').change(function(e){     
        if($('#cliusuario-nombre').val()!='') $('.field-cliusuario-nombre > .help-block').html('');
    });
    
    $('#signupform-email').change(function(e){     
        if($('#signupform-email').val()!='') $('.field-signupform-email > .help-block').html('');
    });
    
    $('#signupform-password').change(function(e){     
        if($('#signupform-password').val()!='') $('.field-signupform-password > .help-block').html('');
    });
    
    $('#signupform-repeat_password').change(function(e){     
        if($('#signupform-repeat_password').val()!='') $('.field-signupform-repeat_password > .help-block').html('');
    });
    
    $('#cliusuario-telefonos').change(function(e){     
        //if($('#cliusuario-telefonos').val()!=''){ 
          $('.field-cliusuario-telefonos > .help-block').html('');
        //}
    });
    
    $('#cliusuario-whatssapp').change(function(e){     
        //if($('#cliusuario-whatssapp').val()!=''){ 
            $('.field-cliusuario-whatssapp > .help-block').html('');
        //}
    });
    
    $('.loginform-username').change(function(e){     
            $('.field-loginform-username > .help-block').html('');
    });
    
    $('.loginform-password').change(function(e){     
            $('.field-loginform-password > .help-block').html('');
    });
    
    $('#signupform-acepto_terminos').change(function() {
        if(this.checked) {
             $('.field-signupform-acepto_terminos > .help-block').html('');
             $('.normal_text2').attr('style','border: 0px');     
             
        }
    });
    
    
    
    
    $('#btn_reg').click(function(e){
        e.preventDefault();   
           var error=0;
           var filter = /^\d*(?:\.\d{1,2})?$/;
           if($('#cliusuario-telefonos').val()!=''){
               if(filter.test($('#cliusuario-telefonos').val())) {
                   if($('#cliusuario-telefonos').val().length < 10){
                       $('.field-cliusuario-telefonos > .help-block').html('El número de contacto debe tener 10 dígitos.');
                       $('#cliusuario-telefonos').focus();
                       error=1; 
                   }
               }else{
                  $('.field-cliusuario-telefonos > .help-block').html('El Número de contacto NO es un número válido.');        
                  error=2;         
               }
           } 
            
           if($('#cliusuario-whatssapp').val()!=''){
               if(filter.test($('#cliusuario-whatssapp').val())) {
                   if($('#cliusuario-whatssapp').val().length < 10){
                       $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp debe tener 10 dígitos.');
                       $('#cliusuario-whatssapp').focus();
                       error=3;
                   }
               }else{
                  $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp NO es un número válido.');
                  error=4;                 
               }
           }         
            
            if($('#signupform-email').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#signupform-email').val($('#signupform-email').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#signupform-email').val()).toLowerCase())){                      
                  $('.field-signupform-email > .help-block').html('¡CORREO INVÁLIDO!');
                  //return false;
                  error=5;
               }
            }   
            
            
            
            if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){
                    $('.field-signupform-password > .help-block').html('La contraseña debe ser de 6 caracteres.');
                    error=6;
            }
            
            if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){
                    $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe ser de 6 caracteres.');
                    error=7;
            }
            
            if($('#signupform-password').val() != $('#signupform-repeat_password').val()){
                $('.field-signupform-repeat_password > .help-block').html('Confirmación contraseña NO concuerda con la contraseña.');
                error=8;
             }else{
                $('.field-signupform-password > .help-block').html('');
                $('.field-signupform-repeat_password > .help-block').html('');
               
             }
            
        var error1=error2=0;
        if( $('#cliusuario-telefonos').val()!='' && $('#signupform-whatssapp').val()!='' && $('#cliusuario-nombre').val()!='' && $('#signupform-email').val()!='' && $('#signupform-password').val()!='' && $('#signupform-repeat_password').val()!='' && $('input[id=signupform-acepto_terminos]:checked').val())
        {            
               if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){                    
                    error1=1;                        
               }                
               if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){                        
                     error2=1;
               }
               
               if(error1==1 && error2==1){
                   $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.'); 
                   $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');   
                   return false;                   
               }else if(error1==1) {
                    $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.');    
                    return false;
               }else if(error2==1) {
                        $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');    
                        return false;  
                    }
            //Si llegó hasta acá y checkeó, REGISTRA
            if($('input[id=signupform-acepto_terminos]:checked').val()){
                
                 $.post('".Url::to(['site/emailexiste'])."', {email: $('#signupform-email').val() }, 
                   function(res_sub) {
                            var res = jQuery.parseJSON(res_sub); 
                            //console.log(res); return false;
                            if(res.res){
                               // alert(error);  
                                if(error==0){
                                   //alert('ingresó hasta el submit()');
                                   $('#register-form').submit();
                                }else 
                                  return false;
                            }else{
                                $('.field-signupform-email > .help-block').html('<b style=\'color:red\'>¡CORREO YA EXISTE EN VTM!</b>');
                                return false;
                            }
                        
                   }
                 );
            }else{
               $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
               $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');               
            }
            
          
        }else{
        
           if($('#cliusuario-whatssapp').val()=='') $('.field-cliusuario-whatssapp > .help-block').html('WhatssApp NO puede estar vacío.');
           if($('#cliusuario-telefonos').val()=='') $('.field-cliusuario-telefonos > .help-block').html('Nro de contacto NO puede estar vacío.');
           if($('#cliusuario-nombre').val()=='') $('.field-cliusuario-nombre > .help-block').html('Nombre completo NO puede estar vacío.');
           if($('#signupform-email').val()=='') $('.field-signupform-email > .help-block').html('Email NO puede estar vacío.');
           if($('#signupform-password').val()=='') $('.field-signupform-password > .help-block').html('Contraseña NO puede estar vacío.');
           if($('#signupform-repeat_password').val()=='') $('.field-signupform-repeat_password > .help-block').html('Confirmación de contraseña NO puede estar vacío.');
           if(!$('input[id=signupform-acepto_terminos]:checked').val()){ 
            $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
            $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');
            
           }
           
           if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){                    
                    $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.'); 
           }
            
           if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){
                    $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');                 
           }
           
             
        }
    
    });
//******************************************************

    

 $('.counter-value').each(function(){
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        },{
            duration: 3500,
            easing: 'swing',
            step: function (now){
                $(this).text(Math.ceil(now));
            }
        });
    });
    

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    }

 
  
    $('.btn_suscribirme').click(function(e){
        e.preventDefault();          
        if($('#txt_email').val()!=''){
          
           $('#txt_email').val($('#txt_email').val().replace(/ /g,''));
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(!re.test(String($('#txt_email').val()).toLowerCase())){
                alert('ERROR: ¡EMAIL INVÁLIDO! Debes ingresar un email correcto.');
                document.getElementById('txt_email').focus();
                return false;
           }
           
           if($('input[name=check_suscrib]:checked').val()==1){
                 //Grabo en BD y envío un email al suscriptor
                 $.post('".Url::to(['site/addsuscriptor'])."', {email: $('#txt_email').val() }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==1)  {                    
                               alert('¡Felicitaciones! Tu email ha quedado inscrito en nuestro NewsLetter.');
                            }else {                                  
                                   alert('Información: ¡El email ingresado YA está inscrito en nuestro NewsLetter!');
                            }                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
           }else{
             alert('ERROR: Debes aceptar los términos y condiciones para aceptación de tratamiento de datos y envío de información.');
             return false;
           }
        
        }else{
           alert('¡Debes ingresar un email!');
        }
   });
   
    $('#login-form-link').click(function(e) {
        $('#login-form').delay(100).fadeIn(100);
        $('#register-form').fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        
        $('.txt_reg').attr('style','font-weight:normal');
        $('.txt_is').attr('style','font-weight:bold');
        
        e.preventDefault();
    });
    
    
 
    
    $('#register-form-link').click(function(e) {
        e.preventDefault();
        
        $('#register-form').delay(100).fadeIn(100);
        $('#login-form').fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
    
        $('.txt_reg').attr('style','font-weight:bold');
        $('.txt_is').attr('style','font-weight:normal');
        
        
        
        
        
        
        
        
    });
});
", View::POS_END);

$this->registerJs(" 
jQuery(document).ready(function () {
   
    $('.btn_login').click(function(e){ 
          
          $('#loginform-username').val('');
          $('#loginform-password').val('');
          $('#signupform-email').val('');
          $('#signupform-password').val('');
          $('#cliusuario-nombre').val('');
          $('#cliusuario-nombre').prop('autofocus',true);
          $('#cliusuario-nombre').focus();
          
          
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            //modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();                 
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');      
            
         
    });
    
    $('.btn_login2').click(function(e){ 
        $('#loginform-username').val('');
          $('#loginform-password').val('');
          $('#signupform-email').val('');
          $('#signupform-password').val('');
          $('#cliusuario-nombre').val('');
          $('#cliusuario-nombre').prop('autofocus',true);
          $('#cliusuario-nombre').focus();
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
             $('#modal-menu').modal('hide');  
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
     $('.btn_menu').click(function(e){ 
       
            var modal = $('#modal-menu').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
 
     
     
     
    $('#btn_busq_filtros').click(function(e)
    {
    
        //Precio validación
        if($('#pubpublicacion-precio_desde').val()!='' && $('#pubpublicacion-precio_hasta').val()!=''){                
            if($('#pubpublicacion-precio_desde').val()*1 > $('#pubpublicacion-precio_hasta').val()*1)
            {
                alert('Error Precio: Precio Desde debe ser menor que Precio Hasta.');
                return false;  
            }
        }
    
         if($('#mottipo-id').val()!='' || $('#motmarca-id').val()!='' || $('#motmodelo-id').val()!=''  || $('#pubpublicacion-precio_desde').val()!=''  || $('#pubpublicacion-precio_hasta').val()!=''
            || $('#pubpublicacion-ano_desde').val()!='' || $('#pubpublicacion-ano_hasta').val()!=''){
        
           
             
             //Año validación
             if($('#pubpublicacion-ano_desde').val() > $('#pubpublicacion-ano_hasta').val()  ){
                   alert('ERROR: El (AÑO Desde) debe ser menor o igual al (AÑO Hasta).'); return false;            
             }
             
             $('#frm').submit();
            //  window.location.href = '".Url::to(['site/resultados'])."?tipo='+$('#mottipo-id').val()+'&id_marca='+$('#motmarca-id').val()+'&id_modelo='+$('#motmodelo-id').val()+'&precio_desde='+$('#desde').val()+'&precio_hasta='+$('#hasta').val()+'&ano_desde='+$('#pubpublicacion-ano_desde').val()+'&ano_hasta='+$('#pubpublicacion-ano_hasta').val();
             
         
        }else{
          alert('ERROR: Debes seleccionar alguna opción.');
        } 
    });
   
   $('.btn_favorito').click(function(e){
    
        e.preventDefault();          
        if($(this).attr('id_usuario')=='' && $(this).attr('logueado')==0){
            //alert('¡Primero debes Iniciar Sesión o Registrarte!');
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            modal.find('.modal-title').html('Debes iniciar sesión para guardar en favoritos');                           
        }else                                   
            if($(this).attr('id_pub')!='')
            {
                 $.post('".Url::to(['site/favoritoupdate'])."', {id_publicacion: $(this).attr('id_pub'), id_usuario: $(this).attr('id_usuario') }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==2)  {                    
                               alert('¡Publicación ha sido eliminada de tus favoritos con éxito!');
                               $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg2 \"><i class=\"fa fa-heart-o\"></i></div>');
                            }else {                                  
                                   alert('¡Publicación agregada a tus favoritos con éxito!');
                                  $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg danger \"><i class=\"fa fa-heart\"></i></div>');
                              }
                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
                 
            }else{
              alert('Error: ID_PUB no existe.');
            }      
         
    });
   
    
   
});  


$('#btn_abrir_cerrar').click(function(e){
    e.preventDefault();
    //alert($('#op').val());

    if($('#op').val()==1){
       $('.bg').attr('style','height: 321px; background: url(\'".Yii::$app->request->baseUrl."/img/banner_home_abierto.png"."\'); background-repeat: no-repeat; background-position: top; background-size: contain;');
       
       $('#btn_abrir_cerrar').removeClass('a1');
       $('#btn_abrir_cerrar').addClass('a2');      
       $('#frm').removeClass('form');
       $('#frm').addClass('form2');      
       
       $('.img-pest').attr('src','".Yii::$app->request->baseUrl."/img/pestana_cerrar.png"."');
       $('.img-pest').attr('style','width: 20px');

        $('#op').val(2);
    }else{
        $('#op').val(1);
        $('.bg').attr('style','height: 156px; background: url(\'".Yii::$app->request->baseUrl."/img/banner_home_sin_abrir.png"."\'); background-repeat: no-repeat; background-position: top; background-size: contain;');
       
        $('.img-pest').attr('src','".Yii::$app->request->baseUrl."/img/pestana_abrir.png"."');
        $('.img-pest').attr('style','width: 20px');
        
        $('#btn_abrir_cerrar').removeClass('a2');
        $('#btn_abrir_cerrar').addClass('a1');
        $('#frm').addClass('form');
        $('#frm').removeClass('form2');      
        

    }
        
        
});

   
");
?>



<link href='<?= Yii::$app->request->baseUrl . '/js/slider/style.css'?>' rel='stylesheet'/>





<style>
    .carousel {
        margin-bottom: 0px;
    }
    .m-l-md {
        margin-left: 0px;
        font-size: 12px;
    }
    .ove{
        min-height: 180px !important;
        text-align: center !important;
    }

    /* Botones de entradas de estilos utilizados en páginas de descripción de películas.*/

    .select2-selection--single{
        border-radius: 0px !important;
    }

    #pubpublicacion-precio_desde{
        border-radius: 0px !important;

    }
    #pubpublicacion-precio_hasta{
        border-radius: 0px !important;
    }

    .bg {
        /* background: url('<?= Yii::$app->request->baseUrl . '/img/banner_home_sin_abrir.png'?>');*/
        background: url('<?= Yii::$app->request->baseUrl . '/img/banner_home_abierto.png'?>');
        background-repeat: no-repeat;
        background-position: top;
        background-size: contain;
        height: 321px;

    }


    .a1{
        position: relative;
        float: right;
        border-radius: 1.5em;
        color: white;
        padding: 10px 10px;
        margin-top: 99px;
        margin-right: 10px;
    }

    .a2{
        position: relative;
        float: right;
        border-radius: 1.5em;
        color: white;
        padding: 10px 10px;
        margin-top: 267px !important;
        margin-right: 10px;
    }


    .form {
        width: 600px;
        max-width: 600px;
        margin: 0 auto;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        visibility: hidden;
    }

    .form2 {
        width: 600px;
        max-width: 600px;
        margin: 0 auto;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        visibility: visible;
    }

    .form input {
        width: 100%;

    }

    .form button {
        padding: 0.5em 1em;
        border: none;
        background: rgb(100, 200, 255);
        cursor: pointer;
    }

    .row_full{
        width: 100%;
        margin-left: 0px ;
        margin-top: 75px;
        background: #2A2A2A;
        padding: 10px;
    }


    @media only screen and (max-width:1024px) {

        .a1 {
            position: relative !important;
            float: right !important;
            border-radius: 1.5em;
            color: white;
            padding: 10px 10px;
            margin-top: 4px !important;
            margin-right: 10px !important;
        }

        .a2 {
            position: relative;
            float: right;
            border-radius: 1.5em;
            color: white;
            padding: 10px 10px;
            margin-top: 212px !important;
            margin-right: 10px;
        }
    }

    .center {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }

    @media only screen and (max-width:600px) {

        .a1{
            position: relative !important;
            float: right !important;
            border-radius: 1.5em;
            color: white;
            padding: 10px 10px;
            margin-top: 4px !important;
            margin-right: 10px !important;
        }

        .a2{
            position: relative;
            float: right;
            border-radius: 1.5em;
            color: white;
            padding: 10px 10px;
            margin-top: 6px !important;
            margin-right: 10px;
        }
        .form {
            width: 100%;
            max-width: 600px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .form2 {
            width: 100%;
            max-width: 600px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            visibility: visible;
        }
        .row_full{
            width: 100%;
            margin-left: 0px !important;
            margin-top: 0px !important;
            margin-right: 0px !important;

            padding: 10px;
        }
        .txt_hasta{
            margin-top: 0;padding-left: 0px;padding-right: 0px !important;
        }
    }


    .txt_hasta{
        margin-top: 0;padding-left: 0px;padding-right:  5px
    }

    ::placeholder, .select2-selection__placeholder{
        color: #474747 !important;
        opacity: 1; /* Firefox */
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
    }

    .ove {
        min-height: 268px !important;
        text-align: center !important;
    }

    .ove2 {
        min-height: 177px !important;
        text-align: center !important;
        background: #80808047 !important;
    }



    .carousel-control {
        line-height:250px;
        font-size:54px !important;
        width:80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }

    .ove2 > .carousel-control{
        line-height: 183px;
    }
    .titulito{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 17px;
        line-height: 21px;
        letter-spacing: 0.54216px;
        color: #474747;
    }
</style>
<input type="hidden" id="op" value="2">
<div class="site-index">
    <div class="body-content">

        <div class="bg">
            <a href="#" id="btn_abrir_cerrar" class="a2">
                <img class='img-responsive img-pest' src="<?= Yii::$app->request->baseUrl . '/img/pestana_cerrar.png'?>" style="width: 20px" >
            </a>
            <!-- TIPO + MARCA + MODELO ------------->
            <?php $form = ActiveForm::begin(
                [ 'options' => [
                    'class' => 'form2'
                ] ,
                    'action' =>['site/resultados'], 'id' => 'frm', 'method' => 'get', ]
            ); ?>

            <div class="row row_full" >

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12" style="padding-right: 0px;padding-left: 0px">
                            <?=
                            $form->field(new \common\models\MotTipo(), 'id')->widget(Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\common\models\MotTipo::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                'options' => ['placeholder' => 'Tipo de moto', 'id'=>'tipo', 'name'=>'tipo'],
                                'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12" style="padding-left: 0px;padding-right: 0px">
                            <?php
                            echo $form->field(new \common\models\MotMarca(), 'id')->widget(Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map(\common\models\MotMarca::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                                'options' => ['placeholder' => 'Todas las marcas', 'id'=>'marca', 'name'=>'marca'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12" style="padding-left: 0px;padding-right: 0px">

                            <?=
                            $form->field(new \common\models\MotModelo(), 'id')->widget(DepDrop::classname(), [
                                'data' => [],
                                'options' => ['placeholder' => \Yii::t('app', 'Todos los modelos'), 'id' => 'modelo', 'name' => 'modelo'
                                ],
                                'type' => DepDrop::TYPE_SELECT2,
                                'select2Options' => [
                                    'pluginOptions' => ['allowClear' => true],

                                ],
                                'pluginOptions' => [
                                    'depends' => ['marca'], 'placeholder' => 'Seleccione...',
                                    'url' => Url::to(['/site/getmodelosmarca']),
                                    'loadingText' => \Yii::t('app', 'Cargando modelos ...'),
                                ],
                            ])->label(false);
                            ?>
                        </div>
                    </div>
                    <script>
                        function onlyNumberKey(evt) {

                            // Only ASCII character in that range allowed
                            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
                            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                                return false;
                            return true;
                        }
                    </script>
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin-top: 0px;padding-right: 0px;padding-left: 0px">

                            <?= $form->field(new \common\models\PubPublicacion(), 'precio_desde')->textInput(['oninput'=>"this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');", 'type' => 'text', 'maxlength' => 10, 'placeholder' => 'Precio desde..'])->label(false)?>

                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 txt_hasta" style="">
                            <?= $form->field(new \common\models\PubPublicacion(), 'precio_hasta')->textInput(['oninput'=>"this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');", 'type' => 'text', 'maxlength' => 10, 'placeholder' => 'Precio hasta'])->label(false)?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin-top: 0px;padding-left: 0px;padding-right: 0px">
                            <?=
                            $form->field(new \common\models\PubPublicacion(), 'ano_desde')->widget(\kartik\select2\Select2::classname(), [
                                'data' => [1980 => '1980',  1981 => '1981',     1982 => '1982',     1983 => '1983',     1984 => '1984',     1985 => '1985',     1986 => '1986',     1987 => '1987',     1988 => '1988',     1989 => '1989',     1990 => '1990',     1991 => '1991',     1992 => '1992',     1993 => '1993',     1994 => '1994',     1995 => '1995',     1996 => '1996',     1997 => '1997',     1998 => '1998',     1999 => '1999',     2000 => '2000',     2001 => '2001',     2002 => '2002',     2003 => '2003',     2004 => '2004',     2005 => '2005',     2006 => '2006',     2007 => '2007',     2008 => '2008',     2009 => '2009',     2010 => '2010',     2011 => '2011',     2012 => '2012',     2013 => '2013',     2014 => '2014',     2015 => '2015',     2016 => '2016',     2017 => '2017',     2018 => '2018',     2019 => '2019',     2020 => '2020',     2021 => '2021',
                                ],
                                'options' => ['placeholder' => 'Año Desde' ],
                                'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3" style="margin-top: 0px;padding-left: 0px;padding-right: 0px">
                            <?=
                            $form->field(new \common\models\PubPublicacion(), 'ano_hasta')->widget(\kartik\select2\Select2::classname(), [
                                'data' => [1980 => '1980',  1981 => '1981',     1982 => '1982',     1983 => '1983',     1984 => '1984',     1985 => '1985',     1986 => '1986',     1987 => '1987',     1988 => '1988',     1989 => '1989',     1990 => '1990',     1991 => '1991',     1992 => '1992',     1993 => '1993',     1994 => '1994',     1995 => '1995',     1996 => '1996',     1997 => '1997',     1998 => '1998',     1999 => '1999',     2000 => '2000',     2001 => '2001',     2002 => '2002',     2003 => '2003',     2004 => '2004',     2005 => '2005',     2006 => '2006',     2007 => '2007',     2008 => '2008',     2009 => '2009',     2010 => '2010',     2011 => '2011',     2012 => '2012',     2013 => '2013',     2014 => '2014',     2015 => '2015',     2016 => '2016',     2017 => '2017',     2018 => '2018',     2019 => '2019',     2020 => '2020',     2021 => '2021',
                                ],
                                'options' => ['placeholder' => 'Año Hasta' ],
                                'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                            ])->label(false);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12" style="margin-top: 0px;float: right; padding-right: 0px;">
                            <button id="btn_busq_filtros" class="btn btn-danger pull-right" style="background:  #F33B42;border-radius: 0px" type="button">Buscar</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>


        <hr class="hr-separador" style="clear:both;">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 bg-muted" style="padding-top: 10px;padding-bottom: 10px">
                <span class="titulo-home">Publicaciones Populares</span>
            </div>
        </div>
        <hr class="hr-separador">


        <!-- 1 lote anuncios -->
        <?php
        // echo "PRIMER LOTE";
        //\common\components\Utils::dump($anuncios_full[0]->attributes);
        //\common\components\Utils::dump(count($anuncios_full_st));

        $col=1;
        $con_prem=$con_stand=0;
        if(!$anuncios_full['premiun']){
            echo '<em style="color: #e46a76">- Sin Anuncios -</em>';
        }else{
            //PLAN PREMIUM
            for($i=0;$i < count($anuncios_full['premiun']);$i++)
            {
                $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['premiun'][$i]['id']) ->one();
                if($col==1 || $col==4) echo '<div class="row">';

                $img1=[];
                $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                foreach ($fotos1 as $foto){
                    $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">prem '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                }

                if($foto_primera)
                    $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                ?>
                <div class="col-lg-4 col-md-4 col-sm-4" >
                    <div class="ibox product-box">
                        <div>
                            <div id="padre" class="ibox-content no-padding border-left-right">
                                <?= $imgenes1 ?>
                                <div class="reco_cuadro">
                                    <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                </div>
                                <?php if($item->descuento>0){ ?>
                                    <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                        <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                    </div>
                                <?php }
                                //SI usuario logueado
                                if(!Yii::$app->user->isGuest){
                                    $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                    //Si No es Favorito: Pinta Corazón Blanco
                                    if($exist_fav==0){
                                        ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg danger ">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                        </div>
                                    <?php }
                                }else{?>
                                    <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" logueado="0" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                        <div class="vertical-timeline-icon navy-bg2  ">
                                            <i class="fa fa-heart-o"></i>
                                        </div>
                                    </div>

                                <?php }

                                ?>
                            </div>
                            <div class="ibox-content profile-content">
                                <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                <?php if($item->descuento >0){ ?>
                                    <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                <?php } ?>
                                <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                <p class="font-16">
                                    <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                        <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                    </a>
                                </p>
                                <p class="font-14">
                                    <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php


                $col++; $con_prem++;
                if($col==7) break;

                if($col==1 || $col==4)  echo '</div>';

            }//endfor

            //PLAN ESTANDARD
            $col=1; $j=0;
            if(count($anuncios_full['premiun']) < 6){

                for($j=0;$j<count($anuncios_full['standard']);$j++)
                {
                    $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['standard'][$j]['id']) ->one();
                    if($col==1 || $col==4) echo '<div class="row">';

                    $img1=[];
                    $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                    $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                    foreach ($fotos1 as $foto){
                        $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">stand '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                    }

                    if($foto_primera)
                        $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                    //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                    else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4" >
                        <div class="ibox product-box">
                            <div>
                                <div id="padre" class="ibox-content no-padding border-left-right">
                                    <?= $imgenes1 ?>
                                    <div class="reco_cuadro">
                                        <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                    </div>
                                    <?php if($item->descuento>0){ ?>
                                        <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                            <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                        </div>
                                    <?php }
                                    //SI usuario logueado
                                    if(!Yii::$app->user->isGuest){
                                        $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                        //Si No es Favorito: Pinta Corazón Blanco
                                        if($exist_fav==0){
                                            ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg2  ">
                                                    <i class="fa fa-heart-o"></i>
                                                </div>
                                            </div>
                                        <?php }else{ ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg danger ">
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                            </div>
                                        <?php }
                                    }else{?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" logueado="0" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>

                                    <?php }

                                    ?>
                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                    <?php if($item->descuento >0){ ?>
                                        <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                    <?php } ?>
                                    <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                    <p class="font-16">
                                        <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                            <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                        </a>
                                    </p>
                                    <p class="font-14">
                                        <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php


                    $col++;$con_stand++;
                    if($col==7) break;

                    if($col==1 || $col==4)  echo '</div>';

                }//endfor
            }
        }



        ?>


        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?= yii\bootstrap\Carousel::widget(['items'=>$images, 'options'=>['class'=>'ove2']]) ?>
            </div>
        </div>
        <hr class="hr-separador">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 bg-muted" style="padding-top: 10px;padding-bottom: 10px">
                <span class="titulo-home">LAS MÁS BUSCADAS...</span>
            </div>
        </div>
        <hr class="hr-separador">

        <!-- 2 lote anuncios -->
        <?php

        //  echo "SEGUNDO LOTE: con_prem=".$con_prem ." -  con_stand: ".$con_stand ;
        //   \common\components\Utils::dump(count($anuncios_full2));
        //     \common\components\Utils::dump(count($anuncios_full2_st));
        $col=1;
        if(!$anuncios_full ){
            echo '<em style="color: #e46a76">- Sin Anuncios -</em>';
        }else{

            //PLAN PREMIUM
            for($i=$con_prem;$i<count($anuncios_full['premiun']);$i++)
            {
                $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['premiun'][$i]['id']) ->one();

                if($col==1 || $col==4) echo '<div class="row">';
                $img2=[];
                $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                foreach ($fotos1 as $foto){
                    $img2[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">prem '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                }
                if($foto_primera)
                    $imgenes2 =  yii\bootstrap\Carousel::widget(['items' => $img2, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                else  $imgenes2 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';
                ?>

                <div class="col-lg-4 col-md-4 col-sm-4" >
                    <div class="ibox product-box">
                        <div>
                            <div id="padre" class="ibox-content no-padding border-left-right">
                                <?= $imgenes2 ?>
                                <div class="reco_cuadro">
                                    <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                </div>
                                <?php if($item->descuento>0){ ?>
                                    <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                        <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                    </div>
                                <?php }
                                //SI usuario logueado
                                if(!Yii::$app->user->isGuest){
                                    $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                    //Si No es Favorito: Pinta Corazón Blanco
                                    if($exist_fav==0){
                                        ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg danger ">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                        </div>
                                    <?php }
                                }else{?>
                                    <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                        <div class="vertical-timeline-icon navy-bg2  ">
                                            <i class="fa fa-heart-o"></i>
                                        </div>
                                    </div>

                                <?php }

                                ?>
                            </div>
                            <div class="ibox-content profile-content">
                                <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                <?php if($item->descuento >0){ ?>
                                    <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                <?php } ?>
                                <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                <p class="font-16">
                                    <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                        <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                    </a>
                                </p>
                                <p class="font-14">
                                    <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

                $col++; $con_prem++;
                if($col==7) break;

                if($col==1 || $col==4)  echo '</div>';

            }//endfor


            //PLAN ESTANDARD
            if(count($anuncios_full) < 12){

                for($j=$con_stand;$j<count($anuncios_full['standard']);$j++)
                {
                    $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['standard'][$j]['id']) ->one();

                    if($col==1 || $col==4) echo '<div class="row">';
                    $img2=[];
                    $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                    $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                    foreach ($fotos1 as $foto){
                        $img2[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">stand '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                    }
                    if($foto_primera)
                        $imgenes2 =  yii\bootstrap\Carousel::widget(['items' => $img2, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                    else  $imgenes2 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4" >
                        <div class="ibox product-box">
                            <div>
                                <div id="padre" class="ibox-content no-padding border-left-right">
                                    <?= $imgenes2 ?>
                                    <div class="reco_cuadro">
                                        <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                    </div>
                                    <?php if($item->descuento>0){ ?>
                                        <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                            <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                        </div>
                                    <?php }
                                    //SI usuario logueado
                                    if(!Yii::$app->user->isGuest){
                                        $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                        //Si No es Favorito: Pinta Corazón Blanco
                                        if($exist_fav==0){
                                            ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg2  ">
                                                    <i class="fa fa-heart-o"></i>
                                                </div>
                                            </div>
                                        <?php }else{ ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg danger ">
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                            </div>
                                        <?php }
                                    }else{?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>

                                    <?php }

                                    ?>
                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                    <?php if($item->descuento >0){ ?>
                                        <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                    <?php } ?>
                                    <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                    <p class="font-16">
                                        <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                            <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                        </a>
                                    </p>
                                    <p class="font-14">
                                        <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php



                    $col++;$con_stand++;
                    if($col==7) break;

                    if($col==1 || $col==4)  echo '</div>';

                }//endfor
            }
        }
        echo '</div>';

        //\common\components\Utils::dumpx(222222);
        ?>
        <hr class="hr-separador">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">

                <?= yii\bootstrap\Carousel::widget(['items'=>$imagesIZQ, 'options'=>['class'=>'ove2']]) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">

                <?= yii\bootstrap\Carousel::widget(['items'=>$imagesDER, 'options'=>['class'=>'ove2']]) ?>
            </div>
        </div>
        <hr class="hr-separador">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 bg-muted" style="padding-top: 10px;padding-bottom: 10px">
                <span class="titulo-home">NUESTRAS RECOMENDADAS...</span>
            </div>
        </div>
        <hr class="hr-separador">
        <!-- 3 lote anuncios -->
        <?php
        //echo "TERCER LOTE: con_prem=".$con_prem ." -  con_stand: ".$con_stand ;

        //   \common\components\Utils::dump(count($anuncios_full3));
        //   \common\components\Utils::dump(count($anuncios_full3_st));
        $col=1;
        if(!$anuncios_full){
            echo '<em style="color: #e46a76">- Sin Anuncios -</em><hr>';
        }else{

            //PLAN PREMIUM
            for($i=$con_prem;$i<count($anuncios_full['premiun']);$i++)
            {
                $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['premiun'][$i]['id']) ->one();
                if($col==1 || $col==4) echo '<div class="row">';
                $img3=[];
                $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                foreach ($fotos1 as $foto){
                    $img3[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">Prem '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                }
                if($foto_primera)
                    $imgenes3 =  yii\bootstrap\Carousel::widget(['items' => $img3, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);

                else  $imgenes3 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                ?>

                <div class="col-lg-4 col-md-4 col-sm-4" >
                    <div class="ibox product-box">
                        <div>
                            <div id="padre" class="ibox-content no-padding border-left-right">
                                <?= $imgenes3 ?>
                                <div class="reco_cuadro">
                                    <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                </div>
                                <?php if($item->descuento>0){ ?>
                                    <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                        <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                    </div>
                                <?php }
                                //SI usuario logueado
                                if(!Yii::$app->user->isGuest){
                                    $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                    //Si No es Favorito: Pinta Corazón Blanco
                                    if($exist_fav==0){
                                        ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg danger ">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                        </div>
                                    <?php }
                                }else{?>
                                    <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                        <div class="vertical-timeline-icon navy-bg2  ">
                                            <i class="fa fa-heart-o"></i>
                                        </div>
                                    </div>

                                <?php }

                                ?>
                            </div>
                            <div class="ibox-content profile-content">
                                <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                <?php if($item->descuento >0){ ?>
                                    <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                <?php } ?>
                                <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                <p class="font-16">
                                    <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                        <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                    </a>
                                </p>
                                <p class="font-14">
                                    <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php


                $col++;
                if($col==7) break;

                if($col==1 || $col==4)  echo '</div>';

            }//endfor


            //PLAN ESTANDARD
            if(count($anuncios_full) < 18){

                for($j=$con_stand;$j<count($anuncios_full['standard']);$j++)
                {
                    $item = \common\models\PubPublicacion::find()->where('id='.$anuncios_full['standard'][$j]['id']) ->one();

                    if($col==1 || $col==4) echo '<div class="row">';
                    $img3=[];
                    $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->limit(1)->one();
                    $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order ASC')->all();
                    foreach ($fotos1 as $foto){
                        $img3[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">stand '.$item->id.'<img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 268px; max-width:1170px    display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                    }
                    if($foto_primera)
                        $imgenes3 =  yii\bootstrap\Carousel::widget(['items' => $img3, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);

                    else  $imgenes3 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4" >
                        <div class="ibox product-box">
                            <div>
                                <div id="padre" class="ibox-content no-padding border-left-right">
                                    <?= $imgenes3 ?>
                                    <div class="reco_cuadro">
                                        <span class="reco_texto"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                    </div>
                                    <?php if($item->descuento>0){ ?>
                                        <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                            <span class="desc_texto">- <?= $item->descuento ?>%</span>
                                        </div>
                                    <?php }
                                    //SI usuario logueado
                                    if(!Yii::$app->user->isGuest){
                                        $exist_fav = PubFavoritos::find()->where('id_publicacion='.$item->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                        //Si No es Favorito: Pinta Corazón Blanco
                                        if($exist_fav==0){
                                            ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg2  ">
                                                    <i class="fa fa-heart-o"></i>
                                                </div>
                                            </div>
                                        <?php }else{ ?>
                                            <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                                <div class="vertical-timeline-icon navy-bg danger ">
                                                    <i class="fa fa-heart"></i>
                                                </div>
                                            </div>
                                        <?php }
                                    }else{?>
                                        <div id="fav_<?= $item->id ?>" class="favorito btn_favorito" id_pub="<?= $item->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                                            <div class="vertical-timeline-icon navy-bg2  ">
                                                <i class="fa fa-heart-o"></i>
                                            </div>
                                        </div>

                                    <?php }

                                    ?>
                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><strong class="font_precio_mons"><?= ($item->descuento > 0) ? 'Ahora':'' ?> $<?= ($item->descuento > 0) ? number_format(($item->precio - ($item->precio*($item->descuento/100))) ,0,',','.') : number_format($item->precio,0,',','.') ?> </strong></h4>
                                    <?php if($item->descuento >0){ ?>
                                        <p class="sub2" <?= ($item->descuento == 0) ? 'style="text-decoration-line: none !important;"' : ''  ?>> <?= ($item->descuento > 0) ? 'Antes $'.number_format($item->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' ?></p>
                                    <?php } ?>
                                    <p class="font-14"><?= $item->ano ?> - <?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></p>
                                    <p class="font-16">
                                        <a  href="<?= Url::to(['site/pubdetallepublico','id'=> $item->id]) ?>" class="titulito">
                                            <?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>
                                        </a>
                                    </p>
                                    <p class="font-14">
                                        <?= ucfirst(strtolower($item->ciudad->name)).', '.ucfirst(strtolower($item->ciudad->state->name))?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php


                    $col++;
                    if($col==7) break;

                    if($col==1 || $col==4)  echo '</div>';

                }//endfor
            }
        }
        echo '</div>';


        ?>





        <!-- 5 AÑOS DE EXP VTM -->

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?= yii\bootstrap\Carousel::widget(['items'=> $imagesABA, 'options'=>['id'=>'w100', 'class'=>'ove2']]) ?>
            </div>
        </div>

        <hr class="hr-separador2">

        <!-- COUNTERS: ESPECIALIZADOS EN MOTOS -->

        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 text-left">
                <hr style="padding: 0px;border: 1px solid red" />
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 text-center">

                <span class="txt-especializados">Especializados en venta de motos</span>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 text-left">
                <hr style="padding: 0px;border: 1px solid red" />
            </div>
        </div>
        <hr class="hr-separador2">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: -webkit-center">
                    <div class="counter blue">
                        <table>
                            <tr>
                                <td style="vertical-align: top"><img alt="image" style="width: auto" class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/target-audience.png'?>"></td>
                                <td style="margin-left: 10px">
                                    <span class="counter-value" style="margin-left: 10px">25000</span><span style="font-weight: bold;font-size: 32px">+</span>
                                    <h4 style="margin-left: 10px">Visitas nuevas <br>semanales</h4>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: -webkit-center">
                    <div class="counter blue">
                        <table>
                            <tr>
                                <td style="vertical-align: top"><img alt="image" style="width: auto" class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/marketing-de-medios-sociales.png'?>"></td>
                                <td>
                                    <span class="counter-value" style="margin-left: 10px">195000</span><span style="font-weight: bold;font-size: 32px">+</span>
                                    <h4 style="margin-left: 10px">Seguidores en instagram <br>y Facebook</h4>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
                <div class="col-md-4 col-sm-4 col-lg-4" style="text-align: -webkit-center">
                    <div class="counter blue">
                        <table>
                            <tr>
                                <td style="vertical-align: top"><img alt="image" style="width: auto" class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/action-plan.png'?>"></td>
                                <td>
                                    <span class="counter-value" style="margin-left: 10px">1000</span><span style="font-weight: bold;font-size: 32px">+</span>
                                    <h4 style="margin-left: 10px">Vehículos vendidos <br>a nivel nacional</h4>
                                </td>
                            </tr>
                        </table>
                    </div>


                </div>
            </div>
        </div>

        <hr class="hr-separador2">





    </div>
</div>




<script id="jsbin-javascript">
    /*
    var slider = new Slider("#slider1");
    slider.on("slide", function(slideEvt) {
        console.log(slider.getValue() );
    });*/
</script>

<style>

    .btn-facebook {
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        padding: 8px;
        color: #969696;font-size: 20px;
    }
    .btn-google {
        /* color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        color: #969696;
        font-size: 20px;
    }

    .btn-login {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px #fff;
        box-shadow: 0 1px 1px #fff;
    }
    .panel-heading .box {
        padding: 18px 0 6px;
        text-align: center;
    }

    .panel-heading .line {
        margin-top: 4px;
        margin-bottom: 20px;
        border: 0;
        width: 100%;
        border-top: 1px solid #c5c5c54d;

    }

    .panel-heading .active .line {
        border-top: 3px solid #F33B42;

    }


    .panel-heading .text {
        color: #000;
        font-weight: bold;
    }

    .panel-heading .text2{
        color: #000;
        font-weight: normal;
    }

    .panel-heading .text, .text2:hover,
    .panel-heading .text, .text2:focus {
        text-decoration: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid #fff;
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .card-header {
        margin-bottom: 0;
        padding: 1.25rem 1.5rem;
        border-bottom: 1px solid rgba(0, 0, 0, .05);
        background-color: #fff;
    }

    .card-header:first-child {
        border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    }

    .shadow {
        /* box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;*/
    }
    #signupform-acepto_terminos {
        display: block;
    }

    .tambien{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 18px;

        letter-spacing: 0.0278198px;

        color: #969696;
    }

    .titulos_form{
        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;
        font-weight: normal;
        color: #F33B42 !important;
    }
    .help-block{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 14px;
        /* identical to box height */


        text-align: right;
        letter-spacing: 0.0218584px;

        color: #353435 !important;
    }

    .rosado_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #FF596A;
    }

    .negro_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #353435;
    }

    .normal_text{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: 0.0278198px;

        color: #353435;
    }

    .normal_text2{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: 0.0278198px;

        color: #353435;
    }
    .box{
        box-shadow: none;
    }
</style>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<?php
use yii\helpers\Html;
use yii\authclient\widgets\AuthChoice;

\yii\bootstrap\Modal::begin([
    'header' => '<h4 class="text-danger  modal-title">Debes iniciar sesión para realizar la acción</h4>',
    'id'     => 'modal-login',
    'size' => 'modal-lg',
    'clientOptions' => [

        //'backdrop' => 'static',
        'keyboard' => false],
    'footer' => null,
]); ?>
<input type="hidden" id="param" name="param">
<?php

$model = new LoginForm();
$modelSignup = new SignupForm();
$modelCliU = new CliUsuario();
/*
echo $this->render('loginpopup', [
    'model' => $model,'modelSignup' => $modelSignup,'modelCliU' => $modelCliU,
])
*/
?>
<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->


    <div class="panel panel-login card bg-secondary shadow border-0" style="width: 80%;height: auto;
    margin: 0 auto;">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6 box active" id="login-form-link">
                    <a href="#" class="text txt_is">INICIAR SESIÓN</a>
                    <hr class="line">
                </div>
                <div class="col-xs-6 box" id="register-form-link">
                    <a href="#" class="text2 txt_reg" style="">REGISTRATE</a>
                    <hr class="line">
                </div>
            </div>
            <!-- <hr> -->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=>false,'method'=>'post',
                        'options' => ['autocomplete'=>'random']
                    ]); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($model, 'username')->textInput(['autofocus222' => true ,'tabindex22' => '1',])->label('<span class="titulos_form">Correo electrónico</span>') ?>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($model, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>

                            <div class="pull-right" style="margin-bottom: 15px"><?= Html::a('<span class="titulos_form">Olvidé mi contraseña</span>', ['site/request-password-reset']) ?></div>

                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#F33B42;font-size:11px', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    </div>



                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>
                    <p class="tambien" style="font-weight: normal">También puedes ingresar con</p>
                    <hr style="width: 70%;text-align: center">
                    <div class="social-button-login">
                        <div class="row">
                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-3 col-sm-offset-3" style="margin: 0.5em;color:#969696">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <?php ActiveForm::end(); ?>

                    <!--




                    -->
                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(
                        [
                            'id' => 'register-form',
                            'enableClientValidation'=>false,
                            'enableAjaxValidation'=> false,
                            'options' => ['style' => 'display: none;', 'autocomplete'=>'random']
                        ]);
                    ?>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'nombre')->textInput()->label( '<span class="titulos_form">Nombre completo</span>')?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'email')->textInput()->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'telefonos')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de contacto</span>')?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'whatssapp')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de WhatsApp</span>')?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('<span class="titulos_form">Confirmar de contraseña</span>') ?>
                        </div>
                    </div>
                    <p class="tambien" style="margin-top: 10px">&nbsp;</p>
                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>


                    <div class="social-button-login">
                        <p class="tambien" style="margin-top: 10px">También puedes ingresar con</p>
                        <hr style="    margin-top: 0px;    margin-bottom: 9px;    border: 0;    border-top: 1px solid #eeeeee;">
                        <div class="row">

                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <div class="row">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">


                                    <table class="table" style="margin-top: 20px">
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-promocionales" name="SignupForm[promocionales]" value="1" aria-required="true">
                                            </td>
                                            <td class="normal_text" style="border: 0px">
                                                Me gustaría recibir comunicaciones promocionales (Recibirás un e-mail de confirmación)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-acepto_terminos" name="SignupForm[acepto_terminos]" value="1" aria-required="true">
                                            </td>
                                            <td style="border: 0px" class="normal_text2">Declaro que he leído y acepto la nueva <a target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>" class="rosado_aref" style="">Política de privacidad</a> y los
                                                <a class="rosado_aref" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de VENDETUMOTO.CO
                                            </td>
                                        </tr>
                                    </table>


                                    <?= $form->field($modelSignup, 'promocionales')->hiddenInput()->label(false) ?>
                                    <?= $form->field($modelSignup, 'acepto_terminos')->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Registrarme', ['class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#FF596A;font-size:11px', 'id'=>'btn_reg', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <!-- ############################################### -->
                </div>

            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::end();


?>



