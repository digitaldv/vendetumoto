<?php

use common\models\PubFavoritos;
use yii\widgets\ActiveForm;
use yii\web\View;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;
use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\CliUsuario;


$this->title = 'Vendetumoto.co | ¡La tienda de motos más grande de Colombia!';

$this->registerJs(" 
 
     $('.btn_suscribirme').click(function(e){
         
        e.preventDefault();
                  
        if($('#txt_email').val()!=''){
          
           $('#txt_email').val($('#txt_email').val().replace(/ /g,''));
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(!re.test(String($('#txt_email').val()).toLowerCase())){
                alert('ERROR: ¡EMAIL INVÁLIDO! Debes ingresar un email correcto.');
                document.getElementById('txt_email').focus();
                return false;
           }
           
           //if($('input[name=check_suscrib]:checked').val()==1){
                 //Grabo en BD y envío un email al suscriptor
                 $.post('".Url::to(['site/addsuscriptor'])."', {email: $('#txt_email').val() }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==1)  {                    
                               alert('¡Felicitaciones! Tu email ha quedado inscrito en nuestro NewsLetter.');
                            }else {                                  
                                   alert('Información: ¡El email ingresado YA está inscrito en nuestro NewsLetter!');
                            }                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
//           }else{
//             alert('ERROR: Debes aceptar los términos y condiciones para aceptación de tratamiento de datos y envío de información.');
//             return false;
//           }
        
        }else{
           alert('¡Debes ingresar un email!');
        }
   });
");
?>




<style>
    b22ody{
        background: #000000;
    }
    body {
        background: url('<?= Yii::$app->request->baseUrl . '/img/fondo-prox.png'?>');
        background-repeat: repeat-y;
        background-position: top;
    }

    .pronto{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 700;
        font-size: 23px;
        line-height: 28px;
        letter-spacing: 0.0732933px;
        color: #FFFFFF;
    }
    .flexbox {
        margin-top: 100px;
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .subtitulo{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 700;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        letter-spacing: 0.0509866px;

        color: #FFFFFF;

    }

    .placeholder{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 300;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        letter-spacing: 0.0446133px;
        color: #FFFFFF;
        background: #000;
        padding: 5px;
        border: 1px solid #fff;
        width: 250px;
        height: 38px;
    }

    .enviar{
        font-family: 'Montserrat' !important;
        font-style: normal !important;
        font-weight: 300 !important;
        font-size: 14px !important;
        line-height: 17px !important;
        /* identical to box height */

        text-align: center !important;
        letter-spacing: 0.0446133px !important;

        color: #FFFFFF !important;
        padding: 5px;
        border: 1px solid #fff;
        cursor: pointer;
    }
    .what{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 400;
        font-size: 14px;
        line-height: 17px;
        /* identical to box height */

        text-align: center;
        letter-spacing: 0.0446133px;

        color: #FFFFFF;
    }

    .num{
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 400;
        font-size: 42px;
        line-height: 51px;
        letter-spacing: 0.13384px;

        color: #FFFFFF;
    }
    .sub2{
        text-align: center;
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 300;
        font-size: 13px;
        line-height: 16px;
        /* identical to box height */

        letter-spacing: 0.0414266px;

        color: #FFFFFF;
    }

    .sub1{
        text-align: center;
    }

    .tab{
        padding: 10px;
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', () => {

        //===
        // VARIABLES
        //===
        const DATE_TARGET = new Date('07/15/2022 0:01 AM');
        // DOM for render
        const SPAN_DAYS = document.querySelector('span#days');
        const SPAN_HOURS = document.querySelector('span#hours');
        const SPAN_MINUTES = document.querySelector('span#minutes');
        const SPAN_SECONDS = document.querySelector('span#seconds');
        // Milliseconds for the calculations
        const MILLISECONDS_OF_A_SECOND = 1000;
        const MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
        const MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
        const MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24

        //===
        // FUNCTIONS
        //===

        /**
         * Method that updates the countdown and the sample
         */
        function updateCountdown() {
            // Calcs
            const NOW = new Date()
            const DURATION = DATE_TARGET - NOW;
            const REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
            const REMAINING_HOURS = Math.floor((DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR);
            const REMAINING_MINUTES = Math.floor((DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE);
            const REMAINING_SECONDS = Math.floor((DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND);
            // Thanks Pablo Monteserín (https://pablomonteserin.com/cuenta-regresiva/)

            // Render
            SPAN_DAYS.textContent = REMAINING_DAYS;
            SPAN_HOURS.textContent = REMAINING_HOURS;
            SPAN_MINUTES.textContent = REMAINING_MINUTES;
            SPAN_SECONDS.textContent = REMAINING_SECONDS;
        }

        //===
        // INIT
        //===
        updateCountdown();
        // Refresh every second
        setInterval(updateCountdown, MILLISECONDS_OF_A_SECOND);
    });
</script>

<div class="bg">
    <center style="">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <p class="pronto flexbox col-lg-12">
                    ¡Pronto estaremos al aire!
                </p>
                <p>
                    <img src="<?= Yii::$app->request->baseUrl.'/img/logo2.png' ?>">
                </p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <section style="width: 40%">
                  <span style="border: 0.5px solid #FFFFFF;border-radius: 10px; display: flex; justify-content: center; ">

                      <table class="tab">
                        <tr><td class="sub1"><span id="days" class="num"></span></td></tr>
                        <tr><td class="sub2">dias</td></tr>
                      </table>
                      <table class="tab">
                          <tr><td class="sub1"><span id="hours" class="num"></span></td></tr>
                          <tr><td class="sub2">horas</td></tr>
                      </table>
                      <table class="tab">
                          <tr><td class="sub1"><span id="minutes" class="num"></span></td></tr>
                          <tr><td class="sub2">minutos</td></tr>
                      </table>
                      <table class="tab">
                          <tr><td class="sub1"><span id="seconds" class="num"></span></td></tr>
                          <tr><td class="sub2">segundos</td></tr>
                      </table>

                  </span>

                </section>

            </div>
            <br>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <p class="subtitulo">
                    Suscríbete a nuestro newsletter para recibir información de nuestro lanzamiento y <br>grandes sorpresas. Recuerda seguirnos en nuestras Redes Sociales
                </p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">

                <table style="width: 10%; " cellspacing="0">
                    <tr>
                        <td style="display: flex; justify-content: center; width: 100%;">
                            <input id="txt_email" type="email" class="form-control placeholder" placeholder="Correo electrónico">
                            <button title="Click para suscribirte"   class="btn  btn-danger btn_suscribirme enviar" style="background:  #F33B42;border-radius: 0px" type="button">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suscríbirse&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>

                            <a title="Seguir en Facebook" target="_blank" href="https://www.facebook.com/vendetumoto.co"><img style="margin-left: 20px" src="<?= Yii::$app->request->baseUrl.'/img/face-prox.png' ?>"></a>
                            <a title="Seguir en Instagram"  target="_blank" href="https://www.instagram.com/vendetumoto.co/"><img style="margin-left: 10px" src="<?= Yii::$app->request->baseUrl.'/img/inst-prox.png' ?>"></a>
                        </td>
                    </tr>
                </table>

            </div>
            <br><br>
            <div>
                <span class="what" >Para mayor información puedes ponerte en contacto con nosotros vía WhatsApp</span>
            </div>
            <br>
            <div>
              <span style="" >
                  <a title="Escribir mensaje por WhatsApp" target="_blank" href="https://api.whatsapp.com/send?phone=573164138276&text=Hola, quiero más información de Vende Tu Moto...">
                      <img src="<?= Yii::$app->request->baseUrl.'/img/escribenos.png' ?>">
                  </a>
              </span>
            </div>

        </div>
    </center>


</div>



