<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use Box\Spout\Common\Entity\Style\Style;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("   

    $('#login-form-link').click(function(e) {
        $('#login-form').delay(100).fadeIn(100);
        $('#register-form').fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $('#register-form').delay(100).fadeIn(100);
        $('#login-form').fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });


");


?>

<link href='<?= Yii::$app->request->baseUrl . '/js/slider/style.css' ?>' rel='stylesheet' />
<style>
    .btn-facebook {
        /* color: #fff;
        background-color: #3b5998;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #b5a2a2;
        border-radius: 5px;
        padding: 8px;
    }

    .btn-google {
        /* color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #b5a2a2;
        border-radius: 5px;
    }

    .btn-login {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }

    .panel-heading .box {
        padding: 18px 0 6px;
        text-align: center;
    }

    .panel-heading .line {
        margin-top: 4px;
        margin-bottom: 20px;
        border: 0;
        width: 70%;
    }

    .panel-heading .active .line {
        border-top: 3px solid #F33B42;
    }


    .panel-heading .text {
        color: #000;
        font-weight: 700;
    }

    .panel-heading .text:hover,
    .panel-heading .text:focus {
        text-decoration: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid rgba(0, 0, 0, .05);
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .card-header {
        margin-bottom: 0;
        padding: 1.25rem 1.5rem;
        border-bottom: 1px solid rgba(0, 0, 0, .05);
        background-color: #fff;
    }

    .card-header:first-child {
        border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    }

    .shadow {
        box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;
    }
    #signupform-acepto_terminos {
        display: block;
    }
</style>

<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="panel panel-login card bg-secondary shadow border-0">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6 box active" id="login-form-link">
                                <a href="#" class="text">INICIAR SESIÓN</a>
                                <hr class="line">
                            </div>
                            <div class="col-xs-6 box" id="register-form-link">
                                <a href="#" class="text">REGISTRATE</a>
                                <hr class="line">
                            </div>
                        </div>
                        <!-- <hr> -->
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- ############################################### -->
                                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Correo electrónico') ?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($model, 'password')->passwordInput()->label('Contraseña') ?>
                                    </div>
                                    <!-- <?= $form->field($model, 'rememberMe')->checkbox() ?> -->
                                </div>
                                <div class="col-lg-12">
                                    <div class="pull-right" style="padding-right: 10px; margin-bottom: 15px"><?= Html::a('Olvidé mi contraseña', ['site/request-password-reset']) ?></div>
                                </div>
                                <div class="form-group" style="margin-bottom: 30px;">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'name' => 'login-button']) ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div style="color:#999;margin:1em 0">
                                                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                                <br>
                                                Need new verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
                                            </div> -->

                                <!-- start auth facebook y google -->
                                <?php
                                $authAuthChoice = AuthChoice::begin([
                                    'baseAuthUrl' => ['auth'],
                                    'options' => ['class' => 'social-auth-links text-center'],
                                ])
                                ?>
                                <p>Tambien puedes ingresar con</p>
                                <div class="social-button-login">
                                    <div class="row">
                                        <div class="" style="display: flex; justify-content: center">
                                            <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                                <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                                    <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                                    <?php $class = $name === 'live' ? 'windows' : $name ?>
                                                    <?php $text = $class === 'facebook' ?
                                                        sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                                        sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                                    ?>
                                                    <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </div>
                                <?php AuthChoice::end() ?>
                                <!-- end auth facebook y google -->
                                <?php ActiveForm::end(); ?>

                                <!-- ############################################### -->
                                <?php $form = ActiveForm::begin(['id' => 'register-form',  'options' => ['style' => 'display: none;']]); ?>

                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <?= $form->field($modelCliU, 'nombre')->label('Nombre completo')?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($modelSignup, 'email')->textInput(['autofocus' => true])->label('Correo electrónico') ?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($modelCliU, 'telefonos')->label('Número de contacto (fijo o celular)')?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($modelCliU, 'whatssapp')->label('Número de WhatsApp1')?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($modelSignup, 'password')->passwordInput()->label('Contraseña') ?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('Confirmación de contraseña') ?>
                                    </div>
                                </div>

                                <!-- start auth facebook y google -->
                                <?php
                                $authAuthChoice = AuthChoice::begin([
                                    'baseAuthUrl' => ['auth'],
                                    'options' => ['class' => 'social-auth-links text-center'],
                                ])
                                ?>
                                <p>Tambien puedes ingresar con</p>
                                <div class="social-button-login">
                                    <div class="row">
                                        <div class="" style="display: flex; justify-content: center">
                                            <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                                <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                                    <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                                    <?php $class = $name === 'live' ? 'windows' : $name ?>
                                                    <?php $text = $class === 'facebook' ?
                                                        sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                                        sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                                    ?>
                                                    <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </div>
                                <?php AuthChoice::end() ?>
                                <!-- end auth facebook y google -->

                                <div class="form-group" style="margin-bottom: 30px;">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <?= $form->field($modelSignup, 'acepto_terminos')->label('Acepto términos y condiciones')->checkbox() ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 30px;">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <?= Html::submitButton('REGISTRATE', ['class' => 'form-control btn btn-login', 'name' => 'signup-button']) ?>
                                        </div>
                                    </div>
                                </div>

                                <?php ActiveForm::end(); ?>
                                <!-- ############################################### -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
