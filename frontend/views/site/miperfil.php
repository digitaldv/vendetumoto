<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$this->title = 'Mi Perfil';
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(' 
jQuery(document).ready(function () {


});');
?>
<style>
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
    }

    .box.box-solid.box-default>.box-header {
        color: #444;
        background: #d2d6de;
        background-color: #d2d6de;
    }
    .box.box-solid.box-default {
        border: 1px solid #d2d6de;
    }
    .box.box-solid.box-default>.box-header {
        color: #444;
        background: #d2d6de;
        background-color: #d2d6de;
    }

    .box-header.with-border {
        border-bottom: 1px solid #f4f4f4;
    }
    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
    }
    .file-caption.icon-visible .file-caption-name {
          padding-left: 2px;
        font-size: 10px;
    }

    .titulo{
        font-family: Nunito;
        font-style: normal;
        font-weight: 600;
        font-size: 29px;
        line-height: 11px;
        /* identical to box height, or 38% */

        letter-spacing: 0.828571px;

        color: #474747;
    }

    .subtitulo{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 15px;
        line-height: 23px;
        /* or 153% */

        letter-spacing: 0.460577px;

        color: #474747;
    }

    .control-label{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: bold;
        font-size: 12px;
        line-height: 15px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;
        color: #FF596A;
    }
    .image-cropper {
        width: 270px;
        height: 270px;
        position: relative;
        overflow: hidden;
        border-radius: 50%;
    }

    img {
        display: inline;
        margin: 0 auto;
        height: 100%;
        width: auto;
    }

   /* .field-cliusuario-imagen{
        margin-top: -28px;
        float: left;
        left: 10px;
        margin-left: 0px;
    }*/
    .input-group {
        position: relative;
        display: block;
        border-collapse: separate;
    }

    .kv-fileinput-caption::placeholder{
        color:#ffffff !important;
    }
    .kv-fileinput-caption{
        color:#ffffff !important;
    }

    .kv-fileinput-caption{
        color:#e46a76;
        font-size: 10px !important;
        border: 0px !important;
        box-shadow: inset 0 1px 1px rgb(255 255 255) !important;
    }
    .file-caption-name{
        border: 0px !important; font-size: 10px !important;
        box-shadow: inset 0 1px 1px rgb(255 255 255) !important;
    }
    . is-valid::placeholder{
        color: #e46a76 !important;
        border: 0px !important; font-size: 10px !important;
        box-shadow: inset 0 1px 1px rgb(255 255 255) !important;
    }

    .btn-file, .btn_borrar{
        background: transparent !important;
        color: #e46a76 !important;
        border: 0px;
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: bold;
        font-size: 15px;
        line-height: 19px;
        /* identical to box height */

       /* display: flex;*/
        align-items: center;
        letter-spacing: 0.0298069px;

        color: #F33B42;
    }
   .btn-vtm{
       background: #F33B42;
       border-radius: 20px;
       font-family: Source Sans Pro;
       font-style: normal;
       font-weight: bold;
       font-size: 14px;
       line-height: 18px;
       /* identical to box height */
       padding: 0px !important;

       align-items: center;
       letter-spacing: 0.844486px;

       color: #FFFFFF;
   }
    .form-group.required .has-star:not(.custom-control-label):not(.custom-file-label)::after, .is-required::after {
        content: "*";
        margin-left: 3px;
        font-weight: normal;
        font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
        color: white;
    }

    .kv-fileinput-caption::placeholder{
        color: white !important;
    }


    img {
        display: inline;
        margin: 0 auto;
        height: auto;
        width: auto;
    }
</style>

<div  class="col-sm-12 col-md-12 col-lg-12" style="border: 0px solid ;background: #EAF1F6;height: 70px;padding-top: 20px;">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
            </a>


        </div>
    </div>
</div>


    <div class="row" style="margin-top: 100px;margin-left: 6px;">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <span class="titulo"> Datos personales</span>
            <h5 class="subtitulo">A continuación podrá actualizar la información básica de su cuenta:</h5>
        </div>
    </div>

    <div class="row" style="margin: 20px">
        <div class="" style="background: #FFFFFF; border-radius: 9px;">
            <div class="site-about" >

                <?php  $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['enctype' => 'multipart/form-data'], // important
                ]);
                ?>

                <div class="row">
                    <div class=" col-lg-2 col-md-2 col-sm-2">


                                <?php
                                if ($model->imagen) {
                                    ?>
                                    <img  style="    margin-top: 21px;
    margin-left: 20px;  " class="rounded profile-user-img img-responsive img-responsive" src="<?= $model->imagen ?>" class="user-image" alt="<?php echo $model->nombre ?>"  title="Mi Avatar" />
                                    <br>
                                <?php } else { ?>
                                    <img style="    margin-top: 21px;
    margin-left: 20px;   " class="rounded  profile-user-img img-responsive img-responsive" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOIAAADiCAYAAABTEBvXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAByhSURBVHgB7Z0LcFxXece/c3dXD1uWZCfxO8k6IRDbIZZJKbYDeB2muARau9MmLQwQGwxDeYxjQVpoh1hKhzBAkG2mEKbE2AbKw+Zhd9IkTlu8hjwoM8UyxE6ICdrUTpzEDZafkqXde3q+c/dKu9qH7u7e17nn+82sd7XaKLb2/vf/ne9xDgMiFPCBQ52QSCTBNJIQhyRw3gmGcTUw1ikfg3hu7MXiOZDPlYdBJv+6QWBc3MQ9F/em+bx8nBXfZzlxn+1nC5YOAhE4DAhfEYJLCsGlhCCSUmicdYEUWRVheQpDIWbAELdc7jDkWD8YZobNv6EfCN8gIXrImOiM+BLhTF2W6IISXK1IN+0Xf+d+yJoHUaBswcIMEJ5AQnSRfHi5FmKxleLLFPCCcDIaZMS/KQ08dxBGY2kSpnuQEBuEH/9NCmKJNcI91kZQeJPAMHxNQ250H7vy9Wkg6oaEWCPS9eLxLnG7Q4Rta9UJNb1GJoT2Qo7vY1ct2gtETZAQHSKdj8TnkLwozewuckpnkBCrIN2vpWWjEN+dJL66sdaVI9BLa8rKkBDLkHe/zeICSgHhHgzXk7ldojSyE4giSIh5yP18JQNmrpcyr+NoL0QSYJDk15IUtuorRH7y6aS4w/BzHRDBw2GnzoLUTogkwJCjqSC1ESKFoIqhmSAjL0QSoNJkxHu2jc1dtBUiTqSFaLWfxXcA6NZ6FjlkljXKZY9ICjG/DtxBdcCIEeFw1YCIwV96BsPQQyTCCMJEgq2ZHRIftJshYkTGEfmJp7ogFttCAtSGDFyCVVFxx0g4onRBI0YuqBdJaIaBqLij0o5Ia0Eij/LuqKwj0lqQKCAp144vHr0TFEU5R5R1weZWEY5wZX/phJewrXBpqFe13emUEqIMRTkcAKoLEtVRLlRVJjQVIrxDhqIkQmJyrFD1xFPrQBGUEKLMjGExl1rUCMfgBs2xHapkVUMdmub7RLfQpATRELIjZ3hTmNeNoRWiXA+a7CdyY16CaJxQrxtDKURKyhAeEVoxhm6NKFvVSISEN2A3zgF5jYWMUDmi/AUZ8QOUlCG8hQ2CmV0VpoN2QuOIsjyB/aIkQsJzZEb1ED/52zsgJITCEa0aIZYnCMJnmLGOzXndLgiYwIVIIiQCJwRiDFSIJEIiNAQsxsCEaCVmcE1IECHBzC0NKoETiBBVzo4eefY5ePJXh+GFF1+GM+fOQ8e0Npg3dxYsvu5aWH7TEiBUJrhsqu9CVLVY/+T/HIYt3/i2FGEl2qdNheVv6ILb3vl2WJ1aAYSSBFL091WIqoqwt+9+eOD7P67pv5k/ZxZ0f+j9cNu73g6qcuLky/ID6PjJl+CEiABsNIgCfBejb0K0BnpblBtj6r7nS7DnwUehXlCQe75+n7ifDSqAwnv04BPwyMHHpRCdsPwNS2DZTTfK+8gIk7N+GBla5VejuH9CPPn0DtWmKLZ841vQJ8LRRmkXDoLu+MG/+QsIK/hhg7dqobcT7PB89coV8ob/dmURGX02b+F68AFfhJifJ+wBhcCkzJ++9yPgJijGTR96H4QJFF+f+MBx6n61snrlzVKQyoboDHrYnIW94DGeC1FOSRuxHaAYy9e815OLMyxidJJ8chMM0TF07f7w+5QJ08fwocboqRCt5AxTrn8UXQLXhl4RpBhPiMRLj0g+7RfrwKBAQaJDquOSoqxxiS/1MnnjmRBVTc4gt3/kk8Ipfg1e0tP9Ud/XjNtF5hfD0LPnLkAYUCyzLDKpw0u9St54J8QXn9mi4paH6BjL1/jjVn133+XLReh3GFor6giSbWVzr98EHuCJEOXmvyZX8kw7r8PSQjCjuPv++2Dxa68FLzh77rwUYK010KDAxE5P90dCvobkm7w4r9F1Iaq6LrTZcNdmX9dPXokRP1B6tnwtNGFoLYQxuzyON+tF94X44tMDoPA2F15lS6uBYuz77F2utMWFPQx1SsgbIdJs7sJV4CKuClHFemEhGMotfltwRXe76F9PETwqAiwk3I0Q7oaorgkx30c6AAqDToiOGCR24mL5TTdO6gb4wbH73x+FR9NPREqAEwltqOri2JR7QlQ8JLW58o//BMIC1ttw7YgN1h1tlkueOX8ejv72OTj67HNw5NhzoAs40dK3+S4IGa6FqK4IUfWQtJAwCZEoJpxidCdEbXgXt3yWNDJHpGFoSISTPSIM7+71p7TkHGMzH5AH5jb2U6BxNtMWiIRfoBi3uDAR4x7i2m+GhnupGxKibOiO2AExHW1TgQg32Ka3Px1cr2wZUvz40ylogMYc0YgpceRVLSg9P6cR3f/0JZk1Dg0x2CH7q+ukbiHKNrYInk9BQlQDFGH3PfdBiEhCU1PduZK6hGgdmRbNM+xJiOqw/+DjspEhNLDYxnpdsV5HxJA0CQQRMKFL3NTpijULMd9Bsw4iCpUv1AI7isLnirWXM+pxxMglaAi1CXK3gVLQFWvXSE1CjLobIleSIyoH1hZDlUFlsK5WV6zVEckNidCBIsRd90JFja7oWIg6uCHS3kZZUxUJV3gKeVd0nkGtxRG1cEMqX6jJ0bA5IlJDBjXu5EX5xu61uPVx1FFViPNnzoTF1yShfeoU+fWJV07Bk785ArqA53OEDquuuNXJzm+OhAi5XAqMmBaN3R2KCBEF98E//zNY/vrFeQGW9sieeOUVKca+7/5ACjPK+L29iTPG6oo9k73S0TxiVIZ+nRCGKf1qoPA2vfuv5X0t9H13N2z53g8gyjy57zshrAOzQTb3+umTvWrSNaKcsNCoiyasBX10wM0bPgC7772nZhEi3e+5HZ7cfr8IYa8Awk94p5PJjMmTNbHYHUAECopn/7Yvw4Y174RGwHXk/q98WYS07wLCR2KTJzqrCjFfskiBZoTJFXH9t0e44PxZM8ENcC3Z86H1MrwlfCM1WSljMkekAn6AoBPudlGEhWCo2nfnxyFKtId5qHuSUkZ1IWrohkgYpvRRhOiE5bKhbnHb21bBI9vuGyt5qE6oS0+ilFHt2xWFyI//JgWajjqF4Q3d45ETTmTxNQuk66ouRq/OD3GP6kmbyo4Yj2ubpAlaiJvefbsvIrRBMWISR+WMqhKHnxpwR+VvVUJ20qgLTm/j1nv2RkO1dOcHKUQUQ/d7/E+kYEZ1z+fvUVaMy266EUIPY2srJW3KCpG/cHSt6lsk4hFf3R9+P1wpPinxVCQ80wKFGaoh0jJgSBoUKotx8XVhD00Roal4S1e575R3RIOtgQiAZQg8/BI7LvDsBJxbu/1vPwUb7uqRB5JWIqjsGyZP/AxJy6GiGPF9Xn7TElCCCuFpeSEqHpaWAw8ysVugMGzFU4G3f/8nZV8bVGjaLdaGYUA1MSojQoSV11aJEK1saTR37pYi/M7XRdhqnUPY0/c16O27H8JAGNywEJXEiGdiqEP57GmpI7JY5NywEHS7B77UO3bMFx5rPfE8hSC2ywiLGxaighiVCkttGC/RWKkQDWMlaEDhmXtBH26CTdxhcsNC7P5UbLULI/g+KkeZHEyREGVvKfAu0AR8EzGZg6AYgwpTMSwNM9jd88i2L4fu74nvnf3+KUZyYhmj2BFxAFgz+u6+ayz1jWHqngcf9bU4jGFf2IVog72pYWkWt09WVpZEoig8LRZiLKZ8WIqDvWfPXRh7jLt7Ye2w2gT3A/f1jk1c9Gy539dp7+WvvwFUwm4WD3rdWPieKQkr1lrRhH5UJ/FRjNuF29nFfCzyY2/iMjwa+3XXSAfE72GNEWmfNnVMzF6z+95e5cSI4DYct33mbt+34MD3pmfTR1UNSQvJsLkLF9hfjAkxP3s4ABEHQ09seyt0Pfxk3fDuvxRi7Pd1Wz5cex35/rdAZR7Y9yBs/7cHfREkvk/ohGp00TihOcnmXvM8PhoXIra1MfYT0IQHvvdj6ZKFgvTTCZHVy/4YHvjHvwfVQXfc/Z9p2PNfP/VMkKtTK6Dvs38n36PIYObWs/k37MSH40I8cXSrSKtuBI1AEaI7oks6BUsNmFxZfkNxyQF3SzsyMADb9zl3B1xrqZKoccLZCxdg/y9+KQR5wLWtHLFGuGnD+9SrFTrB5NvY/EVyYLgwND2g6yBwuXB1IrXsnoYXYe83vimEman6OhzKxRGkKGJv5YjCPPL7gZqcEkW37A03ysb98M8ZNkRarBPlJ/G4EF985nRUW9ucgCLc8KnNcORY6Y7ROB9Yz2gSOkOlPUWjsD6sBXTLI7/PSIEef/mVou9hk32H+H0sWpCEK+fNgfarwtnc4D7jWy1KIfITT3WBETsEhOw/LWwGr1eENpWyi1FZH7pOTFTULtfp2AMrYWPVEZmRBELS0/3Rsda3RkWIYIvYk9u/XrKFIX76E2XImaAV5kUZmua33Me2NkebfmsBdmwsnjMfVr/xj8AtcAtDDEft3bajujZ0Bc5xXAi0gLEk3uUdkUUwJdUAI1lXRWiDXSl2ixjtuF0FrUzRuBr/zDsihqbRP+nJMcOj4BUoRlw3kiNWgaMSY6AFjMkhCytZo3nGtIRT57DGA0RATJ8C0OTsoDL1sTKnTI5jNLecBsJChKVw+iIQAdLeCtCaAG24NDzdgHhcm/lDR2Q1y9oRwZNIJA1dDiB1zGgOCMJXRPnQEKvEJBDj0NoweHSrJQoNGqJmQ45IEEEiNCjqiCwJxDgGNTYQfmNcjaFpBxDjkBAJv2FcZE0Zo9C0kGZd6ldEeGAdYo1IQiwiESNXJPwGs6aUrCkCm41bm4Ag/MQAopSpTeSKhK+I0JTqiCWgK05tBiIgtPsQZJ3kiJWY0mTdCP/RZRZxDE5CrMq0Fr2aj8OChssCEuJk4CQAhan+ouHynITohLZmS5Ax+nX5AjkiUREMUXFgtYVCVc/R8AOPhFgLeIF0tFrb/ZEgvUO7ZI0UIhsEojYmCpJCVveI6/m7jAPjg6KWSN019WALErk0CjCctQaLdZuncxM9P9Qy1OHsFs0J64bQvjf1E9dk97YJUEzlBRSq1k9CRyGyQbxiMkAQYUFLR+SDuFUGJWuIcICRREzHZnt+Bpu+zwBBhAEtw1IBZ6fFRxDPAEGEAW1rs+bzuFUGhaZEOGjS1BEZJms4JWuIEIBuqGFHjURoUAjRzABBBI3O42YmOuLoaAYIIkiwrU2b05/KkB3uN9iCpYPUb0oESpvO855sEDVot4BkgCCCoKWgNVBPMviHJURuHgaC8Bss4Le1gN6Yz+OfeUekWiIRAJ1TNO2kKYDzfrzLOyIJkfAZ3HpE09nDYpgUopWqisXSQMcCEn6A4Wh7i95Z0kLy5cOxuIC/+Mxp3F8RiMbBweD/Ow9EAbghFCZmMEOqa+G+BJExnXv9dHxUEBtYsSoRfUYuXQIz59MR5Ym4tR0lbiuC+8SSCAsY19x4fGDyw+JTKwVE5GlqboahoYswOjIKRiwGiUQCmpoSQiMurNnQ+cTPhJa41S1DwqsMai5PQaBuCnVq2nSrIa2tU4T4cnDxwgU4e8aahIvH49ZNCDMej8nHTsRpchOMFuF6rXFr7UficwaDtP1wXIiUsNGOmHCuae3tMGXqVCnI4eFhyGazIB4Uvc4QCRZ87URyIrxtnt4OU2fOoO1B6mGEjYWmRR9d/OTTA3Q6lAsomqxBYaEgR0ZGwDQn34mupWMaTLtqFhB1kWFzFy6wvyjOIZv8oAgrkkBoie2QyPDwEAxdHLIcsgItnW1A1AkfD0uR4niCm2kgCEFLSytMnzFD3lpaWsAwSkNPg2qB9cPhYOGXxb/d0dG9QBAFYMIGXfKyyy+Hjo4OKUpmJ2NilNyrm9FiRyxJb9E60QU0KOiPinVkYu5l1CtaF6xfFPKXFj5TGm/k+D4giElINNFpynVjmgcnPlUqRM4oPCUILymjsbJxBfWdNoguvaaXT6PQtGbG+0sLKV+F5SaFpwThBZyXjTjLC9FkO4EgCA8on4MpL8TscD9tKEUQbiPC0nmLnDui3NmNwlOCcJcKYSlSuVOXwlOCcBcTdlX6VtWUF2VP64SypkQpRU3eE6k+u8Jz24AgKuHXlH8UmNDkPZHqQhwZ2QoEUYkcDbA6ZgR6q327qhCt7firK5nQmNzkM4uEJM0WLMxUe8HkY9W56komNObiiPikz2I2sPR7Bk3sj1ElSWPjaKVNSZsKoCNkc1aIho/xXn6tmVPMuBxg9jxrLCpmzShmH34YzEOH5GPz1QolaZHoMebNHPuSTZ0ibq0A4samtFpf4/3My0BhqiZpbJxNdmLShhmbQUdQVPiJP1ogOrzH50xaI0k6ZuDWcEVPjT70H5A75N6RKoYQI7tihhQqu+IyMMSNzZwh743kfAgtprOI0pkQMWnT3LpRK1ds7QBITBVXQNz6pMcLbeBZEY5Rw1EJra0lT7kpQsR85VUAvFUAxWigMK8W9wvmQ0zcB++kbBBGedrJKx0JEZM2/IUjkXNFfuEi8FN/ADNzQjweglzmuBDakPj6BYjf+g5o2rC++D9o0v3kojLgXOIEN8wd8n+vanwP8Qa//PXYcxjaxha/BmKLXiuEOk88fi34Cud7J0vS2DiuxvKBQ53Q3HIaFKNQbDkpuItSaPgcPq4Ea2uDqY9M6PI7M2i5IjEOrg+vuqboqdHdP4JLX/kahJHY4usg/sYl8t7zkPYSLHAqRMe7/1iueHQXMHYHhIxCsWEIY5561ZHYqv7M8+dleBVbumT8ybZpQEwA14cTyP78cQgruSPH5A2R60shyETqTe67JYedTkWI1LYNl8F6xP8gMCFabnZCigzFxvOCM6usHRoh+/PHioWIa8XWKQBD9Yk7krSVbqloHnsOVEB+YKdfhWz6F1YY+8Yb3RPlSG1lv5obBYUr7vTaFbkQVu55EUa+8ge5bmvU3eoFw9MpP/xXeT/GC/8LcOolIARt7QCvub7oKVwfDn3ik6AytlM233ZrfQkfdMN5C9fX8p/UvjGldEW2xo0MalgEVwkZnv7qMMTfevP4kx3TSYg2uD6cQPahR0F1Cp0yJkPXZRAXN8eM1N4EU7MQ2ZyFmVozqPYaDmNza/1mhZdhEVw1Rvf8qFiIuE7EEJUansuumd0uWwSNvaY0dj8ETbffOrkga1wb2tQ1w2JlUFsHyrmidLmjx6wMpcdrOL+YsuNfwLju2vEnKDy1IoMF1xU9hUma4c/cDVEGw9b4qjdBYuWycmFrRmRKV9UjxLr2TMcMau7wz7cJkW0uDCtlHSeCjD78CDRf97HxJyg8FWHpFSVPZR/aD1EHI7oR4Y7ZA/8NiXelIHHrLQXf5dvYgkUZqIO6pzpPp1KdsRF2iGlwaE3ZpM1vn9I3e4pF/EVdRU/xl16GC3/1HtANdMh8yOqop7Tiz4E6mZ5ODzKD15QZUhVM2mCRuogyjqANs0sL4SPbJx0wiCTokMNf/bbjntJKNDSrMu2xdFr4RRo0YHT3j6Ugx8CMoY6HsKAbTkjSoBuOPhz9sLQSIqzcyeYv3AkN0PDQWDaXQ1eMfCd0iSuiCK+YDdqBkUDJpMUjoCtcXPujObPhmd2GhTj9F+mMCXoMD5e4IgpRJ1dEN8S5wwLQDUe++S3QFcZgG2oAGsSVMeqOx3+6VYcQtawrTrgwIw2tDYvgwDPTHvtpD7iAa/sZMJ7bBBogXVG4wBjoim3tEHlwTTyhk0b3tWEux1eBS7gmxLYn0v0iRI28GNEVL237avGT866CSFMmJEWGPt4N2sKg142Q1MbVHX50CVGxg6SolQsnMqIsRpw3nJCgGfnmLjBf0rOpwc2Q1Mb1rbZ0yaJe+twXSxM3HdMhcqATTgi9sz97XNsEDWZJ3QxJbVwXoi5ZVHSDkotROkeEjrRGEZbLkoZ0+t4PhGBcDUkLfq77yBBVpHUh4mAGtShExSzqaxZGQ4zlRHj+glwX6hqS4jXdJpdf7uPZLrDZuNnDOc9AxMFpg6IsKq6lVBYjfpigs1dIzui8LsRrGjzCMyFiL2rOlLF0pNeLuE4c+sxni5+0xYhJHJXAteDrbig78Hvp3i+A+bvfgY7Y60K8psEjPN0XHWNpzs3IlzRwj5bhe79Y/CSKES9qFdrg0AUx64vbXkzIjspw9BPdMBqByfu6EdewF+vCQjzvz/r88Uz/P1y9AMetUhBh7A2TYkuLx4OgvcO6uIcuhG+qHwU4ay5A8tqyTQkYcg91fxrMo0+Dtoh6YfvjBzw/Fc23UybP3rxqJ4PwbcXoNk0feL+4lflnoghxmPgPp3DndAgUFB1OUFwxa+ysiomYvxMu/+m79U3MgFwX7hIiXAc+UNeEfj3kEvzO+CgsEdrvgghjlzRKxGj3peL6CzcqPnXSX0Ha4pO36i15mA3Gf0dRnVQzMDmD1yz4hK/nLp9elkrGDHZAh6n+is5YyPlzlkPipL+b0/4YCmOiCAWH51Lg49jkn7kYig5/7guR2wCqVqQIMTnj8bqwEN8PQNdJjPG33AzNGz8GbPasyV+cywoxDlmCHBm2QtlyjllYFkHBxQoOySlzDoUTMCEzuvuHpWNeGhKECBHfhYicX5Hq4sw4IB5G/nQpY/ZsaP3nPmdiDADc8AnDUJ3XgjayTMHNVdOfSPt+ik4gQkTyYjwEmuAoVPUJcsDyZLm5NAgRIoEJETm7IrWOMWMHaAK6IwoyfutqCALcDj/7sycg+/B+EuAEGDfXtz2R3gkBEagQEd3EiNiCjL31zcDapoJXoPOZx46R+CYhaBHKvwOEAB3FaBN/x2qIv3UFxJYubViU/ORLkOvvh9yzv5ftaNhkQOKrThhEKP8eEBJ0FqMNbuuPbinv58ySR57ZmxpLQeVFxc9dkF+bJ1+Wz0nRnXwZiNoIiwiR0AgR0SmbSgRHkNnRSnja9F0ruO+N+JRapcP4FBEMsk4YMhEioXJEG52K/oR/BFWsd0KoHNEGf1E4y0jOSLgH7w+rCJFQChGRYmziS7EDHgiiAfAayibCK0IklKHpRM69+ZYe8dt0fEIxQYzBoNftrQ+9QAkhImdX3CLKG7AFKKNKOAAzowY3N4WlPDEZyggRoSQO4YQwJ2UqEdo1YjnsdaMOWzUSdSKujVyCL1VJhIhSjljImZtvuVN8iuC6kUJVwgpFAXq92nfUa5QVIkKhKmHB09kcX6+aCxaitBBtKKuqL+IC3qSqCxYSCSEi5I66IVyQ801ha1Wrl8gI0SbvjhuB1o6RRPW1YCUiJ0REumOM9eiwj6peqL8WrEQkhWiDTQDizdtM4araYF2QMb5+2mPpNESUSAvRBksdjPONJEi1wDCUibpgNm5u9fIAmDCghRARClfVQScB2mgjRBsSZLgRF+TO0ZzZG8V1YDW0E6INCTJc6CpAG22FaGMLUvwq1jAqefiKjiFoJbQXoo3VEGCkKMvqPSTAUkiIZbBmHzFk5SkgXISngfHeKJch6oWEWIWxsJXDSnLJ+iD3cwYJ0SHn3pxKcc7W0VpycvJtaHs5M3eR+zmDhFgHZ9+SWgsmW0uiHKdQfNk49JP71QYJsUHQKcE01oq1z8qoH0s+Ec4hIy6gfWCYe8n5GoOE6CJjmVcmkjwRXFei8AwGaZPDwVyTuZdczz1IiB4ihZmALpYzhDjZEg68S5VQViZZgPcDZ4fF4/6EaaZbNS22+wEJ0WfwoB0zBkmeM7rEmmqJcM8kB5YMSqB5wYkQk/XnODwv/h4ZEp3/kBBDwulUqjOehS7OoJNnIQnM6IwxuFqEg53CTYVIzU6RtZViFeWAZKWfg8ISf4yFjCI8zoiYEksIgyg04OYgi4sQMweZ0SbIUHgZDv4fuQgYAEqCNtEAAAAASUVORK5CYII=" class="user-image" alt="Sin Avatar"/>

                                    <?php
                                }
                                ?>




                        <br><br>
                        <div style="display: flex;justify-content: center;">
                        <?php
                        echo $form->field($model, 'imagen')->widget(FileInput::classname(), [
                            'options'=>['accept'=>'image/*' ],

                            'pluginOptions' => [
                                'showCaption' => false,
                                'showPreview' => false,
                                'placeholder' => '',
                                'showUpload' => false,
                                'removeClass' => 'btn btn-light btn-sm btn_borrar',
                                'browseClass' => 'btn btn-danger btn-sm ',
                                //'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                                'browseLabel' =>  'Elige una foto de perfil',
                                'removeLabel' => 'Borrar',
                                'allowedFileExtensions'=>['jpg', 'jpeg', 'png']
                            ]
                        ])->label(false);
                        ?>
                        </div>
                    </div>


                    <div class=" col-lg-10 col-md-10 col-sm-10" >


                        <div class="row" style="padding: 40px 50px 0px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true])->label('Nombre completo: ') ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true,'readonly'=>'readonly'])->label('Correo electrónico : ') ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true])->label('Número de contacto: ') ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <?= $form->field($model, 'whatssapp')->textInput(['maxlength' => true])->label('Número de WhatsApp: ') ?>
                            </div>


                        </div>
                        <div class="row" style="padding-left:  50px">

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <?=
                                $form->field($model, 'password', [
                                   ])
                                    ->passwordInput(['placeholder' =>'', 'autocomplete' => 'off'])->label('Cambiar contraseña: ')
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <?=
                                $form->field($model, 'repeat_password', [
                                     ])
                                    ->passwordInput(['placeholder' => '', 'autocomplete' => 'off'])->label('Confirmar contraseña: ')
                                ?>
                            </div>
                        </div>


                    </div>

                </div>


                <div class="row">
                    <div class="form-group text-center">
                        <button type="submit" title="Guardar cambios" class="btn-vtm"> <img style="cursor:pointer;"  src="<?= Yii::$app->request->baseUrl ?>/img/guardar-cambios.png"></button>

                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>



