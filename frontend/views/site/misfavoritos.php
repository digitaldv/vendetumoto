<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

$this->title = 'Mis Favoritos';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(" 
jQuery(document).ready(function () {

$('.btn_vend').click(function(e){
    e.preventDefault();
    var modal = $('#modal-vendido').modal('show');
    $('#id_pub_sel').val($(this).attr('id_pub')); 
});

$('.btn_prom').click(function(e){
    e.preventDefault();
    //alert('".Url::to(['site/publicaranuncio4'])."?id='+$(this).attr('id_pub'));
    window.location = '".Url::to(['site/publicaranuncio4'])."?id='+$(this).attr('id_pub');
   
});
 
$('.btn_ok_vend').click(function(e){
    
    if($('input[type=\"radio\"]:checked').val()){
       //Guardo un post y hago submit
       if(confirm('¿Confirma que efectivamente vendió el vehículo? Si acepta no podrá reversar la actualización.')){
         $.post('".Url::to(['site/addvendido'])."', {id_publicacion: $('#id_pub_sel').val(), tipo: $('input[type=\"radio\"]:checked').val()}, 
           function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                    //console.log(res); return false;
                    if(res.res){                      
                         $('#w0').submit();
                    }else{
                        alert(res.error);
                    }
                
           }
         );
       }
    }else{
      alert('ERROR: Debes seleccionar alguna opción.');
    } 
});
 

});");

$this->registerJs("
$('.btn_favorito').click(function(e){
    
        e.preventDefault();          
                                 
            if($(this).attr('id_pub')!='')
            {
                 $.post('".Url::to(['site/favoritoupdate'])."', {id_publicacion: $(this).attr('id_pub'), id_usuario: $(this).attr('id_usuario') }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==2)  {                    
                               alert('¡Publicación ha sido eliminada de tus favoritos con éxito!');
                               $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg2 \"><i class=\"fa fa-heart-o\"></i></div>');
                            }else {                                  
                                   alert('¡Publicación agregada a tus favoritos con éxito!');
                                  $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg danger \"><i class=\"fa fa-heart\"></i></div>');
                              }
                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
                 
            }else{
              alert('Error: ID_PUB no existe.');
            }      
         
    });
 

");


$model = new \common\models\VMisFavoritos();

?>

<style>
    .padre {
        display: flex;
        align-items: center;
    }
    .vertical-timeline-icon {
        position: absolute;
        top: 0;
        left: 0;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        font-size: 16px;
        border: 3px solid #f1f1f1;
        text-align: center;
    }
</style>

<div  class="col-sm-12 col-md-12 col-lg-12" style="border: 0px solid ;background: #EAF1F6;height: 70px;padding-top: 20px;">
    <div class="row">
        <div class="col-sm-2 col-md-7 col-lg-7">

            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
            </a>


        </div>
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/site/misfavoritos']),
            'method' => 'get',
            //'options' => ['class' => 'form-inline form-group form-group-sm col-xs-12'],
            'fieldConfig' => [
                'template' => "{input}",
            ],
        ]); ?>
        <div class="col-sm-12 col-md-3 col-lg-3">
            <span class="glyphicon glyphicon-search form-control-feedback buscar" style=" right: 13px;"></span>
            <?= $form->field($model, 'palabras_search')->textInput(['placeholder' => 'Busca por tipo, marca, modelo  o ciudad...','style'=>'border: 1px solid #F33B42;box-sizing: border-box;border-radius: 15px;','maxlength' => true,'value'=> (isset($_GET['VMisFavoritos']['palabras_search']) && $_GET['VMisFavoritos']['palabras_search']!='') ? $_GET['VMisFavoritos']['palabras_search'] :'']) ?>
        </div>


        <?php ActiveForm::end(); ?>
    </div>

</div>
<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12">

           <?php
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper text-center mt-10',
                        'id' => 'list-wrapper',
                        'style' => 'margin-top:10px',
                    ],
                    'layout' => '{items}{pager}{summary}',
                   // 'layout' => "{summary}\n{items}\n{pager}",
                    'itemView' => function ($model, $key, $index, $widget) {
                        //\common\components\Utils::dumpx($model->id);
                        $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
                        $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();

                        date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');
                        $visitas = \common\models\PubEstadisticas::find()->where('tipo=1 and id_publicacion='.$model->id)->count();



                            echo '<p>&nbsp;</p>
                                 <div class="row" style="padding-left: 20px;padding-right: 20px"  >
                                            <div class="col-sm-12 col-md-12 col-lg-12 ">
                            
                                            <div class="row" style="border-radius: 8px;border: 1px ;min-height: 140px;background: #FFFFFF;box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.2);">
                            
                                                    <div class="col-sm-2 col-md-2 col-lg-2" style="margin-top: -1px;padding: 10px;;display: flex;justify-content: center;">';
                                                        if($foto)
                                                            echo '<a title="Ver Detalle de la Publicación" href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img class="btn_galeria img-thumbnail img-responsive"  data-method="post" style="width:100%;max-height:165px;padding: 5px;cursor:pointer" title="Click para ir al detalle de la publicación" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" archivo="'.$foto->id.'"  /></a>';
                                                        else
                                                            echo '<a title="Ver Detalle de la Publicación" href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="no-image" style="width: 100%;padding: 5px;max-height:165px" class=" img-respoensive" src="'. Yii::$app->request->baseUrl . '/img/no-image-moto.png'.' "></a>';

                                                    echo '</div>
                                                    <div class="col-sm-7 col-md-7 col-lg-7 " style="margin-top: 12px;">
                                                        <div style="width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 20px;line-height: 21px;letter-spacing: 0.637836px;color: #474747;">
                                                         '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'                                                         
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                                            '.number_format($model->ano,0,',','.').' -  '.number_format($model->recorrido,0,',','.').$model->_unid_recorrido[$model->unid_recorrido].' 
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #F33B42;">
                                                            $'.number_format($model->precio,0,',','.').'
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                                            '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 col-lg-3 padre" style="margin-top: 0px;border-left: 1px solid #EAEAEA;padding-top: 10px;min-height: 140px;">
                                                        <div id="fav_'.$model->id.'" style="z-index: 1500;right: -22px" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.Yii::$app->user->identity->id.'">
                                                            <div class="vertical-timeline-icon navy-bg danger ">
                                                                <i class="fa fa-heart"></i>
                                                            </div>
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                                            <a style="background: #F33B42;border-radius: 25px;display: block"  href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'" class="btn btn-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Consultar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                            </div>
                                            </div>
                                        </div>';
                    },
                    'pager' => [
                        'firstPageLabel' => 'Inicio',
                        'lastPageLabel' => 'Fin',
                        'nextPageLabel' => '>>',
                        'prevPageLabel' => '<<',
                        'maxButtonCount' => 5,
                    ],
                ]);

                ?>
            </div>



        </div>

    <p>&nbsp;</p><p>&nbsp;</p>


</div>


<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-vendido',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
   // 'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
<input type="hidden" id="id_pub_sel" value="">
<div style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 38px;line-height: 46px;text-align: center;letter-spacing: 1.21189px;color: #F33B42;margin-bottom: 15px">
    ¡Vendido!
</div>
<center style="font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 17px;line-height: 15px;text-align: center;letter-spacing: 0.54216px;color: #474747;">
    Por favor indica el canal de venta
</center>
<br><br>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
    <div class="col-sm-6 col-md-6 col-lg-6"  >
        <div style="font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 15px;line-height: 21px;letter-spacing: 0.54216px;color: #F33B42;">
            <input class="form-check-input" type="radio" value="1" name="caracteristica" >  Lo vendí por Vende Tu Moto<br><br>
            <input class="form-check-input" type="radio" value="2" name="caracteristica" >  Lo vendí por WhatsApp<br><br>
            <input class="form-check-input" type="radio" value="3" name="caracteristica" >  Lo vendí por correo electrónico<br><br>
            <input class="form-check-input" type="radio" value="4" name="caracteristica" >  Lo vendí por llamada telefónica<br><br><br>
            <center><button type="button" class="btn btn-danger btn_ok_vend">Aceptar</button></center><br><br><br>
        </div>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
</div>

<?php \yii\bootstrap\Modal::end(); ?>



<?php

$url_self = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-compartir',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    // 'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
    <input type="hidden" id="id_pub_sel1" value="">

    <center style="margin-top: 30px;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 17px;line-height: 15px;text-align: center;letter-spacing: 0.54216px;color: #474747;">
        Comparte tu publicación en:
    </center>
    <br><br>
    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
        <div class="col-sm-6 col-md-6 col-lg-6"  >
            <div style="font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 15px;line-height: 21px;letter-spacing: 0.54216px;color: #F33B42;">
                <center class="text-center url_iconos">
                </center>
                <br><br><br>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
    </div>

<?php \yii\bootstrap\Modal::end(); ?>
