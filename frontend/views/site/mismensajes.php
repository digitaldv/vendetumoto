<?php

use common\models\PubMultimedia;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;
use fedemotta\datatables\DataTables;

$this->title = 'Mis Mensajes';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("

$('.btn_save').click(function(){
  
    if($('#pubmensaje-id').val()!=''){
        $('#modal-delete').modal('hide');
        $('#modal-loading').modal('show');            
        var msg = $('#pubmensaje-id').val();
        $.post('".Url::to(['site/ajaxsavemensajepub'])."', { id_publicacion: $('#id_publicacion').val(), mensaje_enviado: msg, id_creador: $('#id_creador').val(), id_papa: $('#id_papa').val()}, 
           function(res_sub) {
                var res = jQuery.parseJSON(res_sub); 
                if(res.res){
                     alert('¡Se ha enviado el con éxito el mensaje al usuario!.');                        
                     $(\"#form\").submit(); 
                }else{
                    alert('ERROR 105: verificar con soporte del sistema. ');
                }
        });
    }else { 
        $('.error').attr('class','error'); $('#pubmensaje-id').focus(); 
    }
        
});  
 
 $('.cargar_chat').click(function(e){
        e.preventDefault();          
             $('#id_papa_actual').val($(this).attr('id_papa'));
             $('#id_pub_actual').val($(this).attr('id_pub'));
             $('#txt_mensaje').attr('style',' background: transparent; box-sizing: border-box;border-radius: 25.1351px;height: 50px;width: 95%;');
             $('.enviar').attr('style','cursor: pointer;margin-top: 12px;');
             
             $.post('".Url::to(['site/getchats'])."', {id_papa: $(this).attr('id_papa'), id_publicacion: $(this).attr('id_pub')}, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                       // console.log(res); return false;
                        if(res.res){                      
                             $('.chat_hilo').html(res.res);
                        }else{
                           alert('Error 1');
                        }
                    
               }
             );
 });
 
 $('.enviar').click(function(e){
        e.preventDefault();     
      
        if($('#txt_mensaje').val()!=''){  
             $.post('".Url::to(['site/addmensaje'])."', {id_papa: $('#id_papa_actual').val(), id_publicacion: $('#id_pub_actual').val(), mensaje: $('#txt_mensaje').val()}, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                       // console.log(res); return false;
                        if(res.res){
                             alert('¡Mensaje enviado con éxito!');   
                             //vuelve y carga los chats                   
                             $.post('".Url::to(['site/getchats'])."', {id_papa: res.id_papa, id_publicacion: res.id_publicacion}, 
                               function(res_sub) {
                                        var res = jQuery.parseJSON(res_sub); 
                                        if(res.res){                      
                                            $('.chat_hilo').html(res.res);
                                            $('#txt_mensaje').val('');
                                        }else{
                                            alert('Error 2');
                                        }                                    
                               }
                             );                             
                        }else{
                           alert('Error 3');
                        }
                    
               }
             );
        }else{
            alert('Error: No ha ingresado ningún mensaje. Verifique e intente de nuevo.');
            $('#txt_mensaje').focus();
        }
 });
 
 
$('#vmisfavoritos-palabras_search').change(function(e){
        e.preventDefault();     
        $('.lista_v').html('buscando...');
        if($('#vmisfavoritos-palabras_search').val()!=''){  
             $.post('".Url::to(['site/getchatssearch'])."', {mensaje: $('#vmisfavoritos-palabras_search').val(), tipo:1 }, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                       
                        if(res.res){                      
                             $('.lista_v').html(res.res);
                        }else{
                           alert('Error 4');
                        }
                    
               }
             );
       }else{
            $.post('".Url::to(['site/getchatssearch'])."', {mensaje: $('#vmisfavoritos-palabras_search').val(), tipo:2 }, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                       
                        if(res.res){                      
                             $('.lista_v').html(res.res);
                        }else{
                           alert('Error 4');
                        }
                    
               }
             );
            $('#vmisfavoritos-palabras_search').focus();
       }    
        
           
           
    
 
});
  
 ");
$this->registerJs(' 
jQuery(document).ready(function () {

    $("[data-toggle=\'popover\']").popover();
  ///////////////////////////////////////////////////////////////////////////////////////
    $("#datatables_w0_filter").hide();
    $("#datatables_w0 thead tr").clone(true).appendTo("#datatables_w0 thead ");
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $("#datatables_w0 thead tr:eq(1) th").each( function (i) {
        if(i!=5 ){
            var title = $(this).text();
            $(this).html("<input type=\'search\' style=\'width:100%\' placeholder=\'...\' class=\'fin\'  />");
            $("input", this ).on("keyup", function (e) {
            
                if ( $("#datatables_w0").DataTable().column(i).search() !== this.value && e.keyCode == 13 ) {
                    $("#datatables_w0").DataTable().column(i).search( this.value ).draw();
                }else
                 if(this.value == "") {
                     $("#datatables_w0").DataTable().column(i).search("").draw();
                 }         
            } );     
         }else  $(this).html("");             
    } );
 
    var $span = $("#datatables_w0 thead tr:eq(1)");
    $span.find("th").wrapInner("<td />").contents().unwrap();
    $span.find("tbody").contents().unwrap();
   

$("#ir_ini").click(function(){  $(".fin:first").focus();  });
$("#ir_fin").click(function(){  $(".fin:last").focus();  });
   

});

' ); //,View::POS_END
?>
<style>
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
    }

    .box.box-solid.box-default>.box-header {
        color: #444;
        background: #d2d6de;
        background-color: #d2d6de;
    }
    .box.box-solid.box-default {
        border: 1px solid #d2d6de;
    }
    .box.box-solid.box-default>.box-header {
        color: #444;
        background: #d2d6de;
        background-color: #d2d6de;
    }

    .box-header.with-border {
        border-bottom: 1px solid #f4f4f4;
    }
    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
    }
    .file-caption.icon-visible .file-caption-name {
          padding-left: 2px;
        font-size: 10px;
    }
    .text-titulo{
        font-family: Nunito;
        font-style: normal;
        font-weight: 600;
        font-size: 16px;
        line-height: 10px;
        /* identical to box height, or 62% */

        letter-spacing: 0.457143px;

        color: #474747;
    }


    .circle-image{
        display: inline-block;
        border-radius: 50%;
        overflow: hidden;
        width: 83px;
        height: 83px;
    }
    .circle-image img{
        width:100%;
        height:100%;
        object-fit: cover;
    }

    .nombre_pub{
        font-family: Nunito;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 15px;
        color: #010B56;
        margin-top: 10px;
    }

    .comprador{
        font-family: Nunito;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 15px;
        color: #A5A5A5;
    }

    .id_mensaje{
        font-family: Nunito;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 12px;
        color: #A5A5A5;
    }

    .ove{
        background: #FFFFFF;
    }

    .ove:hover{
        background: #FFE1E2;
        cursor: pointer;
    }

    .ove:focus{
        background: #FFE1E2;
        cursor: pointer;
    }

    .fecha{
        font-family: Nunito;
        font-style: normal;
        font-weight: bold;
        font-size: 11px;
        line-height: 12px;
        text-align: right;
        color: #8190B0;
        margin-right: 20px;
    }


    .cols {
        display: table;
        width: 100%;
        padding: 20px;
        padding-bottom: 0px;
        padding-top: 0px;
    }

    .cols div {
        display: table-cell;
    }

    .col1 {
        width: 71%;

        padding: 10px;
        vertical-align: top;
        left: 98px;
    }
    .col11 {
        width: 71%;
        padding: 10px;
        vertical-align: top;
        text-align: left;
        right: 98px;
    }


    .col2 {

        float: right;
        padding: 10px;



    }
    .col22 {

        padding: 10px;
    }

    .txt-der{
        text-align: right;
        background: #0084FF;
        font-family: Nunito;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 19px;
        color: #FFFFFF;
    }

    .txt-izq{
        text-align: left;
    }
    .vineta{
        position:relative;

    }

    .globo{
        position: absolute;

        font-size: 12px;
        border-radius: var(--borde);
        padding: var(--borde);
        --borde: 1em;
        font-family: sans-serif;
        z-index: 1;
        background-color: #0084ff;
        filter: drop-shadow(0px 0px 1px black) ;
    }

    .globo.i{
        width: 150px;
        top: 20px;
        left: 80px;
    }

    .globo.ii{
        width: 100%;
        top: 30px;
        left: 0px;
        text-align: right;
        background: #0084ff;
        font-family: Nunito;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 19px;
        color: #FFFFFF;
    }

    .globo.iii{
        width: 100%;
        top: 30px;
        left: 0px;
        text-align: left;
        background: #ffffff;
        font-family: Nunito;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 19px;
        color: #607d8b;
    }



    .globo.iv{
        width: 150px;
        bottom: 10px;
        left: 40%;
    }

    .globo::before{
        content: '';
        position: absolute;
        z-index: -1;
        width: var(--colita);
        height: var(--colita);
    }

    .globo.abajo-derecha::before{
        --colita: 20px;
        left: 50%;
        bottom: calc( var(--colita) / -1.5);
        transform: skewY(30deg) rotateZ(-30deg);
        border-radius: 0 0 0 10em;
        box-shadow:
                inset calc(var(--colita)/2) calc(var(--colita)/3) 0 0 #0084ff;
    }

    .globo.derecha-arriba::before{
        --colita: 20px;
        top: 10px;
        right: calc( var(--colita) / -1.5);
        transform: skewY(30deg) rotateZ(-120deg);
        border-radius: 0 0 0 10em;
        box-shadow:
                inset calc(var(--colita)/2) calc(var(--colita)/3) 0 0 #0084ff;
    }

    .globo.abajo-izquierda::before{
        --colita: 50px;
        left: 10px;
        bottom: calc( var(--colita) / -1.5);
        border-radius: 0 0 10em 0;
        box-shadow:
                inset calc(var(--colita)/-3) calc(var(--colita)/4) 0 0 #fbfdff;
    }

    .globo.izquierda-arriba::before{
        --colita: 20px;
        top: 0;
        left: calc( var(--colita) / -1.5);
        transform: rotateZ(-60deg);
        border-radius: 10em 0 0 0;
        color: #0084ff;
        box-shadow:
                inset calc(var(--colita)/3) calc(var(--colita)/-4) 0 0 #fbfdff;
    }

    .globo.yellow::before{
        background-color: rgba(255,255,0,.8);
        color: rgba(255,2,0,.5);
        animation: cambio-color ease 5s infinite both;
    }

    @keyframes cambio-color{
        45%,55% {
            background-color: rgba(255,255,255,0);
            color: white;
        }
    }

    .txtnegro{

        height: 16px;

        top: 237.39px;
        font-family: Roboto;
        font-style: normal;
        font-weight: bold;
        font-size: 12px;
        line-height: 16px;
        text-align: right;
        color: #343C4B;
    }

    .txtnegro2{

        height: 16px;
        margin-left: 24px;
        top: 237.39px;
        font-family: Roboto;
        font-style: normal;
        font-weight: bold;
        font-size: 12px;
        line-height: 16px;
        text-align: right;
        color: #343C4B;
    }

    .col2 p{
        margin-left: 100px;
    }

    .col22 p{
        margin-left: -18px;
    }

</style>

<div  class="col-sm-12 col-md-12 col-lg-12" style="border: 0px solid ;background: #EAF1F6;height: 70px;padding-top: 20px;">
    <div class="row">
        <div class="col-sm-2 col-md-7 col-lg-7">

            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
            </a>
            <span class="text-titulo"> Mis Mensajes</span>

        </div>

    </div>

</div>
<input type="hidden" id="id_papa_actual">
<input type="hidden" id="id_pub_actual">
<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12" style="margin: 0px;padding: 0px;overflow: auto;overflow-y: scroll;height: 716px;">
                <div style="background: #ffffff;min-height: 716px">
                    <div id="buscador" style=" border-bottom: 1px solid #DFDFDF;height: 80px;padding: 15px 10px  15px 10px;">
                        <span class="glyphicon glyphicon-search form-control-feedback buscar" style="right: 16px;font-size: 22px;top: 22px;"></span>
                        <input type="text" id="vmisfavoritos-palabras_search" class="form-control" name="VMisFavoritos[palabras_search]" value="" placeholder="Realizar búsqueda..." style="border: 1px solid #F33B42;box-sizing: border-box;border-radius: 25.1351px;height: 50.27px;">
                    </div>
                    <!-- Lista agrupada de (Vehículos + id_Mensaje) -->
                    <div class="lista_v">
                        <?php

                        for($i=0; $i < count($pubs_chats); $i++){

                          $articulo = \common\models\PubPublicacion::find()->where(['id' => $pubs_chats[$i]['id_publicacion']])->one();
                          $foto_principal = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $pubs_chats[$i]['id_publicacion'])->orderBy('id asc')->one();
                          if(!$foto_principal){
                              $foto = Yii::$app->request->baseUrl . '/img/no-image-moto.png';
                          }else{
                              $foto =  'https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $articulo->carpeta . '/' . $foto_principal->archivo;
                          }

                          ?>

                            <div style=" border-bottom: 1px solid #DFDFDF;height: 122px;" class="ove cargar_chat" id_papa="<?= $pubs_chats[$i]['id_papa'] ?>"  id_pub="<?= $pubs_chats[$i]['id_publicacion'] ?>">
                                <table>
                                    <tr>
                                        <td class="image-cropper" style="padding:15px">
                                            <span class="circle-image">
                                            <img class="rounded profile-user-img img-responsive img-responsive" src="<?= $foto ?>" style="">
                                            </span>

                                        </td>
                                        <td style="width: 100%;margin-right: 5px">
                                            <p class="nombre_pub"><?= $pubs_chats[$i]['vehiculo'] ?></p>
                                            <p class="comprador"><?= $pubs_chats[$i]['usuario'] ?></p>
                                            <p class="id_mensaje">#<?= $pubs_chats[$i]['id_publicacion'] ?></p>
                                            <p class="fecha pull-right"  ><?= date('Y-m-d', strtotime($pubs_chats[$i]['fecha_ult_mensaje'])).' ('.date('h:i a', strtotime($pubs_chats[$i]['fecha_ult_mensaje'])).')'  ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12" style="margin: 0px; padding: 0px;">
                <div style="min-height: 716px;background: #FCFCFC;box-shadow: 2px 2px 7px rgba(0, 0, 0, 0.2);">

                    <div style=" border-bottom: 1px solid #DFDFDF;height: 616px;padding: 0px;overflow: auto;overflow-y: scroll;" class="  "  >
                        <div class="chat_hilo" style="padding: 30px">Haz click en alguna conversación de la izquierda, para cargar el chat...</div>
                    </div>
                    <!-- Enviar Mensaje ENTER -->

                    <div style="background: #F1F1F1;height: 100px;" class="  "  >
                        <table class="table">
                            <tr>
                                <td style="width: 100%">
                                    <div id="buscador" style="border-bottom: 1px solid #DFDFDF;height: 80px;padding: 15px 10px  15px 10px;background: transparent;margin-right: -49px;">

                                        <textarea rows="2" id="txt_mensaje" class="form-control" name=" " value="" placeholder="Escribe una respuesta..." style="    visibility: hidden; background: transparent; box-sizing: border-box;border-radius: 25.1351px;height: 50px;width: 95%;"></textarea>

                                    </div>
                                </td>
                                <td style="">

                                    <img class="enviar" title="Click para enviar el mensaje" style=" visibility: hidden;cursor: pointer;margin-top: 12px;" src="<?= Yii::$app->request->baseUrl ?>/img/btn_enviar.png">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
                <?php
                 /*DataTables::widget([
                    'dataProvider' => $dataProvider,
                    'clientOptions' => [
                        "lengthMenu" => [[20], [20]],
                        "processing" => true,
                        "serverSide" => true,
                        "order" => [2, "desc"],
                        "language" => [
                            "search" => "Buscar : _INPUT_",
                            "lengthMenu"=> "Mostrar _MENU_ resultados por página",
                            "zeroRecords"=> "<em style='color:red'>-- No se encontraron registros --</em>",
                            "info"=> "Página _PAGE_ de _PAGES_ - <b>_TOTAL_</b> resultados ",
                            "infoEmpty"=> "Sin resultados",
                            "infoFiltered"=> " de <b>_MAX_</b> registros totales."
                        ],
                        // "colReorder" => true,
                        //  "scrollX" => true,
                        "buttons" => [
                            ["name"=> 'excel', "extend"=> 'excel',
                                "exportOptions" => ["columns" => ':gt(0)', "orthogonal" => 'export'],
                                "filename" => "title", "sheetName" => 'Results', "title"=> null
                            ],

                        ],
                        "ajax" => \yii\helpers\Url::to(['site/ajax_mismensajes']),
                        "columnDefs" => [

                            ["targets" => [0], 'width' => '100px', 'className' => 'text-center'],

                           // ["targets" => [5], 'width' => '100px', 'className' => 'text-center'],
                            ["targets" => [2], 'width' => '100px', 'className' => 'text-center'],
                            ["targets" => [5], 'width' => '1%',  'render' => new \yii\web\JsExpression('function (data, type, row, meta ){
                              return data;
                    }')],
                        ],
                    ],
                    'columns' => [
                        ['attribute' => 'fecha', 'label' => 'Fecha_Envío',], //0
                        ['attribute' => 'fecha', 'label' => 'Publicación',],//2

                        ['attribute' => 'mensaje', 'label' => 'Total de Mensajes',],//3
                        ['attribute' => 'creador', 'label' => 'Último mensaje',],//4
                        ['attribute' => 'leido', 'label' => 'Enviado por',],//7
                        ['attribute' => 'leido', 'label' => '',],//6



                    ],
                ]);

                */
                ?>







</div>

<?php


$form = ActiveForm::begin([
    'id' => 'form',

]);

$model1 = new \common\models\PubMensaje();

\yii\bootstrap\Modal::begin([
    'header' => '<h2 class="modal-title" style="color:#dd4b39">Rechazar Publicación de Anuncio</h2>',
    'id'     => 'modal-delete',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'footer' => '<button type="button" class="btn btn-danger btn_save"   aria-hidden="true">Enviar Mensaje</button>
                 <button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
]); ?>

<?= '<div id="titulo" style="font-size:13px;margin-top: 0px; padding: 3px;text-align:left;">
        
        <div class="overflow-auto" style="height:150px;  overflow-y: scroll;width:100%;">
            <div id="links" class="links" style="padding: 10px"></div>
        </div>
        <input type="hidden" id="id_publicacion" >
        <input type="hidden" id="id_creador" >
        <input type="hidden" id="id_papa" >
        <br>
        <div class="hidden error" style="color: #f9f9f9;font-weight: bold;background: #f33b42;width: 100%;padding: 10px">ERROR: Debe escribir un mensaje.</div>
            <br>
            '.$form->field($model1, 'id')->textarea(['rows' => 2])->label('Escriba el mensaje o respuesta que enviará al usuario : ').' 
     </div>
     <br>
     <em class="modal-texto"><b>Nota : </b> Al enviar un mensaje, se notificará vía email al usuario. </em>'; ?>

<?php \yii\bootstrap\Modal::end();


ActiveForm::end();

?>
