<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

$this->title = 'Mis Publicaciones';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(" 
jQuery(document).ready(function () {

$('.btn_vend').click(function(e){
    e.preventDefault();
    var modal = $('#modal-vendido').modal('show');
    $('#id_pub_sel').val($(this).attr('id_pub')); 
});

$('.btn_prom').click(function(e){
    e.preventDefault();
    //alert('".Url::to(['site/publicaranuncio4'])."?id='+$(this).attr('id_pub'));
    window.location = '".Url::to(['site/publicaranuncio4'])."?id='+$(this).attr('id_pub');
   
});
 
$('.btn_ok_vend').click(function(e){
    
    if($('input[type=\"radio\"]:checked').val()){
       //Guardo un post y hago submit
       if(confirm('¿Confirma que efectivamente vendió el vehículo? Si acepta no podrá reversar la actualización.')){
         $.post('".Url::to(['site/addvendido'])."', {id_publicacion: $('#id_pub_sel').val(), tipo: $('input[type=\"radio\"]:checked').val()}, 
           function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                    //console.log(res); return false;
                    if(res.res){                      
                         $('#w0').submit();
                    }else{
                        alert(res.error);
                    }
                
           }
         );
       }
    }else{
      alert('ERROR: Debes seleccionar alguna opción.');
    } 
});
 

});");

$this->registerJs(' 
jQuery(document).ready(function () {
                                               
    $(".btn_comp").click(function(e){
       e.preventDefault();
        var modal = $("#modal-compartir").modal("show");
        
         $.post("'.Url::to(['site/getbotonescompartir']).'", {id_publicacion: $(this).attr("id_pub")}, 
           function(res_sub) {
               var res = jQuery.parseJSON(res_sub);
               //console.log(res); return false;
               if(res.res){
                  $(".url_iconos").html(res.res);
                  
               }else{
                   alert(res.error);
               }
           }
         );
        $("#id_pub_sel1").val($(this).attr("id_pub")); 
    });
    
     $(".btn_pausar").click(function(e){
         e.preventDefault();
         //var modal = $("#modal-compartir").modal("show");
         var msg = "¿Estás seguro de PAUSAR tu publicación? Sólo podrás pausarla una vez por 20 días máximo. Pasados los 20 días, la publicación quedará nuevamente activa.";
         if($(this).attr("op")==2)
             msg = "¿Está seguro de ACTIVAR la publicación? No podrá PAUSAR nuevamente el anuncio."; 
         if(confirm(msg)){
             $.post("'.Url::to(['site/pausarpub']).'", {id_publicacion: $(this).attr("id_pub"), op: $(this).attr("op")}, 
               function(res_sub) {
                   var res = jQuery.parseJSON(res_sub);
                   //console.log(res); return false;
                   if(res.res){
                       alert(res.res);
                       $("#w0").submit();
                   }else{
                       alert(res.error);
                   }
               }
             );
         } 
    });
   $(".btn_eliminar").click(function(e){
         e.preventDefault();
        
          
         
             $.post("'.Url::to(['site/eliminarpubcli']).'", {id_publicacion: $(this).attr("id_pub"), op: $(this).attr("op")}, 
               function(res_sub) {
                   var res = jQuery.parseJSON(res_sub);
                   //console.log(res); return false;
                   if(res.res){
                       alert(res.res);
                       $("#w0").submit();
                   }else{
                       alert(res.error);
                   }
               }
             );
         
    });
         
        
    $(".btn_vend").click(function(e) {
        e.preventDefault();
     //   alert($(this).attr("id_pub"));
       
        /*if($("#id_tipo_seleccionado").val()!=""){
            
        }else{
          alert("Error: ¡Debes seleccionar un tipo de vehículo para continuar!");
        }*/
    });
    
    $("#vbuscadorpublicaciones-estado_pub").change(function(e) {
        $("#w0").submit();
    });
    
    $(".buscar").click(function(e) {
        $("#w0").submit();
    });
     
     
     
});');


$model = new \common\models\VBuscadorPublicaciones();

?>
<style>
    .text-titulo{
        font-family: Nunito;
        font-style: normal;
        font-weight: 300;
        font-size: 16px;
        line-height: 10px;
        letter-spacing: 0.457143px;
        color: #474747;
    }

    .select2-selection__placeholder{
        color: #F33B42 !important;
    }
    .select2-selection--single,  .select2-container--krajee{
        border: 1px solid #F33B42 !important;
        box-sizing: border-box !important;
        border-radius: 15px !important;
    }

    b,  .select2-selection__arrow{
        border-color: #F33B42 transparent transparent transparent !important;;
    }
    .tblanco{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 18px;
        display: flex;
        align-items: center;
        letter-spacing: 0.844486px;
        color: #FFFFFF;
    }

    .btn_prom {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }

    .btn_prom:hover  {
        padding: 10px;
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 15px;
        letter-spacing: 0.478377px;
        border: 0.5px solid #F33B42;
        box-sizing: border-box;
        border-radius: 3px;
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 15px;
        background: #FFFFFF;
        letter-spacing: 0.478377px;
        color: #F33B42;
    }
</style>

<!--div class="row" style="margin-top: 13px;border: 0px solid ;">
    <div class="col-lg-12" style="background: #EAF1F6;height: 70px;padding-top: 20px ">
        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
            <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
        </a>
        <span class="text-titulo"> Mis Anuncios / <b>Anuncios publicados</b></span>
    </div>
</div-->


<div  class="col-sm-12 col-md-12 col-lg-12" style="border: 0px solid ;background: #EAF1F6;height: 70px;padding-top: 20px;">
<div class="row">
    <div class="col-sm-6 col-md-6 col-lg-6">

            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
            </a>
            <span class="text-titulo"> Mis Anuncios / <b>Anuncios publicados</b></span>

    </div>
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/site/mispublicaciones']),
        'method' => 'get',
        //'options' => ['class' => 'form-inline form-group form-group-sm col-xs-12'],
        'fieldConfig' => [
            'template' => "{input}",
        ],
    ]); ?>
            <div class="col-sm-2 col-md-2 col-lg-2">
                <span class="glyphicon glyphicon-search form-control-feedback buscar" style=" right: 13px;"></span>
                <?= $form->field($model, 'palabras_search')->textInput(['style'=>'border: 1px solid #F33B42;box-sizing: border-box;border-radius: 15px;','maxlength' => true,'value'=> (isset($_GET['VBuscadorPublicaciones']['palabras_search']) && $_GET['VBuscadorPublicaciones']['palabras_search']!='') ? $_GET['VBuscadorPublicaciones']['palabras_search'] :'']) ?>
            </div>
            <div class="col-sm-2 col-md-2 col-lg-2">
                <?php
                if(isset($_GET['VBuscadorPublicaciones']['estado_pub']) && $_GET['VBuscadorPublicaciones']['estado_pub']!='')
                    $model->estado_pub = $_GET['VBuscadorPublicaciones']['estado_pub'];
                echo $form->field($model, 'estado_pub')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->_estado,
                    'options' => ['placeholder' => '- Todos los anuncios -','style'=>'border: 1px solid #F33B42;box-sizing: border-box;border-radius: 15px;',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

            </div>
            <div class="col-sm-2 col-md-2 col-lg-2 ">

                <a title="Nuevo Anuncio" href="<?= Url::to(['/site/publicaranuncio'])?>" class="  pull-left" style="margin-left:51px;height: 35px;display: flex;justify-content: center;width: 10%;background: #F33B42;border-radius: 15px;">
                    <!--span class="tblanco">Nuevo Anuncio</span-->
                    <img src="<?= Yii::$app->request->baseUrl.'/img/btn_nuevo_anuncio.png' ?>">
                </a>
            </div>
    <?php ActiveForm::end(); ?>
</div>

</div>


<div class="container">
    <div class="col-sm-12 col-md-12 col-lg-12">
           <?php
              echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper text-center mt-10',
                        'id' => 'list-wrapper',
                        'style' => 'margin-top:10px',
                    ],
                    'layout' => '{items}{pager}{summary}',
                   // 'layout' => "{summary}\n{items}\n{pager}",
                    'itemView' => function ($model, $key, $index, $widget) {
                        //\common\components\Utils::dumpx($model->id);
                        $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
                        $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('img_order asc')->one();

                        date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');
                        $visitas = \common\models\PubEstadisticas::find()->where('tipo=1 and id_publicacion='.$model->id)->count();

/*if($model->estado==2){
                            echo '<p>&nbsp;</p> 
                                <div class="row clearfix " style="padding-left: 20px;padding-right: 20px; "  >
                                
                                        
                                        <div class="col-sm-12 col-md-12 col-lg-12 "  id="cuadrado1">
                                             <div style="z-index: 3;text-align: center;font-size: 30px;vertical-align: middle;position: absolute;background: #fff;border-radius: 10px;margin-top: 5%;margin-left: 33%;">
                                               <center style="padding: 20px;color:#F33B42;font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 20px;line-height: 21px;text-align: center;letter-spacing: 1.3045px;color: #F33B42;">
                                                    Anuncio en proceso de aprobación
                                               </center>
                                             </div>
                                            <div id="cuadrado2" class="row" style="z-index: 1;border-radius: 8px;border: 1px ;min-height: 181px;background: #FFFFFF;box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.2);">
                                                    <div class="col-sm-3 col-md-3 col-lg-3 box2" style="margin-top: -1px;padding: 10px; display: flex; justify-content: center;">
                                                    <!--div class="ribbon ribbon-top-left"><span>Vendido</span></div-->';
                                                    if($foto)
                                                        echo '<img class="btn_galeria img-thumbnail img-responsive"   style=" max-height:180px;padding: 5px " src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" />';
                                                    else
                                                        echo '<img alt="no-image" style=" max-height:180px; padding: 5px" class=" img-respoensive" src="'. Yii::$app->request->baseUrl . '/img/no-image-moto.png'.' ">';

                                                    echo '</div>
                                                    <div class="col-sm-6 col-md-6 col-lg-6 " style="margin-top: 12px;">
                                                        <div style="width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 20px;line-height: 21px;letter-spacing: 0.637836px;color: #474747;">
                                                         '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.' ('.$model->tipo->nombre.')
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                                            '.number_format($model->ano,0,',','.').' -  '.number_format($model->recorrido,0,',','.').$model->_unid_recorrido[$model->unid_recorrido].' 
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #F33B42;">
                                                            $'.number_format($model->precio,0,',','.').'
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                                            '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-12 col-lg-12 col-sm-12" style="  " >
                                                                    <table class="table">
                                                                       <tr>
                                                                        <td style="border-top:0px;text-align: left"> <button class="btn-vtm-hover"   style=""  disabled>
                                                                                <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Marcar como vendido
                                                                            </button>
                                                                        </td>
                                                                        <td style="border-top:0px;text-align: left">
                                                                            <button class="btn-vtm-hover "  style=""  disabled>
                                                                                <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Promocionar
                                                                            </button>
                                                                        </td>
                                                                        <td style="border-top:0px;text-align: left">
                                                                             <button class="btn-vtm-hover btn_comp "  style=""  disabled>
                                                                                <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Compartir
                                                                            </button>
                                                                        </td>
                                                                        <td style="border-top:0px;text-align: left">
                                                                            ...                                                                    
                                                                        </td>
                                                                       </tr>
                                                                    </table>
                                                                </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 col-lg-3" style="margin-top: 0px;border-left: 1px solid #EAEAEA;padding-top: 10px">
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                                            Publicado:
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                                                           '.ucfirst(strftime("%A, %d de %B de %Y")).'  
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                                            Estado: 
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                                                            '.$model->_estado[$model->estado].'
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                                            Visitas:
                                                        </div>
                                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                                                             '.$visitas.'
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                </div>';
}
else
{*/
      echo  '<p>&nbsp;</p>
            <div class="row" style="padding-left: 20px;padding-right: 20px"  >
                <div class="col-sm-12 col-md-12 col-lg-12 ">

                <div class="row" style="border-radius: 2px;border: 1px ;min-height: 181px;background: #FFFFFF;box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.2);">

                        <div class="col-sm-3 col-md-3 col-lg-3 box2" style="margin-top: -1px;padding: 10px;">
                        <!--div class="ribbon ribbon-top-left"><span>Vendido</span></div-->' ;
                            if($foto)
                                echo '<a style="display: flex; justify-content: center;" title="Ver Detalle de la Publicación" href="'.Url::to(['site/pubdetalle','id'=> $model->id]).'">
                                          <img class="btn_galeria img-thumbnail img-responsive" data-method="post" style=" max-height:180px;padding: 5px;cursor:pointer" title="Click para ir al detalle de la publicación" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' .$foto->archivo . '" archivo="'.$foto->id.'"  />
                                      </a>';
                            else
                                echo '<a title="Ver Detalle de la Publicación" href="'.Url::to(['site/pubdetalle','id'=> $model->id]).'"><img alt="no-image" style="width: 100%;padding: 5px;max-height:165px" class=" img-respoensive" src="'. Yii::$app->request->baseUrl . '/img/no-image-moto.png'.' "></a>';

                        echo '</div>
                        <div class="col-sm-6 col-md-6 col-lg-6 " style="margin-top: 12px;">
                         <b style="float: right;font-size: 12px" >ID # '.$model->id.'</b>
                            <div style="width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 20px;line-height: 21px;letter-spacing: 0.637836px;color: #474747;">
                             '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'                             
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                '.number_format($model->ano,0,',','.').' -  '.number_format($model->recorrido,0,',','.').$model->_unid_recorrido[$model->unid_recorrido].' 
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #F33B42;">
                                $'.number_format($model->precio,0,',','.').'
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                            </div>
                            <div class="row">
                            
                             <div class="col-md-12 col-lg-12 col-sm-12" style="  " >
                                <table class="table" style="margin-bottom:0px !important;">
                                   <tr>
                                    <td style="border-top:0px;text-align: left">                                                                
                            ';
echo $model->estado;
                            /****  BOTON VENDIDO   *********/
                            if($model->estado==0 || $model->estado==1  || $model->estado==7)
                            {
                                echo ' <button class="btn-vtm btn_vend" id_pub="' . $model->id . '" style="width: 100%">
                                                <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Marcar como vendido
                                       </button> ';
                            }elseif($model->estado==6 || $model->estado==2)
                            {
                                echo '  <button class="btn-vtm-hover"   style="width: 100%" disabled  title="No puede marcar como vendida, una publicación en estado BORRADOR o PENDIENTE X APROBAR.">
                                            <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Marcar como vendido
                                        </button> 
 ';
                            }else{
                                    echo '
                                            <button class="btn-vtm-hover "   style="width: 100%"  disabled  title="Se definió que fue vendido '.(isset($model->vendido_por) ? $model->_vendido_por[$model->vendido_por] : '' ).'. ">
                                                <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Marcar como vendido
                                            </button>
                                       ';
                            }

                            echo '</td><td style="border-top:0px;text-align: left">';

                            /****  BOTON PROMOCIONAR   *********/
                           // if($model->estado==0 || $model->estado==6 || $model->estado==4) {
                                echo ' 
                                        <button class="btn-vtm btn_prom" id_pub="' . $model->id . '" style="width: 100%">
                                            <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Promocionar
                                        </button>
                                    ';
                            /*}else{
                                echo'
                                        <button class="btn-vtm-hover "  style="width: 100%"  disabled  title="Sólo se puede Promocionar cuando el plan de la publicación ha finalizado y que NO esté Bloqueada o se haya Vendido.">
                                            <i class="fa fa-check-circle-o" style="font-size: 14px"></i> Promocionar2
                                        </button>
                                      ';
                            }*/

                            echo '</td><td style="border-top:0px;text-align: left">';


                            /**** COMPARTIR REDES SOCIALES *******/
                           if($model->estado==6 || $model->estado==2){
                               echo' 
                                    <button class="btn-vtm-hover" id_pub="'.$model->id.'" style="width: 100%"  title="No se puede compartir. Es una publicación en estado BORRADOR o PENDIENTE X APROBAR.">                                         
                                          <img src="'.Yii::$app->request->baseUrl.'/img/ico_compartir_gris.png'.'"> &nbsp;Compartir
                                    </button>
                                    
                                 ';
                           }else{
                               echo' 
                                    <button class="btn-vtm btn_comp" id_pub="'.$model->id.'" style="width: 100%" >                                         
                                          <img src="'.Yii::$app->request->baseUrl.'/img/ico_compartir.png'.'"> &nbsp;Compartir
                                    </button>
                                    
                                 ';
                           }


                            echo ' </td>
                                   <td style="border-top:0px;text-align: left">
                                   <div class="btn-group pull-right">
                                          <button title="Acción" type="button" class="btn btn-vtm  btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-fw fa-ellipsis-v"></i>
                                          </button>
                                          <div class="dropdown-menu">
                                          <style>
                                             .rojito{
                                             font-family: "Montserrat";
                                                    font-style: normal;
                                                    font-weight: 400;
                                                    font-size: 10px;
                                                    line-height: 15px;
                                                    letter-spacing: 0.478377px;
                                                    color: #F33B42;

                                             }
                                          </style>
                                          ';

                                         // ACTIVO
                                          if($model->estado==1) {
                                              //Pausar - Activar
                                              if ($model->estado == 1 && $model->ultima_renovacion != "") {
                                                  echo '<button style="color:gray;text-decoration: none;text-decoration-line: line-through;" class="dropdown-item rojito" title="Ya se Pausó anteriormente. Solo se puede pausar 1 sola vez." >
                                                               <i style="color: gray;font-weight: bold;font-size: 16px;text-decoration-line: line-through;" class="fa fa-pause text-muted" aria-hidden="true"></i> &nbsp; Pausar
                                                           </button>';
                                              }elseif ($model->estado != 2 && $model->ultima_renovacion != "") {
                                                      echo '<button style="color: #F33B42;text-decoration: none" class="dropdown-item btn_pausar rojito" id_pub="' . $model->id . '" op="2" >
                                                                  <i style="color: #F33B42;font-weight: bold;font-size: 16px" class="fa fa-check" aria-hidden="true"></i> &nbsp; Activar
                                                              </button>';
                                                    } else{
                                                         echo '<button style="color: #F33B42;text-decoration: none" class="dropdown-item btn_pausar rojito" id_pub="' . $model->id . '" op="1" >
                                                                   <i style="color: #F33B42;font-weight: bold;font-size: 16px;    " class="fa fa-pause" aria-hidden="true"></i> &nbsp; Pausar
                                                               </button>   ';
                                                    }
                                              //Siempre Edito y Elimino
                                              echo '  <a style="color: #F33B42;text-decoration: none" class="dropdown-item btn_editar rojito"  id_pub="'.$model->id.'"   href="'.Url::to(['site/publicaranuncio2','id'=> $model->id,'tipo'=>$model->tipo->id]).'" >
                                                               <i style="color: #F33B42;font-weight: bold;font-size: 16px" class="fa fa-pencil" aria-hidden="true"></i> &nbsp; Editar
                                                            </a>';
                                              echo '<a style="color: #F33B42;text-decoration: none" class="dropdown-item btn_eliminar rojito" id_pub="'.$model->id.'" data-confirm="¿Está seguro de ELIMINAR el Anuncio? Si lo elimina, NO volverá a ver el anuncio. " href="#">
                                                            <i style="color: red;font-weight: bold;font-size: 16px" class="fa fa-close" aria-hidden="true"></i> &nbsp; Eliminar
                                                        </a>';

                                          }else{

                                              //2PENDIENTE x APROBAR + BLOQUEADO + VENDIDO + ELIMINADO: NO EDITAR
                                              if($model->estado==2 || $model->estado==3 || $model->estado==4 && $model->estado==8)
                                              {

                                              }else{

                                                  echo '  <a style="color: #F33B42;text-decoration: none" class="dropdown-item btn_editar rojito"  id_pub="'.$model->id.'"   href="'.Url::to(['site/publicaranuncio2','id'=> $model->id,'tipo'=>$model->tipo->id]).'" >
                                                               <i style="color: #F33B42;font-weight: bold;font-size: 16px" class="fa fa-pencil" aria-hidden="true"></i> &nbsp; Editar
                                                            </a>';
                                                  echo '<a style="color: #F33B42;text-decoration: none" class="dropdown-item btn_eliminar rojito" id_pub="'.$model->id.'" data-confirm="¿Está seguro de ELIMINAR el Anuncio? Si lo elimina, NO volverá a ver el anuncio. " href="#">
                                                            <i style="color: red;font-weight: bold;font-size: 16px" class="fa fa-close" aria-hidden="true"></i> &nbsp; Eliminar
                                                        </a>';

                                              }
                                          }
                        /*
                                 //LOGICA DE PLACA
                                  CREAR NUEVA:
                                          Validar que PLACA NO EXISTA en Publicaciones Estado:Activo

                                  Editar BORRADOR:
                                          Validar que PLACA NO EXISTA en Publicaciones Estado:Activo - sin contar con la propia


                                 BLOQUEAR CAMPOS AL EDITAR:
                                 ------------------------------------------------
                                             BORRADOR: NO BLOQUEAR NINGUN CAMPO
                                             ACTIVO + INACTIVO + RECHAZADO + PAUSADO : SI EDITAR  (editar todo, menos: Marca + modelo + placa)
                                             PENDIENTE x APROBAR + BLOQUEADO + VENDIDO + ELIMINADO: NO EDITAR

                              */
//1,5, 7
 /*
<option value="0" data-select2-id="9">INACTIVO</option>
<option value="1" data-select2-id="10">ACTIVO</option>
<option value="2" data-select2-id="11">PENDIENTE x APROBAR</option>
<option value="3" data-select2-id="12">BLOQUEADO</option>
<option value="4" data-select2-id="13">VENDIDO</option>
<option value="5" data-select2-id="14">RECHAZADO</option>
<option value="6" data-select2-id="15">BORRADOR</option>
<option value="7" data-select2-id="16">PAUSADO</option>
<option value="8" data-select2-id="17">ELIMINADO</option>
*/


                                                   /* else
                                                        echo '<button style="color: #F33B42;text-decoration: none" class="dropdown-item btn_pausar rojito" id_pub="'.$model->id.'" op="1" >
                                                                  <i style="color: #F33B42;font-weight: bold;font-size: 16px;    " class="fa fa-pause" aria-hidden="true"></i> &nbsp; Pausar
                                                              </button>
                                                             ';*/



                                                   echo '        <!--button-- style="color: #F33B42;text-decoration: none" class="dropdown-item btn_pausar rojito" id_pub="'.$model->id.'" op="1" >
                                                                  <img style="width:16px" src="'.Yii::$app->request->baseUrl.'/img/renovar_rojo.png" > &nbsp; Renovar (7 días restantes)
                                                              </button-->
                                          </div>
                                    </div>
                                   </td>
                                   </tr>
                                </table>
                            </div>';



                        $newDate = date("d-m-Y", strtotime($model->fecha_creacion));
                        $fecha_pub = utf8_encode(ucfirst(strftime("%A, %d de %B de %Y", strtotime($newDate))));



                        echo '</div>
                        </div>';

                        $query = 'SELECT `id`, `id_usuario`, `id_paquete`, `id_publicacion`, `dias_publicados`, `date_approve_payment` FROM `payments` WHERE `id_publicacion` ='.$model->id;
                    //    echo $query;
                        $pagos = Yii::$app->db->createCommand($query)->queryAll();
                        /* [0] => Array
  [0]  (
            [id] => 13
            [id_usuario] => 2
            [id_paquete] => 1
            [id_publicacion] => 14
            [dias_publicados] => 60
            [date_approve_payment] => 2021-10-16 01:01:04
        )

  [1] => Array
        (
            [id] => 14
            [id_usuario] => 2
            [id_paquete] => 1
            [id_publicacion] => 14
            [dias_publicados] => 60
            [date_approve_payment] => 2022-03-15 01:01:04
        )
                        */
         /*               $sum = 0;
                        for($i=0;$i<count($pagos);$i++){
                            $fecha1 = new DateTime(date('Y-m-d',strtotime($pagos[$i]['date_approve_payment'])));
                            $fecha2 = new DateTime(date('Y-m-d'));
                            $tot_dias = $fecha1->diff($fecha2);

                            echo "<br>tot_dias: ".$tot_dias->days.'<br>';
                            if($tot_dias->days < $pagos[$i]['dias_publicados'])
                               $sum = $sum + ($pagos[$i]['dias_publicados'] - $tot_dias->days);
                            echo "sum_actual: ".$sum.'<br>';
                        }

\common\components\Utils::dumpx($pagos);

<option value="0">INACTIVO</option>
<option value="1">ACTIVO</option>
<option value="2">PENDIENTE x APROBAR</option>
<option value="3">BLOQUEADO</option>
<option value="4">VENDIDO</option>
<option value="5">RECHAZADO</option>
<option value="6">BORRADOR</option>
<option value="7">PAUSADO</option>
<option value="8">ELIMINADO</option>

*/
                        switch ($model->estado){
                            case  0: $color='color: #F33B42'; break;
                            case  1: $color='color: #27e100'; break;
                            case  2: $color='color: #35D0F3'; break;
                            case  3: $color='color: #F33B42'; break;
                            case  4: $color='color: #27e100'; break;
                            case  5: $color='color: #6a6a6a'; break;
                            case  6: $color='color: #fbbc04'; break;
                            case  7: $color='color: #010101'; break;
                            case  8: $color='color: #F33B42'; break;
                        }

                        echo '
                        <div class="col-sm-3 col-md-3 col-lg-3" style="margin-top: 0px;border-left: 1px solid #EAEAEA;padding-top: 10px">
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                Publicado:
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 12px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                               '.$fecha_pub.'  
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                Estado: 
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 12px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                                <span style="'.$color.'">'.$model->_estado[$model->estado].'</span> 
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                Visualizaciones:
                            </div>
                            <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 500;font-size: 12px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                                 '.$visitas.'
                            </div>
                        </div>
                </div>
                </div>
            </div>';

        $ult_msg = \common\models\PubMensaje::find()->where('id_publicacion='.$model->id)->orderBy('id desc')->one();

        if($ult_msg){

            $user = \common\models\CliUsuario::find()->where('id_usuario='.$ult_msg->id_creador)->one();

            $newDate = date("d-m-Y", strtotime($ult_msg->fecha));
            $fecha_um = utf8_encode(ucfirst(strftime("%A, %d de %B de %Y", strtotime($newDate))));
            echo '<div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="row" style="margin-right: -10px;margin-top: 3px;background:#ffffff;margin-left: -9px;">
                        <div class="col-sm-4 col-md-4 col-lg-4" style="padding-top: 10px;">
                        <table style="width:100%">
                            <tr>
                            <td style=" vertical-align: baseline;">
                              <img style="width:34px" class="rounded  profile-user-img img-responsive img-responsive" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOIAAADiCAYAAABTEBvXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAByhSURBVHgB7Z0LcFxXece/c3dXD1uWZCfxO8k6IRDbIZZJKbYDeB2muARau9MmLQwQGwxDeYxjQVpoh1hKhzBAkG2mEKbE2AbKw+Zhd9IkTlu8hjwoM8UyxE6ICdrUTpzEDZafkqXde3q+c/dKu9qH7u7e17nn+82sd7XaKLb2/vf/ne9xDgMiFPCBQ52QSCTBNJIQhyRw3gmGcTUw1ikfg3hu7MXiOZDPlYdBJv+6QWBc3MQ9F/em+bx8nBXfZzlxn+1nC5YOAhE4DAhfEYJLCsGlhCCSUmicdYEUWRVheQpDIWbAELdc7jDkWD8YZobNv6EfCN8gIXrImOiM+BLhTF2W6IISXK1IN+0Xf+d+yJoHUaBswcIMEJ5AQnSRfHi5FmKxleLLFPCCcDIaZMS/KQ08dxBGY2kSpnuQEBuEH/9NCmKJNcI91kZQeJPAMHxNQ250H7vy9Wkg6oaEWCPS9eLxLnG7Q4Rta9UJNb1GJoT2Qo7vY1ct2gtETZAQHSKdj8TnkLwozewuckpnkBCrIN2vpWWjEN+dJL66sdaVI9BLa8rKkBDLkHe/zeICSgHhHgzXk7ldojSyE4giSIh5yP18JQNmrpcyr+NoL0QSYJDk15IUtuorRH7y6aS4w/BzHRDBw2GnzoLUTogkwJCjqSC1ESKFoIqhmSAjL0QSoNJkxHu2jc1dtBUiTqSFaLWfxXcA6NZ6FjlkljXKZY9ICjG/DtxBdcCIEeFw1YCIwV96BsPQQyTCCMJEgq2ZHRIftJshYkTGEfmJp7ogFttCAtSGDFyCVVFxx0g4onRBI0YuqBdJaIaBqLij0o5Ia0Eij/LuqKwj0lqQKCAp144vHr0TFEU5R5R1weZWEY5wZX/phJewrXBpqFe13emUEqIMRTkcAKoLEtVRLlRVJjQVIrxDhqIkQmJyrFD1xFPrQBGUEKLMjGExl1rUCMfgBs2xHapkVUMdmub7RLfQpATRELIjZ3hTmNeNoRWiXA+a7CdyY16CaJxQrxtDKURKyhAeEVoxhm6NKFvVSISEN2A3zgF5jYWMUDmi/AUZ8QOUlCG8hQ2CmV0VpoN2QuOIsjyB/aIkQsJzZEb1ED/52zsgJITCEa0aIZYnCMJnmLGOzXndLgiYwIVIIiQCJwRiDFSIJEIiNAQsxsCEaCVmcE1IECHBzC0NKoETiBBVzo4eefY5ePJXh+GFF1+GM+fOQ8e0Npg3dxYsvu5aWH7TEiBUJrhsqu9CVLVY/+T/HIYt3/i2FGEl2qdNheVv6ILb3vl2WJ1aAYSSBFL091WIqoqwt+9+eOD7P67pv5k/ZxZ0f+j9cNu73g6qcuLky/ID6PjJl+CEiABsNIgCfBejb0K0BnpblBtj6r7nS7DnwUehXlCQe75+n7ifDSqAwnv04BPwyMHHpRCdsPwNS2DZTTfK+8gIk7N+GBla5VejuH9CPPn0DtWmKLZ841vQJ8LRRmkXDoLu+MG/+QsIK/hhg7dqobcT7PB89coV8ob/dmURGX02b+F68AFfhJifJ+wBhcCkzJ++9yPgJijGTR96H4QJFF+f+MBx6n61snrlzVKQyoboDHrYnIW94DGeC1FOSRuxHaAYy9e815OLMyxidJJ8chMM0TF07f7w+5QJ08fwocboqRCt5AxTrn8UXQLXhl4RpBhPiMRLj0g+7RfrwKBAQaJDquOSoqxxiS/1MnnjmRBVTc4gt3/kk8Ipfg1e0tP9Ud/XjNtF5hfD0LPnLkAYUCyzLDKpw0u9St54J8QXn9mi4paH6BjL1/jjVn133+XLReh3GFor6giSbWVzr98EHuCJEOXmvyZX8kw7r8PSQjCjuPv++2Dxa68FLzh77rwUYK010KDAxE5P90dCvobkm7w4r9F1Iaq6LrTZcNdmX9dPXokRP1B6tnwtNGFoLYQxuzyON+tF94X44tMDoPA2F15lS6uBYuz77F2utMWFPQx1SsgbIdJs7sJV4CKuClHFemEhGMotfltwRXe76F9PETwqAiwk3I0Q7oaorgkx30c6AAqDToiOGCR24mL5TTdO6gb4wbH73x+FR9NPREqAEwltqOri2JR7QlQ8JLW58o//BMIC1ttw7YgN1h1tlkueOX8ejv72OTj67HNw5NhzoAs40dK3+S4IGa6FqK4IUfWQtJAwCZEoJpxidCdEbXgXt3yWNDJHpGFoSISTPSIM7+71p7TkHGMzH5AH5jb2U6BxNtMWiIRfoBi3uDAR4x7i2m+GhnupGxKibOiO2AExHW1TgQg32Ka3Px1cr2wZUvz40ylogMYc0YgpceRVLSg9P6cR3f/0JZk1Dg0x2CH7q+ukbiHKNrYInk9BQlQDFGH3PfdBiEhCU1PduZK6hGgdmRbNM+xJiOqw/+DjspEhNLDYxnpdsV5HxJA0CQQRMKFL3NTpijULMd9Bsw4iCpUv1AI7isLnirWXM+pxxMglaAi1CXK3gVLQFWvXSE1CjLobIleSIyoH1hZDlUFlsK5WV6zVEckNidCBIsRd90JFja7oWIg6uCHS3kZZUxUJV3gKeVd0nkGtxRG1cEMqX6jJ0bA5IlJDBjXu5EX5xu61uPVx1FFViPNnzoTF1yShfeoU+fWJV07Bk785ArqA53OEDquuuNXJzm+OhAi5XAqMmBaN3R2KCBEF98E//zNY/vrFeQGW9sieeOUVKca+7/5ACjPK+L29iTPG6oo9k73S0TxiVIZ+nRCGKf1qoPA2vfuv5X0t9H13N2z53g8gyjy57zshrAOzQTb3+umTvWrSNaKcsNCoiyasBX10wM0bPgC7772nZhEi3e+5HZ7cfr8IYa8Awk94p5PJjMmTNbHYHUAECopn/7Yvw4Y174RGwHXk/q98WYS07wLCR2KTJzqrCjFfskiBZoTJFXH9t0e44PxZM8ENcC3Z86H1MrwlfCM1WSljMkekAn6AoBPudlGEhWCo2nfnxyFKtId5qHuSUkZ1IWrohkgYpvRRhOiE5bKhbnHb21bBI9vuGyt5qE6oS0+ilFHt2xWFyI//JgWajjqF4Q3d45ETTmTxNQuk66ouRq/OD3GP6kmbyo4Yj2ubpAlaiJvefbsvIrRBMWISR+WMqhKHnxpwR+VvVUJ20qgLTm/j1nv2RkO1dOcHKUQUQ/d7/E+kYEZ1z+fvUVaMy266EUIPY2srJW3KCpG/cHSt6lsk4hFf3R9+P1wpPinxVCQ80wKFGaoh0jJgSBoUKotx8XVhD00Roal4S1e575R3RIOtgQiAZQg8/BI7LvDsBJxbu/1vPwUb7uqRB5JWIqjsGyZP/AxJy6GiGPF9Xn7TElCCCuFpeSEqHpaWAw8ysVugMGzFU4G3f/8nZV8bVGjaLdaGYUA1MSojQoSV11aJEK1saTR37pYi/M7XRdhqnUPY0/c16O27H8JAGNywEJXEiGdiqEP57GmpI7JY5NywEHS7B77UO3bMFx5rPfE8hSC2ywiLGxaighiVCkttGC/RWKkQDWMlaEDhmXtBH26CTdxhcsNC7P5UbLULI/g+KkeZHEyREGVvKfAu0AR8EzGZg6AYgwpTMSwNM9jd88i2L4fu74nvnf3+KUZyYhmj2BFxAFgz+u6+ayz1jWHqngcf9bU4jGFf2IVog72pYWkWt09WVpZEoig8LRZiLKZ8WIqDvWfPXRh7jLt7Ye2w2gT3A/f1jk1c9Gy539dp7+WvvwFUwm4WD3rdWPieKQkr1lrRhH5UJ/FRjNuF29nFfCzyY2/iMjwa+3XXSAfE72GNEWmfNnVMzF6z+95e5cSI4DYct33mbt+34MD3pmfTR1UNSQvJsLkLF9hfjAkxP3s4ABEHQ09seyt0Pfxk3fDuvxRi7Pd1Wz5cex35/rdAZR7Y9yBs/7cHfREkvk/ohGp00TihOcnmXvM8PhoXIra1MfYT0IQHvvdj6ZKFgvTTCZHVy/4YHvjHvwfVQXfc/Z9p2PNfP/VMkKtTK6Dvs38n36PIYObWs/k37MSH40I8cXSrSKtuBI1AEaI7oks6BUsNmFxZfkNxyQF3SzsyMADb9zl3B1xrqZKoccLZCxdg/y9+KQR5wLWtHLFGuGnD+9SrFTrB5NvY/EVyYLgwND2g6yBwuXB1IrXsnoYXYe83vimEman6OhzKxRGkKGJv5YjCPPL7gZqcEkW37A03ysb98M8ZNkRarBPlJ/G4EF985nRUW9ucgCLc8KnNcORY6Y7ROB9Yz2gSOkOlPUWjsD6sBXTLI7/PSIEef/mVou9hk32H+H0sWpCEK+fNgfarwtnc4D7jWy1KIfITT3WBETsEhOw/LWwGr1eENpWyi1FZH7pOTFTULtfp2AMrYWPVEZmRBELS0/3Rsda3RkWIYIvYk9u/XrKFIX76E2XImaAV5kUZmua33Me2NkebfmsBdmwsnjMfVr/xj8AtcAtDDEft3bajujZ0Bc5xXAi0gLEk3uUdkUUwJdUAI1lXRWiDXSl2ixjtuF0FrUzRuBr/zDsihqbRP+nJMcOj4BUoRlw3kiNWgaMSY6AFjMkhCytZo3nGtIRT57DGA0RATJ8C0OTsoDL1sTKnTI5jNLecBsJChKVw+iIQAdLeCtCaAG24NDzdgHhcm/lDR2Q1y9oRwZNIJA1dDiB1zGgOCMJXRPnQEKvEJBDj0NoweHSrJQoNGqJmQ45IEEEiNCjqiCwJxDgGNTYQfmNcjaFpBxDjkBAJv2FcZE0Zo9C0kGZd6ldEeGAdYo1IQiwiESNXJPwGs6aUrCkCm41bm4Ag/MQAopSpTeSKhK+I0JTqiCWgK05tBiIgtPsQZJ3kiJWY0mTdCP/RZRZxDE5CrMq0Fr2aj8OChssCEuJk4CQAhan+ouHynITohLZmS5Ax+nX5AjkiUREMUXFgtYVCVc/R8AOPhFgLeIF0tFrb/ZEgvUO7ZI0UIhsEojYmCpJCVveI6/m7jAPjg6KWSN019WALErk0CjCctQaLdZuncxM9P9Qy1OHsFs0J64bQvjf1E9dk97YJUEzlBRSq1k9CRyGyQbxiMkAQYUFLR+SDuFUGJWuIcICRREzHZnt+Bpu+zwBBhAEtw1IBZ6fFRxDPAEGEAW1rs+bzuFUGhaZEOGjS1BEZJms4JWuIEIBuqGFHjURoUAjRzABBBI3O42YmOuLoaAYIIkiwrU2b05/KkB3uN9iCpYPUb0oESpvO855sEDVot4BkgCCCoKWgNVBPMviHJURuHgaC8Bss4Le1gN6Yz+OfeUekWiIRAJ1TNO2kKYDzfrzLOyIJkfAZ3HpE09nDYpgUopWqisXSQMcCEn6A4Wh7i95Z0kLy5cOxuIC/+Mxp3F8RiMbBweD/Ow9EAbghFCZmMEOqa+G+BJExnXv9dHxUEBtYsSoRfUYuXQIz59MR5Ym4tR0lbiuC+8SSCAsY19x4fGDyw+JTKwVE5GlqboahoYswOjIKRiwGiUQCmpoSQiMurNnQ+cTPhJa41S1DwqsMai5PQaBuCnVq2nSrIa2tU4T4cnDxwgU4e8aahIvH49ZNCDMej8nHTsRpchOMFuF6rXFr7UficwaDtP1wXIiUsNGOmHCuae3tMGXqVCnI4eFhyGazIB4Uvc4QCRZ87URyIrxtnt4OU2fOoO1B6mGEjYWmRR9d/OTTA3Q6lAsomqxBYaEgR0ZGwDQn34mupWMaTLtqFhB1kWFzFy6wvyjOIZv8oAgrkkBoie2QyPDwEAxdHLIcsgItnW1A1AkfD0uR4niCm2kgCEFLSytMnzFD3lpaWsAwSkNPg2qB9cPhYOGXxb/d0dG9QBAFYMIGXfKyyy+Hjo4OKUpmJ2NilNyrm9FiRyxJb9E60QU0KOiPinVkYu5l1CtaF6xfFPKXFj5TGm/k+D4giElINNFpynVjmgcnPlUqRM4oPCUILymjsbJxBfWdNoguvaaXT6PQtGbG+0sLKV+F5SaFpwThBZyXjTjLC9FkO4EgCA8on4MpL8TscD9tKEUQbiPC0nmLnDui3NmNwlOCcJcKYSlSuVOXwlOCcBcTdlX6VtWUF2VP64SypkQpRU3eE6k+u8Jz24AgKuHXlH8UmNDkPZHqQhwZ2QoEUYkcDbA6ZgR6q327qhCt7firK5nQmNzkM4uEJM0WLMxUe8HkY9W56komNObiiPikz2I2sPR7Bk3sj1ElSWPjaKVNSZsKoCNkc1aIho/xXn6tmVPMuBxg9jxrLCpmzShmH34YzEOH5GPz1QolaZHoMebNHPuSTZ0ibq0A4samtFpf4/3My0BhqiZpbJxNdmLShhmbQUdQVPiJP1ogOrzH50xaI0k6ZuDWcEVPjT70H5A75N6RKoYQI7tihhQqu+IyMMSNzZwh743kfAgtprOI0pkQMWnT3LpRK1ds7QBITBVXQNz6pMcLbeBZEY5Rw1EJra0lT7kpQsR85VUAvFUAxWigMK8W9wvmQ0zcB++kbBBGedrJKx0JEZM2/IUjkXNFfuEi8FN/ADNzQjweglzmuBDakPj6BYjf+g5o2rC++D9o0v3kojLgXOIEN8wd8n+vanwP8Qa//PXYcxjaxha/BmKLXiuEOk88fi34Cud7J0vS2DiuxvKBQ53Q3HIaFKNQbDkpuItSaPgcPq4Ea2uDqY9M6PI7M2i5IjEOrg+vuqboqdHdP4JLX/kahJHY4usg/sYl8t7zkPYSLHAqRMe7/1iueHQXMHYHhIxCsWEIY5561ZHYqv7M8+dleBVbumT8ybZpQEwA14cTyP78cQgruSPH5A2R60shyETqTe67JYedTkWI1LYNl8F6xP8gMCFabnZCigzFxvOCM6usHRoh+/PHioWIa8XWKQBD9Yk7krSVbqloHnsOVEB+YKdfhWz6F1YY+8Yb3RPlSG1lv5obBYUr7vTaFbkQVu55EUa+8ge5bmvU3eoFw9MpP/xXeT/GC/8LcOolIARt7QCvub7oKVwfDn3ik6AytlM233ZrfQkfdMN5C9fX8p/UvjGldEW2xo0MalgEVwkZnv7qMMTfevP4kx3TSYg2uD6cQPahR0F1Cp0yJkPXZRAXN8eM1N4EU7MQ2ZyFmVozqPYaDmNza/1mhZdhEVw1Rvf8qFiIuE7EEJUansuumd0uWwSNvaY0dj8ETbffOrkga1wb2tQ1w2JlUFsHyrmidLmjx6wMpcdrOL+YsuNfwLju2vEnKDy1IoMF1xU9hUma4c/cDVEGw9b4qjdBYuWycmFrRmRKV9UjxLr2TMcMau7wz7cJkW0uDCtlHSeCjD78CDRf97HxJyg8FWHpFSVPZR/aD1EHI7oR4Y7ZA/8NiXelIHHrLQXf5dvYgkUZqIO6pzpPp1KdsRF2iGlwaE3ZpM1vn9I3e4pF/EVdRU/xl16GC3/1HtANdMh8yOqop7Tiz4E6mZ5ODzKD15QZUhVM2mCRuogyjqANs0sL4SPbJx0wiCTokMNf/bbjntJKNDSrMu2xdFr4RRo0YHT3j6Ugx8CMoY6HsKAbTkjSoBuOPhz9sLQSIqzcyeYv3AkN0PDQWDaXQ1eMfCd0iSuiCK+YDdqBkUDJpMUjoCtcXPujObPhmd2GhTj9F+mMCXoMD5e4IgpRJ1dEN8S5wwLQDUe++S3QFcZgG2oAGsSVMeqOx3+6VYcQtawrTrgwIw2tDYvgwDPTHvtpD7iAa/sZMJ7bBBogXVG4wBjoim3tEHlwTTyhk0b3tWEux1eBS7gmxLYn0v0iRI28GNEVL237avGT866CSFMmJEWGPt4N2sKg142Q1MbVHX50CVGxg6SolQsnMqIsRpw3nJCgGfnmLjBf0rOpwc2Q1Mb1rbZ0yaJe+twXSxM3HdMhcqATTgi9sz97XNsEDWZJ3QxJbVwXoi5ZVHSDkotROkeEjrRGEZbLkoZ0+t4PhGBcDUkLfq77yBBVpHUh4mAGtShExSzqaxZGQ4zlRHj+glwX6hqS4jXdJpdf7uPZLrDZuNnDOc9AxMFpg6IsKq6lVBYjfpigs1dIzui8LsRrGjzCMyFiL2rOlLF0pNeLuE4c+sxni5+0xYhJHJXAteDrbig78Hvp3i+A+bvfgY7Y60K8psEjPN0XHWNpzs3IlzRwj5bhe79Y/CSKES9qFdrg0AUx64vbXkzIjspw9BPdMBqByfu6EdewF+vCQjzvz/r88Uz/P1y9AMetUhBh7A2TYkuLx4OgvcO6uIcuhG+qHwU4ay5A8tqyTQkYcg91fxrMo0+Dtoh6YfvjBzw/Fc23UybP3rxqJ4PwbcXoNk0feL+4lflnoghxmPgPp3DndAgUFB1OUFwxa+ysiomYvxMu/+m79U3MgFwX7hIiXAc+UNeEfj3kEvzO+CgsEdrvgghjlzRKxGj3peL6CzcqPnXSX0Ha4pO36i15mA3Gf0dRnVQzMDmD1yz4hK/nLp9elkrGDHZAh6n+is5YyPlzlkPipL+b0/4YCmOiCAWH51Lg49jkn7kYig5/7guR2wCqVqQIMTnj8bqwEN8PQNdJjPG33AzNGz8GbPasyV+cywoxDlmCHBm2QtlyjllYFkHBxQoOySlzDoUTMCEzuvuHpWNeGhKECBHfhYicX5Hq4sw4IB5G/nQpY/ZsaP3nPmdiDADc8AnDUJ3XgjayTMHNVdOfSPt+ik4gQkTyYjwEmuAoVPUJcsDyZLm5NAgRIoEJETm7IrWOMWMHaAK6IwoyfutqCALcDj/7sycg+/B+EuAEGDfXtz2R3gkBEagQEd3EiNiCjL31zcDapoJXoPOZx46R+CYhaBHKvwOEAB3FaBN/x2qIv3UFxJYubViU/ORLkOvvh9yzv5ftaNhkQOKrThhEKP8eEBJ0FqMNbuuPbinv58ySR57ZmxpLQeVFxc9dkF+bJ1+Wz0nRnXwZiNoIiwiR0AgR0SmbSgRHkNnRSnja9F0ruO+N+JRapcP4FBEMsk4YMhEioXJEG52K/oR/BFWsd0KoHNEGf1E4y0jOSLgH7w+rCJFQChGRYmziS7EDHgiiAfAayibCK0IklKHpRM69+ZYe8dt0fEIxQYzBoNftrQ+9QAkhImdX3CLKG7AFKKNKOAAzowY3N4WlPDEZyggRoSQO4YQwJ2UqEdo1YjnsdaMOWzUSdSKujVyCL1VJhIhSjljImZtvuVN8iuC6kUJVwgpFAXq92nfUa5QVIkKhKmHB09kcX6+aCxaitBBtKKuqL+IC3qSqCxYSCSEi5I66IVyQ801ha1Wrl8gI0SbvjhuB1o6RRPW1YCUiJ0REumOM9eiwj6peqL8WrEQkhWiDTQDizdtM4araYF2QMb5+2mPpNESUSAvRBksdjPONJEi1wDCUibpgNm5u9fIAmDCghRARClfVQScB2mgjRBsSZLgRF+TO0ZzZG8V1YDW0E6INCTJc6CpAG22FaGMLUvwq1jAqefiKjiFoJbQXoo3VEGCkKMvqPSTAUkiIZbBmHzFk5SkgXISngfHeKJch6oWEWIWxsJXDSnLJ+iD3cwYJ0SHn3pxKcc7W0VpycvJtaHs5M3eR+zmDhFgHZ9+SWgsmW0uiHKdQfNk49JP71QYJsUHQKcE01oq1z8qoH0s+Ec4hIy6gfWCYe8n5GoOE6CJjmVcmkjwRXFei8AwGaZPDwVyTuZdczz1IiB4ihZmALpYzhDjZEg68S5VQViZZgPcDZ4fF4/6EaaZbNS22+wEJ0WfwoB0zBkmeM7rEmmqJcM8kB5YMSqB5wYkQk/XnODwv/h4ZEp3/kBBDwulUqjOehS7OoJNnIQnM6IwxuFqEg53CTYVIzU6RtZViFeWAZKWfg8ISf4yFjCI8zoiYEksIgyg04OYgi4sQMweZ0SbIUHgZDv4fuQgYAEqCNtEAAAAASUVORK5CYII=" class="user-image" alt="Sin Avatar"/>
                            </td>
                            <td>
                              <p style="font-family: \'Montserrat\';font-style: normal;font-weight: 400;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                              '.$fecha_um.'  </p>
                              <p style="font-family: \'Montserrat\';font-style: normal;font-weight: 500;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #F33B42;">
                              '.$user->nombre.'
                              </p>
                             
                            </td>
                            </tr>
                          </table>                  
                        </div>
                        <div class="col-sm-8 col-md-8 col-lg-8" style="    margin-left: 0px;    background: #F8FBFD;font-family: \'Montserrat\';font-style: normal;font-weight: 300;font-size: 14px;line-height: 17px;letter-spacing: 0.446485px;color: #474747;">
                               <p style="margin-top: 10px">'.$ult_msg->mensaje.'</p> 
                        </div>
                    </div>
            </div>';
         }
         echo '
            
            ';
//}
                    },
                    'pager' => [
                        'firstPageLabel' => 'Inicio',
                        'lastPageLabel' => 'Fin',
                        'nextPageLabel' => '>>',
                        'prevPageLabel' => '<<',
                        'maxButtonCount' => 5,
                    ],
                ]);
           ?>
    </div>

</div>

<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-vendido',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
   // 'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
<input type="hidden" id="id_pub_sel" value="">
<div style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 38px;line-height: 46px;text-align: center;letter-spacing: 1.21189px;color: #F33B42;margin-bottom: 15px">
    ¡Vendido!
</div>
<center style="font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 17px;line-height: 15px;text-align: center;letter-spacing: 0.54216px;color: #474747;">
    Por favor indica el canal de venta
</center>
<br><br>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
    <div class="col-sm-6 col-md-6 col-lg-6"  >
        <div style="font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 15px;line-height: 21px;letter-spacing: 0.54216px;color: #F33B42;">
            <input class="form-check-input" type="radio" value="1" name="caracteristica" >  Lo vendí por Vende Tu Moto.<br><br>
            <input class="form-check-input" type="radio" value="2" name="caracteristica" >  Lo vendí por otro lugar.<br><br>
            <input class="form-check-input" type="radio" value="3" name="caracteristica" >  No, no lo vendí.<br><br>
            <input class="form-check-input" type="radio" value="4" name="caracteristica" >  Prefiero no responder.<br><br><br>
            <center><button type="button" class="btn btn-danger btn_ok_vend">Aceptar</button></center><br><br><br>
        </div>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
</div>

<?php \yii\bootstrap\Modal::end(); ?>



<?php

$url_self = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-compartir',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    // 'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
    <input type="hidden" id="id_pub_sel1" value="">

    <center style="margin-top: 30px;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 17px;line-height: 15px;text-align: center;letter-spacing: 0.54216px;color: #474747;">
        Comparte tu publicación en:
    </center>
    <br><br>
    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
        <div class="col-sm-6 col-md-6 col-lg-6"  >
            <div style="font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 15px;line-height: 21px;letter-spacing: 0.54216px;color: #F33B42;">
                <center class="text-center url_iconos">
                </center>
                <br><br><br>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3"  ></div>
    </div>

<?php \yii\bootstrap\Modal::end(); ?>
