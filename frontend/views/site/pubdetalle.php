<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Detalle Publicación';
$this->params['breadcrumbs'][] = $this->title;
$url_self = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$this->registerJs(" 
jQuery(document).ready(function () {

    $('.btn_galeria').click(function(e){  
        e.preventDefault();                                             
        var modal = $('#modal-fotos').modal('show');    
        var that = $(this);
        modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
        $('#div_image').attr('src',that.attr('url_image'));
                                 
    });
    
   $('.btn_descargar').click(function(e){
        e.preventDefault();  
        if($(this).attr('id_pub')!=''){
          if(confirm('¿Confirma la descarga de las fotos de este anuncio en un archivo comprimido (.ZIP)?')){
            window.open('".Url::to(['site/descargarimagenes','id_publicacion'=> $model->id])."', 'width=800, height=600');
          }
        }else{
            alert('ERROR 105: ¡ID_PUBLICACION VACÍA!');
        } 
}); 
});");
?>
<style>
    .panel-body{
        border: 0px;
    }
    .sub1{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 28px;
        line-height: 34px;
        letter-spacing: 0.89297px;

        color: #F33B42;
    }

    .sub2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        line-height: 16px;
        letter-spacing: 0.414593px;
        text-decoration-line: line-through;
        color: #A0A0A0;
    }
    .sub3{
        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 17px;
        line-height: 21px;
        letter-spacing: 0.54216px;
        color: #474747;
    }

    .sub4{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 275;
        font-size: 16px;


        align-items: center;
        letter-spacing: 0.031794px;

        color: #A0A0A0;
    }
    .sub5{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        align-items: center;
        letter-spacing: 0.031794px;
        border: 1px solid #fff;
        color: #474747;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border: 1px solid #fff;
    }
    .sub6{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        display: flex;
        align-items: center;
        letter-spacing: 0.031794px;

        color: #474747;
    }
    .iconos{
        width: 37px;display: inline-block;text-rendering: auto;-webkit-font-smoothing: antialiased;margin-top: -22px;
    }
    .btnprom{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        /* identical to box height */

        text-align: center;
        letter-spacing: 0.54216px;

        color: #FFFFFF;
        background: #F33B42;border-radius: 25px;
        width: 100%;
        padding: 10px;
    }
    .btnprom2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        border: 1px solid #F33B42;
        box-sizing: border-box;
        border-radius: 25px;

        text-align: center;
        letter-spacing: 0.54216px;

        color: #F33B42;
        background: #F9F7F7;
        border-radius: 25px;
        width: 50%;
        padding: 10px;

    }
    .ove {
        min-height: 393px !important;
        text-align: center !important;
    }


    .ove2 {
        min-height: 177px !important;
        text-align: center !important;
    }

    .ove2 > .carousel-control{
        line-height: 181px;
    }
    .detalle1{

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 24px;
        line-height: 29px;
        display: flex;
        align-items: center;
        letter-spacing: 0.047691px;
        color: #353435;
        margin-left: 10px;
    }

    .detalle2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        letter-spacing: 0.031794px;
        color: #353435;
        float: left;
        margin-left: 10px;
        text-align: justify;
    }


    .padre0 {
        position: relative;
       /* background-color: green;*/
    }
    .reco_cuadro0 {
        position: absolute;
        color: #ffffff;
        width: fit-content;
        width: 100% !important;
    }
    .reco_texto0 {
        display: flex;
        justify-content: center;
        width: 100%;
        horiz-align: center;
        font-family: Source Sans Pro;
        font-size: 44px;
        line-height: 52px;
        align-items: center;
        text-align: center;
        color: #FFFFFF;
    }

    .reco_texto1 {
        display: flex;
        justify-content: center;
        width: 100%;
        horiz-align: center;

        line-height: 52px;
        align-items: center;
        text-align: center;
        color: #FFFFFF;
        font-size: 18px;
        margin-top: 63px;

        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: 700;
        font-size: 13px;
        line-height: 16px;
        text-align: center;

    }

    .reco_texto01 {
        display: flex;
        justify-content: center;
        width: 100%;
        horiz-align: center;
        font-family: SourceSansPro;
        font-size: 44px;
        line-height: 52px;
        align-items: center;
        text-align: center;
        color: #F33B42;
    }
    .reco_texto001 {
        display: flex;
        justify-content: center;
        width: 100%;
        horiz-align: center;

        align-items: center;
        text-align: center;

        font-size: 18px;
        margin-top: 63px;

        color: #F33B42;
        font-family: 'Source Sans Pro';
        font-style: normal;
        font-weight: 700;
        font-size: 13px;
        line-height: 16px;
        text-align: center;

        color: #F33B42;
    }
    .reco_cuadro01 {
        position: absolute;
        color: #ffffff;
        width: fit-content;
        width: 100% !important;
    }

    .reco_texto02 {
        margin-left: 55px    ;

        margin-top: 28px;
        margin-right: 4px;

        display: flex;
        align-items: center;
        text-align: center;

        color: #F33B42;
        font-family: Nunito;
        font-style: normal;
        font-weight: bold;
        font-size: 35px;
        line-height: 35px;
        text-align: center;

        color: #2E3A59;
    }
    .reco_cuadro02 {
        position: absolute;
        color: #F33B42;
        top: 6px;
        left: 0;
        right: 0;
        width: fit-content;
        height: 25px;
    }
 
    .carousel-control {
        line-height:384px;
        font-size:54px !important;
        width:80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }

    .flexbox {
        align-items: center;
        display: flex;

        justify-content: center;

        color: #333;
    }
    .carousel-inner{
        display:flex;justify-content:center;
    }
</style>



<div  class="col-sm-12 col-md-12 col-lg-12" style="border: 0px solid ;background: #EAF1F6;height: 70px;padding-top: 20px;">
    <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-8">

            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: 0px solid;background-color: #eaf1f6 !important;">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/flecha-correcta.png" >
            </a>
            <span class="text-titulo"> Mis Anuncios / <b>Anuncios publicados</b> / <?=$model->modelo->marca->nombre.' / '.$model->modelo->nombre?></span>

        </div>
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/site/mispublicaciones']),
            'method' => 'get',
            //'options' => ['class' => 'form-inline form-group form-group-sm col-xs-12'],
            'fieldConfig' => [
                'template' => "{input}",
            ],
        ]); ?>

        <div class="col-sm-2 col-md-2 col-lg-2"  >
            <a title="Editar Anuncio"  href="<?= Url::to(['site/publicaranuncio2','id'=>$model->id,'tipo'=>$model->tipo->id])?>"   class=" pull-right" style=" display: flex;justify-content: center; ">
                <img src="<?= Yii::$app->request->baseUrl.'/img/btn_editar_anuncio.png' ?>">
            </a>

        </div>
        <div class="col-sm-2 col-md-2 col-lg-2 ">

            <a title="Nuevo Anuncio" href="<?= Url::to(['/site/publicaranuncio'])?>" class="  pull-left" style="display: flex;justify-content: center;">
                <img src="<?= Yii::$app->request->baseUrl.'/img/btn_nuevo_anuncio.png' ?>">
            </a>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>



<div class="container">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-12" style=";margin-top: 20px">
            <div class="row">
                <div class="col-lg-2 col-sm-12 col-md-12" style=" height: 424px;background: #fff;padding: 10px;padding-bottom: 30px; color: inherit;overflow-y: auto;max-width: 424px;">
                    <?php
                    foreach ($imagesPeke as $img){
                        echo $img.'<br>';
                    }
                    ?>
                </div>
                <div class="col-lg-10 col-sm-12 col-md-12 text-center" style="background: #fff;padding: 5px 10px 0px 10px;padding-bottom: 30px; color: inherit;height: 424px; ">
                    <div id="padre" class="ibox-content no-padding border-left-right" style="height: 394px !important;height: 394px !important;border: 0px;background: #ffffff;">
                        <?= yii\bootstrap\Carousel::widget(['items'=>$images, 'options'=>['class'=>'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 " style="">
                        <p class="detalle1" style="margin-top: 30px;margin-left: -2px;">
                            Detalles
                        <hr style="border: 1px solid #FF596A;width: 110px; margin-top: 1px;margin-bottom: 20px" class="pull-left">
                        </p>
                        <p class="sub3 text-left " style="clear: both"><?= $model->modelo->marca->nombre.'  '.$model->modelo->nombre ?></p>
                        <p class="detalle2" style="margin-left: -2px;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.031794px;color: #353435;">
                            <?= $model->descripcion ?>
                        </p>
                    </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12" style=" ">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" style="  min-height: 577px;margin-top: 20px ">

                <div style="background: #fff; padding: 20px">
                    <b style="float: right;font-size: 12px" >ID # <?= $model->id ?></b>
                    <p class="sub1">Ahora: $<?= ($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))),0,',','.') : number_format($model->precio,0,',','.') ?></p>
                    <p class="sub2"><?= ($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '' ?></p>
                    <p class="sub3"><?=  $model->modelo->marca->nombre.'   '.$model->modelo->nombre ?></p>
                    <table class="table" >
                        <tr>
                            <th class="sub4">Año</th><th class="sub4">Recorrido</th><th class="sub4">Cilindraje</th>
                        </tr>
                        <tr>
                            <td class="sub5" width="33%"><?= $model->ano ?></td>
                            <td width="33%" class="sub5"><?= number_format($model->recorrido,0,',','.').' '. $model->_unid_recorrido[$model->unid_recorrido] ?></td>
                            <td  width="33%" class="sub5"><?= number_format($model->cilindraje,0,',','.') ?>cc</td>
                        </tr>
                    </table>

                    <div class="text-left">
                        <p class="sub6 text-left" style="width: 100%; ">Comparte este anuncio:</p>
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $url_self ?>&t=<?= 'Vendo '.$model->modelo->marca->nombre.' / '.$model->modelo->nombre.' ficha técnica aquí: ' ?> "><i class="fa fa-facebook-square" style="font-size: 40px;color: #425EAC"></i></a>
                        <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?= $url_self ?>" style="margin-left: 10px"><i class="fa fa-linkedin-square" style="font-size: 40px"></i></a>
                        <!--a target="_blank" href="#" style="margin-left: 10px"><i class="fa fa-instagram" style="font-size: 40px;color: #B3318B"></i></a-->
                        <a target="_blank" href="https://api.whatsapp.com/send?text=<?= 'Vendo '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.'%20'.$url_self ?>" style="margin-left: 10px"><img class="iconos" src="<?= Yii::$app->request->baseUrl . '/img/wp.png' ?>"></img></a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= 'Vendo '.\common\components\Utils::limpiarCadena2($model->modelo->marca->nombre.' / '.$model->modelo->nombre).' - Ficha técnica aquí: ' ?>&url=<?= $url_self ?>" style="margin-left: 10px"><i class="fa fa-twitter-square" style="font-size: 40px;color: #1DADEB"></i></a>
                    </div>
                    <hr style="background: #DEDEDE;transform: matrix(1, 0, 0, -1, 0, 0);">

                    <p class="sub3"><?php
                        //Seleccioar Plan Actual  de esa publicación

                        $plan_actual = \common\models\Payments::find()->where('id_publicacion='.$model->id.' and state=1')->orderBy('created_at DESC')->limit(1)->one();
                        if(!$plan_actual){
                            echo "<em style='color:red'>No tiene un plan asociado</em>";
                        }else
                            echo $plan_actual->paquete->nombre.' <em style="color: #A0A0A0;font-size:14px;font-weight: normal;">(Adquirido en '.date('Y-m-d',strtotime($plan_actual->created_at)).')</em>' ;


                        ?></p>
                    <p class="sub4">Días restantes:</p>
                    <p class="sub3"><?php
                        //Calcular días Faltantes del plan_
                        if(!$plan_actual){
                            echo "0";
                        }else{
                            $fecha1 = new DateTime(date('Y-m-d',strtotime($plan_actual->created_at)));
                            $fecha2 = new DateTime(date('Y-m-d'));
                            $dias = $fecha1->diff($fecha2);
                            echo $dias->days;
                        }


                        ?></p>
                    <p style="text-align: center; margin-top: 30px" ><a  href="<?= Url::to(['site/publicaranuncio4','id'=>$model->id])?>" class="btn btn-danger btnprom" style="">Promocionar</a></p>
                </div>

        </div>
        </div>
    </div>
    </div>
    <div class="row">

        <div class=" row " style="margin-top: 30px">

            <div class="col-lg-3 col-sm-6 col-md-6 text-center  flexbox"  >
                <image class="" src="<?= Yii::$app->request->baseUrl . '/img/visitas.png' ?>"/>
                <div class="reco_cuadro0">
                    <span class="reco_texto0">
                        <p><?php
                            $visitas = \common\models\PubEstadisticas::find()->where('id_publicacion='.$model->id.' and tipo=1')->count();
                            echo $visitas;
                            ?></p>
                    </span>
                </div>
                <div class="reco_cuadro0">
                <span class="reco_texto1">
                    <p>Visitas</p>
                </span>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-md-6 text-center flexbox">
                <img class="" src="<?= Yii::$app->request->baseUrl . '/img/interacciones1.png'  ?>" />
                <div class="reco_cuadro01">
                <span class="reco_texto01">
                    <p> <?php
                        $interacciones = \common\models\PubEstadisticas::find()->where('id_publicacion='.$model->id)->count();
                        echo $interacciones;
                        ?></p>
                </span>
                </div>
                <div class="reco_cuadro01">
                <span class="reco_texto001">
                    <p>Interacciones</p>
                </span>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-12 text-center">
                <div class=" row ">

                    <div class="col-lg-6 col-sm-6 col-md-6 text-center ">
                        <img class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/save_favorito.png' ?>" />
                        <div class="reco_cuadro02">
                        <span class="reco_texto02">
                            <p><?php
                                $favoritos = \common\models\PubEstadisticas::find()->where('id_publicacion='.$model->id.' and tipo=3')->count();
                                echo $favoritos;
                                ?></p>
                        </span>
                        </div>

                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 text-center">
                        <img class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/save_llamada.png' ?>" />
                        <div class="reco_cuadro02">
                        <span class="reco_texto02">
                            <p><?php
                                $calls = \common\models\PubEstadisticas::find()->where('id_publicacion='.$model->id.' and tipo=5')->count();
                                echo $calls;
                                ?></p>
                        </span>
                        </div>
                    </div>
                </div>
                <div class=" row " style="margin-top: 10px;">
                    <div class="col-lg-6 col-sm-6 col-md-6 text-center">
                        <img class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/save_mensajes.png' ?>" />
                        <div class="reco_cuadro02">
                        <span class="reco_texto02">
                            <p><?php
                                $msgs = \common\models\PubMensaje::find()->where('id_publicacion='.$model->id.' and id_creador<>'.Yii::$app->user->identity->id)->count();
                                echo $msgs;
                                ?></p>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 text-center">
                        <img class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/save_whatsapp.png' ?>" />
                        <div class="reco_cuadro02">
                        <span class="reco_texto02">
                            <p><?php
                                $wps = \common\models\PubEstadisticas::find()->where('id_publicacion='.$model->id.' and tipo=4')->count();
                                echo $wps;
                                ?></p>
                        </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
/*

*/
?>
