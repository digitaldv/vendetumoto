<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\CliUsuario;




$this->title = 'Detalle Publicación';
$this->params['breadcrumbs'][] = $this->title;
$url_self = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_home='';
if (Yii::$app->user->isGuest) {
    $url_home = 'https://api.whatsapp.com/send?phone=573164138276&text=Quiero más información de la '.$model->tipo->nombre.' / '.$model->modelo->marca->nombre.' / '.$model->modelo->nombre.' publicada en *Vende Tu Moto* &'.$url_self;
}

$this->registerJS("
$('.btn_favorito').click(function(e){
    
        e.preventDefault();          
        if($(this).attr('id_usuario')=='' && $(this).attr('logueado')==0){
            //alert('¡Primero debes Iniciar Sesión o Registrarte!');
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            modal.find('.modal-title').html('Debes iniciar sesión para guardar en favoritos');                           
        }else                                   
            if($(this).attr('id_pub')!='')
            {
                 $.post('".Url::to(['site/favoritoupdate'])."', {id_publicacion: $(this).attr('id_pub'), id_usuario: $(this).attr('id_usuario') }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==2)  {                    
                               alert('¡Publicación ha sido eliminada de tus favoritos con éxito!');
                               $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg2 \"><i class=\"fa fa-heart-o\"></i></div>');
                            }else {                                  
                                   alert('¡Publicación agregada a tus favoritos con éxito!');
                                  $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg danger \"><i class=\"fa fa-heart\"></i></div>');
                              }
                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
                 
            }else{
              alert('Error: ID_PUB no existe.');
            }      
         
    });
");

if (Yii::$app->user->isGuest) {
    $this->registerJs(" 
        jQuery(document).ready(function () {

//******************************************************
    
     $('#btn_iniciar_sesion').click(function(e){
        e.preventDefault();   
        
        if($('#loginform-username').val()!='' && $('#loginform-password').val()!='')
        {      
        
            if($('#loginform-username').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#loginform-username').val($('#loginform-username').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#loginform-username').val()).toLowerCase())){                      
                  $('.field-loginform-username > .help-block').html('¡El Correo es incorrecto!');
                  return false;
               }
            }   
        
        
             $.post('".Url::to(['site/emailexiste'])."', {email: $('#loginform-username').val() }, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        console.log(res); //return false;
                        if(!res.res){  //FALSE: SI EXISTE: Ahora pregunte si el password es correcto                    
                             //Preguntar por contraseña
                              $.post('".Url::to(['site/existepassword'])."', {email: $('#loginform-username').val(), pass: $('#loginform-password').val() }, 
                                   function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub); 
                                            console.log(res); //return false;
                                            if(res.res){  //TRUE: SI ES EL PASSWORD: LOGUEARSE....                    
                                                 $('#login-form').submit();                                                  
                                            }else{
                                                $('.field-loginform-password > .help-block').html('<b style=\'color:red\'>¡Contraseña Incorrecta!</b>');
                                                return false;
                                            }
                              });
                        }else{
                            $('.field-loginform-username > .help-block').html('<b style=\'color:red\'>¡Correo NO Existe en VTM!</b>');
                            return false;
                        }
                    
             });
        }else{
          if($('#loginform-username').val()=='') $('.field-loginform-username > .help-block').html('Debe ingresar el correo');
          if($('#loginform-password').val()=='') $('.field-loginform-password > .help-block').html('Debe Ingresar su contraseña');
        }
    }); 
    
    $('#cliusuario-nombre').change(function(e){     
        if($('#cliusuario-nombre').val()!='') $('.field-cliusuario-nombre > .help-block').html('');
    });
    
    $('#signupform-email').change(function(e){     
        if($('#signupform-email').val()!='') $('.field-signupform-email > .help-block').html('');
    });
    
    $('#signupform-password').change(function(e){     
        if($('#signupform-password').val()!='') $('.field-signupform-password > .help-block').html('');
    });
    
    $('#signupform-repeat_password').change(function(e){     
        if($('#signupform-repeat_password').val()!='') $('.field-signupform-repeat_password > .help-block').html('');
    });
    
    $('#cliusuario-telefonos').change(function(e){     
        //if($('#cliusuario-telefonos').val()!=''){ 
          $('.field-cliusuario-telefonos > .help-block').html('');
        //}
    });
    
    $('#cliusuario-whatssapp').change(function(e){     
        //if($('#cliusuario-whatssapp').val()!=''){ 
            $('.field-cliusuario-whatssapp > .help-block').html('');
        //}
    });
    
    $('#loginform-username').change(function(e){     
            $('.field-loginform-username > .help-block').html('');
    });
    
    $('#loginform-password').change(function(e){     
            $('.field-loginform-password > .help-block').html('');
    });
    
    $('#signupform-acepto_terminos').change(function() {
        if(this.checked) {
             $('.field-signupform-acepto_terminos > .help-block').html('');
             $('.normal_text2').attr('style','border: 0px');     
             
        }
    });
    
    $('#btn_reg').click(function(e){
        e.preventDefault();   
        
        if($('#cliusuario-nombre').val()!='' && $('#signupform-email').val()!='' && $('#signupform-password').val()!='' && $('#signupform-repeat_password').val()!='' && $('input[id=signupform-acepto_terminos]:checked').val())
        {            
            
            if($('#signupform-email').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#signupform-email').val($('#signupform-email').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#signupform-email').val()).toLowerCase())){                      
                  $('.field-signupform-email > .help-block').html('¡CORREO INVÁLIDO!');
                  return false;
               }
            }   
            
            if($('#signupform-password').val().length < 6){
                    $('.field-signupform-password > .help-block').html('La contraseña debe ser de 6 caracteres.');
            }
            if($('#signupform-repeat_password').val().length < 6){
                    $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe ser de 6 caracteres.');
                    
            }
            
            if($('#signupform-password').val() != $('#signupform-repeat_password').val()){
                $('.field-signupform-repeat_password > .help-block').html('Confirmación contraseña NO concuerda con la contraseña.');
             }else{
                $('.field-signupform-password > .help-block').html('');
                $('.field-signupform-repeat_password > .help-block').html('');
             }
                     
            
            var filter = /^\d*(?:\.\d{1,2})?$/;
                        
            if($('#cliusuario-telefonos').val()!=''){
               if(filter.test($('#cliusuario-telefonos').val())) {
                   if($('#cliusuario-telefonos').val().length < 10){
                       $('.field-cliusuario-telefonos > .help-block').html('El número de contacto debe tener 10 dígitos.');
                       $('#cliusuario-telefonos').focus();
                   }
               }else{
                  $('.field-cliusuario-telefonos > .help-block').html('El Número de contacto NO es un número válido.');                 
               }
            } 
            
            if($('#cliusuario-whatssapp').val()!=''){
               if(filter.test($('#cliusuario-whatssapp').val())) {
                   if($('#cliusuario-whatssapp').val().length < 10){
                       $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp debe tener 10 dígitos.');
                       $('#cliusuario-whatssapp').focus();
                   }
               }else{
                  $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp NO es un número válido.');                 
               }
            }           

            //Si llegó hasta acá y checkeó, REGISTRA
            if($('input[id=signupform-acepto_terminos]:checked').val()){
                
                 $.post('".Url::to(['site/emailexiste'])."', {email: $('#signupform-email').val() }, 
                   function(res_sub) {
                            var res = jQuery.parseJSON(res_sub); 
                            //console.log(res); return false;
                            if(res.res){                      
                                 $('#register-form').submit();
                            }else{
                                $('.field-signupform-email > .help-block').html('<b style=\'color:red\'>¡CORREO YA EXISTE EN VTM!</b>');
                                return false;
                            }
                        
                   }
                 );
            }else{
               $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
               $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');               
            }
            
          
        }else{
           if($('#cliusuario-nombre').val()=='') $('.field-cliusuario-nombre > .help-block').html('Nombre completo NO puede estar vacío.');
           if($('#signupform-email').val()=='') $('.field-signupform-email > .help-block').html('Email NO puede estar vacío.');
           if($('#signupform-password').val()=='') $('.field-signupform-password > .help-block').html('Contraseña NO puede estar vacío.');
           if($('#signupform-repeat_password').val()=='') $('.field-signupform-repeat_password > .help-block').html('Confirmación de contraseña NO puede estar vacío.');
           if(!$('input[id=signupform-acepto_terminos]:checked').val()){ 
            $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
            $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');
            
           }
           
             
        }
    
    });
//******************************************************

             
            $('.btnwp').click(function(e)
            {       
                    var modal = $('#modal-login').modal('show');    
                    var that = $(this);
                    //modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
                    $('.modal-header').remove();                        
                    $('#login-form').attr('action','" . Url::to(['site/login', 'origen' => '4','url'=>$url_home])."');
                    $('#register-form').attr('action','" . Url::to(['site/login', 'origen' => '4','url'=>$url_home]) . "');
            });
        
            $('.btn_login, .btnpreg, .btntel, .btn_denuncia').click(function(e)
            {       
                    var modal = $('#modal-login').modal('show');    
                    var that = $(this);
                    $('.modal-header').remove();            
                    $('.error').addClass('hidden');                     
                    $('#chk_acepterm_denuncia').prop('checked', false);
                    $('#login-form').attr('action','" . Url::to(['site/login', 'origen' => '3']) . "');
                    $('#register-form').attr('action','" . Url::to(['site/login', 'origen' => '3']) . "');
            });
            
            //INGRESAR
            $('.btn_login2').click(function(e){ 
                    var modal = $('#modal-login').modal('show');    
                    var that = $(this);
                    //modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');     
                    $('.modal-header').remove();                   
                    $('#login-form').attr('action','" . Url::to(['site/login', 'origen' => '3']) . "');          
                    $('#register-form').attr('action','" . Url::to(['site/login', 'origen' => '3']) . "');    
            });
            
             $('.btn_menu').click(function(e){ 
               
                    var modal = $('#modal-menu').modal('show');    
                    var that = $(this);                 
                    
                                
                    
                    $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');          
                    $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');    
                 
            });
            
$('#login-form-link').click(function(e) {
        $('#login-form').delay(100).fadeIn(100);
        $('#register-form').fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        
        $('.txt_reg').attr('style','font-weight:normal');
        $('.txt_is').attr('style','font-weight:bold');
        
        e.preventDefault();
    });
    
    $('#register-form-link').click(function(e) {
        $('#register-form').delay(100).fadeIn(100);
        $('#login-form').fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
    
        $('.txt_reg').attr('style','font-weight:bold');
        $('.txt_is').attr('style','font-weight:normal');
        
        
        
        e.preventDefault();
    }); 




 
            
});");
}

if (!Yii::$app->user->isGuest) {
    $this->registerJs(" 
        jQuery(document).ready(function () {
        
        $('.btnpreg').click(function(e){  
        
         if($('#pubmensaje-mensaje').val()!=''){
            $('.btnpreg').attr('disabled', true);
            $('.field-pubmensaje-mensaje').removeClass('has-error');
            $('.field-pubmensaje-mensaje').addClass('has-success');            
            $.post('".Url::to(['site/enviarpreguntavendedor'])."', {id_publicacion: ".$model->id.", mensaje: $('#pubmensaje-mensaje').val() },
               function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                    console.log(res); //return false;
                    if(res.res){
                          $('.btnpreg').attr('disabled', false);
                          alert('¡Pregunta o mensaje enviado con éxito al vendedor!');
                          $('#pubmensaje-mensaje').val('');                             
                          /*$('.buscador').focus();*/        
                          //$('#frm_recargar').submit();
                          window.location.href = '".Url::to(['site/pubdetallepublico','id'=>$model->id])."';
                    }else{
                        if(res.msg)
                            alert(res.msg);
                        else
                            alert('ERROR 104: verificar con soporte del sistema. ');
                    }
            });         
            }else{
                  //alert('ERROR: ¡No ha ingresado ningún mensaje! ');
                  $('#pubmensaje-mensaje').focus();
                  $('.field-pubmensaje-mensaje').removeClass('has-success');
                  $('.field-pubmensaje-mensaje').addClass('has-error');
            }
         
         });
         
         $('.btn_denuncia').click(function(e){
                    e.preventDefault();
                    
                     var modal = $('#modal-vendido').modal('show');    var that = $(this);
                    $('.modal-header').remove();            
                    $('.error').addClass('hidden'); 
                    $('#chk_acepterm_denuncia').prop('checked', false);
                    modal.find('.modal-title').html('¿Desea denunciar ésta publicación?');                                
                    modal.find('.id_pub').html(that.attr('id_pub'));
                    modal.find('.tmm').html(that.attr('tmm'));
                    modal.find('.descripcion').html(that.attr('descripcion'));                                        
                    $('#id_publicacion').val(that.attr('id_pub'));     
                   // $('#delete-confirm').click(function(e) { e.preventDefault();  window.location = url;  });                                
         });
             
         $('.btn_menu').click(function(e){ 
               
                    var modal = $('#modal-menu').modal('show');    
                    var that = $(this);                 
                    
                    $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');          
                    $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'3'])."');    
                 
         });
});");
}
$this->registerJs(" 
jQuery(document).ready(function () {
              
    $('#chk_acepterm_denuncia').change(function() {
         $('.error').addClass('hidden'); 
         $('input[id=chk_acepterm_denuncia]:checked').val('');
         //$('.normal_text2').attr('style','border: 0px');    
    }); 
    
    $('#motivo').change(function() {
             $('.error').addClass('hidden'); 
             $('input[id=chk_acepterm_denuncia]:checked').val('');
    }); 
    
    $('#pubdenuncia-mensaje').change(function() {
       
             $('.error').addClass('hidden'); 
             $('input[id=chk_acepterm_denuncia]:checked').val('');
       
    });
    
   
              
    $('.btn_save_denuncia').click(function(){
    
        
        if($('#motivo').val()!='' && $('#pubdenuncia-mensaje').val()!=''  && $('#chk_acepterm_denuncia').is(':checked')){
                  
            $.post('".Url::to(['site/ajaxadddenuncia'])."', {id_publicacion: ".$model->id.", motivo: $('#motivo').val(), mensaje: $('#pubdenuncia-mensaje').val() },
               function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                   // console.log(res); return false;
                    if(res.res){
                         alert('¡Anuncio Denunciado con éxito!');                        
                         $('#motivo').val(''); $('#pubdenuncia-mensaje').val('');
                         $('#modal-vendido').modal('hide');          
                         $('.error').addClass('hidden'); 
                         $('input[id=chk_acepterm_denuncia]:checked').val('');
                    }else{
                        if(res.msg)
                            alert(res.msg);
                        else
                            alert('ERROR 104: verificar con soporte del sistema. ');
                    }
            });          
            
        }else { 
             
            $('.error').removeClass('hidden'); 
            $('#motivo').focus(); 
        }
    }); 


    $('.btn_galeria').click(function(e){  
        e.preventDefault();                                             
        var modal = $('#modal-fotos').modal('show');    
        var that = $(this);
        modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
        $('#div_image').attr('src',that.attr('url_image'));
                                 
    });
    
   $('.btn_descargar').click(function(e){
        e.preventDefault();  
        if($(this).attr('id_pub')!=''){
          if(confirm('¿Confirma la descarga de las fotos de este anuncio en un archivo comprimido (.ZIP)?')){
            window.open('".Url::to(['site/descargarimagenes','id_publicacion'=> $model->id])."', 'width=800, height=600');
          }
        }else{
            alert('ERROR 105: ¡ID_PUBLICACION VACÍA!');
        } 
   }); 
    
    $('.btn_suscribirme').click(function(e){
        e.preventDefault();          
        if($('#txt_email').val()!=''){
          
           $('#txt_email').val($('#txt_email').val().replace(/ /g,''));
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(!re.test(String($('#txt_email').val()).toLowerCase())){
                alert('ERROR: ¡EMAIL INVÁLIDO! Debes ingresar un email correcto.');
                document.getElementById('txt_email').focus();
                return false;
           }
           
           
           if($('input[name=check_suscrib]:checked').val()==1){
                 //Grabo en BD y envío un email al suscriptor
                 $.post('".Url::to(['site/addsuscriptor'])."', {email: $('#txt_email').val() }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==1)  {                    
                               alert('¡Felicitaciones! Tu email ha quedado inscrito en nuestro NewsLetter.');
                            }else {                                  
                                   alert('Información: ¡El email ingresado YA está inscrito en nuestro NewsLetter!');
                            }                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
           }else{
             alert('ERROR: Debes aceptar los términos y condiciones para aceptación de tratamiento de datos y envío de información.');
             return false;
           }
        
        }else{
           alert('¡Debes ingresar un email!');
        }
   }); 

});

$('.form-check-input').change(function(e){
 e.preventDefault();     
 if($(this).attr('id') != 'check_suscrib')
   $('#frm').submit();
});

");

$interesado=null;
if (!Yii::$app->user->isGuest) {
    $interesado = \common\models\CliUsuario::find()->where('id_usuario='.Yii::$app->user->identity->id)->one();
    $si_mensaje = \common\models\PubMensaje::find()->where('id_creador='.Yii::$app->user->identity->id.' AND id_publicacion='.$model->id)->one();

}

$cliuser = \common\models\CliUsuario::find()->where('id_usuario='.$model->id_usuario)->one();




?>
<style>
    .box {
        position: relative;
        background: #fff;
        box-shadow: none;
    }
    .panel-body{
        border: 0px;
    }
    .sub1{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 28px;
        line-height: 34px;
        letter-spacing: 0.89297px;

        color: #F33B42;
    }

    .sub2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 13px;
        line-height: 16px;
        letter-spacing: 0.414593px;
        text-decoration-line: line-through;
        color: #A0A0A0;
    }
    .sub3{
        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 17px;
        line-height: 21px;
        letter-spacing: 0.54216px;
        color: #474747;
    }

    .sub4{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 275;
        font-size: 16px;


        align-items: center;
        letter-spacing: 0.031794px;

        color: #A0A0A0;
    }
    .sub5{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 20px;
        align-items: center;
        letter-spacing: 0.031794px;
        border: 1px solid #fff;
        color: #474747;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border: 1px solid #fff;
    }
    .sub6{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        /* identical to box height */

        display: flex;
        align-items: center;
        letter-spacing: 0.031794px;

        color: #474747;
    }
    .iconos{
        width: 37px;display: inline-block;text-rendering: auto;-webkit-font-smoothing: antialiased;margin-top: -22px;
    }

    .btnpreg{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        text-align: center;
        letter-spacing: 0.54216px;
        color: #FFFFFF;
        background: #F33B42;border-radius: 25px;
        width: 100%;
        padding: 10px;
    }

    .btndisabled{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        text-align: center;
        letter-spacing: 0.54216px;
        color: gray;
        border: 1px solid gray;
        width: 100%;
        padding: 10px;
        box-sizing: border-box;
        border-radius: 25px;
    }

    .btntel{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        text-align: center;
        letter-spacing: 0.54216px;
        color: #F33B42;
        border: 1px solid #F33B42;
        width: 100%;
        padding: 10px;
        box-sizing: border-box;
        border-radius: 25px;
    }

    .btnwp{
         font-family: Montserrat;
         font-style: normal;
         font-weight: 600;
         font-size: 17px;
         line-height: 21px;
         text-align: center;
         letter-spacing: 0.54216px;
         color: #64B161;
         border: 1px solid #64B161;
         width: 100%;
         padding: 10px;
         box-sizing: border-box;
         border-radius: 25px;
    }

    #padre {
        position: relative;
        background-color: #9e9e9e05;
        height: 270px;
    }

    .btnprom2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 17px;
        line-height: 21px;
        border: 1px solid #F33B42;
        box-sizing: border-box;
        border-radius: 25px;

        text-align: center;
        letter-spacing: 0.54216px;

        color: #F33B42;
        background: #F9F7F7;
        border-radius: 25px;
        width: 50%;
        padding: 10px;

    }


    .ove {
        min-height: 393px !important;
        text-align: center !important;
    }


  /*
    .ove{
        overflow-y: auto;
        height: 300px;
    }
*/
    .ove2 {
        min-height: 177px !important;
        text-align: center !important;
    }

    .ove2 > .carousel-control{
        line-height: 181px;
    }




    .detalle1{

        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        font-size: 24px;
        line-height: 29px;
        display: flex;
        align-items: center;
        letter-spacing: 0.047691px;
        color: #353435;
        margin-left: 10px;
    }

    .detalle2{
        font-family: Montserrat;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 20px;
        letter-spacing: 0.031794px;
        color: #353435;
        float: left;
        margin-left: 10px;
        text-align: justify;
    }


    .padre0 {
        position: relative;
       /* background-color: green;*/
    }
    .reco_cuadro0 {
        position: absolute;
        color: #ffffff;
        top: 6px;
        left: 0;
        right: 0;
        width: fit-content;
        height: 25px;
    }
    .reco_texto0 {
        margin-left: 176px    ;
        horiz-align: center;
        margin-top: 80px;
        margin-right: 4px;
        font-family: SourceSansPro;
        font-size: 44px;
        line-height: 52px;
        display: flex;
        align-items: center;
        text-align: center;

        color: #FFFFFF;
    }

    .reco_texto1 {
        margin-left: 180px    ;
        horiz-align: center;
        margin-top: 120px;
        margin-right: 4px;
        font-family: SourceSansPro;
        font-size: 18px;
        line-height: 52px;
        display: flex;
        align-items: center;
        text-align: center;
        color: #FFFFFF;
    }

    .reco_texto01 {
        margin-left: 176px    ;
        horiz-align: center;
        margin-top: 80px;
        margin-right: 4px;
        font-family: SourceSansPro;
        font-size: 44px;
        line-height: 52px;
        display: flex;
        align-items: center;
        text-align: center;

        color: #F33B42;
    }
    .reco_texto001 {
        margin-left: 160px    ;
        horiz-align: center;
        margin-top: 120px;
        margin-right: 4px;
        font-family: SourceSansPro;
        font-size: 18px;
        line-height: 52px;
        display: flex;
        align-items: center;
        text-align: center;

        color: #F33B42;
    }
    .reco_cuadro01 {
        position: absolute;
        color: #F33B42;
        top: 6px;
        left: 0;
        right: 0;
        width: fit-content;
        height: 25px;
    }

    .reco_texto02 {
        margin-left: 50px    ;

        margin-top: 28px;
        margin-right: 4px;

        display: flex;
        align-items: center;
        text-align: center;

        color: #F33B42;
        font-family: Nunito;
        font-style: normal;
        font-weight: bold;
        font-size: 35px;
        line-height: 35px;
        text-align: center;

        color: #2E3A59;
    }
    .reco_cuadro02 {
        position: absolute;
        color: #F33B42;
        top: 6px;
        left: 0;
        right: 0;
        width: fit-content;
        height: 25px;
    }

    .carousel-control {
        line-height:384px;
        font-size:54px !important;
        width:80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }

 .control-label{
     font-family: Source Sans Pro;
     font-size: 12px;
     line-height: 14px;

     align-items: center;
     letter-spacing: 0.0238455px;

     color: #F33B42;
 }

 .item{
     text-align: center !important;
 }

    .carousel {
        margin-bottom: 0px !important;
    }

    .carousel-inner{
        display:flex;justify-content:center;
    }
</style>


<form id="frm_recargar" name="frm_recargar" action="<?= 'https://vtmclientes.digitalventures.com.co/site/pubdetallepublico?id='.$model->id ?>" method="get">

</form>
<div class="panel- " style=" padding :10px;color: inherit;margin-top: 10px;border: 0px solid #fff ">
    <div class=" row " style="width: 100%;margin: 10px;position: relative;">
        <div class="col-lg-7 col-md-7 col-sm-12">
            <div class="row">
                <div class="col-lg-2 col-sm-12 col-md-12" style=";height: 424px;background: #fff;padding: 10px;padding-bottom: 30px; color: inherit;overflow-y: auto;max-width: 424px;">
                    <?php
                        foreach ($imagesPeke as $img){
                            echo $img.'<br>';
                        }
                    ?>
                </div>
                <div class="col-lg-10 col-sm-12 col-md-12 text-center" style="background: #fff;padding: 5px 10px 0px 10px;padding-bottom: 30px; color: inherit;height: 424px; ">
                    <div id="padre" class="ibox-content no-padding border-left-right" style="height: 394px !important;height: 394px !important;border: 0px;background: #ffffff;">
                        <?= yii\bootstrap\Carousel::widget(['items'=>$images, 'options'=>['class'=>'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 ">
                    <p class="detalle1" style="margin-top: 30px;margin-left: -2px;">
                        Detalles
                    <hr style="border: 1px solid #FF596A;width: 110px; margin-top: 1px;margin-bottom: 20px" class="pull-left">
                    </p>
                    <p class="sub3 text-left " style="clear: both"><?= $model->tipo->nombre.' / '.$model->modelo->marca->nombre.' / '.$model->modelo->nombre ?></p>
                    <p class="detalle2" style="margin-left: -2px;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.031794px;color: #353435;">
                        <?= $model->descripcion ?>
                    </p>
                <?php
                //Preguntar si está logueado
                if(isset($interesado) && $interesado!='' && isset($si_mensaje) && $si_mensaje!=''){

                    ?>
                        <div class="col-lg-12 col-sm-6 col-md-4 pdre0" style="background: #ff000033; padding:21px 20px 20px 20px;display: flex;justify-content: center">
                            Ya enviaste un mensaje a esta publicación, debes ir Mis Mensajes para continuarla...
                            <a href="<?= Url::to(['site/mismensajes']) ?>" type="button" class="btn btn-danger btnpreg" style="">
                                Ir a mis Mensajes
                            </a>
                        </div>
                    <?php

                }else{
                ?>
                    <div class="col-lg-12 col-sm-6 col-md-4   padre0" style="background: #fff; padding:21px 20px 20px 20px">
                        <p id="pregunta" class="detalle1 " style="margin-top: 5px;">
                            Pregunta al vendedor
                            <hr style="border: 1px solid #FF596A;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class="pull-left">
                              <?php $form = ActiveForm::begin();
                              if(isset($interesado) && $interesado!=''){
                              ?>
                                <div class="row" style="clear: both">
                                    <div class="col-lg-4 col-sm-12 col-md-4">
                                        <?= $form->field($modPreg, 'id_creador')->textInput(['readonly' => true, 'value'=>$interesado->nombre])->label('Nombre:') ?>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-4">
                                        <?= $form->field($modPreg, 'id_creador')->textInput(['readonly' => true, 'value'=>$interesado->telefonos])->label('Teléfonos:') ?>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 col-md-4">
                                        <?= $form->field($modPreg, 'id_creador')->textInput(['readonly' => true, 'value'=>$interesado->email])->label('Email:') ?>
                                    </div>

                                </div>
                                <div class="row">
                                      <div class="col-lg-12 col-sm-12 col-md-12">
                                          <?= $form->field($modPreg, 'mensaje')->textarea(['maxlength' => true]) ?>
                                      </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12 text-sm" style="font-size: 12px">
                                        Al preguntar aceptas los <a style="color: #e46a76" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a>
                                         y las <a style="color: #e46a76" target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>">Políticas de Privacidad</a>
                                        de VENDETUMOTO.CO
                                    </div>
                                </div>
                                <p style="text-align: center; margin-top: 10px;clear: both" >
                                        <button type="button" class="btn btn-danger btnpreg" style="">
                                            Enviar Mensaje
                                        </button>
                                  </p>
                                <?php }else{
                                  echo "<p style='clear: both'><em style='color: #e46a76;clear: both'>- Para enviar un mensaje al vendedor, debes primero registrarte -</em></p>";
                                  echo '<p style="text-align: center; margin-top: 10px;clear: both" >
                                              <button type="button" class="btn btn-default btndisabled" title="Debes primero registrarte">
                                                  <em>Enviar Mensaje</em>
                                              </button>
                                          </p>';
                              }
                              ?>

                                <?php ActiveForm::end(); ?>

                        </p>
                    </div>
        <?php }
                ?>
                </div>
            </div>

            <hr class="hr-separador">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= yii\bootstrap\Carousel::widget(['items'=>$imagesADS, 'options'=>['class'=>'ove2']]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12" style="">
            <?php  //SI usuario logueado
                if(!Yii::$app->user->isGuest){
                    $exist_fav = \common\models\PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                    //Si No es Favorito: Pinta Corazón Blanco
                    if($exist_fav==0){
                        ?>
                        <div id="fav_<?= $model->id ?>" style="z-index: 1500;right: -13px" class="favorito btn_favorito" id_pub="<?= $model->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                            <div class="vertical-timeline-icon navy-bg2  ">
                                <i class="fa fa-heart-o"></i>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div id="fav_<?= $model->id ?>" style="z-index: 1500;right: -13px" class="favorito btn_favorito" id_pub="<?= $model->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                            <div class="vertical-timeline-icon navy-bg danger ">
                                <i class="fa fa-heart"></i>
                            </div>
                        </div>
                    <?php }
                }else{?>
                    <div id="fav_<?= $model->id ?>" style="z-index: 1500;right: -13px" class="favorito btn_favorito" logueado="0" id_pub="<?= $model->id ?>" id_usuario="<?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '' ?>">
                        <div class="vertical-timeline-icon navy-bg2  ">
                            <i class="fa fa-heart-o"></i>
                        </div>
                    </div>

                <?php }

                ?>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 text-left" >
                    <div style=" color: inherit;overflow-y: auto;  background: #fff; padding:21px 20px 20px 20px ">
                        <p class="sub1">Ahora: $<?= ($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))),0,',','.') : number_format($model->precio,0,',','.') ?></p>
                        <p class="sub2"><?= ($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '' ?></p>
                        <p class="sub3"><?=  $model->modelo->marca->nombre.'  '.$model->modelo->nombre ?></p>
                        <table class="table" >
                            <tr>
                                <th class="sub4">Año</th><th class="sub4">Recorrido</th><th class="sub4">Cilindraje</th>
                            </tr>
                            <tr>
                                <td class="sub5" width="33%"><?= $model->ano ?></td>
                                <td width="33%" class="sub5"><?= number_format($model->recorrido,0,',','.').' '. $model->_unid_recorrido[$model->unid_recorrido] ?></td>
                                <td  width="33%" class="sub5"><?= number_format($model->cilindraje,0,',','.') ?>cc</td>
                            </tr>
                        </table>
                        <hr style="background: #DEDEDE;transform: matrix(1, 0, 0, -1, 0, 0);">
                        <p style="text-align: center; margin-top: 10px" >

                            <a href="#pregunta" class="btn btn-danger btnpreg" style="">
                                Preguntar
                            </a>
                        </p>
                        <hr style="background: #DEDEDE;transform: matrix(1, 0, 0, -1, 0, 0);">
                        <p style="text-align: center; margin-top: 10px" >
                            <button target="_blank" class="btn btn-default btntel" >
                                <?php

                                    //\common\components\Utils::dump($cliuser->attributes);
                                    if(isset($cliuser->whatssapp) && $cliuser->whatssapp!=''){

                                        if (!Yii::$app->user->isGuest) {
                                            echo '(+57) '.$cliuser->whatssapp;
                                        }else{
                                            echo '(+57) '.substr($cliuser->whatssapp,0,strlen($cliuser->whatssapp)-7).'*******';
                                        }


                                    }elseif(isset($cliuser->telefonos) && $cliuser->telefonos!=''){
                                        echo substr($cliuser->telefonos,0,strlen($cliuser->telefonos)-5).'*******';
                                    }else echo '<em style="color: #e46a76">- sin números-</em>';

                                ?>
                            </button>
                        </p>
                        <p style="text-align: center; margin-top: 10px" >
                            <?php
                                $url_home = Url::to(['site/login']);
                                if (!Yii::$app->user->isGuest) {
                                    $url_home = 'https://api.whatsapp.com/send?phone=573164138276&text=Quiero más información de la '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' publicada en *Vende Tu Moto* &'.$url_self;
                                    ?>
                                    <a target="_blank" href="<?= $url_home ?>"  class="btn btn-default btnwp" style="">
                                        Escribe vía WhatsApp
                                    </a>
                            <?php
                                }else{
                            ?>
                                <button target="_blank"  class="btn btn-default btnwp" style="">
                                    Escribe vía WhatsApp
                                </button>
                            <?php } ?>
                        </p>
                        <hr style="background: #DEDEDE;transform: matrix(1, 0, 0, -1, 0, 0);">
                        <div class="text-left">
                            <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_ES</script>
                            <p class="sub6 text-left" style="width: 100%; ">Comparte este anuncio:</p>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $url_self ?>&t=<?= 'Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí: ' ?> "><i class="fa fa-facebook-square" style="font-size: 40px;color: #425EAC"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?= urlencode($url_self) ?>" style="margin-left: 10px">
                                <i class="fa fa-linkedin-square" style="font-size: 40px"></i>
                            </a>
                            <a target="_blank" href="https://api.whatsapp.com/send?text=<?= 'Se vende '.$model->modelo->marca->nombre.'  '.$model->modelo->nombre.' '.$model->ano.' en '.$model->ciudad->name.'. *Vende Tu Moto*  %20'.$url_self ?>" style="margin-left: 10px"><img class="iconos" src="<?= Yii::$app->request->baseUrl . '/img/wp.png' ?>"></img></a>
                            <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= 'Se vende '.\common\components\Utils::limpiarCadena2($model->modelo->marca->nombre.' '.$model->modelo->nombre).' '.$model->ano.' en '.$model->ciudad->name.'. Ficha técnica aquí: ' ?>&url=<?= $url_self ?>" style="margin-left: 10px"><i class="fa fa-twitter-square" style="font-size: 40px;color: #1DADEB"></i></a>
                        </div>
                        <hr style="background: #DEDEDE;transform: matrix(1, 0, 0, -1, 0, 0);">

                        <p class="sub3">
                            Información del vendedor:
                        </p>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%"> <p class="sub4">Nombre</p></td><td style="width: 50%"> <p class="sub4">Ubicación del vehículo:</p></td>
                            </tr>
                            <tr>
                                <td>  <?= $cliuser->nombre ?>  </td><td>  <?= $model->ciudad->state->name.', '.$model->ciudad->name ?> </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 text-left"  >
                    <hr>
                    <div style=" color: inherit;overflow-y: auto;  background: #fff; padding:21px 20px 20px 20px ">
                         <div class="row">
                            <p class="detalle1" style="margin-top: 0px;">
                                Consejos de seguridad
                            <hr style="border: 1px solid #FF596A;width: 110px;margin-left: 10px;margin-top: 1px;margin-bottom: 20px" class="pull-left">
                            </p>
                         </div>
                         <div class="row">
                            <p class="detalle33" style="margin-left: 12px;color: #474747;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 18px;letter-spacing: 0.875px; ">	•	Muestra el vehículo (moto, cuatrimoto, UTV, lancha, accesorio) en lugares seguros como un centro comercial, lugar de residencia si cuenta con seguridad privada o en una compraventa con el acompañamiento de un amigo o familiar.
                                <br><br>•	Desconfía de negocios rápidos por motivos de mudanza o por alivios económicos.
                                <br><br>•	Siempre verifica el estado del vehículo (moto, cuatrimoto, UTV, lancha, accesorio) por medio de un peritaje o la documentación por medio del RUNT, SIJIN, DIJIN, etc. Entidades que te garanticen que todo esté en orden.
                                <br><br>•	Vende Tu Moto NO es propietario de ningún vehículo de la plataforma.
                                <br><br>•	Vende Tu Moto NUNCA te pedirá datos sensibles como contraseñas, número de tarjetas de crédito, códigos de verificación por WhatsApp, etc.
                            </p>
                         </div>
                    </div>
                    <div class=" pull-right" style="padding-top: 10px">
                        Anuncio <b>#<?= $model->id ?></b> &nbsp;&nbsp;
                        <button class="btn btn-default btn_denuncia" style="color: #e46a76;border: 1px solid #e46a76">Denunciar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>


<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 ">
        <center class="detalle1" style="margin-top: 30px;margin-left: -2px;width: 100%;display: block;text-align: center">
            Vehículos relacionados

        </center>
        <hr style="border: 1px solid #FF596A;width: 110px; margin-top: 1px;margin-bottom: 20px;clear: both" class=" text-cente r">
        <center class="sub3 text-left " style="clear: both">
            <?php


            if(!$anuncios_rel){
                echo '<em style="color: #e46a76">- Sin Anuncios -</em><hr>';
            }else{
                echo '<div class="row">';
                foreach ($anuncios_rel as $item2)
                {
                    $item = \common\models\PubPublicacion::find()->where('id='.$item2['id'])->one();
                    $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $item->id)->orderBy('img_order asc')->limit(1)->one();
                    if($foto_primera) $img_url = '<a target="_blank" style="display: flex;  justify-content: center;" href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">
                    <img alt="image" style="display: block;max-width: 100%;width: auto;height: auto;max-height: 121px;" class="img-fluid img-responsive" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                    else  $img_url = '<a target="_blank" style="display: flex;  justify-content: center;"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'">
                       <img alt="image" style=" display: block; max-width: 100%;  width: auto; height: auto;" class="img-fluid img-responsive" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'">
                       </a>';
                    ?>
                    <div class="col-lg-2 col-md-2 col-sm-6" >
                        <div class="ibox product-box">
                            <div>
                                <div id="padre" class="ibox-content no-padding border-left-right" style="min-height: 80%;height: auto">
                                    <?= $img_url ?>
                                    <div class="reco_cuadro">
                                        <span class="reco_texto" style="font-size: 11px;font-weight: normal"><?= number_format($item->recorrido,0,',','.').' '.($item->_unid_recorrido[$item->unid_recorrido]) ?></span>
                                    </div>
                                    <?php if($item->descuento>0){ ?>
                                        <div class="desc_cuadro" style=" cursor: pointer;" title="Tiene <?= $item->descuento ?>% de descuento">
                                            <span class="desc_texto" style="font-size: 11px;font-weight: normal">- <?= $item->descuento ?>%</span>
                                        </div>
                                    <?php }
                                    //SI usuario logueado


                                    ?>
                                </div>
                                <div class="ibox-content profile-content" style="min-height:68px !important;background: transparent !important;" title="<?= $item->modelo->marca->nombre.' '.$item->modelo->nombre ?>">
                                    <p class="font-12" style="font-size: 14px;font-weight: bold;">
                                        <?= (strlen($item->modelo->marca->nombre.' '.$item->modelo->nombre)>18) ? substr($item->modelo->marca->nombre.' '.$item->modelo->nombre,0,18).'...': substr($item->modelo->marca->nombre.' '.$item->modelo->nombre,0,18) ?>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php


                }//endfor
                echo '</div>';
            }
            ?>
        </center>
    </div>
</div>

<hr class="hr-separador2">
<style>
    .txt_tit{
        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;

        color: #FF596A
    }
</style>

<?php


$form2 = ActiveForm::begin([
    'id' => 'form2',

]);

$model2 = new \common\models\PubDenuncia();

\yii\bootstrap\Modal::begin([
    'header' => false,
    'id'     => 'modal-vendido',
    'size' => 'modal-lg',
    'clientOptions' => [
    'keyboard' => false],
    'footer' => null,
]); ?>

<?php echo '<div id="titulo" style="font-size:13px;margin-top: 0px; padding: 3px;text-align:left;">
     
<h3 style="margin-top: 30px;margin-bottom: 30px;font-family: Source Sans Pro;font-size: 24px;line-height: 34px;text-align: center;letter-spacing: 1.70385px;color: #E9364B;font-weight: revert;">
   Denuncia este anuncio
</h3>
 
<div class="row" style="margin-left: 30px; margin-right: 30px;padding-bottom: 40px">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="hidden error" style="color: #f9f9f9;font-weight: bold;background: #f33b42;width: 100%;padding: 10px">
        ERROR: Debe seleccionar un motivo, escribir una explicación y aceptar políticas de privacidad.
     </div>
     <span class="txt_tit" style=";padding-bottom: 5px">Número del anuncio </span> 
     <input type="text" readonly value="#'. $model->id .'" class="form-control txt_nro_denuncia" style="width: 100%;">   
     <br>
     <span class="txt_tit" style=";padding-bottom: 5px">Motivo de la denuncia </span>';


     echo '<select id="motivo" class="form-control " name="motivo" style="height: 30px">
            <option value="">-- seleccione --</option>
            <option value="1" data-select2-id="1">Los datos del vehículo no coinciden con lo publicado</option>
            <option value="2" data-select2-id="2">El vehículo ya no esta disponible</option>
            <option value="3" data-select2-id="3">Al parecer es un engaño o intento de fraude</option>
            <option value="4" data-select2-id="4">El vendedor me estafó</option>
            <option value="4" data-select2-id="5">El vehículo publicado es mío</option>
            <option value="4" data-select2-id="6">Otros</option>
          </select>
 
      <div class="row2" style="visibility: visible;margin-top: 10px">
         ';
        echo $form2->field($model2, 'mensaje')->textarea(['rows' => 4])->label('<span class="txt_tit" style="font-weight: normal;">Cuéntanos más sobre tu denuncia</span>');
         echo '
      </div>
      <center class="row" style="padding-bottom: 20px">
          <table   >
              <tr>
                 <td><input type="checkbox" id="chk_acepterm_denuncia"   value="1" aria-required="true"></td>
                 <td style="">
                    <span style="float: left;margin-top: 4px;margin-left: 10px;">
                     He leído y acepto la <a target="_blank" href="'.Url::to(['site/politicadeprivacidad']).'" class="rosado_aref" style="">política de privacidad</a>.
                    </span> 
                 </td>
                </tr>
        </table>       
          
      </center>
      <center class="row">
           <button class="btn btn-default cerrar" style="color: #e46a76;border: 1px solid #e46a76;width:165px;border-radius:25px; font-size:19px" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          <button type="button"   class=" btn btn_save_denuncia" name="login-button" style="width:165px;border-radius:25px;color:#fff;background:#F33B42;font-size:19px">Enviar</button>
      </center>
     </div> 

    </div>     
     
</div>';


         ?>

<?php \yii\bootstrap\Modal::end();

ActiveForm::end();

?>

<style>

    .btn-facebook {
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        padding: 8px;
        color: #969696;font-size: 20px;
    }
    .btn-google {
        /* color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        color: #969696;
        font-size: 20px;
    }

    .btn-login {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px #fff;
        box-shadow: 0 1px 1px #fff;
    }
    .panel-heading .box {
        padding: 18px 0 6px;
        text-align: center;
    }

    .panel-heading .line {
        margin-top: 4px;
        margin-bottom: 20px;
        border: 0;
        width: 100%;
        border-top: 1px solid #c5c5c54d;

    }

    .panel-heading .active .line {
        border-top: 3px solid #F33B42;

    }


    .panel-heading .text {
        color: #000;
        font-weight: bold;
    }

    .panel-heading .text2{
        color: #000;
        font-weight: normal;
    }

    .panel-heading .text, .text2:hover,
    .panel-heading .text, .text2:focus {
        text-decoration: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid #fff;
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .card-header {
        margin-bottom: 0;
        padding: 1.25rem 1.5rem;
        border-bottom: 1px solid rgba(0, 0, 0, .05);
        background-color: #fff;
    }

    .card-header:first-child {
        border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    }

    .shadow {
        /* box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;*/
    }

    #signupform-acepto_terminos {
        display: block;
    }

    .tambien{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 18px;

        letter-spacing: 0.0278198px;

        color: #969696;
    }

    .titulos_form{
        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;
        font-weight: normal;
        color: #F33B42 !important;
    }
    .help-block{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 14px;
        /* identical to box height */


        text-align: right;
        letter-spacing: 0.0218584px;

        color: #353435 !important;
    }

    .rosado_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #FF596A;
    }

    .negro_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #353435;
    }

    .normal_text{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: 0.0278198px;

        color: #353435;
    }

</style>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<?php

use yii\authclient\widgets\AuthChoice;

\yii\bootstrap\Modal::begin([
    'header' => '<h4 class="text-danger  modal-title">Debes iniciar sesión para realizar la acción</h4>',
    'id'     => 'modal-login',
    'size' => 'modal-lg',
    'clientOptions' => [

        //'backdrop' => 'static',
        'keyboard' => false],
    'footer' => null,
]); ?>
<input type="hidden" id="param" name="param">
<?php

$model = new LoginForm();
$modelSignup = new SignupForm();
$modelCliU = new CliUsuario();
/*
echo $this->render('loginpopup', [
    'model' => $model,'modelSignup' => $modelSignup,'modelCliU' => $modelCliU,
])
*/
?>
<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->


    <div class="panel panel-login card bg-secondary shadow border-0" style="width: 80%;height: auto;
    margin: 0 auto;">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6 box active" id="login-form-link">
                    <a href="#" class="text txt_is">INICIAR SESIÓN</a>
                    <hr class="line">
                </div>
                <div class="col-xs-6 box" id="register-form-link">
                    <a href="#" class="text2 txt_reg" style="">REGISTRATE</a>
                    <hr class="line">
                </div>
            </div>
            <!-- <hr> -->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=>false,'method'=>'post','action'=>\yii\helpers\Url::to(['site/login'])]); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($model, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>

                            <div class="pull-right" style="margin-bottom: 15px"><?= Html::a('<span class="titulos_form">Olvidé mi contraseña</span>', ['site/request-password-reset']) ?></div>

                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#F33B42;font-size:11px', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    </div>




                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>
                    <p class="tambien" style="font-weight: normal">También puedes ingresar con</p>
                    <hr style="width: 70%;text-align: center">
                    <div class="social-button-login">
                        <div class="row">
                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-3 col-sm-offset-3" style="margin: 0.5em;color:#969696">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <?php ActiveForm::end(); ?>

                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(
                        [
                            'id' => 'register-form',
                            'enableClientValidation'=>false,
                            'enableAjaxValidation'=> false,
                            'options' => ['style' => 'display: none;']
                        ]);
                    ?>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'nombre')->label( '<span class="titulos_form">Nombre completo</span>')?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'email')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'telefonos')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de contacto</span>')?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelCliU, 'whatssapp')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de WhatsApp</span>')?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('<span class="titulos_form">Confirmar de contraseña</span>') ?>
                        </div>
                    </div>
                    <p class="tambien" style="margin-top: 10px">&nbsp;</p>
                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>


                    <div class="social-button-login">
                        <p class="tambien" style="margin-top: 10px">También puedes ingresar con</p>
                        <hr style="    margin-top: 0px;    margin-bottom: 9px;    border: 0;    border-top: 1px solid #eeeeee;">
                        <div class="row">

                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <div class="row">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">


                                    <table class="table" style="margin-top: 20px">
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-promocionales" name="SignupForm[promocionales]" value="1" aria-required="true">
                                            </td>
                                            <td class="normal_text" style="border: 0px">
                                                Me gustaría recibir comunicaciones promocionales (Recibirás un e-mail de confirmación)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-acepto_terminos" name="SignupForm[acepto_terminos]" value="1" aria-required="true">
                                            </td>
                                            <td style="border: 0px" class="normal_text2">Declaro que he leído y acepto la nueva <a target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>" class="rosado_aref" style="">Política de privacidad</a> y los
                                                <a class="rosado_aref" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de VENDETUMOTO.CO
                                            </td>
                                        </tr>
                                    </table>


                                    <?= $form->field($modelSignup, 'promocionales')->hiddenInput()->label(false) ?>
                                    <?= $form->field($modelSignup, 'acepto_terminos')->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Registrarme', ['id'=>'btn_reg', 'class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#FF596A;font-size:11px', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <!-- ############################################### -->
                </div>

            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::end();


?>
