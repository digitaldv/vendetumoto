<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Publicar Anuncio';
$this->params['breadcrumbs'][] = $this->title;

$img1 = Yii::$app->request->baseUrl . '/img/tipo-moto-1.jpg';       $img2 = Yii::$app->request->baseUrl . '/img/tipo-moto-2.jpg';
$img3 = Yii::$app->request->baseUrl . '/img/tipo-cuatri-1.jpg';     $img4 = Yii::$app->request->baseUrl . '/img/tipo-cuatri-2.jpg';
$img5 = Yii::$app->request->baseUrl . '/img/tipo-nautico-1.jpg';    $img6 = Yii::$app->request->baseUrl . '/img/tipo-nautico-2.jpg';
$img7 = Yii::$app->request->baseUrl . '/img/tipo-uvt-1.jpg';        $img8 = Yii::$app->request->baseUrl . '/img/tipo-uvt-2.jpg';

 $img_ini1 = Yii::$app->request->baseUrl . '/img/tipo-moto-1.jpg';
 $img_ini2 = Yii::$app->request->baseUrl . '/img/tipo-cuatri-1.jpg';
 $img_ini3 = Yii::$app->request->baseUrl . '/img/tipo-nautico-1.jpg';
 $img_ini4 = Yii::$app->request->baseUrl . '/img/tipo-uvt-1.jpg';

if(isset($tipo) && $tipo != '')
{
     switch ($tipo){
         case 1:   $img_ini1 = Yii::$app->request->baseUrl . '/img/tipo-moto-2.jpg'; break;
         case 2:   $img_ini2 = Yii::$app->request->baseUrl . '/img/tipo-cuatri-2.jpg'; break;
         case 3:   $img_ini3 = Yii::$app->request->baseUrl . '/img/tipo-nautico-2.jpg'; break;
         case 4:   $img_ini4 = Yii::$app->request->baseUrl . '/img/tipo-uvt-2.jpg'; break;
     }
}
//\common\components\Utils::dump($_GET);
if(!$model->isNewRecord){
 $str = '$(".btn_p1").click(function() {
    if($("#id_tipo_seleccionado").val()!=""){
       
      $(location).attr("href","'.Url::to(['site/publicaranuncio2']).'?tipo="+$("#id_tipo_seleccionado").val()+"&id="+'.$model->id.');
    }else{
      alert("Error: ¡Debes seleccionar un tipo de vehículo para continuar!");
    }
});';

}else{
    $str = '$(".btn_p1").click(function() {
                if($("#id_tipo_seleccionado").val()!=""){
                   
                  $(location).attr("href","'.Url::to(['site/publicaranuncio2']).'?tipo="+$("#id_tipo_seleccionado").val());
                }else{
                  alert("Error: ¡Debes seleccionar un tipo de vehículo para continuar!");
                }
            });';
}

$this->registerJs(' 
jQuery(document).ready(function () {


'.$str.'

$(".my-img")
        .mouseover(function() { 
            if($("#id_tipo_seleccionado").val()==1){  if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img4.'"); else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img6.'"); else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img8.'");
            }else  if($("#id_tipo_seleccionado").val()==2){ if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img2.'");   else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img6.'");                   else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img8.'");
            }else  if($("#id_tipo_seleccionado").val()==3){ if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img2.'");   else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img4.'");                   else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img8.'");
            }else  if($("#id_tipo_seleccionado").val()==4){ if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img2.'");   else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img6.'");                   else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img4.'");
            }else{ if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img2.'"); else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img4.'");  else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img6.'");               else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img8.'");            }             
        })
        .mouseout(function()  { 
            if($("#id_tipo_seleccionado").val()==1){ if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img3.'");                else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img5.'");                else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img7.'");            }else
            if($("#id_tipo_seleccionado").val()==2){                if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img1.'");                else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img5.'");                else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img7.'");
            }else            if($("#id_tipo_seleccionado").val()==3){                if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img1.'");                else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img3.'");               
             else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img7.'");            }else            if($("#id_tipo_seleccionado").val()==4){                if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img1.'");                else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img3.'");                else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img5.'");
            }else{
                if($(this).attr("img_tipo")==1)       $(this).attr("src", "'.$img1.'");                else if($(this).attr("img_tipo")==2)  $(this).attr("src", "'.$img3.'");                else if($(this).attr("img_tipo")==3)  $(this).attr("src", "'.$img5.'");                else if($(this).attr("img_tipo")==4)  $(this).attr("src", "'.$img7.'");            }
        })
        .click(function() {
           if($(this).attr("img_tipo")==1){  $("#img2").attr("src", "'.$img3.'");  $("#img3").attr("src", "'.$img5.'");  $("#img4").attr("src", "'.$img7.'");  $("#img1").attr("src", "'.$img2.'");  $("#id_tipo_seleccionado").val(1);
           }else if($(this).attr("img_tipo")==2){ $("#img1").attr("src", "'.$img1.'");    $("#img3").attr("src", "'.$img5.'");  $("#img4").attr("src", "'.$img7.'"); $("#img2").attr("src", "'.$img4.'");            $("#id_tipo_seleccionado").val(2);
           }else if($(this).attr("img_tipo")==3){ $("#img1").attr("src", "'.$img1.'");   $("#img2").attr("src", "'.$img3.'");   $("#img4").attr("src", "'.$img7.'");  $("#img3").attr("src", "'.$img6.'"); $("#id_tipo_seleccionado").val(3);
           }else if($(this).attr("img_tipo")==4){ $("#img1").attr("src", "'.$img1.'");   $("#img2").attr("src", "'.$img3.'"); $("#img3").attr("src", "'.$img5.'"); $("#img4").attr("src", "'.$img8.'"); $("#id_tipo_seleccionado").val(4);
           }
        });

});');
?>




<div class="site-about" >
    <h1 style="color: #f33b42;border: 1px solid #80808021;padding: 10px 0px 10px 10px;background: whitesmoke;">Publicación de Anuncio</h1>



    <div id="smartwizard" class="sw-main sw-theme-arrows">
    <ul class="nav nav-tabs step-anchor responsive-utilities-test">
<?php

    if($model->isNewRecord){
        ?>
        <li class="active"><a ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
        <li><a><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
        <li><a><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
        <li><a ><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
        <?php
    }else{
        ?>
        <li class="active"><a ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
        <li><a href="<?= Url::to(['site/publicaranuncio2','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
        <li><a href="<?= Url::to(['site/publicaranuncio3','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
        <li><a href="<?= Url::to(['site/publicaranuncio4','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>

        <?php
    }
?>
        <li class="pull-right" style="margin-right: 20px;margin-top: 25px"><b>ESTADO PUBLICACIÓN:</b> <?= (isset($model) && $model->estado!='' ? $model->_estado[$model->estado]:$model->_estado[6]) ?></li>
    </ul>
    </div>
    <p>&nbsp;</p><p>&nbsp;</p>

    <?php
    if($model->estado==0 || $model->estado==1 || $model->estado==5 || $model->estado==7)
    { ?>
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class="btn_p1">
                    <center>
                        <img id="img1" img_tipo="1" alt="image" style="cursor: pointer;max-width:250px;" class="my-img img-fluid img-responsive" src="<?= $img_ini1 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Moto</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class="btn_p1">
                    <center>
                        <img  id="img2"  img_tipo="2" alt="image" style="cursor: pointer;max-width:250px;" class="my-img img-fluid img-responsive" src="<?= $img_ini2 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Cuatrimoto</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class="btn_p1">
                    <center>
                        <img id="img3"  img_tipo="3" alt="image" style="cursor: pointer;max-width:250px;" class="my-img img-fluid img-responsive " src="<?= $img_ini3 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Náutico</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class="btn_p1">
                    <center>
                        <img id="img4"  img_tipo="4" alt="image" style="cursor: pointer;max-width:250px;" class="my-img img-fluid img-responsive" src="<?= $img_ini4 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">UTV</h4>
                    </center>

                </div>
            </div>
          <input type="hidden" id="id_tipo_seleccionado" value="<?= (isset($_GET['tipo']) && $_GET['tipo'] != '' ? $_GET['tipo'] : '') ?>">

        </div>
    <?php }else{ ?>
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class=" ">
                    <center>
                        <img id="img1"  alt="image" style="cursor: pointer;max-width:250px;" class=" img-fluid img-responsive" src="<?= $img_ini1 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Moto</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class=" ">
                    <center>
                        <img   alt="image" style="cursor: pointer;max-width:250px;" class=" img-fluid img-responsive" src="<?= $img_ini2 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Cuatrimoto</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class=" ">
                    <center>
                        <img   alt="image" style="cursor: pointer;max-width:250px;" class=" img-fluid img-responsive " src="<?= $img_ini3 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">Náutico</h4>
                    </center>

                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 ">
                <div style=" padding: 10px;" class=" ">
                    <center>
                        <img id=" "   alt="image" style="cursor: pointer;max-width:250px;" class=" img-fluid img-responsive" src="<?= $img_ini4 ?>">
                        <h4 class="text-black text-center" style="font-weight: bold;margin-top: 30px;color: #f33b42">UTV</h4>
                    </center>

                </div>
            </div>


        </div>
        <?php } ?>
    <p>&nbsp;</p><p>&nbsp;</p>
    <!--button type="button" class="btn_p1 btn btn-success btn-lg pull-right" style="margin-bottom: 50px "  >Siguiente >></button-->

</div>



<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-vendido',

]);

?>
 ssss

<?php \yii\bootstrap\Modal::end(); ?>
