<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;


$this->title = 'Publicar Anuncio - Paso 2';
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs(" 
jQuery(document).ready(function () {

 
 
  $('#pubpublicacion-numero_contacto').blur(function(){
    if($('#pubpublicacion-numero_contacto').val().length < 10 ){
            //alert('Error: Debe ser un número de 10 dígitos');
            //$('#pubpublicacion-numero_contacto').val('');
           // return false;
    }
  });
 
 
 
  $('#pubpublicacion-whatsapp').blur(function(){
    if($('#pubpublicacion-whatsapp').val().length < 10 ){
            //alert('Error: Debe ser un número de 10 dígitos');
            //$('#pubpublicacion-whatsapp').val('');
            //return false;
    }
  });
 
 
   // Currency Separator
    var commaCounter = 10;

    function numberSeparator(Number) {
        Number += '';

        for (var i = 0; i < commaCounter; i++) {
            Number = Number.replace('.', '');
        }

        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(y)) {
            y = y.replace(rgx, '$1' + '.' + '$2');
        }
        commaCounter++;
        return y + z;
    }

    // Set Currency Separator to input fields
    $(document).on('keypress , paste', '.number-separator', function (e) {
        if (/^-?\d*[,.]?(\d{0,3},)*(\d{3},)?\d{0,3}$/.test(e.key)) {
            $('.number-separator').on('input', function () {
                e.target.value = numberSeparator(e.target.value);
            });
        } else {
            e.preventDefault();
            return false;
        }
    });
    
   
  $('#pubpublicacion-placa').blur(function(){
      
      //console.log($('#pubpublicacion-placa'));
      console.log($('#pubpublicacion-placa').val().length+'-');
      
      if($('#pubpublicacion-placa').val().length > 0 && $('#pubpublicacion-placa').val().length < 5)
      {
            alert('Error: Las Placas deben ser: 3 letras, 2 números y/o 1 Letra. \\n\\nEjemplos: (ABC12) o (ABC57P) ');
            $('#pubpublicacion-placa').val('');
            $('#pubpublicacion-placa').focus();
      }else
       if($('#pubpublicacion-placa').val().length == 5){
            let l1 = $('#pubpublicacion-placa').val().charAt(0);
            let l2 = $('#pubpublicacion-placa').val().charAt(1);
            let l3 = $('#pubpublicacion-placa').val().charAt(2);
            let n1 = $('#pubpublicacion-placa').val().charAt(3);
            let n2 = $('#pubpublicacion-placa').val().charAt(4);
           
            const ll1 = /^[a-zA-Z]$/i.test(l1)
            const ll2 = /^[a-zA-Z]$/i.test(l2)
            const ll3 = /^[a-zA-Z]$/i.test(l3)
            const nn1 = /^[0-9]$/i.test(n1)
            const nn2 = /^[0-9]$/i.test(n2) 
          
            if ( (ll1 && ll2 && ll3) && (n1 && n2)) {
                return false;
            }else{
                alert('Error: Las Placas deben ser: 3 letras, 2 números  y/o 1 Letra. \\n\\nEjemplos: (ABC12)  o (ABC57P) ');
                $('#pubpublicacion-placa').val('');
                $('#pubpublicacion-placa').focus();
            }
       }else
       if($('#pubpublicacion-placa').val().length == 6)
       {
            console.log('entro a == 6 ');
            let l1 = $('#pubpublicacion-placa').val().charAt(0); //letra 1
            let l2 = $('#pubpublicacion-placa').val().charAt(1); //letra 2
            let l3 = $('#pubpublicacion-placa').val().charAt(2); //letra 3
            let n1 = $('#pubpublicacion-placa').val().charAt(3); //num 1
            let n2 = $('#pubpublicacion-placa').val().charAt(4); //num 2
            let l4 = $('#pubpublicacion-placa').val().charAt(5); //letra 4 
            
            
            console.log(l1,l2,l3,n1,n2,l4);
           
            const ll1 = /^[a-zA-Z]$/i.test(l1);
            const ll2 = /^[a-zA-Z]$/i.test(l2);
            const ll3 = /^[a-zA-Z]$/i.test(l3);
            const nn1 = /^[0-9]$/i.test(n1);
            const nn2 = /^[0-9]$/i.test(n2)
            const ll4 = /^[a-zA-Z]$/i.test(l4); //si ultimo existe, asignele si cumple la condición de LETRA 
            
          
            //console.log('isLetter1',isLetter);            console.log('isNumber2',isNumber);
            console.log(ll1+', '+ll2+', '+ll3+', '+nn1+', '+nn2+', '+ll4);
                
            if ( ll1 && ll2 && ll3 && nn1 && nn2 && ll4) {
                return false;
            }else{
                alert('Error: Las Placas deben ser: 3 letras, 2 números  y/o 1 Letra. \\n\\nEjemplos: (ABC12)  o (ABC57P) ');
                $('#pubpublicacion-placa').val('');
                $('#pubpublicacion-placa').focus();
            }
       } 
  
  });

  $('#pubpublicacion-id_tipo').prop('disabled', true);
  
  
});");

// NUEVA PUBLICACIÓN  o Ya Existe pero su Estado= 6: Borrador ****************
if($model->isNewRecord || (!$model->isNewRecord && $model->estado==6))
{
    $this->registerJs("
     
     $('#btn_guardar').click(function(){
         //alert($('#pubpublicacion-placa').val());
         
         //Si Condición es 2: USADA : PLACA OBLIGATORIA
         if($('#pubpublicacion-condicion').val()==2)
         {         
             // SI MOTO:
             if($('#pubpublicacion-id_tipo').val()==1)
             {
                   //Si placa = Vacio
                   if($('#pubpublicacion-placa').val()==''){
                       alert('Error: Placa es obligatorio');
                       $('#pubpublicacion-placa').focus();
                   }else{
                         // Sinó, es por que está lleno y debe validarse en BD
                         // Pregunto si ya existe. si Si existe: error ya existe, sinó existe: Guardar normal
                         $.post('".Url::to(['site/placaexiste'])."', {placa: $('#pubpublicacion-placa').val(), id_pub: ".((isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : 'null')." }, 
                           function(res_sub) {
                                    var res = jQuery.parseJSON(res_sub); 
                                    console.log(res); //return false;
                                    if(!res.res){  //FALSE: NO EXISTE
                                        console.log('No existe');                                        
                                        $('#w0').submit();              
                                    }else{
                                        alert('Error: ¡Esa placa ('+$('#pubpublicacion-placa').val()+') ya existe para otra publicación! Verifica e intenta de nuevo.');
                                        return false;
                                    }                
                         });                     
                   }               
             }              
             
         }else{
             $('#w0').submit();    
         }
        
      });
  
 
     $('#pubpublicacion-condicion').change(function()
     {     
           //Si (NUEVA la PUBLICACIÓN) (se inhabilita Recorrido=0 y Placa)  
           if($('#pubpublicacion-condicion').val()==1){
               
               //RECORRIDO
               $('#pubpublicacion-recorrido').val(0);
               $('#pubpublicacion-recorrido').prop('disabled', true);
               
               $('#pubpublicacion-recorrido-disp').val(0);
               $('#pubpublicacion-recorrido-disp').prop('disabled', true);
               $('#pubpublicacion-recorrido').prop('title', 'Si la condición es Nueva, su recorrido es 0 (Kilómetros, millas o horas)');
               
               //PLACA se inhabilita
               $('#pubpublicacion-placa').val('');
               $('#pubpublicacion-placa').prop('disabled', true);      
             
               
          }else //SINÓ: ES USADA 
            {
               
                     //PLACA: Deshabilitada
                     $('#pubpublicacion-placa').val('');
                     $('#pubpublicacion-placa').prop('disabled', false);
           
                     $('#pubpublicacion-recorrido').val('');
                     $('#pubpublicacion-recorrido').prop('disabled', false);
                     $('#pubpublicacion-recorrido-disp').val('');
                     $('#pubpublicacion-recorrido-disp').prop('disabled', false);
                      
              
           }
     
 }); ");

}else
//EDITAR
{
    $this->registerJs("


    $('#btn_guardar').click(function()
    {
         $('#w0').submit();    
 
    });


     $('#pubpublicacion-condicion').change(function()
     {     
           //Si (NUEVA la PUBLICACIÓN) (se inhabilita Recorrido=0 y Placa)  
           if($('#pubpublicacion-condicion').val()==1){
               
               //RECORRIDO
               $('#pubpublicacion-recorrido').val(0);
               $('#pubpublicacion-recorrido').prop('disabled', true);
               
               $('#pubpublicacion-recorrido-disp').val(0);
               $('#pubpublicacion-recorrido-disp').prop('disabled', true);
               $('#pubpublicacion-recorrido').prop('title', 'Si la condición es Nueva, su recorrido es 0 (Kilómetros, millas o horas)');
               
               //PLACA se inhabilita
               $('#pubpublicacion-placa').val('');
               $('#pubpublicacion-placa').prop('disabled', true);      
             
               
          }else //SINÓ: ES USADA 
            {
               
                     //PLACA: Deshabilitada
                     $('#pubpublicacion-placa').val('');
                     $('#pubpublicacion-placa').prop('disabled', false);
           
                     $('#pubpublicacion-recorrido').val('');
                     $('#pubpublicacion-recorrido').prop('disabled', false);
                     $('#pubpublicacion-recorrido-disp').val('');
                     $('#pubpublicacion-recorrido-disp').prop('disabled', false);
                      
              
           }

     });
");

}


?>
<style>

    .select2{
        width: 100% !important;
    }
    .select2-container{
        width: 100% !important;
    }
    .select2-container--krajee{
        width: 100% !important;
    }
    .select2-container{
        width: 100% !important;
    }
    .help-block{
        font-size: 11px;
    }
</style>
<div class="site" >
    <h1 style="color: #f33b42;border: 1px solid #80808021;padding: 10px 0px 10px 10px;background: whitesmoke;margin-bottom: -14px;">Publicación de Anuncio</h1>

    <div id="smartwizard" class="sw-main sw-theme-arrows">
    <ul class="nav nav-tabs step-anchor responsive-utilities-test">

        <?php
        use kartik\money\MaskMoney;


        if($model->isNewRecord){
            if(isset($_GET['tipo']) && $_GET['tipo']!=''){?>
                    <li><a href="<?= Url::to(['site/publicaranuncio', 'tipo'=> isset($_GET['tipo']) && $_GET['tipo'] != '' ? $_GET['tipo'] : '']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
                <?php
            }else{ ?>
                     <li><a href="<?= Url::to(['site/publicaranuncio']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
            <?php } ?>

            <li class="active"><a><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
            <li><a><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
            <li><a ><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
            <li class="pull-right" style="margin-right: 20px;margin-top: 25px"><b>ESTADO PUBLICACIÓN:</b> <?= (isset($model->estado) && $model->estado!='') ? $model->_estado[$model->estado]: 'BORRADOR' ?></li>
            <?php
        }else{
            ?>
            <li><a href="<?= Url::to(['site/publicaranuncio', 'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
            <li class="active"><a><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio3','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio4','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
            <li class="pull-right" style="margin-right: 20px;margin-top: 25px">
                <b>ESTADO PUBLICACIÓN:</b> <?= $model->_estado[$model->estado] ?>
            </li>
            <?php
        }
        ?>



    </ul>
    </div>



    <div class="panel panel-default" style="margin-top: 7px">

        <div class="panel-body " style="background: #efebeb;margin-top:0px">
            <!--button class=" btn btn-sm btn-primary pull-right" href="#" style="position: relative;float: right;margin-top: -12px;margin-right: 0px">Vista previa</button-->

            <?php
//\common\components\Utils::dumpx($model->isNewRecord);
     if(!$model->isNewRecord && ($model->estado!=3) || ($model->isNewRecord))
     {
            $form = ActiveForm::begin([
                'options'=>['style'=>'margin-top:20px'],
            ]); ?>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 " >
                    <?php
                    //'0' => 'INACTIVO', '1' => 'ACTIVO', '2' => 'PENDIENTE x APROBAR', '3' => 'BLOQUEADO', '4' => 'VENDIDO',  '5' => 'RECHAZADO',   '6' => 'BORRADOR', 7: PAUSADO
                    if(!$model->isNewRecord && ($model->estado==0 || $model->estado==1 || $model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==5 || $model->estado==7 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-id_tipo required has-success"  title="No puede Editar este campo.">
                                <label class="control-label" value="'.$model->tipo->nombre.'" readonly="readonly" for="pubpublicacion-id_tipo">
                                <span style="cursor: pointer;color: #607d8b">Tipo Vehículo:  </span></label>
                                <label class="form-control" name="PubPublicacion[id_tipo]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '.$model->tipo->nombre.'
                                </label>                               
                                </div>';
                        echo $form->field($model, 'id_tipo')->hiddenInput(['value' => $model->id_tipo])->label(false);

                    }else {
                        echo $form->field($model, 'id_tipo')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(common\models\MotTipo::find()->orderBy('nombre')->all(), 'id', 'nombre'),
                            'options' => ['placeholder' => '- seleccione -','readonly' => true],
                            'pluginOptions' => [
                                'allowClear' => true,'readonly' => 'readonly'
                            ],
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="fa fa-motorcycle"></i>',
                                ],
                            ],
                        ])->label('Tipo Vehículo: ');
                    }
                    ?>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 " >
                <?php

                if(!$model->isNewRecord && ($model->estado==0 || $model->estado==1 || $model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==5 || $model->estado==7 || $model->estado==8))
                {
                    echo '<div class="form-group field-pubpublicacion-id_marca required has-success"  title="No puede Editar este campo." >
                                <label class="control-label" value="'. $model->modelo->marca->nombre.'" readonly="readonly" for="pubpublicacion-id_marca">
                                    <span style="cursor: pointer;color: #607d8b" title="No puede Editar este campo.">Marca:  </span></label>
                                    <label class="form-control" name="PubPublicacion[id_marca]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                    '. $model->modelo->marca->nombre.'
                                </label>                               
                          </div>';
                    echo $form->field($model, 'id_marca')->hiddenInput(['value' => $model->modelo->marca->id])->label(false);

                }else {
                    echo $form->field($model, 'id_marca')->widget(Select2::classname(), [
                        'data' => $marcas,
                        'options' => ['placeholder' => '-- seleccione --', 'id' => 'pubpublicacion-id_marca', 'name' => 'PubPublicacion[id_marca]'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="fa fa-bookmark-o"></i>',
                            ],
                        ]
                    ])->label('Marca: ');
                }
                ?>
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3 " >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==0 || $model->estado==1 || $model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==5 || $model->estado==7 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-id_modelo required has-success"  title="No puede Editar este campo." >
                                <label class="control-label" value="'. $model->modelo->nombre.'" readonly="readonly" for="pubpublicacion-id_modelo">
                                <span style="cursor: pointer;color: #607d8b" title="No puede Editar este campo.">Modelo:  </span></label>
                                <label class="form-control" name="PubPublicacion[id_modelo]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->modelo->nombre.'
                                </label>                               
                                </div>';
                        echo $form->field($model, 'id_modelo')->hiddenInput(['value' => $model->modelo->id])->label(false);

                    }else {
                        echo $form->field($model, 'id_modelo')->widget(DepDrop::classname(), [
                            'data' => $modelos,
                            'options' => ['placeholder' => \Yii::t('app', '-- seleccione --'), 'id' => 'pubpublicacion-id_modelo', 'name' => 'PubPublicacion[id_modelo]'],
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => true],
                                'addon' => ['prepend' => ['content' => '<i class="fa fa-maxcdn"></i>',],
                                ]
                            ],
                            'pluginOptions' => [
                                'depends' => ['pubpublicacion-id_marca'], 'placeholder' => 'Seleccione...',
                                'url' => Url::to(['/site/getmodelosmarca']),
                                'loadingText' => \Yii::t('app', 'Cargando Modelos ...'),
                            ],
                        ])->label('Modelo: ');
                    }
                    ?>
                </div>
                <div class="col-sm-3 col-md-3 col-lg-3 "  >

                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->ano.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Año:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->_ano[$model->ano].'
                            </label>
                        </div>';
                        echo $form->field($model, 'ano')->hiddenInput(['value' => $model->ano])->label(false);
                    }else {
                        echo  $form->field($model, 'ano')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $model->_ano,
                            'options' => ['placeholder' => '- seleccione -'],
                            'pluginOptions' => ['allowClear' => true, 'style' => 'color:red'],
                        ])->label('Año: ');
                    }
                    ?>

                </div>
                <!--div-- class="col-sm-3 col-md-3 col-lg-3 " >
                    <?php
                    //'0' => 'INACTIVO', '1' => 'ACTIVO', '2' => 'PENDIENTE x APROBAR', '3' => 'BLOQUEADO', '4' => 'VENDIDO',  '5' => 'RECHAZADO',   '6' => 'BORRADOR',
                    echo  '<div class="form-group field-pubpublicacion-descripcion required">
                                <label class="control-label" for="pubpublicacion-descripcion">Estado: </label>';
                        if($model->isNewRecord) $model->estado=6;

                        switch ($model->estado){
                            case 0: $color='red'; break;
                            case 1: $color='green'; break;
                            case 2: $color='orange'; break;
                            case 3: $color='#e46a76'; break;
                            case 4: $color='blue'; break;
                            case 5: $color='red'; break;
                            case 6: $color='#7a43b6'; break;
                            case 7: $color='gray'; break;
                            case 8: $color='red'; break;
                        }
                        echo '<label class="form-control" name="PubPublicacion[descripcion]" style="color: '.$color.';background: #f5f5f5">
                                '.$model->_estado[$model->estado].'
                              </label>                            
                          </div>';
                    ?>
                </div-->
            </div>
            <div class="row">

                <div class="col-sm-3 col-md-3 col-lg-3 " >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->condicion.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Condición:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->_condicion[$model->condicion].'
                            </label>
                        </div>';
                        echo $form->field($model, 'condicion')->hiddenInput(['value' => $model->condicion])->label(false);
                    }else {
                        echo $form->field($model, 'condicion')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $model->_condicion,
                            'options' => ['placeholder' => '- seleccione -'],
                            'pluginOptions' => ['allowClear' => true, 'style' => 'color:red'],
                        ])->label('<span style="cursor: pointer"  title="Condición: NUEVA o USADA">Condición: </span>');
                    }
                    ?>

                </div>

                <div class="col-sm-2 col-md-2 col-lg-2" >

                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->recorrido.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Recorrido:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '.$model->recorrido.'
                            </label>
                        </div>';
                        echo $form->field($model, 'recorrido')->hiddenInput(['value' => $model->recorrido])->label(false);
                    }else {
                        echo $form->field($model, 'recorrido')->textInput(['maxlength' => 7, 'class' => 'number-separator form-control'])
                            ->label('<span style="cursor: pointer" title="Cantidad que ha recorrido el vehículo. Debe ser un número.">Recorrido: </span>');
                    }
                    ?>

                </div>
                <div class="col-sm-1 col-md-1 col-lg-1" >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->unid_recorrido.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Unidad:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '.$model->_unid_recorrido[$model->unid_recorrido].'
                            </label>
                        </div>';
                        echo $form->field($model, 'unid_recorrido')->hiddenInput(['value' => $model->unid_recorrido])->label(false);
                    }else {
                        echo $form->field($model, 'unid_recorrido')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $model->_unid_recorrido,
                            'options' => ['placeholder' => 'Unidad'],
                            'pluginOptions' => ['allowClear' => true, 'style' => 'color:red'],
                        ])->label('<span style="cursor: pointer"  title="Unidad del recorrido: Kilometros, Millas o Horas">Unidad:</span>');
                    }
                    ?>

                </div>
                <div class="col-sm-3 col-md-3 col-lg-3" >
                    <?php

                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                                <label class="control-label" value="'.$model->cilindraje.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Cilindraje (cc):  </span></label>
                                <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '.$model->cilindraje.'
                                </label>                               
                                </div>';
                        echo $form->field($model, 'cilindraje')->hiddenInput(['value' => $model->cilindraje])->label(false);
                    }else {
                        echo $form->field($model, 'cilindraje')->textInput(['maxlength' => 5, 'class' => 'number-separator form-control'])
                            ->label('<span style="cursor: pointer"  title="En centrímetos cúbicos">Cilindraje (cc): </span>');
                    }
                    ?>
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3" >
                    <?php
                    //'0' => 'INACTIVO', '1' => 'ACTIVO', '2' => 'PENDIENTE x APROBAR', '3' => 'BLOQUEADO', '4' => 'VENDIDO',  '5' => 'RECHAZADO',   '6' => 'BORRADOR', 7: PAUSADO
                    if(!$model->isNewRecord && ($model->estado==0 || $model->estado==1 || $model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==5 || $model->estado==7 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                                <label class="control-label" value="'.$model->placa.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Placa:  </span></label>
                                <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '.$model->placa.'
                                </label>                               
                                </div>';
                        echo $form->field($model, 'placa')->hiddenInput(['value' => $model->placa])->label(false);
                    }else {
                        //si es nueva.... o :Borrador
                        echo $form->field($model, 'placa')->textInput(['maxlength' => 6, 'title'=>'Las Placas deben ser: 3 letras, 2 números y/o 1 Letra. - No mostraremos la placa en tu publicación. Te la pedimos para evitar que haya vehículos duplicados.'])->label('<span style="cursor: pointer" title="Las Placas deben ser: 3 letras, 2 números y/o 1 Letra - No mostraremos la placa en tu publicación. Te la pedimos para evitar que haya vehículos duplicados.">Placa:</span>');
                    }
                    ?>

                </div>


                <input type="hidden" id="pubpublicacion-descuento"  name="PubPublicacion[descuento]" value="0" aria-required="true">

            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->precio.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Precio:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                $'. number_format($model->precio,0,',','.').'
                            </label>
                        </div>';
                        echo $form->field($model, 'precio')->hiddenInput(['value' => $model->precio])->label(false);
                    }else {
                      echo $form->field($model, 'precio')->widget(MaskMoney::classname(), [
                            'name' => 'currency',
                            'value' => null,
                            'pluginOptions' => [
                                'prefix' => '$ ',
                                'precision' => 0,
                                'allowZero' => true,
                                'allowNegative' => true,
                            ],
                        ])->label('Precio ($):');
                    }
                    ?>

                </div>
                <div class="col-sm-3 col-md-3 col-lg-3" >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->id_ciudad.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">¿Ubicación del vehículo?:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->ciudad->name.' - '.$model->ciudad->state->name.'
                            </label>
                        </div>';
                        echo $form->field($model, 'id_ciudad')->hiddenInput(['value' => $model->id_ciudad])->label(false);
                    }else {
                      echo  $form->field($model, 'id_ciudad')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(common\models\City::find()->orderBy('state_id')->all(), 'id', 'nombreFull'),
                            'options' => ['placeholder' => '- seleccione -'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('<span style="cursor: pointer"  title="Ciudad donde se encuentra el vehículo">¿Ubicación del vehículo?</span>');
                    }
                    ?>
                </div>

                <div class="col-sm-2 col-md-2 col-lg-2 " >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->tipo_combustible.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Tipo Combustible:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->_tipo_combustible[$model->tipo_combustible].'
                            </label>
                        </div>';
                        echo $form->field($model, 'tipo_combustible')->hiddenInput(['value' => $model->tipo_combustible])->label(false);
                    }else {
                        echo $form->field($model, 'tipo_combustible')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $model->_tipo_combustible,
                            'options' => ['placeholder' => '- seleccione -'],
                            'pluginOptions' => ['allowClear' => true, 'style' => 'color:red'],
                        ])->label('<span style="cursor: pointer"  title="Tipo de combustible: Gasolina, Disel, Gas, Eléctrica">Tipo Combustible: </span>');
                    }
                    ?>

                </div>

                <div class="col-sm-2 col-md-2 col-lg-2" >

                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->numero_contacto.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Número Contacto:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->numero_contacto.'
                            </label>
                        </div>';
                        echo $form->field($model, 'numero_contacto')->hiddenInput(['value' => $model->numero_contacto])->label(false);
                    }else {
                       echo  $form->field($model, 'numero_contacto')->textInput(['maxlength' => 10, 'pattern'=>'^[0-9]{10}$', 'value'=>$numero_contacto])
                        ->label('<span style="cursor: pointer"  title="Número Contacto del usuario publicador. Debe contener 10 digitos, si lo actualiza, quedará actualizado para las demás publicaciones, al igual que su perfil.">Número Contacto: </span>');
                    }
                        ?>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2" >
                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->whatsapp.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">WhatsApp:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb">
                                '. $model->whatsapp.'
                            </label>
                        </div>';
                        echo $form->field($model, 'whatsapp')->hiddenInput(['value' => $model->whatsapp])->label(false);
                    }else {
                        echo $form->field($model, 'whatsapp')->textInput(['maxlength' => 10, 'pattern' => '^[0-9]{10}$', 'value' => $whatsapp])
                            ->label('<span style="cursor: pointer"  title="WhatsApp del usuario usuario publicador. Debe contener 10 digitos, si lo actualiza, quedará actualizado para las demás publicaciones, al igual que su perfil.">WhatsApp: </span>');
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >

                    <?php
                    if(!$model->isNewRecord && ($model->estado==2 || $model->estado==3 || $model->estado==4 || $model->estado==8))
                    {
                        echo '<div class="form-group field-pubpublicacion-placa required has-success"  title="No puede Editar este campo.">
                            <label class="control-label" value="'.$model->descripcion.'" readonly="readonly" for="pubpublicacion-placa">
                                <span style="cursor: pointer;color: #607d8b">Descripción detallada del vehículo:  </span></label>
                            <label class="form-control" name="PubPublicacion[placa]" style="background: #f5f5f5; border: 1px solid #ccc;color: #bfbbbb;height: 75px">
                                '. $model->descripcion.'
                            </label>
                        </div>';
                        echo $form->field($model, 'descripcion')->hiddenInput(['value' => $model->descripcion])->label(false);
                    }else {
                        echo $form->field($model, 'descripcion')->textarea(['rows' => 2])
                            ->label('<span style="cursor: pointer"  title="Descripción: Detalle o descripción completa del vehículo que verán los compradores!">Descripción detallada del vehículo: </span>');
                    }
                    ?>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jumbotron" style="background: #fff;text-align: left;padding: 10px; ">

                        <b style="font-size: 14px;color:green">Características adicionales: </b><hr style="padding: 2px;margin-top: 4px;margin-bottom: 4px">
                        <div class="row" style="padding-left:  15px">
                            <?php

                            if (!$model->isNewRecord && ($model->estado == 2 || $model->estado == 3 || $model->estado == 4 || $model->estado == 8))
                            {
                                foreach ($modCaracts as $car){   ?>
                                    <div class="col-sm-3 col-md-3 col-lg-3" style="padding-left:  15px">
                                        <?php if(!$model->isNewRecord) {
                                            $pubCar = \common\models\PubCaracteristica::find()->where('id_publicacion='.$model->id.' and id_caracteristica='.$car->id)->count();
                                            if($pubCar==1){                      ?>
                                                <input class="form-check-input" disabled="disabled" type="checkbox" value="1" name="caracteristicas[<?= $car->id ?>]" checked >
                                            <?php }else { ?>
                                                <input class="form-check-input" disabled="disabled"  type="checkbox" value="" name="caracteristicas[<?= $car->id ?>]" >
                                            <?php }
                                        }else { ?>
                                            <input class="form-check-input" disabled="disabled" type="checkbox" value="" name="caracteristicas[<?= $car->id ?>]" >
                                        <?php  }                  ?>
                                        <label style="font-weight: normal;margin-top: -5px">&nbsp;<?= $car->nombre ?></label>
                                    </div>
                                    <?php
                                }
                            }
                            else
                            {
                                foreach ($modCaracts as $car){   ?>
                                    <div class="col-sm-3 col-md-3 col-lg-3" style="padding-left:  15px">
                                        <?php if(!$model->isNewRecord) {
                                                 $pubCar = \common\models\PubCaracteristica::find()->where('id_publicacion='.$model->id.' and id_caracteristica='.$car->id)->count();
                                                 if($pubCar==1){                      ?>
                                                        <input class="form-check-input" type="checkbox" value="1" name="caracteristicas[<?= $car->id ?>]" checked >
                                           <?php }else { ?>
                                                        <input class="form-check-input" type="checkbox" value="" name="caracteristicas[<?= $car->id ?>]" >
                                                  <?php }
                                            }else { ?>
                                                 <input class="form-check-input" type="checkbox" value="" name="caracteristicas[<?= $car->id ?>]" >
                                        <?php  }                  ?>
                                        <label style="font-weight: normal;margin-top: -5px">&nbsp;<?= $car->nombre ?></label>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>

            <div class="form-group">
                <?php

                    if (!$model->isNewRecord && ($model->estado == 2 || $model->estado == 3 || $model->estado == 4 || $model->estado == 8))
                    {

                    }else{
                      echo  Html::Button('Continuar ... ', ['id'=>'btn_guardar','class' => 'btn btn-success btn-lg pull-right']);
                    }
                ?>
            </div>

            <?php ActiveForm::end();

    }else{
            echo "<center><em style='color: red'>Publicación Bloqueada por el Administrador de vendetumoto.co!<br>Si requiere soporte escriba a: soporte@vendetumoto.co</em></center>";
     }
    ?>
        </div>


    </div>




    <p>&nbsp;</p><p>&nbsp;</p>


  </div>
</div>

