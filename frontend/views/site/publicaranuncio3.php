<?php

use common\models\PubMultimedia;
use common\models\PubPublicacion;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\web\View;

$this->title = 'Publicar Anuncio - Paso 3';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(" 
 jQuery(document).ready(function () 
 {
  
   $('.btn_paso4').click(function(){             

       window.location = '".Url::to(['/site/publicaranuncio4'])."?id=".(isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '')."';
                                 
   });
 
   $('.btn_galeria').click(function(e){  
        e.preventDefault();                                             
        var modal = $('#modal-fotos').modal('show');    
        var that = $(this);
        modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
        $('#div_image').attr('src',that.attr('url_image'));
                                 
   });
 
 
     
});",View::POS_END);

?>



<input type="hidden" id="id_pub" value="<?= isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '' ?>">

<div class="site" >
    <h1 style="color: #f33b42;border: 1px solid #80808021;padding: 10px 0px 10px 10px;background: whitesmoke;margin-bottom: -14px;">Publicación de Anuncio</h1>

    <div id="smartwizard" class="sw-main sw-theme-arrows">
        <ul class="nav nav-tabs step-anchor responsive-utilities-test">
            <li><a href="<?= Url::to(['site/publicaranuncio', 'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio2','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
            <li class="active"><a><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio4','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
            <li class="pull-right" style="margin-right: 20px;margin-top: 25px"><b>ESTADO PUBLICACIÓN:</b> <?= $model->_estado[$model->estado] ?></li>
        </ul>
    </div>
    <?php
            if(!$model->isNewRecord && ($model->estado==0 || $model->estado==1 || $model->estado==5 || $model->estado==6 || $model->estado==7))
            {
                ?>

                    <div class="panel panel-default" style="margin-top: 7px">
                        <div class="panel-body " style="background: #efebeb;margin-top:0px">
                             <iframe src="<?= Url::to(['site/subirimagenes','id'=>isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '', 'tipo'=>isset($_GET['tipo']) && $_GET['tipo']!='' ? $_GET['tipo'] : '' ]) ?>" style="border: none;width: 100%;height: 550px">
                             </iframe>
                        </div>
                        <?php
                        //1: ACTIVO
                        if($model->estado==1){
                        ?>
                            <button type="button" id_pub="<?= $_GET['id'] ?>" class="btn btn-primary btn-lg btn btn-success btn-lg pull-right btn_paso4" role="button"> Actualizar </button>

                        <?php
                        }else{ ?>
                            <button type="button" id_pub="<?= $_GET['id'] ?>" class="btn btn-primary btn-lg btn btn-success btn-lg pull-right btn_paso4" role="button"> Siguiente >> </button>
                        <?php }
                        ?>
                    </div>
      <?php }else{

                $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . (isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : ''))->orderBy('img_order asc')->all();
                if ($fotos) {
                    $model = PubPublicacion::find()->where('id=' . $_GET['id'] . ' and id_usuario=' . Yii::$app->user->identity->id)->one();
                    $output = '<div class="gallery" style="padding: 20px">
                           
                            <ul class="nav nav-pills">';
                    foreach ($fotos as $foto) {
                        $output .= '<li id="image_li_' . $foto->id . '" class="ui-sortable-handle mr-2 mt-2"  id_foto="' . $foto->id . '" >
                                                      <div>
                                                      <a href="javascript:void(0);" class="img-link">
                                                         <img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" alt="" class="img-thumbnail" width="100">
                                                      </a>
                                                     
                                                      </div>
                                                </li>
                                ';
                    }
                    $output .= ' </ul>
        </div>';
                    echo $output;
                }


            } ?>
</div>
</div>



<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-fotos',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    //'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
<div style="padding: 30px;text-align: center;display: flex; justify-content: center;">
    <img id="div_image" src="" class="img-responsive"  style="max-width: 600px" />
</div>

<?php \yii\bootstrap\Modal::end(); ?>


