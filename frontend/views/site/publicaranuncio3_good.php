<?php

use common\models\PubMultimedia;
use common\models\PubPublicacion;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\web\View;

$this->title = 'Publicar Anuncio - Paso 3';
$this->params['breadcrumbs'][] = $this->title;



$this->registerJs(" 
function list_image(id_pub)
 {
 // alert(id_pub);
  //consulto la BD PubMultimedia y cargo las imagenes en time real desde S3
  $.ajax({
   url:'subirarchivos3',
   data:{id:id_pub},
   success:function(data){
    $('#preview').html(data);
   }
  });
  
 }
 
 jQuery(document).ready(function () 
 {
  
   $('.btn_paso4').click(function(){             

         $.post('".Url::to(['site/getfotosanuncio'])."', {id_publicacion: $('.btn_paso4').attr('id_pub') }, 
           function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                    //console.log(res); return false;
                    if(res.res){                      
                         window.location = '".Url::to(['/site/publicaranuncio4'])."?id=".(isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '')."';
                    }else{
                        alert(res.error);
                    }
                
           }
         );
                             
        
                                 
   });
 
   $('.btn_galeria').click(function(e){  
        e.preventDefault();                                             
        var modal = $('#modal-fotos').modal('show');    
        var that = $(this);
        modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
        $('#div_image').attr('src',that.attr('url_image'));
                                 
   });
 
 
    //Eliminar Imagen 
    $(document).on('click', '.remove_image', function(e)
    {
        
        e.preventDefault();
        
        //alert($('#rm'+$(this).attr('id_foto')).attr('id_foto'));
        
         $.post('".Url::to(['site/eliminarfoto'])."', {id_pub:$(this).attr('id_pub'), id_foto:$(this).attr('id_foto') }, 
           function(res_sub) {
                    var res = jQuery.parseJSON(res_sub); 
                    //console.log(res); return false;
                    if(res.res){                      
                        
                       //$('#rm'+$(this).attr('id_foto')).attr('id_foto').remove();
                       $('#rm'+res.id_foto).remove();
                       $('.rm'+res.id_foto).remove();
                       alert('¡Foto eliminada con éxito!');
                       //validar que NO pueda eliminar la foto, si esta es la única.
                       
                       //******************************************
                    }else{
                        alert('ERROR ------');
                    }
                
           }
         );     
        
     });
     
});",View::POS_END);
?>
<link href="<?= Yii::$app->request->baseUrl . '/js/dropzone/dropzone.css'?>" rel="stylesheet">
<script src="<?= Yii::$app->request->baseUrl . '/js/dropzone/dropzone.js'?>"></script>


<input type="hidden" id="id_pub" value="<?= isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '' ?>">

<div class="site" >
    <h1 style="color: #f33b42;border: 1px solid #80808021;padding: 10px 0px 10px 10px;background: whitesmoke;margin-bottom: -14px;">Publicación de Anuncio</h1>

    <div id="smartwizard" class="sw-main sw-theme-arrows">
        <ul class="nav nav-tabs step-anchor responsive-utilities-test">
            <li><a href="<?= Url::to(['site/publicaranuncio', 'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio2','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
            <li class="active"><a><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio4','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
            <li class="pull-right" style="margin-right: 20px;margin-top: 25px"><b>ESTADO PUBLICACIÓN:</b> <?= $model->_estado[$model->estado] ?></li>
        </ul>
    </div>


    <div class="panel panel-default" style="margin-top: 7px">


        <div class="panel-body " style="background: #efebeb;margin-top:0px">


            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 " >

                    <br />
                    <h3 align="center">Suelta las fotos de tu vehículo aquí o haz clic para subirlas.</h3>
                    <br />

                    <form action="subirarchivos3?id=<?= isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '' ?>" class="dropzone" id="dropzoneFrom">

                    </form>
                    <br />
                    <br />
                    <div align="center">
                        <button type="button" class="btn btn-info" id="submit-all">Cargar imágenes ...</button>
                    </div>
                    <br />
                    <b>Fotos agregadas: </b>
                    <br /><br />

                    <div id="preview">
                        <?php
                        $fotos = PubMultimedia::find()->where('tipo=1 and id_publicacion=' . (isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : ''))->all();
                        if($fotos) {
                            $model = PubPublicacion::find()->where('id='.$_GET['id'].' and id_usuario=' . Yii::$app->user->identity->id)->one();
                            $output = "<div class='row'>
                                        ";
                            foreach ($fotos as $foto) {
                                $output .= '<div class=" col-sm-1 col-md-1 col-lg-1 " id="rm'.$foto->id.'" id_foto="'.$foto->id.'" >
                                                <img  class="btn_galeria img-thumbnail img-responsive"data-method="post" style="width:100%;cursor:pointer" title="Click para ampliar" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" url_image="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/' . $model->carpeta . '/' . $foto->archivo . '" archivo="' . $foto->id . '"  />
                                                <button type="button" class="btn btn-sm btn-link remove_image text-danger rm'.$foto->id.'" style="color:red" id_pub="'.$model->id.'" id_foto="' .  $foto->id. '">Eliminar</button>
                                            </div>';
                            }
                            $output .= '</div>';
                            echo $output;
                        }
                        ?>

                    </div>
                    <br />
                    <br />


                    <button type="button" id_pub="<?= $_GET['id'] ?>" class="btn btn-primary btn-lg btn btn-success btn-lg pull-right btn_paso4" role="button"> Siguiente >> </button>
                </div>
            </div>

        </div>

    </div>

</div>
</div>



<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-fotos',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    //'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
<div style="padding: 30px;text-align: center;display: flex; justify-content: center;">
    <img id="div_image" src="" class="img-responsive"  style="max-width: 600px" />
</div>

<?php \yii\bootstrap\Modal::end(); ?>



<script>

    Dropzone.options.dropzoneFrom = {
        maxFiles: 10,
        autoProcessQueue: false,
        acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg,.PNG,.JPG,.GIF,.BMP,.JPEG",
        // autoProcessQueue: true,
        uploadMultiple: false,
        parallelUploads: 10,
        maxFilesize: 5,
        dictDefaultMessage: 'Haz clic aquí y adjunta un máximo 10 fotos.',
        accept: function(file, done) {
            console.log(file.upload);
            done();
        },
        init: function(){
            /*this.on("addedfile", function() {
                if (this.files[10]!=null){
                    this.removeFile(this.files[10]);
                    alert("¡Limite de 10 fotos excedido!");
                }
            });*/
            var submitButton = document.querySelector('#submit-all');
            myDropzone = this;
            submitButton.addEventListener("click", function(){
                myDropzone.processQueue();
            });
            this.on("complete", function(){
                //alert(this.getUploadingFiles().length);
                if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                {
                    var _this = this;
                    _this.removeAllFiles();
                }

                list_image($('#id_pub').val());
            });
        },
    };

</script>
