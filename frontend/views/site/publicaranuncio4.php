<?php

use common\models\PubMultimedia;
use common\models\PubPublicacion;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;
use yii\web\View;

$this->title = 'Publicar Anuncio - Paso 4';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(" 
function list_image(id_pub)
 {
 // alert(id_pub);
  //consulto la BD PubMultimedia y cargo las imagenes en time real desde S3
  $.ajax({
   url:'subirarchivos3',
   data:{id:id_pub},
   success:function(data){
    $('#preview').html(data);
   }
  });
  
 }
 
 jQuery(document).ready(function () 
 {
   //HOVER CAJA PLAN
   $('.box').hover(function(){      $(this).toggleClass('cn');   });
  
  
   //CLICK EN BOTÓN : QUIERO ESTE PLAN! - SIMULA LA COMPRA
        /*  RANDI: Esta función SIMULA que SI hizo todo el pago efectivo:
         *  Va y guarda en la tabla 'Payments' como si 'el pago fue exitoso', pero todavía no toca nada en 'payments_webhook'
         */
   $('.btn_epayco').click(function(e)
   {
      e.preventDefault();       
 
      $.post('".Url::to(['site/pagoaprobado'])."', {id_publicacion: $(this).attr('id_pub'), id_plan: $(this).attr('id_plan')  }, 
       function(res_sub) {
                var res = jQuery.parseJSON(res_sub); 
                if(res.res){
                   alert('¡PAGO con Epayco realizado con éxito! Ir a : '+res.url);
                   window.location = res.url;
                }else{
                    alert(res.error);
                }
            
       }
      );
                                 
   });
 
   $('.btn_galeria').click(function(e){  
        e.preventDefault();                                             
        var modal = $('#modal-fotos').modal('show');    
        var that = $(this);
        modal.find('.modal-title').html('Foto de galería - ID # '+that.attr('archivo'));                                
        $('#div_image').attr('src',that.attr('url_image'));
                                 
   });
 
 
    //Eliminar Imagen 
    $(document).on('click', '.remove_image', function(){
      var name = $(this).attr('id');
      $.ajax({
       url:'eliminarfoto',
       method:'POST',
       data:{name:name},
       success:function(data)
       {
        list_image();
       }
      })
     });
});",View::POS_END);
?>
<link href="<?= Yii::$app->request->baseUrl . '/js/dropzone/dropzone.css'?>" rel="stylesheet">
<script src="<?= Yii::$app->request->baseUrl . '/js/dropzone/dropzone.js'?>"></script>

<input type="hidden" id="id_pub" value="<?= isset($_GET['id']) && $_GET['id']!='' ? $_GET['id'] : '' ?>">
<input type="hidden" id="id_plan">

<div class="site" >
    <h1 style="color: #f33b42;border: 1px solid #80808021;padding: 10px 0px 10px 10px;background: whitesmoke;margin-bottom: -14px;">Publicación de Anuncio</h1>

    <div id="smartwizard" class="sw-main sw-theme-arrows">
        <ul class="nav nav-tabs step-anchor responsive-utilities-test">
             <li><a href="<?= Url::to(['site/publicaranuncio', 'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>" ><b style="font-size: 20px">Paso 1</b><br><small>Tipo de vehículo</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio2','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 2</b><br><small>Datos básicos</small></a></li>
            <li><a href="<?= Url::to(['site/publicaranuncio3','tipo'=>2,'id'=> isset($_GET['id']) && $_GET['id'] != '' ? $_GET['id'] : '']) ?>"><b style="font-size: 20px">Paso 3</b><br><small>Fotos de vehículo</small></a></li>
            <li class="active"><a><b style="font-size: 20px">Paso 4</b><br><small>Publicación &nbsp;&nbsp;</small></a></li>
            <li class="pull-right" style="margin-right: 20px;margin-top: 25px"><b>ESTADO PUBLICACIÓN:</b> <?= $model->_estado[$model->estado] ?></li>
        </ul>
    </div>


    <div class="panel panel-default" style="margin-top: 7px">


        <div class="panel-body " style="background: #efebeb;margin-top:0px">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?php
                    if($model->estado==0 || $model->estado==6){
                    ?>
                    <h3 style="color: #474747">Escoge un plan de para promocionar tu anuncio</h3>
                    <h5 style="color: #474747">Todos los planes son pagos, cada plan define los tiempo y servicios a continuación: </h5>
                    <?php } ?>
                </div>
            </div>
            <div class="row">

                <?php
                $pago_activo = \common\models\Payments::find()->where('id_publicacion='.$model->id.' and state=1')->one();
                //  0 - INACTIVO', 1 - ACTIVO,  2 - PENDIENTE_X_APROBAR', 3 - BLOQUEADO, 4 - VENDIDO , 5 - RECHAZADO, 6 - BORRADOR ,  7 - PAUSADO
                if(/*$model->estado==1 || */$model->estado==2/* || $model->estado==5*/){

                    //Obtengo el Pago ACTIVO que debe existir, de ésta publicación, sinó existe: Nunca ha pagado


                    if($pago_activo)
                    {
                        foreach ($planes as $plan){
                            $servicios = explode(',',$plan->descripcion);
                            ?>
                            <div class="col-sm-6 col-md-6 col-lg-4">
                                <div class="well " style="background: ;">
                                    <div class="well" style="background: ;border: 1px solid #fff;box-shadow:none;border-bottom: 1px solid #8080804d; ">
                                        <p style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 32px;line-height: 39px;letter-spacing: 1.02054px;color: gray;">
                                            <?= $plan->nombre ?><br>
                                            <b style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 22px;line-height: 27px;letter-spacing: 0.701619px;color: #474747;">
                                                $<?= number_format($plan->precio,0,',','.') ?>
                                            </b>
                                        </p>
                                    </div>
                                    <div class="well" style="padding-top: 0px;background: ;border: 1px solid #fff;box-shadow:none;margin-top: 0px; min-height: 300px ">
                                        <?php
                                        if($plan->id==$pago_activo->id_paquete){
                                            ?>
                                            <p>
                                            <center style="padding: 20px;font-size:18px;color: green">
                                                Este es el plan adquirido para esta publicación.<br>
                                                <i style=" color:green; font-size: 40px " class="glyphicon glyphicon-check"></i><br><br>
                                                ESTADO DE LA PUBLICACIÓN<br>  <?php
                                                echo '<span style="color: green">';
                                                switch ($model->estado){
                                                    case 1:
                                                        echo '<b style="color:green">'.$model->_estado[$model->estado].'</b>
                                                         <br><em style="font-size: 16px"> Quedan 45 días de publicación </em>';break;
                                                    case 2: echo '<b style="color:blue">'.$model->_estado[$model->estado].'</b><br><em style="font-size: 16px">(Debes esperar unos minutos en que sea aprobado por el administrador)</em>';break;
                                                    case 5: $rechazo = \common\models\PubRechazo::find()->where('id_publicacion='.$model->id)->orderBy('id DESC')->limit(1)->one();
                                                        echo '<b style="color:red">'.$model->_estado[$model->estado].'</b><br>
                                                                    Motivo de Rechazo es:<br><b style="color:red;font-size: 14px"> '.$rechazo->motivo.'</b><br>
                                                                    <em style="font-size: 12px">(Debes realizar las correcciones y volver a publicar, para que sea nuevamente revisado para Aprobación por el administrador).</em>'; break;
                                                }
                                                echo '</span>';



                                                ?>

                                            </center>
                                            </p>
                                        <?php } ?>


                                    </div>

                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        echo "<em style='color:red;padding: 10px'>¡Error! La Publicación # ".$model->id." que está intentando acceder, NO tiene ningún pago Activo! - Preguntar caso a : soporte@vendetumoto.co</em>";
                    }


                }else//  0 - INACTIVO', 1 - ACTIVO,  2 - PENDIENTE_X_APROBAR', 3 - BLOQUEADO, 4 - VENDIDO , 5 - RECHAZADO, 6 - BORRADOR ,  7 - PAUSADO
                    if($model->estado==0 || $model->estado==1 || $model->estado==6 || $model->estado==4)
                    {

                      foreach ($planes as $plan){
                          $servicios = explode(',',$plan->descripcion);      ?>

                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <?php
                             if($pago_activo){
                                if($plan->id==$pago_activo->id_paquete){
                                ?>
                                <p>
                                    <?php

                                    switch ($model->estado){
                                        case 1:  $str = '<b style="color:green">'.$model->_estado[$model->estado].'</b>';break;
                                        case 2: $str = '<b style="color:blue">'.$model->_estado[$model->estado].'</b><br><em style="font-size: 16px">(Debes esperar unos minutos en que sea aprobado por el administrador)</em>';break;
                                        case 5: $rechazo = \common\models\PubRechazo::find()->where('id_publicacion='.$model->id)->orderBy('id DESC')->limit(1)->one();
                                                $str = '<b style="color:red">'.$model->_estado[$model->estado].'</b><br>
                                                            Motivo de Rechazo es:<br><b style="color:red;font-size: 14px"> '.$rechazo->motivo.'
                                                        </b><br>
                                                        <em style="font-size: 12px">(Debes realizar las correcciones y volver a publicar, para que sea nuevamente revisado para Aprobación por el administrador).</em>';
                                                break;
                                    }
                                    ?>

                                <div style="border: 2px dotted red;padding: 10px;display: flex;text-align: justify;">
                                   <span style=" font-size:12px;color: green">
                                       Esta publicación tiene éste plan <?= $str ?>. En <b><?=$model->dias_faltantes ?></b> días finaliza la publicación. Puedes comprar nuevamente este o cualquier otro plan. Se acumularán los días restantes más los del nuevo plan adquirido.<br>
                                    </span>
                                </div>
                                </p>
                            <?php }
                             }?>
                            <div class="well box" style="background: #fff;">

                                <div class="well" style="background: #fff;border: 1px solid #fff;box-shadow:none;border-bottom: 1px solid #8080804d; ">
                                    <p style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 32px;line-height: 39px;letter-spacing: 1.02054px;color: #F33B42;">
                                        <?= $plan->nombre ?><br>
                                        <b style="font-family: Montserrat;font-style: normal;font-weight: bold;font-size: 22px;line-height: 27px;letter-spacing: 0.701619px;color: #474747;">
                                            $<?= number_format($plan->precio,0,',','.') ?>
                                        </b>
                                    </p>
                                </div>
                                <div class="well" style="padding-top: 0px;background: #fff;border: 1px solid #fff;box-shadow:none;margin-top: 0px; min-height: 300px ">
                                    <ul>
                                    <?php
                                       for($i=0;$i<count($servicios);$i++){
                                           echo '<li class="">'.$servicios[$i].'</li>';
                                       }
                                    ?>
                                    </ul>

                                </div>
                            <!-- <center><button id_plan="<?= $plan->id ?>" type="button" id_pub="<?= $_GET['id'] ?>" class="btn btn-primary btn-lg btn btn-danger btn-lg btn_epayco" role="button">¡Quiero este plan!</button></center> -->
                            
                            <form style="text-align: center;">
                                
                                <?php 
                                // Crear hash de data  ---------------------------------------------------
                                    $data = (object) array('email' =>  Yii::$app->user->identity->email,
                                    'id_usuario' => Yii::$app->user->identity->id,
                                    'id_plan' => $plan->id,
                                    'id_publicacion' => $_GET['id'],
                                    );
                                    $externalEncode =  base64_encode( json_encode($data) );
                                ?>
                                <script
                                    src="https://checkout.epayco.co/checkout.js"
                                    class="epayco-button"
                                    data-epayco-key="ad3803f83651008db945a6feb5168558"
                                    data-epayco-amount="<?= $plan->precio ?>"
                                    data-epayco-extra1="soporte@vendetumoto.co"
                                    data-epayco-extra2="<?= $externalEncode ?>"
                                    data-epayco-extra3="<?= $model->id ?>"
                                    data-epayco-extra4="<?= Yii::$app->user->identity->id ?>"
                                    data-epayco-extra5="<?= $plan->id ?>"
                                    data-epayco-name="<?= $plan->nombre ?> ($<?= number_format($plan->precio,0,'.',',') ?>)"
                                    data-epayco-description="Publicación ID # <?= $model->id ?> - <?= $plan->nombre ?> ($<?= number_format($plan->precio,0,'.',',') ?>)"
                                    data-epayco-currency="cop"
                                    data-epayco-country="co"
                                    data-epayco-test="true"
                                    data-epayco-external="false"
                                    data-epayco-response="<?= Url::to(['/site/epaycotransaction'],true) ?>"
                                    data-epayco-confirmation="<?= Url::to(['/site/epaycoconfirmation'],true) ?>"
                                    data-epayco-button="<?= Yii::$app->request->baseUrl.'/img/pagar_ahora.png' ?>"
                                    >
                                </script>
                            </form>
                        </div>
                    </div>
                <?php
                  }
                }elseif($model->estado==3){ ?>
                    <p>
                    <center style="padding: 20px;font-size:18px;color: red">
                        Esta publicación ha sido  <b>BLOQUEADA</b> debido a denuncias de usuarios. <br>Su contenido va en contra de las políticas de publicación de <b>vendetumoto.co</b><br>
                        
                        <i style="color: red;font-weight: bold;font-size: 40px" class="fa fa-ban" aria-hidden="true"></i>
                        <br><br>

                    </center>
                    </p>
                <?php }elseif($model->estado==4){ ?>

                <?php }else{
                        echo 'estado: '.$model->estado;
                    }
                ?>

            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">

                </div>
            </div>

        </div>

    </div>

</div>
</div>



<?php
\yii\bootstrap\Modal::begin([
    //'header' => '<h2 class="modal-title" style="color:#dd4b39"></h2>',
    'id'     => 'modal-fotos',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false, ],
    //'footer' => '<button type="button" class="btn cerrar" data-dismiss="modal" aria-hidden="true">Cerrar</button>',
    'size'=>' modal-lg',

    'bodyOptions' => ['class' => 'modal-wide']
]);

?>
<div style="padding: 30px;text-align: center;display: flex; justify-content: center;">
    <img id="div_image" src="" class="img-responsive"  style="max-width: 600px" />
</div>

<?php \yii\bootstrap\Modal::end(); ?>



<script>

    Dropzone.options.dropzoneFrom = {
        maxFiles: 10,
        autoProcessQueue: false,
        acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg,.PNG,.JPG,.GIF,.BMP,.JPEG",
       // autoProcessQueue: true,
        uploadMultiple: false,
        parallelUploads: 10,
        maxFilesize: 5,
        dictDefaultMessage: 'Haz clic aquí y adjunta un máximo 10 fotos.',
        accept: function(file, done) {
            console.log(file.upload);
            done();
        },
        init: function(){
            /*this.on("addedfile", function() {
                if (this.files[10]!=null){
                    this.removeFile(this.files[10]);
                    alert("¡Limite de 10 fotos excedido!");
                }
            });*/
            var submitButton = document.querySelector('#submit-all');
            myDropzone = this;
            submitButton.addEventListener("click", function(){
                myDropzone.processQueue();
            });
            this.on("complete", function(){
                //alert(this.getUploadingFiles().length);
                if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                {
                    var _this = this;
                    _this.removeAllFiles();
                }

                list_image($('#id_pub').val());
            });
        },
    };

</script>
