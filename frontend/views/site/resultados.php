<?php

use common\components\Utils;
use common\models\PubFavoritos;
use yii\widgets\ActiveForm;
use yii\web\View;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\widgets\ListView;

use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\CliUsuario;


$this->title = 'Resultados de búsqueda';



$this->registerJs(" 
jQuery(document).ready(function () {

//******************************************************
     
      
     $('#btn_iniciar_sesion').click(function(e){
        e.preventDefault();   
        
        if($('#loginform-username').val()!='' && $('#loginform-password').val()!='')
        {      
        
            if($('#loginform-username').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#loginform-username').val($('#loginform-username').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#loginform-username').val()).toLowerCase())){                      
                  $('.field-loginform-username > .help-block').html('¡El Correo es incorrecto!');
                  return false;
               }
            }   
        
        
             $.post('".Url::to(['site/emailexiste'])."', {email: $('#loginform-username').val() }, 
               function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        console.log(res); //return false;
                        if(!res.res){  //FALSE: SI EXISTE: Ahora pregunte si el password es correcto                    
                             //Preguntar por contraseña
                              $.post('".Url::to(['site/existepassword'])."', {email: $('#loginform-username').val(), pass: $('#loginform-password').val() }, 
                                   function(res_sub) {
                                            var res = jQuery.parseJSON(res_sub); 
                                            console.log(res); //return false;
                                            if(res.res){  //TRUE: SI ES EL PASSWORD: LOGUEARSE....                    
                                                 $('#login-form').submit();                                                  
                                            }else{
                                                $('.field-loginform-password > .help-block').html('<b style=\'color:red\'>¡Contraseña Incorrecta!</b>');
                                                return false;
                                            }
                              });
                        }else{
                            $('.field-loginform-username > .help-block').html('<b style=\'color:red\'>¡Correo NO Existe en VTM!</b>');
                            return false;
                        }
                    
             });
        }else{
          if($('#loginform-username').val()=='') $('.field-loginform-username > .help-block').html('Debe ingresar el correo');
          if($('#loginform-password').val()=='') $('.field-loginform-password > .help-block').html('Debe Ingresar su contraseña');
        }
    }); 
    
    $('#cliusuario-nombre').change(function(e){     
        if($('#cliusuario-nombre').val()!='') $('.field-cliusuario-nombre > .help-block').html('');
    });
    
    $('#signupform-email').change(function(e){     
        if($('#signupform-email').val()!='') $('.field-signupform-email > .help-block').html('');
    });
    
    $('#signupform-password').change(function(e){     
        if($('#signupform-password').val()!='') $('.field-signupform-password > .help-block').html('');
    });
    
    $('#signupform-repeat_password').change(function(e){     
        if($('#signupform-repeat_password').val()!='') $('.field-signupform-repeat_password > .help-block').html('');
    });
    
    $('#cliusuario-telefonos').change(function(e){     
        //if($('#cliusuario-telefonos').val()!=''){ 
          $('.field-cliusuario-telefonos > .help-block').html('');
        //}
    });
    
    $('#cliusuario-whatssapp').change(function(e){     
        //if($('#cliusuario-whatssapp').val()!=''){ 
            $('.field-cliusuario-whatssapp > .help-block').html('');
        //}
    });
    
    $('.loginform-username').change(function(e){     
            $('.field-loginform-username > .help-block').html('');
    });
    
    $('.loginform-password').change(function(e){     
            $('.field-loginform-password > .help-block').html('');
    });
    
    $('#signupform-acepto_terminos').change(function() {
        if(this.checked) {
             $('.field-signupform-acepto_terminos > .help-block').html('');
             $('.normal_text2').attr('style','border: 0px');     
             
        }
    });
    
    
    
    
    $('#btn_reg').click(function(e){
        e.preventDefault();   
           var error=0;
           var filter = /^\d*(?:\.\d{1,2})?$/;
           if($('#cliusuario-telefonos').val()!=''){
               if(filter.test($('#cliusuario-telefonos').val())) {
                   if($('#cliusuario-telefonos').val().length < 10){
                       $('.field-cliusuario-telefonos > .help-block').html('El número de contacto debe tener 10 dígitos.');
                       $('#cliusuario-telefonos').focus();
                       error=1; 
                   }
               }else{
                  $('.field-cliusuario-telefonos > .help-block').html('El Número de contacto NO es un número válido.');        
                  error=2;         
               }
           } 
            
           if($('#cliusuario-whatssapp').val()!=''){
               if(filter.test($('#cliusuario-whatssapp').val())) {
                   if($('#cliusuario-whatssapp').val().length < 10){
                       $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp debe tener 10 dígitos.');
                       $('#cliusuario-whatssapp').focus();
                       error=3;
                   }
               }else{
                  $('.field-cliusuario-whatssapp > .help-block').html('El WhatssApp NO es un número válido.');
                  error=4;                 
               }
           }         
            
            if($('#signupform-email').val()!='')
            {  //quita espacios que existan en el email (antes, contenidos y al final de la cadena)
               $('#signupform-email').val($('#signupform-email').val().replace(/ /g,''));
               const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
               if(!re.test(String($('#signupform-email').val()).toLowerCase())){                      
                  $('.field-signupform-email > .help-block').html('¡CORREO INVÁLIDO!');
                  //return false;
                  error=5;
               }
            }   
            
            
            
            if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){
                    $('.field-signupform-password > .help-block').html('La contraseña debe ser de 6 caracteres.');
                    error=6;
            }
            
            if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){
                    $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe ser de 6 caracteres.');
                    error=7;
            }
            
            if($('#signupform-password').val() != $('#signupform-repeat_password').val()){
                $('.field-signupform-repeat_password > .help-block').html('Confirmación contraseña NO concuerda con la contraseña.');
                error=8;
             }else{
                $('.field-signupform-password > .help-block').html('');
                $('.field-signupform-repeat_password > .help-block').html('');
               
             }
            
        var error1=error2=0;
        if( $('#cliusuario-telefonos').val()!='' && $('#signupform-whatssapp').val()!='' && $('#cliusuario-nombre').val()!='' && $('#signupform-email').val()!='' && $('#signupform-password').val()!='' && $('#signupform-repeat_password').val()!='' && $('input[id=signupform-acepto_terminos]:checked').val())
        {            
               if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){                    
                    error1=1;                        
               }                
               if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){                        
                     error2=1;
               }
               
               if(error1==1 && error2==1){
                   $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.'); 
                   $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');   
                   return false;                   
               }else if(error1==1) {
                    $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.');    
                    return false;
               }else if(error2==1) {
                        $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');    
                        return false;  
                    }
            //Si llegó hasta acá y checkeó, REGISTRA
            if($('input[id=signupform-acepto_terminos]:checked').val()){
                
                 $.post('".Url::to(['site/emailexiste'])."', {email: $('#signupform-email').val() }, 
                   function(res_sub) {
                            var res = jQuery.parseJSON(res_sub); 
                            //console.log(res); return false;
                            if(res.res){
                               // alert(error);  
                                if(error==0){
                                   //alert('ingresó hasta el submit()');
                                   $('#register-form').submit();
                                }else 
                                  return false;
                            }else{
                                $('.field-signupform-email > .help-block').html('<b style=\'color:red\'>¡CORREO YA EXISTE EN VTM!</b>');
                                return false;
                            }
                        
                   }
                 );
            }else{
               $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
               $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');               
            }
            
          
        }else{
        
           if($('#cliusuario-whatssapp').val()=='') $('.field-cliusuario-whatssapp > .help-block').html('WhatssApp NO puede estar vacío.');
           if($('#cliusuario-telefonos').val()=='') $('.field-cliusuario-telefonos > .help-block').html('Nro de contacto NO puede estar vacío.');
           if($('#cliusuario-nombre').val()=='') $('.field-cliusuario-nombre > .help-block').html('Nombre completo NO puede estar vacío.');
           if($('#signupform-email').val()=='') $('.field-signupform-email > .help-block').html('Email NO puede estar vacío.');
           if($('#signupform-password').val()=='') $('.field-signupform-password > .help-block').html('Contraseña NO puede estar vacío.');
           if($('#signupform-repeat_password').val()=='') $('.field-signupform-repeat_password > .help-block').html('Confirmación de contraseña NO puede estar vacío.');
           if(!$('input[id=signupform-acepto_terminos]:checked').val()){ 
            $('.field-signupform-acepto_terminos > .help-block').html('<b style=\'color:red\'>Debes Aceptar Términos y Condiciones para Registrarte</b>');
            $('.normal_text2').attr('style','border: 1px solid #FF596A; border-radius:7px');
            
           }
           
           if($('#signupform-password').val()!='' && $('#signupform-password').val().length < 6){                    
                    $('.field-signupform-password > .help-block').html('Contraseña debe ser mínimo de 6 caracteres.'); 
           }
            
           if($('#signupform-repeat_password').val()!='' && $('#signupform-repeat_password').val().length < 6){
                    $('.field-signupform-repeat_password > .help-block').html('Confirmar contraseña debe mínimo ser de 6 caracteres.');                 
           }
           
             
        }
    
    });
//******************************************************

    

 $('.counter-value').each(function(){
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        },{
            duration: 3500,
            easing: 'swing',
            step: function (now){
                $(this).text(Math.ceil(now));
            }
        });
    });
    

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    }

 
  
    $('.btn_suscribirme').click(function(e){
        e.preventDefault();          
        if($('#txt_email').val()!=''){
          
           $('#txt_email').val($('#txt_email').val().replace(/ /g,''));
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(!re.test(String($('#txt_email').val()).toLowerCase())){
                alert('ERROR: ¡EMAIL INVÁLIDO! Debes ingresar un email correcto.');
                document.getElementById('txt_email').focus();
                return false;
           }
           
           if($('input[name=check_suscrib]:checked').val()==1){
                 //Grabo en BD y envío un email al suscriptor
                 $.post('".Url::to(['site/addsuscriptor'])."', {email: $('#txt_email').val() }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==1)  {                    
                               alert('¡Felicitaciones! Tu email ha quedado inscrito en nuestro NewsLetter.');
                            }else {                                  
                                   alert('Información: ¡El email ingresado YA está inscrito en nuestro NewsLetter!');
                            }                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
           }else{
             alert('ERROR: Debes aceptar los términos y condiciones para aceptación de tratamiento de datos y envío de información.');
             return false;
           }
        
        }else{
           alert('¡Debes ingresar un email!');
        }
   });
   
    $('#login-form-link').click(function(e) {
        $('#login-form').delay(100).fadeIn(100);
        $('#register-form').fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        
        $('.txt_reg').attr('style','font-weight:normal');
        $('.txt_is').attr('style','font-weight:bold');
        
        e.preventDefault();
    });
    
    
 
    
    $('#register-form-link').click(function(e) {
        e.preventDefault();
        
        $('#register-form').delay(100).fadeIn(100);
        $('#login-form').fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
    
        $('.txt_reg').attr('style','font-weight:bold');
        $('.txt_is').attr('style','font-weight:normal');
        
        
        
        
        
        
        
        
    });
});
", View::POS_END);

$this->registerJs(" 
jQuery(document).ready(function () {
   
    $('.btn_login').click(function(e){ 
          
          $('#loginform-username').val('');
          $('#loginform-password').val('');
          $('#signupform-email').val('');
          $('#signupform-password').val('');
          $('#cliusuario-nombre').val('');
          $('#cliusuario-nombre').prop('autofocus',true);
          $('#cliusuario-nombre').focus();
          
          
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            //modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();                 
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');      
            
         
    });
    
    $('.btn_login2').click(function(e){ 
        $('#loginform-username').val('');
          $('#loginform-password').val('');
          $('#signupform-email').val('');
          $('#signupform-password').val('');
          $('#cliusuario-nombre').val('');
          $('#cliusuario-nombre').prop('autofocus',true);
          $('#cliusuario-nombre').focus();
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
             $('#modal-menu').modal('hide');  
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
     $('.btn_menu').click(function(e){ 
       
            var modal = $('#modal-menu').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
 
     
     
     
    $('#btn_busq_filtros').click(function(e)
    {
    
        //Precio validación
        if($('#pubpublicacion-precio_desde').val()!='' && $('#pubpublicacion-precio_hasta').val()!=''){                
            if($('#pubpublicacion-precio_desde').val()*1 > $('#pubpublicacion-precio_hasta').val()*1)
            {
                alert('Error Precio: Precio Desde debe ser menor que Precio Hasta.');
                return false;  
            }
        }
    
         if($('#mottipo-id').val()!='' || $('#motmarca-id').val()!='' || $('#motmodelo-id').val()!=''  || $('#pubpublicacion-precio_desde').val()!=''  || $('#pubpublicacion-precio_hasta').val()!=''
            || $('#pubpublicacion-ano_desde').val()!='' || $('#pubpublicacion-ano_hasta').val()!=''){
        
           
             
             //Año validación
             if($('#pubpublicacion-ano_desde').val() > $('#pubpublicacion-ano_hasta').val()  ){
                   alert('ERROR: El (AÑO Desde) debe ser menor o igual al (AÑO Hasta).'); return false;            
             }
             
             $('#frm').submit();
            //  window.location.href = '".Url::to(['site/resultados'])."?tipo='+$('#mottipo-id').val()+'&id_marca='+$('#motmarca-id').val()+'&id_modelo='+$('#motmodelo-id').val()+'&precio_desde='+$('#desde').val()+'&precio_hasta='+$('#hasta').val()+'&ano_desde='+$('#pubpublicacion-ano_desde').val()+'&ano_hasta='+$('#pubpublicacion-ano_hasta').val();
             
         
        }else{
          alert('ERROR: Debes seleccionar alguna opción.');
        } 
    });
   
   $('.btn_favorito').click(function(e){
    
        e.preventDefault();          
        if($(this).attr('id_usuario')=='' && $(this).attr('logueado')==0){
            //alert('¡Primero debes Iniciar Sesión o Registrarte!');
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            modal.find('.modal-title').html('Debes iniciar sesión para guardar en favoritos');                           
        }else                                   
            if($(this).attr('id_pub')!='')
            {
                 $.post('".Url::to(['site/favoritoupdate'])."', {id_publicacion: $(this).attr('id_pub'), id_usuario: $(this).attr('id_usuario') }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==2)  {                    
                               alert('¡Publicación ha sido eliminada de tus favoritos con éxito!');
                               $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg2 \"><i class=\"fa fa-heart-o\"></i></div>');
                            }else {                                  
                                   alert('¡Publicación agregada a tus favoritos con éxito!');
                                  $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg danger \"><i class=\"fa fa-heart\"></i></div>');
                              }
                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
                 
            }else{
              alert('Error: ID_PUB no existe.');
            }      
         
    });
   
    
   
});  


$('#btn_abrir_cerrar').click(function(e){
    e.preventDefault();
    //alert($('#op').val());

    if($('#op').val()==1){
       $('.bg').attr('style','height: 321px; background: url(\'".Yii::$app->request->baseUrl."/img/banner_home_abierto.png"."\'); background-repeat: no-repeat; background-position: top; background-size: contain;');
       
       $('#btn_abrir_cerrar').removeClass('a1');
       $('#btn_abrir_cerrar').addClass('a2');      
       $('#frm').removeClass('form');
       $('#frm').addClass('form2');      
       
       $('.img-pest').attr('src','".Yii::$app->request->baseUrl."/img/pestana_cerrar.png"."');
       $('.img-pest').attr('style','width: 20px');

        $('#op').val(2);
    }else{
        $('#op').val(1);
        $('.bg').attr('style','height: 156px; background: url(\'".Yii::$app->request->baseUrl."/img/banner_home_sin_abrir.png"."\'); background-repeat: no-repeat; background-position: top; background-size: contain;');
       
        $('.img-pest').attr('src','".Yii::$app->request->baseUrl."/img/pestana_abrir.png"."');
        $('.img-pest').attr('style','width: 20px');
        
        $('#btn_abrir_cerrar').removeClass('a2');
        $('#btn_abrir_cerrar').addClass('a1');
        $('#frm').addClass('form');
        $('#frm').removeClass('form2');      
        

    }
        
        
});

   
");


$this->registerJs(" 
jQuery(document).ready(function () {
 
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    }

    $('.min-slider-handle').click(function(e){
        e.preventDefault();
  
        $('.desde').html('$'+numberWithCommas($(this).attr('aria-valuenow')));
         $('#frm').submit();
    });
    
    $('.max-slider-handle').click(function(e){
        e.preventDefault();
  
        $('.hasta').html('$'+numberWithCommas($(this).attr('aria-valuenow')));
        $('#frm').submit();
    });
  
   $('.btn_favorito').click(function(e){
    
        e.preventDefault();          
        if($(this).attr('id_usuario')=='' && $(this).attr('logueado')==0){
            //alert('¡Primero debes Iniciar Sesión o Registrarte!');
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            modal.find('.modal-title').html('Debes iniciar sesión para guardar en favoritos');                           
        }else                                   
            if($(this).attr('id_pub')!='')
            {
                 $.post('".Url::to(['site/favoritoupdate'])."', {id_publicacion: $(this).attr('id_pub'), id_usuario: $(this).attr('id_usuario') }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==2)  {                    
                               alert('¡Publicación ha sido eliminada de tus favoritos con éxito!');
                               $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg2 \"><i class=\"fa fa-heart-o\"></i></div>');
                            }else {                                  
                                   alert('¡Publicación agregada a tus favoritos con éxito!');
                                  $('#fav_'+res.id_pub).html('<div class=\"vertical-timeline-icon navy-bg danger \"><i class=\"fa fa-heart\"></i></div>');
                              }
                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
                 
            }else{
              alert('Error: ID_PUB no existe.');
            }      
    });  
 
    $('.btn_suscribirme').click(function(e){
        e.preventDefault();          
        if($('#txt_email').val()!=''){
          
           $('#txt_email').val($('#txt_email').val().replace(/ /g,''));
           const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(!re.test(String($('#txt_email').val()).toLowerCase())){
                alert('ERROR: ¡EMAIL INVÁLIDO! Debes ingresar un email correcto.');
                document.getElementById('txt_email').focus();
                return false;
           }
           
           
           if($('input[name=check_suscrib]:checked').val()==1){
                 //Grabo en BD y envío un email al suscriptor
                 $.post('".Url::to(['site/addsuscriptor'])."', {email: $('#txt_email').val() }, 
                   function(res_sub) {
                        var res = jQuery.parseJSON(res_sub); 
                        //console.log(res); return false;
                        if(res.res){
                           if(res.tipo==1)  {                    
                               alert('¡Felicitaciones! Tu email ha quedado inscrito en nuestro NewsLetter.');
                            }else {                                  
                                   alert('Información: ¡El email ingresado YA está inscrito en nuestro NewsLetter!');
                            }                              
                        }else{
                            alert('ERROR: ¡Parámetros inválidos!');
                        }
                   }
                 );
           }else{
             alert('ERROR: Debes aceptar los términos y condiciones para aceptación de tratamiento de datos y envío de información.');
             return false;
           }
        
        }else{
           alert('¡Debes ingresar un email!');
        }
    }); 
    
    

    $('.form-check-input').change(function(e){
     e.preventDefault();     
     if($(this).attr('id') != 'check_suscrib')
       $('#frm').submit();
    });


    $('#login-form-link').click(function(e) {
        $('#login-form').delay(100).fadeIn(100);
        $('#register-form').fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        
        $('.txt_reg').attr('style','font-weight:normal');
        $('.txt_is').attr('style','font-weight:bold');
        
        e.preventDefault();
    });
    
    $('#register-form-link').click(function(e) {
        $('#register-form').delay(100).fadeIn(100);
        $('#login-form').fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
    
        $('.txt_reg').attr('style','font-weight:bold');
        $('.txt_is').attr('style','font-weight:normal');
        
        
        
        e.preventDefault();
    }); 

    $('.btn_login').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
            //modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();                 
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'1'])."');      
            
         
    });
    $('.btn_login2').click(function(e){ 
       
            var modal = $('#modal-login').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });
    
    $('.btn_menu').click(function(e){ 
           
            var modal = $('#modal-menu').modal('show');    
            var that = $(this);
           // modal.find('.modal-title').html('Ingresar a Vendetumoto.co :');
            $('.modal-header').remove();
                        
            
            $('#login-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');          
            $('#register-form').attr('action','".\yii\helpers\Url::to(['site/login','origen'=>'2'])."');    
         
    });

    $('.vista').click(function(e){ 
           
        e.preventDefault();     
      
        $('#vista').val($(this).attr('id')); 
        
       
        
        
        //Creamos la instancia
        const valores = window.location.search;
        console.log(valores);
        console.log(window.location.pathname);
        
        
        const urlParams = new URLSearchParams(valores);
        
        //Accedemos a los valores
        var buscador_arriba = urlParams.get('buscador_arriba');
        
        //Verificar si existe el parámetro
        console.log(urlParams.has('buscador_arriba'));
        
        if(urlParams.has('buscador_arriba')){
            //Puedes recorrer los valores, claves y pares completos.
            const
            keys = urlParams.keys(),
            values = urlParams.values(),
            entries = urlParams.entries();
            
            for (const value of values){
               console.log(value);
               //$('#frm').attr('action',$('#frm').attr('action')+window.location.pathname+'&buscador_arriba='+value); 
                var formData=$($('#frm')).serialize()+'&buscador_arriba='+value;
                var fullUrl = window.location.href;
                var finalUrl = fullUrl+'&'+formData;
                window.location.href = finalUrl;
                return false;
            }
            $('#frm').submit();
        }else{
            $('#frm').submit();
        }  
         
    });
    


});", View::POS_END);



?>
<link href='<?= Yii::$app->request->baseUrl . '/js/slider/style.css'?>' rel='stylesheet'/>
<link href="https://rawgit.com/seiyria/bootstrap-slider/master/dist/css/bootstrap-slider.min.css" rel="stylesheet" type="text/css" />
<script src="https://rawgit.com/seiyria/bootstrap-slider/master/dist/bootstrap-slider.min.js"></script>
<style>
    .carousel-control {
        line-height:269px;
        font-size:80px !important;
        width:80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }

    #333pubpublicacion-precio_desde:first-child:after {
        color: #999;
        content: "—";
        float: left;
        font-size: 12px;
        line-height: 2px;
        margin: 0 5px;
        padding: 7px 0;
    }

    .tit_ciudad{
        font-family: Nunito;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 0.603333px;
        text-transform: uppercase;
        color: #333333;

    }

    .btn-buscar  {
        padding: 3px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 3px;
        background-color: #f33b42 !important;
        border-color: #f33b42 !important;
    }

    .select2-results,  ul > li
    {
        font-size: 10px !important;
        font-family: Nunito !important;
    }

    .field-id_ciudad{
        margin-top: 3px;
    }

    .filtro_izq{
        font-family: Nunito !important;
        font-style: normal !important;
        font-weight: normal !important;
        font-size: 13px !important;
        line-height: 18px !important;
        letter-spacing: 0.653611px !important;
        color: #B6B6B6 !important;
    }

    label,
    input[type="radio"] + span,
    input[type="radio"] + span::before,
    label,
    input[type="checkbox"] + span,
    input[type="checkbox"] + span::before
    {
        display: inline-block;
        vertical-align: middle;
    }

    label *,
    label *
    {
        cursor: pointer;
    }

    input[type="radio"],


    input[type="radio"] + span,
    input[type="checkbox"] + span
    {
        font-family: Nunito !important;
        font-style: normal !important;
        font-weight: normal !important;
        font-size: 13px !important;
        line-height: 18px !important;
        letter-spacing: 0.653611px !important;
        color: #B6B6B6 !important;
    }

    label:hover span::before,
    label:hover span::before
    {
        -moz-box-shadow: 0 0 2px #ccc;
        -webkit-box-shadow: 0 0 2px #ccc;
        box-shadow: 0 0 2px #ccc;
    }

    label:hover span,
    label:hover span
    {
        color: #000;
    }

    input[type="radio"] + span::before,
    input[type="checkbox"] + span::before
    {
        content: "";
        width: 12px;
        height: 12px;
        margin: 0 4px 0 0;
        border: solid 1px #a8a8a8;
        line-height: 14px;
        text-align: center;

        -moz-border-radius: 100%;
        -webkit-border-radius: 100%;
        border-radius: 100%;

        background: #f6f6f6;
        background: -moz-radial-gradient(#f6f6f6, #dfdfdf);
        background: -webkit-radial-gradient(#f6f6f6, #dfdfdf);
        background: -ms-radial-gradient(#f6f6f6, #dfdfdf);
        background: -o-radial-gradient(#f6f6f6, #dfdfdf);
        background: radial-gradient(#f6f6f6, #dfdfdf);
    }

    input[type="radio"]:checked + span::before,
    input[type="checkbox"]:checked + span::before
    {
        color: red;
        background: #ff00003b;
        border-color: #ff000047;
    }

    label > input[type="checkbox"]:checked + * {
        font-family: Nunito !important;
        font-style: normal !important;
        font-weight: normal !important;
        font-size: 13px !important;
        line-height: 18px !important;
        letter-spacing: 0.653611px !important;
        color: #e46a76 !important;
    }

    input[type="radio"]:disabled + span,
    input[type="checkbox"]:disabled + span
    {
        cursor: default;

        -moz-opacity: .4;
        -webkit-opacity: .4;
        opacity: .4;
    }

    input[type="checkbox"] + span::before
    {
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        border-radius: 2px;
    }

    input[type="radio"]:checked + span::before
    {
        content: "\2022";
        font-size: 30px;
        margin-top: -1px;
    }

    input[type="checkbox"]:checked + span::before
    {
        content: "\2714";
        font-size: 12px;
    }



    input[class="blue"] + span::before
    {
        border: solid 1px blue;

        background: -moz-radial-gradient(#B2DBFF, #dfdfdf);
        background: -webkit-radial-gradient(#B2DBFF, #dfdfdf);
        background: -ms-radial-gradient(#B2DBFF, #dfdfdf);
        background: -o-radial-gradient(#B2DBFF, #dfdfdf);
        background: radial-gradient(#B2DBFF, #dfdfdf);
    }
    input[class="blue"]:checked + span::before
    {
        color: darkblue;
    }



    input[class="red"] + span::before
    {
        border: solid 1px red;
        background: #FF9593;
        background: -moz-radial-gradient(#FF9593, #dfdfdf);
        background: -webkit-radial-gradient(#FF9593, #dfdfdf);
        background: -ms-radial-gradient(#FF9593, #dfdfdf);
        background: -o-radial-gradient(#FF9593, #dfdfdf);
        background: radial-gradient(#FF9593, #dfdfdf);
    }
    input[class="red"]:checked + span::before
    {
        color: darkred;
    }

    .select2-container--krajee .select2-results > .select2-results__options {
        max-height: 200px;
        overflow-y: auto;
        font-size: 10px !important;
    }

    #select2-id_ciudad-container{
        max-height: 200px;
        overflow-y: auto;
        font-size: 10px !important;
        margin-top: 4px !important;
    }

    .filtro_scroll{
        overflow: auto;
        overflow-y: auto;
        max-height: 100px;
    }

    #padre {
        position: relative;
        background-color: #fbfbfb;
        height: 213px;
    }
    .summary {
        margin-top: 4px !important;
        margin-right: 10px;
        font-family: Nunito;
    }

</style>
<div class="site-index">
    <?php $form = ActiveForm::begin(
        ['action' =>['site/resultados'], 'id' => 'frm', 'method' => 'get',]
    );



    ?>

    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 cuadro_izq" style="padding: 15px">
            <div class="row" >
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="tit_ciudad">RANGO DE PRECIO</span>
                    <span class="range">
                        <table style="margin-top: 10px;width: 100%" >
                            <tr>
                                <td>
                                    <div class="form-group " style="width: 100%">
                                        <!--input style="width: 100%" name="precio" id="slider1" type="text" class="span2 " value="" data-slider-min="0" data-slider-max="100000000" data-slider-step="500000" data-slider-value="[<?= isset($desde) ? $desde : '0' ?>,<?= isset($hasta) ? $hasta : '50000000' ?>]"/-->
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%">
                                                        <?= $form->field(new \common\models\PubPublicacion(), 'precio_desde')->textInput(['value'=>isset($desde)?$desde:'', 'oninput'=>"this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');", 'type' => 'text', 'maxlength' => 10, 'placeholder' => 'Mínimo', 'pattern'=>'[0-9]*', 'class' => 'text-sm','style' => 'width:100px;border-width:1px;    background-color: #fafafa;    border-color: #777; font-size: 12px; border-radius:7px;  height: 25px;  margin: 1px -1px;  width: 84px;   text-indent: 0.625em;   box-sizing: border-box; resize: none;', ])->label(false)?>
                                                            <?php // number_format(isset($desde) ? $desde : '0',0,',','.') ?>
                                                </td>

                                                <td style="padding-left: 2px;padding-right: 2px">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 50%; text-align: right">
                                                         <?= $form->field(new \common\models\PubPublicacion(), 'precio_hasta')->textInput(['value'=> isset($hasta) ? $hasta  : '50000000' , 'oninput'=>"this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');", 'type' => 'text', 'maxlength' => 10, 'pattern'=>'[0-9]*', 'class' => 'text-sm','style' => 'width:100px;border-width:1px;    background-color: #fafafa; border-color: #777; font-size: 12px; height: 25px; margin: 1px -1px; border-radius:7px; width: 84px;  text-indent: 0.625em; box-sizing: border-box;  resize: none;', 'placeholder' => 'Máximo'])->label(false)?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" >
                                                    <button type="submit" class="btn btn-danger btn-sm text-success pull-right btn-buscar"  style="color: #ffffff !important;">
                                                        Buscar
                                                    </button>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </span>

                </div>
            </div>
            <div class="row" style="padding-left: 12px;padding-top: 0px;padding-right: 5px">
                <span class="tit_ciudad" style="margin-bottom: 2px">Ubicación</span>
                <?php
                     $modCity = new \common\models\City();
                     if(isset($_GET['id_ciudad']) && $_GET['id_ciudad']!='')
                         $modCity->id = $_GET['id_ciudad'];
                ?>
                <?=
                    $form->field($modCity, 'id')->widget(Select2::classname(), [

                        'data' => \yii\helpers\ArrayHelper::map(\common\models\City::find()->orderBy('state_id,name')->all(), 'id', 'nombreFull3'),
                        //'value' =>(($_GET['id_ciudad']) && $_GET['id_ciudad']!='')?$_GET['id_ciudad']:'',
                        'options' => ['placeholder' => 'seleccione...', 'id'=>'id_ciudad', 'name'=>'id_ciudad'],
                        'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                        'pluginEvents' => [
                            "change" => 'function() {  $("#frm").submit();  }',
                        ]
                    ])->label(false);
                ?>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Tipo de vehículo</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php

                        foreach ($tipos as $reg){
                            echo '
                                
                                <div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                    <label>
                                        <input '.(isset($_GET['tipo'][$reg->id]) || (isset($_GET['tipo']) && $_GET['tipo']!='' && $_GET['tipo']==$reg->id) ? 'checked' : '').' type="checkbox" class="form-check-input blue" id="tipo_'.$reg->id.'" value="'.$reg->id.'" name="tipo['.$reg->id.']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="tipo_'.$reg->id.'" title="'.$reg->nombre.'">'.(strlen($reg->nombre)<15 ? substr($reg->nombre,0,15) : substr($reg->nombre,0,15).'..').'</span>
                                     </label> 
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Marca</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($marcas as $reg){
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                    <label>
                                        <input '.(isset($_GET['marca'][$reg->id]) || (isset($_GET['marca']) && $_GET['marca']!='' && $_GET['marca']==$reg->id) ? 'checked' : '').' type="checkbox" class="form-check-input" id="marca_'.$reg->id.'" value="'.$reg->id.'"  name="marca['.$reg->id.']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="marca_'.$reg->id.'" title="'.$reg->nombre.'">'.(strlen($reg->nombre)<15 ? substr($reg->nombre,0,15) : substr($reg->nombre,0,15).'..').'</span>
                                    </label>
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Modelo</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        //\common\components\Utils::dumpx($modelos);
                        foreach ($modelos as $reg){
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                    <label>
                                        <input '.(isset($_GET['modelo'][$reg->id_modelo])  || (isset($_GET['modelo']) && $_GET['modelo']!='' && $_GET['modelo']==$reg->id_modelo) ? 'checked' : '').' type="checkbox" class="form-check-input" id="modelo_'.$reg->id_modelo.'" value="'.$reg->id_modelo.'"  name="modelo['.$reg->id_modelo.']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="modelo_'.$reg->id_modelo.'" title="'.$reg->modelo.'">'.(strlen($reg->modelo)<15 ? substr($reg->modelo,0,15) : substr($reg->modelo,0,15).'..').'</span>
                                    </label> 
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Cilindraje</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($cilindrajes as $reg){
                            // \common\components\Utils::dumpx($reg);
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                    <label>
                                        <input '.(isset($_GET['cilindrajes'][$reg['cilindraje']]) ? 'checked' : '').' type="checkbox" class="form-check-input" id="cilindraje_'.$reg['cilindraje'].'" value="'.$reg['cilindraje'].'"  name="cilindrajes['.$reg['cilindraje'].']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="cilindraje_'.$reg['cilindraje'].'">'.$reg['cilindraje'].'</span>
                                     </label>
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Año</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        // Utils::dump($anos_home);
                        // Utils::dump($anos);

                        foreach ($anos as $reg){
                            $sw=0;
                            //Año actual está en el vecto de años?
                            if(count($anos_home)>0) {
                                foreach ($anos_home as $ano) {
                                    // Utils::dumpx($reg);
                                    if ($ano == $reg['ano']) { //si lo encuentra lo pinta y lo chekea
                                        $sw=1;
                                        break;
                                    }
                                }
                            }
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                     <label>
                                        <input '.(isset($_GET['anos'][$reg['ano']]) || $sw==1 ? 'checked' : '').' type="checkbox" class="form-check-input" id="ano_'.$reg['ano'].'" value="'.$reg['ano'].'"  name="anos['.$reg['ano'].']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="ano_'.$reg['ano'].'">'.$reg['ano'].'</span>
                                     </label>
                                  </div>';


                        }




                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Vendedor</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($vendedores as $reg){
                            //\common\components\Utils::dumpx($reg);
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                    <label>
                                        <input '.(isset($_GET['vendedores'][$reg['tipo_vendedor']]) ? 'checked' : '').' type="checkbox" class="form-check-input" id="vendedor_'.$reg['tipo_vendedor'].'" value="'.$reg['tipo_vendedor'].'" name="vendedores['.$reg['tipo_vendedor'].']" style="color: #B6B6B6;font-size: 12px">
                                        <span class="form-check-label " for="vendedor_'.$reg['tipo_vendedor'].'" title="'.$reg['vendedor'].'">'.(strlen($reg['vendedor'])<15 ? substr($reg['vendedor'],0,15) : substr($reg['vendedor'],0,15).'..').'</span>
                                    </label> 
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Condición</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($condicion as $reg){
                            // \common\components\Utils::dumpx($reg);
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                     <label>
                                            <input '.(isset($_GET['condiciones'][$reg['tipo_condicion']]) ? 'checked' : '').' type="checkbox" class="form-check-input" id="condicion_'.$reg['tipo_condicion'].'" value="'.$reg['tipo_condicion'].'"  name="condiciones['.$reg['tipo_condicion'].']" style="color: #B6B6B6;font-size: 12px">
                                            <span class="form-check-label " for="condicion_'.$reg['tipo_condicion'].'" title="'.$reg['condicion'].'">'.(strlen($reg['condicion'])<15 ? substr($reg['condicion'],0,15) : substr($reg['condicion'],0,15).'..').'</span>
                                     </label>
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">Recorrido</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($recorridos as $reg){
                            //\common\components\Utils::dumpx($reg);
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                <label>
                                    <input '.(isset($_GET['recorridos'][$reg['recorrido'].'_'.$reg['unid_recorrido']]) ? 'checked' : '').' type="checkbox" class="form-check-input" id="recorrido_'.$reg['recorrido'].'_'.$reg['unid_recorrido'].'" value="'.$reg['recorrido'].'_'.$reg['unid_recorrido'].'"  name="recorridos['.$reg['recorrido'].'_'.$reg['unid_recorrido'].']" style="color: #B6B6B6;font-size: 12px">
                                    <span class="form-check-label " for="recorrido_'.$reg['recorrido'].'_'.$reg['unid_recorrido'].'" title="'.$reg['recorrido'].' '.$reg['str_recorrido'].'">'.(strlen($reg['recorrido'].' '.$reg['str_recorrido'])<15 ? substr( $reg['str_recorrido'],0,15) : substr( $reg['str_recorrido'],0,15).'..').'</span>
                                </label>     
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <hr class="linea"/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span class="res_subtitulo">OTRAS CARACTERÍSTICAS</span>
                    <div class="filtro_scroll" style="padding-top: 4px;padding-left: 4px">
                        <?php
                        foreach ($otras_car as $reg){
                            //\common\components\Utils::dumpx($reg);
                            echo '<div class="form-check" style="color: #B6B6B6;font-size: 12px">
                                     <label>
                                        <input '.(isset($_GET['otras'][$reg['id']]) ? 'checked' : '').' type="checkbox" class="form-check-input" id="otras_cars_'.$reg['id'].'" value="'.$reg['id'].'"  name="otras['.$reg['id'].']" style="color: #B6B6B6;font-size: 12px">
                                         <span class="form-check-label " for="otras_cars_'.$reg['id'].'" title="'.$reg['caracteristica'].'">'.(strlen($reg['caracteristica'])<15 ? substr($reg['caracteristica'],0,15) : substr($reg['caracteristica'],0,15).'..').'</span>
                                     </label>
                                  </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-10 col-md-10 col-sm-12 " >
            <div class="row" style=" " >
                <center class="col-lg-12 col-md-12 col-sm-12  cuadro_izq" style="background: transparent">
                    <img class="img-fluid img-responsive" src="<?= Yii::$app->request->baseUrl . '/img/banner-search.jpg'?>" style="width: 100%">
                </center>
            </div>

<div class="row">

    <div  style="padding:10px" >
        <div class="col-lg-12 col-md-12 col-sm-12  cuadro_izq" style="padding-left: 10px; padding-top: 10px; " >
            <div class="col-lg-3 col-md-3 col-sm-12" >
                <?php
                echo $form->field( $model, 'orden')->widget(Select2::classname(), [
                        'data' => $model->_orden,
                        'options' => ['placeholder' => 'Ordenar por precio...'],
                        'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                        'pluginEvents' => [
                            "change" => 'function() {  $("#frm").submit();   }',
                        ],
                    ])->label(false);
                ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12" >
                <?php
                    echo $form->field($model, 'items_x_pag')->widget(Select2::classname(), [
                        'data' => $model->_items_x_pag,
                        'options' => ['placeholder' => 'Anuncios por página'],
                        'pluginOptions' => ['allowClear' => true,'style' => 'color:red'],
                        'pluginEvents' => [
                            "change" => 'function() {  $("#frm").submit();  }',
                         ],
                    ])->label(false)
                ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-right" >
                <div class="btn-toolbar text-right" role="toolbar">

                    <div class="btn-group text-right" style="float: right">
                        <table style="width: 100%">
                            <tr>
                                <td style="    margin-top: 8px;    padding-right: 5px;font-family: 'Source Sans Pro';font-style: normal;font-weight: 700;font-size: 14px;line-height: 18px;display: flex;align-items: center;text-align: right;letter-spacing: 0.0278198px;color: #353435;">Vistas: </td>
                                <td><button type="submit" class="btn btn-default btn-sm vista" id="3"  title="Ver en 3 Columnas"><i style=" color: #F33B42;font-size: 20px " class="glyphicon glyphicon-th glyphicon-th-large"></i></button></td>
                                <td><button type="submit" class="btn btn-default btn-sm vista" id="2"  title="Ver en 2 Columnas"><i style=" color: #F33B42;font-size: 20px  " class="glyphicon glyphicon-th-large"></i></button></td>
                                <td><button type="submit" class="btn btn-default btn-sm vista" id="1"  title="Ver en Listado"><i style=" color: #F33B42;font-size: 20px  " class="glyphicon glyphicon-menu-hamburger"></i></button></td>
                            </tr>
                        </table>



                    </div>
                </div>
            </div>

        </div>
        <input type="hidden" id="vista" name="PubPublicacion[vista]" value="<?= (isset($_GET['PubPublicacion']['vista'])) ? $_GET['PubPublicacion']['vista'] : 3 ?> ">

    </div>


</div>
            <!-- RESULTADOS -->

<style>
    .btntel {
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 21px;
        text-align: center;
        letter-spacing: 0.54216px;
        color: #F33B42;
        border: 1px solid #F33B42;
        width: 100%;
        padding: 10px;
        box-sizing: border-box;
        border-radius: 25px;
    }
    .btnwp {
        font-family: Montserrat;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 21px;
        text-align: center;
        letter-spacing: 0.54216px;
        color: #64B161;
        border: 1px solid #64B161;
        width: 100%;
        padding: 10px;
        box-sizing: border-box;
        border-radius: 25px;
    }
    .ove {
        min-height: 212px !important;
        text-align: center !important;
        width: 100% !important;
    }
    .carousel-inner {
        position: relative;
        width: 100%;
        overflow: hidden;
        display: flex;
        justify-content: center;
    }
    .carousel-control {
        line-height: 213px;
        font-size: 54px !important;
        width: 80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }

    #padre2 {
        position: relative;
        background-color: #fbfbfb;
        max-height: 178px;
    }
    .ove2 {
        min-height: 160px !important;
        text-align: center !important;
        width: 100% !important;
    }

    .ove2 a{
        line-height: 156px;
        font-size: 54px !important;
        width: 80px !important;
        color: #ffffff !important;
        font-weight: bold !important;
    }
</style>
<?php



  if(isset($_GET['PubPublicacion']['vista'])){
      //1 COLUMNA: lista
      if($_GET['PubPublicacion']['vista']==1){


          echo ListView::widget([
              'dataProvider' => $dataProvider,
              'options' => ['tag' => 'div','class' => 'row2', 'id' => 'list-group',  ],
               'layout' => '<div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                            <div class="row" style="margin-top: 15px">{items}</div>                        
                            <div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                            ',
              'summary' => 'Mostrando <b>{begin}-{end}</b> de <b>{totalCount}</b> resultados.' ,

              'itemView' => function ($model, $key, $index, $widget) {
                  //\common\components\Utils::dumpx($model->id);
                  $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
                  $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();
                  $str = '';
                  date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');

                  $img1=[];
                  $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->limit(1)->one();
                  $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->all();
                  foreach ($fotos1 as $foto){
                      $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$model->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 159px;  display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                  }

                  if($foto_primera)
                      $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove2', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                  //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                  else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';

                  //\common\components\Utils::dumpx($widget);
                  //0 3   6   9   12  15  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63
                  if($index=='0' ||  $index=='3' || $index=='6' || $index=='9' || $index=='12' || $index=='15' || $index=='18' || $index=='21' || $index=='24' || $index=='27' ||
                      $index=='30' || $index=='33' || $index=='36' || $index=='39' || $index=='42' || $index=='45' || $index=='48' || $index=='51' || $index=='54' || $index=='57' || $index=='60'){
                      //  $str = '<div class="row"  >';
                  }

                  echo '<p>&nbsp;</p>
                        <div class="row" style="padding-left: 20px;padding-right: 20px"  >
                            <div class="col-sm-12 col-md-12 col-lg-12 ">            
                                <div class="row" style="border-radius: 8px;border: 1px ;min-height: 140px;background: #FFFFFF;box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.2);">
                
                                    <div id="padre2" class="col-md-3 col-lg-3 col-sm-12" style="margin-top: -1px;padding: 10px;;display: flex;justify-content: center;">
                                        '.$imgenes1.'
                                    </div>
                                    <div class=" col-md-6 col-lg-6 col-sm-12" style="margin-top: 12px;">
                                        <b style="float: right;font-size: 12px" >ID # '.$model->id.'</b>
                                        <div style="width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 20px;line-height: 21px;letter-spacing: 0.637836px;color: #474747;">
                                         '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.' dddddddddd
                                         
                                        </div>
                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                            '.number_format($model->ano,0,',','.').' -  '.number_format($model->recorrido,0,',','.').$model->_unid_recorrido[$model->unid_recorrido].' 
                                        </div>
                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: 600;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #F33B42;">
                                            $'.number_format($model->precio,0,',','.').'
                                        </div>
                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 16px;line-height: 20px;letter-spacing: 0.510269px;color: #474747;">
                                            '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3 padre" style="margin-top: 0px;border-left: 1px solid #EAEAEA;padding-top: 10px;min-height: 140px;">';

                                          if(!Yii::$app->user->isGuest){
                                              $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                                              //Si No es Favorito: Pinta Corazón Blanco
                                              if($exist_fav==0){
                                                  echo '<div id="fav_'.$model->id.'" style="z-index: 1500;right: -22px"  class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                           <div class="vertical-timeline-icon navy-bg2  ">
                                                               <i class="fa fa-heart-o"></i>
                                                           </div>
                                                        </div>';
                                              }else{
                                                  echo  '<div id="fav_'.$model->id.'" style="z-index: 1500;right: -22px" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                           <div class="vertical-timeline-icon navy-bg danger ">
                                                               <i class="fa fa-heart"></i>
                                                           </div>
                                                         </div>';

                                              }
                                          }else{
                                              echo ' <div id="fav_'.$model->id.'" style="z-index: 1500;right: -22px" class="favorito btn_favorito" logueado="0" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id:'').'">
                                                        <div class="vertical-timeline-icon navy-bg2  ">
                                                            <i class="fa fa-heart-o"></i>
                                                        </div>
                                                    </div>';
                                          }


                                        echo ' <!-- botones -->
                                        <div style="margin-top:10px;width: 100%;font-family: Montserrat;font-style: normal;font-weight: normal;font-size: 14px;line-height: 17px;letter-spacing: 0.558106px;color: #7D7D7D;">
                                            <a style="background: #F33B42;border-radius: 25px;display: block"   href="'.Url::to(['site/pubdetallepublico','id'=> $model->id,'#'=>'pregunta']).'" class="btn btn-danger">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Preguntar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </a>
                                        </div>
                                        <p style="text-align: center; margin-top: 10px">
                                            <button  type="button" class="btn btn-default btntel" >';
                                                    $cliuser = \common\models\CliUsuario::find()->where('id_usuario='.$model->id_usuario)->one();
                                                    //\common\components\Utils::dump($cliuser->attributes);
                                                    if(isset($cliuser->whatssapp) && $cliuser->whatssapp!=''){
                                                        if (!Yii::$app->user->isGuest) {
                                                            echo '(+57) '.$cliuser->whatssapp;
                                                        }else{
                                                            echo '(+57) '.substr($cliuser->whatssapp,0,strlen($cliuser->whatssapp)-7).'*******';
                                                        }
                                                    }elseif(isset($cliuser->telefonos) && $cliuser->telefonos!=''){
                                                        echo substr($cliuser->telefonos,0,strlen($cliuser->telefonos)-5).'*******';
                                                    }else echo '<em style="color: #e46a76">- sin números-</em>';
                                     echo ' </button>
                                         </p>
                                         <p style="text-align: center; margin-top: 10px" >';
                                               if (!Yii::$app->user->isGuest) {
                                                  $url_self = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                                  $url_home = 'https://api.whatsapp.com/send?phone=573164138276&text=Quiero más información de '.$model->modelo->marca->nombre.'   '.$model->modelo->nombre.' publicada en *Vende Tu Moto* &'.$url_self;

                                                    echo '<a target="_blank" href="'.$url_home.'"  class="btn btn-default btnwp" style="">
                                                              Escribe vía WhatsApp
                                                          </a>';
                                                }else{
                                                    echo ' <button target="_blank"  class="btn btn-default btnwp" style="">
                                                            Escribe vía WhatsApp
                                                        </button>';
                                                }
                                    echo ' </p>
                                        
                                    </div>
                                </div>
                            </div>
                       </div>';

/*

                  $str .= ' <div class="col-lg-6 col-md-6 col-sm-12 inicio_cuadro" > 
                                       <div class="ibox product-box">
                                           
                                               <div id="padre" class="ibox-content no-padding border-left-right" style="display: flex;justify-content: center;">
                                                  '. $img_url .'
                                                   <div class="reco_cuadro">
                                                       <span class="reco_texto">'.number_format($model->recorrido,0,',','.').' '.($model->_unid_recorrido[$model->unid_recorrido]).'</span>
                                                   </div>';

                  if($model->descuento>0){
                      $str .=  '<div class="desc_cuadro" style=" cursor: pointer;" title="Tiene '.$model->descuento.'% de descuento">
                                                                 <span class="desc_texto">- '. $model->descuento .'%</span>
                                                              </div>';
                  }
                  //SI usuario logueado
                  if(!Yii::$app->user->isGuest){
                      $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                      //Si No es Favorito: Pinta Corazón Blanco
                      if($exist_fav==0){
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg2  ">
                                                                           <i class="fa fa-heart-o"></i>
                                                                       </div>
                                                                  </div>';
                      }else{
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg danger ">
                                                                           <i class="fa fa-heart"></i>
                                                                       </div>
                                                                  </div>';
                      }
                  }

                  $str .= '          </div>
      
                                       <div class="ibox-content profile-content">
                                           <h4><strong class="font_precio_mons" style="font-size: 19px">'.(($model->descuento > 0) ? 'Ahora':'').' $'.(($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))) ,0,',','.') : number_format($model->precio,0,',','.')).' </strong></h4>
                                           <p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').' > '. (($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>').' </p>
                                           <p class="font-14" style="font-size: 12px">'. $model->ano.' - '. number_format($model->recorrido,0,',','.').'<span style="font-size: 11px">'.($model->_unid_recorrido[$model->unid_recorrido]).'</span></p>
                                           <p>
                                           <a title="Click para ir a la publicación..." href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"  class="font-15" style="font-size: 15px; text-decoration:none;color:#474747">
                                               '.$model->tipo->nombre.' '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'
                                           </a>
                                           </p>
                                           <p class="font-14" style="font-size: 12px">
                                               '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                           </p>
        
                                                 
                                       </div>
                                   
                               </div> <!-- ibox product-box  -->
                           </div> <!-- inicio_cuadro  -->
                            ';

                  if($index=='2' || $index=='5' || $index=='8' || $index=='11' || $index=='14' || $index=='17' || $index=='20' || $index=='23' || $index=='26' ||
                      $index=='29' || $index=='32' || $index=='35' || $index=='38' || $index=='41' || $index=='44' || $index=='47' || $index=='50' || $index=='53' || $index=='56' || $index=='59'){
                      $str .= '</div><div class="row"  > '; //row fin
                  }
*/
                  return $str;
              },
              'pager' => [
                  'firstPageLabel' => 'Inicio',
                  'lastPageLabel' => 'Fin',
                  'nextPageLabel' => '>>',
                  'prevPageLabel' => '<<',
                  'maxButtonCount' => 10,
              ],
          ]);
      }else

      if($_GET['PubPublicacion']['vista']==2){
          echo ListView::widget([
              'dataProvider' => $dataProvider,
              //'itemOptions' => ['class' => 'col-lg-4 col-md-4 col-sm-4'],
              'options' => [
                  'tag' => 'div',
                  'class' => 'row2',
                  'id' => 'list-group',
              ],
              'itemOptions' => [
                  'tag' => false,
              ],


              'layout' => '<div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                 <div class="row" style="margin-top: 15px">{items}</div>                          
                                 <div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                ',
              'summary' =>
                  'Mostrando <b>{begin}-{end}</b> de <b>{totalCount}</b> resultados.'
              ,
              'itemView' => function ($model, $key, $index, $widget) {
                  //\common\components\Utils::dumpx($model->id);
                  $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
                  $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();
                  $str = '';
                  date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');

                  $img1=[];
                  $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->limit(1)->one();
                  $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->all();
                  foreach ($fotos1 as $foto){
                      $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$model->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 213px;  display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                  }

                  if($foto_primera)
                      $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                  //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                  else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';


                  $str .= ' <div class="col-lg-6 col-md-6 col-sm-12 inicio_cuadro" > 
                                       <div class="ibox product-box">
                                           
                                               <div id="padre" class="ibox-content no-padding border-left-right" style="display: flex;justify-content: center;">
                                                  '. $imgenes1 .'
                                                   <div class="reco_cuadro">
                                                       <span class="reco_texto">'.number_format($model->recorrido,0,',','.').' '.($model->_unid_recorrido[$model->unid_recorrido]).'</span>
                                                   </div>';

                  if($model->descuento>0){
                      $str .=  '<div class="desc_cuadro" style=" cursor: pointer;" title="Tiene '.$model->descuento.'% de descuento">
                                                                 <span class="desc_texto">- '. $model->descuento .'%</span>
                                                              </div>';
                  }
                  //SI usuario logueado
                  if(!Yii::$app->user->isGuest){
                      $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                      //Si No es Favorito: Pinta Corazón Blanco
                      if($exist_fav==0){
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg2  ">
                                                                           <i class="fa fa-heart-o"></i>
                                                                       </div>
                                                                  </div>';
                      }else{
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg danger ">
                                                                           <i class="fa fa-heart"></i>
                                                                       </div>
                                                                  </div>';
                      }
                  }else{
                      $str .= ' <div id="fav_'.$model->id.'"   class="favorito btn_favorito" logueado="0" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id:'').'">
                                                        <div class="vertical-timeline-icon navy-bg2  ">
                                                            <i class="fa fa-heart-o"></i>
                                                        </div>
                                                    </div>';
                  }

                  $str .= '          </div>
      
                                       <div class="ibox-content profile-content">
                                           <h4><strong class="font_precio_mons" style="font-size: 19px">'.(($model->descuento > 0) ? 'Ahora':'').' $'.(($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))) ,0,',','.') : number_format($model->precio,0,',','.')).' </strong></h4>';
                                           /*<!--p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').' > '. (($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>').' </p-->*/
                                           if($model->descuento >0){
                                              $str .= '             <p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').'>'. ($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' .'</p>';
                                           }
                                $str .= '           <p class="font-14" style="font-size: 12px">'. $model->ano.' - '. number_format($model->recorrido,0,',','.').'<span style="font-size: 11px">'.($model->_unid_recorrido[$model->unid_recorrido]).'</span></p>
                                           <p>
                                           <a title="Click para ir a la publicación..." href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"  class="font-15" style="font-size: 15px; text-decoration:none;color:#474747">
                                                 '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'
                                           </a>
                                           </p>
                                           <p class="font-14" style="font-size: 12px">
                                               '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                           </p>
        
                                                 
                                       </div>
                                   
                               </div> <!-- ibox product-box  -->
                           </div> <!-- inicio_cuadro  -->
                            ';

                  if($index=='1' || $index=='3' || $index=='5' || $index=='7' || $index=='9' || $index=='11' || $index=='13' || $index=='15' || $index=='17' ||
                      $index=='19' || $index=='21' || $index=='23' || $index=='25' || $index=='27' || $index=='29' || $index=='31' || $index=='33' || $index=='35' || $index=='37'
                      || $index=='39'){
                      $str .= '</div><div class="row"  > '; //row fin
                  }

                  return $str;
              },
              'pager' => [
                  'firstPageLabel' => 'Inicio',
                  'lastPageLabel' => 'Fin',
                  'nextPageLabel' => '>>',
                  'prevPageLabel' => '<<',
                  'maxButtonCount' => 10,
              ],
          ]);
      }else

      if($_GET['PubPublicacion']['vista']==3){
          echo ListView::widget([
              'dataProvider' => $dataProvider,
              //'itemOptions' => ['class' => 'col-lg-4 col-md-4 col-sm-4'],
              'options' => [
                  'tag' => 'div',
                  'class' => 'row2',
                  'id' => 'list-group',
              ],
              'itemOptions' => [
                  'tag' => false,
              ],


              'layout' => '<div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                 <div class="row" style="margin-top: 15px">{items}</div>                          
                                 <div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                ',
              'summary' =>
                  'Mostrando <b>{begin}-{end}</b> de <b>{totalCount}</b> resultados.'
              ,
              'itemView' => function ($model, $key, $index, $widget) {
                  //\common\components\Utils::dumpx($model->id);
                  $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
                  $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();
                  $str = '';
                  date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');


                  $img1=[];
                  $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->limit(1)->one();
                  $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->all();
                  foreach ($fotos1 as $foto){
                      $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$model->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 213px; display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
                  }

                  if($foto_primera)
                      $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
                  //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
                  else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';




                  //\common\components\Utils::dumpx($widget);
                  //0 3   6   9   12  15  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63
                  if($index=='0' ||  $index=='3' || $index=='6' || $index=='9' || $index=='12' || $index=='15' || $index=='18' || $index=='21' || $index=='24' || $index=='27' ||
                      $index=='30' || $index=='33' || $index=='36' || $index=='39' || $index=='42' || $index=='45' || $index=='48' || $index=='51' || $index=='54' || $index=='57' || $index=='60'){
                      //  $str = '<div class="row"  >';
                  }

                  $str .= ' <div class="col-lg-4 col-md-4 col-sm-4 inicio_cuadro" > 
                                       <div class="ibox product-box">
                                           
                                               <div id="padre" class="ibox-content no-padding border-left-right" style="display: flex;justify-content: center;">
                                                  '. $imgenes1 .'
                                                   <div class="reco_cuadro">
                                                       <span class="reco_texto">'.number_format($model->recorrido,0,',','.').' '.($model->_unid_recorrido[$model->unid_recorrido]).'</span>
                                                   </div>';

                  if($model->descuento>0){
                      $str .=  '<div class="desc_cuadro" style=" cursor: pointer;" title="Tiene '.$model->descuento.'% de descuento">
                                                                 <span class="desc_texto">- '. $model->descuento .'%</span>
                                                              </div>';
                  }
                  //SI usuario logueado
                  if(!Yii::$app->user->isGuest){
                      $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                      //Si No es Favorito: Pinta Corazón Blanco
                      if($exist_fav==0){
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg2  ">
                                                                           <i class="fa fa-heart-o"></i>
                                                                       </div>
                                                                  </div>';
                      }else{
                          $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg danger ">
                                                                           <i class="fa fa-heart"></i>
                                                                       </div>
                                                                  </div>';
                      }
                  }else{
                      $str .= ' <div id="fav_'.$model->id.'"   class="favorito btn_favorito" logueado="0" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id:'').'">
                                    <div class="vertical-timeline-icon navy-bg2  ">
                                        <i class="fa fa-heart-o"></i>
                                    </div>
                                </div>';
                  }

                  $str .= '          </div>
      
                                       <div class="ibox-content profile-content">
                                           <h4><strong class="font_precio_mons" style="font-size: 19px">'.(($model->descuento > 0) ? 'Ahora':'').' $'.(($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))) ,0,',','.') : number_format($model->precio,0,',','.')).' </strong></h4>';
                                           /*<p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').' > '. (($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>').' </p>*/
                                            if($model->descuento >0){
                                              $str .= ' <p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').'>'. ($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' .'</p>';
                                           }

                  $str .= '  <p class="font-14" style="font-size: 12px">'. $model->ano.' - '. number_format($model->recorrido,0,',','.').'<span style="font-size: 11px">'.($model->_unid_recorrido[$model->unid_recorrido]).'</span></p>
                                           <p>
                                           <a title="Click para ir a la publicación..." href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"  class="font-15" style="font-size: 15px; text-decoration:none;color:#474747">
                                               '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'
                                           </a>
                                           </p>
                                           <p class="font-14" style="font-size: 12px">
                                               '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                           </p>
        
                                                 
                                       </div>
                                   
                               </div> <!-- ibox product-box  -->
                           </div> <!-- inicio_cuadro  -->
                            ';

                  if($index=='2' || $index=='5' || $index=='8' || $index=='11' || $index=='14' || $index=='17' || $index=='20' || $index=='23' || $index=='26' ||
                      $index=='29' || $index=='32' || $index=='35' || $index=='38' || $index=='41' || $index=='44' || $index=='47' || $index=='50' || $index=='53' || $index=='56' || $index=='59'){
                      $str .= '</div><div class="row"  > '; //row fin
                  }

                  return $str;
              },
              'pager' => [
                  'firstPageLabel' => 'Inicio',
                  'lastPageLabel' => 'Fin',
                  'nextPageLabel' => '>>',
                  'prevPageLabel' => '<<',
                  'maxButtonCount' => 10,
              ],
          ]);
      }

  }else{
      echo ListView::widget([
          'dataProvider' => $dataProvider,
          //'itemOptions' => ['class' => 'col-lg-4 col-md-4 col-sm-4'],
          'options' => [
              'tag' => 'div',
              'class' => 'row2',
              'id' => 'list-group',
          ],
          'itemOptions' => [
              'tag' => false,
          ],


          'layout' => '<div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                 <div class="row" style="margin-top: 15px">{items}</div>                          
                                 <div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                ',
          'summary' =>
              'Mostrando <b>{begin}-{end}</b> de <b>{totalCount}</b> resultados.'
          ,
          'itemView' => function ($model, $key, $index, $widget) {
              //\common\components\Utils::dumpx($model->id);
              $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
              $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();
              $str = '';
              date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');


              $img1=[];
              $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->limit(1)->one();
              $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->all();
              foreach ($fotos1 as $foto){
                  $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$model->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 213px; display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
              }

              if($foto_primera)
                  $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
              //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
              else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';




              //\common\components\Utils::dumpx($widget);
              //0 3   6   9   12  15  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63
              if($index=='0' ||  $index=='3' || $index=='6' || $index=='9' || $index=='12' || $index=='15' || $index=='18' || $index=='21' || $index=='24' || $index=='27' ||
                  $index=='30' || $index=='33' || $index=='36' || $index=='39' || $index=='42' || $index=='45' || $index=='48' || $index=='51' || $index=='54' || $index=='57' || $index=='60'){
                  //  $str = '<div class="row"  >';
              }

              $str .= ' <div class="col-lg-4 col-md-4 col-sm-4 inicio_cuadro" > 
                                       <div class="ibox product-box">
                                           
                                               <div id="padre" class="ibox-content no-padding border-left-right" style="display: flex;justify-content: center;">
                                                  '. $imgenes1 .'
                                                   <div class="reco_cuadro">
                                                       <span class="reco_texto">'.number_format($model->recorrido,0,',','.').' '.($model->_unid_recorrido[$model->unid_recorrido]).'</span>
                                                   </div>';

              if($model->descuento>0){
                  $str .=  '<div class="desc_cuadro" style=" cursor: pointer;" title="Tiene '.$model->descuento.'% de descuento">
                                                                 <span class="desc_texto">- '. $model->descuento .'%</span>
                                                              </div>';
              }
              //SI usuario logueado
              if(!Yii::$app->user->isGuest){
                  $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                  //Si No es Favorito: Pinta Corazón Blanco
                  if($exist_fav==0){
                      $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg2  ">
                                                                           <i class="fa fa-heart-o"></i>
                                                                       </div>
                                                                  </div>';
                  }else{
                      $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg danger ">
                                                                           <i class="fa fa-heart"></i>
                                                                       </div>
                                                                  </div>';
                  }
              }else{
                  $str .= ' <div id="fav_'.$model->id.'"   class="favorito btn_favorito" logueado="0" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id:'').'">
                                    <div class="vertical-timeline-icon navy-bg2  ">
                                        <i class="fa fa-heart-o"></i>
                                    </div>
                                </div>';
              }

              $str .= '          </div>
      
                                       <div class="ibox-content profile-content">
                                           <h4><strong class="font_precio_mons" style="font-size: 19px">'.(($model->descuento > 0) ? 'Ahora':'').' $'.(($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))) ,0,',','.') : number_format($model->precio,0,',','.')).' </strong></h4>';
                                           /*<p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').' > '. (($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>').' </p>*/
                                           if($model->descuento >0){
                                              $str .= ' <p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').'>'. ($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>' .'</p>';
                                           }
              $str .= '           <p class="font-14" style="font-size: 12px">'. $model->ano.' - '. number_format($model->recorrido,0,',','.').'<span style="font-size: 11px">'.($model->_unid_recorrido[$model->unid_recorrido]).'</span></p>
                                           <p>
                                           <a title="Click para ir a la publicación..." href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"  class="font-15" style="font-size: 15px; text-decoration:none;color:#474747">
                                               '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'
                                           </a>
                                           </p>
                                           <p class="font-14" style="font-size: 12px">
                                               '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                           </p>
        
                                                 
                                       </div>
                                   
                               </div> <!-- ibox product-box  -->
                           </div> <!-- inicio_cuadro  -->
                            ';

              if($index=='2' || $index=='5' || $index=='8' || $index=='11' || $index=='14' || $index=='17' || $index=='20' || $index=='23' || $index=='26' ||
                  $index=='29' || $index=='32' || $index=='35' || $index=='38' || $index=='41' || $index=='44' || $index=='47' || $index=='50' || $index=='53' || $index=='56' || $index=='59'){
                  $str .= '</div><div class="row"  > '; //row fin
              }

              return $str;
          },
          'pager' => [
              'firstPageLabel' => 'Inicio',
              'lastPageLabel' => 'Fin',
              'nextPageLabel' => '>>',
              'prevPageLabel' => '<<',
              'maxButtonCount' => 10,
          ],
      ]);
      /*
      echo ListView::widget([
          'dataProvider' => $dataProvider,
          //'itemOptions' => ['class' => 'col-lg-4 col-md-4 col-sm-4'],
          'options' => [
              'tag' => 'div',
              'class' => 'row2',
              'id' => 'list-group',
          ],
          'itemOptions' => [
              'tag' => false,
          ],


          'layout' => '<div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                 <div class="row" style="margin-top: 15px">{items}</div>
                                 <div style="display: flex;justify-content: space-between;"><span class="pull-left">{summary}</span> <span class="right">{pager}</span></div>
                                ',
          'summary' =>
              'Mostrando <b>{begin}-{end}</b> de <b>{totalCount}</b> resultados.'
          ,
          'itemView' => function ($model, $key, $index, $widget) {
              //\common\components\Utils::dumpx($model->id);
              $model = \common\models\PubPublicacion::find()->where('id='.$model->id)->one();
              $foto = \common\models\PubMultimedia::find()->where('id_publicacion='.$model->id)->orderBy('id ASC')->one();
              $str = '';
              date_default_timezone_set('America/Bogota');  setlocale(LC_TIME, 'es_ES.UTF-8');  setlocale(LC_TIME, 'spanish');


              $img1=[];
              $foto_primera = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->limit(1)->one();
              $fotos1 = \common\models\PubMultimedia::find()->where('tipo=1 and id_publicacion=' . $model->id)->orderBy('id ASC')->all();
              foreach ($fotos1 as $foto){
                  $img1[] ='<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$model->carpeta.'/'.$foto->archivo.'" class="img-fluid img-responsive center"  style=" max-height: 213px; display: block;    width: auto;    height: auto;cursor:pointer; " title="Click para ver publicación"  /></a>';
              }

              if($foto_primera)
                  $imgenes1 =  yii\bootstrap\Carousel::widget(['items' => $img1, 'options' => ['class' => 'ove', 'data-interval'=>"false", 'data-ride'=>"carousel", 'data-pause'=>"hover"]]);
              //$img_url = '<a target="_blank"  href="'.Url::to(['site/pubdetallepublico','id'=> $item->id]).'"> <img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="https://vtmprod.s3.us-east-2.amazonaws.com/pubs/'.$item->carpeta.'/'.$foto_primera->archivo.'"></a>';
              else  $imgenes1 = '<a href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"><img alt="image" style=" display: block; max-width: 100%; max-height: 100%; width: auto; height: auto;" class="img-fluid img-responsive center" src="'.Yii::$app->request->baseUrl.'/img/no-image-moto.png'.'"></a>';




              //\common\components\Utils::dumpx($widget);
              //0 3   6   9   12  15  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63
              if($index=='0' ||  $index=='3' || $index=='6' || $index=='9' || $index=='12' || $index=='15' || $index=='18' || $index=='21' || $index=='24' || $index=='27' ||
                  $index=='30' || $index=='33' || $index=='36' || $index=='39' || $index=='42' || $index=='45' || $index=='48' || $index=='51' || $index=='54' || $index=='57' || $index=='60'){
                  //  $str = '<div class="row"  >';
              }

              $str .= ' <div class="col-lg-4 col-md-4 col-sm-4 inicio_cuadro" >
                                       <div class="ibox product-box">

                                               <div id="padre" class="ibox-content no-padding border-left-right" style="display: flex;justify-content: center;">
                                                  '. $imgenes1 .'
                                                   <div class="reco_cuadro">
                                                       <span class="reco_texto">'.number_format($model->recorrido,0,',','.').' '.($model->_unid_recorrido[$model->unid_recorrido]).'</span>
                                                   </div>';

              if($model->descuento>0){
                  $str .=  '<div class="desc_cuadro" style=" cursor: pointer;" title="Tiene '.$model->descuento.'% de descuento">
                                                                 <span class="desc_texto">- '. $model->descuento .'%</span>
                                                              </div>';
              }
              //SI usuario logueado
              if(!Yii::$app->user->isGuest){
                  $exist_fav = PubFavoritos::find()->where('id_publicacion='.$model->id.' and id_usuario='.Yii::$app->user->identity->id)->count();
                  //Si No es Favorito: Pinta Corazón Blanco
                  if($exist_fav==0){
                      $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg2  ">
                                                                           <i class="fa fa-heart-o"></i>
                                                                       </div>
                                                                  </div>';
                  }else{
                      $str .=  '<div id="fav_'.$model->id.'" class="favorito btn_favorito" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id: '').'">
                                                                       <div class="vertical-timeline-icon navy-bg danger ">
                                                                           <i class="fa fa-heart"></i>
                                                                       </div>
                                                                  </div>';
                  }
              }else{
                  $str .= ' <div id="fav_'.$model->id.'"   class="favorito btn_favorito" logueado="0" id_pub="'.$model->id.'" id_usuario="'.((!Yii::$app->user->isGuest) ? Yii::$app->user->identity->id:'').'">
                                    <div class="vertical-timeline-icon navy-bg2  ">
                                        <i class="fa fa-heart-o"></i>
                                    </div>
                                </div>';
              }

              $str .= '          </div>

                                       <div class="ibox-content profile-content">
                                           <h4><strong class="font_precio_mons" style="font-size: 19px">'.(($model->descuento > 0) ? 'Ahora':'').' $'.(($model->descuento > 0) ? number_format(($model->precio - ($model->precio*($model->descuento/100))) ,0,',','.') : number_format($model->precio,0,',','.')).' </strong></h4>
                                           <p class="sub2" '.(($model->descuento == 0) ? 'style="text-decoration-line: none !important;"' : '').' > '. (($model->descuento > 0) ? 'Antes $'.number_format($model->precio,0,',','.') : '<span style="color:#fff;">&nbsp;</span>').' </p>
                                           <p class="font-14" style="font-size: 12px">'. $model->ano.' - '. number_format($model->recorrido,0,',','.').'<span style="font-size: 11px">'.($model->_unid_recorrido[$model->unid_recorrido]).'</span></p>
                                           <p>
                                           <a title="Click para ir a la publicación..." href="'.Url::to(['site/pubdetallepublico','id'=> $model->id]).'"  class="font-15" style="font-size: 15px; text-decoration:none;color:#474747">
                                                '.$model->modelo->marca->nombre.' '.$model->modelo->nombre.'
                                           </a>
                                           </p>
                                           <p class="font-14" style="font-size: 12px">
                                               '.ucfirst(strtolower($model->ciudad->name)).', '.ucfirst(strtolower($model->ciudad->state->name)).'
                                           </p>


                                       </div>

                               </div> <!-- ibox product-box  -->
                           </div> <!-- inicio_cuadro  -->
                            ';

              if($index=='2' || $index=='5' || $index=='8' || $index=='11' || $index=='14' || $index=='17' || $index=='20' || $index=='23' || $index=='26' ||
                  $index=='29' || $index=='32' || $index=='35' || $index=='38' || $index=='41' || $index=='44' || $index=='47' || $index=='50' || $index=='53' || $index=='56' || $index=='59'){
                  $str .= '</div><div class="row"  > '; //row fin
              }

              return $str;
          },
          'pager' => [
              'firstPageLabel' => 'Inicio',
              'lastPageLabel' => 'Fin',
              'nextPageLabel' => '>>',
              'prevPageLabel' => '<<',
              'maxButtonCount' => 10,
          ],
      ]);
      */
  }



                ?>
            </div>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

<!-- SUSCRIPCIONES -->


<hr class="hr-separador">

<script id="jsbin-javascript">
    var slider = new Slider("#slider1");
    slider.on("slide", function(slideEvt) {
        console.log(slider.getValue() );
    });
</script>


<style>
    .box{
        box-shadow: none;
    }
    .btn-facebook {
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        padding: 8px;
        color: #969696;font-size: 20px;
    }
    .btn-google {
        /* color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0, 0, 0, 0.2); */
        border: 1px solid #D9D9D9;
        border-radius: 15px;
        color: #969696;
        font-size: 20px;
    }

    .btn-login {
        color: #fff;
        background-color: #F33B42;
        border-color: #F33B42;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px #fff;
        box-shadow: 0 1px 1px #fff;
    }
    .panel-heading .box {
        padding: 18px 0 6px;
        text-align: center;
    }

    .panel-heading .line {
        margin-top: 4px;
        margin-bottom: 20px;
        border: 0;
        width: 100%;
        border-top: 1px solid #c5c5c54d;

    }

    .panel-heading .active .line {
        border-top: 3px solid #F33B42;

    }


    .panel-heading .text {
        color: #000;
        font-weight: bold;
    }

    .panel-heading .text2{
        color: #000;
        font-weight: normal;
    }

    .panel-heading .text, .text2:hover,
    .panel-heading .text, .text2:focus {
        text-decoration: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        border: 1px solid #fff;
        border-radius: .375rem;
        background-color: #fff;
        background-clip: border-box;
    }

    .card-body {
        padding: 1.5rem;
        flex: 1 1 auto;
    }

    .card-header {
        margin-bottom: 0;
        padding: 1.25rem 1.5rem;
        border-bottom: 1px solid rgba(0, 0, 0, .05);
        background-color: #fff;
    }

    .card-header:first-child {
        border-radius: calc(.375rem - 1px) calc(.375rem - 1px) 0 0;
    }

    .shadow {
        /* box-shadow: 0 0 2rem 0 rgba(136, 152, 170, .15) !important;*/
    }

    #signupform-acepto_terminos {
        display: block;
    }

    .tambien{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 18px;

        letter-spacing: 0.0278198px;

        color: #969696;
    }

    .titulos_form{
        font-family: Source Sans Pro;
        font-size: 12px;
        line-height: 14px;
        display: flex;
        align-items: center;
        letter-spacing: 0.0238455px;
        font-weight: normal;
        color: #F33B42 !important;
    }
    .help-block{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 14px;
        /* identical to box height */


        text-align: right;
        letter-spacing: 0.0218584px;

        color: #353435 !important;
    }

    .rosado_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #FF596A;
    }

    .negro_aref{
        font-family: Source Sans Pro;
        font-style: normal;font-weight: 600;font-size: 14px;line-height: 18px; letter-spacing: 0.0278198px;
        color: #353435;
    }

    .normal_text{
        font-family: Source Sans Pro;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 18px;
        letter-spacing: 0.0278198px;

        color: #353435;
    }

    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 0px;
        font-weight: 700;
    }
</style>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<?php
use yii\helpers\Html;
use yii\authclient\widgets\AuthChoice;

\yii\bootstrap\Modal::begin([
    'header' => '<h4 class="text-danger  modal-title">Debes iniciar sesión para realizar la acción</h4>',
    'id'     => 'modal-login',
    'size' => 'modal-lg',
    'clientOptions' => [

        //'backdrop' => 'static',
        'keyboard' => false],
    'footer' => null,
]); ?>
<input type="hidden" id="param" name="param">
<?php

$model = new LoginForm();
$modelSignup = new SignupForm();
$modelCliU = new CliUsuario();
/*
echo $this->render('loginpopup', [
    'model' => $model,'modelSignup' => $modelSignup,'modelCliU' => $modelCliU,
])
*/
?>
<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->


    <div class="panel panel-login card bg-secondary shadow border-0" style="width: 80%;height: auto;
    margin: 0 auto;">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6 box active" id="login-form-link">
                    <a href="#" class="text txt_is">INICIAR SESIÓN</a>
                    <hr class="line">
                </div>
                <div class="col-xs-6 box" id="register-form-link">
                    <a href="#" class="text2 txt_reg" style="">REGISTRATE</a>
                    <hr class="line">
                </div>
            </div>
            <!-- <hr> -->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=>false,'method'=>'post','action'=>\yii\helpers\Url::to(['site/login'])]); ?>

                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <!-- <?= $form->field($model, 'rememberMe')->checkbox() ?> -->
                    </div>
                    <div class="col-lg-12">
                        <div class="pull-right" style="padding-right: 10px; margin-bottom: 15px"><?= Html::a('<span class="titulos_form">Olvidé mi contraseña</span>', ['site/request-password-reset']) ?></div>
                    </div>
                    <div class="form-group" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::Button('Iniciar sesion', ['id'=>'btn_iniciar_sesion','class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#F33B42;font-size:11px', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                    </div>



                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>
                    <p class="tambien" style="font-weight: normal">También puedes ingresar con</p>
                    <hr style="width: 70%;text-align: center">
                    <div class="social-button-login">
                        <div class="row">
                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-3 col-sm-offset-3" style="margin: 0.5em;color:#969696">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <?php ActiveForm::end(); ?>

                    <!-- ############################################### -->
                    <?php $form = ActiveForm::begin(['id' => 'register-form',  'options' => ['style' => 'display: none;']]); ?>

                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'nombre')->label( '<span class="titulos_form">Nombre completo</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'email')->textInput(['autofocus' => true])->label('<span class="titulos_form">Correo electrónico</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'telefonos')->label('<span class="titulos_form">Número de contacto</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelCliU, 'whatssapp')->textInput(['type' => 'text', 'maxlength' => 10])->label('<span class="titulos_form">Número de WhatsApp</span>')?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'password')->passwordInput()->label('<span class="titulos_form">Contraseña</span>') ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($modelSignup, 'repeat_password')->passwordInput()->label('<span class="titulos_form">Confirmación de contraseña</span>') ?>
                        </div>
                    </div>
                    <p class="tambien" style="margin-top: 10px">&nbsp;</p>
                    <!-- start auth facebook y google -->
                    <?php
                    $authAuthChoice = AuthChoice::begin([
                        'baseAuthUrl' => ['auth'],
                        'options' => ['class' => 'social-auth-links text-center'],
                    ])
                    ?>


                    <div class="social-button-login">
                        <p class="tambien" style="margin-top: 10px">También puedes ingresar con</p>
                        <hr style="    margin-top: 0px;    margin-bottom: 9px;    border: 0;    border-top: 1px solid #eeeeee;">
                        <div class="row">

                            <div class="" style="display: flex; justify-content: center">
                                <?php foreach ($authAuthChoice->getClients() as $name => $client) : ?>
                                    <div class="col-sm-4 col-sm-offset-3" style="margin: 0.5em;">
                                        <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                        <?php $class = $name === 'live' ? 'windows' : $name ?>
                                        <?php $text = $class === 'facebook' ?
                                            sprintf('<img src="https://static.xx.fbcdn.net/rsrc.php/yD/r/d4ZIVX-5C-b.ico?_nc_eui2=AeG9RbK1PdO9U0L2rKpXjC0yaBWfmC2eGbdoFZ-YLZ4Zt7MyxjDN3l9r3i_CEZf7lSY"> ' . 'Facebook', "fa fa-$class", $letter, $client->getTitle()) :
                                            sprintf('<img width="35px" src="https://img.icons8.com/fluency/48/000000/google-logo.png"> ' . 'Google', "fa fa-$class", $letter, $client->getTitle());
                                        ?>
                                        <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <?php AuthChoice::end() ?>
                    <!-- end auth facebook y google -->
                    <div class="row">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12">


                                    <table class="table" style="margin-top: 20px">
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-promocionales" name="SignupForm[promocionales]" value="1" aria-required="true">
                                            </td>
                                            <td class="normal_text" style="border: 0px">
                                                Me gustaría recibir comunicaciones promocionales (Recibirás un e-mail de confirmación)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 0px">
                                                <input type="checkbox" id="signupform-acepto_terminos" name="SignupForm[acepto_terminos]" value="1" aria-required="true">
                                            </td>
                                            <td style="border: 0px" class="normal_text">Declaro que he leído y acepto la nueva <a target="_blank" href="<?= Url::to(['site/politicadeprivacidad']) ?>" class="rosado_aref" style="">Política de privacidad</a> y los
                                                <a class="rosado_aref" target="_blank" href="<?= Url::to(['site/terminosycondiciones']) ?>">Términos y condiciones</a> de VENDETUMOTO.CO
                                            </td>
                                        </tr>
                                    </table>


                                    <?= $form->field($modelSignup, 'promocionales')->hiddenInput()->label(false) ?>
                                    <?= $form->field($modelSignup, 'acepto_terminos')->hiddenInput()->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <?= Html::submitButton('Registrarme', ['class' => 'form-control btn btn-login', 'style' => 'border-radius:17px;background:#FF596A;font-size:11px', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <!-- ############################################### -->
                </div>

            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::end();


?>
