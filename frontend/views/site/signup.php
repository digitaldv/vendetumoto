<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<link href='<?= Yii::$app->request->baseUrl . '/js/slider/style.css'?>' rel='stylesheet'/>
<style>
    .btn-facebook {
        color: #fff;
        background-color: #3b5998;
        border-color: rgba(0,0,0,0.2);
    }
    .btn-google{
        color: #fff;
        background-color: #dd4b39;
        border-color: rgba(0,0,0,0.2);
    }
</style>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <!-- start auth facebook y google -->
                <?php
                $authAuthChoice = AuthChoice::begin([
                            'baseAuthUrl' => ['auth'],
                            'options' => ['class' => 'social-auth-links text-center'],
                        ])
                ?>
                <!--<p>- O -</p>-->
                <div class="social-button-login">
                    <div class="row">
                            <?php foreach ($authAuthChoice->getClients() as $name => $client):   ?>
                            <div class="col-xs-12" style="margin-bottom: 0.5em">
                                <?php $letter = $name === 'yandex' ? 'Я' : '' ?>
                                <?php $class = $name === 'live' ? 'windows' : $name ?>
                                <?php $text = 'Acceder con' . sprintf('<i class="%s">%s</i>&nbsp;%s', "fa fa-$class", $letter, $client->getTitle()) ?>
                            <?= $authAuthChoice->clientLink($client, $text, ['class' => "btn btn-block btn-social btn-$class "]) ?>
                            </div>
                            <?php endforeach ?>
                    </div>
                </div>
                <?php AuthChoice::end() ?>
                <!-- end auth facebook y google -->
                
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
