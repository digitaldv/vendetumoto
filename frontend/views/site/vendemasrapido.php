<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Vende más rápido - VTM';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left" style="margin-bottom: 18px">
    <div class="fusion-page-title-row">
        <div class="container" >
            <div class="col-lg-12 col-md-12 col-sm-12" style="vertical-align: middle;margin-top: 0px;" >
                <h1 style="color:#e46a76;font-size: 33px" class=" ">
                 <?= Html::encode('Vende más rápido') ?>
                </h1>

            </div>
        </div>
    </div>
</div>
<hr style="margin-bottom: 3px; margin-top: 3px; border: 1px solid #e46a7652">
<div class="container" style="margin-top: 20px">

    <?php


        if ($contenido->foto == 0 || $contenido->foto == 1) {
            ?>

            <div class="row margin_top_2">
                <div class="col-lg-12" >
                    <div>
                        <h4 class='div_1'><?= $contenido->titulo ?></h4>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="padd_1" >
                        <?= $contenido->contenido ?>
                    </div>
                </div>

            </div>

            <?php } else if ($contenido->foto == 2) {
            ?>

            <div class="row margin_top_2" >
                <div class="col-lg-4 col-md-4 col-sm-12"  >
                    <div class="div_dot" >
                        <img width="100%" src="<?= Yii::$app->request->baseUrl . '/../../backend/web/uploads/paginas_internas/' . $contenido->archivo ?>" />&nbsp;
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="padd_1" >
                        <h4 class='div_1'><?= $contenido->titulo ?></h4>
                        <hr style="margin-bottom: 3px; margin-top: 3px; border: 1px solid #c9b3b552">
                        <p><?= $contenido->contenido ?></p>
                    </div>
                </div>

            </div>

        <?php } if ($contenido->foto == 3) { ?>
        <div class="row margin_top_2" >

            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="padd_1" >
                    <h4 class='div_1'><?= $contenido->titulo ?></h4>
                    <hr style="margin-bottom: 3px; margin-top: 3px; border: 1px solid #c9b3b552">
                    <p><?= $contenido->contenido ?></p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12"  >
                <div class="div_dot" >
                    <img width="100%" src="<?= Yii::$app->request->baseUrl . '/../../backend/web/uploads/paginas_internas/' . $contenido->archivo ?>" />&nbsp;
                </div>
            </div>

        </div>
            <?php
        
    }
    ?>


</div>
