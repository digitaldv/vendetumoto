<?php
/**
 * @var $this yii\web\View
 * @var $model webvimark\modules\UserManagement\models\forms\LoginForm
 */

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
    <style>
        .yyy {
            float: center;
            background-color: rgba(255, 255, 255, 0.498039);
            border: 1px solid #fff;
            border-radius: 8px;
            margin-left: auto;
            margin-right: auto;
        }
        .btn-danger:active:hover, .btn-danger.active:hover, .open > .dropdown-toggle.btn-danger:hover, .btn-danger:active:focus, .btn-danger.active:focus, .open > .dropdown-toggle.btn-danger:focus, .btn-danger:active.focus, .btn-danger.active.focus, .open > .dropdown-toggle.btn-danger.focus {
            color: #fff;
            background-color: #ff5722;
            border-color: #761c19;
        }
        .btn-danger {
            color: #fff;
            background-color: #ff5722;
            border-color: #989ec5;
        }

    </style>
<div class="container" id="login-wrapper">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">

            <div class="panel panel-default yyy">

                <div class="panel-heading bg-red text-center " style="background-color: rgba(255, 255, 255, 0.498039) !important">
                    <span class='text-center' style="margin-bottom:10px;width:100%; font-size:32px;"  >
                        <img class=" "    style="margin-left: -13px;" src="<?= Yii::$app->request->baseUrl . '/img/logo.png' ?>">
                    </span>

                    <div class='row' style="padding: 7px 0px 0px 0px;border-top: 1px solid appworkspace;margin-top: 16px;">
                        <span style="margin-top: 10px;color:#333"><?= 'INICIO DE SESIÓN' ?></span>
                    </div>
                </div>
				<div class="panel-body">

					<?php $form = ActiveForm::begin([
						'id'      => 'login-form',
						'options'=>['autocomplete'=>'off'],
						'validateOnBlur'=>false,
						'fieldConfig' => [
							'template'=>"{input}\n{error}",
						],
					]) ?>

                    <?= $form->field($model, 'username')
                        ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'autocomplete' => 'off'])
                    ?>

                    <?= $form->field($model, 'password')
                        ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off'])
                    ?>

                    <?= $form->field($model, 'rememberMe')->label('Recordarme')->checkbox(['value' => true]) ?>

                    <?=
                    Html::submitButton(
                        Yii::t('app', 'Ingresar'), ['class' => 'btn btn-lg btn-danger btn-block', 'style'=>'background:#F33B42'                          ]
                    )
                    ?>

					<div class="row registration-block">
						<div class="col-sm-6">
							<?= GhostHtml::a(
								UserManagementModule::t('front', "Registration"),
								['/user-management/auth/registration']
							) ?>
						</div>
						<!--div class="col-sm-6 text-right">
							<?= GhostHtml::a(
								UserManagementModule::t('front', "Forgot password ?"),
								['/user-management/auth/password-recovery']
							) ?>
						</div-->
					</div>




					<?php ActiveForm::end() ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$css = <<<CSS
html, body {
	background: #eee;
	-webkit-box-shadow: inset 0 0 100px rgba(0,0,0,.5);
	box-shadow: inset 0 0 100px rgba(0,0,0,.5);
	height: 100%;
	min-height: 100%;
	position: relative;
}
#login-wrapper {
	position: relative;
	top: 30%;
}
#login-wrapper .registration-block {
	margin-top: 15px;
}
CSS;

$this->registerCss($css);
?>
